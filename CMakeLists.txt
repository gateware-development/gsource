# File: CMakeLists.txt
# Purpose: CMake text file for the creation of the project.
# Author: Gerard Vega
# Contributors: Yangjie Yao, Lari Norri, Caio Tavares, Artemis Kelsie Frost, Alex Kozyrev, Ozzie Mercado
# Copyright: 7thGate Software LLC.
# License: MIT

cmake_minimum_required(VERSION 3.19.2)

# Defined for developers but not for end users using Gateware.h
add_compile_definitions(GATEWARE_FEATURE_FLAG_IN_DEVELOPMENT)

########################################################################

if(WIN32)
    message("This is a Win32 OS")

    #suppress warnings
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /bigobj /wd4250 /wd4723 /Zc:strictStrings")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj /wd4250 /wd4723 /Zc:strictStrings")

    set(CMAKE_EXE_LINKER_FLAGS "/machine:x64")
    # These do nothing on MSVC, removed
    #set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -m64 -g")
    #set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -m64 -g")

    if(${CMAKE_EXE_LINKER_FLAGS} MATCHES "/machine:x64")
        message("This is a x64 machine.")
        project(Gateware_amd64)
		
        set(Architecture amd64)

    elseif(${CMAKE_EXE_LINKER_FLAGS} MATCHES "/machine:X86")
        message("Only x64 is supported.") #maybe include the correct steps to fix this from user side.

    endif()

    set(CMAKE_CONFIGURATION_TYPES "Debug;Release")

endif(WIN32)

########################################################################

## Linux ##
if (UNIX AND NOT APPLE)

    # Set the C++ standard to C++11
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)

    # Set the C standard to C11 (not sure if this is the correct version, but I would assume so)
    set(CMAKE_C_STANDARD 11)
    set(CMAKE_C_STANDARD_REQUIRED ON)

    message("This is Linux OS")
    # Gateware uses [#pragma comment(lib,"somelib.a")] to auto link dependencies on Windows & Linux (@Import on MacOS)
    # This means we need Clang because GCC does not support this feature.
    # UPDATE: Unfortunately Auto-Linking is not yet supported in the LLD linker used by Clang & LLVM.(use GCC for now)   
    # set(CMAKE_C_COMPILER clang)
    # set(CMAKE_CXX_COMPILER clang++) # The Mint version of Clang is 6, Ubuntu seems to run the latest one.

    message("This is an amd64 config")
    set(Architecture amd64)

    # If the build directory doesn't exist, create it.
    if(NOT EXISTS ${CMAKE_BINARY_DIR})
        file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR})
    endif()

    # Get the folder name of the build directory.
    get_filename_component(BUILD_DIR_NAME ${CMAKE_BINARY_DIR} NAME)

    # Add a '.' to the beginning of the folder name.
    set(BUILD_DIR_NAME .${BUILD_DIR_NAME})

    # Execute process touch with the BUILD_DIR_NAME as the argument, set the working directory to the build directory.
    execute_process(COMMAND touch ${BUILD_DIR_NAME} WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

    # Create project
    project(Gateware)

    # If the system is 32-bit, exit with critical error.
    if(CMAKE_SIZEOF_VOID_P EQUAL 4)
        message(FATAL_ERROR "Gateware does not support 32-bit systems.")
    endif()

    # Configure for multi-configuration build systems.
    if(CMAKE_CONFIGURATION_TYPES)

        message("This is a multi-configuration build system.")
        set(CMAKE_CONFIGURATION_TYPES "Debug;Release")

    else()

        message("This is a single-configuration build system.")

    endif(CMAKE_CONFIGURATION_TYPES)

    # NOTE: There is no configuration needed for debug and release as CMake will already handle this for us through
    # the CMAKE_CXX_FLAGS_DEBUG and CMAKE_CXX_FLAGS_RELEASE variables. On multi-configuration build systems, CMake
    # will automatically use CMAKE_CXX_FLAGS_DEBUG_INIT and CMAKE_CXX_FLAGS_RELEASE_INIT to set the flags for the
    # respective configurations. The debug will use the commands `-g` and the release will use `-O3 -DNDEBUG`.
    # If the compiler is GCC, set the flags
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        message("This is a GCC compiler.")
        SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m64 -Werror=write-strings")
        SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64 -pthread -Werror=write-strings") # test: -std=c++1y -fms-extensions
        SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -m64 -Werror=write-strings")
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        message("This is a Clang compiler.")
        SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m64 -Wno-missing-braces -Wno-parentheses -Wno-switch -Wno-reorder -Werror=write-strings")
        SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64 -pthread -Wno-missing-braces -Wno-parentheses -Wno-switch -Wno-reorder -Werror=write-strings") # test: -std=c++1y -fms-extensions
        SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -m64 -Werror=write-strings")
    else()
        message(FATAL_ERROR "Unsupported compiler.")
    endif()

    # If the generator is "CodeLite - Unix Makefiles" set the CMAKE_CODELITE_USE_TARGETS to ON.
    if(CMAKE_GENERATOR MATCHES "CodeLite - Unix Makefiles")

        # This & "-pthread" above was added to enable CodeLite+gcc IDE to be compatible with existing scripts.
        # It seems it may be possible to merge both debug & release into one workspace(but not project) I have not investigated further.
        set(CMAKE_CODELITE_USE_TARGETS ON)

    endif()

endif()

########################################################################

## Mac ##
if(APPLE)
    message("This is Apple OS")

    Project(Gateware_Apple)

    # Thanks to wildkid.long for this snippet.
    # https://discourse.cmake.org/t/how-to-determine-which-architectures-are-available-apple-m1/2401/13
    execute_process(COMMAND uname -m
    OUTPUT_VARIABLE CMAKE_OSX_ARCHITECTURES
    OUTPUT_STRIP_TRAILING_WHITESPACE)

    if (CMAKE_OSX_ARCHITECTURES STREQUAL "arm64")
        message("Building for arm64 chipset [Apple Silicon]")
    elseif (CMAKE_OSX_ARCHITECTURES STREQUAL "x86_64")
	message("Building for x86_64 chipset [Intel Silicon]")
    else()
	message("Building for unsupported chipset [?PowerPC Silicon?]")
    endif()

    #suppress warnings
    Set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Werror=write-strings -Wno-missing-braces -Wno-parentheses -Wno-switch -Wno-reorder")
    Set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fmodules -fcxx-modules -x objective-c++ -Werror=write-strings -Wno-missing-braces -Wno-parentheses -Wno-switch -Wno-reorder")

    
endif(APPLE)

########################################################################

ADD_DEFINITIONS(-DUNICODE)
ADD_DEFINITIONS(-D_UNICODE)

#For doxygen, need more research and intergration into the project
#add_custom_target(docs ALL DEPENDS Doxyfile)

add_subdirectory(Desktop)