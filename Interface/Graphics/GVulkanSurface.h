// Currently there is no method to installing Vulkan in Linux Pipelines,
// enabling CICD feature flag skips this library entirely.
#ifndef GVULKANSURFACE_H
#define GVULKANSURFACE_H

/*!
	File: GVulkanSurface.h
	Purpose: A Gateware interface that initializes a Vulkan rendering surface and manages it's core resources.
	Author: Derrick Ramirez
	Contributors: Ryan Powser, Lari H. Norri, John Fordinal
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventResponder.h"
#include "../Core/GEventGenerator.h"
#include "../System/GWindow.h"
#include "../Graphics/GGraphicsDefines.h"

namespace GW
{
	namespace I
	{
		class GVulkanSurfaceInterface : public virtual GEventResponderInterface, public virtual GEventGeneratorInterface
		{
		public:
			enum class Events {
				REBUILD_PIPELINE,
				RELEASE_RESOURCES
			};

			struct EVENT_DATA {
				int eventResult;
				unsigned int surfaceExtent[2];
			};

			struct GVulkanSurfaceQueryInfo {
				unsigned long long initializationMask;
				unsigned int instanceLayerCount;
				const char** instanceLayers;
				void*		 instanceLayerProperties;
				unsigned int instanceExtensionCount;
				const char** instanceExtensions;
				void*		 instanceExtensionProperties;
				unsigned int deviceExtensionCount;
				const char** deviceExtensions;
				void*		 deviceExtensionProperties;
				void*		 physicalDeviceFeatures;
			};

			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;
			virtual GReturn GetSwapchainImageCount(unsigned int& _outImageCount) const = 0;
			virtual GReturn GetSwapchainCurrentImage(unsigned int& _outImageIndex) const = 0;
			virtual GReturn GetQueueFamilyIndices(unsigned int& _outGraphicsIndex, unsigned int& _outPresentIndex) const = 0;
			virtual GReturn GetGraphicsQueue(void** _outVkQueue) const = 0;
			virtual GReturn GetPresentQueue(void** _outVkQueue) const = 0;
			virtual GReturn GetSwapchainImage(const int& _index, void** _outVkImage) const = 0;
			virtual GReturn GetSwapchainView(const int& _index, void** _outVkImageView) const = 0;
			virtual GReturn GetSwapchainFramebuffer(const int& _index, void** _outVkFramebuffer) const = 0;
			virtual GReturn GetSwapchainDepthBufferImage(const int _index, void** _outVkDepthImage) const = 0;
			virtual GReturn GetSwapchainDepthBufferView(const int _index, void** _outVkDepthView) const = 0;

			virtual GReturn GetInstance(void** _outVkInstance) const = 0;
			virtual GReturn GetSurface(void** _outVkSurfaceKHR) const = 0;
			virtual GReturn GetPhysicalDevice(void** _outVkPhysicalDevice) const = 0;
			virtual GReturn GetDevice(void** _outVkDevice) const = 0;
			virtual GReturn GetCommandPool(void** _outCommandPool) const = 0;
			virtual GReturn GetSwapchain(void** _outVkSwapchainKHR) const = 0;
			virtual GReturn GetRenderPass(void** _outRenderPass) const = 0;
			virtual GReturn GetCommandBuffer(const int& _index, void** _outCommandBuffer) const = 0;
			virtual GReturn GetImageAvailableSemaphore(const int& _index, void** _outVkSemaphore) const = 0;
			virtual GReturn GetRenderFinishedSemaphore(const int& _index, void** _outVkSemaphore) const = 0;
			virtual GReturn GetRenderFence(const int& _index, void** _outVkFence) const = 0;

			virtual GReturn StartFrame(const unsigned int& _clearCount, void* _vkClearValues) = 0;
			virtual GReturn EndFrame(const bool& _vSync) = 0;
		};
	}
}

#include "../../Source/Graphics/GVulkanSurface/GVulkanSurface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware graphics interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that initializes a Vulkan rendering surface and manages it's core resources.
		class GVulkanSurface final
			: public I::GProxy<I::GVulkanSurfaceInterface, I::GVulkanSurfaceImplementation, GW::SYSTEM::GWindow, unsigned long long>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GVulkanSurface)
			GATEWARE_TYPEDEF(Events)
			GATEWARE_TYPEDEF(EVENT_DATA)
			GATEWARE_TYPEDEF(GVulkanSurfaceQueryInfo)
			GATEWARE_CONST_FUNCTION(GetAspectRatio)
			GATEWARE_CONST_FUNCTION(GetSwapchainImageCount)
			GATEWARE_CONST_FUNCTION(GetSwapchainCurrentImage)
			GATEWARE_CONST_FUNCTION(GetQueueFamilyIndices)
			GATEWARE_CONST_FUNCTION(GetGraphicsQueue)
			GATEWARE_CONST_FUNCTION(GetPresentQueue)
			GATEWARE_CONST_FUNCTION(GetSwapchainImage)
			GATEWARE_CONST_FUNCTION(GetSwapchainView)
			GATEWARE_CONST_FUNCTION(GetSwapchainFramebuffer)
			GATEWARE_CONST_FUNCTION(GetSwapchainDepthBufferImage)
			GATEWARE_CONST_FUNCTION(GetSwapchainDepthBufferView)
			GATEWARE_CONST_FUNCTION(GetInstance)
			GATEWARE_CONST_FUNCTION(GetSurface)
			GATEWARE_CONST_FUNCTION(GetPhysicalDevice)
			GATEWARE_CONST_FUNCTION(GetDevice)
			GATEWARE_CONST_FUNCTION(GetCommandPool)
			GATEWARE_CONST_FUNCTION(GetSwapchain)
			GATEWARE_CONST_FUNCTION(GetRenderPass)
			GATEWARE_CONST_FUNCTION(GetCommandBuffer)
			GATEWARE_CONST_FUNCTION(GetImageAvailableSemaphore)
			GATEWARE_CONST_FUNCTION(GetRenderFinishedSemaphore)
			GATEWARE_CONST_FUNCTION(GetRenderFence)
			GATEWARE_FUNCTION(StartFrame)
			GATEWARE_FUNCTION(EndFrame)

			// reimplemented functions
			// from GEventResponderInterface
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)

			// from GEventGeneratorInterface
			GATEWARE_FUNCTION(Register)
			GATEWARE_CONST_FUNCTION(Observers)
			GATEWARE_FUNCTION(Push)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
		   	//! Creates and outputs a new GVulkanSurface object.
		   	/*!
		   	*	Initializes a handle to a GVulkanSurface object with an existing GWindow.
		   	*	The created GVulkanSurface object will have its reference count initialized
		   	*	to one and register as a listener to the provided GWindow object.
		   	*
		   	*	This function accepts a bit mask that can hold
		   	*	supported GGraphicsInitOptions, which will
		   	*	be taken into account when creating the context.
		   	*	To ignore this mask, simply pass in 0 when calling
		   	*	this function and the context will be created with
		   	*	default settings.
		   	*
		   	*
			*	\param [in] _gwindow A pointer to an existing GWindow object.
			*	\param [in] _initMask The bit mask that can hold special initialization options.
			*
			*	\retval GReturn::INVALID_ARGUMENT _gwindow is invalid.
			*	\retval GReturn::FAILURE A GVulkanSurface object was not created.
			*	\retval GReturn::SUCCESS A GVulkanSurface object was successfully created.
			*/
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask);

			//! Creates and outputs a new GVulkanSurface object, along with giving more control to add extensions and layers
		   	/*!
		   	*	Initializes a handle to a GVulkanSurface object with an existing GWindow.
		   	*	The created GVulkanSurface object will have its reference count initialized
		   	*	to one and register as a listener to the provided GWindow object.
		   	*
		   	*	This function accepts a bit mask that can hold
		   	*	supported GGraphicsInitOptions, which will
		   	*	be taken into account when creating the context.
		   	*	To ignore this mask, simply pass in 0 when calling
		   	*	this function and the context will be created with
		   	*	default settings.
			*	
			*	This function accepts 6 additional parameters. This allows the ability to
		   	*	add additional functionality to your vulkan surface. This include additional
			*	debugging layers and additional extensions based on what your GPU can do.
			*
		   	*
			*	\param [in] _gwindow A pointer to an existing GWindow object.
			*	\param [in] _initMask The bit mask that can hold special initialization options.
			*	\param [in] _layerCount The count on how many layers you are providing to the vulkan instance.
			*	\param [in] _layers The list of Validation Layers to enable within the vulkan instance
			*	\param [in] _instanceExtensionCount The count on how many instance extensions you are providing to the vulkan instance
			*	\param [in] _instanceExtensions The list of Instance Extensions to enable within the vulkan instance
			*	\param [in] _deviceExtensionCount The count on how many device extensions you are providing to the vulkan logical device
			*	\param [in] _deviceExtensions The list of device extensions to enable within the vulkan logical device
			*	\param [in] _allPhysicalDeviceFeatures Bool if you want to enable all supported features of your GPU
			*
			*	\retval GReturn::INVALID_ARGUMENT _gwindow is invalid, or there is a bad match of "count" and layers/extension provided (ex: 1 count but no layer/extension, or 0 count yet a layer/extension was sent.)
			*	\retval GReturn::FAILURE A GVulkanSurface object was not created.
			*	\retval GReturn::SUCCESS A GVulkanSurface object was successfully created.
			*/
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask, unsigned int _layerCount, const char** _layers, unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount, const char** _deviceExtensions, bool _allPhysicalDeviceFeatures);

			//! Creates and outputs a new GVulkanSurface object with enhanced options, *NEW* add support for EXT layer settings
			/*!
			*	Same as the previous Create() function, but with the added support for EXT layer settings.
			*
			*
			*	\param [in] _gwindow A pointer to an existing GWindow object.
			*	\param [in] _initMask The bit mask that can hold special initialization options.
			*	\param [in] _layerCount The count on how many layers you are providing to the vulkan instance.
			*	\param [in] _layers The list of Validation Layers to enable within the vulkan instance
			*	\param [in] _layerSettingsEXTCount The count on how many layer settings you are providing to the vulkan instance
			*	\param [in] _layerSettingsEXT The list of layer settings (VkLayerSettingEXT) to enable within the vulkan instance
			*	\param [in] _instanceExtensionCount The count on how many instance extensions you are providing to the vulkan instance
			*	\param [in] _instanceExtensions The list of Instance Extensions to enable within the vulkan instance
			*	\param [in] _deviceExtensionCount The count on how many device extensions you are providing to the vulkan logical device
			*	\param [in] _deviceExtensions The list of device extensions to enable within the vulkan logical device
			*	\param [in] _allPhysicalDeviceFeatures Bool if you want to enable all supported features of your GPU
			*
			*	\retval GReturn::INVALID_ARGUMENT _gwindow is invalid, or there is a bad match of "count" and layers/extension provided (ex: 1 count but no layer/extension, or 0 count yet a layer/extension was sent.)
			*	\retval GReturn::FAILURE A GVulkanSurface object was not created.
			*	\retval GReturn::SUCCESS A GVulkanSurface object was successfully created.
			*/
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask, unsigned int _layerCount, const char** _layers, unsigned int _layerSettingsEXTCount, const void* _layerSettingsEXT, unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount, const char** _deviceExtensions, bool _allPhysicalDeviceFeatures);

			//! Events generated by GVulkanSurface. These messages are used to notify GVulkanSurface's observers whenever the swapchain is required to be rebuilt.
			enum class Events {
				REBUILD_PIPELINE, //!< VkSwapchainKHR has been destroyed and pipelines must be rebuilt to accomodate.
				RELEASE_RESOURCES //!< Internal Vulkan resources are about to be destroyed.Destroy any child objects.
			};

			/*! EVENT_DATA provided by GVulkanSurface. */
			struct EVENT_DATA {
				int eventResult; /*!< This provides what VkResult was the reason that the VkSwapchainKHR must be rebuilt. */
				unsigned int surfaceExtent[2]; /*!< The surface extent used to create the VkSwapchainKHR. */
			};

			/*! GVulkanSurfaceQueryInfo contains all the features that Vulkan supports on the host machine. */
			struct GVulkanSurfaceQueryInfo {
				unsigned long long initializationMask;	/*!< The init mask that can be used to initialize the GVulkanSurface. */
				unsigned int instanceLayerCount;		/*!< The total amount of Validation Layers supported. */
				const char** instanceLayers;			/*!< The Validation Layers in const char* form. can be used directly with Create() method. */
				void* instanceLayerProperties;			/*!< The Validation Layers in property form. <b>can not</b> be used directly with Create() method. */
				unsigned int instanceExtensionCount;	/*!< The total amount of Instance Rxtensions supported. */
				const char** instanceExtensions;		/*!< The Instance Extensions in const char* form. can be used directly with Create() method. */
				void* instanceExtensionProperties;		/*!< The Instance Extensions in property form. <b>can not</b> be used directly with Create() method. */
				unsigned int deviceExtensionCount;		/*!< The total amount of Device Extensions supported. */
				const char** deviceExtensions;			/*!< The Device Extensions in const char* form. can be used directly with Create() method. */
				void* deviceExtensionProperties;		/*!< The Device Extensions in property form. <b>can not</b> be used directly with Create() method. */
				void* physicalDeviceFeatures;			/*!< The Physical Device Features supported. */
			};

			//! Returns the aspect ratio for the current window.
			/*!
			*
			*	\param	[out] _outRatio will return the calculated aspect ratio.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION		All Vulkan Objects fails to exist due to GWindow being destroyed.
			*   \retval GReturn::FAILURE					No active GWindow exists to calculate an aspect ratio from.
			*	\retval GReturn::EMPTY_PROXY				The proxy is empty
			*	\retval GReturn::SUCCESS					The current aspect ratio was calculated and returned.
			*/
			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;

			//! Returns the size of the Swapchain's Buffers.
			/*!
			*
			*	\param	[out] _outImageCount will return the amount of images the swap chain contains.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The current swap chain's image count was returned.
			*/
			virtual GReturn GetSwapchainImageCount(unsigned int& _outImageCount) const = 0;

			//! Returns the swap chain's current index.
			/*!
			*
			*	\param	[out] _outImageIndex will return the current index in the system.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The current swap chain's image count was returned.
			*/
			virtual GReturn GetSwapchainCurrentImage(unsigned int& _outImageIndex) const = 0;

			//! Returns the Queue Family Indices for both Graphics and Present Queue. Used for Swapchain (and others)
			/*!
			*
			*	\param	[out] _outGraphicsIndex will return the index that belongs to the graphics queue
			*	\param	[out] _outPresentIndex return the index that belongs to the present queue
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outQueueIndices is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The index count is greater than 0 and returned along with the Queue Indices
			*/	
			virtual GReturn GetQueueFamilyIndices(unsigned int& _outGraphicsIndex, unsigned int& _outPresentIndex) const = 0;

			//! Returns the address of the current VkQueue for Graphic Queue List.
			/*!
			*
			*	\param	[out] _outVkQueue Will contain the address of the VkQueue for graphics queue list.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outGraphicsQueue parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan graphics queue exists and was returned.
			*/
			virtual GReturn GetGraphicsQueue(void** _outVkQueue) const = 0;

			//! Returns the address of the current VkQueue for Present Queue List.
			/*!
			*
			*	\param	[out] _outVkQueue Will contain the address of the VkQueue for present queue list.
			*
			*	\retval GReturn::INVALID_ARGUMENT _outPresentQueue parameter is nullptr.
			*   \retval GReturn::FAILURE No present queue exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan present queue exists and was returned.
			*/
			virtual GReturn GetPresentQueue(void** _outVkQueue) const = 0;

			//! Returns the address of the current VkImage for swap chain.
			/*!
			*
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					**NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.**
			*	\param	[out]	_outVkImage Will contain the address of the swap chain's image.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range (x < -1, x >= image_count).
			*   \retval GReturn::FAILURE No Swapchain or Swapchain image exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan swap chain images exists and was returned.
			*/
			virtual GReturn GetSwapchainImage(const int& _index, void** _outVkImage) const = 0;

			//! Returns the address of the current VkImageView for the swap chain.
			/*!
			*
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					**NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.**
			*	\param	[out]	_outVkImageView Will contain the address of the swap chain's image view.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range.
			*   \retval GReturn::FAILURE No Swapchain or Swapchain image view exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan swap chain image views exists and was returned.
			*/
			virtual GReturn GetSwapchainView(const int& _index, void** _outVkImageView) const = 0;

			//! Returns the address of the current VkFrameBuffer for the swap chain.
			/*!
			*
			*	\param	[in] _index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*				 **NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.**
			*	\param	[out] _outVkFramebuffer Will contain the address of the swap chain's frame buffers.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outVkFrameBuffer parameter is nullptr or index is out of range. ( -1 <= x < image_count ) is the valid range.
			*   \retval GReturn::FAILURE No Swapchain or Framebuffer exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetSwapchainFramebuffer(const int& _index, void** _outVkFramebuffer) const = 0;

			//! Returns the address of the Depth Buffer image
			/*!
			*
			*	\param	[out] _outVkDepthImage will return the address of the Instance.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*   \retval GReturn::INVALID_ARGUMENT _outVkInstance parameter is nullptr
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan Instance exists and was returned.
			*/
			virtual GReturn GetSwapchainDepthBufferImage(const int _index, void** _outVkDepthImage) const = 0;

			//! Returns the address of the Depth Buffer view
			/*!
			*
			*	\param	[out] _outVkDepthImage will return the address of the Instance.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*   \retval GReturn::INVALID_ARGUMENT _outVkInstance parameter is nullptr
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan Instance exists and was returned.
			*/
			virtual GReturn GetSwapchainDepthBufferView(const int _index, void** _outVkDepthView) const = 0;

			//! Returns the address of the current Instance.
			/*!
			*
			*	\param	[out] _outVkInstance will return the address of the Instance.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outVkInstance parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan Instance exists and was returned.
			*/
			virtual GReturn GetInstance(void** _outVkInstance) const = 0;

			//! Returns the address of the current Surface.
			/*!
			*
			*	\param	[out] _outVkSurfaceKHR will return the address of the Surface.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outVkSurfaceKHR parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan Surface exists and was returned.
			*/
			virtual GReturn GetSurface(void** _outVkSurfaceKHR) const = 0;

			//! Returns the address of the current physical device.
			/*!
			*
			*	\param	[out] _outVkPhysicalDevice will return the address of the physical device.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outPhysicalDevice parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan physical device exists and was returned.
			*/
			virtual GReturn GetPhysicalDevice(void** _outVkPhysicalDevice) const = 0;

			//! Returns the address of the current VkDevice.
			/*!
			*
			*	\param	[out] _outVkDevice Will contain the address of the logical device.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval	GReturn::INVALID_ARGUMENT _outVkDevice parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval	GReturn::SUCCESS A Vulkan logical device exists and was returned.
			*/
			virtual GReturn GetDevice(void** _outVkDevice) const = 0;

			//! Returns the address of the current Command Pool.
			/*!
			*
			*	\param	[out] _outVkCommandPool Will contain the address of the Command Pool.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval	GReturn::INVALID_ARGUMENT _outVkCommandPool parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval	GReturn::SUCCESS A Vulkan Command Pool exists and was returned.
			*/
			virtual GReturn GetCommandPool(void** _outVkCommandPool) const = 0;

			//! Returns the address of the current Swapchain.
			/*!
			*
			*	\param [out] _outVkSwapchainKHR Will contain the address of the swap chain.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchain parameter is nullptr.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS A Vulkan swap chain exists and was returned.
			*/
			virtual GReturn GetSwapchain(void** _outVkSwapchainKHR) const = 0;

			//! Returns the address of the current Renderpass for the swap chain.
			/*!
			*
			*	\param	[out] _outRenderPass Will contain the address of the Renderpass.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range. ( -1 <= x < image_count ) is the valid range.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetRenderPass(void** _outRenderPass) const = 0;

			//! Returns the address of the current Command Buffer for the swap chain.
			/*!
			*
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.
			*	\param	[out] _outCommandBuffer Will contain the address of the Command Buffers.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range. ( -1 <= x < image_count ) is the valid range.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetCommandBuffer(const int& _index, void** _outCommandBuffer) const = 0;

			//! Returns the address of the Semaphore for the swap chain. This Semaphore is designed to signal based on the availability of the Swapchain's image.
			/*!
			*
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.
			*	\param	[out]	_outVkSemaphore Will contain the address of the swap chain's frame buffers.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range (x < -1, x >= image_count).
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetImageAvailableSemaphore(const int& _index, void** _outVkSemaphore) const = 0;

			//! Returns the address of the Semaphore for the swap chain. This Semaphore is designed to signal based on when the rendering has finished.
			/*!
			*
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.
			*	\param	[out]	_outVkSemaphore Will contain the address of the swap chain's frame buffers.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range (x < -1, x >= image_count).
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetRenderFinishedSemaphore(const int& _index, void** _outVkSemaphore) const = 0;

			//! Returns the address of the Fence for the swap chain.
			/*!
			*	\param	[in]	_index will determine what swap chain image the user requests. Valid Index Range: ( -1 <= x < image_count ).
			*					NOTE: sending -1 as a parameter will send you the current object based on Gateware's current index.
			*	\param [out]	_outVkFence Will contain the address of the swap chain's frame buffers.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _outSwapchainImage parameter is nullptr or index is out of range (x < -1, x >= image_count).
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan frame buffers exists and was returned.
			*/
			virtual GReturn GetRenderFence(const int& _index, void** _outVkFence) const = 0;

			//! Setups the command buffer and begins the render pass. [commands: vkBeginCommandBuffer -> vkCmdBeginRenderPass]
			/*!
			*	\param	[in]	_clearCount is the count for the amount of "VkClearColor" being passed in the _vkClearColor parameter.
			*	\param 	[in]	_vkClearValues is the values for the VkClearColor.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*	\retval GReturn::INVALID_ARGUMENT _vkClearColor is nullptr while _vkclearCount is > 0.
			*   \retval GReturn::FAILURE Failed to begin command buffer.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS Vulkan has begin its Render Pass.
			*/
			virtual GReturn StartFrame(const unsigned int& _clearCount, void* _vkClearValues) = 0;

			//! Ends the Command Buffer and Presents the command to the swapchain. [commands: vkCmdEndRenderPass -> vkEndCommandBuffer]
			/*!
			*	\param	[in]	_vSync will enable or disable vsync when presenting to the swapchain.
			*					NOTE: The swapchain is destroyed and re-created when changing the vsync.
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION All Vulkan Objects fails to exist due to GWindow being destroyed.
			*   \retval GReturn::FAILURE	This can come from the following: StartFrame has not been called or did not return SUCCESS, vkEndCommandBuffer has failed, vkQueueSubmit has failed, vkQueuePresentKHR has failed, or failed to recreate swapchain due to vsync change.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS	The swapchain has successfully present what was given to it.
			*/
			virtual GReturn EndFrame(const bool& _vSync) = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GVULKANSURFACE_H