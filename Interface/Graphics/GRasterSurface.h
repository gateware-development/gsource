#ifndef GRASTERSURFACE_H
#define GRASTERSURFACE_H

/*!
	File: GRasterSurface.h
	Purpose: A Gateware interface that allows efficiently drawing a raster to a window.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Artemis Kelsie Frost
	Contributors: Ryan Powser, Ozzie Mercado, Jordan Teasdale
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../System/GWindow.h"

namespace GW
{
	namespace I
	{
		class GRasterSurfaceInterface : public virtual GEventResponderInterface
		{
		public:
			virtual GReturn Clear(unsigned int _xrgbColor) = 0;
			virtual GReturn UpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels) = 0;
			virtual GReturn UpdateSurfaceSubset(const unsigned int* _xrgbPixels, unsigned short _numRows, unsigned short _rowWidth, unsigned short _rowStride, int _destX, int _destY) = 0;
			virtual GReturn SmartUpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels, unsigned short _rowWidth, unsigned int _drawOptionFlags) = 0;
			virtual GReturn LockUpdateBufferWrite(unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) = 0;
			virtual GReturn LockUpdateBufferRead(const unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) = 0;
			virtual GReturn UnlockUpdateBufferWrite() = 0;
			virtual GReturn UnlockUpdateBufferRead() = 0;
			virtual GReturn Present() = 0;
		};
	};
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Graphics/GRasterSurface/GRasterSurface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware Graphics library interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that allows efficiently drawing a raster to a window.
		class GRasterSurface final
			: public I::GProxy<I::GRasterSurfaceInterface, I::GRasterSurfaceImplementation, GW::SYSTEM::GWindow>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GRasterSurface)
			GATEWARE_FUNCTION(Clear)
			GATEWARE_FUNCTION(UpdateSurface)
			GATEWARE_FUNCTION(UpdateSurfaceSubset)
			GATEWARE_FUNCTION(SmartUpdateSurface)
			GATEWARE_FUNCTION(LockUpdateBufferWrite)
			GATEWARE_FUNCTION(LockUpdateBufferRead)
			GATEWARE_FUNCTION(UnlockUpdateBufferWrite)
			GATEWARE_FUNCTION(UnlockUpdateBufferRead)
			GATEWARE_FUNCTION(Present)

			// reimplemented functions
			// from GEventResponderInterface
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates and initializes a GRasterSurface.
			/*!
			*	Creates a GRasterSurface proxy which enables you to draw pixel data to a window.
			*
			*	\param [in] _gWindow						An existing GWindow object to bind the surface to.
			*
			*	\retval GReturn::INVALID_ARGUMENT			An invalid GWindow was passed.
			*	\retval GReturn::SUCCESS					GRasterSurface created successfully, ready to draw to window.
			*/
			GReturn Create(GW::SYSTEM::GWindow _gWindow);

			//! Clears the surface to the specified color.
			/*!
			*	\param [in] _xrgbColor						The color to clear the surface to.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::SUCCESS					The surface was cleared.
			*/
			virtual GReturn Clear(unsigned int _xrgbColor) = 0;

			//! Draws a raster to the surface.
			/*!
			*	\param [in] _xrgbPixels						The raster to draw to the surface.
			*	\param [in] _numPixels						The number of pixels in the raster.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::INVALID_ARGUMENT			Raster was nullptr and/or pixel count was greater than surface pixel count.
			*	\retval GReturn::SUCCESS					Pixel data was drawn to the surface.
			*/
			virtual GReturn UpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels) = 0;

			//! Draws raster data to a rectangular subset region of the surface.
			/*!
			*	Notes:
			*		To draw a subset region from source data, pass &sourcePixels[sourceX + (sourceY * sourceWidth)] as _xrgbPixels and sourceWidth as _rowStride.
			*
			*	\param [in] _xrgbPixels						The start of the raster data to draw to the surface.
			*	\param [in] _numRows						The number of rows in the subset region.
			*	\param [in] _rowWidth						The width of each row in the subset region.
			*	\param [in] _rowStride						The pixel stride length between rows in the subset region. (Pass 0 to use _rowWidth as stride.)
			*	\param [in] _destX							The x coordinate in the surface to draw to.
			*	\param [in] _destY							The y coordinate in the surface to draw to.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::INVALID_ARGUMENT			Raster was nullptr, row count was < 1, row width was < 1, and/or row stride was less than row width.
			*	\retval GReturn::SUCCESS					Pixel data was drawn to the surface.
			*	\retval GReturn::REDUNDANT					The region to draw was entirely outside the surface's bounds, so drawing was unnecessary.
			*/
			virtual GReturn UpdateSurfaceSubset(const unsigned int* _xrgbPixels, unsigned short _numRows, unsigned short _rowWidth, unsigned short _rowStride, unsigned short _destX, unsigned short _destY) = 0;

			//! Processes pixel data from a raster and draws it to the surface in the manner specified by draw option bit-flags.
			/*!
			*	\param [in] _xrgbPixels						The raster to draw to the surface.
			*	\param [in] _numPixels						The number of pixels in the raster.
			*	\param [in] _rowWidth						The width of each row in the raster.
			*	\param [in] _drawOptionFlags				A bit flag set specifying how to process the pixel data before drawing.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::INVALID_ARGUMENT			Raster was nullptr, pixel count was greater than surface pixel count, raster width was greater than surface width, and/or conflicting bit flags were passed.
			*	\retval GReturn::SUCCESS					Pixel data was drawn to the surface.
			*/
			virtual GReturn SmartUpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels, unsigned short _rowWidth, unsigned int _drawOptionFlags) = 0;

			//! Locks drawing to surface and gives access to internal memory for writing.
			/*!
			*	Blocks reading and writing access until unlocked.
			*
			*	\param [out] _outMemoryBuffer				A pointer to the internal memory array.
			*	\param [out] _outWidth						The width of the internal memory array.
			*	\param [out] _outHeight						The height of the internal memory array.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::INVALID_ARGUMENT			Memory buffer pointer was nullptr.
			*	\retval GReturn::FAILURE					A write lock was already active.
			*	\retval GReturn::SUCCESS					Access was given to internal memory and reading/writing was blocked.
			*/
			virtual GReturn LockUpdateBufferWrite(unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) = 0;

			//! Locks drawing to surface and gives access to internal memory for reading.
			/*!
			*	Blocks reading and writing access until unlocked.
			*
			*	\param [out] _outMemoryBuffer				A pointer to the internal memory array.
			*	\param [out] _outWidth						The width of the internal memory array.
			*	\param [out] _outHeight						The height of the internal memory array.
			*
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::INVALID_ARGUMENT			Memory buffer pointer was nullptr.
			*	\retval GReturn::FAILURE					A read lock was already active.
			*	\retval GReturn::SUCCESS					Access was given to internal memory and reading/writing was blocked.
			*/
			virtual GReturn LockUpdateBufferRead(const unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) = 0;

			//! Releases a write lock and re-allows drawing to surface.
			/*!
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::SUCCESS					Reading/writing was unblocked.
			*	\retval GReturn::REDUNDANT					No write locks were active.
			*/
			virtual GReturn UnlockUpdateBufferWrite() = 0;

			//! Releases a read lock and re-allows drawing to surface.
			/*!
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::UNEXPECTED_RESULT			Internal API failure. This should not happen, please report this.
			*	\retval GReturn::SUCCESS					Reading/writing was unblocked.
			*	\retval GReturn::REDUNDANT					No read locks were active.
			*/
			virtual GReturn UnlockUpdateBufferRead() = 0;

			//! Draws the surface to the window.
			/*!
			*	\retval GReturn::PREMATURE_DEALLOCATION		The associated GWindow was deallocated and the surface is now invalid.
			*	\retval GReturn::FAILURE					The surface failed to draw to the window.
			*	\retval GReturn::SUCCESS					The surface was drawn to the window.
			*/
			virtual GReturn Present() = 0;
#endif // DOXYGEN_ONLY
		};
	};
};

#endif // GRASTERSURFACE_H