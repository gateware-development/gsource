#ifndef GMUSIC_H
#define GMUSIC_H

/*!
    File: GMusic.h
    Purpose: A Gateware internal interface that handles music.
	Dependencies: GAudio, GFile, AUDIO, SYSTEM
    Author: TJay Cargle, Alexandr Kozyrev (new architecture), Ryan Powser (GFile I/O)
    Contributors: Jacob Pendleton, Chase Richards, Alexander Cusaac
    Interface Status: Beta
    Asynchronous: Yes
    Copyright: 7thGate Software LLC.
    License: MIT
*/

#include "../Core/GEventReceiver.h"
#include "GAudio.h"
#include "../System/GConcurrent.h"
#include "../System/GFile.h"

namespace GW
{
    namespace I
    {
        class GMusicInterface : public virtual GInterfaceInterface
        {
        public:
			virtual GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) = 0;
			virtual GReturn SetVolume(float _newVolume) = 0;
			virtual GReturn Play(bool _loop = false) = 0;
			virtual GReturn Pause() = 0;
			virtual GReturn Resume() = 0;
			virtual GReturn Stop() = 0;
			virtual GReturn GetSourceChannels(unsigned int& returnedChannelNum) const = 0;
			virtual GReturn GetOutputChannels(unsigned int& returnedChannelNum) const = 0;
			virtual GReturn isPlaying(bool& _returnedBool) const = 0;
            //virtual GReturn SetPCMShader(const char* _data) = 0; // not implemented yet
        };
    }
}

#include "../../Source/Audio/GAudio/GMusic.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware audio libraries must belong.
    namespace AUDIO
    {
		//! A Gateware internal interface that handles music
        class GMusic final 
			: public I::GProxy<I::GMusicInterface,  I::GMusicImplementation, const char*, GAudio, float>
        {
			// End users please feel free to ignore this struct, it is temporary and only used for internal API wiring.
			struct init_callback
			{
				init_callback()
				{
					internal_gw::event_receiver_callback = internal_gw::event_receiver_logic<CORE::GEventReceiver>;
				}
			}init; // hopefully your compiler will optimize this out

            // All Gateware API interfaces contain no variables & are pure virtual.
        public:
			//! \cond
			GATEWARE_PROXY_CLASS(GMusic)
			GATEWARE_FUNCTION(SetChannelVolumes)
			GATEWARE_FUNCTION(SetVolume)
			GATEWARE_FUNCTION(Play)
			GATEWARE_FUNCTION(Pause)
			GATEWARE_FUNCTION(Resume)
			GATEWARE_FUNCTION(Stop)
			GATEWARE_CONST_FUNCTION(GetSourceChannels)
			GATEWARE_CONST_FUNCTION(GetOutputChannels)
			GATEWARE_CONST_FUNCTION(isPlaying)
			//! \endcond

            // This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Fills out a GMusic with data from provided .wav file.
			/*!
			*	Creates a GMusic to return, attempts to initialize internal variables, and loads header information from .wav file for setup.
			*   Supports 16/32 bit uncompressed CD-quality .WAV format.
			*
			*   \param [in] _path A char array to the .wav path.
			*   \param [in] _audio A pointer to the GAudio handle.
			*	\param [in] _volume (OPTIONAL) parameter to set music volume (set to 1 by default).
			*
			*   \retval INVALID_ARGUMENT _path is a nullptr, _audio is an EMPTY_PROXY, or _volume is outside of 0.0f to 1.0f range.
			*   \retval FAILURE An error occurred during loading of header information, internal objects could not be created (On Windows these are the source voice and sub-mix voice, on other platforms this is all in GSound's init), or failed to initialize internal variables. See Below for Platform
			*	- On Linux
			*		+ pa_main_loop or pa_context could not be created, pa_context could not be connected, pa_channel_map could not be created, pa_stream could not be created, or pa_stream could not be connected.
			*	- On Mac
			*		+ AvAudioPlayerNode could not be created or AvAudioPCMBuffers could not be initialized
			*	\retval SUCCESS None of the above errors occurred.
			*/
			GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, float _volume = 1.0f);

			//! Attempts to set the output volume for the specified number of outputs to the passed in values.
			/*!
			* The amount of values in _values that will be used is based on _numChannels.
			* If you attempt to set the volume of an output your hardware does not support, it will be ignored.
			* Automatically applies all the master volume ratios.
			*
			* Channels: Supports up to 6 channels AKA 5.1 Audio:
			*
			*		INDEX[0] = Left,
			*		INDEX[1] = Right,
			*		INDEX[2] = Front Center,
			*		INDEX[3] = LFE,
			*		INDEX[4] = Rear Left,
			*		INDEX[5] = Rear Right,
			*
			*	\param [in] _values The output volumes to be set.
			*	\param [in] _numChannels The number of channels affected.
			*
			*	\retval FAILURE _numChannels is less than 1, internal object encountered an error which could occur if GAudio's number of outputs changed during runtime, _values is NULL, or _values tried to read garbage data - (_numChannels > amount of values in _values)
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) = 0;

			//! Attempts to change the overall volume.
			/*!
			* Automatically applies all the master volume ratios.
			*
			*	\param [in] _newVolume The output volume to be set.
			*
			*	\retval FAILURE GAudio has not filled out this object with GAudio's function: "CreateMusicStream", _newVolume is less than Zero, or internal object encountered an error. Unlikely to occur if GAudio's "CreateMusicStream" did not return an error upon filling out this object.
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn SetVolume(float _newVolume) = 0;

			//! Attempts to start a music stream.
			/*!
			* Currently only supports .wav files.
			* Upon Succession will launch a new thread that will continuously read in audio data from file.
			*
			*	\retval FAILURE GAudio has not filled out this object with GAudio's function: "CreateMusicStream", specified .wav file could not be found or encountered an error upon reading in data (This happens when "CreateMusicStream" fills out data), or internal object encountered an error. Unlikely to occur if GAudio's "CreateMusicStream" did not return an error upon filling out this object.
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn Play(bool _loop = false) = 0;

			//! Attempts to pause a currently playing music stream.
			/*!
			* Currently only supports .wav files.
			* If a stream is currently paused, will not attempt to pause playback without returning an error.
			*
			*	\param [in] _loop (OPTIONAL) Bool for whether or not should play looping.
			*
			*	\retval FAILURE GAudio has not filled out this object with GAudio's function: "CreateMusicStream", specified .wav file could not be found or encountered an error upon reading in data (This happens when "CreateMusicStream" fills out data), or internal object encountered an error. Unlikely to occur if GAudio's "CreateMusicStream" did not return an error upon filling out this object.
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn Pause() = 0;

			//! Attempts to resume a currently paused music stream.
			/*!
			* Currently only supports .wav files.
			* If a music stream is currently playing, will not attempt to play file without returning an error.
			*
			*	\retval FAILURE If internal object encounters an error.
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn Resume() = 0;

			//! Attempts to stop playback of a music stream and reset it to beginning for future use.
			/*!
			* Currently only supports .wav files.
			* Tells the previously launched thread to stop, calls join on it, and resets thread for future use.
			* Will attempt to completely stop playback regardless of whether or not music is playing or paused.
			*
			*	\retval FAILURE If internal object encounters an error.
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn Stop() = 0;

			//! Stores the .wav files internal amount of channels the sound was recorded with into the passed in unsigned int.
			/*!
			* This value is read and stored upon creation of GMusic.
			*
			*	\param [out] _outChannelNum The unsigned int which will be stored with the value.
			*
			*	\retval FAILURE GAudio has not filled out this object with GAudio's function: "CreateMusicStream".
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn GetSourceChannels(unsigned int& _outChannelNum) const = 0;

			//! Stores the number of specified outputs into the passed in unsigned int.
			/*!
			* This value is obtained from G_NUM_OF_OUTPUTS macro.
			* By default the number of output channels is 6 unless GAudio was created with a different amount (Currently Linux set to 2).
			*
			*	\param [out] _outChannelNum The unsigned int which will be stored with the value.
			*
			*	\retval FAILURE GAudio has not filled out this object with GAudio's function: "CreateMusicStream".
			*	\retval SUCCESS Successfully ran without running into any of the above issues.
			*/
			virtual GReturn GetOutputChannels(unsigned int& _outChannelNum) const = 0;

			//! Fills out the passed in bool with information as to if the music stream is playing or not.
			/*!
			* GMusic internally keeps track of whether or not it playing via the launched thread, as well as the Start/Pause/Resume/Stop functions.
			*
			*	\param [out] _outBool The bool which will be stored with the value.
			*
			*	\retval SUCCESS Always, boolean passed in will tell you if music is playing or not.
			*/
			virtual GReturn isPlaying(bool& _outBool) const = 0;
#endif // DOXYGEN_ONLY
        };
    }
}

#endif // GMUSIC_H
