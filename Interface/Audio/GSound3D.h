#ifndef GSOUND3D_H
#define GSOUND3D_H

/*!
	File: GSound3D.h
	Purpose: A Gateware interface that handles spacial sounds.
	Dependencies: GSound
	Author: Jacob Pendleton
	Contributors: Alexandr Kozyrev (new arch), Ryan Powser, Chase Richards, Alexander Cusaac
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GSound.h"
#include "GAudio3D.h"
#include "GAudioDefines.h"

namespace GW
{
	namespace I
	{
		class GSound3DInterface : public virtual GSoundInterface
		{
		public:
			virtual GReturn UpdatePosition(GW::MATH::GVECTORF _position) = 0;
			virtual GReturn UpdateAttenuation(float _minRadius, float _maxRadius, GW::AUDIO::GATTENUATION _attenuation) = 0;
		};
	}
}

#include "../../Source/Audio/GAudio3D/GSound3D.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware audio libraries must belong.
	namespace AUDIO
	{
		//! A Gateware interface that handles spacial sounds
		class GSound3D final 
			: public I::GProxy<I::GSound3DInterface, I::GSound3DImplementation, const char*, float, float, GW::AUDIO::GATTENUATION, GW::AUDIO::GAudio3D>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GSound3D);
			GATEWARE_FUNCTION(UpdatePosition);
			GATEWARE_FUNCTION(UpdateAttenuation);

			// reimplemented 
			GATEWARE_FUNCTION(SetChannelVolumes);
			GATEWARE_FUNCTION(SetVolume);
			GATEWARE_FUNCTION(Play);
			GATEWARE_FUNCTION(Pause);
			GATEWARE_FUNCTION(Resume);
			GATEWARE_FUNCTION(Stop);
			GATEWARE_CONST_FUNCTION(GetSourceChannels);
			GATEWARE_CONST_FUNCTION(GetOutputChannels);
			GATEWARE_CONST_FUNCTION(isPlaying);
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a handle to GSound3D an internally fills out a GSound with data from provided .wav file.
			/*!
			*	Creates a GSound to return, attempts to initialize internal variables, and loads audio fully into memory.
			*   Supports 16/32 bit uncompressed CD-quality .WAV format.
			*	The sample has to be STEREO or better for surround to work (2+ channels). A mono sample can be easily transformed to Stereo by duplicating the track in L and R channels.
			*
			*   \param [in] _path A char array to the .wav path.
			*   \param [in] _minRadius The radius at which the sound is at max volume.
			*   \param [in] _maxRadius The radius at which the sound is silent.
			*   \param [in] _attenuation The type of curve between _minRadius and _maxRadius.
			*   \param [in] _audio3D A pointer to the GAudio3D handle.
			*	\param [in] _volume (OPTIONAL) parameter to set sound volume (set to 1 by default).
			*
			*
			*   \retval INVALID_ARGUMENT _path is a nullptr, _audio3D is an EMPTY_PROXY, or _volume is outside of 0.0f to 1.0f range.
			*   \retval FAILURE Failed to create base GMusic instance, or failed to initialize internal variables. See Below for Platform
			*	- On Linux
			*		+ pa_main_loop or pa_context could not be created, pa_context could not be connected, pa_channel_map could not be created, pa_stream could not be created, or pa_stream could not be connected.
			*	- On Mac
			*		+ AvAudioPlayerNode could not be created or could not read in file.
			*	\retval SUCCESS Successfully created GSound3D instance and proxy.
			*/
			GReturn Create(const char* _path, float _minRadius, float _maxRadius, GW::AUDIO::GATTENUATION _attenuation, GW::AUDIO::GAudio3D _audio3D, float _volume = 1.0f);

			//! Updates the position of the sound source
			/*!
			*	\param [in] _position The position of the music as a GVECTORF.
			*
			*	\retval FAILURE Failed to spatialize/set correct volume.
			*	\retval SUCCESS Position was successfully set and volume updated.
			*/
			virtual GReturn UpdatePosition(GW::MATH::GVECTORF _position) = 0;

			//! Updates the attenuation curve of the sound between _minRadius and _maxRadius
			/*!
			*    \param [in] _minRadius The radius at which the sound is at max volume.
			*    \param [in] _maxRadius The radius at which the sound is silent.
			*    \param [in] _attenuation The type of curve between _minRadius and _maxRadius.
			*
			*    \retval INVALID_ARGUMENT _attenuation is not a defined GATTENUATION, or _minRadius is greater than _maxRadius.
			*    \retval SUCCESS		  The attenuation was successfully set and volume updated.
			*/
			virtual GReturn UpdateAttenuation(float _minRadius, float _maxRadius, GW::AUDIO::GATTENUATION _attenuation) = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GSOUND3D_H