#ifndef GCONTROLLER_H
#define GCONTROLLER_H

/*!
	File: GController.h
	Purpose: A Gateware interface handles controller input and provides disconnection and connection events
	Author: Devin Wright
	Contributors: Chris Kennedy, Kai Huang, Lari Norri, Ozzie Mercado, Ryan Powser, Alexander Cusaac
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventGenerator.h"
#include "GInputDefines.h"

namespace GW
{
	namespace I
	{
		class GControllerInterface : public virtual GEventGeneratorInterface
		{
		public:
			struct EVENT_DATA
			{
				int controllerIndex;
				GW::INPUT::GControllerType controllerID;
				int inputCode;
				float inputValue;
				int isConnected;
			};

			enum class Events
			{
				CONTROLLERBUTTONVALUECHANGED,
				CONTROLLERAXISVALUECHANGED,	
				CONTROLLERCONNECTED,		
				CONTROLLERDISCONNECTED		
			};

			enum class DeadZoneTypes
			{
				DEADZONESQUARE,
				DEADZONECIRCLE
			};

			virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) = 0;
			virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) = 0;
			virtual GReturn GetMaxIndex(int& _outMax) = 0;
			virtual GReturn GetNumConnected(int& _outConnectedCount) = 0;
			virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) = 0;
			virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) = 0;
			virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) = 0;
			virtual GReturn StopVibration(unsigned int _controllerIndex) = 0;
			virtual GReturn StopAllVibrations() = 0;
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Input/GController/GController.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The INPUT namespace to which all Gateware input interfaces must belong.
	namespace INPUT
	{
		//! A multi-threaded controller input library.
		/*!
		*	This library can be used to poll the current state of connected controllers.
		*	The controllers are assigned indices in a array for the lifetime that they are connected.
		*	It also provides events for controller connections and disconnections,
		*	which a GListener can be written to receive.
		*/
		class GController final
			: public I::GProxy<I::GControllerInterface, I::GControllerImplementation>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GController);
			GATEWARE_TYPEDEF(Events);
			GATEWARE_TYPEDEF(EVENT_DATA);
			GATEWARE_TYPEDEF(DeadZoneTypes);
			GATEWARE_FUNCTION(GetState);
			GATEWARE_FUNCTION(IsConnected);
			GATEWARE_FUNCTION(GetMaxIndex);
			GATEWARE_FUNCTION(GetNumConnected);
			GATEWARE_FUNCTION(SetDeadZone);
			GATEWARE_FUNCTION(StartVibration);
			GATEWARE_FUNCTION(IsVibrating);
			GATEWARE_FUNCTION(StopVibration);
			GATEWARE_FUNCTION(StopAllVibrations);

			// reimplemented functions
			GATEWARE_FUNCTION(Register);
			GATEWARE_CONST_FUNCTION(Observers);
			GATEWARE_FUNCTION(Push);
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a new GController object
			/*!
			*	Initializes a handle to a GController object.
			*	Created GController object will have its reference count initialized to one.
			*
			*	\retval SUCCESS GController was successfully created
			*/
			GReturn Create();

			//! GControllerEvents holds the possible events that can be sent from GController
			enum class Events
			{
				CONTROLLERBUTTONVALUECHANGED,		//!< Input event for button value change
				CONTROLLERAXISVALUECHANGED,			//!< Input event for axis value changed
				CONTROLLERCONNECTED,				//!< Connection event for a controller being connected
				CONTROLLERDISCONNECTED				//!< Connection event for a controller being disconnected
			};

			//! GControllerDeadzoneTypes holds the different type of deadzone calculations
			enum class DeadZoneTypes
			{
				DEADZONESQUARE,	//!< Calculates the deadzone of x and y separately
				DEADZONECIRCLE	//!< Calculates the deadzone of x and y as a single magnitude
			};

			/*! EVENT_DATA holds the information about a controller event. See \ref GControllerCodes for a list of inputCode defines.
			*
			*	If the corresponding event is an Input Event inputCode and inputValue will contain the input code and value
			*	respectively for the detected input. If the corresponding event is an Connection event inputCode and inputValue will be 0.
			*	isConnected will be 0 for false and 1 for true.
			*/
			struct EVENT_DATA
			{
				int controllerIndex;							/*!< The Controller index where the event took place */
				GW::INPUT::GControllerType controllerID;		/*!<  The Controller ID of the controller that sent the event */
				int inputCode;									/*!< The Code representing the Detected input */
				float inputValue;								/*!< The value of the detected input */
				int isConnected;								/*!< Value indicating whether the controller is connected */
			};

			//! Used to poll the current state of a button or axis on a controller
			/*!
			*	Use an Input code to check the state of a button or axis on the controller
			*	at the selected index. If a button is being checked the _outState will be 0 for
			*	up and 1 for down. If a axis is being checked the _outState will be between -1 and 1 inclusively.
			*
			*	\param [in] _controllerIndex The controller index to check
			*	\param [in] _inputCode The input code for the button/axis to check
			*	\param [out] _outState A reference to a float to store the state.
			*
			*	\retval GReturn::INVALID_ARGUMENT	Ether _controllerIndex is out of range or _inputCode is invalid.
			*	\retval GReturn::FAILURE			No controller is connected at the chosen index.
			*	\retval GReturn::EMPTY_PROXY		The proxy is empty
			*	\retval GReturn::SUCCESS			The button/axis state was was successfully stored in the out-param
			*/
			virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) = 0;

			//! Used to check if a controller is connected at a specified index
			/*!
			*	\param [in] _controllerIndex The controller index to check.
			*	\param [out] _outIsConnected A reference to a bool to store whether a controller is at the index.
			*
			*	\retval GReturn::INVALID_ARGUMENT	_controllerIndex was out of range.
			*	\retval GReturn::EMPTY_PROXY		The proxy is empty
			*	\retval GReturn::SUCCESS			The out-param was successfully filled out
			*/
			virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) = 0;

			//! Outputs the max index of the array of controllers.
			/*!
			*	The controllers will be stored in a constant array where the size of the array
			*	is the maximum amount of supported controllers.
			*
			*	\param [out] _outMax A reference to a int to store the max index.
			*
			*	\retval GReturn::EMPTY_PROXY	The proxy is empty
			*	\retval GReturn::SUCCESS		The max index was successfully stored in the out-param
			*/
			virtual GReturn GetMaxIndex(int& _outMax) = 0;

			//! Outputs the number of controllers connected
			/*!
			*	The number returned will be how many controllers are currently stored in the
			*	controller array.
			*
			*	\param [out] _outConnectedCount A reference to a int to store the count of connected controllers
			*
			*	\retval GReturn::EMPTY_PROXY	The proxy is empty
			*	\retval GReturn::SUCCESS		The count of connected controllers was successfully stored in the out-param
			*/
			virtual GReturn GetNumConnected(int& _outConnectedCount) = 0;

			//! Set how the stick deadzones should be calculated
			/*!
			*	Default deadzone is _type = DEADZONESQUARE, _deadzonePercentage = .2f
			*
			*	\param [in] _type Specifies the dead zone calculation to be used.
			*	\param [in] _deadzonePercentage The size of the deadzone.
			*
			*	\retval GReturn::INVALID_ARGUMENT	_deadzonePercentage is invalid
			*	\retval GReturn::EMPTY_PROXY		The proxy is empty
			*	\retval GReturn::SUCCESS			The deadzone was set to the new parameters
			*/
			virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) = 0;

			//! Start vibration in selected controller
			/*!
			*	This will not replace a currently running vibration, the previous vibration must end or be stopped first.
			*	Controllers have different start up times and strengths this method does not account for this.
			*
			*	\param [in] _controllerIndex The controller to vibrate.
			*	\param [in] _pan -1 to 1 ratio where -1 is full left motor, 1 is full right motor.
			*	\param [in] _duration In seconds how long the vibration will run.
			*	\param [in] _strength 0 to 1 ratio of how strong the vibration will be.
			*
			*	\retval GReturn::FEATURE_UNSUPPORTED	Vibration for the current controller type is not supported.
			*	\retval	GReturn::INVALID_ARGUMENT		One or more arguments are out of range.
			*	\retval GReturn::FAILURE				Controller is currently vibrating.
			*	\retval GReturn::EMPTY_PROXY			The proxy is empty
			*	\retval GReturn::SUCCESS				The vibration has been started in the selected controller.
			*/
			virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) = 0;

			//! Used to check if a controller is currently vibrating
			/*!
			*	\param [in] _controllerIndex The controller index to check.
			*	\param [out] _outIsVibrating Is a reference to a bool to store whether a controller is vibrating.
			*
			*	\retval GReturn::FEATURE_UNSUPPORTED	Vibration for the current controller type is not supported.
			*	\retval GReturn::INVALID_ARGUMENT		_controllerIndex was out of range.
			*	\retval GReturn::EMPTY_PROXY			The proxy is empty
			*	\retval GReturn::SUCCESS				The out-param was successfully filled out
			*/
			virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) = 0;

			//! Used to stop a controller from vibrating 
			/*!
			*	\param [in] _controllerIndex The controller index to stop
			*
			*	\retval GReturn::INVALID_ARGUMENT		_controllerIndex was out of range.
			*	\retval GReturn::FEATURE_UNSUPPORTED	Vibration for the current controller type is not supported.
			*	\retval GReturn::EMPTY_PROXY			The proxy is empty
			*	\retval GReturn::SUCCESS				The vibration was stop
			*	\retval	GReturn::REDUNDANT				The controller was not vibrating.
			*/
			virtual GReturn StopVibration(unsigned int _controllerIndex) = 0;

			//! Stops all currently vibrating controllers
			/*!
			*	\retval GReturn::FEATURE_UNSUPPORTED Vibration for the current controller type is not supported.
			*	\retval GReturn::EMPTY_PROXY	The proxy is empty
			*	\retval GReturn::SUCCESS All vibrations were stopped
			*/
			virtual GReturn StopAllVibrations() = 0;
#endif
		};
	}// end SYSTEM namespace
}// end GW namespace
#endif // #endif GCONTROLLER_H