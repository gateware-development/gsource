#ifndef GVECTOR_H
#define GVECTOR_H

/*!
	File: GVector.h
	Purpose: A Gateware interface that handles all vector functions.
	Asynchronous: NO
	Author: Shuo-Yi Chang
	Contributors: Chris Kennedy, Ryan Powser, Lari Norri
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMathDefines.h"

namespace GW
{
	namespace I
	{
		class GVectorInterface : public virtual GInterfaceInterface
		{
		public:
			// Floats
			static GReturn AddVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleF(MATH::GVECTORF _vector, float _scalar, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn DotF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossVector2F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossVector3F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrixF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TransformF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MagnitudeF(MATH::GVECTORF _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn NormalizeF(MATH::GVECTORF _vector, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float _ratio, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SplineF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF _vector3, MATH::GVECTORF _vector4, float _ratio, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade(MATH::GVECTORF _vectorF, MATH::GVECTORD& _outVectorD) { return GReturn::NO_IMPLEMENTATION; }
			// Doubles
			static GReturn AddVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleD(MATH::GVECTORD _vector, double _scalar, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn DotD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossVector2D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossVector3D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrixD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TransformD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MagnitudeD(MATH::GVECTORD _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn NormalizeD(MATH::GVECTORD _vector, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double _ratio, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SplineD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD _vector3, MATH::GVECTORD _vector4, double _ratio, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade(MATH::GVECTORD _vectorD, MATH::GVECTORF& _outVectorF) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math/GVector/GVector.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware math interfaces must belong.
	namespace MATH
	{
		//!  A Gateware interface that handles all vector functions.
		class GVector final
			: public I::GProxy<I::GVectorInterface, I::GVectorImplementation>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GVector)
			GATEWARE_STATIC_FUNCTION(AddVectorF)
			GATEWARE_STATIC_FUNCTION(SubtractVectorF)
			GATEWARE_STATIC_FUNCTION(ScaleF)
			GATEWARE_STATIC_FUNCTION(DotF)
			GATEWARE_STATIC_FUNCTION(CrossVector2F)
			GATEWARE_STATIC_FUNCTION(CrossVector3F)
			GATEWARE_STATIC_FUNCTION(VectorXMatrixF)
			GATEWARE_STATIC_FUNCTION(TransformF)
			GATEWARE_STATIC_FUNCTION(MagnitudeF)
			GATEWARE_STATIC_FUNCTION(NormalizeF)
			GATEWARE_STATIC_FUNCTION(LerpF)
			GATEWARE_STATIC_FUNCTION(SplineF)
			GATEWARE_STATIC_FUNCTION(Upgrade)

			// doubles
			GATEWARE_STATIC_FUNCTION(AddVectorD)
			GATEWARE_STATIC_FUNCTION(SubtractVectorD)
			GATEWARE_STATIC_FUNCTION(ScaleD)
			GATEWARE_STATIC_FUNCTION(DotD)
			GATEWARE_STATIC_FUNCTION(CrossVector2D)
			GATEWARE_STATIC_FUNCTION(CrossVector3D)
			GATEWARE_STATIC_FUNCTION(VectorXMatrixD)
			GATEWARE_STATIC_FUNCTION(TransformD)
			GATEWARE_STATIC_FUNCTION(MagnitudeD)
			GATEWARE_STATIC_FUNCTION(NormalizeD)
			GATEWARE_STATIC_FUNCTION(LerpD)
			GATEWARE_STATIC_FUNCTION(SplineD)
			GATEWARE_STATIC_FUNCTION(Downgrade)
			//! \endcond
				
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GVector Object. 
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Add two vectors
			/*!
			*	Adds the two specified vectors and stores the result in the output vector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AddVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two vectors
			/*!
			*	Subtracts the two specified vectors and stores the result in the output vector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the subtraction 
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn SubtractVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the vector
			/*!
			*	Scales all elements of the input vector by the specified value
			*	and stores the result in the output vector.
			*
			*	\param [in]  _vector		The first vector
			*	\param [in]  _scalar		The specified value to scale
			*	\param [out] _outVector		The result of the scaling 
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleF(MATH::GVECTORF _vector, float _scalar, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of the two vectors
			/*!
			*	Calculates the dot product of two specified vectors and stores the result in the output value.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn DotF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the Cross product of the two vectors, which are treated as 2D vectors 
			/*!
			*	Calculates the cross product of two specified vectors, which are treated as 2D vectors (The input vectors' z and w value will be ignored).
			*	and stores the result in the output value. 
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the 2D cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn CrossVector2F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of the two given vectors, which are treated as 3D vectors (the input vectors' w value will be ignored). 
			/*!
			*	Calculates the cross product of two specified vectors, which are treated as 3D vectors
			*	and stores the result in the output vector. The output w will be the result of multiplying the w components of the inputs. 
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the cross product, the output w will be the result of multiplying the w components of the inputs. 
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn CrossVector3F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiplies the specified vector by the specified matrix.
			/*!
			*	Multiplies the specified vector by the specified matrix
			*	and stores the result in the output vector. 
			*
			*	\param [in]  _vector		The input vector
			*	\param [in]  _matrix		The input matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrixF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform specified specified vector by specified matrix.
			/*!
			*	Transforms the specified vector by the specified matrix, treating the fourth row of the given matrix as (0, 0, 0, 1).
			*	The output vector's w value will always be whatever w was passed in. 
			*
			*	\param [in]  _vector		The specified vector
			*	\param [in]  _matrix		The transform matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TransformF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the magnitude of the specified vector.
			/*!
			*	Computes the magnitude of the specified vector,	and stores the result in the output value.
			*
			*	\param [in]  _vector			The specified vector
			*	\param [out] _outMagnitude		The magnitude of the vector
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MagnitudeF(MATH::GVECTORF _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalizes the specified vector.
			/*!
			*	Normalizes the specified vector, and stores the result in the output vector.
			*
			*	\param [in]  _vector		The specified vector
			*	\param [out] _outVector		The result of the normalization.
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn NormalizeF(MATH::GVECTORF _vector, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linearly interpolates between two specified vectors
			/*!
			*	Linearly interpolates between two specified vectors
			*	and stores the result in the output vector.
			*
			*	\param [in]  _vector1			The start vector
			*	\param [in]  _vector2			The end vector
			*	\param [in]  _ratio				The interpolation amount
			*	\param [out] _outVector			The result of the lerp
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn LerpF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float _ratio, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the interpolation on a curve which connects two specified 3D vectors
			/*!
			*	Calculate the interpolation on a curve which connects two specified 3D vectors
			*	and stores the result in the output quaternion. The component of w
			*	will return 0. The interpolation will happen between the second point
			*	and third point.
			*
			*	\param [in]  _vector1			The first control point
			*	\param [in]  _vector2			The second control point
			*	\param [in]  _vector3			The third control point
			*	\param [in]  _vector4			The fourth control point
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outVector			The result of the spline
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SplineF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF _vector3, MATH::GVECTORF _vector4, float _ratio, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Promotes float vector to double vector
			/*!
			*	Performs a static_cast<double>() on every element of the input vector and assigns them to the output vector
			*
			*	\param [in]  _vectorF		A float vector
			*	\param [out] _outVectorD	The input float vector static_casted to a double vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Upgrade(MATH::GVECTORF _vectorF, MATH::GVECTORD& _outVectorD) { return GReturn::NO_IMPLEMENTATION; }













			//! Add two vectors
			/*!
			*	Adds the two specified vectors and stores the result in the output vector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AddVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two vectors
			/*!
			*	Subtracts the two specified vectors and stores the result in the output vector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the subtraction 
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn SubtractVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the vector
			/*!
			*	Scales all elements of the input vector by the specified value
			*	and stores the result in the output vector.
			*
			*	\param [in]  _vector		The first vector
			*	\param [in]  _scalar		The specified value to scale
			*	\param [out] _outVector		The result of the scaling 
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleD(MATH::GVECTORD _vector, double _scalar, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of the two vectors
			/*!
			*	Calculates the dot product of two specified vectors and stores the result in the output Value.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn DotD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the Cross product of the two vectors, which are treated as 2D vectors 
			/*!
			*	Calculates the cross product of two specified vectors, which are treated as 2D vectors (The input vectors' z and w value will be ignored.)
			*	and stores the result in the output value.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the 2D cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn CrossVector2D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of the two given vectors, which are treated as 3D vectors (the input vectors' w value will be ignored). 
			/*!
			*	Calculates the cross product of two specified vectors, which are treated as 3D vectors
			*	and stores the result in the output vector. The output w will be the result of multiplying the w components of the inputs.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the cross product, the output w will be the result of multiplying the w components of the inputs.
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn CrossVector3D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiplies the specified vector by the specified matrix.
			/*!
			*	Multiplies the specified vector by the specified matrix
			*	and stores the result in the output vector. The output w will be the result of multiplying the t
			*
			*	\param [in]  _vector		The input vector
			*	\param [in]  _matrix		The input matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrixD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform specified specified vector by specified matrix.
			/*!
			*	Transforms the specified vector by the specified matrix, treating the fourth row of the given matrix as (0, 0, 0, 1).
			*	The output vector's w value will always be whatever w was passed in.
			*
			*	\param [in]  _vector		The specified vector
			*	\param [in]  _matrix		The transform matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TransformD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the magnitude of the specified vector.
			/*!
			*	Computes the magnitude of the specified vector,	and stores the result in the output value.
			*
			*	\param [in]  _vector			The specified vector
			*	\param [out] _outMagnitude		The magnitude of the vector
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MagnitudeD(MATH::GVECTORD _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalizes the specified vector.
			/*!
			*	Normalizes the specified vector, and stores the result in the output vector.
			*
			*	\param [in]  _vector		The specified vector
			*	\param [out] _outVector		The result of the normalization.
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn NormalizeD(MATH::GVECTORD _vector, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linearly interpolates between two specified vectors
			/*!
			*	Linearly interpolates between two specified vectors
			*	and stores the result in the output vector.
			*
			*	\param [in]  _vector1			The start vector
			*	\param [in]  _vector2			The end vector
			*	\param [in]  _ratio				The interpolation amount 
			*	\param [out] _outVector			The result of the lerp
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn LerpD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double _ratio, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the interpolation between two specified 3D vectors
			/*!
			*	Calculate the interpolation between two specified 3D vectors
			*	and stores the result in the output quaternion. The component of w
			*	will return 0. The interpolation will happen between the second point
			*	and third point.
			*
			*	\param [in]  _vector1			The first control point
			*	\param [in]  _vector2			The second control point
			*	\param [in]  _vector3			The third control point
			*	\param [in]  _vector4			The fourth control point
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outVector			The result of the spline
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SplineD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD _vector3, MATH::GVECTORD _vector4, double _ratio, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Demotes double vector to float vector
			/*!
			*	Performs a static_cast<float>() on every element of the input vector and assigns them to the output vector
			*
			*	\param [in]  _vectorD		A double vector
			*	\param [out] _outVectorF	The input double vector static_casted to a float vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Downgrade(MATH::GVECTORD _vectorD, MATH::GVECTORF& _outVectorF) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GVECTOR_H
