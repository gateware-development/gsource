#ifndef GQUATERNION_H
#define GQUATERNION_H

/*!
	File: GQuaternion.h
	Purpose: A Gateware interface that handles quaternion functions.
	Asynchronous: YES
	Author: Shuo-Yi Chang
	Contributors: Kai Huang, Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMathDefines.h"

namespace GW
{
	namespace I
	{
		class GQuaternionInterface : public virtual GInterfaceInterface
		{
		public:
			// Floats
			static GReturn AddQuaternionF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractQuaternionF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyQuaternionF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleF( MATH::GQUATERNIONF _quaternion, float _scalar, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SetByVectorAngleF( MATH::GVECTORF _vector, float _radian, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SetByMatrixF( MATH::GMATRIXF _matrix, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn DotF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float& _outValue ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GVECTORF& _outVector ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConjugateF( MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn InverseF( MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MagnitudeF( MATH::GQUATERNIONF _quaternion, float& _outMagnitude ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn NormalizeF( MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IdentityF( MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SlerpF( MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade(MATH::GQUATERNIONF _quaternionF, MATH::GQUATERNIOND& _outQuaternionD) { return GReturn::NO_IMPLEMENTATION; }
			// Doubles
			static GReturn AddQuaternionD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractQuaternionD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyQuaternionD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleD( MATH::GQUATERNIOND _quaternion, double _scalar, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SetByVectorAngleD( MATH::GVECTORD _vector, double _radian, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SetByMatrixD( MATH::GMATRIXD _matrix, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn DotD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double& _outValue ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn CrossD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GVECTORD& _outVector ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConjugateD( MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn InverseD( MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MagnitudeD( MATH::GQUATERNIOND _quaternion, double& _outMagnitude ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn NormalizeD( MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IdentityD( MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SlerpD( MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion ) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade(MATH::GQUATERNIOND _quaternionD, MATH::GQUATERNIONF& _outQuaternionF) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math/GQuaternion/GQuaternion.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware math interfaces must belong.
	namespace MATH
	{
		//! A Gateware interface that handles quaternion functions.
		class GQuaternion final
			: public I::GProxy<I::GQuaternionInterface, I::GQuaternionImplementation >
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS( GQuaternion )
			GATEWARE_STATIC_FUNCTION(AddQuaternionF)
			GATEWARE_STATIC_FUNCTION(SubtractQuaternionF)
			GATEWARE_STATIC_FUNCTION(MultiplyQuaternionF)
			GATEWARE_STATIC_FUNCTION(ScaleF)
			GATEWARE_STATIC_FUNCTION(SetByVectorAngleF)
			GATEWARE_STATIC_FUNCTION(SetByMatrixF)
			GATEWARE_STATIC_FUNCTION(DotF)
			GATEWARE_STATIC_FUNCTION(CrossF)
			GATEWARE_STATIC_FUNCTION(ConjugateF)
			GATEWARE_STATIC_FUNCTION(InverseF)
			GATEWARE_STATIC_FUNCTION(MagnitudeF)
			GATEWARE_STATIC_FUNCTION(NormalizeF)
			GATEWARE_STATIC_FUNCTION(IdentityF)
			GATEWARE_STATIC_FUNCTION(LerpF)
			GATEWARE_STATIC_FUNCTION(SlerpF)
			GATEWARE_STATIC_FUNCTION(Upgrade)


			// Double Quaternion Methods
			GATEWARE_STATIC_FUNCTION(AddQuaternionD)
			GATEWARE_STATIC_FUNCTION(SubtractQuaternionD)
			GATEWARE_STATIC_FUNCTION(MultiplyQuaternionD)
			GATEWARE_STATIC_FUNCTION(ScaleD)
			GATEWARE_STATIC_FUNCTION(SetByVectorAngleD)
			GATEWARE_STATIC_FUNCTION(SetByMatrixD)
			GATEWARE_STATIC_FUNCTION(DotD)
			GATEWARE_STATIC_FUNCTION(CrossD)
			GATEWARE_STATIC_FUNCTION(ConjugateD)
			GATEWARE_STATIC_FUNCTION(InverseD)
			GATEWARE_STATIC_FUNCTION(MagnitudeD)
			GATEWARE_STATIC_FUNCTION(NormalizeD)
			GATEWARE_STATIC_FUNCTION(IdentityD)
			GATEWARE_STATIC_FUNCTION(LerpD)
			GATEWARE_STATIC_FUNCTION(SlerpD)
			GATEWARE_STATIC_FUNCTION(Downgrade)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GQuaternion Object. 
			/*!
			*
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory or something terribly)
			*/
			GReturn Create( );

			//! Add two quaternions
			/*!
			*	Adds the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the addition
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn AddQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two quaternions
			/*!
			*	Subtracts the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SubtractQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two quaternions
			/*!
			*	Multiplies the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn MultiplyQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the quaternion
			/*!
			*	Scales the specified quaternion with a number and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [in]  _scalar			The specified value to scale
			*	\param [out] _outQuaternion		The result of the scaling
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn ScaleF(MATH::GQUATERNIONF _quaternion, float _scalar, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Set the quaternion by the specified vector and the specified angle
			/*!
			*	Sets the quaternion with a number and stores the result in the output quaternion.
			*
			*	\param [in]  _vector			The specified vector
			*	\param [in]  _radian			The specified value of angle
			*	\param [out] _outQuaternion		The result of the rotation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SetByVectorAngleF(MATH::GVECTORF _vector, float _radian, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Set the quaternion by the specified matrix
			/*!
			*	Sets the quaternion by the rotational part of the specified matrix
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _matrix			The specified matrix
			*	\param [out] _outQuaternion		The result of the rotation of matrix
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SetByMatrixF(MATH::GMATRIXF _matrix, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of the two specified quaternions
			/*!
			*	Calculates the dot product of two specified quaternions and stores the result in the output Value.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outValue			The value of the dot product
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn DotF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of the two specified quaternions
			/*!
			*	Crosses two specified quaternions and stores the result in the output Value.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outVector			The vector of the cross product
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn CrossF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Conjugate the specified quaternion
			/*!
			*	Conjugates the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outQuaternion		The result of the conjugate
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn ConjugateF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Inverse the specified quaternion
			/*!
			*	Inverses the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The specified quaternion
			*	\param [out] _outQuaternion		The result of the inverse
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn InverseF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of quaternion
			/*!
			*	Calculate the magnitude of the specified quaternion and stores the result in the output value.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outMagnitude		The result of the Calculation
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn MagnitudeF(MATH::GQUATERNIONF _quaternion, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize the specified quaternion
			/*!
			*	Normalizes the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outQuaternion		The result of the normalization
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn NormalizeF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Identity the specified quaternion
			/*!
			*	Set the output quaternion as an identity quaternion
			*
			*	\param [out] _outQuaternion		The result of the identity
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn IdentityF(MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolate between two specified quaternions
			/*!
			*	Linear interpolates between two specified quaternions
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The seconds quaternion
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outQuaternion		The result of the lerp
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn LerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Spherical linear interpolates between two specified quaternions
			/*!
			*	Spherical linear interpolates between two specified quaternions
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The seconds quaternion
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outQuaternion		The result of the lerp
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SlerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Promotes float quaternion to double quaternion
			/*!
			*	Performs a static_cast<double>() on every element of the input quaternion and assigns them to the output quaternion
			*
			*	\param [in]  _quaternionF		A float quaternion
			*	\param [out] _outQuaternionD	The input float quaternion static_casted to a double quaternion
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Upgrade(MATH::GQUATERNIONF _quaternionF, MATH::GQUATERNIOND& _outQuaternionD) { return GReturn::NO_IMPLEMENTATION; }












			//! Add two quaternions
			/*!
			*	Adds the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the addition
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn AddQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two quaternions
			/*!
			*	Subtracts the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SubtractQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two quaternions
			/*!
			*	Multiplies the two specified quaternions and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outQuaternion		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn MultiplyQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the quaternion
			/*!
			*	Scales the specified quaternion with a number and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [in]  _scalar			The specified value to scale
			*	\param [out] _outQuaternion		The result of the scaling
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn ScaleD(MATH::GQUATERNIOND _quaternion, double _scalar, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Set the quaternion by the specified vector and the specified angle
			/*!
			*	Sets the quaternion with a number and stores the result in the output quaternion.
			*
			*	\param [in]  _vector			The specified vector
			*	\param [in]  _radian			The specified value of angle
			*	\param [out] _outQuaternion		The result of the rotation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SetByVectorAngleD(MATH::GVECTORD _vector, double _radian, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Set the quaternion by the specified matrix
			/*!
			*	Sets the quaternion by the rotational part of the specified matrix
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _matrix			The specified matrix
			*	\param [out] _outQuaternion		The result of the rotation of matrix
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SetByMatrixD(MATH::GMATRIXD _matrix, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of the two specified quaternions
			/*!
			*	Calculates the dot product of two specified quaternions and stores the result in the output Value.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outValue			The value of the dot product
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn DotD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of the two specified quaternions
			/*!
			*	Crosses two specified quaternions and stores the result in the output Value.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The second quaternion
			*	\param [out] _outVector			The vector of the cross product
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn CrossD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Conjugate the specified quaternion
			/*!
			*	Conjugates the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outQuaternion		The result of the conjugate
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn ConjugateD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Inverse the specified quaternion
			/*!
			*	Inverses the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The specified quaternion
			*	\param [out] _outQuaternion		The result of the inverse
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn InverseD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of quaternion
			/*!
			*	Calculate the magnitude of the specified quaternion and stores the result in the output value.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outMagnitude		The result of the Calculation
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn MagnitudeD(MATH::GQUATERNIOND _quaternion, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize the specified quaternion
			/*!
			*	Normalizes the specified quaternion and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion		The quaternion
			*	\param [out] _outQuaternion		The result of the normalization
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn NormalizeD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Identity the specified quaternion
			/*!
			*	Set the output quaternion as an identity quaternion
			*
			*	\param [out] _outQuaternion		The result of the identity
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn IdentityD(MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolate between two specified quaternions
			/*!
			*	Linear interpolates between two specified quaternions
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The seconds quaternion
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outQuaternion		The result of the lerp
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn LerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Spherical linear interpolates between two specified quaternions
			/*!
			*	Spherical linear interpolates between two specified quaternions
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _quaternion1		The first quaternion
			*	\param [in]  _quaternion2		The seconds quaternion
			*	\param [in]  _ratio				The interpolation coefficient
			*	\param [out] _outQuaternion		The result of the lerp
			*
			*	\retval GReturn::FAILURE					The calculation failed
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn SlerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Demotes double quaternion to float quaternion
			/*!
			*	Performs a static_cast<float>() on every element of the input quaternion and assigns them to the output quaternion
			*
			*	\param [in]  _quaternionD		A double quaternion
			*	\param [out] _outQuaternionF	The input double quaternion static_casted to a float quaternion
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Downgrade(MATH::GQUATERNIOND _quaternionD, MATH::GQUATERNIONF& _outQuaternionF) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GQUATERNION_H