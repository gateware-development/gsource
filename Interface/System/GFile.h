#ifndef GFILE_H
#define GFILE_H

/*!
	File: GFile.h
	Purpose: A Gateware interface that handles both binary and textual file io and directory.
	Dependencies: Windows[(none)] Linux[(none)] Mac[(none)] 
	Authors: Justin W. Parks, Ryan Powser
	Contributors: Lari H. Norri, Anthony G. Balsamo, Kai Huang, Alexander Clemmons, Chase Richards, Alexander Cusaac
	Interface Status: Beta
	Asynchronous: Yes[X] No[]
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"

namespace GW
{
	namespace I
	{
		class GFileInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn OpenBinaryRead(const char* const _file) = 0;
			virtual GReturn OpenBinaryWrite(const char* const _file) = 0;
			virtual GReturn AppendBinaryWrite(const char* const _file) = 0;
			virtual GReturn OpenTextRead(const char* const _file) = 0;
			virtual GReturn OpenTextWrite(const char* const _file) = 0;
			virtual GReturn AppendTextWrite(const char* const _file) = 0;
			virtual GReturn Write(const char* const _inData, unsigned int _numBytes) = 0;
			virtual GReturn Read(char* _outData, unsigned int _numBytes) = 0;
			virtual GReturn WriteLine(const char* const _inData) = 0;
			virtual GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) = 0;
			virtual GReturn CloseFile() = 0;
			virtual GReturn FlushFile() = 0;
			virtual GReturn SetCurrentWorkingDirectory(const char* const _dir) = 0;
			virtual GReturn GetCurrentWorkingDirectory(char* _outDir, unsigned int _dirSize) = 0;
			virtual GReturn GetDirectorySize(unsigned int& _outSize) = 0;
			virtual GReturn GetSubDirectorySize(unsigned int& _outSize) = 0;
			virtual GReturn GetFilesFromDirectory(char* _outFiles[], unsigned int _numFiles, unsigned int _fileNameSize) = 0;
			virtual GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize, char* _outsubDir[]) = 0;
			virtual GReturn GetFileSize(const char* const _file, unsigned int& _outSize) = 0;
			virtual GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) = 0;  //IOS: APPNAME.APP - Location of the installed app on the current device, used for items such as app resources 
			virtual GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) = 0; // UWP: LOCAL //IOS: DOCUMENTS - Data that exists on the current device and is backed up in the cloud 
			virtual GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) = 0; // UWP: ROAMING //IOS: LIBRARY - Data that exists on all devices on which the user has installed the app 
			virtual GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) = 0; // UWP: TEMPORARY //IOS: TEMP - Data that could be removed by the system at any time 
			virtual GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) = 0; // UWP: LOCALCACHE //IOS: LIBRARY/CACHES - Persistent data that exists only on the current device 
			virtual GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) = 0;
		};
	}
};

#include "../../Source/System/GFile/GFile.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware library interfaces must belong.
	namespace SYSTEM
	{
		//! Cross platform FileIO/Directory handling.
		/*!
		*	Handles file input/output operations, as well as directory information and file information.
		*	GFile inherits directly from GMultiThreaded, therefore its implementation must be thread safe.
		*/
		class GFile final 
			: public I::GProxy<I::GFileInterface, I::GFileImplementation>
		{
			// All Gateware API interfaces contain no variables & are pure virtual.
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GFile)
			GATEWARE_FUNCTION(OpenBinaryRead)
			GATEWARE_FUNCTION(OpenBinaryWrite)
			GATEWARE_FUNCTION(AppendBinaryWrite)
			GATEWARE_FUNCTION(OpenTextRead)
			GATEWARE_FUNCTION(OpenTextWrite)
			GATEWARE_FUNCTION(AppendTextWrite)
			GATEWARE_FUNCTION(Write)
			GATEWARE_FUNCTION(Read)
			GATEWARE_FUNCTION(WriteLine)
			GATEWARE_FUNCTION(ReadLine)
			GATEWARE_FUNCTION(CloseFile)
			GATEWARE_FUNCTION(FlushFile)
			GATEWARE_FUNCTION(SetCurrentWorkingDirectory)
			GATEWARE_FUNCTION(GetCurrentWorkingDirectory)
			GATEWARE_FUNCTION(GetDirectorySize)
			GATEWARE_FUNCTION(GetSubDirectorySize)
			GATEWARE_FUNCTION(GetFilesFromDirectory)
			GATEWARE_FUNCTION(GetFoldersFromDirectory)
			GATEWARE_FUNCTION(GetFileSize)
			GATEWARE_FUNCTION(Seek)
			GATEWARE_FUNCTION(GetInstallFolder)
			GATEWARE_FUNCTION(GetSaveFolder)
			GATEWARE_FUNCTION(GetPreferencesFolder)
			GATEWARE_FUNCTION(GetTempFolder)
			GATEWARE_FUNCTION(GetCacheFolder)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//!Creates a GFile Object.
			/*!
			*	The GFile created by this function will have its current working directory defaulted to
			*	the directory where the program was ran from. Call SetCurrentWorkingDirectory to change it.
			*	No file will be opened in creation of GFile. Call an Open function to open one.
			*	Created GFile object will have its reference count initialized to one.
			*
			*	\retval GReturn::FAILURE  GFile could not be created.
			*	\retval GReturn::SUCCESS  Gfile successfully created.
			*/
			GReturn Create();

			//! Opens a file for binary read.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the function will fail.
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::FILE_NOT_FOUND  File could not be found.
			*	\retval GReturn::INVALID_ARGUMENT  A null pointer was passed in.
			*	\retval GReturn::FAILURE  A file is already opened.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn OpenBinaryRead(const char* const _file) = 0;

			//! Opens a file for binary write with truncation.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the file will be created in
			*	the current working directory. File can now be read from with Read().
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  A file is already open or file could not be found/created.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn OpenBinaryWrite(const char* const _file) = 0;

			//! Opens a file for binary write with append.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the file will be created in
			*	the current working directory. File can now be written to with Write().
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  A file is already open or the file could not be found/created.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn AppendBinaryWrite(const char* const _file) = 0;

			//! Opens a file for text read.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the function will fail.
			*   File can now be written to with Write().
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::FILE_NOT_FOUND  File could not be found.
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  A file is already open.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn OpenTextRead(const char* const _file) = 0;

			//! Opens a file for text write with truncation.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the file will be created in
			*	the current working directory. File can now be read from with Read().
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  A file is already open or the file could not be found/created.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn OpenTextWrite(const char* const _file) = 0;

			//! Opens a file for text write with append.
			/*!
			*	The file name passed into the function should be passed like it is a relative path.
			*	The function will look in the current working directory for the file.
			*	If the file is not found in the current working directory, the file will be created in
			*	the current working directory. File can now be written to with Write().
			*
			*	\param [in] _file The file name of the file to open.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  A file is already open or the file could not be found/created.
			*	\retval GReturn::SUCCESS  Successfully opened the file.
			*/
			virtual GReturn AppendTextWrite(const char* const _file) = 0;

			//! Writes binary data to the currently opened file.
			/*!
			*	Will append or truncate file based on what mode the currently
			*	opened file was opened with.
			*
			*	\param [in] _inData The data to write out to file.
			*	\param [in] _numBytes The number of bytes to write out to the file.
			*
			*	\retval GReturn::INVALID_ARGUMENT  Either a nullptr was passed in or a size of 0 bytes was passed in.
			*	\retval GReturn::FAILURE  Either a file is not open or the write failed.
			*	\retval GReturn::SUCCESS  Successfully wrote out the data.
			*/
			virtual GReturn Write(const char* const _inData, unsigned int _numBytes) = 0;

			//! Reads binary from the currently opened file.
			/*!
			*	Reads binary data and stores it into a char* until the byte limit
			*	is reached.
			*
			*	\param [out] _outData The variable to store the read in bytes.
			*	\param [in] _numBytes The number of bytes to read in from the file.
			*
			*	\retval GReturn::INVALID_ARGUMENT A byte size of 0 was passed in.
			*	\retval GReturn::FAILURE  Either file is not open or read failed. _outData will be null.
			*	\retval GReturn::SUCCESS  Successful read.
			*/
			virtual GReturn Read(char* _outData, unsigned int _numBytes) = 0;

			//! Writes text to the currently opened file.
			/*!
			*	Will append or truncate file based on what mode the currently
			*	opened file was opened with.
			*
			*	\param [in] _inData Null terminated string to write out.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  Either file is not open or read failed.
			*	\retval GReturn::SUCCESS  Successful write.
			*/
			virtual GReturn WriteLine(const char* const _inData) = 0;

			//! Reads text to the currently opened file.
			/*!
			*	Reads text from the current file until either the size is
			*	reached or delimiter is reached.
			*
			*	\param [out] _outData Null terminated string to write out.
			*	\param [in] _outDataSize The size of _outData.
			*	\param [in] _delimiter The delimiter to stop reading at.
			*
			*	\retval GReturn::INVALID_ARGUMENT  Either a nullptr was passed in or the size request is 0.
			*	\retval GReturn::FAILURE  Either file is not open or read failed.
			*	\retval GReturn::SUCCESS  Successful read.
			*/
			virtual GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) = 0;

			//! Flushes and closes the current file.
			/*!
			*	\retval FAILURE  A file is not currently open.
			*	\retval SUCCESS  File successfully flushed and closed.
			*/
			virtual GReturn CloseFile() = 0;

			//! Flushes the current file from memory to disk.
			/*!
			*	\retval GReturn::FAILURE  A file is not currently open.
			*	\retval GReturn::SUCCESS  File successfully flushed.
			*/
			virtual GReturn FlushFile() = 0;

			//! Changes the current working directory.
			/*!
			*	This sets the directory we will look into with any of the Open functions or other directory functions.
			*	Paths that are not relative to the directory the program was ran from should be passed in as absolute paths.
			*
			*	\param [in] _dir An absolute path to the directory to set as the current working directory.
			*
			*	\retval GReturn::FILE_NOT_FOUND  The directory could not be found.
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval GReturn::FAILURE  Failed to open directory (Could be because it was not found).
			*	\retval GReturn::SUCCESS  Successfully set the current working directory.
			*/
			virtual GReturn SetCurrentWorkingDirectory(const char* const _dir) = 0;

			//! Retrieves the absolute path of the current working directory.
			/*!
			*	This is the directory we will look into for any file Open commands.
			*	This is by Windows standard guaranteed to be 255 or less.
			*
			*	\param [out] _outDir An absolute path to the directory to set as the current working directory.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval GReturn::INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*	\retval GReturn::FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval GReturn::SUCCESS  Successfully obtained the working directory.
			*/
			virtual GReturn GetCurrentWorkingDirectory(char* _outDir, unsigned int _dirSize) = 0;

			//! Gets the number of files in the current working directory.
			/*!
			*	\param [out] _outSize The number of files in the directory.
			*
			*	\retval GReturn::FAILURE  Either currently working directory is invalid or count failed. _outSize will be -1.
			*	\retval GReturn::SUCCESS  Successfully counted the files in the directory.
			*/
			virtual GReturn GetDirectorySize(unsigned int& _outSize) = 0;

			//! Gets the number of files in the current working sub-directory
			/*!
			*   \param [out] _outSize The number of files in the sub-directory.
			*
			*	\retval GReturn::FAILURE  Either currently working directory is invalid or count failed. _outSize will be -1.
			*   \retval GReturn::SUCCESS  Successfully counted the files in the directory.
			*/
			virtual GReturn GetSubDirectorySize(unsigned int& _outSize) = 0;

			//! Gets the names of all files in the current working directory.
			/*!
			*	This function will retrieve just the file names and extensions.
			*	Any Open function using these names will assume the files are in the current working directory.
			*	Any change of the current working directory will make the names generated by this function
			*	invalid until called again.
			*
			*	\param [out] _outFiles Stores the names of the files retrieved.
			*	\param [in] _numFiles The number of files.
			*	\param [in] _fileNameSize The size of the file names.
			*
			*	\retval GReturn::FAILURE  Either current working directory is invalid or obtaining file names failed.
			*	\retval GReturn::SUCCESS  Successfully retrieved the file names.
			*/
			virtual GReturn GetFilesFromDirectory(char* _outFiles[], unsigned int _numFiles, unsigned int _fileNameSize) = 0;

			//! Gets the names of all folders/sub-directories in the current working directory.
			/*!
			*	This function will retrieve just the folders/sub-directories names.
			*	Any change of the current working directory will make the names generated by this function
			*	invalid until called again.
			*
			*	\param [in] _numsubDir The number of sub-directories.
			*	\param [in] _subDirNameSize The size of the sub-directories names.
			*	\param [out] _outsubDir Stores the names of the sub-directories retrieved.
			*
			*	\retval GReturn::FAILURE  Either current working directory is invalid or obtaining sub-directories names failed.
			*	\retval GReturn::SUCCESS  Successfully retrieved the folders/sub-directories names.
			*/
			virtual GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize, char* _outsubDir[]) = 0;

			//! Gets the size of the specified file in bytes.
			/*!
			*	The filename passed into this function should be passed as a relative path.
			*	This function will assume the file passed in is in the current working directory
			*	and will look for it there.
			*
			*	\param [in] _file The file to get the size of.
			*	\param [out] _outSize will store the size of the file.
			*
			*	\retval GReturn::FILE_NOT_FOUND		Could not locate the file. Check that the current working directory is valid.
			*	\retval GReturn::SUCCESS			Successfully retrieved the file size.
			*/
			virtual GReturn GetFileSize(const char* const _file, unsigned int& _outSize) = 0;

			//!	Gets the absolute path of the install location on the current platform.
			/*!
			*	This is the directory we will look into for the installed location of the current platform.
			*	Location of the installed app on the current device, used for items such as app resources.
			*
			*	\param [out] _outDir An absolute path to the install directory.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval SUCCESS  Successfully obtained the install directory.
			*	\retval FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*/
			virtual GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) = 0;

			//!	Gets the absolute path of the directory commonly used for the save folder on the current platform.
			/*!
			*	This is the directory we will look into for game saves files.
			*	Data that exists on the current device and is backed up in the cloud.
			*
			*	\param [out] _outDir An absolute path to the the folder used for save files.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval SUCCESS  Successfully obtained the save directory.
			*	\retval FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*/
			virtual GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) = 0;

			//!	Gets the absolute path of the directory commonly used for the player settings on the current platform.
			/*!
			*	This is the directory we will look into for game settings files.
			*	Data that exists on all devices on which the user has installed the app.
			*
			*	\param [out] _outDir An absolute path to the the folder used for settings files.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval SUCCESS  Successfully obtained the settings directory.
			*	\retval FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*/
			virtual GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) = 0;
				
			//!	Gets the absolute path of the directory commonly used for the temp files on the current platform.
			/*!
			*	This is the directory we will look into for temporary files.
			*	Data that could be removed by the system at any time.
			*
			*	\param [out] _outDir An absolute path to the the folder used for temp files.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval SUCCESS  Successfully obtained the temporary files directory.
			*	\retval FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*/
			virtual GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) = 0;

			//!	Gets the absolute path of the directory commonly used for the cache folder on the current platform.
			/*!
			*	This is the directory we will look into for cached files.
			*	Persistent data that exists only on the current device.
			*
			*	\param [out] _outDir An absolute path to the the folder used for cached files.
			*	\param [in] _dirSize The size of _outDir.
			*
			*	\retval SUCCESS  Successfully obtained the cache directory.
			*	\retval FAILURE  The current working directory is invalid or _outDir was not big enough. _outDir will be null.
			*	\retval INVALID_ARGUMENT  A nullptr was passed in or the size is 0.
			*/
			virtual GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) = 0;
			
			//! Seeks to a location in the file from a given starting point.
			/*!
			*	This function will seek to a new location starting from _seekFrom and moving _amount bytes. If _seekFrom
			*	is set to -1, the function will start from whatever position it is currently in. _outCurrPos will be set
			*	to the new offset when the function completes (essentially _seekFrom + _amount). This function will fail
			*	and return GReturn::FILE_FOUND_FOUND if there is no file currently open. If _seekFrom + _amount is less than 0 or
			*	greater than the file size, this means you are trying to seek past the beginning or end of the file and
			*	the function will return GReturn::INVALID_ARGUMENT.
			*
			*	\param [in] _seekFrom The byte location to start seeking from. -1 (0xFFFFFFFF) seeks from your current position.
			*	\param [in] _amount The amount of bytes to traverse either forwards or backwards.
			*	\param [out] _outCurrPos The offset of the file pointer relative to the beginning of the file after the seek is complete
			*
			*	\retval GReturn::FILE_NOT_FOUND		There is no file currently open.
			*	\retval GReturn::INVALID_ARGUMENT	One or more arguments were invalid.
			*	\retval GReturn::SUCCESS			Successfully seeked.
			*/
			virtual GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GFILE_H