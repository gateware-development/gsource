#ifndef GSYSTEMDEFINES_H
#define GSYSTEMDEFINES_H

/*!
	File: GSystemDefines.h
	Purpose: Gateware system related defines or structs.
	Author: Peter Farber
	Contributors: Lari H. Norri, Ryan Powser
	Copyright: 7thGate Software LLC.
	License: MIT
*/

// Maximum thread pool size allocated by thread pool. Only actual available hardware thread count is used.
#define G_MAX_THREAD_POOL_SIZE		1024
// default amount of time in microseconds to nap threads that should release resources
#define G_THREAD_DEFAULT_SLEEP		5000
// amount of time in microseconds below which daemons are spin-locked for launch
#define G_DAEMON_LAUNCH_THRESHOLD	100
// amount of bytes gconcurrent uses to determine how many elements should be allocated per-thread in BranchParallel
#define G_CONCURRENT_AUTO_SECTION	131072

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware library interfaces must belong.
	namespace SYSTEM
	{
		/*! \addtogroup SystemDefines
		*  @{
		*/

		/*! The structure used to pass into input libraries on all platforms. */
		struct UNIVERSAL_WINDOW_HANDLE
		{
			void* window;	/*!< Void pointer storing the window handle. You can cast this to your platform specific window handle like HWND. */
			void* display;	/*!< Void pointer storing the display handle. Currently only used on Linux, possibly other systems in the future. */
		};

		//! GWindowStyle holds the possible types of window GWindow can create.
		enum class GWindowStyle
		{
			WINDOWEDBORDERED,		//!< a standard window
			WINDOWEDBORDERLESS,		//!< window with no title bar or borders
			WINDOWEDLOCKED,			//!< same as WINDOWEDBORDERED but with no maximize or resize options
			FULLSCREENBORDERED,		//!< as if you maximized a standard window
			FULLSCREENBORDERLESS,	//!< common for modern video games
			MINIMIZED				//!< as if you minimized a standard window
		};

		/*! @} */
	}
}
#endif // GSYSTEMDEFINES_H