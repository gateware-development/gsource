#ifndef GAPP_H
#define GAPP_H

/*!
	File: GApp.h
	Purpose: This interface provides the required basic framework on mobile/console platforms.
	Author: Lari H. Norri
	Contributors: Chase Richards, Jacob Morales
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include <functional> // required for GEventReceiver

namespace GW
{
	namespace I
	{
		class GAppInterface : public virtual GInterfaceInterface
		{
		public:
			// if your looking for application events create a GWindow.

			// static functions
			static GReturn PreviousExecutionStateWasTerminated(bool& _outWasTerminated) { return GReturn::NO_IMPLEMENTATION; };
		};
	}// end I namespace
}//end GW namespace

#include "../../Source/System/GApp/GApp.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware library interfaces must belong.
	namespace SYSTEM
	{
		//! The GApp class allows the running of "main()" in non desktop environments.
		/*! 
		*	Provides Mobile/Console entry point and framework when a basic "main" is not supported.
		*	The GApp class is designed to simplify the deployment of Gateware based projects to
		*	mobile and tightly controlled platforms like iOS, UWP, Android and Game consoles.
		*	If you intend to use your own framework on a non-desktop platform we recommend you
		*	disable this library by defining GATEWARE_DISABLE_GAPP above your Gateware.h include.
		*/
		class GApp final : public I::GProxy<I::GAppInterface, 
											I::GAppImplementation, 
											std::function<int()>>
		{
			// All Gateware API interfaces contain no variables & are pure virtual.
		public:
			GATEWARE_PROXY_CLASS(GApp)

			//! Special Constructor used to invoke a desktop main entrypoint in an App based environment.
			/*!
			*	Calls the "Create" routine using the given entrypoint function. 
			*
			*	\param _desktopMainFunction	Pointer to your desktop console main entrypoint function (a.k.a "main")
			*/
			GApp(int(*_desktopMainFunction)()) { Create(_desktopMainFunction); }
			//! Special Constructor used to invoke a desktop main entrypoint in an App based environment.
			/*!
			*	Calls the "Create" routine using the given entrypoint function. 
			*	This variant supports main functions which utilize argument lists, 
			*	though be aware said console args are not provided by Apps so will be set to Zero & nullptr respectively.
			*
			*	\param _desktopMainFunction	Pointer to your desktop console main entrypoint function (a.k.a "main")
			*/
			GApp(int(*_desktopMainFunction)(int, char*[])) { 
				Create(std::bind(_desktopMainFunction,0,nullptr)); 
			}

			//! Function that returns whether or not the app was terminated by the system after being suspended.
			/*!
			*	Sets the bool to true or false depending on the previous execution state the app was in prior to being suspended.
			*	True being that it was terminated after suspension
			*	False being that it was not terminated after suspension
			* 
			*	\param [out]  _outWasTerminated		The bool that the result is stored in
			*
			*	\retval SUCCESS						The previous execution state was gotten and the bool was set
			*/
			GATEWARE_STATIC_FUNCTION(PreviousExecutionStateWasTerminated)
			
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! The Create in GApp will invoke the provided entrypoint if not on a desktop platform.
			/*!
			*	GApp is a one of the more unique interfaces in Gateware. While it compiles
			*	as a proxy class for consistency, it is not designed to be Created in the
			*	traditional sense. GApp is an interface designed to compile required framework
			*	code for mobile and console environments. The way this class is used is that
			*	you declare a GApp below your desktop console entrypoint function.(a.k.a main())
			*	You then pass the "main" function to the constructor of GApp and it ensures the
			*	function is executed by passing it to this function. On Desktop platforms that
			*	already support the "main" entrypoint this process is bypassed.
			*
			*	\retval SUCCESS The provided function was successfully executed.
			*/
			GReturn Create(std::function<int()> _desktopMainFunction);
#endif
		}; // end GApp class
	} // end SYSTEM namespace
} // end GW namespace
#endif // #endif GAPP_H
