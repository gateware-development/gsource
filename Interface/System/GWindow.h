#ifndef GWINDOW_H
#define GWINDOW_H

/*!
	File: GWindow.h
	Purpose: A Gateware interface that creates a window and handles its properties.
	Author: Nic Russell
	Contributors: ShuoYi Chang, Andre Reid, Kai Huang, Ozzie Mercado, Ryan Powser, Chase Richards, Lari Norri, Alexander Cusaac
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventGenerator.h"
#include "../System/GSystemDefines.h"

namespace GW
{
	namespace I
	{
		class GWindowInterface : public virtual GEventGeneratorInterface
		{
		public:
			enum class Events
			{
				SUSPEND, //!< App only message that is fired when the application is suspended
				RESUME, //!< App only message that is fired when the application is resumed from a suspended state
				LEAVING_BACKGROUND, //!< App only message that is fired when: background state -> foreground state
				ENTERED_BACKGROUND, //!< App only message that is fired when: foreground state -> background state
				NOTIFY,
				NON_EVENT,
				MINIMIZE,
				MAXIMIZE,
				RESIZE,
				MOVE,
				DISPLAY_CLOSED,
				EVENTS_PROCESSED,
				DESTROY,
			};

			struct EVENT_DATA
			{
				Events eventFlags;
				unsigned int height;
				unsigned int width;
				unsigned int clientHeight;
				unsigned int clientWidth;
				int windowX;
				int windowY;
				void* windowHandle;
			};

			virtual GReturn ProcessWindowEvents() = 0;
			virtual GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, GW::SYSTEM::GWindowStyle _style) = 0;
			virtual GReturn SetWindowName(const char* _newName) = 0;
			virtual GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) = 0;
			virtual GReturn MoveWindow(int _x, int _y) = 0;
			virtual GReturn ResizeWindow(int _width, int _height) = 0;
			virtual GReturn Maximize() = 0;
			virtual GReturn Minimize() = 0;
			virtual GReturn ChangeWindowStyle(GW::SYSTEM::GWindowStyle _style) = 0;
			virtual GReturn	GetWidth(unsigned int& _outWidth) const = 0;
			virtual GReturn	GetHeight(unsigned int& _outHeight) const = 0;
			virtual GReturn	GetClientWidth(unsigned int& _outClientWidth) const = 0;
			virtual GReturn	GetClientHeight(unsigned int& _outClientHeight) const = 0;
			virtual GReturn	GetX(unsigned int& _outX) const = 0;
			virtual GReturn	GetY(unsigned int& _outY) const = 0;
			virtual GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const = 0;
			virtual GReturn GetWindowHandle(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const = 0;
			virtual GReturn IsFullscreen(bool& _outIsFullscreen) const = 0;
			virtual GReturn IsFocus(bool& _outIsFocus) const = 0;
		};
	}
}

#include "../../Source/System/GWindow/GWindow.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware system interfaces must belong.
	namespace SYSTEM
	{
		//! A Gateware interface that creates a window and handles its properties.
		class GWindow final
			: public I::GProxy<I::GWindowInterface, I::GWindowImplementation, int, int, int, int, GW::SYSTEM::GWindowStyle>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GWindow)
			GATEWARE_TYPEDEF(Events)
			GATEWARE_TYPEDEF(EVENT_DATA)
			GATEWARE_FUNCTION(ProcessWindowEvents)
			GATEWARE_FUNCTION(ReconfigureWindow)
			GATEWARE_FUNCTION(SetWindowName)
			GATEWARE_FUNCTION(SetIcon)
			GATEWARE_FUNCTION(MoveWindow)
			GATEWARE_FUNCTION(ResizeWindow)
			GATEWARE_FUNCTION(Maximize)
			GATEWARE_FUNCTION(Minimize)
			GATEWARE_FUNCTION(ChangeWindowStyle)
			GATEWARE_CONST_FUNCTION(GetWidth)
			GATEWARE_CONST_FUNCTION(GetHeight)
			GATEWARE_CONST_FUNCTION(GetClientWidth)
			GATEWARE_CONST_FUNCTION(GetClientHeight)
			GATEWARE_CONST_FUNCTION(GetX)
			GATEWARE_CONST_FUNCTION(GetY)
			GATEWARE_CONST_FUNCTION(GetClientTopLeft)
			GATEWARE_CONST_FUNCTION(GetWindowHandle)
			GATEWARE_CONST_FUNCTION(IsFullscreen)
			GATEWARE_CONST_FUNCTION(IsFocus)

			// reimplemented functions from GEventGenerator
			GATEWARE_FUNCTION(Register)
			GATEWARE_CONST_FUNCTION(Observers)
			GATEWARE_FUNCTION(Push)
			//! \endcond
			

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GWindow object. 
			/*!
			*	\param [in] _x		X position of the window
			*	\param [in] _y		Y position of the window
			*	\param [in] _width	Client width of the window
			*	\param [in] _height Client height of the window
			*	\param [in] _style	Style of the window. See \ref GWindowStyle for more info.
			*
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create(int _x, int _y, int _width, int _height, GW::SYSTEM::GWindowStyle _style);

			//! Events holds the possible events a GWindow window can broadcast.
			enum class Events {
				MINIMIZE,			//!< Window was minimized.
				MAXIMIZE,			//!< Window was maximized.
				RESIZE,				//!< Window was resized in some way.
				MOVE,				//!< Window was moved.
				DISPLAY_CLOSED,		//!< Linux only message that is fired after XCloseDisplay() but before XCloseWindow() 
				EVENTS_PROCESSED,	//!< Notifies users that all queued window events have been processed 
				DESTROY,			//!< Window was closed.
			};

			/*! EVENT_DATA holds the properties of the window the event was sent from. */
			struct EVENT_DATA {
				Events eventFlags;			/*!< The type of GWindow::Events this EVENT_DATA belongs to. */
				unsigned int height;		/*!< The total height of the window including borders. */
				unsigned int width;			/*!< The total width of the window including borders. */
				unsigned int clientHeight;	/*!< The height of the client area. */
				unsigned int clientWidth;	/*!< The width of the client area. */
				int windowX;				/*!< The X position of the top left corner of the window in pixels. */
				int windowY;				/*!< The Y position of the top left corner of the window in pixels. */
				void* windowHandle;			/*!< A handle to the native OS window. */
			};

			//! Flushes and processes all messages from the window's event queue.
			/*!
			*	This function is meant to be called once a frame in an application's
			*	main loop. This function will break when all waiting messages have
			*   been processed and the event queue is empty.
			*
			*	\retval GReturn::FAILURE The window is invalid or is about to be invalid
			*	\retval GReturn::SUCCESS The messages were successfully processed and removed
			*/
			virtual GReturn ProcessWindowEvents() = 0;

			//! Gives the currently opened window the specified size, position and style.
			/*!
			*	If width and height are equal to or greater than the native resolution, the passed in
			*	GWindowStyle will be overwritten to be the fullscreen version if it is not already.
			*	If position parameters are less than 0 then 0 will be used. If position parameters
			*	are greater than native resolution, maximum native resolution parameters will be used.
			*
			*	\param [in] _x The x position on screen to place the top left corner of the window.
			*	\param [in] _y The y position on screen to place the top left corner of the window.
			*	\param [in] _width The width to resize the client area of the window to.
			*	\param [in] _height The height to resize the client area of the window to.
			*	\param [in] _style The style to give to the window. (see \ref GWindowStyle for style options)
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, GW::SYSTEM::GWindowStyle _style) = 0;

			//! Changes the text displayed at the top bar of the window.
			/*!
			*	\param [in] _newName The new name to give to the window.
			*
			*	\retval GReturn::INVALID_ARGUMENT The string passed in contains invalid characters or is null.
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn SetWindowName(const char* _newName) = 0;

			//! Changes the icon displayed in the title bar of the window.
			/*!
			*	\param [in] _width The width of the icon.
			*	\param [in] _height The height of the icon.
			*	\param [in] _argbPixels An array of ARGB formatted pixels that represent the new icon.
			*
			*	\retval GReturn::INVALID_ARGUMENT One of the arguments is less than 1 or a nullptr
			*	\retval GReturn::FAILURE The window or pixel array is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) = 0;

			//! Repositions the top left corner of the window to the specified x and y coordinates on screen.
			/*!
			*	If position parameters are less than 0, then 0 will be used. If position parameters
			*	are greater than native resolution, maximum native resolution parameters will be used.
			*
			*	\param [in] _x The x position on screen to move the window to.
			*	\param [in] _y The y position on screen to move the window to.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn MoveWindow(int _x, int _y) = 0;

			//! Resizes the currently opened window to the specified width and height.
			/*!
			*	If width and height are greater than the native resolution, the GWindowStyle
			*	will be overwritten to be the fullscreen version if it is not already.
			*   If position parameters are less than 0 then 0 will be used. If position parameters
			*	are greater than native resolution, maximum native resolution parameters will be used.
			*
			*	\param [in] _width The width to resize the client area of the window to.
			*	\param [in] _height The height to resize the client area of the window to.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn ResizeWindow(int _width, int _height) = 0;

			//! Maximize the currently opened window.
			/*!
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn Maximize() = 0;

			//! Minimizes the currently opened window.
			/*!
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn Minimize() = 0;

			//! Sets the currently opened window's style to the specified style.
			/*!
			*	\param [in] _style The \ref GWindowStyle to change the window to.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*	\retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn ChangeWindowStyle(GW::SYSTEM::GWindowStyle _style) = 0;

			//! Gets the width in pixels of the currently opened window.
			/*!
			*	\param [out] _outWidth Width of the currently opened window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetWidth(unsigned int& _outWidth) const = 0;

			//! Gets the height in pixels of the currently opened window.
			/*!
			*	\param [out] _outHeight Height of the currently opened window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetHeight(unsigned int& _outHeight) const = 0;

			//! Gets the client width in pixels of the currently opened window.
			/*!
			*	\param [out] _outClientWidth Client width of the currently opened window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetClientWidth(unsigned int& _outClientWidth) const = 0;

			//! Gets the client height in pixels of the currently opened window.
			/*!
			*	\param [out] _outClientHeight Client height of the currently opened window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetClientHeight(unsigned int& _outClientHeight) const = 0;

			//! Gets the X position in screen coordinates of the top left corner of the window.
			/*!
			*	\param [out] _outX X position in screen coordinates of the top left corner of the window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetX(unsigned int& _outX) const = 0;

			//! Gets the Y position in screen coordinates of the top left corner of the window.
			/*!
			*	\param [out] _outY Y position in screen coordinates of the top left corner of the window.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn	GetY(unsigned int& _outY) const = 0;

			//! Gets the location of the top-left corner of the client area of the window.
			/*!
			*	\param [out] _outX X location of the top-left corner of the client area.
			*	\param [out] _outY Y location of the top-left corner of the client area.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const = 0;

			//! Gets a UNIVERSAL_WINDOW_HANDLE containing cast-able void pointers to the platform specific window handle and display.
			/*!
			*	\param [out] _outUniversalWindowHandle Contains information for handles
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn GetWindowHandle(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const = 0;

			//! Gets a bool specifying whether or not the currently opened window is fullscreen.
			/*!
			*	\param [out] _outIsFullscreen True if fullscreen, false otherwise.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn IsFullscreen(bool& _outIsFullscreen) const = 0;

			//! Gets a bool specifying whether or not the window has input focus.
			/*!
			*	\param [out] _outIsFocus True if in focus, false otherwise.
			*
			*	\retval GReturn::FAILURE The window is invalid
			*   \retval GReturn::SUCCESS The operation is successful
			*/
			virtual GReturn IsFocus(bool& _outIsFocus) const = 0;
#endif // DOXYGEN_ONLY
		};
	};
}
#endif // GWINDOW_H
