#ifndef GDAEMON_H
#define GDAEMON_H

/*!
	File: GDaemon.h
	Purpose: A Gateware interface that allows simple and efficient management of recurring thread work. 
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventGenerator.h"

namespace GW
{
	namespace I
	{
		class GDaemonInterface : public virtual GEventGeneratorInterface
		{
		public:
			enum class Events
			{
				OPERATION_COMPLETED,
				OPERATIONS_PAUSED,
				OPERATIONS_RESUMING
			};

			struct EVENT_DATA
			{
				unsigned long long operationCount;
			};

			virtual GReturn Pause(bool _wait, unsigned int _spinUntil) = 0;
			virtual GReturn Resume() = 0;
			virtual GReturn Counter(unsigned long long& _outCounter) const = 0;
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/System/GDaemon/GDaemon.hpp"

//! The SYSTEM namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The SYSTEM namespace to which all Gateware fundamental interfaces must belong.
	namespace SYSTEM
	{
		//! This Proxy is a simple way to schedule & monitor recurring background operations.  
		/*!
		*	GDaemon is used by end users and even Gateware itself to schedule repeating background thread work.
		*	Even though you can create multiple instances of a GDaemon they all utilize the same background thread pool.
		*	While simple recurring tasks based on the passage of time are standard, you can also create co-dependent 
		*   chains of tasks by using the built-in events to "Pause" and "Resume" other GDaemons. 
		*/
		class GDaemon final
			: public I::GProxy<I::GDaemonInterface, I::GDaemonImplementation, unsigned int, std::function<void()>>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GDaemon)
			GATEWARE_TYPEDEF(Events)
			GATEWARE_TYPEDEF(EVENT_DATA)
			GATEWARE_FUNCTION(Pause)
			GATEWARE_FUNCTION(Resume)
			GATEWARE_CONST_FUNCTION(Counter)

			// reimplemented functions
			GATEWARE_FUNCTION(Register)
			GATEWARE_CONST_FUNCTION(Observers)
			GATEWARE_FUNCTION(Push)
			//! \endcond
				
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GDaemon. 
			/*!
			*	The GDaemon is used by end users and even Gateware itself to schedule repeating background thread work.
			*	Even though you can create multiple instances of a GDaemon they all utilize the same background thread pool.
			*	While simple recurring tasks based on the passage of time are standard, you can also create co-dependent 
			*   chains of tasks by using the built-in events to "Pause" and "Resume" other GDaemons. 
			*
			*	\param [in] _targetInterval		How often (MILLISECONDS) should this daemon attempt to execute the operation provided.(inclusive)
			*									Recurring tasks are launched based on an absolute time interval rather than a relative time interval. 
			*									NOTE: If an operation takes longer than the target interval, the next operation will be delayed.
			*	\param [in] _daemonOperation	(OPTIONAL) The function to be executed recurring based on the provided interval.
			*									NOTE: If an operation is not provided the Daemon will still spawn an events at the interval.
			*
			*	\retval GReturn::INVALID_ARGUMENT	The _targetInterval must be greater than 0 milliseconds.
			*	\retval GReturn::SUCCESS			The Daemon was successfully initialized and created and is now running.
			*/
			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation);

			//! Alternate Allocation & Initialization of a GDaemon. 
			/*!
			*	Same as standard Create call but allows the user to start the Daemon in a paused state if desired.	
			*
			*	\param [in] _targetInterval		Same as standard Create parameter.
			*	\param [in] _daemonOperation	Same as standard Create parameter.
			*	\param [in] _delayOrPause		How many milliseconds a daemon will wait before starting operations.
			*									Passing no delay (0) will cause the daemon to begin in a manually paused state.
			*
			*	\retval GReturn::INVALID_ARGUMENT	The _targetInterval must be greater than 0 milliseconds.
			*	\retval GReturn::SUCCESS			The Daemon was successfully initialized and created.
			*/
			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation, unsigned int _delayOrPause);
			//! Alternate Allocation & Initialization of a GDaemon. 
			/*!
			*	Same as alternate Create call but allows the user to provide a GLogic object with swappable logic.
			*
			*	\param [in] _daemonLogic		GLogic object containing the logic you wish to execute.
			*	\param [in] _targetInterval		Same as standard Create parameter.
			*	\param [in] _delayOrPause		Same as alternate Create parameter.
			*
			*	\retval GReturn::INVALID_ARGUMENT	_daemonLogic must be valid and the _targetInterval 
													must be greater than 0 milliseconds.
			*	\retval GReturn::SUCCESS			The Daemon was successfully initialized and created.
			*/
			GReturn Create(GLogic _daemonLogic, unsigned int _targetInterval, unsigned int _delayOrPause);

			//! Events generated by GDaemon. These messages are used to notify observers when changes to operations occur. 
			enum class Events {
				OPERATION_COMPLETED,	//!< An iteration of the provided operation was just executed and completed.
				OPERATIONS_PAUSED,		//!< All future operations are on hold and none are currently running.
				OPERATIONS_RESUMING		//!< Scheduled operations are about to resume.
			};

			/*! EVENT_DATA provided by GDaemon. All events provide the following relevant information. */
			struct EVENT_DATA {
				unsigned long long operationCount; /*!< The number of times the internal operation has been fully executed so far. */
			};

			//! Requests that a GDaemon cease re-scheduling of operations once/if the current operation is complete. 
			/*!
			*	If an operation is currently active the pause will take effect once it completes.
			*	The Pause will not block the current thread of execution unless "_wait" is true.
			*
			*	\param [in] _wait		Should this function block until the active execution of this Daemon has completed?
			*							NOTE: Do not set this to true if you are on the same thread as the Daemon (ex: within Operation).
			*	\param [in] _spinUntil	Determine how long this thread will spin-lock waiting for daemon to complete (NANOSECONDS).
			*							Once "_spinUntil" nanoseconds has elapsed the current thread will yield to the OS scheduler.
			*							NOTE: If your not in a time/performance critical area you should just use 0.
			*
			*	\retval GReturn::DEADLOCK	You have attempted wait for Daemon within itself. (This would cause a Deadlock)
			*	\retval GReturn::SUCCESS	We have successfully signaled the Daemon to hold off on future operations.
			*	\retval GReturn::REDUNDANT	Future scheduled Operations have already been Paused.
			*/
			virtual GReturn Pause(bool _wait, unsigned int _spinUntil) = 0;

			//! Requests that a GDaemon resume re-scheduling of operations once enough time has elapsed. 
			/*!
			*	Signals the Daemon to append sequential operations once/if any current operations have completed.
			*
			*	\retval GReturn::SUCCESS	Scheduled operations have been resumed.
			*	\retval GReturn::REDUNDANT	Scheduled operations are already in progress.
			*/
			virtual GReturn Resume() = 0;

			//! Gets the number of times this Daemon has completed requested operations.  
			/*!
			*	The internal counter is incremented every time an operation has completed.
			*	This counter combined with Daemon events can potentially be used to synchronize data or chain workloads.
			*
			*	\param [out] _outCounter	The current number of completed operations.
			*
			*	\retval GReturn::SUCCESS	Value of internal counter was successfully written to output.
			*/
			virtual GReturn Counter(unsigned long long& _outCounter) const = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GDAEMON_H