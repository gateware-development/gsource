#ifndef GCOREDEFINES_H
#define GCOREDEFINES_H

/*!
	File: GCoreDefines.h
	Purpose: Lists the core #defines, enums, MACROS & root template/classes/structures used by the Gateware interfaces.
	Dependencies: Any first-party per-platform libraries that need to be manually linked will be noted in these sections.
	Author: Lari H. Norri
	Contributors: Ryan Powser, Artemis Kelsie Frost, Ozzie Mercado, Alexander Clemmons
	Interface Status: Alpha (GatewareX)
	Copyright: 7thGate Software LLC.
	License: MIT
*/

// The Gateware proxy pattern is fundamentally built upon std::weak & std::shared
// Gateware Event communication is based on a type erasure observer pattern <typeinfo>
#include <memory>
#include <algorithm>

//! The core namespace to which all Gateware interfaces must belong.
namespace GW
{
	/*! \addtogroup GReturnValues
	*  @{
	*/

	//! Listing of common failure/success codes returned by Gateware functions.
	enum class GReturn : int
	{
		// additional failure codes here
		TIMED_OUT				= -20,  //!< The operation has timed out and was not completed.
		SOFTWARE_UNAVAILABLE	= -19,  //!< A software dependency required for this operation is not available or too old.
		END_OF_FILE				= -18,  //!< The requested file has no more data to read in.
		FORMAT_UNSUPPORTED		= -17,	//!< The requested file format is not supported by the current platform/object.
		NO_IMPLEMENTATION		= -16,	//!< An interface function was called directly without a valid implementation.
		HARDWARE_UNAVAILABLE	= -15,	//!< This feature requires hardware/drivers that are currently not present.
		DEADLOCK				= -14,	//!< The thread on which you called this function would cause a deadlock.
		UNEXPECTED_RESULT		= -13,	//!< A function has reached a spot it was never expected to be able to. (Report these!)
		RESOURCE_LOCKED			= -12,	//!< This operation has failed because a shared resource could not be acquired.
		DISCONNECTED			= -11,	//!< This operation has failed because you are no longer connected.
		PREMATURE_DEALLOCATION	= -10,	//!< You have deallocated some Interface this object was using internally. (Hint::Share())
		FUNCTION_DEPRECATED		= -9,	//!< This function has been disabled but has not been removed.
		FEATURE_UNSUPPORTED		= -8,	//!< Attempted an operation that is currently not supported.
		FILE_NOT_FOUND			= -7,	//!< A requested file is not found.
		INTERFACE_UNSUPPORTED	= -6,	//!< The requested interface is not supported by the current platform/object.
		MEMORY_CORRUPTION		= -5,	//!< A memory corruption occurred in the function.
		INVALID_ARGUMENT		= -4,	//!< An invalid argument was passed into the function.
		IGNORED					= -3,	//!< The requested operation has already been performed and cannot be done again.
		EXPIRED_PROXY			= -2,	//!< The Interface referenced by this Proxy has been deallocated. (C++17 future support)
		FAILURE					= -1,	//!< The function failed (Check function documentation for possible reasons).
		EMPTY_PROXY				=  0,	//!< This Proxy was never (or is no longer) initialized to a valid Interface.
		// Here lies the split between success and failure codes
		SUCCESS					= +1,	//!< The function succeeded.
		REDUNDANT				= +2,	//!< The function succeeded but was not necessary.
		// additional success codes here

	}; // MOST COMMON: +1 == SUCCESS, +2 == REDUNDANT, -1 == FAILURE, -3 == IGNORED, 0 == EMPTY_PROXY, -2 == EXPIRED_PROXY (future)

	/*! @} */
};

/*! \addtogroup Operators
*  @{
*/

//! Macro used to determine if a function succeeded.
/*!
*	\param [in] _greturn_ The GReturn value to check.
*
*	\retval true GReturn value passed in was a success code.
*	\retval false GReturn value passed in was a failure code.
*/
#define G_PASS(_greturn_) (static_cast<int>(_greturn_) > 0)

//! Macro used to determine if a function has failed.
/*!
*	\param [in] _greturn_ The GReturn value to check.
*
*	\retval true GReturn value passed in was a failure code.
*	\retval false GReturn value passed in was a success code.
*/
#define G_FAIL(_greturn_) (static_cast<int>(_greturn_) < 1)

/*! @} */


// Internal definition, how large a GEvent is in memory.
// Note to Gateware Developers: first 16bytes are reserved for basic type identification. Design event packets appropriately.
#define G_EVENT_BLOCK_SIZE 64u
#define G_RAW_DATA_PACKET_SIZE (((G_EVENT_BLOCK_SIZE) - ((sizeof(std::size_t) << 1)) + 2))

//! The core namespace to which all Gateware structures & classes must belong.
namespace GW
{
	/*! \addtogroup Operators
	*  @{
	*/
	/*! Allows Gateware users to use the prefix "+" operator as shorthand for calling the G_PASS Macro. 
		\code
		GW::SYSTEM GWindow win;

		if (+win.Create()) {
			// do cool stuff
		} else {
			// abort program
		}
		\endcode
	*/
	inline bool operator+(const GReturn&& _rvalue) { return G_PASS(_rvalue); }
	/*! Allows Gateware users to use the prefix "-" operator as shorthand for calling the G_FAIL Macro. 
		\code
		GW::SYSTEM GWindow win;

		if (-win.Create()) {
			// abort program
		} else {
			// do cool stuff
		}
		\endcode
	*/
	inline bool operator-(const GReturn&& _rvalue) { return G_FAIL(_rvalue); }
	/*! @} */

	// Internal namespace for Gateware developers, Doxygen should be configured to ignore this namespace.
	namespace I {
		// Define the template class expander GProxy.
		// This is a core pillar used in the C++11 modernized version of Gateware.
		// A GProxy is a smart wrapper around a Gateware Interface & Implementation.
		// The proxy allows safe transfer and allocation/deallocation of interfaces as well as safe access.
		// Proxy rules: Copying creates safe handles but does not transfer ownership. Only moving r-values can transfer ownership.
		template<typename _Interface, typename _Implementation, typename... _CreationParams>
		class GProxy
		{
		protected:
			typedef _Interface forceRaw; // used to support the rare need of construction from raw pointers (prefer to avoid)
			typedef _Implementation forceStatic; // used to support static calls from proxy ops
			typedef std::shared_ptr<const _Interface> forceConst; // used to support const correctness in proxy ops
			typedef GProxy<_Interface, _Implementation, _CreationParams...> thisClass; // short hand for this type of class
			std::weak_ptr<_Interface> access; // most common way of access via proxy, does not require ownership
			std::shared_ptr<_Interface> storage; // Hard handle to data, not all instances use this, most are weak by default

		private:
			// All Proxys are friends of one another (required for casting)
			template<typename A, typename B, typename... C> friend class GProxy;
			// This function is what actually makes a GProxy "proxy" an interface & associated implementation.
			template<typename... _anyArgs>
			GReturn allocate(_anyArgs&&... _argTransfer)
			{
				GReturn result = GReturn::FAILURE;
				access.reset(); // clear any prior access, storage will be overwritten below
				storage = std::make_shared<_Implementation>(); // so we can write the below more safely...
				// *** A COMPILER ERROR HERE MEANS YOU ARE NOT PASSING ".Create(" A VALID SET OF ARGUMENTS ***
				// *** READ OUR INCLUDED DOCUMENTATION TO FIND THE PROPER ARGUMENTS TO ANY GATEWARE FUNCTION ***
				if (storage && G_PASS(result = dynamic_cast<_Implementation*>
					(storage.get())->Create(std::forward<_anyArgs>(_argTransfer)...)))
					access = storage; // used for primary access
				else // if we failed to create, destruct the interface immediately
					storage.reset();
				// return what happened
				return result;
			}

		public:
			// Creates an invalid but safe handle
			GProxy() = default;
			// Special constructor, allows creation from existing implementation.
			// This should only be used in conjunction with std::shared_from_this from inside an implementation.
			GProxy(const std::shared_ptr<_Interface>& _sharedFromThis)
			{
				access = storage = _sharedFromThis;
			}
			// copy constructor
			GProxy(const thisClass& _cpy)
			{
				if (this != &_cpy)
					access = _cpy.access;
			}
			// template version, attempts to convert incoming data to this class type using dynamic_pointer_cast if possible
			// We use SFINAE to ensure this is only called for non-derived types, those should use the standard overloads
			template<typename T, typename = std::enable_if<!std::is_convertible<T*, thisClass*>::value>>
			GProxy(const T& _cpy)
			{
				access = std::dynamic_pointer_cast<_Interface>(_cpy.access.lock());
			}
			// move constructor does a direct transfer since it is not considered a copy
			GProxy(thisClass&& _mov) noexcept //: storage(_mov.storage), access(_mov.access)
			{
				storage = std::move(_mov.storage);
				access = std::move(_mov.access);
			}
			// template version, attempts to convert incoming data to this class type using dynamic_pointer_cast if possible
			// We use SFINAE to ensure this is only called for non-derived types, those should use the standard overloads
			template<typename T, typename = std::enable_if<!std::is_convertible<T*, thisClass*>::value>>
			GProxy(T&& _mov) noexcept //: storage(std::dynamic_pointer_cast<_Interface>(std::move(_mov.storage))), access(storage)
			{
				storage = std::dynamic_pointer_cast<_Interface>(std::move(_mov.storage));
				access = (storage) ? storage : std::dynamic_pointer_cast<_Interface>(_mov.access.lock());
			}
			// Assignment operator
			virtual thisClass& operator=(const thisClass& _cpy) final
			{
				if (this != &_cpy)
				{
					access = _cpy.access;
					storage.reset(); // copy assignment may not transfer or retain any prior ownership
				}
				return *this;
			}
			// template version, attempts to convert incoming data to this class type using dynamic_pointer_cast if possible
			// We use SFINAE to ensure this is only called for non-derived types, those should use the standard overloads
			template<typename T, typename = std::enable_if<!std::is_convertible<T*, thisClass*>::value>>
			thisClass& operator=(const T& _cpy)
			{
				access = std::dynamic_pointer_cast<_Interface>(_cpy.access.lock());
				storage.reset(); // copy assignment may not transfer or retain any prior ownership
				return *this;
			}
			// move assignment
			virtual thisClass& operator=(thisClass&& _mov) noexcept final
			{
				storage = std::move(_mov.storage);
				access = std::move(_mov.access);
				return *this;
			}
			// template version, attempts to convert incoming data to this class type using dynamic_pointer_cast if possible
			// We use SFINAE to ensure this is only called for non-derived types, those should use the standard overloads
			template<typename T, typename = std::enable_if<!std::is_convertible<T*, thisClass*>::value>>
			thisClass& operator=(T&& _mov) noexcept
			{
				storage = std::dynamic_pointer_cast<_Interface>(std::move(_mov.storage));
				access = (storage) ? storage : std::dynamic_pointer_cast<_Interface>(_mov.access.lock());
				return *this;
			}
			// basic destructor
			~GProxy() = default;
			// Allows testing the class for validity
			virtual operator bool() const final { return !access.expired(); };
			// nullptr assignment is how to clear a proxy before waiting for it to fall out of scope
			virtual thisClass& operator =(std::nullptr_t) final { access.reset(); storage.reset(); return *this; };
			// The GProxy variadic template should match the main internal implementation "Create" function.
			// Though the templated variadic version of this function can support any set of arguments,
			// This version requires derived classes to define & support at least one specific Create variant.
			// This is required for API consistency. Alternate Create routines are supported by the next function.
			virtual GReturn Create(_CreationParams... _parameter) final
			{
				return allocate<_CreationParams...>(std::forward<_CreationParams>(_parameter)...);
			}
			// Provided for alternate Create function potential.(direct use by derived classes is optional)
			// This version is much more functional but cannot be virtual which is why the prior version exists.
			template<	typename... _AltArguments,
				typename = std::enable_if<!std::is_same< // SFINAE used to force virtual override in impl.
				std::tuple<_AltArguments...>, std::tuple<_CreationParams...> >::value>>
			GReturn Create(_AltArguments... _parameter)
			{
				return allocate<_AltArguments...>(std::forward<_AltArguments>(_parameter)...);
			}
			// Return a copy of the Proxy that allows for non-weak transfer of ownership
			// If direct ownership cannot be shared it will return an invalid handle
			virtual thisClass Share() const final
			{
				thisClass canShare;
				if (storage)
				{
					canShare.storage = storage;
					canShare.access = access;
				}
				return canShare; // r-value will invoke move to allow full transfer
			}
			// Surrenders strong ownership of your internal handle to another Proxy, weak ownership is maintained
			// If direct ownership cannot be surrendered it will return an invalid handle
			virtual thisClass Relinquish() final
			{
				thisClass canYield;
				if (storage)
				{
					canYield.storage = storage;
					canYield.access = access;
					storage.reset();
				}
				return canYield;
			}
			// Returns a direct copy of the Proxy with the current ownership status maintained
			// If direct ownership cannot be shared it will provide it's weak ownership
			virtual thisClass Mirror() const final
			{
				thisClass canReflect;
				canReflect.storage = storage;
				canReflect.access = access;
				return canReflect;
			}

			// *** BEGIN WARNING: The following operators & typedefs are provided only for advanced users, avoid unless needed. ***
			// Potentially unsafe (nullptr) access to underlying interface type, only use when a normal proxy call does not suffice
			virtual const std::shared_ptr<_Interface> operator*() noexcept final //std::move will handled by compiler
			{
				return (storage) ? storage : access.lock();
			}
			// Potentially unsafe (nullptr) access to underlying interface type, only use when a normal proxy call does not suffice
			virtual const std::shared_ptr<const _Interface> operator*() const noexcept final //std::move will handled by compiler
			{
				return (storage) ? storage : access.lock();
			}
			// Types that can be used to lock down the internal weak pointer in performance critical areas.
			// Do not use these unless you are very familiar & comfortable with how shared & weak pointers function.
			// As a general rule of thumb only create these on the stack where you need multiple subsequent API calls.
			// These should only be in places like inner loops or code that is currently being bottlenecked by std::weak_ptr::lock.
			// If you are concerned about access performance it may make more sense to GProxy<>::Share() your proxies with systems
			// which need to constantly access them. However, for edge cases of required weak access in high-perf systems use below:
			typedef const std::shared_ptr<_Interface> burst_w; // use if you are about to author a burst of write (non-const) calls.
			typedef const std::shared_ptr<const _Interface> burst_r; // use if you are about to author a burst of read (const) calls.
			// *** END WARNING: using the above haphazardly can & will invalidate the inherent safety of the Proxy pattern. ***
		};
		// Non-Member comparison operations allow a proxy to behave like a standard shared_ptr when compared against each other
		template<typename... L, typename... R>
		bool operator==(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs == *_rhs;
		}
		template<typename... L, typename... R>
		bool operator!=(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs != *_rhs;
		}
		template<typename... L, typename... R>
		bool operator<(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs < *_rhs; // this operator required for associative container support
		}
		template<typename... L, typename... R>
		bool operator>(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs > *_rhs;
		}
		template<typename... L, typename... R>
		bool operator<=(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs <= *_rhs;
		}
		template<typename... L, typename... R>
		bool operator>=(const GProxy<L...>& _lhs, const GProxy<R...>& _rhs) noexcept
		{
			return *_lhs >= *_rhs;
		}
		// Nullptr comparison support
		template<typename... T>
		bool operator==(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return !(bool)_lhs;
		}
		template<typename... T>
		bool operator==(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return !(bool)_rhs;
		}
		template<typename... T>
		bool operator!=(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return (bool)_lhs;
		}
		template<typename... T>
		bool operator!=(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return (bool)_rhs;
		}
		template<typename... T>
		bool operator<(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return *_lhs < _rhs;
		}
		template<typename... T>
		bool operator<(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return _lhs < *_rhs;
		}
		template<typename... T>
		bool operator>(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return *_lhs > _rhs;
		}
		template<typename... T>
		bool operator>(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return _lhs > *_rhs;
		}
		template<typename... T>
		bool operator<=(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return *_lhs <= _rhs;
		}
		template<typename... T>
		bool operator<=(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return _lhs <= *_rhs;
		}
		template<typename... T>
		bool operator>=(const GProxy<T...>& _lhs, std::nullptr_t _rhs) noexcept
		{
			return *_lhs >= _rhs;
		}
		template<typename... T>
		bool operator>=(std::nullptr_t _lhs, const GProxy<T...>& _rhs) noexcept
		{
			return _lhs >= *_rhs;
		}
	} // end I internal namespace

	// Defines the simple type-erasure struct GEvent
	// This is a another core pillar used in the C++11 modernized version of Gateware.
	// A GEvent is a generic but type-safe way to communicate small-footprint event messages to listeners.
	// All sent messages must fit within G_MESSAGE_BLOCK_SIZE total including type_info hashes.
	// A GEvent only supports safe transfer of raw data blocks(C structs, POD) & enum class types.
	// (Copy)Constructors/Destructors/Assignment Operators etc... of stored types are NOT respected in any way!
	// Future versions (c++17+) of Gateware could replace some of this with the FAR more robust std::any if needed.

	//! Typesafe and Typeless communication of simplistic event data. Used by GEventGenerator & GEventReceiver. (Observer pattern)
	/*!
	*	This struct is how GEventGenerators communicate custom Events to any GEventReceiver who registers themselves to the object.
	*	This struct alongside std::function/std::bind is used to circumvent the traditional derivation based observer model.
	*	You can think of GEvent as a less-robust and more specialized version of std::any. (we currently only target c++11)
	*/
	struct GEvent
	{
	private:
		std::size_t enumType = 0; // identifies stored enumerator representing the event ID
		std::size_t dataType = 0; // identifies stored structure representing the associated data bundled with an event
		// raw data containing specific enum value & data packet.
		// The leading byte of each type-erased data block records the size of the embedded data.
		// This information is used for additional data integrity verification during reads and offset jumps when required.
		unsigned char rawData[G_RAW_DATA_PACKET_SIZE] = {0,};
		// due to odd include order issues across platforms regarding std::memcpy & std::copy
		// we provide a substitution here that copies a fixed amount of bytes
		static inline void memory_copy(void *_dst, const void* _src, unsigned int _byteCount)
		{
			std::size_t* write = static_cast<std::size_t*>(_dst);
			std::size_t const *read = static_cast<std::size_t const*>(_src);
			for (; _byteCount >= sizeof(std::size_t); _byteCount -= sizeof(std::size_t))
				*write++ = *read++;
			unsigned char* w = reinterpret_cast<unsigned char*>(write);
			unsigned char const* r = reinterpret_cast<unsigned char const*>(read);
			for (; _byteCount; --_byteCount)
				*w++ = *r++;
		}
		
	public:
		//! Allows writing of any enum class & raw data to a GEvent while retaining internal type hashes.(type-erasure)
		/*!
		*	DEVNOTE: Written event data must be <= to (G_MESSAGE_BLOCK_SIZE - 16) - sizeof(chosen enum class type).
		*	Only base type enums & raw data streams (C style structs) may be written as data (POD Plain Old Data)
		*	Each stored type may not exceed 255 bytes for size verification/offset reasons.
		*
		*	\param _eventEnumValue Enum used to represent which event has occurred.(any simple POD enum valid)
		*	\param _eventData Extra data that accompanies  this event.(any small POD works here)
		*
		*	\retval SUCCESS	Valid types were provided and the event data was successfully embedded.
		*	\retval UNEXPECTED_RESULT Encoded type matched the provided type hash but does not match its size in bytes.
		*/
		template<class enumT, typename dataT>
		GReturn Write(const enumT& _eventEnumValue, const dataT& _eventData)
		{
			// ensure developers stay within data size limits of storable data types
			static_assert(	sizeof(enumT) < 256, "Enum types stored by GEvent must be < 256 bytes!");
			static_assert(	sizeof(dataT) < 256, "Data types stored by GEvent must be < 256 bytes!");
			// ensure developers do not exceed internal storage limits
			static_assert(	(sizeof(enumT) + sizeof(dataT)) <= G_RAW_DATA_PACKET_SIZE,
							"Enum + Data combined size attempted to be stored by GEvent is too large!");
			// ensure developers do not send types other than an enum to identify their events
			static_assert(	std::is_enum<enumT>::value,
							"Gateware Events{} are required to use an \"enum class\" list to identify themselves!");
			// ensure developers use only C Style structures & basic enumerators for information transfer
			static_assert(	std::is_trivially_copyable<dataT>::value,
							"Enum & Data Types must be fully transferable with std:memcpy only! Plain Old Data Only!");
			// Once we know the storable types are safe we record their type and transfer the data
			enumType = typeid(enumT).hash_code();
			dataType = typeid(dataT).hash_code();
			// Unlikely, but the standard doesn't say it can't happen, would interfere with error checking
			if (enumType == 0 || dataType == 0)
				return GReturn::UNEXPECTED_RESULT;
			// Store bit data for typeless transfer
			rawData[0] = sizeof(enumT); // encode size of enum
			memory_copy(rawData + 1, &_eventEnumValue, sizeof(enumT)); // store enum
			rawData[sizeof(enumT) + 1] = sizeof(dataT); // encode size of data
			memory_copy(rawData + sizeof(enumT) + 2, &_eventData, sizeof(dataT)); // store data
			// transfer to internal storage should now be successful
			return GReturn::SUCCESS;
		};
		//! Safely extract G::Events and G::EVENT_DATA from a GEventGenerator if you know what type of information you need.
		/*!
		*	Once your GEventReceiver has responded to an event and is receiving a GEvent, you can use this operation
		*	to safely extract information about a specific event. Not only can you establish which event was generated,
		*	you can also receive detailed information about the event if that event contains additional data.
		*
		*	\param [out] _outEventID Variable used to capture which event has occurred.(See interface docs)
		*	\param [out] _outEventData Variable used to capture incoming data from an event.(See interface docs)
		*
		*	\retval SUCCESS	The proper types were provided and the event data was successfully extracted.
		*	\retval FAILURE	This GEvent was never initialized with any event data. (Dataless Event)
		*	\retval INVALID_ARGUMENT Provided template data types do NOT match internally stored hashes for this event.
		*	\retval MEMORY_CORRUPTION Encoded type matched the provided type hash but does not match its size in bytes.
		*/
		template<class enumT, typename dataT>
		GReturn Read(enumT& _outEventID, dataT& _outEventData) const
		{
			// If you get this compiler error you are providing Read() with an incompatible type.
			// Always use Gxxx::Events and Gxxx::EVENT_DATA to examine an event.
			static_assert((sizeof(enumT) + sizeof(dataT)) <= G_RAW_DATA_PACKET_SIZE &&
				std::is_enum<enumT>::value && std::is_trivially_copyable<enumT>::value &&
				std::is_trivially_copyable<dataT>::value,
				"Types provided to GEvent::Read() operation are not a compatible types!"
				"Use appropriate GEventGenerator derived G::Events enum and G::EVENT_DATA structure.");
			// Type hashes will ensure safe access of underlying data in 99.9% of situations
			// Not 100% perfect but much safer than a void* like it used to be.
			if (enumType == 0 || dataType == 0) return GReturn::FAILURE; // No event data present
			// Requested type does not match internal enum format (use proper G::Events)
			if (enumType != typeid(enumT).hash_code()) return GReturn::INVALID_ARGUMENT;
			// Requested type does not match internal data structure format (use proper G::EVENT_DATA)
			if (dataType != typeid(dataT).hash_code()) return GReturn::INVALID_ARGUMENT;
			// Once types have been safely checked we verify the byte sizes of types and copy data
			if (sizeof(enumT) == rawData[0])
				memory_copy(&_outEventID, rawData + 1, sizeof(enumT));
			else
				return GReturn::MEMORY_CORRUPTION; // data size verification failed
			// check that data type is also size verified before transfer
			if (sizeof(dataT) == rawData[sizeof(enumT) + 1])
				memory_copy(&_outEventData, rawData + sizeof(enumT) + 2, sizeof(dataT));
			else
				return GReturn::MEMORY_CORRUPTION; // data size verification failed
			// if we made it this far everything checked out and transferred successfully.
			return GReturn::SUCCESS;
		}
		//! Safely extract G::Events and G::EVENT_DATA from a GEventGenerator if you know what type of information you need.
		/*!
		*	Once your GEventReceiver has responded to an event and is receiving a GEvent, you can use this operation
		*	to safely extract information about a specific event. Not only can you establish which event was generated,
		*	you can also receive detailed information about the event if that event contains additional data.
		*
		*	\param [out] _outEventOrData Variable used to capture incoming data or event ID from an event.(See interface docs)
		*
		*	\retval SUCCESS	A proper type was provided and the information was successfully extracted.
		*	\retval FAILURE	This GEvent was never initialized with any information. (Uninitialized Event)
		*	\retval INVALID_ARGUMENT Provided template data type does NOT match any internal stored hash for this event.
		*	\retval MEMORY_CORRUPTION Encoded type matched the provided type hash but does not match its size in bytes.
		*/
		template<class enumOrDataT>
		GReturn Read(enumOrDataT& _outEventOrData) const
		{
			// If you get this compiler error you are providing Read() with an incompatible type.
			// Always use Gxxx::Events and Gxxx::EVENT_DATA to examine an event.
			static_assert(sizeof(enumOrDataT) <= G_RAW_DATA_PACKET_SIZE &&
				std::is_trivially_copyable<enumOrDataT>::value,
				"Type provided to GEvent::Read() operation is not a compatible type!"
				"Use appropriate GEventGenerator derived G::Events enum or G::EVENT_DATA structure.");
			// Type hashes will ensure safe access of underlying data in 99.9% of situations
			// Not 100% perfect but much safer than a void* like it used to be.
			if (enumType == 0 && dataType == 0)
				return GReturn::FAILURE; // No event data present
			// enum type identified
			else if (enumType == typeid(enumOrDataT).hash_code())
				if (sizeof(enumOrDataT) == rawData[0])
					memory_copy(&_outEventOrData, rawData + 1, sizeof(enumOrDataT)); // transfer enum bits
				else // data size verification failed
					return GReturn::MEMORY_CORRUPTION;
			// event data identified
			else if (dataType == typeid(enumOrDataT).hash_code())
				if (rawData[0] < (G_EVENT_BLOCK_SIZE - (sizeof(std::size_t) << 1)) && // encoded enum type integrity check
					sizeof(enumOrDataT) == rawData[rawData[0] + 1]) // jump to type size offset
					memory_copy(&_outEventOrData, rawData + rawData[0] + 2, sizeof(enumOrDataT)); // transfer enum bits
				else // data size verification failed
					return GReturn::MEMORY_CORRUPTION;
			else // Requested type does not match internal enum/data structure format (use proper G::Events/G::EVENT_DATA)
				return GReturn::INVALID_ARGUMENT;
			// with transfer complete notify success
			return GReturn::SUCCESS;
		}
	};
};

// Creates an identically named typedef in the proxy to represent a public type in the interface
// These are typically used by Event Generator Proxies for end user typing convenience
#define GATEWARE_TYPEDEF(interface_type) typedef forceRaw::interface_type interface_type;
// Returns if a proxy is empty or it is expired if this detection functionality is supported
// In Visual Studio you can add "/std:c++17 /Zc:__cplusplus" to your C++ command line for partial c++17 support
#if __cplusplus >= 201703L // This requires c++17 support
// Determines if an invalid proxy has expired or was never initialized in the first place.
#define GATEWARE_EXPIRED(weak_pointer) \
(!(weak_pointer).owner_before(std::weak_ptr<forceRaw>{}) && \
!std::weak_ptr<forceRaw>{}.owner_before((weak_pointer))) \
? GW::GReturn::EMPTY_PROXY : GW::GReturn::EXPIRED_PROXY
#else
// if we don't have support for c++17 we can't detect an expired proxy and fallback to empty.
#define GATEWARE_EXPIRED(weak_pointer) GW::GReturn::EMPTY_PROXY
#endif
// Writes a wrapper function to wrap the internal implementation call
// Uses a variadic template & perfect forwarding of arguments for ease of development & performance
#define GATEWARE_PROXY_FUNCTION(function_name, access_type, lock_type, cast_type) \
template<typename... Args> \
GW::GReturn \
function_name \
(Args&&... parameter) access_type { \
if(storage) return (cast_type(storage.get()))->function_name(std::forward<Args>(parameter)...);\
lock_type _proxy = access.lock(); \
return (_proxy) ? (cast_type(_proxy.get()))->function_name(std::forward<Args>(parameter)...) \
: GATEWARE_EXPIRED(access); \
}
#define GATEWARE_FUNCTION(function_name) GATEWARE_PROXY_FUNCTION(function_name, ,auto, )
#define GATEWARE_CONST_FUNCTION(function_name) GATEWARE_PROXY_FUNCTION(function_name,const,forceConst, )
// Template Proxy functions convert the base class pointer to its known implementation before
// Passing the list of arguments to the appropriate routine, convenient but not particularly efficient
#define GATEWARE_TEMPLATE_FUNCTION(function_name) \
GATEWARE_PROXY_FUNCTION(function_name, ,auto,dynamic_cast<forceStatic*>)
#define GATEWARE_CONST_TEMPLATE_FUNCTION(function_name) \
GATEWARE_PROXY_FUNCTION(function_name,const,forceConst,dynamic_cast<forceStatic*>)
// Static functions allow high speed calls with no reference juggling for things like math libraries.
// Yet they still allow such Proxys to operate like any other Proxy transparently
#define GATEWARE_STATIC_FUNCTION(function_name) \
template<typename... Args> \
static GW::GReturn \
function_name \
(Args&&... parameter) { \
return forceStatic::function_name(std::forward<Args>(parameter)...); \
}
// This macro ensures the derived proxy fully supports dynamic casting and transfer of proxy ownership
// ALL Gateware Proxy classes MUST have this in the public section of the derived proxy class.
#define GATEWARE_PROXY_CLASS(class_name) \
class_name() = default; \
class_name(std::shared_ptr<forceRaw> shr) : thisClass(shr) {} \
template<class Proxy> class_name(const Proxy& cpy) : thisClass(cpy) { } \
template<class Proxy> class_name(Proxy&& mov) noexcept : thisClass(std::forward<Proxy>(mov)) { } \
template<class Proxy> class_name& operator=(const Proxy& cpy) { thisClass::operator=(cpy); return *this; } \
template<class Proxy> class_name& operator=(Proxy&& mov) \
noexcept { thisClass::operator=(std::forward<Proxy>(mov)); return *this; } \
forceStatic* operator--(int) const { std::abort(); return nullptr; }; // The "--->" is ONLY for Intellisense Viewing

// Deprecates an entire class and displays a message recommending an alternative class.
// To use, replace: class Foo
// With: class GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo
#if defined(_MSC_VER)
#define GATEWARE_DEPRECATED(deprecated_class, replacing_class) \
__declspec(deprecated(#deprecated_class " is deprecated: use " #replacing_class " instead."))
#elif defined(__GNUC__)
// XCode and CodeLite will prepend a message indicating the class is deprecated.
#define GATEWARE_DEPRECATED(deprecated_class, replacing_class) \
__attribute__ ((deprecated("use " #replacing_class " instead.")))
#else
#define GATEWARE_DEPRECATED(deprecated_class, replacing_class)
#endif

// NOTE: The GATEWARE_DEPRECATED macro was originally designed for deprecating a class, but it could be used
// to deprecated other things. For example:
//
// Structs: 
//    struct GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo
// Typedefs: 
//    GATEWARE_DEPRECATED(Foo, FoosReplacement) GATEWARE_TYPEDEF(Foo)
//
// For deprecating functions, GATEWARE_DEPRECATED has to be placed inside the argument area of the function
// macro, before the function name. This is because GATEWARE_DEPRECATED has to be placed after the template<T>
// part of the function header. For example:
//
// Functions:
//    GATEWARE_FUNCTION(GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo)
//    GATEWARE_CONST_FUNCTION(GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo)
//    GATEWARE_PROXY_FUNCTION(GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo)
//    GATEWARE_TEMPLATE_FUNCTION(GATEWARE_DEPRECATED(Foo, FoosReplacement) Foo)
//
// Currently, there is no way to use GATEWARE_DEPRECATED with the GATEWARE_STATIC_FUNCTION macro. This is
// because GATEWARE_DEPRECATED has to be placed after the 'static' keyword. One solution for this would be
// to create a new macro for this specific case.


// If you are looking for binary build support, it has been dropped from the late Beta version of Gateware.
// As much as I wanted to embrace Libs & DLLs... C++11 functional additions, ease of deployment considerations,
// and simplicity for student developers both internal & external have prompted this decision.
// Obviously binaries have many advantages, but their drawbacks simply don't fit the future of this project.
// I spent many weeks trying to have my cake & eat it too, and theoretically it remains possible to support.
// However, the additional complexity and support needed for them was deemed too costly. -L.Norri Gatekeeper

#endif // GCOREDEFINES_H



