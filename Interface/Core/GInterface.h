#ifndef GINTERFACE_H
#define GINTERFACE_H

/*!
	File: GInterface.h
	Purpose: The fundamental interface which all Gateware interfaces must adhere to at a minimum.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: Ryan Powser
	Interface Status: Release
	Copyright: 7thGate Software LLC.
	License: MIT
*/

// Contains all defined elements shared among base interfaces.
#include "GCoreDefines.h"

namespace GW
{
	// The "I" namespace can be safely ignored by end users. I = ('I'nternal 'I'nterfaces & 'I'mplementations)
	namespace I // Doxygen ignores this namespace.
	{
		// Base interface all Gateware interfaces must support at a minimum.
		// The only purpose of this interface is to identify Gateware Objects, enable run-time polymorphism.
		class GInterfaceInterface
		{
			// All Gateware API interfaces contain no variables & are pure virtual.
		public:
			virtual ~GInterfaceInterface() = 0; // all interfaces must support vtables & RTTI 
		};
		inline GInterfaceInterface::~GInterfaceInterface() = default;
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GInterface/GInterface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proximity class that can be used to safely store and access ANY Gateware object.
		/*!
		*	The only purpose of this proxy is to store Gateware Objects and enable run-time polymorphism.
		*/
		class GInterface final // Note to developers: this is a Proxy, what end users actually interact with. 
		: public I::GProxy<I::GInterfaceInterface, I::GInterfaceImplementation>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GInterface)
			//! \endcond

		// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates a GInterface 
			/*!
			*	A GInterface has no real functionality on its own other than being able to be used to track object lifetime.
			*
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Shares ownership of a Proxy (if possible).
			/*!
			*	By default when proxies are copied they do not share ownership, this allows them to.
			*	After sharing, a Proxy retains shared ownership of memory.
			*
			*	\returns GInterface	An owned proxy, if this proxy does not own the object it will be invalid.
			*/
			GInterface Share() const;

			//! Gives ownership of one Proxy to another (if possible).
			/*!
			*	Allows a Proxy to yield its ownership to another Proxy.
			*	If a Proxy does not have ownership of its memory then it can not yield it.
			*	After a successful yield this Proxy will still retain access but not memory ownership. 
			*
			*	\returns GInterface	Proxy owning internal memory or an invalid handle if it cannot be yielded.
			*/
			GInterface Relinquish();

			//! Copies a Proxy while retaining it's ownership state.
			/*!
			*	Allows a Proxy to be Shared but unlike "Share()" it will not fail if the object is not owned.
			*	An unowned Proxy will transfer normally while an owned proxy will share ownership.
			*
			*	\returns GInterface	Proxy referencing same object with same ownership model preserved.
			*/
			GInterface Mirror() const;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GINTERFACE_H