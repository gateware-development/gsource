#ifndef GEVENTQUEUE_H
#define GEVENTQUEUE_H

/*!
	File: GEventQueue.h
	Purpose: A Gateware interface that allows anyone to receive & store events from a GEventGenerator or derived class.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GEventReceiver.h"

namespace GW
{
	namespace I
	{
		class GEventQueueInterface : public virtual GEventReceiverInterface
		{
		public:
			// Let the compiler know that we intend to use the base class method as well as an override with an additional parameter
			using GEventReceiverInterface::Peek;
			// new methods
			virtual GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const = 0; // override
			virtual GReturn Max(unsigned int& _outSize) const = 0;

			// we internally override Queue template "Find" functions to search whole queue
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GEventQueue/GEventQueue.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy class that can be used to record multiple Events for later inspection. 
		/*!
		*	Receives and stores all events produced by an interface derived from GEventGenerator.
		*	You must provide the EventQueue a maximum storage capacity as the underlying container is a circular queue. 
		*	Accepts an std::function<void()> as the callback function/lambda for flexibility.
		*	Use std::bind to supply your own arguments to your callback, the only restriction is the void return type.
		*	Because all events are captured (assuming adequate storage) you may process events later when ready to do so.
		*	\code{.cpp}
		*		MyEventQueue.Create( 16, MyEventGenerator, nullptr); // if you only need the last event, use GEventResponder
		*		GEvent g; 
		*		GEventGenerator::Events e; // replace with desired Generator
		*		GEventGenerator::EVENT_DATA d; 
		*		while(+MyEventQueue.Pop(g)) // later, possibly in main loop...
		*			if(+g.Read(e,d))
		*				if(e == GEventGenerator::Events::NON_EVENT) // replace with actual event
		*					std::cout << d.noData; // replace with actual data
		*	\endcode
		*	\deprecated **Replaced by GW::CORE::GEventCache.**
		*/
		class  GEventQueue final
			: public I::GProxy<I::GEventQueueInterface, I::GEventQueueImplementation, unsigned int, GEventGenerator, const std::function<void()>>
		{
			// End users please feel free to ignore this struct, it is temporary and only used for internal API wiring.
			struct init_callback {
				init_callback() {
					internal_gw::event_receiver_callback = internal_gw::event_receiver_logic<GEventReceiver>;
				}
			}init; // hopefully your compiler will optimize this out

		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GEventQueue)
			GATEWARE_CONST_FUNCTION(Peek)
			GATEWARE_CONST_FUNCTION(Max)
			
			// reimplemented 
			GATEWARE_FUNCTION(Append)
			GATEWARE_CONST_FUNCTION(Waiting)
			GATEWARE_FUNCTION(Pop)
			GATEWARE_CONST_FUNCTION(Missed)
			GATEWARE_FUNCTION(Clear)
			GATEWARE_CONST_FUNCTION(Invoke)
			GATEWARE_TEMPLATE_FUNCTION(Find)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GEventQueue. 
			/*!
			*	A GEventQueue is capable of receiving and reacting to events created by a GEventGenerator.
			*	You can use this class whenever there is a Gateware class that spawns events you wish to know about.
			*	Unlike GEventReceiver the GEventQueue is capable of storing multiple events up to a limit defined by you.
			*	If you need to process events on a specific thread, you can use this class to delay processing until
			*	a safe time to do so. Just make sure there is adequate capacity in the Queue, and "Pop" them off when ready.
			*	
			*	\param [in] _maxSize		The maximum amount of messages that can be retained by this instance. (must be > 0)
			*								Exceeding this amount without processing a message will cause the oldest to be missed.
			*	\param [in] _eventSource	The GEventGenerator you wish to receive messages from.
			*	\param [in] _callback		The function or lambda you wish to be invoked when an event is received.
			*								(optional, can pass nullptr). May be invoked from a different thread.
			*								To ensure you receive messages on a specific thread, just use "Pop/Peek" instead. 
			*
			*	\retval GReturn::UNEXPECTED_RESULT	Internal API failure, should not happen please report this.
			*	\retval GReturn::INVALID_ARGUMENT	A valid GEventGenerator and Queue Capacity > 0 must be provided.
			*	\retval GReturn::SUCCESS	The receiver was correctly bound and registered to the generator provided.
			*/
			GReturn Create(unsigned int _maxSize, CORE::GEventGenerator _eventSource, const std::function<void()> _callback);

			//! Examines but does not remove a waiting event if one is available based on it's historical index. 
			/*!
			*	Same operation as Peek() in GEventReceiver but can also be used to examine the Queue's current backlog.
			*
			*	\param [in] _eventIndex	The event you wish to examine from 0 to "Waiting"-1, with 0 being the most recent.
			*	\param [out] _outEvent A copy of the event at _eventIndex waiting it's turn to be processed.
			*
			*	\retval GReturn::INVALID_ARGUMENT	The provided _eventIndex was out of bounds.
			*	\retval GReturn::FAILURE	There were no waiting events that could be inspected.
			*	\retval GReturn::SUCCESS	Successfully copied a waiting event from the Queue.
			*/
			virtual GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const = 0;

			//! Reports the Maximum amount of Events that this GEventQueue instance can record.  
			/*!
			*	This returns the same number that is passed to the GEventQueue "Create" function.
			*	Any events recorded while the Queue is at capacity will cause the oldest event to be lost.
			*	Whenever an Event is replaced without processing, it will increase the internal "Missed" count.
			*
			*	\param [out] _outCapacity The maximum capacity of the Event Queue.
			*
			*	\retval GReturn::SUCCESS	The maximum capacity was successfully reported.
			*/
			virtual GReturn Max(unsigned int& _outCapacity) const = 0;

#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GEVENTQUEUE_H
