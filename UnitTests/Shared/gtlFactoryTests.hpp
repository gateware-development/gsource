#if defined(GATEWARE_ENABLE_CORE)

#include "../../Source/Shared/gtl/factory.h"

TEST_CASE("Create a Factory of ints", "[Shared]")
{
	int arrA[6] = { 1, 2, 3, 4, 5, 6 };
	int arrB[4] = { 1, 6, 5, 4};
	gtl::factory<int> factory;

	SECTION("Default state of a Factory")
	{
		//all values are defined in Factory.h's "Create" function
		REQUIRE(factory.size() == 0);
		REQUIRE(factory.capacity() == 2); //capacity is initialized to 2

		factory.remove(0);
		REQUIRE(factory.size() == 0);
		REQUIRE(factory.capacity() == 2);
	}

	SECTION("Adding and Removing variables and checking storage")
	{
		REQUIRE(factory.push_back(1) == 0); //returns "next_free" 
		REQUIRE(factory[0] == 1);
		REQUIRE(factory.size() == 1);

		REQUIRE(factory.push_back(8) == 1);
		REQUIRE(factory[1] == 8);
		REQUIRE(factory.size() == 2);
		REQUIRE(factory.capacity() == 4); //expand called

		REQUIRE(factory.push_back(4) == 2);
		REQUIRE(factory[2] == 4);
		REQUIRE(factory.size() == 3);

		factory.pop_back();

		REQUIRE(factory.push_back(4) == 2);
		REQUIRE(factory[2] == 4);
		REQUIRE(factory.size() == 3);

		//remove the beginning of the factory array and test Router
		factory.remove(0);
		REQUIRE(factory[2] == 4);
		REQUIRE(*factory.begin() == 4);
		REQUIRE(factory.front() == 4);
		REQUIRE(factory.begin());
		REQUIRE(factory.back() == 8);

		factory.clear();
	}

	SECTION("Iterate through a factory")
	{
		for (int i = 0; i < 5; i++) //add some variables!
		{
			REQUIRE(factory.push_back(i) == i);
		}

		
		for (gtl::factory<int>::iterator iter = factory.begin(); iter != factory.end(); iter++) //PostInc
		{
			REQUIRE(iter);
		}
		
		for (gtl::factory<int>::iterator iter = factory.begin(); iter != factory.end(); ++iter) //PreInc
		{
			REQUIRE(iter);
		}

		for (gtl::factory<int>::iterator iter = factory.end(); iter != factory.begin(); iter--)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<int>::iterator iter = factory.end(); iter != factory.begin(); --iter)
		{
			REQUIRE(iter);
		}
	}

	SECTION("While Loop Iterate through a factory")
	{
		for (int i = 0; i < 100000; i++) //add some variables!
		{
			REQUIRE(factory.push_back(1) == i);
		}
		gtl::factory<int>::iterator iter = factory.begin();
		int result = 0;
		while (iter != factory.end())
		{
			result += *iter;
			++iter;
		}
		REQUIRE(result == factory.size());
		//factory.pop_back();
		factory.remove(0);
		result = 0;
		iter = factory.begin();
		auto b = factory.end();
		while (iter != factory.end())
		{
			result += *iter;
			REQUIRE(iter);
			iter++;
		}
		REQUIRE(result == factory.size());
	}

	SECTION("const_iterator Iterate through a factory")
	{
		for (int i = 0; i < 5; i++) //add some variables!
		{
			REQUIRE(factory.push_back(i) == i);
		}

		//iterate through a factory (forward)
		for (gtl::factory<int>::const_iterator iter = factory.cbegin(); iter != factory.cend(); iter++)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<int>::const_iterator iter = factory.cbegin(); iter != factory.cend(); ++iter)
		{
			REQUIRE(iter);
		}
		//(backward)
		for (gtl::factory<int>::const_iterator iter = factory.cend(); iter != factory.cbegin(); iter--)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<int>::const_iterator iter = factory.cend(); iter != factory.cbegin(); --iter)
		{
			REQUIRE(iter);
		}
	}

	SECTION("Remove, Iterate, and shrink")
	{
		for (int i = 0; i < 6; i++)
		{
			REQUIRE(factory.push_back(arrA[i]) == i);
		}
		auto end = factory.end();
		for (auto iter = factory.begin(); iter != factory.end(); iter++)
		{
			REQUIRE(*iter == arrA[static_cast<int>(iter)]);
		}

		factory.remove(1);
		factory.remove(2);
		factory.shrink();
		//factory.Align();

		/*for (auto iter = factory.begin(); iter != factory.end(); iter++)
		{
			REQUIRE(*iter == arrB[static_cast<int>(iter)]);
			REQUIRE(*iter == factory[static_cast<int>(iter)]);
		}*/

		factory.remove(3);
		factory.reserve(3);
		REQUIRE(factory.capacity() == 3);
		factory.reserve(6);
		REQUIRE(factory.capacity() == 6);
	}

	SECTION("Constructor tests")
	{
		for (int i = 0; i < 6; i++)
		{
			REQUIRE(factory.push_back(arrA[i]) == i);
		}

		gtl::factory<int> clone = factory;
		REQUIRE(*factory.begin() == *clone.begin()); //clone creates a copy but at a different memory address
		REQUIRE(factory.begin() != clone.begin());
		REQUIRE(*(factory.end()-1) == *(clone.end()-1));
		REQUIRE(factory.end() != clone.end());

		REQUIRE(*factory.cbegin() == *clone.cbegin());
		REQUIRE(factory.cbegin() != clone.cbegin());
		REQUIRE(factory.cend() != clone.cend());

		gtl::factory<int> cloneB;
		cloneB = std::move(factory);
	}
	SECTION("Delete odd numbers test")
	{
		int results = 0;
		for (int i = 0; i < 100; i++)
		{
			REQUIRE(factory.push_back(i) == i);
		}

		for (auto iter = factory.begin(); iter != factory.end(); iter++)
		{		
			if ((iter.operator int() % 2) == 1)
				iter = factory.erase(iter);

			results++;
		}
		REQUIRE(factory.size() == 67);
	}
	SECTION("Test data() function")
	{
		int results = 0;
		for (int i = 0; i < 100; i++)
		{
			REQUIRE(factory.push_back(i) == i);
		}

		int* testArray = factory.data();

		for (int i = 0; i < 101; i++)
		{
			REQUIRE(testArray[i] == factory.data()[i]);
		}

		testArray[5] = 55;

		REQUIRE(factory.data()[5] == 55);

		int testInt = *(factory.end() - 1);

		factory.remove(7);

		REQUIRE(testArray[7] == testInt);
	}
}

TEST_CASE("Factory of non Fixed Size objects", "[Shared]")
{	
	std::string strA("Hello There!");
	std::string strB("Hey! Long time no see!");
	std::string strC("Beautiful day, isn't it?");
	std::string strD(" Beautiful day indeed!");
	std::string stringArr[4] = { strA, strB, strC, strD };
	std::string stringArrB[2] = { strD, strC};
    gtl::factory<std::string> stringFactory;

	SECTION("Default state of a Factory")
	{
		//all values are defined in Factory.h's "Create" function
		REQUIRE(stringFactory.size() == 0); //size returns size -1
		REQUIRE(stringFactory.capacity() == 2); //capacity is initialized to 2
		stringFactory.remove(0);
		REQUIRE(stringFactory.size() == 0); //should be unaffected by the remove since nothing was there
		REQUIRE(stringFactory.capacity() == 2);
	}

	SECTION("Add, Modify, and Remove Variables")
	{
		REQUIRE(stringFactory.push_back(strA) == 0);
		REQUIRE(stringFactory.push_back(strB) == 1);
		REQUIRE(stringFactory.push_back(strC) == 2);

		stringFactory[0].append(strD);
		////does changing the size of a string affect adjacent strings? (It shouldn't!)
		REQUIRE(stringFactory[0] == (strA + strD));
		REQUIRE(stringFactory[1] == strB);

		stringFactory.remove(0);
		REQUIRE(stringFactory.push_back("User 1 Disconnected") == 0);
		REQUIRE(stringFactory[2] == strC);
		REQUIRE(*stringFactory.begin() == strC);

		stringFactory[0].erase(stringFactory[0].begin(), stringFactory[0].end());
		REQUIRE(stringFactory[0] == "");
	}

	SECTION("Iterate through a factory")
	{
		for (int i = 0; i < 4; i++) //add some variables!
		{
			REQUIRE(stringFactory.push_back(stringArr[i]) == i);
		}

		for (gtl::factory<std::string>::iterator iter = stringFactory.begin(); iter != stringFactory.end(); iter++) //PostInc
		{
			REQUIRE(iter);
		}

		for (gtl::factory<std::string>::iterator iter = stringFactory.begin(); iter != stringFactory.end(); ++iter) //PreInc
		{
			REQUIRE(iter);
		}

		for (gtl::factory<std::string>::iterator iter = stringFactory.end(); iter != stringFactory.begin(); iter--)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<std::string>::iterator iter = stringFactory.end(); iter != stringFactory.begin(); --iter)
		{
			REQUIRE(iter);
		}
	}

	SECTION("const_iterator Iterate through a factory")
	{
		for (int i = 0; i < 4; i++) //add some variables!
		{
			REQUIRE(stringFactory.push_back(stringArr[i]) == i);
		}

		//iterate through a factory (forward)
		for (gtl::factory<std::string>::const_iterator iter = stringFactory.cbegin(); iter != stringFactory.cend(); iter++)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<std::string>::const_iterator iter = stringFactory.cbegin(); iter != stringFactory.cend(); ++iter)
		{
			REQUIRE(iter);
		}
		//(backward)
		for (gtl::factory<std::string>::const_iterator iter = stringFactory.cend(); iter != stringFactory.cbegin(); iter--)
		{
			REQUIRE(iter);
		}

		for (gtl::factory<std::string>::const_iterator iter = stringFactory.cend(); iter != stringFactory.cbegin(); --iter)
		{
			REQUIRE(iter);
		}
	}

	SECTION("Remove, Iterate, and shrink")
	{
		for (int i = 0; i < 4; i++)
		{
			REQUIRE(stringFactory.push_back(stringArr[i]) == i);
		}

		for (auto iter = stringFactory.begin(); iter != stringFactory.end(); iter++)
		{
			REQUIRE(*iter == stringArr[static_cast<int>(iter)]);
		}

		stringFactory.remove(0);
		stringFactory.remove(1);
		stringFactory.shrink(); //this corrupts the strings???
		//stringFactory.Align();

		/*for (auto iter = stringFactory.begin(); iter != stringFactory.end(); iter++)
		{
			REQUIRE(*iter == stringArrB[static_cast<int>(iter)]);
			REQUIRE(*iter == stringFactory[static_cast<int>(iter)]);
		}*/

		stringFactory.reserve(3);
		REQUIRE(stringFactory.capacity() == 3);
		stringFactory.reserve(6);
		REQUIRE(stringFactory.capacity() == 6);
	}

	SECTION("Constructor tests")
	{
		for (int i = 0; i < 4; i++)
		{
			REQUIRE(stringFactory.push_back(stringArr[i]) == i);
		}

		gtl::factory<std::string> clone = stringFactory;
		REQUIRE(*stringFactory.begin() == *clone.begin());
		REQUIRE(stringFactory.begin() != clone.begin());
		REQUIRE(*stringFactory.end() == *clone.end());
		REQUIRE(stringFactory.end() != clone.end());

		REQUIRE(*stringFactory.cbegin() == *clone.cbegin());
		REQUIRE(stringFactory.cbegin() != clone.cbegin());
		REQUIRE(*stringFactory.cend() == *clone.cend());
		REQUIRE(stringFactory.cend() != clone.cend());

		clone[0].append(" Clone 1");

		gtl::factory<std::string> cloneMove;
		cloneMove = std::move(stringFactory);
	}
}
#endif