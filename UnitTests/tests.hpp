///----------Testing Utilities--------------
#include "utils.hpp"

///----------Core Tests--------------
#include "Core/GInterfaceTest.hpp"
#include "Core/GThreadSharedTest.hpp"
#include "Core/GEventGeneratorTest.hpp"
#include "Core/GEventReceiverTest.hpp"
#include "Core/GEventQueueTest.hpp"
#include "Core/GEventTest.hpp"
#include "Core/GLogicTest.hpp"
#include "Core/GEventCacheTest.hpp"
#include "Core/GEventResponderTest.hpp"

///----------Shared Tests--------------
#include "Shared/gtlFactoryTests.hpp"

///----------System Tests--------------
#include "System/GConcurrentTest.hpp"
#include "System/GDaemonTest.hpp"
#include "System/GFileTest.hpp"
#include "System/GLogTest.hpp"
#include "System/GWindowTest.hpp"
#include "System/GAppTest.hpp"

///----------Math Tests--------------
#include "Math/GVectorTest.hpp"
#include "Math/GQuaternionTest.hpp"
#include "Math/GMatrixTest.hpp"
#include "Math/GCollisionTest.hpp"

///----------Math2D Tests--------------
#include "Math2D/GVector2DTest.hpp"
#include "Math2D/GMatrix2DTest.hpp"
#include "Math2D/GCollision2DTest.hpp"

///----------Audio Tests-------------
#include "Audio/GAudioTest.hpp"
#include "Audio/GAudio3DTest.hpp"

///----------Graphics Tests----------
#include "Graphics/GDirectX11SurfaceTest.hpp"
#include "Graphics/GDirectX12SurfaceTest.hpp"
#include "Graphics/GOpenGLSurfaceTest.hpp"
#include "Graphics/GRasterSurfaceTest.hpp"
#include "Graphics/GBlitterTest.hpp"
#include "Graphics/GVulkanSurfaceTest.hpp"

///----------Input Tests-------------
#include "Input/GInputTest.hpp"
#include "Input/GBufferedInputTest.hpp"
#include "Input/GControllerTest.hpp"

