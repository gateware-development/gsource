#if defined(GATEWARE_ENABLE_AUDIO) && !defined(GATEWARE_DISABLE_GAUDIO) && !defined(GATEWARE_DISABLE_GMUSIC) && !defined(GATEWARE_DISABLE_GSOUND)

#include "Resources.h"
#include <thread>
#include "../../Interface/System/GFile.h"

/*
All the sounds:
testSong, testBeep, testLaser, beep, stereo5_1Test
frontLeft, frontCenter, frontRight,
surroundLeft, bass, surroundRight,
masterVolume1, masterVolume_5, volume1, volume_5,
musicPause, musicResume, musicPlay,
soundPause, soundStop, soundResume, soundPlay
*/

/// If appropriate TESTS macro is defined, the tests would run for that section
#define G_SOUND_TESTS
#define G_MUSIC_TESTS
#define G_AUDIO_COMPOSITE_TESTS

#if 0 // enable for very fast audio tests
    #define GSLEEP_FOR(g_time_ms) std::this_thread::sleep_for(std::chrono::milliseconds(100))
#else
    #define GSLEEP_FOR(g_time_ms) std::this_thread::sleep_for(std::chrono::milliseconds(g_time_ms))
#endif

#define G_FRONT_LEFT 0
#define G_FRONT_RIGHT 1
#define G_FRONT_CENTER 2
#define G_SURROUND_CENTER 3
#define G_SURROUND_LEFT 4
#define G_SURROUND_RIGHT 5

bool EnabledirBuffer = false, AudioTests = false;
TEST_CASE("Check for Audio Hardware in GAudio", "[Audio]")
{
    GW::AUDIO::GAudio audio;
    if (audio.Create() != GW::GReturn::HARDWARE_UNAVAILABLE)
    {
        EnabledirBuffer = true;
        AudioTests = true;
        std::cout << "AUDIO HARDWARE DETECTED: RUNNING ALL AUDIO UNIT TESTS" << std::endl;
    }
    else
        std::cout << "WARNING: AUDIO HARDWARE NOT DETECTED: SKIPPING ALL AUDIO UNIT TESTS" << std::endl;
}

TEST_CASE("Creating audio", "[CreateGAudio], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;

    REQUIRE(+testAudio.Create());
    testAudio = nullptr;
    REQUIRE(testAudio.SetMasterVolume(1.0f) == GW::GReturn::EMPTY_PROXY);
}
//
//TEST_CASE("Setting sound channel volumes", "[SetChannelVolumes]")
//{
//    if (EnableAudioTests == false)
//    {
//        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
//        return; // ignore this test
//    }
//    GW::AUDIO::GAudio testAudio;
//    testAudio.Create();
//    float testvolumes[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
//
//    SECTION("Front Channels")
//    {
//        GW::AUDIO::GSound soundFrontRight;
//        GW::AUDIO::GSound soundFrontCenter;
//        GW::AUDIO::GSound soundFrontLeft;
//
//        soundFrontLeft.Create(PATH(AudioTest::frontLeft, testAudio);
//        testvolumes[G_FRONT_LEFT] = 1.0f;
//        REQUIRE(+soundFrontLeft.SetChannelVolumes(testvolumes, 6));
//        soundFrontLeft.Play();
//        GSLEEP_FOR(500);
//        testvolumes[G_FRONT_LEFT] = 0.0f;
//        soundFrontLeft = nullptr;
//
//        soundFrontCenter.Create(PATH(AudioTest::frontCenter, testAudio);
//        testvolumes[G_FRONT_CENTER] = 1.0f;
//        REQUIRE(+soundFrontCenter.SetChannelVolumes(testvolumes, 6));
//        soundFrontCenter.Play();
//        GSLEEP_FOR(500);
//        testvolumes[G_FRONT_CENTER] = 0.0f;
//        soundFrontCenter = nullptr;
//
//        soundFrontRight.Create(PATH(AudioTest::frontRight, testAudio);
//        testvolumes[G_FRONT_RIGHT] = 1.0f;
//        REQUIRE(+soundFrontRight.SetChannelVolumes(testvolumes, 6));
//        soundFrontRight.Play();
//        GSLEEP_FOR(500);
//        testvolumes[G_FRONT_RIGHT] = 0.0f;
//        soundFrontRight = nullptr;
//    }
//
//    SECTION("Surround/Back Channels")
//    {
//        GW::AUDIO::GSound soundSurroundRight;
//        GW::AUDIO::GSound soundSubwoofer;
//        GW::AUDIO::GSound soundSurroundLeft;
//
//        soundSurroundLeft.Create(PATH(AudioTest::surroundLeft, testAudio);
//        testvolumes[G_SURROUND_LEFT] = 1.0f;
//        REQUIRE(+soundSurroundLeft.SetChannelVolumes(testvolumes, 6));
//        soundSurroundLeft.Play();
//        GSLEEP_FOR(500);
//        testvolumes[G_SURROUND_LEFT] = 0.0f;
//        soundSurroundLeft = nullptr;
//
//        soundSubwoofer.Create(PATH(AudioTest::bass, testAudio);
//        testvolumes[G_SURROUND_CENTER] = 1.0f;
//        REQUIRE(+soundSubwoofer.SetChannelVolumes(testvolumes, 6));
//        soundSubwoofer.Play();
//        GSLEEP_FOR(250);
//        testvolumes[G_SURROUND_CENTER] = 0.0f;
//        soundSubwoofer = nullptr;
//
//        soundSurroundRight.Create(PATH(AudioTest::surroundRight, testAudio);
//        testvolumes[G_SURROUND_RIGHT] = 1.0f;
//        REQUIRE(+soundSurroundRight.SetChannelVolumes(testvolumes, 6));
//        soundSurroundRight.Play();
//        GSLEEP_FOR(500);
//        testvolumes[G_SURROUND_RIGHT] = 0.0f;
//        soundSurroundRight = nullptr;
//    }
//
//    // Fail cases
//    GW::AUDIO::GSound testSound;
//    testSound.Create(PATH(AudioTest::testBeep, testAudio);
//
//    REQUIRE(testSound.SetChannelVolumes(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
//    REQUIRE(testSound.SetChannelVolumes(testvolumes, -1) == GW::GReturn::INVALID_ARGUMENT);
//}

//TEST_CASE("TEST")
//{
//    GW::AUDIO::GAudio testAudio;
//    testAudio.Create();
//    float testvolumes[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
//
//    GW::AUDIO::GSound soundFrontRight;
//
//    soundFrontRight.Create(PATH(AudioTest::frontLeft, testAudio);
//    testvolumes[G_FRONT_RIGHT] = 1.0f;
//    REQUIRE(+soundFrontRight.SetChannelVolumes(testvolumes, 2));
//    soundFrontRight.Play();
//    GSLEEP_FOR(1000);
//    testvolumes[G_FRONT_LEFT] = 1.0f;
//    REQUIRE(+soundFrontRight.SetChannelVolumes(testvolumes, 2));
//        soundFrontRight.Play();
//    GSLEEP_FOR(1000);
//    testvolumes[G_FRONT_RIGHT] = 0.0f;
//    REQUIRE(+soundFrontRight.SetChannelVolumes(testvolumes, 2));
//        soundFrontRight.Play();
//    GSLEEP_FOR(1000);
//        soundFrontRight.Play();
//    GSLEEP_FOR(1000);
//        soundFrontRight.Play();
//    GSLEEP_FOR(1000);
//    soundFrontRight = nullptr;
//}

// Starting Sound Tests
#ifdef G_SOUND_TESTS
TEST_CASE("Creating sound", "[Create], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GAudio testAudio;

   
    
    testAudio.Create();

    // Fail cases
    REQUIRE(testSound.Create(nullptr, testAudio) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testSound.Create(AudioTest::testBeep, testSound) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testSound.Create(AudioTest::testBeep, testAudio));
}

TEST_CASE("Creating sound with 0 volume", "[Create], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();

    // Fail case
    REQUIRE(testSound.Create(AudioTest::testBeep, testAudio, -1.0f) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testSound.Create(AudioTest::testBeep, testAudio, 0.0f));
}

TEST_CASE("Playing sound", "[Play], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::soundPlay, testAudio);

    REQUIRE(+testSound.Play());
    REQUIRE(+testSound.Play());
    GSLEEP_FOR(500);

    testAudio = nullptr;
    REQUIRE(testSound.Play() == GW::GReturn::PREMATURE_DEALLOCATION);
}

TEST_CASE("Pausing sound", "[Pause], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::soundPause, testAudio);

    testSound.Play();
    GSLEEP_FOR(500);
    REQUIRE(+testSound.Pause());
    GSLEEP_FOR(200);
}

TEST_CASE("Resuming sound", "[Resume], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::soundResume, testAudio);

    testSound.Play();
    testSound.Pause();

    REQUIRE(+testSound.Resume());
    GSLEEP_FOR(1000);
}

TEST_CASE("Setting sound channel volumes", "[SetChannelVolumes], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();
    float testvolumes[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

    SECTION("Front Channels")
    {
        GW::AUDIO::GSound soundFrontRight;
        GW::AUDIO::GSound soundFrontCenter;
        GW::AUDIO::GSound soundFrontLeft;

        soundFrontLeft.Create(AudioTest::frontLeft, testAudio);
        testvolumes[G_FRONT_LEFT] = 1.0f;
        REQUIRE(+soundFrontLeft.SetChannelVolumes(testvolumes, 6));
        soundFrontLeft.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_LEFT] = 0.0f;
        soundFrontLeft = nullptr;

        soundFrontCenter.Create(AudioTest::frontCenter, testAudio);
        testvolumes[G_FRONT_CENTER] = 1.0f;
        REQUIRE(+soundFrontCenter.SetChannelVolumes(testvolumes, 6));
        soundFrontCenter.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_CENTER] = 0.0f;
        soundFrontCenter = nullptr;

        soundFrontRight.Create(AudioTest::frontRight, testAudio);
        testvolumes[G_FRONT_RIGHT] = 1.0f;
        REQUIRE(+soundFrontRight.SetChannelVolumes(testvolumes, 6));
        soundFrontRight.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_RIGHT] = 0.0f;
        soundFrontRight = nullptr;
    }

    SECTION("Surround/Back Channels")
    {
        GW::AUDIO::GSound soundSurroundRight;
        GW::AUDIO::GSound soundSubwoofer;
        GW::AUDIO::GSound soundSurroundLeft;

        soundSurroundLeft.Create(AudioTest::surroundLeft, testAudio);

        testvolumes[G_SURROUND_LEFT] = 1.0f;
        REQUIRE(+soundSurroundLeft.SetChannelVolumes(testvolumes, 6));
        soundSurroundLeft.Play();
        GSLEEP_FOR(500);
        testvolumes[G_SURROUND_LEFT] = 0.0f;
        soundSurroundLeft = nullptr;

        soundSubwoofer.Create(AudioTest::bass, testAudio);

        testvolumes[G_SURROUND_CENTER] = 1.0f;
        REQUIRE(+soundSubwoofer.SetChannelVolumes(testvolumes, 6));
        soundSubwoofer.Play();
        GSLEEP_FOR(250);
        testvolumes[G_SURROUND_CENTER] = 0.0f;
        soundSubwoofer = nullptr;

        soundSurroundRight.Create(AudioTest::surroundRight, testAudio);

        testvolumes[G_SURROUND_RIGHT] = 1.0f;
        REQUIRE(+soundSurroundRight.SetChannelVolumes(testvolumes, 6));
        soundSurroundRight.Play();
        GSLEEP_FOR(500);
        testvolumes[G_SURROUND_RIGHT] = 0.0f;
        soundSurroundRight = nullptr;
    }

    // Fail cases
    GW::AUDIO::GSound testSound;

    testSound.Create(AudioTest::testBeep, testAudio);

    REQUIRE(testSound.SetChannelVolumes(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testSound.SetChannelVolumes(testvolumes, -1) == GW::GReturn::INVALID_ARGUMENT);
}

TEST_CASE("Setting sound volume", "[SetVolume], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::volume_5, testAudio);

	// Fail cases
	REQUIRE(testSound.SetVolume(-1.0f) == GW::GReturn::INVALID_ARGUMENT);
	// Pass cases
	REQUIRE(+testSound.SetVolume(0.25f));

	testSound.Play();
    GSLEEP_FOR(1000);
}

TEST_CASE("Stopping sound", "[Stop], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::soundStop, testAudio);

	testSound.Play();
    GSLEEP_FOR(1250);

	// Pass cases
	REQUIRE(+testSound.Stop());
    GSLEEP_FOR(200);
}

TEST_CASE("Stopping sound after pausing", "[Stop], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    testAudio.Create();

    testSound.Create(AudioTest::soundPause, testAudio);

	testSound.Play();
	testSound.Pause();

	REQUIRE(+testSound.Stop());
}
#endif // end of Sound tests

// Starting Music Tests
#ifdef G_MUSIC_TESTS
TEST_CASE("Creating music", "[Create], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GMusic testMusic;
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();

    // Fail cases
    REQUIRE(testMusic.Create(nullptr, testAudio) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testMusic.Create(AudioTest::testSong, testMusic) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testMusic.Create(AudioTest::testSong, testAudio));
}

TEST_CASE("Creating music with 0 volume", "[Create], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GMusic testMusic;
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();

    // Fail cases
    REQUIRE(testMusic.Create(AudioTest::testSong, testAudio, -1.0f) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testMusic.Create(AudioTest::testSong, testAudio, 0.0f));
}

TEST_CASE("Playing music stream", "[Play], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();
    testMusic.Create(AudioTest::musicPlay, testAudio);

	REQUIRE(+testMusic.Play());
    REQUIRE(+testMusic.Play());
    GSLEEP_FOR(1000);
    testAudio = nullptr;
    REQUIRE(testMusic.Play() == GW::GReturn::PREMATURE_DEALLOCATION);
}

TEST_CASE("Looping music stream", "[Play], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();
    testMusic.Create(AudioTest::testBeep, testAudio);

    REQUIRE(+testMusic.Play(true));
    GSLEEP_FOR(1000);
}

TEST_CASE("Pausing music stream", "[Pause], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();
    testMusic.Create(AudioTest::musicPause, testAudio);

    testMusic.Play();
    GSLEEP_FOR(1200);

	REQUIRE(+testMusic.Pause());
    GSLEEP_FOR(300);
}

TEST_CASE("Resuming music stream", "[Resume], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();
    testMusic.Create(AudioTest::musicResume, testAudio);

	testMusic.Play();
	testMusic.Pause();

	// Pass cases
	REQUIRE(+testMusic.Resume());
    GSLEEP_FOR(1100);
}

TEST_CASE("Setting music channel volumes", "[SetChannelVolumes], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();
    float testvolumes[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

    SECTION("Front Channels")
    {
        GW::AUDIO::GMusic musicFrontRight;
        GW::AUDIO::GMusic musicFrontCenter;
        GW::AUDIO::GMusic musicFrontLeft;

        musicFrontLeft.Create(AudioTest::frontLeft, testAudio);

        testvolumes[G_FRONT_LEFT] = 1.0f;
        REQUIRE(+musicFrontLeft.SetChannelVolumes(testvolumes, 6));
        musicFrontLeft.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_LEFT] = 0.0f;
        musicFrontLeft = nullptr;

        musicFrontCenter.Create(AudioTest::frontCenter, testAudio);

        testvolumes[G_FRONT_CENTER] = 1.0f;
        REQUIRE(+musicFrontCenter.SetChannelVolumes(testvolumes, 6));
        musicFrontCenter.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_CENTER] = 0.0f;
        musicFrontCenter = nullptr;

        musicFrontRight.Create(AudioTest::frontRight, testAudio);

        testvolumes[G_FRONT_RIGHT] = 1.0f;
        REQUIRE(+musicFrontRight.SetChannelVolumes(testvolumes, 6));
        musicFrontRight.Play();
        GSLEEP_FOR(500);
        testvolumes[G_FRONT_RIGHT] = 0.0f;
        musicFrontRight = nullptr;
    }

    SECTION("Surround/Back Channels")
    {
        GW::AUDIO::GMusic musicSurroundRight;
        GW::AUDIO::GMusic musicSubwoofer;
        GW::AUDIO::GMusic musicSurroundLeft;

        musicSurroundLeft.Create(AudioTest::surroundLeft, testAudio);

        testvolumes[G_SURROUND_LEFT] = 1.0f;
        REQUIRE(+musicSurroundLeft.SetChannelVolumes(testvolumes, 6));
        musicSurroundLeft.Play();
        GSLEEP_FOR(500);
        testvolumes[G_SURROUND_LEFT] = 0.0f;
        musicSurroundLeft = nullptr;

        musicSubwoofer.Create(AudioTest::bass, testAudio);

        testvolumes[G_SURROUND_CENTER] = 1.0f;
        REQUIRE(+musicSubwoofer.SetChannelVolumes(testvolumes, 6));
        musicSubwoofer.Play();
        GSLEEP_FOR(250);
        testvolumes[G_SURROUND_CENTER] = 0.0f;
        musicSubwoofer = nullptr;

        musicSurroundRight.Create(AudioTest::surroundRight, testAudio);

        testvolumes[G_SURROUND_RIGHT] = 1.0f;
        REQUIRE(+musicSurroundRight.SetChannelVolumes(testvolumes, 6));
        musicSurroundRight.Play();
        GSLEEP_FOR(500);
        testvolumes[G_SURROUND_RIGHT] = 0.0f;
        musicSurroundRight = nullptr;
    }

    // Fail cases
    GW::AUDIO::GMusic testMusic;

    testMusic.Create(AudioTest::testBeep, testAudio);

    REQUIRE(testMusic.SetChannelVolumes(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testMusic.SetChannelVolumes(testvolumes, -1) == GW::GReturn::INVALID_ARGUMENT);
}

TEST_CASE("Setting music volume", "[SetVolume], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testMusic.Create(AudioTest::volume1, testAudio);

	// Fail cases
	REQUIRE(testMusic.SetVolume(-1.0f) == GW::GReturn::INVALID_ARGUMENT);
	// Pass cases
	REQUIRE(+testMusic.SetVolume(1.0f));

	testMusic.Play();
    GSLEEP_FOR(1000);
}

TEST_CASE("Stopping music stream", "[Stop], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testMusic.Create(AudioTest::testSong, testAudio);

	testMusic.Play();

	REQUIRE(+testMusic.Stop());
    GSLEEP_FOR(200);
}

TEST_CASE("Stopping music after pausing", "[Stop], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testMusic.Create(AudioTest::testSong, testAudio);

	testMusic.Play();
	testMusic.Pause();

	REQUIRE(+testMusic.Stop());
}
#endif // end of Music tests

// Composite tests (and Events)
#ifdef G_AUDIO_COMPOSITE_TESTS
TEST_CASE("Setting master volume", "[SetMasterVolume], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

	REQUIRE(testAudio.SetMasterVolume(-1.0f) == GW::GReturn::INVALID_ARGUMENT);
	REQUIRE(+testAudio.SetMasterVolume(1.0f));
    REQUIRE(testAudio.SetMasterVolume(1.0f) == GW::GReturn::REDUNDANT);
    REQUIRE(+testAudio.SetGlobalSoundVolume(1.0f));
    REQUIRE(testAudio.SetGlobalSoundVolume(1.0f) == GW::GReturn::REDUNDANT);
    REQUIRE(+testAudio.SetGlobalMusicVolume(1.0f));
    REQUIRE(testAudio.SetGlobalMusicVolume(1.0f) == GW::GReturn::REDUNDANT);

    testSound.Create(AudioTest::masterVolume1, testAudio);

	testSound.Play();
    GSLEEP_FOR(1000);
	testSound.Stop();

	REQUIRE(+testAudio.SetMasterVolume(0.5f));

    testSound.Create(AudioTest::masterVolume_5, testAudio);

	testMusic.Play();
    GSLEEP_FOR(1000);
	testMusic.Stop();
}

TEST_CASE("Pausing all sounds and music.", "[PauseSounds], [PauseMusic], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testSound.Create(AudioTest::soundPause, testAudio);

    float RIGHT_EAR[6] = { 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };
    testSound.SetChannelVolumes(RIGHT_EAR, 6);

    testMusic.Create(AudioTest::musicPause, testAudio);

    float LEFT_EAR[6] = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	testMusic.SetChannelVolumes(LEFT_EAR, 6);

	testMusic.Play();
	testSound.Play();
    bool isPlaying = false;
    REQUIRE(+testSound.isPlaying(isPlaying));
    REQUIRE(isPlaying == true);
    REQUIRE(+testMusic.isPlaying(isPlaying));
    REQUIRE(isPlaying == true);
    GSLEEP_FOR(1200);

	REQUIRE(+testAudio.PauseSounds());
    REQUIRE(+testAudio.PauseMusic());
    GSLEEP_FOR(200);
    
    REQUIRE(+testSound.isPlaying(isPlaying));
    REQUIRE(isPlaying == false);
    REQUIRE(+testMusic.isPlaying(isPlaying));
    REQUIRE(isPlaying == false);
}

TEST_CASE("Setting master channel volumes", "[SetMusicChannelVolumes], [SetSoundsChannelVolumes], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testSound.Create(AudioTest::volume_5, testAudio);

    float RIGHT_EAR[6] = { 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };
    // Fail cases
    REQUIRE(testAudio.SetSoundsChannelVolumes(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testAudio.SetSoundsChannelVolumes(RIGHT_EAR, -1) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testAudio.SetSoundsChannelVolumes(RIGHT_EAR, 6));

    testMusic.Create(AudioTest::volume_5, testAudio);

    float LEFT_EAR[6] = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
    // Fail cases
    REQUIRE(testAudio.SetMusicChannelVolumes(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
    REQUIRE(testAudio.SetMusicChannelVolumes(LEFT_EAR, -1) == GW::GReturn::INVALID_ARGUMENT);
    // Pass case
    REQUIRE(+testAudio.SetMusicChannelVolumes(LEFT_EAR, 6));

	// Pass cases
    float HALVES[6] = { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f };
	REQUIRE(+testAudio.SetMusicChannelVolumes(HALVES, 6));
    REQUIRE(+testAudio.SetSoundsChannelVolumes(HALVES, 6));

	testSound.Play();
	testMusic.Play();
    GSLEEP_FOR(1000);
}

TEST_CASE("Resuming all sounds and music.", "[ResumeSounds], [ResumeMusic], [Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GAudio testAudio;
    GW::AUDIO::GSound testSound;
    GW::AUDIO::GMusic testMusic;
    testAudio.Create();

    testSound.Create(AudioTest::soundResume, testAudio);

    float RIGHT_EAR[6] = { 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };
    testSound.SetChannelVolumes(RIGHT_EAR, 6);

    testSound.Create(AudioTest::musicResume, testAudio);

    float LEFT_EAR[6] = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	testMusic.SetChannelVolumes(LEFT_EAR, 6);

	testSound.Play();
	testMusic.Play();
	testAudio.PauseSounds();
    testAudio.PauseMusic();

	REQUIRE(+testAudio.ResumeMusic());
    REQUIRE(+testAudio.ResumeSounds());
    GSLEEP_FOR(1000);
}
#endif // end of Composite tests

// WARNING!
#if 0 // Noise hazard, be aware enabling this (can be extremely loud)
TEST_CASE("Audio stress test", "[Audio]")
{
    if (!EnabledirBuffer && !AudioTests)
    {
        std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::AUDIO::GSound testSound[25];
    GW::AUDIO::GMusic testMusic[25];
    GW::AUDIO::GAudio testAudio;
    testAudio.Create();

    for (int i = 0; i < 25; i++)
    {
        REQUIRE(+testSound[i].Create(PATH(dirBuffer, AudioTest::soundPlay, testAudio));
        REQUIRE(+testMusic[i].Create(PATH(dirBuffer, AudioTest::musicPlay, testAudio));
        REQUIRE(+testSound[i].Play());
        REQUIRE(+testMusic[i].Play());
        GSLEEP_FOR(50);
    }

    GSLEEP_FOR(1000);
    testAudio.PauseSounds();
    testAudio.PauseMusic();
    GSLEEP_FOR(1000);
    testAudio.ResumeSounds();
    testAudio.ResumeMusic();
    GSLEEP_FOR(1000);
    testAudio.StopSounds();
    testAudio.StopMusic();
    GSLEEP_FOR(1000);
    testAudio.PlaySounds();
    testAudio.PlayMusic();
    GSLEEP_FOR(1000);
    testAudio.SetMasterVolume(0);
    GSLEEP_FOR(1000);
    testAudio.SetMasterVolume(1);
    GSLEEP_FOR(1000);
    float LEFT_EAR[6] = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
    float RIGHT_EAR[6] = { 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };
    testAudio.SetSoundsChannelVolumes(LEFT_EAR, 6);
    testAudio.SetMusicChannelVolumes(RIGHT_EAR, 6);
    GSLEEP_FOR(1000);
    testAudio.PlaySounds();
    testAudio.PlayMusic();
    GSLEEP_FOR(1000);
}
#endif

#undef GSLEEP_FOR
#undef G_FRONT_LEFT 
#undef G_FRONT_RIGHT 
#undef G_FRONT_CENTER 
#undef G_SURROUND_CENTER 
#undef G_SURROUND_LEFT 
#undef G_SURROUND_RIGHT 
#undef G_SOUND_TESTS
#undef G_MUSIC_TESTS
#undef G_AUDIO_COMPOSITE_TESTS
#endif /* defined(GATEWARE_ENABLE_AUDIO) && !defined(GATEWARE_DISABLE_GAUDIO) && !defined(GATEWARE_DISABLE_GMUSIC) && !defined(GATEWARE_DISABLE_GSOUND) */