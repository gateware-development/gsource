#if defined(GATEWARE_ENABLE_AUDIO) && !defined(GATEWARE_DISABLE_GAUDIO3D) && !defined(GATEWARE_DISABLE_GMUSIC3D) && !defined(GATEWARE_DISABLE_GSOUND3D)

#include "Resources.h"
#include <thread>
#include "../../Interface/System/GFile.h"

/*
All the sounds:
testSong, testBeep, testLaser, beep, stereo5_1Test
frontLeft, frontCenter, frontRight,
surroundLeft, bass, surroundRight,
masterVolume1, masterVolume_5, volume1, volume_5,
musicPause, musicResume, musicPlay,
soundPause, soundStop, soundResume, soundPlay
*/

/// If appropriate TESTS macro is defined, the tests would run for that section
#define G_SOUND3D_TESTS
#define G_MUSIC3D_TESTS
#define G_AUDIO3D_COMPOSITE_TESTS

#if 0 // enable for very fast audio tests
#define GSLEEP_FOR(g_time_ms) std::this_thread::sleep_for(std::chrono::milliseconds(100))
#else
#define GSLEEP_FOR(g_time_ms) std::this_thread::sleep_for(std::chrono::milliseconds(g_time_ms))
#endif

// Starting Audio3D Tests
bool EnableAudio3DTests = false;
TEST_CASE("Check for Audio Hardware in GAudio3D", "[Audio]")
{
	GW::AUDIO::GAudio3D audio3D;
	if (audio3D.Create() != GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		EnableAudio3DTests = true;
		std::cout << "AUDIO HARDWARE DETECTED: RUNNING ALL AUDIO3D UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: AUDIO HARDWARE NOT DETECTED: SKIPPING ALL AUDIO3D UNIT TESTS" << std::endl;
}

TEST_CASE("Creating audio3D", "[CreateGAudio3D], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	REQUIRE(+testAudio3D.Create());
	testAudio3D = nullptr;
}



// Starting Sound3D Tests
#ifdef G_SOUND3D_TESTS
TEST_CASE("Creating sound3D", "[CreateGSound3D], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	GW::AUDIO::GSound3D testSound3D;

	testSound3D.Create(AudioTest::testBeep, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);
}

TEST_CASE("Playing sound3D", "[Play], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GSound3D testSound3D;
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	testSound3D.Create(AudioTest::soundPlay, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	REQUIRE(+testSound3D.Play());
	GSLEEP_FOR(1000);
}

TEST_CASE("Surround update position Sound3D", "[UpdatePosition], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	GW::AUDIO::GSound3D testSound3D;
	testAudio3D.Create();

	testSound3D.Create(AudioTest::testLaser, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	GW::MATH::GVECTORF vecPos = GW::MATH::GIdentityVectorF; // at (0,0,0)
	GW::MATH::GQUATERNIONF vecOrientation = GW::MATH::GIdentityQuaternionF;

	testAudio3D.Update3DListener(vecPos, vecOrientation);

	vecPos.x = -50.0f;
	REQUIRE(+testSound3D.UpdatePosition(vecPos));
	testSound3D.Play();
	GSLEEP_FOR(250);

	vecPos.x = 50.0f;
	REQUIRE(+testSound3D.UpdatePosition(vecPos));
	testSound3D.Play();
	GSLEEP_FOR(250);
}

TEST_CASE("Surround update orientation Sound3D", "[Update3DListener], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();
	GW::AUDIO::GSound3D testSound3D;

	testSound3D.Create(AudioTest::testLaser, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	GW::MATH::GMatrix gMatrix;

	const float halfPi = static_cast<float>(G_PI_F * 0.5f);
	GW::MATH::GVECTORF vecPos = { 75.0f, 0.0f, 0.0f, 1.0f }; // at (75, 0, 0)
	testSound3D.UpdatePosition(vecPos);
	vecPos.x = 0.0f;

	GW::MATH::GQUATERNIONF vecOrientation = GW::MATH::GIdentityQuaternionF;
	GW::MATH::GMATRIXF rotationMatrix = GW::MATH::GIdentityMatrixF;

	for (int i = 0; i < 5; ++i)
	{
		gMatrix.RotateYGlobalF(GW::MATH::GIdentityMatrixF, halfPi * i, rotationMatrix);
		GW::MATH::GQuaternion::SetByMatrixF(rotationMatrix, vecOrientation);
		REQUIRE(+testAudio3D.Update3DListener(vecPos, vecOrientation));

		testSound3D.Play();
		GSLEEP_FOR(250);
	}
}
#endif // end G_SOUND3D_TESTS

// Starting Music3D Tests
#ifdef G_MUSIC3D_TESTS
TEST_CASE("Creating music3D", "[CreateGMusic3D], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	GW::AUDIO::GMusic3D testMusic3D;

	REQUIRE(+testMusic3D.Create(AudioTest::testBeep, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D));
}

TEST_CASE("Playing music3D", "[Play], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GMusic3D testMusic3D;
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	testMusic3D.Create(AudioTest::testSong, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	REQUIRE(+testMusic3D.Play(true));
	GSLEEP_FOR(2000);
}

TEST_CASE("Surround update position Music3D", "[UpdatePosition], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	GW::AUDIO::GMusic3D testMusic3D;
	testMusic3D.Create(AudioTest::testLaser, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	GW::MATH::GVECTORF vecPos = GW::MATH::GIdentityVectorF; // at (0,0,0)
	GW::MATH::GQUATERNIONF vecOrientation = GW::MATH::GIdentityQuaternionF;

	testAudio3D.Update3DListener(vecPos, vecOrientation);

	vecPos.x = -50.0f;
	REQUIRE(+testMusic3D.UpdatePosition(vecPos));
	testMusic3D.Play();
	GSLEEP_FOR(250);

	vecPos.x = 50.0f;
	REQUIRE(+testMusic3D.UpdatePosition(vecPos));
	testMusic3D.Play();
	GSLEEP_FOR(250);
}

TEST_CASE("Surround update orientation Music3D", "[Update3DListener], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	GW::AUDIO::GMusic3D testMusic3D;
	testMusic3D.Create(AudioTest::testLaser, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	GW::MATH::GMatrix gMatrix;

	const float halfPi = static_cast<float>(G_PI_F * 0.5f);
	GW::MATH::GVECTORF vecPos = { 75.0f, 0.0f, 0.0f, 1.0f };  // at (75, 0, 0)
	testMusic3D.UpdatePosition(vecPos);
	vecPos.x = 0.0f;

	GW::MATH::GQUATERNIONF vecOrientation = GW::MATH::GIdentityQuaternionF;
	GW::MATH::GMATRIXF rotationMatrix = GW::MATH::GIdentityMatrixF;

	for (int i = 0; i < 5; ++i)
	{
		gMatrix.RotateYGlobalF(GW::MATH::GIdentityMatrixF, -halfPi * i, rotationMatrix);
		GW::MATH::GQuaternion::SetByMatrixF(rotationMatrix, vecOrientation);
		REQUIRE(+testAudio3D.Update3DListener(vecPos, vecOrientation));

		testMusic3D.Play();
		GSLEEP_FOR(250);
	}
}
#endif // end G_MUSIC3D_TESTS

// Starting Composite Tests
#ifdef G_AUDIO3D_COMPOSITE_TESTS
TEST_CASE("Simultaneous GSound3D and GMusic3D", "[UpdatePosition], [Audio]")
{
	if (EnableAudio3DTests == false)
	{
		std::cout << "Audio HW support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::AUDIO::GAudio3D testAudio3D;
	testAudio3D.Create();

	GW::AUDIO::GSound3D testSound3D;
	GW::AUDIO::GMusic3D testMusic3D;

	testSound3D.Create(AudioTest::frontLeft, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);
	testMusic3D.Create(AudioTest::frontRight, 0.0f, 100.0f, GW::AUDIO::GATTENUATION::LINEAR, testAudio3D);

	GW::MATH::GVECTORF vecPos = GW::MATH::GIdentityVectorF; // at (0,0,0)
	GW::MATH::GQUATERNIONF vecOrientation = GW::MATH::GIdentityQuaternionF;

	testAudio3D.Update3DListener(vecPos, vecOrientation);

	vecPos.z = 25.0f;

	vecPos.x = -50.0f;
	REQUIRE(+testSound3D.UpdatePosition(vecPos));

	vecPos.x = 50.0f;
	REQUIRE(+testMusic3D.UpdatePosition(vecPos));

	testSound3D.Play();
	testMusic3D.Play();

	GSLEEP_FOR(1000);
}
#endif // end G_AUDIO3D_COMPOSITE_TESTS

#undef G_SOUND3D_TESTS
#undef G_MUSIC3D_TESTS
#undef G_AUDIO_COMPOSITE_TESTS
#undef GSLEEP_FOR
#endif /* defined(GATEWARE_ENABLE_AUDIO) && (!defined(GATEWARE_DISABLE_GAUDIO3D) || !defined(GATEWARE_DISABLE_GMUSIC3D) || !defined(GATEWARE_DISABLE_GSOUND3D)) */