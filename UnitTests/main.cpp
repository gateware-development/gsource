#define CATCH_CONFIG_RUNNER

#if defined(__APPLE__)
#include <TargetConditionals.h>
#if TARGET_OS_IOS
#define CATCH_CONFIG_COLOUR_NONE // Disable XCode output colors.
#endif // TARGET_OS_IOS
#endif // __APPLE__

#include "catch.hpp"

#if defined(_WIN32) && !defined(NDEBUG) 
	#include <crtdbg.h>
	#include <stdlib.h>
	#define _CRTDBG_MAO_ALLOC
#endif

#include <chrono>

// DO NOT REMOVE THIS, YOU CAN UNCOMMENT, BUT DONT REMOVEEEEE
#define DISABLE_USER_INPUT_TESTS 0

// All the tests you want to run
#define GATEWARE_ENABLE_CORE
#define GATEWARE_ENABLE_AUDIO
#define GATEWARE_ENABLE_MATH
#define GATEWARE_ENABLE_MATH2D
#define GATEWARE_ENABLE_SYSTEM
#define GATEWARE_ENABLE_GRAPHICS
#define GATEWARE_ENABLE_INPUT

#if defined(_WIN32)
	#include <winapifamily.h>
	#if (WINAPI_FAMILY == WINAPI_FAMILY_APP)
		// will never be supported on UWP (we should standardize platfrom UT opt-in)
		#define GATEWARE_DISABLE_GVULKANSURFACE
		#define GATEWARE_DISABLE_GOPENGLSURFACE
		
    #endif
#endif 

#include "API.h"
#include "tests.hpp"
#include "../Source/Shared/GDependenciesTest.hpp"


int main(int argc, char ** argv)
{
#if defined(_WIN32) && !defined(NDEBUG) 
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(12345);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
#endif

	auto startTime = std::chrono::system_clock::now();
	int result = // only run with args if they are detected
		(argc) ? Catch::Session().run(argc, argv) : Catch::Session().run();


	std::cout << "== EXITING TESTS ==" << std::endl;
	std::chrono::duration<float> time = std::chrono::system_clock::now() - startTime;
	std::cout << "Elapsed Time: " << time.count() << '\n';
	//printf("Elapsed Time: %fs\n", time.count());


	GW::SYSTEM::GFile fileProxy;
	fileProxy.Create();
	char workingDir[512] = "";
	fileProxy.GetSaveFolder(512, workingDir);
	if (result == 0) {
		fileProxy.SetCurrentWorkingDirectory(workingDir);
		if (+fileProxy.OpenTextWrite("UTResult.txt")) {
			fileProxy.Write((const char*)&result, sizeof(int));
			fileProxy.CloseFile();
		}
	}
    
	return result;
}
// this is ignored by desktop applications but allows UWP/iOS/Android to use the main entrypoint.
// without it you will need to define the specific platform entry point (ex: wWinMain in C++/WinRT)
GW::SYSTEM::GApp myApp(main);
