TEST_CASE("Creating windows using different flags", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // Ignore this test
	}
	GW::SYSTEM::GWindow window;

	constexpr bool testWithDelay = false;
	constexpr int delayTime = 2500;
	constexpr int delayWindowTime = 5000;

	auto DelayTest = [&](int delay) {
		if (!testWithDelay)
			return;

		auto start = std::chrono::steady_clock::now();

		while (true)
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
	};

	auto DelayTestAsWindowProcesses = [&](int delay) {
		if (!testWithDelay)
			return;

		auto start = std::chrono::steady_clock::now();

		while (+window.ProcessWindowEvents())
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
	};

	// Pass Cases
	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::MINIMIZED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;
}

TEST_CASE("Reconfiguring windows created with different styles", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // Ignore this test
	}
	GW::SYSTEM::GWindow window;

	constexpr bool testWithDelay = false;
	constexpr int delayTime = 2500;
	constexpr int delayWindowTime = 5000;

	auto DelayTest = [&](int delay) {
		if (!testWithDelay)
			return;

		auto start = std::chrono::steady_clock::now();

		while (true)
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
	};

	auto DelayTestAsWindowProcesses = [&](int delay) {
		if (!testWithDelay)
			return;

		auto start = std::chrono::steady_clock::now();

		while (+window.ProcessWindowEvents())
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
	};

	// Pass Cases: Test if the window can be created with each style, then reconfigured to another.
	//             Also, tests if the window can be reconfigured the style it is currently without issue.
	constexpr int GWINDOW_STYLE_COUNT = 6;
	for (int i = 0; i < GWINDOW_STYLE_COUNT; ++i)
	{
		GW::SYSTEM::GWindowStyle style = (GW::SYSTEM::GWindowStyle)i;

		for (int j = 0; j < GWINDOW_STYLE_COUNT; ++j)
		{
			GW::SYSTEM::GWindowStyle nextStyle = (GW::SYSTEM::GWindowStyle)j;
			DelayTest(delayTime);
			// on MacOS FullScreen/Minimized doesn't seem to work from a logged out runner
#ifdef __APPLE__
			if (nextStyle == GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED ||
				nextStyle == GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS ||
				nextStyle == GW::SYSTEM::GWindowStyle::MINIMIZED)
				continue;
#endif
			//LT-- Causes build error, could be used for debugging
			//PrintCreateMessage(style); PrintReconfigMessage(nextStyle);
			REQUIRE(G_PASS(window.Create(100, 200, 500, 400, style)));
			DelayTestAsWindowProcesses(delayWindowTime);
			REQUIRE(G_PASS(window.ReconfigureWindow(100, 200, 500, 400, nextStyle)));
			DelayTestAsWindowProcesses(delayWindowTime);

			window = nullptr;
		}
	}
}

TEST_CASE("Setting window icon", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // Ignore this test
	}
	GW::SYSTEM::GWindow window;

	// Icons for the Mac must have one of these dimension for them to render properly: 16x16, 32x32, 48x48, 128x128, 256x256, 512x512, 1024x1024.
	// Windows and Linux will scale icons to fit.
	constexpr int ICON_SIZE_SMALL = 16;
	constexpr int ICON_SIZE_SMALL_SQ = ICON_SIZE_SMALL * ICON_SIZE_SMALL;

	const unsigned int oneIconPixelsSmall[ICON_SIZE_SMALL_SQ] =
	{
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x33ff0000, 0x33ff0000, 0xffff0000, 0x33ff0000, 0x33ff0000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000
	};
	const unsigned int twoIconPixelsSmall[ICON_SIZE_SMALL_SQ] =
	{
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000
	};
	const unsigned int gwIconPixelsSmall[ICON_SIZE_SMALL_SQ] =
	{
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000,
		0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000
	};

	constexpr int ICON_SIZE_BIG = 32;
	constexpr int ICON_SIZE_BIG_SQ = ICON_SIZE_BIG * ICON_SIZE_BIG;

	const unsigned int twoIconPixelsBig[ICON_SIZE_BIG_SQ] =
	{
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000,
		0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000,
		0x00000000, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x00000000,
		0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00,
		0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00,
		0x00000000, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x00000000,
		0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000,
		0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x3300ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0x3300ff00, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
	};
	const unsigned int gwIconPixelsBig[ICON_SIZE_BIG_SQ] =
	{
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
	};

	// Fail Case: Test SetIcon() returns fail if an attempt is made to set the icon before the window is created.
	REQUIRE(G_FAIL(window.SetIcon(ICON_SIZE_SMALL, ICON_SIZE_SMALL, oneIconPixelsSmall)));

	// Setup
	REQUIRE(G_PASS(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));

	constexpr int delayPerTest = 250;
	auto start = std::chrono::steady_clock::now();
	int test = 0;

	// Pass Cases: Tests SetIcon() can set all icons. Also tests setting individual icons using consecutive calls to SetIcon().
	// Note: On Windows and Linux, the first call to SetIcon() will change all display icons. After the first call to SetIcon(), only certain icons will change depending on size.
	//		 On Mac, all dislay icons will change with each call to SetIcon().
	while (+window.ProcessWindowEvents())
	{
		auto end = std::chrono::steady_clock::now();
		auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

		if (lapse > delayPerTest)
		{
			if (test == 0)
				REQUIRE(G_PASS(window.SetIcon(ICON_SIZE_SMALL, ICON_SIZE_SMALL, oneIconPixelsSmall)));
			else if (test == 1)
				REQUIRE(G_PASS(window.SetIcon(ICON_SIZE_BIG, ICON_SIZE_BIG, twoIconPixelsBig)));
			else if (test == 2)
				REQUIRE(G_PASS(window.SetIcon(ICON_SIZE_BIG, ICON_SIZE_BIG, gwIconPixelsBig)));
			else if (test == 3)
				REQUIRE(G_PASS(window.SetIcon(ICON_SIZE_SMALL, ICON_SIZE_SMALL, twoIconPixelsSmall)));
			else if (test == 4)
				REQUIRE(G_PASS(window.SetIcon(ICON_SIZE_SMALL, ICON_SIZE_SMALL, gwIconPixelsSmall)));
			else
				break;

			start = end;
			++test;
		}
	}

	// Fail Cases: Test SetIcon() returns fail when given invalid parameters.
	REQUIRE(G_FAIL(window.SetIcon(ICON_SIZE_BIG, ICON_SIZE_BIG, nullptr)));
	REQUIRE(G_FAIL(window.SetIcon(0, 0, oneIconPixelsSmall)));
}

TEST_CASE("GWindow core method test battery", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	GW::CORE::GInterface emptyInterface;
	unsigned int listenerCount = 0;
	unsigned int width = 0, height = 0;
	unsigned int clientWidth = 0, clientHeight = 0;
	unsigned int x = 0, y = 0;
	GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE universalWindowHandle;
	bool fullscreen = false;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(window.ProcessWindowEvents() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.SetWindowName("EMPTY WINDOW") == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.MoveWindow(0, 0) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ResizeWindow(1920, 1080) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Maximize() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Minimize() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetWidth(width) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetHeight(height) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientWidth(clientWidth) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientHeight(clientHeight) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetX(x) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetY(y) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientTopLeft(x, y) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetWindowHandle(universalWindowHandle) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.IsFullscreen(fullscreen) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Observers(listenerCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Register(emptyInterface, nullptr) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		window = nullptr;
		REQUIRE(!window);
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

		REQUIRE(window.ProcessWindowEvents() == GW::GReturn::SUCCESS);
		REQUIRE(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(window.SetWindowName("VALID WINDOW") == GW::GReturn::SUCCESS);
		REQUIRE(window.MoveWindow(0, 0) == GW::GReturn::SUCCESS);
		REQUIRE(window.ResizeWindow(1920 >> 1, 1080 >> 1) == GW::GReturn::SUCCESS);
		REQUIRE(window.Maximize() == GW::GReturn::SUCCESS);
		REQUIRE(window.Minimize() == GW::GReturn::SUCCESS);
		REQUIRE(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

#ifdef __linux__
		// On Linux GetX(), GetY(), GetWidth(), GetHeight(), GetClientWidth(), GetClientHeight(), and
		// GetClientTopLeft() take into the account the frame and title bar height which the window 
		// manager may not have set up by the time we call the functions and these functions will fail.
		// MAX_WINDOW_WAIT_TIME is used to give the window manager some time to finish setting up our
		// window if it fails initially.
		constexpr int MAX_WINDOW_WAIT_TIME = 2500; // The maximum time we'll give the window to pass its test before we move on.
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetWidth(width) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetHeight(height) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetX(x) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetX(y) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetClientTopLeft(x, y) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetClientWidth(clientWidth) == GW::GReturn::SUCCESS);
		GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, window.GetClientHeight(clientHeight) == GW::GReturn::SUCCESS);
#else
		REQUIRE(window.GetWidth(width) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetHeight(height) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetX(x) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetY(y) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientTopLeft(x, y) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientWidth(clientWidth) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientHeight(clientHeight) == GW::GReturn::SUCCESS);
#endif

		REQUIRE(window.GetWindowHandle(universalWindowHandle) == GW::GReturn::SUCCESS);
		REQUIRE(window.IsFullscreen(fullscreen) == GW::GReturn::SUCCESS);
		REQUIRE(window.Observers(listenerCount) == GW::GReturn::SUCCESS);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::REDUNDANT);
		REQUIRE(emptyInterface.Create() == GW::GReturn::SUCCESS);
		REQUIRE(window.Register(emptyInterface, [](const GW::GEvent& event, GW::CORE::GInterface& observer) { return; }) == GW::GReturn::SUCCESS);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::SUCCESS);
	}
}

TEST_CASE("Moving Window.", "[MoveWindow], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Pass cases
	REQUIRE(G_PASS(window.MoveWindow(42, 42)));
}

TEST_CASE("Resizing Window.", "[ResizeWindow], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Pass cases
	REQUIRE(G_PASS(window.ResizeWindow(300, 300)));
	REQUIRE(G_PASS(window.Maximize()));
	REQUIRE(window.Minimize() == GW::GReturn::SUCCESS);
}

TEST_CASE("Querying Window information.", "[GetWidth], [GetHeight], [GetX], [GetY], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Set window parameters for query tests
	REQUIRE(G_PASS(window.ReconfigureWindow(250, 500, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	bool windowIsFullscreen = false;
	unsigned int windowHeight = 0;
	unsigned int windowWidth = 0;
	unsigned int windowPosX = 0;
	unsigned int windowPosY = 0;

	GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH;

	window.IsFullscreen(windowIsFullscreen);
	CHECK(windowIsFullscreen == false);
	// Fail cases
	CHECK(G_FAIL(window.GetWidth(windowWidth) == GW::GReturn::INVALID_ARGUMENT));

	CHECK(G_FAIL(windowIsFullscreen == true));

	// Resize windows for pass tests
	REQUIRE(G_PASS(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED)));

	// Pass cases	
#ifdef __linux__
	// On Linux GetX(), GetY(), GetWidth(), GetHeight(), GetClientWidth(), GetClientHeight(), and
	// GetClientTopLeft() take into the account the frame and title bar height which the window 
	// manager may not have set up by the time we call the functions and these functions will fail.
	// MAX_WINDOW_WAIT_TIME is used to give the window manager some time to finish setting up our
	// window if it fails initially.
	constexpr int MAX_WINDOW_WAIT_TIME = 2500; // The maximum time we'll give the window to pass its test before we move on.
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.IsFullscreen(windowIsFullscreen)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetHeight(windowHeight)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetWidth(windowWidth)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetX(windowPosX)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetY(windowPosY)));
#else
	REQUIRE(G_PASS(window.IsFullscreen(windowIsFullscreen)));
	REQUIRE(G_PASS(window.GetHeight(windowHeight)));
	REQUIRE(G_PASS(window.GetWidth(windowWidth)));
	REQUIRE(G_PASS(window.GetX(windowPosX)));
	REQUIRE(G_PASS(window.GetY(windowPosY)));
#endif

#ifdef _WIN32
	REQUIRE(windowIsFullscreen == true);
	REQUIRE(G_PASS(window.GetWindowHandle(UWH)));
#elif __linux__
	REQUIRE(G_PASS(window.GetWindowHandle(UWH)));
#elif __APPLE__
	REQUIRE(G_PASS(window.GetWindowHandle(UWH)));
#endif
}

TEST_CASE("Querying Client Information.", "[GetClientWidth], [GetClientHeight], [GetClientTopLeft], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(10, 20, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	unsigned int windowClientWidth = 0;
	unsigned int windowClientHeight = 0;
	unsigned int windowClientPosX = 0;
	unsigned int windowClientPosY = 0;

	// Pass Cases
#ifdef __linux__
	// On Linux GetX(), GetY(), GetWidth(), GetHeight(), GetClientWidth(), GetClientHeight(), and
	// GetClientTopLeft() take into the account the frame and title bar height which the window 
	// manager may not have set up by the time we call the functions and these functions will fail.
	// MAX_WINDOW_WAIT_TIME is used to give the window manager some time to finish setting up our
	// window if it fails initially.
	constexpr int MAX_WINDOW_WAIT_TIME = 2500; // The maximum time we'll give the window to pass its test before we move on.
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetClientWidth(windowClientWidth)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetClientHeight(windowClientHeight)));
#else
	REQUIRE(G_PASS(window.GetClientWidth(windowClientWidth)));
	REQUIRE(G_PASS(window.GetClientHeight(windowClientHeight)));
#endif

	// Functions return correct information
	CHECK(windowClientWidth == 640);
	CHECK(windowClientHeight == 480);

	unsigned int windowWidth = 0;
	unsigned int windowHeight = 0;

#ifdef __linux__
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetClientTopLeft(windowClientPosX, windowClientPosY)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetWidth(windowWidth)));
	GiveWindowTimeBeforeRequiringPass(window, MAX_WINDOW_WAIT_TIME, G_PASS(window.GetHeight(windowHeight)));
#else
	window.GetClientTopLeft(windowClientPosX, windowClientPosY);
	window.GetWidth(windowWidth);
	window.GetHeight(windowHeight);
#endif

	// The title bar of each window is a different height and different border size on each OS and
	// on different flavors of Linux (by default). Because of this, the approach to test 
	// GetClientTopLeft() is dynamic in that it relys on the attributes of the window we've created
	// to determine if the function passes the Unit Test.
	unsigned int borderSize = (windowWidth - windowClientWidth) / 2;
	unsigned int expectedX = borderSize;
	unsigned int expectedY = windowHeight - borderSize - windowClientHeight;

	CHECK(windowClientPosX == expectedX);
	CHECK(windowClientPosY == expectedY);
}

TEST_CASE("Creating Multiple GWindow", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window1;
	GW::SYSTEM::GWindow window2;
	GW::SYSTEM::GWindow window3;
	GW::SYSTEM::GWindow window4;
	GW::SYSTEM::GWindow window5;

	REQUIRE(G_PASS(window1.Create(0, 0, 100, 100, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window2.Create(100, 100, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window3.Create(200, 200, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window4.Create(300, 300, 400, 400, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window5.Create(400, 400, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
}

TEST_CASE("GWINDOW STRESS TEST 1 (Set all 3 window to nullptr after certain time)", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window1;
	GW::SYSTEM::GWindow window2;
	GW::SYSTEM::GWindow window3;
	GW::CORE::GEventResponder responder1;
	GW::CORE::GEventResponder responder2;
	GW::CORE::GEventResponder responder3;
	int count = 0;

	REQUIRE(G_PASS(window1.Create(0, 0, 100, 100, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window2.Create(100, 100, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window3.Create(200, 200, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	responder1.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 1\n"; } break;
			}
		});
	responder2.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 2\n"; } break;
			}
		});
	responder3.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 3\n"; } break;
			}
		});
	window1.Register(responder1);
	window2.Register(responder2);
	window3.Register(responder3);
	double t = 0.0;
	while (t < 5.0)
	{
		t += 0.001;
		window1.ProcessWindowEvents();
		window2.ProcessWindowEvents();
		window3.ProcessWindowEvents();
	}
	window1 = nullptr;
	window2 = nullptr;
	window3 = nullptr;

	REQUIRE(count == 3);
	std::cout << "COUNT: " << count << std::endl;
}

/*
	Tests if the window events are being sent when they are expected to be, after calling certain interface functions.
*/
TEST_CASE("GWINDOW EVENT TEST CASE", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// CONSTANTS

	// Windows cannot be expected to instantly move, resize, etc when told to. Each operating system uses
	// different window managers that have the final say of when an action on a window is carried out. We
	// need to give the manager time to carry out our requests for the window. Also, some windows may 
	// animate (like on macOS) and therefore need time to do so.

	// The minimum and maximum time we test for.
#if defined(__APPLE__)
	constexpr int MAX_WINDOW_WAIT_TIME = 20000; // The maximum time we'll give the window to pass its test before we move on.
	constexpr int MIN_WINDOW_WAIT_TIME = 2500; // Value chosen due to animations when minimizing and maximizing.
#elif defined(__linux__)
	constexpr int MAX_WINDOW_WAIT_TIME = 5000;
	constexpr int MIN_WINDOW_WAIT_TIME = 250; // Value chosen due to the window manager not being ready right after a window is created.
#else
	constexpr int MAX_WINDOW_WAIT_TIME = 5000;
	constexpr int MIN_WINDOW_WAIT_TIME = 0; // No problems with Win32.
#endif

	// VARIABLES

	GW::SYSTEM::GWindow::Events targetEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;
	GW::SYSTEM::GWindow::Events receivedEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;

	GW::CORE::GThreadShared threadLock;
	GW::SYSTEM::GWindow window;
	GW::CORE::GEventResponder responder;

	// SETUP

	threadLock.Create();
	window.Create(240, 270, 400, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	responder.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;

			if (+_event.Read(windowEvent, windowEventData))
			{
				threadLock.LockAsyncRead();
				// When targetEvent is set to something other than EVENTS_PROCESSED, it will start being compared to windowEvent.
				if (targetEvent != GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED && windowEvent == targetEvent)
				{
					threadLock.UnlockAsyncRead();

					threadLock.LockSyncWrite();
					receivedEvent = windowEvent;
					threadLock.UnlockSyncWrite();
				}
				else
					threadLock.UnlockAsyncRead();
			}
		});

	window.Register(responder);

	// HELPER FUNCTIONS

	// Sets targetEvent in a thread safe manor.
	auto SetTargetEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		threadLock.LockSyncWrite();
		targetEvent = _event;
		threadLock.UnlockSyncWrite();
	};

#if defined(__APPLE__) || defined(__linux__)
#define PrintEvent snprintf
#else
#define PrintEvent snprintf
#endif

	// Takes a window event and outs a string version of it.
	auto EventToString = [&](GW::SYSTEM::GWindow::Events _event, char* _outEventStr, unsigned int _eventStrLen)
	{
		switch (_event)
		{
		case GW::SYSTEM::GWindow::Events::MINIMIZE: 		PrintEvent(_outEventStr, _eventStrLen, "%s", "MINIMIZE"); 		break;
		case GW::SYSTEM::GWindow::Events::MAXIMIZE: 		PrintEvent(_outEventStr, _eventStrLen, "%s", "MAXIMIZE"); 		break;
		case GW::SYSTEM::GWindow::Events::RESIZE: 			PrintEvent(_outEventStr, _eventStrLen, "%s", "RESIZE"); 			break;
		case GW::SYSTEM::GWindow::Events::MOVE: 			PrintEvent(_outEventStr, _eventStrLen, "%s", "MOVE"); 			break;
		case GW::SYSTEM::GWindow::Events::DISPLAY_CLOSED: 	PrintEvent(_outEventStr, _eventStrLen, "%s", "DISPLAY_CLOSED"); 	break;
		case GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED: PrintEvent(_outEventStr, _eventStrLen, "EVENTS_PROCESSED");	break;
		case GW::SYSTEM::GWindow::Events::DESTROY: 			PrintEvent(_outEventStr, _eventStrLen, "%s", "DESTROY"); 			break;
		default:
			PrintEvent(_outEventStr, _eventStrLen, "%s", "[this should never be reached]");
			break;
		}
	};

	// Checks if received event matches the target event, and prints a string representation
	// of each event if they don't match.
	auto PrintEventsIfWeAreGoingToFail = [&]()
	{
		threadLock.LockAsyncRead();
		GW::SYSTEM::GWindow::Events _targetEvent = targetEvent;
		GW::SYSTEM::GWindow::Events _receivedEvent = receivedEvent;
		threadLock.UnlockAsyncRead();

		if (_receivedEvent != _targetEvent)
		{
			constexpr unsigned int eventStrSize = 32;
			char targetEventStr[eventStrSize];
			char receivedEventStr[eventStrSize];

			EventToString(_targetEvent, targetEventStr, eventStrSize);
			EventToString(_receivedEvent, receivedEventStr, eventStrSize);

			if (_receivedEvent == GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED)
			{
				printf(
					"Fail: No event was received when expecting event %s(%d)\n",
					targetEventStr,
					static_cast<int>(_targetEvent)
				);
			}
			else
			{
				printf(
					"Fail: Received event %s(%d) when expecting event %s(%d)\n",
					receivedEventStr,
					static_cast<int>(_receivedEvent),
					targetEventStr,
					static_cast<int>(_targetEvent)
				);
			}
		}


		// Is the last event we received the event we're expecting.
	};

	// Waits for the received GWindow event to match the tested. Will not wait longer than MAX_WINDOW_WAIT_TIME.
	auto WaitToReceiveEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		bool testPasses = false;

		while ((lapsedTime < MAX_WINDOW_WAIT_TIME && window != nullptr) || lapsedTime < MIN_WINDOW_WAIT_TIME)
		{
			window.ProcessWindowEvents();

			threadLock.LockAsyncRead();
			testPasses = (receivedEvent == _event);
			threadLock.UnlockAsyncRead();

			if (!testPasses)
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));
			else if (lapsedTime >= MIN_WINDOW_WAIT_TIME)
				break;

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		}
	};

	// EVENT TESTS

	GW::SYSTEM::GWindow::Events event = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;

	// Test MOVE event.
	event = GW::SYSTEM::GWindow::Events::MOVE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.MoveWindow(250, 260)));
	WaitToReceiveEvent(event);
	PrintEventsIfWeAreGoingToFail();
	// Did we receive the event we were looking for?
	threadLock.LockAsyncRead();
	REQUIRE(receivedEvent == targetEvent);
	threadLock.UnlockAsyncRead();

	// Test RESIZE event.
	event = GW::SYSTEM::GWindow::Events::RESIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.ResizeWindow(410, 310)));
	WaitToReceiveEvent(event);
	PrintEventsIfWeAreGoingToFail();
	// Did we receive the event we were looking for?
	threadLock.LockAsyncRead();
	REQUIRE(receivedEvent == targetEvent);
	threadLock.UnlockAsyncRead();

	// on MacOS MAXIMIZE/MINIMIZE doesn't seem to work from a logged out runner
	// we may be able to adjust the docker container settings to workaround this
#ifndef __APPLE__
	// Test MAXIMIZE event.
	event = GW::SYSTEM::GWindow::Events::MAXIMIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.Maximize()));
	WaitToReceiveEvent(event);
	PrintEventsIfWeAreGoingToFail();
	// Did we receive the event we were looking for?
	threadLock.LockAsyncRead();
	REQUIRE(receivedEvent == targetEvent);
	threadLock.UnlockAsyncRead();

	// Test MINIMIZE event.
	event = GW::SYSTEM::GWindow::Events::MINIMIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.Minimize()));
	WaitToReceiveEvent(event);
	PrintEventsIfWeAreGoingToFail();
	// Did we receive the event we were looking for?
	threadLock.LockAsyncRead();
	REQUIRE(receivedEvent == targetEvent);
	threadLock.UnlockAsyncRead();
#endif

	// Test DESTROY event.
	event = GW::SYSTEM::GWindow::Events::DESTROY;
	SetTargetEvent(event);
	window = nullptr;
	WaitToReceiveEvent(event);
	PrintEventsIfWeAreGoingToFail();
	// Did we receive the event we were looking for?
	threadLock.LockAsyncRead();
	REQUIRE(receivedEvent == targetEvent);
	threadLock.UnlockAsyncRead();

#undef PrintEvent
}

/*
	Tests if the window's getter functions and event data return expected information about the window.
*/
TEST_CASE("GWINDOW EVENT DATA TEST CASE", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// CONSTANTS

	// Windows cannot be expected to instantly move, resize, etc when told to. Each operating system uses
	// different window managers that have the final say of when an action on a window is carried out. We
	// need to give the manager time to carry out our requests for the window. Also, some windows may 
	// animate (like on macOS) and therefore need time to do so.
	constexpr int MAX_WINDOW_WAIT_TIME = 5000; // The maximum time we'll give the window to pass its test before we move on.

	// The minimum time we test for.
#if defined(__APPLE__)
	constexpr int MIN_WINDOW_WAIT_TIME = 2500; // Value chosen due to animations when minimizing and maximizing.
#elif defined(__linux__)
	constexpr int MIN_WINDOW_WAIT_TIME = 250; // Value chosen due to the window manager not being ready right after a window is created.
#else
	constexpr int MIN_WINDOW_WAIT_TIME = 0; // No problems with Win32.
#endif

	// VARIABLES

	unsigned int targetX = 220;
	unsigned int targetY = 250;
	unsigned int targetClientWidth = 300;
	unsigned int targetClientHeight = 200;
	GW::SYSTEM::GWindowStyle targetStyle = GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED;

	unsigned int eventDataX = 0;
	unsigned int eventDataY = 0;
	unsigned int eventDataWidth = 0;
	unsigned int eventDataHeight = 0;
	unsigned int eventDataClientWidth = 0;
	unsigned int eventDataClientHeight = 0;

	unsigned int getXFunction = 0;
	unsigned int getYFunction = 0;
	unsigned int getWidthFunction = 0;
	unsigned int getHeightFunction = 0;
	unsigned int getClientWidthFunction = 0;
	unsigned int getClientHeightFunction = 0;

	GW::SYSTEM::GWindow::Events targetEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;
	GW::SYSTEM::GWindow::Events receivedEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;

	GW::CORE::GThreadShared threadLock;
	GW::SYSTEM::GWindow window;
	GW::CORE::GEventResponder responder;

	// SETUP

	threadLock.Create();
	window.Create(targetX, targetY, targetClientWidth, targetClientHeight, targetStyle);
	responder.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;

			if (+_event.Read(windowEvent, windowEventData))
			{
				threadLock.LockAsyncRead();

				// When targetEvent is set to something other than EVENTS_PROCESSED, it will start being compared to receivedEvent.
				// Once receivedEvent == targetEvent, it will no longer set receivedEvent until targetEvent changes to some other event.
				if (targetEvent != GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED &&
					receivedEvent != targetEvent &&
					windowEvent == targetEvent)
				{
					threadLock.UnlockAsyncRead();

					threadLock.LockSyncWrite();

					bool eventMatch = false;

					switch (windowEvent)
					{
					case GW::SYSTEM::GWindow::Events::MINIMIZE:
					case GW::SYSTEM::GWindow::Events::MAXIMIZE:
					case GW::SYSTEM::GWindow::Events::RESIZE:
					case GW::SYSTEM::GWindow::Events::MOVE:
					case GW::SYSTEM::GWindow::Events::DESTROY:
						eventMatch = true;
						break;
					}

					if (eventMatch)
					{
						receivedEvent = windowEvent;

						eventDataX = windowEventData.windowX;
						eventDataY = windowEventData.windowY;
						eventDataWidth = windowEventData.width;
						eventDataHeight = windowEventData.height;
						eventDataClientWidth = windowEventData.clientWidth;
						eventDataClientHeight = windowEventData.clientHeight;

						window.GetX(getXFunction);
						window.GetY(getYFunction);
						window.GetWidth(getWidthFunction);
						window.GetHeight(getHeightFunction);
						window.GetClientHeight(getClientHeightFunction);
						window.GetClientWidth(getClientWidthFunction);
					}
					threadLock.UnlockSyncWrite();
				}
				else
					threadLock.UnlockAsyncRead();
			}
		});

	window.Register(responder);

	// HELPER FUNCTIONS

	// Sets targetEvent in a thread safe manor.
	auto SetTargetEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		threadLock.LockSyncWrite();
		targetEvent = _event;
		threadLock.UnlockSyncWrite();
	};

	// Waits for the window is finished being created and setup. The test is conducted for no longer
	// than MAX_WINDOW_WAIT_TIME or until they pass.
	auto GiveWindowTimeToSetup = [&]()
	{
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		bool testPasses = false;

		while ((lapsedTime < MAX_WINDOW_WAIT_TIME && window != nullptr) || lapsedTime < MIN_WINDOW_WAIT_TIME)
		{
			testPasses = +window.ProcessWindowEvents();

			if (!testPasses)
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));
			else if (lapsedTime >= MIN_WINDOW_WAIT_TIME)
				break;

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		}
	};

	// Waits for the target event to be received. The test is conducted for no longer than MAX_WINDOW_WAIT_TIME.
	auto WaitToReceiveTargetEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		bool testPasses = false;

		while ((lapsedTime < MAX_WINDOW_WAIT_TIME && window != nullptr) || lapsedTime < MIN_WINDOW_WAIT_TIME)
		{
			window.ProcessWindowEvents();

			threadLock.LockAsyncRead();
			testPasses = (receivedEvent == _event);
			threadLock.UnlockAsyncRead();

			if (!testPasses)
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));
			else if (lapsedTime >= MIN_WINDOW_WAIT_TIME)
				break;

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		}

	};

	// EVENT DATA TESTS	

	// Test getter functions against target values after creating the window.
	GiveWindowTimeToSetup();
	REQUIRE(G_PASS(window.GetX(getXFunction)));
	REQUIRE(G_PASS(window.GetY(getYFunction)));
	REQUIRE(G_PASS(window.GetClientWidth(getClientWidthFunction)));
	REQUIRE(G_PASS(window.GetClientHeight(getClientHeightFunction)));
	// Getter functions vs. target values
	REQUIRE(getXFunction == targetX);
	REQUIRE(getYFunction == targetY);
	REQUIRE(getClientWidthFunction == targetClientWidth);
	REQUIRE(getClientHeightFunction == targetClientHeight);

	GW::SYSTEM::GWindow::Events event = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED;

	// Test getter functions, against event data, against target values, for MOVE event.
	event = GW::SYSTEM::GWindow::Events::MOVE;
	SetTargetEvent(event);
	targetX = 210;
	targetY = 321;
	REQUIRE(G_PASS(window.MoveWindow(targetX, targetY))); // Interface function being tested.
	WaitToReceiveTargetEvent(event);
	// event data vs. target values
	threadLock.LockAsyncRead();
	REQUIRE(eventDataX == targetX);
	REQUIRE(eventDataY == targetY);
	REQUIRE(eventDataClientWidth == targetClientWidth);
	REQUIRE(eventDataClientHeight == targetClientHeight);
	// Getter functions vs. eventData
	REQUIRE(getXFunction == eventDataX);
	REQUIRE(getYFunction == eventDataY);
	REQUIRE(getWidthFunction == eventDataWidth);
	REQUIRE(getHeightFunction == eventDataHeight);
	REQUIRE(getClientWidthFunction == eventDataClientWidth);
	REQUIRE(getClientHeightFunction == eventDataClientHeight);
	// Getter functions vs. target values
	REQUIRE(getXFunction == targetX);
	REQUIRE(getYFunction == targetY);
	REQUIRE(getClientWidthFunction == targetClientWidth);
	REQUIRE(getClientHeightFunction == targetClientHeight);
	threadLock.UnlockAsyncRead();

	// Test getter functions, against event data, against target values, for RESIZE event.
	event = GW::SYSTEM::GWindow::Events::RESIZE;
	SetTargetEvent(event);
	targetClientWidth = 540;
	targetClientHeight = 450;
	REQUIRE(G_PASS(window.ResizeWindow(targetClientWidth, targetClientHeight))); // Interface function being tested.
	WaitToReceiveTargetEvent(event);
	// event data vs. target values
	threadLock.LockAsyncRead();
	// We don't want to test position because some Window servers might move the window when
	// resizing. This happens with X11 when window is near the edge of the screen.
	REQUIRE(eventDataClientWidth == targetClientWidth);
	REQUIRE(eventDataClientHeight == targetClientHeight);
	// Getter functions vs. eventData
	REQUIRE(getXFunction == eventDataX); // Even though we're not testing position, these
	REQUIRE(getYFunction == eventDataY); // should match each other.
	REQUIRE(getWidthFunction == eventDataWidth);
	REQUIRE(getHeightFunction == eventDataHeight);
	REQUIRE(getClientWidthFunction == eventDataClientWidth);
	REQUIRE(getClientHeightFunction == eventDataClientHeight);
	// Getter functions vs. target values
	REQUIRE(getClientWidthFunction == targetClientWidth);
	REQUIRE(getClientHeightFunction == targetClientHeight);
	threadLock.UnlockAsyncRead();

	// on MacOS MAXIMIZE/MINIMIZE doesn't seem to work from a logged out runner
#ifndef __APPLE__
	// Test getter functions, against event data, for MAXIMIZE event.
	event = GW::SYSTEM::GWindow::Events::MAXIMIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.Maximize())); // Interface function being tested.
	WaitToReceiveTargetEvent(event);
	// Getter functions vs. eventData
	threadLock.LockAsyncRead();
	REQUIRE(getXFunction == eventDataX);
	REQUIRE(getYFunction == eventDataY);
	REQUIRE(getWidthFunction == eventDataWidth);
	REQUIRE(getHeightFunction == eventDataHeight);
	REQUIRE(getClientWidthFunction == eventDataClientWidth);
	REQUIRE(getClientHeightFunction == eventDataClientHeight);
	threadLock.UnlockAsyncRead();

	// Test getter functions, against event data, for MINIMIZE event.
	event = GW::SYSTEM::GWindow::Events::MINIMIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.Minimize())); // Interface function being tested.
	WaitToReceiveTargetEvent(event);
	// Getter functions vs. eventData
	threadLock.LockAsyncRead();
	REQUIRE(getXFunction == eventDataX);
	REQUIRE(getYFunction == eventDataY);
	REQUIRE(getWidthFunction == eventDataWidth);
	REQUIRE(getHeightFunction == eventDataHeight);
	REQUIRE(getClientWidthFunction == eventDataClientWidth);
	REQUIRE(getClientHeightFunction == eventDataClientHeight);
	threadLock.UnlockAsyncRead();
#endif

	// Test getter functions against event data for DESTROY event. 
	event = GW::SYSTEM::GWindow::Events::DESTROY;
	SetTargetEvent(event);
	window = nullptr; // Interface function being tested.
	WaitToReceiveTargetEvent(event);
	threadLock.LockAsyncRead();
	REQUIRE(getXFunction == eventDataX);
	REQUIRE(getYFunction == eventDataY);
	REQUIRE(getWidthFunction == eventDataWidth);
	REQUIRE(getHeightFunction == eventDataHeight);
	REQUIRE(getClientWidthFunction == eventDataClientWidth);
	REQUIRE(getClientHeightFunction == eventDataClientHeight);
	threadLock.UnlockAsyncRead();
}

#if !defined(DISABLE_USER_INPUT_TESTS)
TEST_CASE("GWINDOW STRESS TEST 2 (Close all window explicitly using X Button)", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window1;
	GW::SYSTEM::GWindow window2;
	GW::SYSTEM::GWindow window3;
	GW::CORE::GEventResponder responder1;
	GW::CORE::GEventResponder responder2;
	GW::CORE::GEventResponder responder3;
	int count = 0;

	REQUIRE(G_PASS(window1.Create(0, 0, 100, 100, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window2.Create(100, 100, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window3.Create(200, 200, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	responder1.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 1\n"; } break;
			}
		});
	responder2.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 2\n"; } break;
			}
		});
	responder3.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 3\n"; } break;
			}
		});

	window1.Register(responder1);
	window2.Register(responder2);
	window3.Register(responder3);

	std::cout << "EXPLICITLY CLOSE THE WINDOW PLEASE\n";

	int STAYALIVE = 1;
	while (STAYALIVE)
	{
		STAYALIVE = 0;
		if (+window1.ProcessWindowEvents())
			++STAYALIVE;
		if (+window2.ProcessWindowEvents())
			++STAYALIVE;
		if (+window3.ProcessWindowEvents())
			++STAYALIVE;
	}

	REQUIRE(count == 3);
	std::cout << "COUNT: " << count << std::endl;
}

TEST_CASE("GWINDOW STRESS TEST 3 (Close all window explicitly using X Button, setting window to nullptr afterwards should not generate destroy event)", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window1;
	GW::SYSTEM::GWindow window2;
	GW::SYSTEM::GWindow window3;
	GW::CORE::GEventResponder responder1;
	GW::CORE::GEventResponder responder2;
	GW::CORE::GEventResponder responder3;
	int count = 0;

	REQUIRE(G_PASS(window1.Create(0, 0, 100, 100, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window2.Create(100, 100, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(G_PASS(window3.Create(200, 200, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	responder1.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 1\n"; } break;
			}
		});
	responder2.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 2\n"; } break;
			}
		});
	responder3.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			_event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::DESTROY: { ++count; std::cout << "Responder 3\n"; } break;
			}
		});

	window1.Register(responder1);
	window2.Register(responder2);
	window3.Register(responder3);

	std::cout << "EXPLICITLY CLOSE THE WINDOW PLEASE\n";
	int STAYALIVE = 1;
	while (STAYALIVE)
	{
		STAYALIVE = 0;
		if (+window1.ProcessWindowEvents())
			++STAYALIVE;
		if (+window2.ProcessWindowEvents())
			++STAYALIVE;
		if (+window3.ProcessWindowEvents())
			++STAYALIVE;
	}
	window1 = nullptr;
	window2 = nullptr;
	window3 = nullptr;

	REQUIRE(count == 3);
	std::cout << "COUNT: " << count << std::endl;
}

/*
	This is a test case used internally to test events from gwindow
	This does not have to be enable, but it is a good test case that allows GW Developers
	to see how a event responder respond to events
*/
TEST_CASE("GWINDOW MANUAL EVENT DATA TEST CASE", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// Define
#define PrintEventInfo(count, id, eventStr, eventData)\
	printf("#%d: | RECEIVER %d: %8s EVENT | X: %-5d | Y: %-5d | Width: %-5d | Height: %-5d | ClientWidth: %-5d | ClientHeight: %-5d\n",\
			count,\
			id,\
			eventStr,\
			eventData.windowX,\
			eventData.windowY,\
			eventData.width,\
			eventData.height,\
			eventData.clientWidth,\
			eventData.clientHeight);

	// GWindow "unit test" Proof Of Concept
	int eventCount1 = 0;
	GW::SYSTEM::GWindow window1;
	window1.Create(150, 150, 150, 150, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	GW::CORE::GEventResponder responder1;
	responder1.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			if (+_event.Read(windowEvent, windowEventData))
			{
				switch (windowEvent)
				{
				case GW::SYSTEM::GWindow::Events::MINIMIZE: { PrintEventInfo(++eventCount1, 1, "MINIMIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::MAXIMIZE: { PrintEventInfo(++eventCount1, 1, "MAXIMIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::RESIZE: { PrintEventInfo(++eventCount1, 1, "RESIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::MOVE: { PrintEventInfo(++eventCount1, 1, "MOVE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::DESTROY: { PrintEventInfo(++eventCount1, 1, "DESTROY", windowEventData); } break;
				}
			}
		});
	window1.Register(responder1);
	int eventCount2 = 0;
	GW::SYSTEM::GWindow window2;
	window2.Create(300, 300, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	GW::CORE::GEventResponder responder2;
	responder2.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			if (+_event.Read(windowEvent, windowEventData))
			{
				switch (windowEvent)
				{
				case GW::SYSTEM::GWindow::Events::MINIMIZE: { PrintEventInfo(++eventCount2, 2, "MINIMIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::MAXIMIZE: { PrintEventInfo(++eventCount2, 2, "MAXIMIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::RESIZE: { PrintEventInfo(++eventCount2, 2, "RESIZE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::MOVE: { PrintEventInfo(++eventCount2, 2, "MOVE", windowEventData); } break;
				case GW::SYSTEM::GWindow::Events::DESTROY: { PrintEventInfo(++eventCount2, 2, "DESTROY", windowEventData); } break;
				}
			}
		});
	window2.Register(responder2);

	std::cout << "GWINDOW EVENT TEST. CLOSE BOTH WINDOWS WHEN FINISHED.\n";

	int STAYALIVE = 1;
	while (STAYALIVE)
	{
		STAYALIVE = 0;
		if (+window1.ProcessWindowEvents())
			++STAYALIVE;
		if (+window2.ProcessWindowEvents())
			++STAYALIVE;
	}

	// Cleanup define
#undef PrintEventInfo
}

#undef WindowSleepTime
#undef GiveWindowTimeBeforeRequiringPass

#endif /* DISABLE_USER_INPUT_TESTS */