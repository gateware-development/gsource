#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GFILE)
//Includes needed for this test suite
#include <cstring>
#include <iostream>
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
#include <winrt/Windows.Storage.h>
#endif

//This is the ammount of files that should be in the directory after all test cases run
#define DIR_SIZE 10
// This is the amount of sub-directories that should be in the directory after all test cases run
#define SUBDIR_SIZE 5

//Test case file names will not be larger than 40 characters
#define FILE_NAME_SIZE 40
//Test case file names will not be larger than 40 characters
#define SUBDIR_NAME_SIZE 40

// This is the size of the test.test files found in resources
// IMPORTANT: Don't use known text files (ex: *.txt) for binary file size checks!
// Most GIT repos will auto adjust the newlines on them for your OS on checkout.
// Obviously this could modify the final size of the file which is very confusing.
#define SIZE_OF_TEST_FILE 64 

#define TEST_STRING "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-=_+[]\\"

// Custom Unit Tests specific to this interface follow..
TEST_CASE("Directory handling.", "[SetCurrentWorkingDirectory], [GetCurrentWorkingDirectory], [System]") // need to add all get folder funtions here
{
	char directoryBuffer[260]; //Same size as MAX_PATH MACRO
	GW::SYSTEM::GFile file;
	file.Create();

	SECTION("Getting the current install directory", "[GetInstallFolder]")
	{
		//char installDirBuffer[260];

		//Fail cases
		CHECK(file.GetInstallFolder(1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetInstallFolder(0, &directoryBuffer[0]) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetInstallFolder(260, directoryBuffer)));

		std::cout << "Install Directory: " << directoryBuffer << "\n";
	}

	SECTION("Getting the current save directory", "[GetSaveFolder]")
	{
		//char saveDirBuffer[260];
		//Fail cases
		CHECK(file.GetSaveFolder(1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetSaveFolder(0, &directoryBuffer[0]) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetSaveFolder(260, directoryBuffer)));

		std::cout << "Save Directory: " << directoryBuffer << "\n";
	}

	SECTION("Getting the current preferences directory", "[GetPreferencesFolder]")
	{
		//char preferencesDirBuffer[260];
		//Fail cases
		CHECK(file.GetPreferencesFolder(1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetPreferencesFolder(0, &directoryBuffer[0]) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetPreferencesFolder(260, directoryBuffer)));

		std::cout << "Preferences Directory: " << directoryBuffer << "\n";
	}

	SECTION("Getting the current temporary directory", "[GetTempFolder]")
	{
		//char tempDirBuffer[260];
		//Fail cases
		CHECK(file.GetTempFolder(1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetTempFolder(0, &directoryBuffer[0]) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetTempFolder(260, directoryBuffer)));

		std::cout << "Temporary Directory: " << directoryBuffer << "\n";
	}

	SECTION("Getting the current cache directory", "[GetCacheFolder]")
	{
		//char cacheDirBuffer[260];
		//Fail cases
		CHECK(file.GetCacheFolder(1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetCacheFolder(0, &directoryBuffer[0]) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetCacheFolder(260, directoryBuffer)));

		std::cout << "Cache Directory: " << directoryBuffer << "\n";
	}

	SECTION("Setting the current working directory.", "[SetCurrentWorkingDirectory]")
	{
		//Fail cases
		CHECK(file.SetCurrentWorkingDirectory(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(-(file.SetCurrentWorkingDirectory("./A NonExsitant Directory")));
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
		file.GetSaveFolder(260, directoryBuffer);
		std::string upDir = "/../../";
		std::string dirBuff = directoryBuffer;
		dirBuff += upDir;

		CHECK(file.SetCurrentWorkingDirectory(dirBuff.c_str()) == GW::GReturn::FILE_NOT_FOUND);
#endif

		//Pass cases
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
		REQUIRE(+(file.SetCurrentWorkingDirectory(winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str())));
#elif defined(WIN32)
		REQUIRE(+(file.SetCurrentWorkingDirectory(u8"./")));
#elif __APPLE__
		REQUIRE(+(file.SetCurrentWorkingDirectory(u8"./")));
#elif __linux__
		REQUIRE(+(file.SetCurrentWorkingDirectory(u8"./")));
#endif
		//REQUIRE(+(file.SetCurrentWorkingDirectory(u8"../gateware-test-suite.git.0/XCode/Gateware/GTests/TestDirectory")));
	}

	SECTION("Get current working directory.", "[GetCurrentWorkingDirectory]")
	{
		//Fail cases
		CHECK(file.GetCurrentWorkingDirectory(nullptr, 1) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.GetCurrentWorkingDirectory(&directoryBuffer[0], 0) == GW::GReturn::INVALID_ARGUMENT);

		//Pass cases
		REQUIRE(+(file.GetCurrentWorkingDirectory(directoryBuffer, 260)));

		std::cout << "Current Directory: " << directoryBuffer << "\n";
	}
}

TEST_CASE("Binary file testing.", "[OpenBinaryWrite], [Write], [AppendBinaryWrite], [OpenBinaryRead], [Read], [Seek], [System]")
{
	char directoryBuffer[260]; //Same size as MAX_PATH MACRO
	GW::SYSTEM::GFile file;
	file.Create();
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	file.SetCurrentWorkingDirectory(winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str());
#elif defined(_WIN32) || defined(__Linux__)
	file.SetCurrentWorkingDirectory(u8"./");
#elif defined(__APPLE__) && TARGET_OS_IOS
    file.SetCurrentWorkingDirectory([[[NSBundle mainBundle] bundlePath] UTF8String]);
#elif defined(__APPLE__)
	file.SetCurrentWorkingDirectory(u8"../../../../Xcode/Unit Tests"); // THIS IS GOING TO NEED TO CHANGE FOR MAC TO PASS UNIT TESTS SINCE DIRECTORY DEPTHS HAVE BEEN ADJUSTED
#endif
	file.GetCurrentWorkingDirectory(directoryBuffer, 260);

	SECTION("Open binary file for writing.", "[OpenBinaryWrite]")
	{
		//Pass cases. Pass cases are first in this case because we have to open a file before we can check the fail outs
		REQUIRE(+(file.OpenBinaryWrite(u8"g_binary_test.gtest")));
	}

	//Writing to binary file
	SECTION("Write to binary file.", "[Write]")
	{
		//Pass cases
		//Write the current directory to the file.
		file.OpenBinaryWrite(u8"g_binary_test.gtest");
		REQUIRE(+(file.Write(TEST_STRING, static_cast<unsigned int>(strlen(TEST_STRING) + 1))));

		//We need to close the file in order to test some of the fail cases
		file.CloseFile();

		//Fail cases
		CHECK(G_FAIL(file.Write(TEST_STRING, static_cast<unsigned int>(strlen(TEST_STRING) + 1))));
		CHECK(file.Write(nullptr, 1) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.Write(TEST_STRING, 0) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.Write(nullptr, 0) == GW::GReturn::INVALID_ARGUMENT);
	}

	SECTION("Open binary file for appending.", "[AppendBinaryWrite]")
	{
		//Pass cases
		REQUIRE(+(file.AppendBinaryWrite(u8"g_binary_test.gtest")));

		//We need to close the file now so future tests can open a file
		file.CloseFile();
	}

	SECTION("Open file for binary read.", "[OpenBinaryRead]")
	{
		//Special fail case
		REQUIRE(file.OpenBinaryRead(u8"FileThatDoesntExist.bin") == GW::GReturn::FILE_NOT_FOUND);

		//Pass cases
		REQUIRE(+(file.OpenBinaryRead(u8"g_binary_test.gtest")));

		//Fail cases
		CHECK(file.OpenBinaryRead(nullptr) == GW::GReturn::INVALID_ARGUMENT);
	}

	SECTION("Read in from binary file.", "[Read]")
	{
		char buffer[80] = { '\0' }; //Same size as our TEST_STRING.
		file.OpenBinaryRead(u8"g_binary_test.gtest");

		//Pass cases
		REQUIRE(+(file.Read(buffer, static_cast<unsigned int>(strlen(TEST_STRING)) + 1)));
		REQUIRE(strcmp(buffer, TEST_STRING) == 0);

		//We need to close the file to ensure we can not write to a closed file
		file.CloseFile();

		//Fail cases
		CHECK(G_FAIL(file.Read(&buffer[0], 260)));
	}

	SECTION("Seek within a binary file.", "[Seek]") 
	{
		// need this to see if we're in the right spot
		char buffer = '\0';

		// open a file for the rest of the tests
		file.OpenBinaryRead(u8"g_binary_test.gtest");

		unsigned int currPosition = 0;

		// pass cases
		REQUIRE(+(file.Seek(0, 20, currPosition)));
			CHECK(currPosition == 20);
			file.Read(&buffer, 1);
				CHECK(buffer == 'u');
		REQUIRE(+(file.Seek(30, 20, currPosition)));
			CHECK(currPosition == 50);
			file.Read(&buffer, 1);
				CHECK(buffer == 'Y');
		REQUIRE(+(file.Seek(30, -20, currPosition)));
			CHECK(currPosition == 10);
			file.Read(&buffer, 1);
				CHECK(buffer == 'k');
		REQUIRE(+(file.Seek(-1, 10, currPosition)));
			CHECK(currPosition == 21);
			file.Read(&buffer, 1);
				CHECK(buffer == 'v');
		REQUIRE(+(file.Seek(-1, -5, currPosition)));
			CHECK(currPosition == 17);
			file.Read(&buffer, 1);
				CHECK(buffer == 'r');
		REQUIRE(+(file.Seek(0, 0, currPosition))); // seek to the beginning of the file
			CHECK(currPosition == 0);
			file.Read(&buffer, 1);
				CHECK(buffer == 'a');
		REQUIRE(+file.Seek(100, -50, currPosition)); // this should pass because essentially we are seeking to position 50
			CHECK(currPosition == 50);
			file.Read(&buffer, 1);
				CHECK(buffer == 'Y');
		REQUIRE(+(file.Seek(0, 79, currPosition))); // seek to the end of the file and read the binary data
			CHECK(currPosition == 79);
			file.Read(&buffer, 1);
				CHECK(buffer == 0);


		// fail cases
		CHECK(file.Seek(0, -1, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the beginning of the file
		CHECK(file.Seek(0, 100, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the end of the file
		CHECK(file.Seek(50, 100, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the beginning of the file
		CHECK(file.Seek(50, -100, currPosition) == GW::GReturn::INVALID_ARGUMENT);// cant seek past the end of the file
		CHECK(file.Seek(100, 50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to 150 in an 80byte file
		CHECK(file.Seek(-100, 50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to -50
		CHECK(file.Seek(-100, -50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to -150 in an 80byte file
		

		//We need to close the file now so future tests can open a file
		file.CloseFile();

		// fail case when no file open
		CHECK(file.Seek(0, 20, currPosition) == GW::GReturn::FILE_NOT_FOUND);
	}
}

TEST_CASE("Text file testing.", "[OpenTextWrite], [WriteLine], [AppendTextWrite], [OpenTextRead], [ReadLine], [Seek], [System]")
{
	char directoryBuffer[260]; //Same size as MAX_PATH MACRO
	GW::SYSTEM::GFile file;
	file.Create();
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	file.SetCurrentWorkingDirectory(winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str());
#elif defined(WIN32) || defined(__Linux__)
	file.SetCurrentWorkingDirectory(u8"./");
#elif defined(__APPLE__)
	file.SetCurrentWorkingDirectory(u8"../../../../Xcode/Unit Tests");
#endif
	file.GetCurrentWorkingDirectory(directoryBuffer, 260);

	SECTION("Open file for text write", "[OpenTextWrite]")
	{
		//Pass cases.
		REQUIRE(+(file.OpenTextWrite(u8"g_test.gtest")));

		//Fail cases
		CHECK(G_FAIL(file.OpenTextWrite(u8"g_test.gtest")));
		CHECK(file.OpenTextWrite(nullptr) == GW::GReturn::INVALID_ARGUMENT);
	}

	SECTION("Write to text file.", "[WriteLine]")
	{
		//Pass cases
		file.OpenTextWrite(u8"g_test.gtest");
		REQUIRE(+(file.WriteLine(TEST_STRING)));

		//Close the file to make sure we can not write to a closed file
		file.CloseFile();

		//Fail cases
		CHECK(file.WriteLine(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(G_FAIL(file.WriteLine(TEST_STRING)));
	}

	SECTION("Open text file for appending.", "[AppendTextWrite]")
	{
		//Pass cases
		REQUIRE(+(file.AppendTextWrite(u8"g_test.gtest")));

		//Fail cases
		CHECK(file.AppendTextWrite(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(G_FAIL(file.AppendTextWrite(u8"g_test.gtest")));

		//We need to close the file so future tests can open files
		file.CloseFile();
	}

	SECTION("Open file for text read.", "[OpenTextRead]")
	{
		//Special fail Case
		REQUIRE(file.OpenTextRead(u8"FileThatDoesntExist.bin") == GW::GReturn::FILE_NOT_FOUND);

		//Pass cases
		REQUIRE(+(file.OpenTextRead(u8"g_test.gtest")));

		//Fail cases
		CHECK(file.OpenTextRead(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(G_FAIL(file.OpenTextRead(u8"g_test.gtest")));
	}

	//Read in from the text file
	SECTION("Read in from text file.", "[Readline]")
	{
		char buffer[80];// Same size as TEST_STRING
		file.OpenTextRead(u8"g_test.gtest");

		//Pass cases.
		REQUIRE(+(file.ReadLine(buffer, 80, '\n')));
		REQUIRE(strcmp(buffer, TEST_STRING) == 0);
		

		//Close the file at end of testing. Will no longer be needed
		file.CloseFile();

		//Fail cases
		CHECK(file.ReadLine(nullptr, 1, '\n') == GW::GReturn::INVALID_ARGUMENT);
		CHECK(file.ReadLine(&buffer[0], 0, '\n') == GW::GReturn::INVALID_ARGUMENT);
		CHECK(G_FAIL(file.ReadLine(&buffer[0], 79, '\0')));
	}

	SECTION("Seek within a text file.", "[Seek]") {
		// need this to see if we're in the right spot
		char buffer = '\0';

		// open a file for the rest of the tests
		file.OpenTextRead(u8"g_test.gtest");

		unsigned int currPosition = 0;



		// pass cases
		REQUIRE(+(file.Seek(0, 20 , currPosition)));
			CHECK(currPosition == 20);
				file.Read(&buffer, 1);
				CHECK(buffer == 'u');
		REQUIRE(+(file.Seek(30, 20, currPosition)));
			CHECK(currPosition == 50);
				file.Read(&buffer, 1);
				CHECK(buffer == 'Y');
		REQUIRE(+(file.Seek(30, -20, currPosition)));
			CHECK(currPosition == 10);
				file.Read(&buffer, 1);
				CHECK(buffer == 'k');
		REQUIRE(+(file.Seek(-1, 10, currPosition)));
			CHECK(currPosition == 21);
				file.Read(&buffer, 1);
				CHECK(buffer == 'v');
		REQUIRE(+(file.Seek(-1, -5, currPosition)));
			CHECK(currPosition == 17);
				file.Read(&buffer, 1);
				CHECK(buffer == 'r');
		REQUIRE(+(file.Seek(0, 0, currPosition))); // seek to the beginning of the file
			CHECK(currPosition == 0);
				file.Read(&buffer, 1);
				CHECK(buffer == 'a');
		REQUIRE(+file.Seek(100, -50, currPosition)); // this should pass because essentially we are seeking to position 50
			CHECK(currPosition == 50);
				file.Read(&buffer, 1);
				CHECK(buffer == 'Y');
		REQUIRE(+(file.Seek(0, 78, currPosition))); // seek to the end of the file and read the last character
			CHECK(currPosition == 78);
			file.Read(&buffer, 1);
				CHECK(buffer == '\\');


		// fail cases
		CHECK(file.Seek(0, -1, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the beginning of the file
		CHECK(file.Seek(0, 100, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the end of the file
		CHECK(file.Seek(50, 100, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek past the beginning of the file
		CHECK(file.Seek(50, -100, currPosition) == GW::GReturn::INVALID_ARGUMENT);// cant seek past the end of the file
		CHECK(file.Seek(100, 50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to 150 in an 80byte file
		CHECK(file.Seek(-100, 50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to -50
		CHECK(file.Seek(-100, -50, currPosition) == GW::GReturn::INVALID_ARGUMENT); // cant seek to -150 in an 80byte file


		//We need to close the file now so future tests can open a file
		file.CloseFile();

		// fail case when no file open
		CHECK(file.Seek(0, 20, currPosition) == GW::GReturn::FILE_NOT_FOUND);
	}
}

TEST_CASE("Reading beyond the end of a Binary file", "[Read], [Seek], [System]")
{

	char directoryBuffer[260]; //Same size as MAX_PATH MACRO
	GW::SYSTEM::GFile file;
	file.Create();
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	file.SetCurrentWorkingDirectory(winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str());
#elif defined(_WIN32) || defined(__Linux__)
	file.SetCurrentWorkingDirectory(u8"./");
#elif defined(__APPLE__)
	file.SetCurrentWorkingDirectory(u8"../../../../Xcode/Unit Tests");
#endif
	file.GetCurrentWorkingDirectory(directoryBuffer, 260);


	SECTION("Testing Read Function.", "[Read]")
	{
		char buffer[282] = { '\0' }; //Same size as our TEST_STRING.;
		file.OpenBinaryRead(u8"g_test.gtest");

		REQUIRE(G_FAIL(file.Read(buffer, static_cast<unsigned int>(sizeof(char) * 282))));
		file.CloseFile();
	}

	SECTION("Testing Failbit Reset", "[Seek]")
	{
		// open a file for the rest of the tests
		file.OpenBinaryRead(u8"g_binary_test.gtest");
		unsigned int currPosition = 0;

		// pass case taken from Previous Binary file unit tests
		REQUIRE(+(file.Seek(0, 20, currPosition)));	
		file.CloseFile();
	}
}

TEST_CASE("Reading beyond the end of a textfile", "[Read], [ReadLine], [System]")
{

	char directoryBuffer[260]; //Same size as MAX_PATH MACRO
	GW::SYSTEM::GFile file;
	file.Create();
#if defined(WIN32) || defined(__Linux__)
	file.SetCurrentWorkingDirectory(u8"./");
#elif defined(__APPLE__)
	file.SetCurrentWorkingDirectory(u8"../../../../Xcode/Unit Tests");
#endif
	file.GetCurrentWorkingDirectory(directoryBuffer, 260);
	

	SECTION("Testing ReadLine Function.", "[ReadLine], [Seek]")
	{
		file.OpenTextRead(u8"g_test.gtest");

		char buffer[82];// Same size as TEST_STRING + 2
		REQUIRE(G_FAIL(file.ReadLine(buffer, 82, '\0')));
		//e
		file.CloseFile();
	}
}

TEST_CASE("Directory Handling continued.", "[GetDirectorySize], [GetFileSize], [GetFilesFromDirectory], [System]")
{
	unsigned int dirSize;
	GW::SYSTEM::GFile file;
	file.Create();
	file.SetCurrentWorkingDirectory(u8"./");

	SECTION("Getting directory size.", "[GetDirectorySize]")
	{
		//Pass cases
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
		winrt::Windows::Storage::StorageFolder installPath = winrt::Windows::ApplicationModel::Package::Current().InstalledLocation();
		winrt::hstring strPath = installPath.Path();
		REQUIRE(+(file.SetCurrentWorkingDirectory(winrt::to_string(strPath + L"\\Resources\\GFile_TestFolder").c_str())));
#elif defined(WIN32)
        REQUIRE(+(file.SetCurrentWorkingDirectory(u8"../../../UnitTests/Resources/GFile_TestFolder")));
#elif defined(__APPLE__) && TARGET_OS_IOS
        REQUIRE(+(file.SetCurrentWorkingDirectory((std::string([[[NSBundle mainBundle] bundlePath] UTF8String]) + u8"/UnitTests/Resources/GFile_TestFolder").c_str())));
#else
        REQUIRE(+(file.SetCurrentWorkingDirectory(u8"../../../UnitTests/Resources/GFile_TestFolder")));
#endif
        REQUIRE(+(file.GetDirectorySize(dirSize)));
#if defined(__APPLE__) && !TARGET_OS_IOS // iOS does not have the .DS_STORE files.
        //Mac likes to shove .ds files all over the place, and this test has been shown to fail due to that uncontrollable behaviour, so now there's padding for the .ds to show up
        REQUIRE((dirSize == (DIR_SIZE) || dirSize == (DIR_SIZE + 1))); // 10 is the number of files that should be in the directory after all other testing
#else
        REQUIRE(dirSize == DIR_SIZE); // 10 is the number of files that should be in the directory after all other testing
#endif
		//Fail cases
		//This function should not fail.

		SECTION("Getting all files in the directory.", "[GetFilesFromDirectory]")
		{
			//Create a string buffer to hold our filenames
			int MatchNum = 0;
			char* files[DIR_SIZE];
			char* Checklist[DIR_SIZE];

			for (unsigned int i = 0; i < dirSize; ++i)
				files[i] = new char[FILE_NAME_SIZE];

			//Pass cases
			REQUIRE(+(file.GetFilesFromDirectory(files, dirSize, FILE_NAME_SIZE)));

			Checklist[0] = (char*)"testFile1.test";
			Checklist[1] = (char*)"testFile2.test";
			Checklist[2] = (char*)"testFile3.test";
			Checklist[3] = (char*)"testFile4.test";
			Checklist[4] = (char*)"testFile5.test";
			Checklist[5] = (char*)"testFile6.test";
			Checklist[6] = (char*)"testFile7.test";
			Checklist[7] = (char*)"testFile8.test";
			Checklist[8] = (char*)"testFile9.test";
			Checklist[9] = (char*)"testFile10.test";

			for (int i = 0; i < DIR_SIZE; i++)
			{
				for (int j = 0; j < DIR_SIZE; j++)
				{
					if (strcmp(files[i], Checklist[j]) == 0)
					{
						MatchNum++;
						break;
					}
					else
						continue;
				}
			}
#if defined(__APPLE__) && !TARGET_OS_IOS // iOS doesn't have .DS_STORE files.
			REQUIRE((MatchNum == dirSize || (MatchNum + 2) == dirSize)); //mac sees two extra files
#elif defined(_WIN32) || defined(__LINUX__)
            REQUIRE(MatchNum == dirSize);
#endif
			for (unsigned int i = 0; i < dirSize; ++i)
				delete[] files[i];

			SECTION("Getting file size.", "[GetFileSize]")
			{
				const char * testFile = "testFile1.test";
				const char * fakeFile = "fakeFile3.test";
				unsigned int sizeOfFile = 0;
				
				//Pass cases
				REQUIRE(+(file.GetFileSize(testFile, sizeOfFile)));
				REQUIRE(sizeOfFile == SIZE_OF_TEST_FILE); // this size

				//Fail cases
				CHECK(file.GetFileSize(fakeFile, sizeOfFile) == GW::GReturn::FILE_NOT_FOUND);
			}
		}
	}
}

TEST_CASE("Sub-Directory Handling continued.", "[GetSubDirectorySize], [GetFoldersFromDirectory], [System]")
{
	unsigned int subDirSize;
	GW::SYSTEM::GFile file;
	file.Create();
	file.SetCurrentWorkingDirectory(u8"./");

	SECTION("Getting sub-directory size.", "[GetSubDirectorySize]")
	{
		//Pass cases
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
		winrt::Windows::Storage::StorageFolder installPath = winrt::Windows::ApplicationModel::Package::Current().InstalledLocation();
		winrt::hstring strPath = installPath.Path();
		REQUIRE(+(file.SetCurrentWorkingDirectory(winrt::to_string(strPath + L"\\Resources\\GFile_TestFolder").c_str())));
#elif defined(WIN32)
		REQUIRE(+(file.SetCurrentWorkingDirectory(u8"../../../UnitTests/Resources/GFile_TestFolder")));
#elif defined(__APPLE__) && TARGET_OS_IOS
        REQUIRE(+(file.SetCurrentWorkingDirectory((std::string([[[NSBundle mainBundle] bundlePath] UTF8String]) + u8"/UnitTests/Resources/GFile_TestFolder").c_str())));
#else
		REQUIRE(+(file.SetCurrentWorkingDirectory(u8"../../../UnitTests/Resources/GFile_TestFolder")));
#endif
		REQUIRE(+(file.GetSubDirectorySize(subDirSize)));
		REQUIRE(subDirSize == SUBDIR_SIZE); // 5 is the number of files that should be in the directory after all other testing
        
		//Fail cases
		//This function should not fail.

		SECTION("Getting all folders/sub-directories in the directory.", "[GetFoldersFromDirectory]")
		{
			//Create a string buffer to hold our filenames
			int MatchNum = 0;
			char* subDir[SUBDIR_SIZE];
			char* Checklist[SUBDIR_SIZE];

			for (unsigned int i = 0; i < subDirSize; ++i)
				subDir[i] = new char[SUBDIR_NAME_SIZE];

			//Pass cases
			REQUIRE(+(file.GetFoldersFromDirectory(subDirSize, SUBDIR_NAME_SIZE, subDir)));

			Checklist[0] = (char*)"TestFolder1";
			Checklist[1] = (char*)"TestFolder2";
			Checklist[2] = (char*)"TestFolder3";
			Checklist[3] = (char*)"TestFolder4";
			Checklist[4] = (char*)"TestFolder5";

			for (int i = 0; i < SUBDIR_SIZE; i++)
			{
				for (int j = 0; j < SUBDIR_SIZE; j++)
				{
					if (strcmp(subDir[i], Checklist[j]) == 0)
					{
						MatchNum++;
						break;
					}
					else
						continue;
				}
			}
			REQUIRE(MatchNum == subDirSize);

			for (unsigned int i = 0; i < subDirSize; ++i)
				delete[] subDir[i];
		}
	}
}


#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GFILE) */