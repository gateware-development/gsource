#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GAPP)
#include <iostream>

bool EnableAppTests = false;

TEST_CASE("Check for app support", "[System]")
{
	// This is used over the conventional callng of create as calling Create, creates a weird instance where after it leaves this scope
	// it calls the destructor and is never able to continue as it is waiting for threads to converge
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	
		EnableAppTests = true;
		std::cout << "APP PLATFORM DETECTED: RUNNING ALL GAPP UNIT TESTS" << std::endl;
	
#else
	
		std::cout << "WARNING: APP PLATFORM NOT DETECTED; SKIPPING ALL GAPP UNIT TESTS" << std::endl;
#endif
}

TEST_CASE("Check to see if the app was terminated after a suspend operation", "[PreviousExecutionStateWasTerminated], [System]")
{
	if (EnableAppTests == false)
	{
		std::cout << "App support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	bool terminated = false;
	// Pass cases
	CHECK(+GW::SYSTEM::GApp::PreviousExecutionStateWasTerminated(terminated));
	terminated ? std::cout << "App was terminated after suspend" << std::endl : std::cout << "App was not terminated after suspend" << std::endl;
}

#endif