#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GCONCURRENT)
namespace GCTest
{
	const unsigned int GC_TEST_SIZE = 10000; // drop to 10,000 for VMs
	const unsigned int GC_SECTION_SIZE = 128;
	const unsigned int GC_SPIN_WAIT = 1000000; // 1ms in nanoseconds
	const unsigned int GC_RANDOM_SEED = 42; // repeatable random seed for consistency (degredation tests)
	// some globally accessible data arrays
	unsigned int inputArray[GC_TEST_SIZE] = { 0, };
	unsigned int outputArray[GC_TEST_SIZE] = { 0, };
	// global parallel processing function
	void ComputeNaiveSumOfDigits(	const unsigned int* in, unsigned int* out, 
									unsigned int index, const void* user)
	{
		unsigned int start = *in;
		for (*out = 0; start > 0; (*out) += start, --start) {}
	}

	void factorial(int num, int* result)
	{
		if (num == 0 || num == 1)
			return;
		else
		{
			*result *= num;
			factorial((num - 1), result);
		}
	}

	void swap(int* _one, int* _two)
	{
		int temp = *_one;
		*_one = *_two;
		*_two = temp;
	}

	void bubbleSort(int _numArray[], int _num)
	{
		for (int i = 0; i < _num - 1; i++)
		{
			for (int j = 0; j < _num - i - 1; j++)
			{
				if (_numArray[j] > _numArray[j + 1])
					swap(&_numArray[j], &_numArray[j + 1]);
			}
		}
	}
}

TEST_CASE("Creation and recreation checks", "[Create], [System]")
{
	GW::SYSTEM::GConcurrent create;

	SECTION("Checking false creation")
	{
		printf("RUNNING GCONCURRENT UNIT TESTS... \n");
		printf("Logical cores count : % u\n", (unsigned int)std::thread::hardware_concurrency());

		REQUIRE(+(create.Create(false)));
	}

	SECTION("Checking false recreation")
	{
		REQUIRE(+(create.Create(false)));
	}

	SECTION("Checking true creation")
	{
		REQUIRE(+(create.Create(true)));
	}

	SECTION("Checking true recreation")
	{
		REQUIRE(+(create.Create(true)));
	}
}

TEST_CASE("Branch Singular Testing", "[System]")
{
	GW::SYSTEM::GConcurrent createTrue;
	GW::SYSTEM::GConcurrent createFalse;

	createFalse.Create(false);
	createTrue.Create(true);

	int result = 1;
	int num = 20;
	
	SECTION("Testing Create with True with a factorial function")
	{
		WHEN("I launch a branch singular with a factorial thread")
		{
			THEN("It should pass and run without a hitch")
			{				
				REQUIRE(+(createTrue.BranchSingular([=, &result]() mutable {	GCTest::factorial((num), &result);})));
			}
		}
		WHEN("On createFalse I call converge to bring it all together")
		{
			THEN("It should converge successfully")
			{
				REQUIRE(+(createFalse.Converge(GCTest::GC_SPIN_WAIT)));
			}
		}
	}
}

TEST_CASE("Branch Parallel Testing", "[System]")
{
	GW::SYSTEM::GConcurrent createTrue;
	GW::SYSTEM::GConcurrent createFalse;

	createFalse.Create(false);
	createTrue.Create(true);
	WHEN("I call branch parallel on create true")
	{
		THEN("It should go and start adding up all the spaces faster then if it was a branch singular")
		{
			REQUIRE(+(createTrue.BranchParallel(GCTest::ComputeNaiveSumOfDigits, GCTest::GC_SECTION_SIZE,
				GCTest::GC_TEST_SIZE, nullptr, 0, GCTest::inputArray, 0, GCTest::outputArray)));
		}
	}

	WHEN("On createFalse I call converge to bring it all together")
	{
		THEN("It should converge successfully")
		{
			REQUIRE(+(createFalse.Converge(GCTest::GC_SPIN_WAIT)));
		}
	}
}

TEST_CASE("Recursive Creation Test", "[System]")
{
    std::atomic_int counter = { 0 };
    GW::SYSTEM::GConcurrent testConcurrent;
    std::function<void()> go_on;

    go_on = [&]()
    {
        ++counter;
        if (counter < 5)
        {
            // When the old object destructs it must safely avoid a Converge() Deadlock.
            testConcurrent.Create(false);
            testConcurrent.BranchSingular(go_on);
        }
    };
    // Recursively recreates itself within itself (5 times)
    testConcurrent.Create(false);
    testConcurrent.BranchSingular(go_on);

    // we need to sleep for a moment because the handle to testConcurrent gets replaced
	UTILS::Sleep_Exact(5);

    REQUIRE(counter == 5);
}

TEST_CASE("GConcurrent Event Testing", "[System]")
{
	// generate events
	GW::SYSTEM::GConcurrent testGConcurrent;
	testGConcurrent.Create(false); // events please
	// listen for events
	bool single = false, parallel = false, section = false;
	GW::CORE::GEventResponder listen;
	listen.Create([&](const GW::GEvent& e) {
		GW::SYSTEM::GConcurrent::Events type;
		GW::SYSTEM::GConcurrent::EVENT_DATA data;
		if (+e.Read(type, data)) {
			switch (type) {
			case GW::SYSTEM::GConcurrent::Events::SINGULAR_TASK_COMPLETE:
				single = true;
				break;
			case GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE:
				parallel = true;
				break;
			case GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE:
				section = true;
				break;
			}
		}
	});
	// register
	testGConcurrent.Register(listen);
	// make all events occur
	testGConcurrent.BranchSingular([]() {}); // single
	// parallel function (converting lambda to raw pointer)
	static void(*run)(int&) = { [](int& x)-> void { ++x; } };
	// parallel data, 2 sections of ints
	constexpr unsigned int count = (G_CONCURRENT_AUTO_SECTION >> 2) << 1;
	int array[count] = { 0, };
	// run at least two sections of computation
	testGConcurrent.BranchParallel(run, count, array);
	// wait for all events to happen
	testGConcurrent.Converge(0);
	// did they transpire?
	REQUIRE(single);
	REQUIRE(parallel);
	REQUIRE(section);
}

// Make sure Branch Parallel does not significantly degrade in performance over time
TEST_CASE("Branch Parallel Performance Degredation Testing", "[System]")
{
	GW::SYSTEM::GConcurrent checkOps;
	GW::CORE::GThreadShared safe; 
	REQUIRE(+safe.Create());
	REQUIRE(+checkOps.Create(false));

	//// *DEBUGGING* print how long it took 
	//GW::CORE::GEventResponder rUdone; // switch this to a Queue and cache for printing
	//rUdone.Create([&rUdone, safe](const GW::GEvent& e) mutable {
	//	// print all events as they happen
	//	GW::SYSTEM::GConcurrent::Events ev;
	//	GW::SYSTEM::GConcurrent::EVENT_DATA ed;
	//	if (+e.Read(ev, ed))
	//	{
	//		safe.LockSyncWrite();
	//		switch (ev)
	//		{
	//		case GW::SYSTEM::GConcurrent::Events::SINGULAR_TASK_COMPLETE:
	//			std::cout << "Singular Task (no sections) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
	//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
	//			break;
	//		/*case GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE:
	//			std::cout << "Parallel Section (partial array) from " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
	//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
	//			break;*/
	//		case GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE:
	//			std::cout << "Parallel Task (full array) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
	//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
	//			break;
	//		}
	//		safe.UnlockSyncWrite();
	//	}
	//});
	//checkOps.Register(rUdone);

	//std::random_device rd; // not used for performance testing
	std::mt19937_64 gen(GCTest::GC_RANDOM_SEED); // seed the generator with a constant value for reproducibility
	/* This is where you define the number generator for unsigned long long: */
	std::uniform_int_distribution<unsigned long long> dis;
	// init input array
	for (int i = 0; i < GCTest::GC_TEST_SIZE; ++i) {
		GCTest::inputArray[i] = dis(gen) % 5000 + 5000;
	}

	// test raw operation speed:
	auto start = std::chrono::steady_clock::now();
	for (int i = 0; i < GCTest::GC_TEST_SIZE; ++i) {
		GCTest::ComputeNaiveSumOfDigits(&GCTest::inputArray[i], &GCTest::outputArray[i], i, nullptr);
	}
	auto end = std::chrono::steady_clock::now();
	
	// print information about the tests
	std::cout << "GConcurrent Performance Degredation Tests:" << std::endl;
	std::cout << "Single core test pass (baseline): " 
		<< std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0 
		<< " milliseconds." << std::endl;
	std::cout << "Running all Parallel test passes, please wait..." << std::endl;
		
	// number of tests, must be enough to show a trend beyond noise and cache effects
	const unsigned int testSize = 1000;
	// sections size is artificially reduced to increase the number of sections
	// this will spawn more jobs and increase the chance of performance degredation
	const unsigned int sectionSize = GCTest::GC_SECTION_SIZE / 8;
	// time values to track performance changes
	double first = 0.0;
	double average = 0.0;
	double last = 0.0;
	double trend = 0.0;
	
	for (int i = 0; i < testSize; ++i)
	{
		start = std::chrono::steady_clock::now();
		checkOps.BranchParallel(GCTest::ComputeNaiveSumOfDigits, sectionSize,
			GCTest::GC_TEST_SIZE, nullptr, 0, GCTest::inputArray, 0, GCTest::outputArray);
		checkOps.Converge(GCTest::GC_SPIN_WAIT);
		end = std::chrono::steady_clock::now();
		// print out the time it took
		/*std::cout << "Parallel elapsed time on Pass: " << i << " was: " <<
			std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0 <<
			" milliseconds." << std::endl;*/
		
		// track the time values
		double lapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0;
		average += lapsed;
		// use running average to compute performance trend
		double runnningAverage = average / (i + 1);
		trend += (lapsed - runnningAverage) / runnningAverage;
		// track the first and last times
		if (i == 0)
			first = lapsed;
		else if (i == testSize - 1)
			last = lapsed;
	}
	// show time differences
	std::cout << "\tFirst test pass: " << first << " milliseconds." << std::endl;
	std::cout << "\tLast test pass: " << last << " milliseconds." << std::endl;
	std::cout << "\tAverage test pass: " << average / testSize << " milliseconds." << std::endl;

	// find ratio of first to average and last to average
	double ratioFirst = first / (average / testSize);
	double ratioLast = last / (average / testSize);
	double firstDeviation = (ratioFirst - 1.0) * 100;
	double lastDeviation = (ratioLast - 1.0) * 100;
	double lastFirstDeviation = ((last / first) - 1.0) * 100;
	std::cout << "\tFirst test pass difference compared to Average: " << firstDeviation << "%" << std::endl;
	std::cout << "\tLast test pass difference compared to Average: " << lastDeviation << "%" << std::endl;
	std::cout << "\tLast test pass difference compared to First: " << lastFirstDeviation << "%" << std::endl;
	// show the trend
	trend = (trend / testSize) * 100;
	std::cout << "\tChange in performance trend over time: " << trend << "% (0% or less is ideal)" << std::endl;

	// Check the results fall within acceptable ranges
	CHECK(trend < 10.0); // 10% worse is the maximum acceptable performance trend from run to run
	CHECK(firstDeviation < 100.0); // 100% worse is the maximum acceptable deviation on first run (cold cache)
	CHECK(lastDeviation < 100.0); // 100% worse is the maximum acceptable deviation from last to average
	CHECK(lastFirstDeviation < 100.0); // 100% worse is the maximum acceptable deviation from last to first
}

//function takes, output input integer
//
//void Incrememter(const int* in, int* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	*out = (*out) + 1;
//};
//TEST_CASE("Branch Paralell Range Test 1")
//{
//	//Make an array of 500 integers
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	//assign all to 1,2,3 etc, count up
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//
//	//branch parallell
//	//run function on array
//	testConcurrent.Create(true);
//	//true is supresss events, false is not
//
//	//SHOULD I USE WHEN THEN?
//	WHEN("I pass in array once as out put, and STATIC CAST nullptr")
//	{
//		THEN("I should have a successful GReturn")
//		{
//			//CURRENTLY HAVE TO STATIC CAST NULLPTR HERE
//			REQUIRE(+(testConcurrent.BranchParallel(Incrememter, GCTest::GC_SECTION_SIZE,
//				arrSize, nullptr, 0, static_cast<int*>(nullptr), 0, myInts)));
//		}
//	}
//
//	//after, run .converge
//	testConcurrent.Converge(0);
//
//	//check all values in array to make sure its expected results (i++ or i--)
//	WHEN("I loop through int array")
//	{
//		THEN("All values should be 1+ their original values")
//		{
//			for (int i = 0; i < arrSize; i++)
//				REQUIRE(myInts[i] == (i + 1));
//		}
//	}
//}

//
//
////byte width testing, jumping
//
//void Incrememter2(const int* in, int* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	(*out)++;
//};
//TEST_CASE("Branch Paralell Range Test 2")
//{
//
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//
//
//	testConcurrent.Create(true);
//	//true is supresss events, false is not
//
//
//	WHEN("I pass in array once as out put, and STATIC CAST nullptr")
//	{
//		THEN("I should have a successful GReturn")
//		{
//			//CURRENTLY HAVE TO STATIC CAST NULLPTR HERE
//			REQUIRE(+(testConcurrent.BranchParallel(Incrememter2, 125,
//				250, nullptr, (sizeof(int)*2), static_cast<int*>(nullptr), 0, myInts)));
//		}
//	}
//
//	testConcurrent.Converge(0);
//
//	//check all values in array to make sure its expected results (i++ or i--)
//	WHEN("I loop through int array")
//	{
//		THEN("EVERY OTHER value should be 1+ their original value")
//		{
//			for (int i = 0; i < arrSize; i++)
//				if (i % 2 == 0)
//				{
//					REQUIRE(myInts[i] == (i + 1));
//				}
//				
//		}
//	}
//}
//
////WILL RUN A STRUCT FUNCTION
//struct testInfo
//{
//	int x, y;
//};
//void Incrememter3(const testInfo* in, testInfo* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	(*out).y++;
//};
////ranging
//TEST_CASE("Branch Paralell Range Test STRUCT")
//{
//	const unsigned int arrSize = 500;
//	testInfo myStructs[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	for (int i = 0; i < arrSize; i++)
//	{
//		myStructs[i].x = i;
//		myStructs[i].y = i;
//	}
//		
//
//
//	testConcurrent.Create(true);
//	//true is supresss events, false is not
//
//
//	WHEN("I pass in array once as out put, and STATIC CAST nullptr")
//	{
//		THEN("I should have a successful GReturn")
//		{
//			//CURRENTLY HAVE TO STATIC CAST NULLPTR HERE
//			REQUIRE(+(testConcurrent.BranchParallel(Incrememter2,125,
//				250, nullptr, (sizeof(testInfo) * 2), static_cast<int*>(nullptr), 0, myStructs)));
//		}
//	}
//
//	testConcurrent.Converge(0);
//
//	//check all values in array to make sure its expected results (i++ or i--)
//	WHEN("I loop through struct array")
//	{
//		THEN("EVERY OTHER value should be 1+ their original value")
//		{
//			for (int i = 0; i < arrSize; i++)
//				if (i % 2 == 0)
//				{
//					REQUIRE(myStructs[i].y == (myStructs[i].x + 1));
//				}
//
//		}
//	}
//}
//
//void Incrememter4(const int* in, int* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	(*out)++;
//};
//TEST_CASE("Basic PARAMETER Negative Tests 1 ")
//{
//	//Make an array of 500 integers
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	int myOuts[100]; 
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	//assign all to 1,2,3 etc, count up
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//	for (int i = 0; i < 100; i++)
//		myOuts[100] = i;
//	//branch parallell
//	//run function on array
//	testConcurrent.Create(true);
//
//	//EXAMPLE in was half an array, second is whole array//
//
//
//	REQUIRE(testConcurrent.BranchParallel(nullptr, GCTest::GC_SECTION_SIZE,
//		arrSize, nullptr, 0, static_cast<int*>(nullptr), 0, myInts) == GW::GReturn::INVALID_ARGUMENT); //nullptr function
//	REQUIRE(testConcurrent.BranchParallel(Incrememter4, 0,
//		arrSize, nullptr, 0, static_cast<int*>(nullptr), 0, myInts) == GW::GReturn::INVALID_ARGUMENT); //section size 0
//	REQUIRE(testConcurrent.BranchParallel(Incrememter4, GCTest::GC_SECTION_SIZE,
//		0, nullptr, 0, static_cast<int*>(nullptr), 0, myInts) == GW::GReturn::INVALID_ARGUMENT); //array size 0
//	REQUIRE(testConcurrent.BranchParallel(Incrememter4, GCTest::GC_SECTION_SIZE,
//		arrSize, nullptr, 0, myInts, 0, myInts) == GW::GReturn::INVALID_ARGUMENT);//in array == out array
	//REQUIRE(testConcurrent.BranchParallel(Incrememter4, GCTest::GC_SECTION_SIZE,
	//	arrSize, nullptr, 0, myOuts, 0, myInts) == GW::GReturn::MEMORY_CORRUPTION); //Memory overlap
	//REQUIRE(testConcurrent.BranchParallel(Incrememter4, GCTest::GC_SECTION_SIZE,
	//	arrSize, nullptr, 0, myInts, 0, myOuts) == GW::GReturn::MEMORY_CORRUPTION); 




//}
//
////all messages happen, TEST EVENT SYSTEM
//void Incrememter5(const int* in, int* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	(*out)++;
//};
//TEST_CASE("Branch Paralell Event System")
//{
//	//ASK FOR OVERVIEW OF MESSAGE SYSTEM IN GCONCURRENT
//	//Create event reciever
//	GW::CORE::GEventReceiver msgTest;
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//	bool par_task = false;
//	bool multiple = false;
//	int par_sect = 0;
//
//	//assign all to 1,2,3 etc, count up
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//	testConcurrent.Create(false);
//
//	msgTest.Create(testConcurrent, [&]() {
//		if (+msgTest.Find(GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE)) //SHOULD BE 1
//		{
//			//test event hit
//			if (par_task)
//			{
//				multiple = true;
//			}
//			par_task = true;
//		}
//		if (+msgTest.Find(GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE)) //EACH ARE 125, SHOULD BE 4 TOTAL
//		{
//			par_sect++;
//		}
//
//	});
//
//	REQUIRE(+(testConcurrent.BranchParallel(Incrememter5, 125,
//		arrSize, nullptr, 0, static_cast<int*>(nullptr), 0, myInts))); //should have 4 sections
//	testConcurrent.Converge(0);
//	REQUIRE(par_task == true);
//	REQUIRE(multiple == false);
//	REQUIRE(par_sect == 4);
//
//
//	
//	//AFTER take out, converge should be redundant
//}
//
//void Incrememter6(const int* in, int* out,
//	unsigned int index, const void* user)
//{
//	//increment or decrement
//	(*out)++;
//};
//TEST_CASE("Branch Paralell Event System NEGATIVE TEST")
//{
//	//ASK FOR OVERVIEW OF MESSAGE SYSTEM IN GCONCURRENT
//	//Create event reciever
//	GW::CORE::GEventReceiver msgTest;
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	int myOut[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	//assign all to 1,2,3 etc, count up
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//	testConcurrent.Create(false);
//
//	//EXAMPLE in was half an array, second is whole array//
//
//
//	msgTest.Create(testConcurrent, [&]() {
//		if (+msgTest.Find(GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE)) //SHOULD BE 500, 125 per SECTION
//		{
//			//test event hit
//			//REQUIRE()
//		}
//		if (+msgTest.Find(GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE))//EACH ARE 125, SHOULD BE 4 TOTAL
//		{
//			//test event hit
//			//REQUIRE()	
//		}
//	});
//
//	REQUIRE(+(testConcurrent.BranchParallel(Incrememter6, 125,
//		arrSize, nullptr, 0, myInts[249], 0, myOut))); //SHOULD FAIL, ask
//
//	testConcurrent.Converge(0);
//
//}
//
//
//TEST_CASE("Branch Singular Event System")
//{
//	//ASK FOR OVERVIEW OF MESSAGE SYSTEM IN GCONCURRENT
//	//Create event reciever
//	GW::CORE::GEventReceiver msgTest;
//	const unsigned int arrSize = 500;
//	int myInts[arrSize];
//	GW::SYSTEM::GConcurrent testConcurrent;
//
//	//assign all to 1,2,3 etc, count up
//	for (int i = 0; i < arrSize; i++)
//		myInts[i] = i;
//	testConcurrent.Create(false);
//
//	//EXAMPLE in was half an array, second is whole array//
//
//
//	msgTest.Create(testConcurrent, [&]() {
//		if (+msgTest.Find(GW::SYSTEM::GConcurrent::Events::SINGULAR_TASK_COMPLETE)) //SHOULD BE 500, 125 per SECTION
//		{
//			
//			//REQUIRE()
//		}
//		});
//
//	testConcurrent.BranchSingular(Incrememter4);
//
//	testConcurrent.Converge(0);
//
//}



	//Things that are supposed to fail
//BRANCH PARALLEL nullptr for function, should NOT work

//WRITE NEGATIVE TESTS
//EXAMPLE in was half an array, second is whole array//
//FINISH EVENT TEST, BOTH POSITIVE and NEGATIVE
 //BRANCH SINGULAR AND PARALLEL
//FIRST DRAFT OF NEW FUNCTION
//byte width testing, jumping
//ranging
//all messages happen, TEST EVENT SYSTEM
	

//TEST_CASE("This is not a real unit test, just testing basic functionality as I develop, thorough testing later")
//{
//	GW::SYSTEM::GConcurrent checkOps;
//	GW::CORE::GThreadShared safe; 
//	safe.Create();
//	checkOps.Create(false);
//
//	// print how long it took 
//	GW::CORE::GEventReceiver rUdone; // switch this to a Queue and cache for printing
//	rUdone.Create(checkOps, [&rUdone, safe]() mutable {
//		// print all events as they happen
//		GW::GEvent e;
//		GW::SYSTEM::GConcurrent::Events ev;
//		GW::SYSTEM::GConcurrent::EVENT_DATA ed;
//		if (+rUdone.Pop(e) && +e.Read(ev, ed))
//		{
//			safe.LockSyncWrite();
//			switch (ev)
//			{
//			case GW::SYSTEM::GConcurrent::Events::SINGULAR_TASK_COMPLETE:
//				std::cout << "Singular Task (no sections) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//					" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//				break;
//				//case GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE:
//				//	std::cout << "Parallel Section (partial array) from " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//				//		" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//				//	break;
//			case GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE:
//				std::cout << "Parallel Task (full array) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//					" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//				break;
//			}
//			safe.UnlockSyncWrite();
//		}
//	});
//
//	std::random_device rd;
//	std::mt19937_64 gen(rd());
//	/* This is where you define the number generator for unsigned long long: */
//	std::uniform_int_distribution<unsigned long long> dis;
//	for (int i = 0; i < 10; ++i) {
//		checkOps.BranchSingular([=, &gen]() mutable {
//			std::this_thread::sleep_for(std::chrono::nanoseconds(dis(gen)) % 100000);
//			safe.LockSyncWrite();
//				std::cout << "I'm on another thread BRO!!! Thread: " << i << std::endl;
//				std::cout.flush();
//			safe.UnlockSyncWrite();
//		});
//	}
//	// wait so we don't get leftover messages below
//	checkOps.Converge(GCTest::GC_SPIN_WAIT);
//
//	// init input array
//	for (int i = 0; i < GCTest::GC_TEST_SIZE; ++i)
//		GCTest::inputArray[i] = dis(gen) % 100000;
//
//	// test raw operation speed:
//	//auto start = std::chrono::steady_clock::now();
//	//for (int i = 0; i < GCTest::GC_TEST_SIZE; ++i)
//	//	GCTest::ComputeNaiveSumOfDigits(i, &GCTest::inputArray[i], &GCTest::outputArray[i]);
//	//auto end = std::chrono::steady_clock::now();
//	//std::cout << "Raw elapsed time: " <<
//	//	std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0 <<
//	//	" milliseconds." << std::endl;
//
//	// Parallel speed	
//	checkOps.BranchParallel(GCTest::ComputeNaiveSumOfDigits, GCTest::GC_SECTION_SIZE, 
//							GCTest::GC_TEST_SIZE, nullptr, GCTest::inputArray, GCTest::outputArray);
//
//	//// We have to wait here otherwise things we want to use to print with will fall out of scope (ex: rUdone)
//	checkOps.Converge(GCTest::GC_SPIN_WAIT); // comment in to get last events
//	//auto end = std::chrono::steady_clock::now();
//	//std::cout << "Raw elapsed time: " <<
//	//	std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0 <<
//	//	" milliseconds." << std::endl;
//
//
//	// Print all ququed messages
//	GW::GEvent e;
//	GW::SYSTEM::GConcurrent::Events ev;
//	GW::SYSTEM::GConcurrent::EVENT_DATA ed;
//	while (+rUdone.Pop(e) && +e.Read(ev, ed))
//	{
//		safe.LockSyncWrite();
//		switch (ev)
//		{
//		case GW::SYSTEM::GConcurrent::Events::SINGULAR_TASK_COMPLETE:
//			std::cout << "Singular Task (no sections) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//			break;
//		case GW::SYSTEM::GConcurrent::Events::PARALLEL_SECTION_COMPLETE:
//			std::cout << "Parallel Section (partial array) from " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//			break;
//		case GW::SYSTEM::GConcurrent::Events::PARALLEL_TASK_COMPLETE:
//			std::cout << "Parallel Task (full array) " << ed.completionRange[0] << " to " << ed.completionRange[1] <<
//				" completed in: " << ed.microsecondsElapsed / 1000.0 << " miliseconds." << std::endl;
//			break;
//		}
//		safe.UnlockSyncWrite();
//	}
//	
//	// contents of output (takes too long :P)
//	//for (int i = 0; i < 10000; ++i)
//	//	std::cout << "sum of index " << i << " is " << GCTest::outputArray[i] << std::endl;
//
//	// We have to wait here otherwise things we want to use to print with will fall out of scope (ex: rUdone)
//	checkOps.Converge(GCTest::GC_SPIN_WAIT); // This only applies to concurrent printing (not GEventQueue as used)
//}

//// perf_info
///*
//	DEBUG: 1 million items 1 million section size via branchparrallel: 514455 milliseconds
//	DEBUG: 1 million items 128 section size via branchparrallel: 22238 milliseconds
//	Result: 23x Faster
//
//	RELEASE: 1 million items 1 million section size via branchparrallel: 401555 milliseconds
//	RELEASE: 1 million items 128 section size via branchparrallel: 16492 milliseconds
//	Result:  24x Faster
//
//	RELEASE(no profiling): 1 million items Naive array traversal: 227491 milliseconds
//	RELEASE(no profiling): 1 million items 1 million section size via branchparrallel: 228247 milliseconds
//	RELEASE(no profiling): 1 million items 128 section size via branchparrallel: 13525 milliseconds
//	Result:  17x Faster, 0.33% overhead when running one section alone compared to raw
//*/
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GCONCURRENT) */
