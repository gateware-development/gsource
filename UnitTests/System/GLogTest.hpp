#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GLOG)
// Custom Unit Tests specific to this interface follow..
//Testing creating a GLog
TEST_CASE("Creating a GLog.", "[CreateGLog], [System]")
{
	GW::SYSTEM::GLog log;
	GW::SYSTEM::GFile gfile;

	REQUIRE(-(log.Create(gfile)));

	REQUIRE(+(log.Create("./DONOTDELETE.txt")));

	REQUIRE(+(gfile.Create()));
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	gfile.SetCurrentWorkingDirectory(winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str());
#elif defined(WIN32)
	gfile.SetCurrentWorkingDirectory(u8"./");
#elif __APPLE__
	gfile.SetCurrentWorkingDirectory(u8"../../../../Xcode/Unit Tests");
#elif __linux__
	gfile.SetCurrentWorkingDirectory(u8"./");
#endif
	gfile.OpenTextWrite("DONOTDELETE.txt");
}

TEST_CASE("Testing logging functions", "[Log], [LogCategorized], [System]")
{
	GW::SYSTEM::GLog log;
	GW::SYSTEM::GFile gfile;

	REQUIRE(+(log.Create("./DONOTDELETE.txt")));
	REQUIRE(+(gfile.Create()));

	//Fail cases
	REQUIRE(log.Log(nullptr) == GW::GReturn::INVALID_ARGUMENT);
	REQUIRE(log.LogCategorized(nullptr, "Something") == GW::GReturn::INVALID_ARGUMENT);
	REQUIRE(log.LogCategorized("Something", nullptr) == GW::GReturn::INVALID_ARGUMENT);

	//Pass cases
	REQUIRE(+(log.Log("Something I wanna do.")));
	//Turn verbose logging off and try again
	log.EnableVerboseLogging(false);
	REQUIRE(+(log.Log("Something else I wanna do.")));

	REQUIRE(+(log.LogCategorized("MINE", "Something I wanna do more")));
	//Turn verbose logging on and try again
	log.EnableVerboseLogging(true);
	REQUIRE(+(log.LogCategorized("MINE", "Something I wanna do more")));

	//Test flush
	REQUIRE(+(log.Flush()));
}
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GLOG) */