TEST_CASE("GWindow core method test battery", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	GW::CORE::GInterface emptyInterface;
	unsigned int listenerCount = 0;
	unsigned int width = 0, height = 0;
	unsigned int clientWidth = 0, clientHeight = 0;
	unsigned int x = 0, y = 0;
	GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE universalWindowHandle;
	bool fullscreen = false;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(window.ProcessWindowEvents() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.SetWindowName("EMPTY WINDOW") == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.MoveWindow(0, 0) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ResizeWindow(1920, 1080) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Maximize() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Minimize() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetWidth(width) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetHeight(height) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientWidth(clientWidth) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientHeight(clientHeight) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetX(x) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetY(y) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetClientTopLeft(x, y) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.GetWindowHandle(universalWindowHandle) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.IsFullscreen(fullscreen) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Observers(listenerCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(window.Register(emptyInterface, nullptr) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		window = nullptr;
		REQUIRE(!window);
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

		REQUIRE(window.ProcessWindowEvents() == GW::GReturn::SUCCESS);
#if !TARGET_OS_IOS // iOS cannot reconfig windows.
		REQUIRE(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(window.SetWindowName("VALID WINDOW") == GW::GReturn::SUCCESS);
#else
		REQUIRE(window.ReconfigureWindow(0, 0, 1920, 1080, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::FEATURE_UNSUPPORTED);
		REQUIRE(window.SetWindowName("VALID WINDOW") == GW::GReturn::FEATURE_UNSUPPORTED);
#endif
		REQUIRE(window.MoveWindow(0, 0) == GW::GReturn::FEATURE_UNSUPPORTED);
#if !TARGET_OS_IOS
		GAMING_DEVICE_MODEL_INFORMATION info = {};
		GetGamingDeviceModelInformation(&info);
		if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
		{
			switch (info.deviceId)
			{
			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:

			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:

			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:

			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
				// this is a good thing to fail here
				REQUIRE(G_FAIL(window.ResizeWindow(1920 >> 1, 1080 >> 1)));
			default:
				break;
			}
		}
		else
		{
			REQUIRE(window.ResizeWindow(1920 >> 1, 1080 >> 1) == GW::GReturn::SUCCESS);
		}

		REQUIRE(+window.Maximize());
		REQUIRE(window.Minimize() == GW::GReturn::FEATURE_UNSUPPORTED);
		REQUIRE(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
#elif TARGET_OS_IOS
		REQUIRE(window.Maximize() == GW::GReturn::FEATURE_UNSUPPORTED);
		REQUIRE(window.Minimize() == GW::GReturn::FEATURE_UNSUPPORTED);
		REQUIRE(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::FEATURE_UNSUPPORTED);
#endif
		REQUIRE(window.GetWidth(width) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetHeight(height) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientWidth(clientWidth) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientHeight(clientHeight) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetX(x) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetY(y) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetClientTopLeft(x, y) == GW::GReturn::SUCCESS);
		REQUIRE(window.GetWindowHandle(universalWindowHandle) == GW::GReturn::SUCCESS);
		REQUIRE(window.IsFullscreen(fullscreen) == GW::GReturn::SUCCESS);
		REQUIRE(window.Observers(listenerCount) == GW::GReturn::SUCCESS);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::REDUNDANT);
		REQUIRE(emptyInterface.Create() == GW::GReturn::SUCCESS);
		REQUIRE(window.Register(emptyInterface, [](const GW::GEvent& event, GW::CORE::GInterface& observer) { return; }) == GW::GReturn::SUCCESS);
		REQUIRE(window.Push(GW::GEvent()) == GW::GReturn::SUCCESS);
	}
}

#if !TARGET_OS_IOS
TEST_CASE("Resizing Window.", "[ResizeWindow], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Pass cases
	GAMING_DEVICE_MODEL_INFORMATION info = {};
	GetGamingDeviceModelInformation(&info);
	if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
	{
		switch (info.deviceId)
		{
		case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:

		case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:

		case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:

		case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
			// this is a good thing to fail here
			REQUIRE(G_FAIL(window.ResizeWindow(300, 300)));
			REQUIRE(G_PASS(window.Maximize()));
		default:
			break;
		}
	}
	else
	{
		REQUIRE(G_PASS(window.ResizeWindow(300, 300)));
		REQUIRE(G_PASS(window.Maximize()));
	}

}

TEST_CASE("Creating windows using different flags", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // Ignore this test
	}
	GW::SYSTEM::GWindow window;

#if defined(__APPLE__)
	constexpr int delayTime = 2500; // Window's animating to and from Minimized and Maximized states need more time to finish.
	constexpr int delayWindowTime = 5000;
#else
	constexpr int delayTime = 500;
	constexpr int delayWindowTime = 1500;
#endif

	auto DelayTest = [&](int delay) {
#if defined(GWINDOWTEST_SLOW_CREATE_TEST)
		auto start = std::chrono::steady_clock::now();

		while (true)
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
#endif
	};

	auto DelayTestAsWindowProcesses = [&](int delay) {
#if defined(GWINDOWTEST_SLOW_CREATE_TEST)
		auto start = std::chrono::steady_clock::now();

		while (+window.ProcessWindowEvents())
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
#endif
	};

	auto PrintCreateMessage = [&](GW::SYSTEM::GWindowStyle _style) {
#if defined(GWINDOWTEST_SLOW_CREATE_TEST)
		const char* msg = "Creating window using GWindowStyle: ";
		switch (_style)
		{
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED:
			std::cout << msg << "WINDOWEDBORDERED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
			std::cout << msg << "WINDOWEDBORDERLESS" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED:
			std::cout << msg << "WINDOWEDLOCKED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED:
			std::cout << msg << "FULLSCREENBORDERED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
			std::cout << msg << "FULLSCREENBORDERLESS" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::MINIMIZED:
			std::cout << msg << "MINIMIZED" << std::endl;
			break;
		}
#endif
	};

	// Pass Cases
	DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	/*DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;*/

	// on MacOS MAXIMIZE/MINIMIZE doesn't seem to work from a logged out runner
#ifndef __APPLE__
	DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;

	/*DelayTest(delayTime);
	PrintCreateMessage(GW::SYSTEM::GWindowStyle::MINIMIZED);
	REQUIRE(G_PASS(window.Create(100, 250, 600, 500, GW::SYSTEM::GWindowStyle::MINIMIZED)));
	DelayTestAsWindowProcesses(delayWindowTime);
	window = nullptr;*/
#endif
}

TEST_CASE("Reconfiguring windows created with different styles", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // Ignore this test
	}
	GW::SYSTEM::GWindow window;

#if defined(__APPLE__)
	constexpr int delayTime = 2500; // Window's animating to and from Minimized and Maximized states need more time to finish.
	constexpr int delayWindowTime = 5000;
#else
	constexpr int delayTime = 500;
	constexpr int delayWindowTime = 1500;
#endif

	auto DelayTest = [&](int delay) {
#if defined(GWINDOWTEST_SLOW_RECONFIGURE_TEST)
		auto start = std::chrono::steady_clock::now();

		while (true)
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
#endif
	};

	auto DelayTestAsWindowProcesses = [&](int delay) {
#if defined(GWINDOWTEST_SLOW_RECONFIGURE_TEST)
		auto start = std::chrono::steady_clock::now();

		while (+window.ProcessWindowEvents())
		{
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			if (lapse > delay)
				break;
		}
#endif
	};

	auto PrintCreateMessage = [&](GW::SYSTEM::GWindowStyle _style) {
#if defined(GWINDOWTEST_SLOW_CREATE_TEST)
		const char* msg = "Creating with ";
		switch (_style)
		{
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED:
			std::cout << msg << "WINDOWEDBORDERED";
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
			std::cout << msg << "WINDOWEDBORDERLESS";
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED:
			std::cout << msg << "WINDOWEDLOCKED";
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED:
			std::cout << msg << "FULLSCREENBORDERED";
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
			std::cout << msg << "FULLSCREENBORDERLESS";
			break;
		case GW::SYSTEM::GWindowStyle::MINIMIZED:
			std::cout << msg << "MINIMIZED";
			break;
		}
#endif
	};

	auto PrintReconfigMessage = [&](GW::SYSTEM::GWindowStyle _style) {
#if defined(GWINDOWTEST_SLOW_CREATE_TEST)
		const char* msg = " and reconfiguring with ";
		switch (_style)
		{
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED:
			std::cout << msg << "WINDOWEDBORDERED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
			std::cout << msg << "WINDOWEDBORDERLESS" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED:
			std::cout << msg << "WINDOWEDLOCKED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED:
			std::cout << msg << "FULLSCREENBORDERED" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
			std::cout << msg << "FULLSCREENBORDERLESS" << std::endl;
			break;
		case GW::SYSTEM::GWindowStyle::MINIMIZED:
			std::cout << msg << "MINIMIZED" << std::endl;
			break;
		}
#endif
	};

	// Pass Cases: Test if the window can be created with each style, then reconfigured to another.
	//             Also, tests if the window can be reconfigured the style it is currently without issue.
	constexpr int GWINDOW_STYLE_COUNT = 6;
	for (int i = 0; i < GWINDOW_STYLE_COUNT; ++i)
	{
		GW::SYSTEM::GWindowStyle style = (GW::SYSTEM::GWindowStyle)i;
		// this is done to avoid styles unsupported by UWP
		if (i == 2 || i == 5)
		{
			break;
		}
		for (int j = 0; j < GWINDOW_STYLE_COUNT; ++j)
		{
			GW::SYSTEM::GWindowStyle nextStyle = (GW::SYSTEM::GWindowStyle)j;
			// this is done to avoid styles unsupported by UWP
			if (j == 2 || j == 5)
			{
				break;
			}
			DelayTest(delayTime);
			REQUIRE(G_PASS(window.Create(100, 200, 500, 400, style)));
			DelayTestAsWindowProcesses(delayWindowTime);
			REQUIRE(G_PASS(window.ReconfigureWindow(100, 200, 500, 400, nextStyle)));
			DelayTestAsWindowProcesses(delayWindowTime);

			window = nullptr;
		}
	}
}
#endif // !TARGET_OS_IOS
/*
	This is a test case used internally to test events from gwindow
	This does not have to be enable, but it is a good test case that allows GW Developers
	to see how a event receiver respond to events
*/
#if !defined(DISABLE_USER_INPUT_TESTS)
TEST_CASE("GWINDOW EVENT TEST CASE", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	// GWindow "unit test" Proof Of Concept
	GW::SYSTEM::GWindow window1;
	window1.Create(100, 100, 200, 100, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	GW::CORE::GEventReceiver receiver1;
	receiver1.Create(window1, [&receiver1, &window1]()
		{
			GW::GEvent event;
			receiver1.Pop(event);
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::RESIZE: { std::cout << "RECEIVER 1 RECEIVED RESIZE EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::SUSPEND: { std::cout << "RECEIVER 1 RECEIVED SUSPEND EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::RESUME: { std::cout << "RECEIVER 1 RECEIVED RESUME EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::LEAVING_BACKGROUND: { std::cout << "RECEIVER 1 RECEIVED LEAVING_BACKGROUND EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::ENTERED_BACKGROUND: { std::cout << "RECEIVER 1 RECEIVED ENTERED_BACKGROUND EVENT\n"; } break;
			}
		});

	GW::CORE::GEventReceiver receiver2;
	receiver2.Create(window1, [&receiver2, &window1]()
		{
			GW::GEvent event;
			receiver2.Pop(event);
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			event.Read(windowEvent, windowEventData);
			switch (windowEvent)
			{
			case GW::SYSTEM::GWindow::Events::RESIZE: { std::cout << "RECEIVER 2 RECEIVED RESIZE EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::SUSPEND: { std::cout << "RECEIVER 2 RECEIVED SUSPEND EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::RESUME: { std::cout << "RECEIVER 2 RECEIVED RESUME EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::LEAVING_BACKGROUND: { std::cout << "RECEIVER 2 RECEIVED LEAVING_BACKGROUND EVENT\n"; } break;
			case GW::SYSTEM::GWindow::Events::ENTERED_BACKGROUND: { std::cout << "RECEIVER 2 RECEIVED ENTERED_BACKGROUND EVENT\n"; } break;
			}
		});

	int STAYALIVE = 1;
	while (STAYALIVE)
	{
		STAYALIVE = 0;
		if (+window1.ProcessWindowEvents())
			++STAYALIVE;
	}
}
#endif // !defined(DISABLE_USER_INPUT_TESTS)