#version 450
layout(location = 0) out vec4 FragColor;

layout(location = 0) in vec2 texCoord; //#us_pos.xy from the vert shader should be here
layout(set = 1, binding = 0) uniform sampler2D tex;

void main(void)
{
    vec4 color = texture(tex, texCoord);
    FragColor = color.yzwx;
}
