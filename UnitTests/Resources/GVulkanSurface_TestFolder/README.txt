To edit shaders, Open the shader.frag or shader.vert file. When you are done editing, run the compile.bat file.
NOTE: It is possible that your VulkanSDK does not match the exact SDK in the file, if so, change the \x.x.x.x\ section to your version of the SDK.

basicOriginalfrag and basicOriginalvert are guaranteed to work, as they're the designed for the triangle test specifically