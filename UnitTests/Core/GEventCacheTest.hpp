#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTCACHE)

#include "GEventCommonIncludes.h"

TEST_CASE("GEventCache core method test battery", "[Core]")
{
	GW::CORE::GEventCache eventReceiver;
	
	GW::CORE::GEventGenerator eventGenerator;
	GW::GEvent event;
	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(eventReceiver.Append(event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Clear() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Find(API::DirectX, false) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Find(API::DirectX, false, g_outEnum) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Missed(missedEventCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Peek(event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Pop(event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventReceiver.Waiting(waitingEventCount) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS); // Need a valid event generator
		REQUIRE(eventReceiver.Create(eventGenerator) == GW::GReturn::SUCCESS);
		eventReceiver = nullptr; // Destruction
		REQUIRE(!eventReceiver); // Invalidation check
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS); // Need a valid event generator

		REQUIRE(eventReceiver.Create(eventGenerator) == GW::GReturn::SUCCESS);
		REQUIRE(event.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS); // Write an event and add it to the listener
		REQUIRE(eventReceiver.Append(event) == GW::GReturn::SUCCESS);
		REQUIRE(eventReceiver.Find(API::DirectX, false) == GW::GReturn::SUCCESS);
		REQUIRE(eventReceiver.Find(API::DirectX, false, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(eventReceiver.Peek(event) == GW::GReturn::SUCCESS);
		REQUIRE(event.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventReceiver.Pop(event) == GW::GReturn::SUCCESS);
		REQUIRE(event.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventReceiver.Append(event) == GW::GReturn::SUCCESS);
		REQUIRE(eventReceiver.Clear() == GW::GReturn::SUCCESS); // Removes all events
		REQUIRE(eventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 0); // waitingEventCount should be 0 after GEventCache::Clear() call
	}
	ResetGlobals();
} // End of test case GEventCache core method test battery

TEST_CASE("GEventCache test using GEvent and GEventGenerator", "[Core]")
{
	GW::GEvent apiEvent;
	GW::CORE::GEventGenerator EventGenerator;
	GW::CORE::GEventCache EventReceiver;

	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;

	// Create an event generator that will be used to generate events
	// and sends them out to all listeners
	REQUIRE(EventGenerator.Create() == GW::GReturn::SUCCESS);
	// Create an event receiver that will receive events from event generator
	// callback function does nothing
	REQUIRE(EventReceiver.Create(EventGenerator) == GW::GReturn::SUCCESS);
	REQUIRE(EventGenerator.Register(EventReceiver) == GW::GReturn::SUCCESS); // must be registered

	SECTION("Testing pop and peek for event receivers")
	{
		//----------------------------------------------------------------------------------
		REQUIRE(apiEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::SUCCESS);

		REQUIRE(EventReceiver.Peek(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(apiEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));

		// Currently we have no missing events because only 1 Push() has been called
		// There is a event waiting because we havent popped yet, every time new events has been push
		// it will mark event waiting as 1, unless we pop it
		REQUIRE(EventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);

		//----------------------------------------------------------------------------------
		REQUIRE(apiEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::SUCCESS);

		// Previously we only peeked at an event, so when we push again, previous event will be missed
		// and event missed count should equal to 1
		// and the event waiting should be 1 as well because we havent popped yet
		REQUIRE(EventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 1);
		REQUIRE(EventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);

		// Now we are popping the recent event, waiting events should be 0 now
		// missed event should still be 1 (it is a "tracker" for how many events this receiver
		// has missed during its life time
		REQUIRE(EventReceiver.Pop(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(EventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 1);
		REQUIRE(EventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 0);

		REQUIRE(apiEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::Vulkan);
		REQUIRE((g_outData == g_pods[1]));

		//----------------------------------------------------------------------------------
		REQUIRE(apiEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::SUCCESS);

		REQUIRE(EventReceiver.Pop(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(apiEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));

		// Now we pushed a new event we are going to pop it, since we are popping it no more events
		// will be waiting, missing will still be 1
		REQUIRE(EventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 1);
		REQUIRE(EventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 0);

		//----------------------------------------------------------------------------------
		REQUIRE(apiEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventReceiver.Append(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(EventReceiver.Append(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(EventReceiver.Append(apiEvent) == GW::GReturn::SUCCESS);

		// Now we are forcing the receiver to append new events,
		// event missed should be 3 because we append 3 times but we havent dealt with
		// the recent event so it will be 2 + 1 = 3, the + 1 is from previous tests
		REQUIRE(EventReceiver.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 3);
		REQUIRE(EventReceiver.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);
	}
}
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTCACHE) */