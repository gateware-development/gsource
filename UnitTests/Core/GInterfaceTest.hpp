#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GINTERFACE)

TEST_CASE("GInterface Creation and Recreation Checks", "[Core]")
{
	// Making the things I need Por favor
	GW::CORE::GInterface Create;
	GW::CORE::GInterface Copy;

	SECTION("Creating an Interface")
	{
		REQUIRE(Create.Create() == GW::GReturn::SUCCESS);

		SECTION("Recreation check")
		{
			Copy = Create;

			WHEN("Create is called on an interface that is already created")
			{
				REQUIRE(Create.Create() == GW::GReturn::SUCCESS);

				THEN("The newly created interface is different from before")
				{
					REQUIRE(Create != Copy);
				}
			}
		}
	}
}

TEST_CASE("GInterface Mirror Checks", "[Core]")
{
	// Making the things I need
	GW::CORE::GInterface Create;
	GW::CORE::GInterface Mirror;
	GW::CORE::GInterface Empty;
	GW::CORE::GInterface Copy;

	SECTION("General Mirror Tests")
	{
		WHEN("Mirror an empty object with another empty object")
		{
			REQUIRE(!Empty);
			REQUIRE(!Mirror);

			Mirror = Empty.Mirror();

			THEN("The mirrored object should remain empty")
			{
				REQUIRE(Mirror == Empty);
				REQUIRE(!Mirror);
			}
		}

		WHEN("Mirror an empty object with a created object")
		{
			Create.Create();
			Mirror = Create.Mirror();

			THEN("The mirrored object should become the original")
			{
				REQUIRE(Create == Mirror);
			}
		}

		WHEN("Mirror a non-empty object with an empty object")
		{
			REQUIRE(!Empty);
			Create.Create();
			Mirror = Create.Mirror();
			Mirror = Empty.Mirror();

			THEN("The Mirror should become empty since the object it is mirroring is empty")
			{
				REQUIRE(Mirror == Empty);
				REQUIRE(Mirror != Create);
			}
		}

		WHEN("Mirror a non empty object with a different non empty object")
		{
			Mirror = Create.Mirror();
			Copy = Create;
			Create.Create();
			Mirror = Create.Mirror();

			THEN("The Mirror should become the new object and not have any relation to the old one")
			{
				REQUIRE(Copy != Create);
				REQUIRE(Mirror == Create);
				REQUIRE(Mirror != Copy);
			}
		}
	}
}

TEST_CASE("GInterface Share Checks", "[Core]")
{
	// Making the things I need
	GW::CORE::GInterface Create;
	GW::CORE::GInterface Share1;
	GW::CORE::GInterface Share2;
	GW::CORE::GInterface Copy;
	GW::CORE::GInterface Empty;

	SECTION("General Share Checks")
	{
		WHEN("Share an empty object with another empty object")
		{
			REQUIRE(!Empty);

			Share1 = Empty.Share();

			THEN("The Object remains empty")
			{
				REQUIRE(!Share1);
			}
		}

		WHEN("Share an empty object with a non-empty one")
		{
			Create.Create();

			REQUIRE(!Share1);
			REQUIRE(Create);

			Share1 = Create.Share();

			THEN("The Shared object should have a weak pointer to the object")
			{
				REQUIRE(Share1 == Create);
			}
		}

		WHEN("Share a non-empty object with an empty one")
		{
			REQUIRE(!Empty);
			Share1 = Empty.Share();

			THEN("The shared object should have a weak pointer to nullptr")
			{
				REQUIRE(Share1 == Empty);
			}
		}

		SECTION("Shareing non-empty objects with another")
		{
			WHEN("Share a non-empty object to another non-empty object")
			{
				Share1 = Create;
				Create.Create();

				Share1 = Create.Share();

				THEN("It should have a weak pointer to the object")
				{
					REQUIRE(Share1 == Create);

				}
				WHEN("You share an object off of another shared object")
				{
					Share2 = Share1.Share();

					THEN("It should also have a weak pointer to the original object")
					{
						REQUIRE(Share2 == Create);

					}

					WHEN("You change the orignal with another create")
					{
						Copy = Create;
						Create.Create();
						REQUIRE(Copy != Create);

						THEN("All the shared objects should not change to the new object")
						{
							REQUIRE(Share1 != Create);
							REQUIRE(Share2 != Create);
							REQUIRE(Share1 == Copy);
							REQUIRE(Share2 == Copy);
						}
					}
				}
			}
		}
	}
}

TEST_CASE("GInterface Relinquish Checks", "[Core]")
{
	// Making the things I need
	GW::CORE::GInterface Create;
	GW::CORE::GInterface Relinquish1;
	GW::CORE::GInterface Relinquish2;
	GW::CORE::GInterface Empty;

	SECTION("General Relinquish Checks")
	{
		WHEN("An empty object Relinquishes to an empty object")
		{
			CHECK(!Empty);
			CHECK(!Relinquish1);

			Relinquish1 = Empty.Relinquish();

			THEN("Returns an invalid Proxy since it failed")
			{
				REQUIRE(!Empty);
				REQUIRE(!Relinquish1);
			}
		}

		WHEN("An object relinquishes to an empty object")
		{
			Create.Create();
			Relinquish1 = Create.Relinquish();

			THEN("the strong pointer gets transferred to the object but a weak pointer is shared")
			{
				REQUIRE(Create == Relinquish1);
			}
		}

		WHEN("Object Relinquishes to another object")
		{
			Relinquish2 = Relinquish1.Relinquish();

			THEN("Second object has a weak pointer to the first object which has a weak pointer to the original object")
			{
				REQUIRE(Relinquish1 == Relinquish2);
				REQUIRE(Relinquish2 == Create);
			}
		}
	}
}

namespace GInterfaceUnitTests
{
	// Value
	GW::CORE::GInterface value;
	GW::CORE::GInterface emptyValue;
	// Reference
	GW::CORE::GInterface strongReference;
	GW::CORE::GInterface weakReference;
	GW::CORE::GInterface emptyReference;
	// Pointer
	GW::CORE::GInterface* strongPointer;
	GW::CORE::GInterface* weakPointer;
	GW::CORE::GInterface* emptyPointer;
	// rvalue
	GW::CORE::GInterface rval;
	GW::CORE::GInterface rval2;

	GW::GReturn func(GW::CORE::GInterface _value, GW::CORE::GInterface _emptyValue,
		GW::CORE::GInterface& _strongReference, GW::CORE::GInterface& _weakReference, GW::CORE::GInterface& _emptyReference,
		GW::CORE::GInterface* _strongPointer, GW::CORE::GInterface* _weakPointer, GW::CORE::GInterface* _emptyPointer,
		GW::CORE::GInterface&& _strongRvalue, GW::CORE::GInterface&& _weakRvalue, GW::CORE::GInterface&& _emptyRvalue)
	{
		SECTION("An Object is passed by value")
		{
			WHEN("An object in passed by value")
			{
				THEN("The passed in value should only have a weak pointer")
				{
					REQUIRE(_value.Share() == nullptr);
				}
			}

			WHEN("An empty object in passed by value")
			{
				THEN("The passed in value should be completly empty")
				{
					REQUIRE(!_emptyValue);
				}
			}
		}

		SECTION("An object is passed by Reference")
		{
			WHEN("The object has a weak pointer")
			{
				THEN("The object should have a weak pointer")
				{
					REQUIRE(_weakReference.Share() == nullptr);
				}
			}

			WHEN("The object has a strong pointer")
			{
				THEN("The object passed in should have a strong pointer")
				{
					REQUIRE(_strongReference.Share() != nullptr);
				}
			}

			WHEN("An empty object in passed by reference")
			{
				THEN("An empty reference should be null")
				{
					REQUIRE(!_emptyReference);
				}
			}
		}

		SECTION("An object is passed by Pointer")
		{
			WHEN("The object has a weak pointer")
			{
				THEN("The object should have a weak connection")
				{
					REQUIRE(_weakPointer->Share() == nullptr);
				}
			}

			WHEN("The object has a strong pointer")
			{
				THEN("The object should have a strong connection")
				{
					REQUIRE(_strongPointer->Share() != nullptr);
				}
			}

			WHEN("An empty object in passed by pointer")
			{
				THEN("The object should have no connection")
				{
					REQUIRE((*_emptyPointer) == nullptr);
				}
			}
		}

		SECTION("An object is passed by rValue")
		{
			WHEN("The object has a weak pointer")
			{
				THEN("The object should have a weak connection")
				{
					REQUIRE(_weakRvalue.Share() == nullptr);
				}
			}

			WHEN("The object has a strong pointer")
			{
				THEN("The object should have a strong connection")
				{
					REQUIRE(_strongRvalue.Share() != nullptr);
				}
			}

			WHEN("An empty object in passed by rValue")
			{
				THEN("The object should have no connection")
				{
					REQUIRE(!_emptyRvalue);
				}
			}
		}

		return GW::GReturn::SUCCESS;
	}
}

TEST_CASE("Misc Tests", "[Core]")
{
	SECTION("Copy Constructor")
	{
		GW::CORE::GInterface Create;
		GW::CORE::GInterface Copy;

		WHEN("You copy a object to another one then create on the original object")
		{
			Create.Create();
			Copy = Create;

			REQUIRE(Create == Copy);

			Create.Create();

			THEN("The copy should be different from the newly created one")
			{
				REQUIRE(Copy != Create);
				REQUIRE(!Copy);
				REQUIRE(Copy == nullptr);
			}
		}
	}

	SECTION("Passing in the Ginterface by all 4 argument styles")
	{
		GInterfaceUnitTests::value.Create();

		GInterfaceUnitTests::weakPointer = new GW::CORE::GInterface;
		GInterfaceUnitTests::strongPointer = new GW::CORE::GInterface;
		GInterfaceUnitTests::emptyPointer = new GW::CORE::GInterface;
		GInterfaceUnitTests::weakPointer->Create();
		GInterfaceUnitTests::strongPointer->Create();
		GInterfaceUnitTests::weakReference.Create();

		GInterfaceUnitTests::rval.Create();
		GInterfaceUnitTests::rval2 = GInterfaceUnitTests::rval.Relinquish();

		GInterfaceUnitTests::strongReference = GInterfaceUnitTests::weakReference.Relinquish();
		(*GInterfaceUnitTests::strongPointer) = GInterfaceUnitTests::weakPointer->Relinquish();

		GInterfaceUnitTests::func(GInterfaceUnitTests::value, GInterfaceUnitTests::emptyValue,
			GInterfaceUnitTests::strongReference, GInterfaceUnitTests::weakReference, GInterfaceUnitTests::emptyReference,
			GInterfaceUnitTests::strongPointer, GInterfaceUnitTests::weakPointer, GInterfaceUnitTests::emptyPointer,
			GInterfaceUnitTests::rval2.Relinquish(), GInterfaceUnitTests::rval.Mirror(), GW::CORE::GInterface());

		delete GInterfaceUnitTests::weakPointer;
		delete GInterfaceUnitTests::strongPointer;
		delete GInterfaceUnitTests::emptyPointer;
	}
}

// create a function that takes in all 4 argument styles, by value, reference, by pointer, by rvalue with is double reference
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GINTERFACE) */