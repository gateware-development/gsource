#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GLOGIC)

TEST_CASE("GLogic Creation and Destruction", "[Core]")
{
	GW::CORE::GLogic Logic_obj;
	// Initial check that Logic_obj has not been created yet.
	CHECK(Logic_obj.Invoke() == GW::GReturn::EMPTY_PROXY);

	SECTION("Positive testing of GLogic object")
	{
		REQUIRE(Logic_obj.Create(nullptr) == GW::GReturn::SUCCESS);

		GW::CORE::GLogic SecondLogic_obj;

		WHEN("Weak ownership of valid proxy is shared")
		{
			SecondLogic_obj = Logic_obj;
			THEN("Second ThreadShare Proxy returns success")
			{
				REQUIRE(SecondLogic_obj.Assign([]() {}) == GW::GReturn::SUCCESS);
				REQUIRE(SecondLogic_obj.Invoke() == GW::GReturn::SUCCESS);
			}

			WHEN("Second object is released")
			{
				SecondLogic_obj = nullptr;
				THEN("Second ThreadShare Proxy is empty and original is still valid")
				{
					REQUIRE(SecondLogic_obj.Assign([]() {}) == GW::GReturn::EMPTY_PROXY);
					REQUIRE(SecondLogic_obj.Invoke() == GW::GReturn::EMPTY_PROXY);
					REQUIRE(Logic_obj.Assign([]() {}) == GW::GReturn::SUCCESS);
					REQUIRE(Logic_obj.Invoke() == GW::GReturn::SUCCESS);
				}
			}
		}
	}
	SECTION("Negative testing of GLogic object")
	{
		WHEN("Logic object is set to nullptr")
		{
			Logic_obj = nullptr;
			THEN("Logic_obj should be an empty proxy")
			{
				REQUIRE(Logic_obj.Assign([]() {}) == GW::GReturn::EMPTY_PROXY);
				REQUIRE(Logic_obj.Invoke() == GW::GReturn::EMPTY_PROXY);
			}
		}
	}
}
TEST_CASE("GLogic Behaviors", "[Core]")
{
	SECTION("GLogic Recursion")
	{
		WHEN("A Logic proxy that Invokes itself is created")
		{
			unsigned counter = 0;
			GW::CORE::GLogic logic;
			logic.Create([&]() {
				if (++counter < 100)
					REQUIRE(logic.Invoke() == GW::GReturn::SUCCESS);
			});
			THEN("That Recursion should be allowed on that thread without additional locks")
			{
				REQUIRE(logic.Invoke() == GW::GReturn::SUCCESS);
				REQUIRE(counter == 100);
			}
		}
	}
}
/* DEBUG ONLY TESTS */
#ifndef NDEBUG
// Initial testing of safety behaviors, better test with actual threading are needed
TEST_CASE("GLogic DEADLOCK detection/prevention in DEBUG mode", "[Core]")
{
	SECTION("Testing Deadlock returns")
	{
		WHEN("A Logic object is intialized and invoked")
		{
			GW::CORE::GLogic logic;
			logic.Create([&logic]() {
				REQUIRE(logic.Assign([](){}) == GW::GReturn::DEADLOCK);
			});
			THEN("Attempting to Assign a new routine within the exisitng routine should DEADLOCK")
			{
				logic.Invoke();
			}
		}
	}
}
#endif /* DEBUG ONLY TESTS */
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GLOGIC) */