#ifndef _GEVENTCOMMONINCLUDES_H_
#define _GEVENTCOMMONINCLUDES_H_

#include <iostream>
#include <string>

// enum class for testing purposes (Graphics FOR THE WIN!)
enum class API : int
{
	DirectX,
	Vulkan,
	OpenGL,
	Metal,
	Mantle,
	Count
};

// POD class for testing purposes (stores API data)
struct POD
{
	int id;
	char name[8];
	char developer[32];
	bool operator==(const POD& rhs)
	{
		return id == rhs.id && strcmp(name, rhs.name) == 0 && strcmp(developer, rhs.developer) == 0;
	}
};

/*
!	sizeof(API) == 4 bytes
!	sizeof(POD) == 44 bytes
!	4 + 44 + 2 = 50 == RAW_DATA_PACKET_SIZE // 2 for size markers
*/

POD g_pods[static_cast<int>(API::Count)] =
{
	{ 0, "DirectX", "Microsoft"	    }	,
	{ 1, "Vulkan" , "Khronos Group" }	,
	{ 2, "OpenGL" , "Khronos Group" }	,
	{ 3, "Metal"  , "Apple Inc"     }	,
	{ 4, "Mantle" , "AMD"		    }	,
};
API g_outEnum = API::Count;
POD g_outData = { -1, "NULL", "NULL" };
GW::GEvent g_outEvent;

// Call this at the end of every TEST_CASE
inline void ResetGlobals()
{
	g_outEnum = API::Count;
	g_outData = POD{ -1, "NULL", "NULL" };
	g_outEvent.Write(g_outEnum, g_outData);
}

#endif // _GEVENTCOMMONINCLUDES_H_
