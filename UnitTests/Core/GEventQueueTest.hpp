#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTQUEUE)

#include "GEventCommonIncludes.h"

TEST_CASE("GEventQueue core method test battery", "[Core]")
{
	GW::CORE::GEventQueue eventQueue;

	GW::CORE::GEventGenerator eventGenerator;
	GW::GEvent event;
	unsigned int maxEventCount = 0;
	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(eventQueue.Append(event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Clear() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Find(API::DirectX, false) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Find(API::DirectX, false, g_outEnum) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Invoke() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Max(maxEventCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Missed(missedEventCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Peek(0, event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Pop(event) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventQueue.Waiting(waitingEventCount) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS); // Need a valid event generator
		REQUIRE(eventQueue.Create(10, eventGenerator, []() { return; }) == GW::GReturn::SUCCESS);
		eventQueue = nullptr; // Destruction
		REQUIRE(!eventQueue); // Invalidation check
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS); // Need a valid event generator

		REQUIRE(eventQueue.Create(10, eventGenerator, []() { return; }) == GW::GReturn::SUCCESS);
		REQUIRE(event.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS); // Write an event and add it to the listener
		REQUIRE(eventQueue.Append(event) == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Find(API::DirectX, false) == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Find(API::DirectX, false, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventQueue.Invoke() == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Max(maxEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Peek(0, event) == GW::GReturn::SUCCESS);
		REQUIRE(event.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventQueue.Pop(event) == GW::GReturn::SUCCESS);
		REQUIRE(event.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));
		REQUIRE(eventQueue.Append(event) == GW::GReturn::SUCCESS);
		REQUIRE(eventQueue.Clear() == GW::GReturn::SUCCESS); // Removes all events
		REQUIRE(eventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 0); // waitingEventCount should be 0 after GEventReceiver::Clear() call
	}
	ResetGlobals();
} // End of test case GEventQueue core method test battery

TEST_CASE("GEventQueue test using GEvent and GEventGenerator", "[Core]")
{
	GW::CORE::GEventGenerator EventGenerator;
	GW::CORE::GEventQueue EventQueue;

	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;
	unsigned int queueSize = 0;

	REQUIRE(EventGenerator.Create() == GW::GReturn::SUCCESS); // Need a valid event generator
	REQUIRE(EventQueue.Create(10, EventGenerator, []() {}) == GW::GReturn::SUCCESS);

	// Make sure queue size is 10
	REQUIRE(EventQueue.Max(queueSize) == GW::GReturn::SUCCESS);
	REQUIRE(queueSize == 10);

	SECTION("Testing pop and peek for event queues")
	{
		// Current Queue: Empty
		//----------------------------------------------------------------------------------
		REQUIRE(g_outEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);

		REQUIRE(EventQueue.Peek(0, g_outEvent) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));

		// Currently we have no missing events because only 1 Push() has been called
		// There is a event waiting because we havent popped yet
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);

		// Current Queue: DirectX
		//----------------------------------------------------------------------------------
		REQUIRE(g_outEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);

		// Previously we only peeked at an event, so when we push again, previous event will not be missed
		// because we are using a queue and event missed count should equal to 0
		// and the event waiting should be 1 as well because we havent popped yet
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 2);

		// Now we are popping the recent event, waiting events should be 1 now
		// missed event is still be 0 (it is a "tracker" for how many events this receiver
		// has missed during its life time
		REQUIRE(EventQueue.Pop(g_outEvent) == GW::GReturn::SUCCESS);
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);

		// The recent event should be DirectX because it was the first event that we pushed and
		// we never popped it back. But now we are popping it back so the recent event remove dequed
		REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::DirectX);
		REQUIRE((g_outData == g_pods[0]));

		// Current Queue: Vulkan
		// DirectX has been removed
		//----------------------------------------------------------------------------------
		REQUIRE(g_outEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);

		// The recent event should be Vulkan because it was the second event that we pushed and
		// we popped DirectX event the first time
		REQUIRE(EventQueue.Pop(g_outEvent) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEnum == API::Vulkan);
		REQUIRE((g_outData == g_pods[1]));

		// missed event count should still be 0 (size hasnt been reached yet)
		// there still 1 event waiting, which is the one we just pushed, (API::DirectX and g_pods[0])
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 1);

		// Current Queue: DirectX
		// Vulkan has been removed
		//----------------------------------------------------------------------------------
		// Now we are forcing the receiver to append new events,
		REQUIRE(g_outEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);
		REQUIRE(g_outEvent.Write(API::Metal, g_pods[3]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);

		// missed event count should still be 0 (size hasnt been reached yet)
		// Theres now an extra 3 in the queue + 1 for previous event so = 4
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 0);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 4);

		// Over load the queue with 30 opengl events
		REQUIRE(g_outEvent.Write(API::OpenGL, g_pods[2]) == GW::GReturn::SUCCESS);
		for (unsigned int i = 0; i < 30; i++)
		{
			REQUIRE(EventGenerator.Push(g_outEvent) == GW::GReturn::SUCCESS);
		}

		// Before the for loop the queue can still hold 6 more events, so when for loop gets ran
		// it should have 24 events missing and 10 events waiting
		REQUIRE(EventQueue.Missed(missedEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(missedEventCount == 24);
		REQUIRE(EventQueue.Waiting(waitingEventCount) == GW::GReturn::SUCCESS);
		REQUIRE(waitingEventCount == 10);

		// Current Queue : OpenGL, OpenGL, OpenGL, OpenGL, OpenGL, OpenGL, OpenGL, OpenGL, OpenGL, OpenGL
		//----------------------------------------------------------------------------------
		// Now we are going to pop all the events waiting

		// All events should be opengl because we have pushed more than the queue can hold,
		// so the queue will prioritize new events
		for (unsigned int i = 0; i < waitingEventCount; i++)
		{
			EventQueue.Pop(g_outEvent);
			g_outEvent.Read(g_outEnum, g_outData);
			REQUIRE(g_outEnum == API::OpenGL);
			REQUIRE((g_outData == g_pods[2]));
		}
	}
}
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTQUEUE) */