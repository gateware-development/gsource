#include "API.h" // dummies should not cause compilation issues

#include <cstdio>

int main() 
{ 
    printf("Dummy Main is tested\n");
    return 0; 
}
// this is ignored by desktop applications but allows UWP/iOS/Android to use the main entrypoint.
// without it you will need to define the specific platform entry point (ex: wWinMain in C++/WinRT)
GW::SYSTEM::GApp myApp(main);
