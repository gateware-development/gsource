//My name is Colby Peck and I refactored all of these tests in April 2022. If you have a problem with them, you have a problem with me. Don't be afraid to reach out with any questions :) 

#if defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GMATRIX)

TEST_CASE("Create GMatrix.", "[CreateGMatrix], [Math]")
{
	GW::MATH::GMatrix MatrixC;

	//Pass Cases
	REQUIRE(+(MatrixC.Create()));
}

TEST_CASE("Add two matrices.", "[AddMatrixF], [AddMatrixD], [Math]")
{
	SECTION("Float matrix addition.", "[AddMatrixF]")
	{
		auto TestAddF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::AddMatrixF(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestAddF
		(
			{ 
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f 
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f 
			},
			{
				+0.2f, +0.0f, +2.5f, +0.4f,
				+1.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +9.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +2.0f 
			}
		);

	}
	SECTION("Double matrix addition.", "[AddMatrixD]")
	{
		auto TestAddD = [](GW::MATH::GMATRIXD _a, GW::MATH::GMATRIXD _b, GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::AddMatrixD(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestAddD
		(
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{
				+0.2, +0.0, +2.5, +0.4,
				+1.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +9.5, +12.4,
				+7.6, +5.5, +9.8, +2.0
			}
		);
	}
}

TEST_CASE("Subtract two matrices.", "[SubtractMatrixF], [SubtractMatrixD], [Math]")
{
	SECTION("Float matrix subtraction.", "[SubtractMatrixF]")
	{
		auto TestSubtractF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::SubtractMatrixF(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestSubtractF
		(
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f 
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f 
			},
			{
				-0.2f, -2.0f, -2.5f, -0.4f,
				+0.3f, -5.9f, -2.2f, -4.1f,
				-3.8f, +0.0f, -7.5f, -12.4f,
				-7.6f, -5.5f, -9.8f, +0.0f 
			}
		);
	}
	SECTION("Double matrix subtraction.", "[SubtractMatrixD]")
	{
		auto TestSubtractD = [](GW::MATH::GMATRIXD _a, GW::MATH::GMATRIXD _b, GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::SubtractMatrixD(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestSubtractD
		(
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{
				-0.2, -2.0, -2.5, -0.4,
				+0.3, -5.9, -2.2, -4.1,
				-3.8, +0.0, -7.5, -12.4,
				-7.6, -5.5, -9.8, +0.0
			}
		);
	}
}

TEST_CASE("Multiply a matrix by a matrix.", "[MultiplyMatrixF], [MultiplyMatrixD], [Math]")
{
	SECTION("Float matrix multiplication.", "[MultiplyMatrixF]")
	{
		auto TestMultiplyF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::MultiplyMatrixF(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

	//multiplying by a zero matrix should result in a zero matrix 
		TestMultiplyF
		(
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			}
		);

		//multiplying by identity should change nothing 
		TestMultiplyF
		(
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f 
			},
			{
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f 
			},
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			}
		);

		//test with random-ish values
		TestMultiplyF
		(
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f 
			},
			{
				-0.7f, -5.9f, -2.2f, -4.1f,
				+0.2f, +1.0f, +2.5f, +0.4f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			}
		);
	}
	SECTION("Double matrix multiplication.", "[MultiplyMatrixD]")
	{
		auto TestMultiplyD = [](GW::MATH::GMATRIXD _a, GW::MATH::GMATRIXD _b, GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::MultiplyMatrixD(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//multiplying by a zero matrix should result in a zero matrix 
		TestMultiplyD(
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			},
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			}
		);

		//multiplying by identity should change nothing 
		TestMultiplyD(
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			}
		);

		//test with random-ish values
		TestMultiplyD(
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{
				-0.7, -5.9, -2.2, -4.1,
				+0.2, +1.0, +2.5, +0.4,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			}
		);
	}
}


TEST_CASE("Multiply a vector by a matrix.", "[VectorXMatrixF], [VectorXMatrixD], [Math]")
{
	SECTION("Float matrix multiplication.", "[VectorXMatrixF]")
	{
		auto TestVectorXMatrixMultiplyF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GMatrix::VectorXMatrixF(_b, _a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//multiplying by a 0 matrix should yield a 0 vector
		TestVectorXMatrixMultiplyF
		(
			{
				+1.0f, -0.2f, +0.3f, +0.4f
			},
			{ 
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f
			}
		);
		//multiplying by identity matrix shouldn't change anything 
		TestVectorXMatrixMultiplyF
		(
			{	
				+1.0f, -0.2f, +0.3f, +0.4f	
			},
			{	
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+1.0f, -0.2f, +0.3f, +0.4f
			}
		);
		//multiplying by a negative identity matrix should flip the sign of each component of the vector 
		TestVectorXMatrixMultiplyF
		(
			{
				+1.0f, -0.2f, +0.3f, +0.4f
			},
			{
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, -1.0f
			},
			{
				-1.0f, +0.2f, -0.3f, -0.4f
			}
		);
		//test with random-ish values
		TestVectorXMatrixMultiplyF
		(
			{
				+1.0f, -0.2f, +0.3f, +0.4f
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			},
			{
				+4.24f, +2.02f, +8.53f, +3.7f
			}
		);
	}

	SECTION("Double matrix multiplication.", "[VectorXMatrixD]")
	{
		auto TestVectorXMatrixMultiplyD = [](GW::MATH::GVECTORD _a, GW::MATH::GMATRIXD _b, GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GMatrix::VectorXMatrixD(_b, _a, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//multiplying by a 0 matrix should yield a 0 vector
		TestVectorXMatrixMultiplyD
		(
			{
				+1.0, -0.2, +0.3, +0.4
			},
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			},
			{
				+0.0, +0.0, +0.0, +0.0
			}
		);
		//multiplying by identity matrix shouldn't change anything 
		TestVectorXMatrixMultiplyD
		(
			{
				+1.0, -0.2, +0.3, +0.4
			},
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+1.0, -0.2, +0.3, +0.4
			}
		);
		//multiplying by a negative identity matrix should flip the sign of each component of the vector 
		TestVectorXMatrixMultiplyD
		(
			{
				+1.0, -0.2, +0.3, +0.4
			},
			{
				-1.0, +0.0, +0.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, -1.0
			},
			{
				-1.0, +0.2, -0.3, -0.4
			}
		);
		//test with random-ish values
		TestVectorXMatrixMultiplyD
		(
			{
				+1.0, -0.2, +0.3, +0.4
			},
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{
				+4.24, +2.02, +8.53, +3.7
			}
		);
	}
}

TEST_CASE("Convert a quaternion to a rotation matrix.", "[ConvertQuaternionF], [ConvertQuaternionD], [Math]")
{
	SECTION("Convert a float quaternion to a float rotation matrix.", "[ConvertQuaternionF]")
	{
		auto TestConvertQuaternionF = [](const GW::MATH::GQUATERNIONF _a, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ConvertQuaternionF(_a, result)));
			COMPARE_MATRICES_STANDARD_F (result, _expectedResult);
		};

		TestConvertQuaternionF
		(
			{	
				+0.0f, +0.0f, +0.70710678f, +0.70710678f	
			},
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f 
			}
		);
	}

	SECTION("Convert a double quaternion to a double rotation matrix.", "[ConvertQuaternionD]")
	{
		auto TestConvertQuaternionD = [](const GW::MATH::GQUATERNIOND _a, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ConvertQuaternionD(_a, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestConvertQuaternionD
		(
			{
				0.0, 0.0, 0.7071067811865475, 0.7071067811865475
			},
			{
				+0.0, -1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			}
		);
	}
}

TEST_CASE("Multiply a matrix by a number.", "[MultiplyNumF], [MultiplyNumD], [Math]")
{
	SECTION("Float matrix multiplication.", "[MultiplyNumF]")
	{
		auto TestMultiplyNumF = [](const GW::MATH::GMATRIXF _a, const float _b, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::MultiplyNumF(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//multiplying by 1 shouldn't change anything
		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				+1.0f,
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			}
		);

		//Multiplying by 0 should give a 0 matrix 
		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				+0.0f,
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			}
		);

		//Multiplying by -1 should flip the sign of each component 
		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				-1.0f,
			{
				-0.2f, -1.0f, -2.5f, +0.4f,
				-0.7f, -5.9f, +2.2f, -4.1f,
				-3.8f, +0.0f, -8.5f, -2.4f,
				+7.6f, -5.5f, -9.8f, -1.0f
			}
		);

		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				+0.3f,
			{
				+0.06f, +0.30f, +0.75f, -0.12f,
				+0.21f, +1.77f, -0.66f, +1.23f,
				+1.14f, +0.00f, +2.55f, +0.72f,
				-2.28f, +1.65f, +2.94f, +0.30f
			}
		);

		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, -0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				+3.0f,
			{
				+0.60f, +3.00f, +7.50f, -1.20f,
				+2.10f, +17.7f, -6.60f, +12.3f,
				+11.4f, +0.00f, +25.5f, +7.20f,
				-22.8f, +16.5f, +29.4f, +3.00f
			}
		);

		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, -0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				-0.3f,
			{
				-0.06f, -0.30f, -0.75f, +0.12f,
				-0.21f, -1.77f, +0.66f, -1.23f,
				-1.14f, +0.00f, -2.55f, -0.72f,
				+2.28f, -1.65f, -2.94f, -0.30f
			}
		);

		TestMultiplyNumF
		(
			{
				+0.2f, +1.0f, +2.5f, -0.4f,
				+0.7f, +5.9f, -2.2f, +4.1f,
				+3.8f, -0.0f, +8.5f, +2.4f,
				-7.6f, +5.5f, +9.8f, +1.0f
			},
				-3.0f,
			{
				-0.60f, -3.00f, -7.50f, +1.20f,
				-2.10f, -17.7f, +6.60f, -12.3f,
				-11.4f, +0.00f, -25.5f, -7.20f,
				+22.8f, -16.5f, -29.4f, -3.00f
			}
		);
	}

	SECTION("Double matrix multiplication.", "[MultiplyNumD]")
	{
		auto TestMultiplyNumD = [](const GW::MATH::GMATRIXD _a, const double _b, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::MultiplyNumD(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//multiplying by 1 shouldn't change anything
		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},					
				+1.0,			
			{					
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			}
		);

		//Multiplying by 0 should give a 0 matrix 
		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				+0.0,
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			}
		);

		//Multiplying by -1 should flip the sign of each component 
		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				-1.0,
			{
				-0.2, -1.0, -2.5, +0.4,
				-0.7, -5.9, +2.2, -4.1,
				-3.8, +0.0, -8.5, -2.4,
				+7.6, -5.5, -9.8, -1.0
			}
		);

		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				+0.3,
			{
				+0.06, +0.30, +0.75, -0.12,
				+0.21, +1.77, -0.66, +1.23,
				+1.14, +0.00, +2.55, +0.72,
				-2.28, +1.65, +2.94, +0.30
			}
		);

		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, -0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				+3.0,
			{
				+0.60, +3.00, +7.50, -1.20,
				+2.10, +17.7, -6.60, +12.3,
				+11.4, +0.00, +25.5, +7.20,
				-22.8, +16.5, +29.4, +3.00
			}
		);

		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, -0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				-0.3,
			{
				-0.06, -0.30, -0.75, +0.12,
				-0.21, -1.77, +0.66, -1.23,
				-1.14, +0.00, -2.55, -0.72,
				+2.28, -1.65, -2.94, -0.30
			}
		);

		TestMultiplyNumD
		(
			{
				+0.2, +1.0, +2.5, -0.4,
				+0.7, +5.9, -2.2, +4.1,
				+3.8, -0.0, +8.5, +2.4,
				-7.6, +5.5, +9.8, +1.0
			},
				-3.0,
			{
				-0.60, -3.00, -7.50, +1.20,
				-2.10, -17.7, +6.60, -12.3,
				-11.4, +0.00, -25.5, -7.20,
				+22.8, -16.5, -29.4, -3.00
			}
		);
	}
}


TEST_CASE("Calculate a determinant of a matrix.", "[DeterminantF], [DeterminantD], [Math]")
{
	SECTION("Float matrix determinant.", "[DeterminantF]")
	{
		auto TestDeterminantF = [](const GW::MATH::GMATRIXF _a, const float _expectedResult)
		{
			float result;
			CHECK(+(GW::MATH::GMatrix::DeterminantF(_a, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_F(result, _expectedResult));
		};
		
		TestDeterminantF
		(
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			},
				+1209.6248f
		);
	}

	SECTION("Double matrix determinant.", "[DeterminantD]")
	{

		auto TestDeterminantD = [](const GW::MATH::GMATRIXD _a, const double _expectedResult)
		{
			double result;
			CHECK(+(GW::MATH::GMatrix::DeterminantD(_a, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_D(result, _expectedResult));
		};

		TestDeterminantD
		(
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			+1209.6248
		);
	}
}

TEST_CASE("Transpose a matrix.", "[TransposeF], [TransposeD], [Math]")
{
	SECTION("Float matrix transpose.", "[TransposeF]")
	{
		auto TestTransposeF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::TransposeF(_a, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestTransposeF
		(
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			},
			{
				+0.2f, +0.7f, +3.8f, +7.6f,
				+1.0f, +5.9f, +0.0f, +5.5f,
				+2.5f, +2.2f, +8.5f, +9.8f,
				+0.4f, +4.1f, +2.4f, +1.0f
			}
		);
	}
	SECTION("Double matrix transpose.", "[TransposeD]")
	{
		auto TestTransposeD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::TransposeD(_a, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestTransposeD
		(
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +2.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{
				+0.2, +0.7, +3.8, +7.6,
				+1.0, +5.9, +0.0, +5.5,
				+2.5, +2.2, +8.5, +9.8,
				+0.4, +4.1, +2.4, +1.0
			}
		);
	}
}

TEST_CASE("Inverse a matrix.", "[InverseF], [InverseD], [Math]")
{
	SECTION("Float matrix inverse.", "[InverseF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF 
			nonInvertibleMatrix 
			{
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f
			},
			resultF;
			CHECK(-(GW::MATH::GMatrix::InverseF(nonInvertibleMatrix, resultF)));
		}

		auto TestInverseF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::InverseF(_a, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestInverseF
		(
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			},
			{
				-0.58567995f, -0.03205952f, +0.01813206f, +0.14087839f,
				-0.00634742f, +0.15918324f, -0.05341326f, +0.01221205f,
				+0.47249858f, -0.07040200f, +0.00887383f, -0.01038669f,
				-0.14440759f, +0.05808413f, +0.06900569f, -0.03605250f
			}
		);
	}

	SECTION("Double matrix inverse.", "[InverseD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD
				nonInvertibleMatrix
				{
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0
				},
				resultD;
			CHECK(-(GW::MATH::GMatrix::InverseD(nonInvertibleMatrix, resultD)));
		}

		auto TestInverseD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::InverseD(_a, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestInverseD
		(
			{
				0.2, 1.0, 2.5, 0.4,
				0.7, 5.9, 2.2, 4.1,
				3.8, 0.0, 8.5, 12.4,
				7.6, 5.5, 9.8, 1.0
			},
			{
				-0.5856799562971923, -0.0320595278800501, +0.0181320687208133, +0.1408783946889978,
				-0.0063474227710940, +0.1591832442588809, -0.0534132567387838, +0.0122120512079448,
				+0.4724985797248866, -0.0704019957262781, +0.0088738259995992, -0.0103866918072447,
				-0.1444075882042101, +0.0580841265820608, +0.0690056949890578, -0.0360525015690816
			}
		);
	}
}

TEST_CASE("Set a matrix to an identity matrix.", "[IdentityF], [IdentityD], [Math]")
{
	SECTION("Float identity matrix.", "[IdentityF]")
	{
		GW::MATH::GMATRIXF
			expectedResultF = {
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f },
			resultF;

		CHECK(+(GW::MATH::GMatrix::IdentityF(resultF)));
		COMPARE_MATRICES_STANDARD_F(resultF, expectedResultF);
	}
	SECTION("Double identity matrix.", "[IdentityD]")
	{
		GW::MATH::GMATRIXD 
			expectedResultD = {
				1.0, 0.0, 0.0, 0.0,
				0.0, 1.0, 0.0, 0.0,
				0.0, 0.0, 1.0, 0.0,
				0.0, 0.0, 0.0, 1.0 },
			resultD;

		CHECK(+(GW::MATH::GMatrix::IdentityD(resultD)));
		COMPARE_MATRICES_STANDARD_D(resultD, expectedResultD);
	}
}


TEST_CASE("Get a rotation quaternion from a transform matrix.", "[GetRotationF], [GetRotationD], [Math]")
{
	SECTION("Get a float rotation quaternion from a float transform matrix.", "[GetRotationF]")
	{
		//fail cases
		GW::MATH::GMATRIXF mF = {
			+0.0f, +0.0f, +0.0f, +0.0f,
			+0.0f, +0.0f, +0.0f, +0.0f,
			+0.0f, +0.0f, +0.0f, +0.0f,
			+0.0f, +0.0f, +0.0f, +0.0f };
		GW::MATH::GQUATERNIONF resultF;
		CHECK(-(GW::MATH::GMatrix::GetRotationF(mF, resultF))); //GetRotation should fail for a matrix with a determinant of 0 

		//success cases
		auto TestGetRotationF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GQUATERNIONF _expectedResult)
		{
			GW::MATH::GQUATERNIONF result;
			CHECK(+(GW::MATH::GMatrix::GetRotationF(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		TestGetRotationF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+0.0f, +0.0f, +0.38268343f, +0.92387953f
			}
		);
	}

	SECTION("Double matrix.", "[GetRotationD]")
	{
		//fail cases
		GW::MATH::GMATRIXD mD = {
			+0.0, +0.0, +0.0, +0.0,
			+0.0, +0.0, +0.0, +0.0,
			+0.0, +0.0, +0.0, +0.0,
			+0.0, +0.0, +0.0, +0.0};
		GW::MATH::GQUATERNIOND resultD;
		CHECK(-(GW::MATH::GMatrix::GetRotationD(mD, resultD))); //GetRotation should fail for a matrix with a determinant of 0 

		//success cases
		auto TestGetRotationD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GQUATERNIOND _expectedResult)
		{
			GW::MATH::GQUATERNIOND result;
			CHECK(+(GW::MATH::GMatrix::GetRotationD(_a, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		TestGetRotationD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				0.0, 0.0, 0.3826834323650898, 0.9238795325112867
			}
		);
	}
}

TEST_CASE("Get a translation vector from a transform matrix.", "[GetTranslationF], [GetTranslationD], [Math]")
{
	SECTION("Get a float translation vector from a float transform matrix.", "[GetTranslationF]")
	{
		auto TestGetTranslationF = [](const GW::MATH::GMATRIXF _a, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GMatrix::GetTranslationF(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		TestGetTranslationF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+5.0f, +6.0f, +7.0f, +1.0f
			},
			{
				+5.0f, +6.0f, +7.0f, +0.0f
			}
		);
	}

	SECTION("Get a double translation vector from a double transform matrix.", "[GetTranslationD]")
	{
		auto TestGetTranslationD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GMatrix::GetTranslationD(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		TestGetTranslationD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+5.0, +6.0, +7.0, +1.0
			},
			{
				+5.0, +6.0, +7.0, +0.0
			}
		);
	}
}

TEST_CASE("Get a scale vector from a transform matrix.", "[GetScaleF], [GetScaleD], [Math]")
{
	SECTION("Get a float scale vector from a float transform matrix.", "[GetScaleF]")
	{
		auto TestGetScaleF = [] (const GW::MATH::GMATRIXF _a, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GMatrix::GetScaleF(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};
		
		TestGetScaleF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				-2.82842712f , +1.41421356f, +1.0f, +0.0f
			}
		);
	}

	SECTION("Get a double scale vector from a double transform matrix.", "[GetScaleD]")
	{
		auto TestGetScaleD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GMatrix::GetScaleD(_a, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		TestGetScaleD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				-2.8284271247461903, 1.4142135623730951, 1.0, 0.0
			}
		);
	}
}


TEST_CASE("Rotate a specified matrix around the x-axis, globally. A * B = C", "[RotateXGlobalF], [RotateXGlobalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateXGlobalF]")
	{
		auto TestRotateXGlobalF = [](const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateXGlobalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};
		
		//rotation by 0 rads shouldn't change the matrix 
		TestRotateXGlobalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		//rotation by 2 PI rads (360 degrees) shouldn't change anything 
		TestRotateXGlobalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		// PI/2 rads (90 degree) rotation 
		TestRotateXGlobalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * +0.5f,
			{
				-2.0f, -3.0f, -1.0f, +2.0f,
				-2.0f, -3.5f, +1.0f, -1.0f,
				+9.0f, -1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		// -3PI/2 rads (-270 degree) rotation 
		TestRotateXGlobalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * -1.5f,
			{
				-2.0f, -3.0f, -1.0f, +2.0f,
				-2.0f, -3.5f, +1.0f, -1.0f,
				+9.0f, -1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateXGlobalD]")
	{
		auto TestRotateXGlobalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateXGlobalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//rotation by 0 rads shouldn't change the matrix 
		TestRotateXGlobalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				+0.0,
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		//rotation by 2 PI rads (360 degrees) shouldn't change anything 
		TestRotateXGlobalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * +2.0,
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		// PI/2 rads (90 degree) rotation 
		TestRotateXGlobalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * +0.5,
			{
				-2.0, -3.0, -1.0, +2.0,
				-2.0, -3.5, +1.0, -1.0,
				+9.0, -1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		// -3PI/2 rads (-270 degree) rotation 
		TestRotateXGlobalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * -1.5,
			{
				-2.0, -3.0, -1.0, +2.0,
				-2.0, -3.5, +1.0, -1.0,
				+9.0, -1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);
	}
}

TEST_CASE("Rotate a specified matrix around the x-axis, locally. B * A = C", "[RotateXLocalF], [RotateXLocalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateXLocalF]") 
	{
		auto TestRotateXLocalF = [] (const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateXLocalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};
		
		//rotating by 0 rads shouldn't change anything 
		TestRotateXLocalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		//rotating by 2 PI rads (360 degrees) shouldn't change anything
		TestRotateXLocalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		//Rotation by PI/2 rads (90 degrees) 
		TestRotateXLocalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * +0.5f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+2.0f, -1.0f, -3.5f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);

		//Rotation by -3PI/2 rads (-270 degrees) 
		TestRotateXLocalF
		(
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				-2.0f, +1.0f, +3.5f, -1.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			},
				G_PI_F * -1.5f,
			{
				-2.0f, -1.0f, +3.0f, +2.0f,
				+9.0f, +1.0f, +1.0f, +1.0f,
				+2.0f, -1.0f, -3.5f, +1.0f,
				+9.0f, -5.0f, +7.0f, +1.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateXLocalD]") 
	{
		auto TestRotateXLocalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateXLocalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//rotating by 0 rads shouldn't change anything 
		TestRotateXLocalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				+0.0,
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		//rotating by 2 PI rads (360 degrees) shouldn't change anything
		TestRotateXLocalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * +2.0, 
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		//Rotation by PI/2 rads (90 degrees) 
		TestRotateXLocalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * +0.5,
			{
				-2.0, -1.0, +3.0, +2.0,
				+9.0, +1.0, +1.0, +1.0,
				+2.0, -1.0, -3.5, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);

		//Rotation by -3PI/2 rads (-270 degrees) 
		TestRotateXLocalD
		(
			{
				-2.0, -1.0, +3.0, +2.0,
				-2.0, +1.0, +3.5, -1.0,
				+9.0, +1.0, +1.0, +1.0,
				+9.0, -5.0, +7.0, +1.0
			},
				G_PI_D * -1.5,
			{
				-2.0, -1.0, +3.0, +2.0,
				+9.0, +1.0, +1.0, +1.0,
				+2.0, -1.0, -3.5, +1.0,
				+9.0, -5.0, +7.0, +1.0
			}
		);
	}
}

TEST_CASE("Rotate a specified matrix around the y-axis, globally. A * B = C", "[RotateYGlobalF], [RotateYGlobalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateYGlobalF]")
	{
		auto TestRotateYGlobalF = [] (const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateYGlobalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//rotating by 0 rads shouldn't change anything
		TestRotateYGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);


		//rotating by 2 PI rads (360 degrees) shouldn't change anything
		TestRotateYGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);


		//Rotation by PI/2 rads (90 degrees) 
		TestRotateYGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * +0.5f,
			{
				+0.0f, -1.0f, +2.0f, +2.0f,
				+0.0f, +1.0f, +2.0f, -1.0f,
				+1.0f, +0.0f, +0.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);

		//Rotation by -3PI/2 rads (-270 degrees), should give the same result as a 90 degree rotation
		TestRotateYGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * -1.5f,
			{
				+0.0f, -1.0f, +2.0f, +2.0f,
				+0.0f, +1.0f, +2.0f, -1.0f,
				+1.0f, +0.0f, +0.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateYGlobalD]")
	{
		auto TestRotateYGlobalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateYGlobalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//rotating by 0 rads shouldn't change anything
		TestRotateYGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				+0.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);


		//rotating by 2 PI rads (360 degrees) shouldn't change anything
		TestRotateYGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * +2.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);


		//Rotation by PI/2 rads (90 degrees) 
		TestRotateYGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * +0.5,
			{
				+0.0, -1.0, +2.0, +2.0,
				+0.0, +1.0, +2.0, -1.0,
				+1.0, +0.0, +0.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);

		//Rotation by -3PI/2 rads (-270 degrees), should give the same result as a 90 degree rotation
		TestRotateYGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * -1.5f,
			{
				+0.0, -1.0, +2.0, +2.0,
				+0.0, +1.0, +2.0, -1.0,
				+1.0, +0.0, +0.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);
	}
}

TEST_CASE("Rotate a specified matrix around the y-axis, locally. B * A = C", "[RotateYLocalF], [RotateYLocalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateYLocalF]") 
	{
		auto TestRotateYLocalF = [] (const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateYLocalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//rotation by 0 rads shouldn't change anything 
		TestRotateYLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);

		//rotation by 2 PI rads (360 degrees) shouldn't change anything 
		TestRotateYLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);

		//rotation by PI/2 rads (90 degrees) 
		TestRotateYLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * +0.5f,
			{
				+0.0f, +0.0f, -1.0f, -1.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				-2.0f, -1.0f, +0.0f, +2.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);

		//rotation by -3PI/2 rads (-270 degrees), should have the same result as a 90 degree rotation 
		TestRotateYLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			},
				G_PI_F * -1.5f,
			{
				+0.0f, +0.0f, -1.0f, -1.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				-2.0f, -1.0f, +0.0f, +2.0f,
				+6.0f, -5.0f, +2.0f, +5.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateYLocalD]") 
	{
		auto TestRotateYLocalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateYLocalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//rotation by 0 rads shouldn't change anything 
		TestRotateYLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				+0.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);

		//rotation by 2 PI rads (360 degrees) shouldn't change anything 
		TestRotateYLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * +2.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);

		//rotation by PI/2 rads (90 degrees) 
		TestRotateYLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * +0.5,
			{
				+0.0, +0.0, -1.0, -1.0,
				-2.0, +1.0, +0.0, -1.0,
				-2.0, -1.0, +0.0, +2.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);

		//rotation by -3PI/2 rads (-270 degrees), should have the same result as a 90 degree rotation 
		TestRotateYLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+6.0, -5.0, +2.0, +5.0
			},
				G_PI_D * -1.5f,
			{
				+0.0, +0.0, -1.0, -1.0,
				-2.0, +1.0, +0.0, -1.0,
				-2.0, -1.0, +0.0, +2.0,
				+6.0, -5.0, +2.0, +5.0
			}
		);
	}
}

TEST_CASE("Rotate a specified matrix around the z-axis, globally. A * B = C", "[RotateZGlobalF], [RotateZGlobalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateZGlobalF]")
	{
		auto TestRotateZGlobalF = [] (const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateZGlobalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//0 rad rotation shouldn't change anything 
		TestRotateZGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);


		//2 PI rad (360 degree) rotation shouldn't change anything 
		TestRotateZGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);

		//PI/2 rad (90 degree) rotation 
		TestRotateZGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * +0.5f,
			{
				+1.0f, -2.0f, +0.0f, +2.0f,
				-1.0f, -2.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);

		//-3PI/2 rad (-270 degree) rotation, should have the same result as a 90 degree rotation 
		TestRotateZGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * -1.5f,
			{
				+1.0f, -2.0f, +0.0f, +2.0f,
				-1.0f, -2.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateZGlobalD]")
	{
		auto TestRotateZGlobalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateZGlobalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//0 rad rotation shouldn't change anything 
		TestRotateZGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},						  
				+0.0,				  
			{						  
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);


		//2 PI rad (360 degree) rotation shouldn't change anything 
		TestRotateZGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * +2.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);

		//PI/2 rad (90 degree) rotation 
		TestRotateZGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * +0.5,
			{
				+1.0, -2.0, +0.0, +2.0,
				-1.0, -2.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);

		//-3PI/2 rad (-270 degree) rotation, should have the same result as a 90 degree rotation 
		TestRotateZGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * -1.5,
			{
				+1.0, -2.0, +0.0, +2.0,
				-1.0, -2.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);
	}
}

TEST_CASE("Rotate a specified matrix around the z-axis, locally. B * A = C", "[RotateZLocalF], [RotateZLocalD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotateZLocalF]") 
	{
		auto TestRotateZLocalF = [] (const GW::MATH::GMATRIXF _a, const float _rotationRads, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::RotateZLocalF(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//0 rad rotation shouldn't change anything
		TestRotateZLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);
		
		//2PI rad (360 degree) rotation shouldn't change anything
		TestRotateZLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * +2.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);

		//PI/2 rad (90 degree) rotation 
		TestRotateZLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * +0.5f,
			{
				-2.0f, +1.0f, +0.0f, -1.0f,
				+2.0f, +1.0f, +0.0f, -2.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);

		//-3PI/2 rad (-270 degree) rotation, should have the same result as a 90 degree rotation 
		TestRotateZLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			},
				G_PI_F * -1.5f,
			{
				-2.0f, +1.0f, +0.0f, -1.0f,
				+2.0f, +1.0f, +0.0f, -2.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				-5.0f, +1.2f, +2.0f, -3.0f
			}
		);
	}

	SECTION("Double matrix rotation.", "[RotateZLocalD]") 
	{
		auto TestRotateZLocalD = [](const GW::MATH::GMATRIXD _a, const double _rotationRads, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::RotateZLocalD(_a, _rotationRads, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//0 rad rotation shouldn't change anything
		TestRotateZLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				+0.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);

		//2PI rad (360 degree) rotation shouldn't change anything
		TestRotateZLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * +2.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);

		//PI/2 rad (90 degree) rotation 
		TestRotateZLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * +0.5,
			{
				-2.0, +1.0, +0.0, -1.0,
				+2.0, +1.0, +0.0, -2.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);

		//-3PI/2 rad (-270 degree) rotation, should have the same result as a 90 degree rotation 
		TestRotateZLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			},
				G_PI_D * -1.5,
			{
				-2.0, +1.0, +0.0, -1.0,
				+2.0, +1.0, +0.0, -2.0,
				+0.0, +0.0, +1.0, +1.0,
				-5.0, +1.2, +2.0, -3.0
			}
		);
	}
}

TEST_CASE("Rotate a specified vector by rotating Yaw, Pitch and Roll axis.", "[RotationYawPitchRollF], [RotationYawPitchRollD], [Math]")
{
	SECTION("Float matrix rotation.", "[RotationYawPitchRollF]")
	{
		auto TestRotationYawPitchRollF = [] (const GW::MATH::GVECTORF _input, const GW::MATH::GVECTORF _yawPitchRoll, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GMATRIXF rotMat;
			GW::MATH::GVECTORF result;

			CHECK(+(GW::MATH::GMatrix::RotationYawPitchRollF(_yawPitchRoll.x, _yawPitchRoll.y, _yawPitchRoll.z, rotMat)));
			GW::MATH::GMatrix::VectorXMatrixF(rotMat, _input, result);
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//rotate 0 degrees on all axes 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +0.0f, +0.0f, +1.0f },
			{ +1.0f, +2.0f, +3.0f, +4.0f }
		);

		//rotate 360 degrees on all axes 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * +2.0f, G_PI_F * +2.0f, G_PI_F * +2.0f, +1.0f },
			{ +1.0f, +2.0f, +3.0f, +4.0f }
		);

		//yaw 90 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * +0.5f, +0.0f, +0.0f, +1.0f },
			{ +3.0f, +2.0f, -1.0f, +4.0f }
		);

		//yaw -270 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * -1.5f, +0.0f, +0.0f, +1.0f },
			{ +3.0f, +2.0f, -1.0f, +4.0f }
		);

		//pitch 90 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, G_PI_F * +0.5f, +0.0f, +1.0f },
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);

		//pitch -270 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, G_PI_F * -1.5f, +0.0f, +1.0f },
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);

		//roll 90 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +0.0f, G_PI_F * +0.5f, +1.0f },
			{ -2.0f, +1.0f, +3.0f, +4.0f }
		);

		//roll -270 degrees 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +0.0f, G_PI_F * -1.5f, +1.0f },
			{ -2.0f, +1.0f, +3.0f, +4.0f }
		);

		//yaw & pitch 90 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * +0.5f, G_PI_F * +0.5f, +0.0f, +1.0f },
			{ +2.0f, -3.0f, -1.0f, +4.0f }
		);

		//yaw & pitch -270 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * -1.5f, G_PI_F * -1.5f, +0.0f, +1.0f },
			{ +2.0f, -3.0f, -1.0f, +4.0f }
		);

		//yaw & roll 90 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * +0.5f, +0.0f, G_PI_F * +0.5f, +1.0f },
			{ +3.0f, +1.0f, +2.0f, +4.0f }
		);

		//yaw & roll -270 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * -1.5f, +0.0f, G_PI_F * -1.5f, +1.0f },
			{ +3.0f, +1.0f, +2.0f, +4.0f }
		);

		//pitch & roll 90 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, G_PI_F * +0.5f, G_PI_F * +0.5f, +1.0f },
			{ -2.0f, -3.0f, +1.0f, +4.0f }
		);

		//pitch & roll -270 degrees
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, G_PI_F * -1.5f, G_PI_F * -1.5f, +1.0f },
			{ -2.0f, -3.0f, +1.0f, +4.0f }
		);

		//rotate 90 degrees on all axes 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * +0.5f, G_PI_F * +0.5f, G_PI_F * +0.5f, +1.0f },
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);

		//rotate -270 degrees on all axes 
		TestRotationYawPitchRollF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ G_PI_F * -1.5f, G_PI_F * -1.5f, G_PI_F * -1.5f, +1.0f },
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);
	}

	SECTION("Double matrix rotation.", "[RotationYawPitchRollD")
	{
		auto TestRotationYawPitchRollD = [](const GW::MATH::GVECTORD _input, const GW::MATH::GVECTORD _yawPitchRoll, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GMATRIXD rotMat;
			GW::MATH::GVECTORD result;

			CHECK(+(GW::MATH::GMatrix::RotationYawPitchRollD(_yawPitchRoll.x, _yawPitchRoll.y, _yawPitchRoll.z, rotMat)));
			GW::MATH::GMatrix::VectorXMatrixD(rotMat, _input, result);
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//rotate 0 degrees on all axes 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +0.0, +0.0, +1.0 },
			{ +1.0, +2.0, +3.0, +4.0 }
		);

		//rotate 360 degrees on all axes 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * +2.0, G_PI_D * +2.0, G_PI_D * +2.0, +1.0 },
			{ +1.0, +2.0, +3.0, +4.0 }
		);

		//yaw 90 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * +0.5, +0.0, +0.0, +1.0 },
			{ +3.0, +2.0, -1.0, +4.0 }
		);

		//yaw -270 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * -1.5, +0.0, +0.0, +1.0 },
			{ +3.0, +2.0, -1.0, +4.0 }
		);

		//pitch 90 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, G_PI_D * +0.5, +0.0, +1.0 },
			{ +1.0, -3.0, +2.0, +4.0 }
		);

		//pitch -270 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, G_PI_D * -1.5, +0.0, +1.0 },
			{ +1.0, -3.0, +2.0, +4.0 }
		);

		//roll 90 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +0.0, G_PI_D * +0.5, +1.0 },
			{ -2.0, +1.0, +3.0, +4.0 }
		);

		//roll -270 degrees 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +0.0, G_PI_D * -1.5, +1.0 },
			{ -2.0, +1.0, +3.0, +4.0 }
		);

		//yaw & pitch 90 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * +0.5, G_PI_D * +0.5, +0.0, +1.0 },
			{ +2.0, -3.0, -1.0, +4.0 }
		);

		//yaw & pitch -270 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * -1.5, G_PI_D * -1.5, +0.0, +1.0 },
			{ +2.0, -3.0, -1.0, +4.0 }
		);

		//yaw & roll 90 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * +0.5, +0.0, G_PI_D * +0.5, +1.0 },
			{ +3.0, +1.0, +2.0, +4.0 }
		);

		//yaw & roll -270 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * -1.5, +0.0, G_PI_D * -1.5, +1.0 },
			{ +3.0, +1.0, +2.0, +4.0 }
		);

		//pitch & roll 90 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, G_PI_D * +0.5, G_PI_D * +0.5, +1.0 },
			{ -2.0, -3.0, +1.0, +4.0 }
		);

		//pitch & roll -270 degrees
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, G_PI_D * -1.5, G_PI_D * -1.5, +1.0 },
			{ -2.0, -3.0, +1.0, +4.0 }
		);

		//rotate 90 degrees on all axes 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * +0.5, G_PI_D * +0.5, G_PI_D * +0.5, +1.0 },
			{ +1.0, -3.0, +2.0, +4.0 }
		);

		//rotate -270 degrees on all axes 
		TestRotationYawPitchRollD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ G_PI_D * -1.5, G_PI_D * -1.5, G_PI_D * -1.5, +1.0 },
			{ +1.0, -3.0, +2.0, +4.0 }
		);
	}
}

TEST_CASE("Rotate the vector by a specified vector and angle.", "[RotationByVectorF], [RotationByVectorD], [Math]")
{
	SECTION("Float vector rotation.", "[RotationByVectorF]")
	{
		//fail cases 
		{
			GW::MATH::GVECTORF axisF = { +0.0f, +0.0f, +0.0f, +0.0f };
			GW::MATH::GMATRIXF output;
			CHECK(-(GW::MATH::GMatrix::RotationByVectorF(axisF, +0.0f, output))); //rotation should fail if given a 0 vector as the axis
		}

		auto TestRotateByVectorF = [] (const GW::MATH::GVECTORF _input, const GW::MATH::GVECTORF _axis, const float _radians, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			GW::MATH::GMATRIXF rotMat;

			CHECK(+(GW::MATH::GMatrix::RotationByVectorF(_axis, _radians, rotMat)));
			GW::MATH::GMatrix::VectorXMatrixF(rotMat, _input, result);
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};
		

		//rotate by 0 rads
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +1.0f, +1.0f, +1.0f },
			+0.0f,
			{ +1.0f, +2.0f, +3.0f, +4.0f }
		);

		//rotate by 2PI rads (360 degrees)
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +1.0f, +1.0f, +1.0f },
			G_PI_F * +2.0f, 
			{ +1.0f, +2.0f, +3.0f, +4.0f }
		);
		
		//this should swap the axes (X/Y/Z becomes Z/X/Y)
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +1.0f, +1.0f, +1.0f },
			G_PI_F * (+2.0f / +3.0f),
			{ +3.0f, +1.0f, +2.0f, +4.0f }
		);

		//this should swap the axes (X/Y/Z becomes Y/Z/X)
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +1.0f, +1.0f, +1.0f },
			G_PI_F * (+4.0f / +3.0f),
			{ +2.0f, +3.0f, +1.0f, +4.0f }
		);
		
		//90 degrees about the x axis
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +0.0f, +0.0f, +1.0f },
			G_PI_F * +0.5f,
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);
		
		//-270 degrees about the x axis 
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +1.0f, +0.0f, +0.0f, +1.0f },
			G_PI_F * -1.5f,
			{ +1.0f, -3.0f, +2.0f, +4.0f }
		);
		
		//90 degrees about the y axis
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +1.0f, +0.0f, +1.0f },
			G_PI_F * +0.5f,
			{ +3.0f, +2.0f, -1.0f, +4.0f }
		);
		
		//-270 degrees about the y axis 
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +1.0f, +0.0f, +1.0f },
			G_PI_F * -1.5f,
			{ +3.0f, +2.0f, -1.0f, +4.0f }
		);
		
		//90 degrees about the z axis
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +0.0f, +1.0f, +1.0f },
			G_PI_F * +0.5f,
			{ -2.0f, +1.0f, +3.0f, +4.0f }
		);
		
		//-270 degrees about the z axis 
		TestRotateByVectorF
		(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{ +0.0f, +0.0f, +1.0f, +1.0f },
			G_PI_F * -1.5f,
			{ -2.0f, +1.0f, +3.0f, +4.0f }
		);
	}

	SECTION("Double vector rotation.", "[RotationByVectorD]")
	{
		//fail cases 
		{
			GW::MATH::GVECTORD axisD = { +0.0, +0.0, +0.0, +0.0 };
			GW::MATH::GMATRIXD output;
			CHECK(-(GW::MATH::GMatrix::RotationByVectorD(axisD, +0.0, output))); //rotation should fail if given a 0 vector as the axis
		}

		auto TestRotateByVectorD = [](const GW::MATH::GVECTORD _input, const GW::MATH::GVECTORD _axis, const double _radians, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			GW::MATH::GMATRIXD rotMat;

			CHECK(+(GW::MATH::GMatrix::RotationByVectorD(_axis, _radians, rotMat)));
			GW::MATH::GMatrix::VectorXMatrixD(rotMat, _input, result);
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};


		//rotate by 0 rads
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +1.0, +1.0, +1.0 },
			+0.0,
			{ +1.0, +2.0, +3.0, +4.0 }
		);

		//rotate by 2PI rads (360 degrees)
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +1.0, +1.0, +1.0 },
			G_PI_D * +2.0,
			{ +1.0, +2.0, +3.0, +4.0 }
		);

		//this should swap the axes (X/Y/Z becomes Z/X/Y)
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +1.0, +1.0, +1.0 },
			G_PI_D * (+2.0 / +3.0),
			{ +3.0, +1.0, +2.0, +4.0 }
		);

		//this should swap the axes (X/Y/Z becomes Y/Z/X)
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +1.0, +1.0, +1.0 },
			G_PI_D * (+4.0 / +3.0),
			{ +2.0, +3.0, +1.0, +4.0 }
		);

		//90 degrees about the x axis
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +0.0, +0.0, +1.0 },
			G_PI_D * +0.5,
			{ +1.0, -3.0, +2.0, +4.0 }
		);

		//-270 degrees about the x axis 
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +1.0, +0.0, +0.0, +1.0 },
			G_PI_D * -1.5,
			{ +1.0, -3.0, +2.0, +4.0 }
		);

		//90 degrees about the y axis
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +1.0, +0.0, +1.0 },
			G_PI_D * +0.5,
			{ +3.0, +2.0, -1.0, +4.0 }
		);

		//-270 degrees about the y axis 
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +1.0, +0.0, +1.0 },
			G_PI_D * -1.5,
			{ +3.0, +2.0, -1.0, +4.0 }
		);

		//90 degrees about the z axis
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +0.0, +1.0, +1.0 },
			G_PI_D * +0.5,
			{ -2.0, +1.0, +3.0, +4.0 }
		);

		//-270 degrees about the z axis 
		TestRotateByVectorD
		(
			{ +1.0, +2.0, +3.0, +4.0 },
			{ +0.0, +0.0, +1.0, +1.0 },
			G_PI_D * -1.5,
			{ -2.0, +1.0, +3.0, +4.0 }
		);
	}
}


TEST_CASE("Translate a specified matrix by the vector, globally. A * B = C", "[TranslateGlobalF], [TranslateGlobalD], [Math]") {
	SECTION("Float matrix translation.", "[TranslateGlobalF]") 
	{
		auto TestTranslateGlobalF = [] (const GW::MATH::GMATRIXF _input, const GW::MATH::GVECTORF _translation, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::TranslateGlobalF(_input, _translation, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//translation by a 0 vector shouldn't change anything 
		TestTranslateGlobalF
		(
			{ 
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f 
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f
			}
		);

		TestTranslateGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f
			},
			{
				+0.3f, +2.0f, -7.0f, +0.0f
			},
			{
				-1.4f, +3.0f, -14.0f, 2.0f,
				-2.3f, -1.0f, +7.0f, -1.0f,
				+0.3f, +2.0f, -6.0f, +1.0f,
				+7.3f, +11.0f, -19.0f, +1.0f
			}
		);
	}	

	SECTION("Double matrix translation.", "[TranslateGlobalD]") 
	{
		auto TestTranslateGlobalD = [](const GW::MATH::GMATRIXD _input, const GW::MATH::GVECTORD _translation, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::TranslateGlobalD(_input, _translation, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//translation by a 0 vector shouldn't change anything 
		TestTranslateGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			},
			{
				+0.0, +0.0, +0.0, +0.0
			},
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			}
		);

		TestTranslateGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			},
			{
				+0.3, +2.0, -7.0, +0.0
			},
			{
				-1.4, +3.0, -14.0, 2.0,
				-2.3, -1.0, +7.0, -1.0,
				+0.3, +2.0, -6.0, +1.0,
				+7.3, +11.0, -19.0, +1.0
			}
		);
	}
}

TEST_CASE("Translate a specified matrix by the vector, locally. B * A = C", "[TranslateLocalF], [TranslateLocalD], [Math]")
{
	SECTION("Float matrix translation.", "[TranslateLocalF]")
	{
		auto TestTranslateLocalF = [] ( const GW::MATH::GMATRIXF _input, const GW::MATH::GVECTORF _translation, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::TranslateLocalF(_input, _translation, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//translation by a 0 vector shouldn't change anything 
		TestTranslateLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f
			}
		);

		TestTranslateLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+7.0f, +9.0f, -12.0f, +1.0f
			},
			{
				+0.3f, +2.0f, -7.0f, +0.0f
			},
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+2.4f, +10.7f, -19.0f, -7.4f
			}
		);	
	}

	SECTION("Double matrix translation.", "[TranslateLocalD]")
	{
		auto TestTranslateLocalD = [](const GW::MATH::GMATRIXD _input, const GW::MATH::GVECTORD _translation, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::TranslateLocalD(_input, _translation, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//translation by a 0 vector shouldn't change anything 
		TestTranslateLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			},
			{
				+0.0, +0.0, +0.0, +0.0
			},
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			}
		);

		TestTranslateLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+7.0, +9.0, -12.0, +1.0
			},
			{
				+0.3, +2.0, -7.0, +0.0
			},
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+2.4, +10.7, -19.0, -7.4
			}
		);
	}
}


TEST_CASE("Scale a specified matrix by the vector, globally. A * B = C", "[ScaleGlobalF], [ScaleGlobalD], [Math]")
{
	SECTION("Float matrix Scaling.", "[ScaleGlobalF]")
	{
		auto TestScaleGlobalF = [] (const GW::MATH::GMATRIXF _input, const GW::MATH::GVECTORF _scale, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ScaleGlobalF(_input, _scale, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//scaling by a vector full of 1s shouldn't change anything 
		TestScaleGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				+1.0f, +1.0f, +1.0f, +1.0f
			},
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);

		TestScaleGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +2.0f,
				+0.0f, +0.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);

		TestScaleGlobalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				+0.3f, +2.0f, -7.0f, +0.0f
			},
			{
				-0.6f, -2.0f, +0.0f, +2.0f,
				-0.6f, +2.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, -7.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);
	}

	SECTION("Double matrix Scaling.", "[ScaleGlobalD]")
	{
		auto TestScaleGlobalD = [](const GW::MATH::GMATRIXD _input, const GW::MATH::GVECTORD _scale, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ScaleGlobalD(_input, _scale, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//scaling by a vector full of 1s shouldn't change anything 
		TestScaleGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				+1.0, +1.0, +1.0, +1.0
			},
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);

		TestScaleGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				+0.0, +0.0, +0.0, +0.0
			},
			{
				+0.0, +0.0, +0.0, +2.0,
				+0.0, +0.0, +0.0, -1.0,
				+0.0, +0.0, +0.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);

		TestScaleGlobalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				+0.3, +2.0, -7.0, +0.0
			},
			{
				-0.6, -2.0, +0.0, +2.0,
				-0.6, +2.0, +0.0, -1.0,
				+0.0, +0.0, -7.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);
	}
}

TEST_CASE("Scale a specified matrix by the vector, locally. B * A = C", "[ScaleLocalF], [ScaleLocalD], [Math]")
{
	SECTION("Float matrix Scaling.", "[ScaleLocalF]") 
	{
		auto TestScaleLocalF = [] (const GW::MATH::GMATRIXF _input, const GW::MATH::GVECTORF _scale, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ScaleLocalF(_input, _scale, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		//scaling by a vector full of 1s shouldn't change anything 
		TestScaleLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				+1.0f, +1.0f, +1.0f, +1.0f
			},
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);

		TestScaleLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);

		TestScaleLocalF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			},
			{
				 +0.3f, +2.0f, -7.0f, +0.0f
			},
			{
				-0.6f, -0.3f, +0.0f, +0.6f,
				-4.0f, +2.0f, +0.0f, -2.0f,
				+0.0f, +0.0f, -7.0f, -7.0f,
				+12.0f, +6.0f, -89.0f, +1.86f
			}
		);
	}

	SECTION("Double matrix Scaling.", "[ScaleGlobalD]") 
	{
		auto TestScaleLocalD = [](const GW::MATH::GMATRIXD _input, const GW::MATH::GVECTORD _scale, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ScaleLocalD(_input, _scale, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		//scaling by a vector full of 1s shouldn't change anything 
		TestScaleLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				+1.0, +1.0, +1.0, +1.0
			},
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);

		TestScaleLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				+0.0, +0.0, +0.0, +0.0
			},
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);

		TestScaleLocalD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+12.0, +6.0, -89.0, +1.86
			},
			{
				 +0.3, +2.0, -7.0, +0.0
			},
			{
				-0.6, -0.3, +0.0, +0.6,
				-4.0, +2.0, +0.0, -2.0,
				+0.0, +0.0, -7.0, -7.0,
				+12.0, +6.0, -89.0, +1.86
			}
		);
	}
}


TEST_CASE("Lerp between two matrices.", "[LerpF], [LerpD], [Math]")
{
	SECTION("Float matrix lerping.", "[LerpF]")
	{
		auto TestLerpF = [] (const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b, const float _ratio, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::LerpF(_a, _b, _ratio, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestLerpF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f
			},
				+0.0f,
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			}
		);

		TestLerpF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f
			},
				+1.0f,
			{
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f
			}
		);

		TestLerpF
		(
			{
				-2.0f, -1.0f, +0.0f, +2.0f,
				-2.0f, +1.0f, +0.0f, -1.0f,
				+0.0f, +0.0f, +1.0f, +1.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f,
				+1.0f, +1.0f, +1.0f, +1.0f
			},
				+0.5f,
			{
				-0.5f, +0.0f, +0.5f, +1.5f,
				-0.5f, +1.0f, +0.5f, +0.0f,
				+0.5f, +0.5f, +1.0f, +1.0f,
				+0.5f, +0.5f, +0.5f, +1.0f
			}
		);
	}

	SECTION("Double matrix lerping.", "[LerpD]")
	{
		auto TestLerpD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GMATRIXD _b, const double _ratio, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::LerpD(_a, _b, _ratio, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestLerpD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0
			},
				+0.0,
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			}
		);

		TestLerpD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0
			},
				+1.0,
			{
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0
			}
		);

		TestLerpD
		(
			{
				-2.0, -1.0, +0.0, +2.0,
				-2.0, +1.0, +0.0, -1.0,
				+0.0, +0.0, +1.0, +1.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0,
				+1.0, +1.0, +1.0, +1.0
			},
				+0.5,
			{
				-0.5, +0.0, +0.5, +1.5,
				-0.5, +1.0, +0.5, +0.0,
				+0.5, +0.5, +1.0, +1.0,
				+0.5, +0.5, +0.5, +1.0
			}
		);
	}
}


TEST_CASE("Set a matrix to a project matrix for DirectX left-handed system.", "[ProjectionDirectXLHF], [ProjectionDirectXLHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionDirectXLHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionDirectXLHF(1.0f, 0.0f, 1.0f, 1.0f, resultF))); //projection should fail with an aspect of 0 
		}

		auto TestProjectionDirectXLHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionDirectXLHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionDirectXLHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0, 1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.00100100f, +1.0f,
				+0.0f, +0.0f, -0.100100100f, +0.0f
			}
		);
	}
	SECTION("Double matrix.", "[ProjectionDirectXLHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionDirectXLHD(1.0, 0.0, 1.0, 1.0, resultD))); //projection should fail with an aspect of 0 
		}

		auto TestProjectionDirectXLHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionDirectXLHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionDirectXLHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, +1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, +1.0010010010110211, +1.0,
				+0.0, +0.0, -0.10010010110210311, +0.0
			}
		);
	}
}

TEST_CASE("Set a matrix to a project matrix for DirectX right-handed system.", "[ProjectionDirectXRHF], [ProjectionDirectXRHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionDirectXRHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionDirectXRHF(+1.0f, +0.0f, +1.0f, +1.0f, resultF))); //projection should fail with an aspect of 0 
		}

		auto TestProjectionDirectXRHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionDirectXRHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionDirectXRHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0, +1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.00100100f, -1.0f,
				+0.0f, +0.0f, -0.100100100f, +0.0f
			}
		);
	}

	SECTION("Double matrix.", "[ProjectionDirectXRHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionDirectXRHD(+1.0, +0.0, +1.0, +1.0, resultD))); //projection should fail with an aspect of 0 
		}

		auto TestProjectionDirectXRHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionDirectXRHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionDirectXRHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, +1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, -1.0010010010110211, -1.0,
				+0.0, +0.0, -0.10010010110210311, +0.0
			}
		);
	}
}

TEST_CASE("Set a matrix to a project matrix for OpenGL right-handed system.", "[ProjectionOpenGLRHF], [ProjectionOpenGLRHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionOpenGLRHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionOpenGLRHF(+1.0f, +0.0f, +1.0f, +1.0f, resultF))); //projection with an aspect of 0 should fail
		}

		auto TestProjectionOpenGLRHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionOpenGLRHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionOpenGLRHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.00200200f, -1.0f,
				+0.0f, +0.0f, -0.200200200f, +0.0f
			}
		);
	}

	SECTION("Double matrix.", "[ProjectionOpenGLRHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionOpenGLRHD(+1.0, +0.0, +1.0, +1.0, resultD))); //projection with an aspect of 0 should fail
		}

		auto TestProjectionOpenGLRHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionOpenGLRHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionOpenGLRHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, +1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, -1.0020020020220419, -1.0,
				+0.0, +0.0, -0.20020020220420623, +0.0
			}
		);
	}
}

TEST_CASE("Set a matrix to a project matrix for OpenGL left-handed system.", "[ProjectionOpenGLLHF], [ProjectionOpenGLLHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionOpenGLLHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionOpenGLLHF(+1.0f, +0.0f, +1.0f, +1.0f, resultF))); //projection with an aspect of 0 should fail
		}

		auto TestProjectionOpenGLLHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionOpenGLLHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionOpenGLLHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.00200200f, +1.0f,
				+0.0f, +0.0f, -0.200200200f, +0.0f
			}
		);
	}

	SECTION("Double matrix.", "[ProjectionOpenGLLHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionOpenGLLHD(+1.0, +0.0, +1.0, +1.0, resultD))); //projection with an aspect of 0 should fail
		}

		auto TestProjectionOpenGLLHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionOpenGLLHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionOpenGLLHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, +1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, +1.0020020020220419, +1.0,
				+0.0, +0.0, -0.20020020220420623, +0.0
			}
		);
	}
}

TEST_CASE("Set a matrix to a project matrix for Vulkan left-handed system.", "[ProjectionVulkanLHF], [ProjectionVulkanLHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionVulkanLHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionVulkanLHF(+1.0f, +0.0f, +1.0f, +1.0f, resultF))); //projection with an aspect of 0 should fail 
		}

		auto TestProjectionVulkanLHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionVulkanLHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionVulkanLHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.00100100f, +1.0f,
				+0.0f, +0.0f, -0.100100100f, +0.0f
			}
		);
	}

	SECTION("Double matrix.", "[ProjectionVulkanLHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionVulkanLHD(+1.0, +0.0, +1.0, +1.0, resultD))); //projection with an aspect of 0 should fail 
		}

		auto TestProjectionVulkanLHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionVulkanLHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionVulkanLHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, -1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, +1.0010010010110211, +1.0,
				+0.0, +0.0, -0.100100101102103110, +0.0
			}
		);
	}
}

TEST_CASE("Set a matrix to a project matrix for Vulkan right-handed system.", "[ProjectionVulkanRHF], [ProjectionVulkanRHD], [Math]")
{
	SECTION("Float matrix.", "[ProjectionVulkanRHF]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXF resultF;
			CHECK(-(GW::MATH::GMatrix::ProjectionVulkanRHF(+1.0f, +0.0f, +1.0f, +1.0f, resultF))); //projection with an aspect of 0 should fail 
		}

		auto TestProjectionVulkanRHF = [] (const float _fovY, const float _aspectRatio, const float _zNear, const float _zFar, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::ProjectionVulkanRHF(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestProjectionVulkanRHF
		(
			+1.04719758f, +1.77864587f, +0.100000001f, +100.000000f,
			{
				+0.973803103f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.73205090f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.00100100f, -1.0f,
				+0.0f, +0.0f, -0.100100100f, +0.0f
			}
		);
	}

	SECTION("Double matrix.", "[ProjectionVulkanRHD]")
	{
		//fail cases
		{
			GW::MATH::GMATRIXD resultD;
			CHECK(-(GW::MATH::GMatrix::ProjectionVulkanRHD(+1.0, +0.0, +1.0, +1.0, resultD))); //projection with an aspect of 0 should fail 
		}

		auto TestProjectionVulkanRHD = [](const double _fovY, const double _aspectRatio, const double _zNear, const double _zFar, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::ProjectionVulkanRHD(_fovY, _aspectRatio, _zNear, _zFar, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		TestProjectionVulkanRHD
		(
			+1.04719758, +1.77864587, +0.100000001, +100.000000,
			{
				+0.97380303700481663, +0.0, +0.0, +0.0,
				+0.0, -1.7320507499620743, +0.0, +0.0,
				+0.0, +0.0, -1.0010010010110211, -1.0,
				+0.0, +0.0, -0.100100101102103110, +0.0
			}
		);
	}
}


#define TRANSFORM_MATRIX_FROM_ORIENTATION_AND_POSITION(right, forward, up, position){\
	{\
	right.x, up.x, forward.x, 0,\
	right.y, up.y, forward.y, 0,\
	right.z, up.z, forward.z, 0,\
	position.x, position.y, position.z, 1\
	}\
}

TEST_CASE("Build left-handed LookAt matrix", "[LookAtLHF], [LookAtLHD], [Math]")
{
	SECTION("Float matrix", "[LookAtLHF]")
	{
		//fail cases
		{
			GW::MATH::GVECTORF eye{ +0.0f, +0.0f, +0.0f }, at{ +0.0f, +0.0f, +0.0f }, up{ +0.0f, +1.0f, +0.0f };
			GW::MATH::GMATRIXF result;
			CHECK(-(GW::MATH::GMatrix::LookAtLHF(eye, at, up, result))); //LookAt should fail if eye == at
			eye = { +1.0f, +1.0f, +1.0f };
			at = { +1.0f, +1.0f, +1.0f };
			CHECK(-(GW::MATH::GMatrix::LookAtLHF(eye, at, up, result))); //LookAt should fail if eye == at
		}

		auto MakeExpectedLookAtF = [] (const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up) -> GW::MATH::GMATRIXF
		{
			GW::MATH::GVECTORF expectedRight, expectedForward, expectedUp, expectedPosition;
			GW::MATH::GVector::SubtractVectorF(_at, _eye, expectedForward);
			GW::MATH::GVector::NormalizeF(expectedForward, expectedForward);

			GW::MATH::GVector::CrossVector3F(_up, expectedForward, expectedRight);
			GW::MATH::GVector::NormalizeF(expectedRight, expectedRight);

			GW::MATH::GVector::CrossVector3F(expectedForward, expectedRight, expectedUp);
			GW::MATH::GVector::NormalizeF(expectedUp, expectedUp);

			expectedPosition =
			{
				-(expectedRight.x * _eye.x + expectedRight.y * _eye.y + expectedRight.z * _eye.z),
				-(expectedUp.x * _eye.x + expectedUp.y * _eye.y + expectedUp.z * _eye.z),
				-(expectedForward.x * _eye.x + expectedForward.y * _eye.y + expectedForward.z * _eye.z)
			};

			GW::MATH::GMATRIXF out = TRANSFORM_MATRIX_FROM_ORIENTATION_AND_POSITION(expectedRight, expectedForward, expectedUp, expectedPosition);
			return out;
		};


		auto TestLookAtLHF = [](const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::LookAtLHF(_eye, _at, _up, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};


		auto TestLookAtLHF_MakeExpected = [&](const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up) 
		{
			GW::MATH::GMATRIXF expectedResult = MakeExpectedLookAtF(_eye, _at, _up);
			TestLookAtLHF(_eye, _at, _up, expectedResult);
		};

		TestLookAtLHF_MakeExpected
		(
			{ +0.0f, +0.0f, +0.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ +0.0f, +0.0f, +0.0f },
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +0.0f, +0.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +0.0f, +0.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ -2.0f, -2.0f, -2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ -1.0f, -1.0f, -1.0f },
			{ -2.0f, -2.0f, -2.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ +2.0f, +2.0f, +2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ +1.0f, +1.0f, +1.0f },
			{ +2.0f, +2.0f, +2.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ +2.0f, +2.0f, +2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +0.0f, +1.0f }
		);

		TestLookAtLHF_MakeExpected
		(
			{ -2.0f, -2.0f, -2.0f },
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +0.0f, +1.0f }
		);
	}

	SECTION("Double matrix", "[LookAtLHD]")
	{
		//fail cases
		{
			GW::MATH::GVECTORD eye{ +0.0, +0.0, +0.0 }, at{ +0.0, +0.0, +0.0 }, up{ +0.0, +1.0, +0.0 };
			GW::MATH::GMATRIXD result;
			CHECK(-(GW::MATH::GMatrix::LookAtLHD(eye, at, up, result))); //LookAt should fail if eye == at
			eye = { +1.0, +1.0, +1.0 };
			at = { +1.0, +1.0, +1.0 };
			CHECK(-(GW::MATH::GMatrix::LookAtLHD(eye, at, up, result))); //LookAt should fail if eye == at
		}

		auto MakeExpectedLookAtD = [](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up) -> GW::MATH::GMATRIXD
		{
			GW::MATH::GVECTORD expectedRight, expectedForward, expectedUp, expectedPosition;
			GW::MATH::GVector::SubtractVectorD(_at, _eye, expectedForward);
			GW::MATH::GVector::NormalizeD(expectedForward, expectedForward);

			GW::MATH::GVector::CrossVector3D(_up, expectedForward, expectedRight);
			GW::MATH::GVector::NormalizeD(expectedRight, expectedRight);

			GW::MATH::GVector::CrossVector3D(expectedForward, expectedRight, expectedUp);
			GW::MATH::GVector::NormalizeD(expectedUp, expectedUp);

			expectedPosition =
			{
				-(expectedRight.x * _eye.x + expectedRight.y * _eye.y + expectedRight.z * _eye.z),
				-(expectedUp.x * _eye.x + expectedUp.y * _eye.y + expectedUp.z * _eye.z),
				-(expectedForward.x * _eye.x + expectedForward.y * _eye.y + expectedForward.z * _eye.z)
			};

			GW::MATH::GMATRIXD out = TRANSFORM_MATRIX_FROM_ORIENTATION_AND_POSITION(expectedRight, expectedForward, expectedUp, expectedPosition);
			return out;
		};

		auto TestLookAtLHD = [](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::LookAtLHD(_eye, _at, _up, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};

		auto TestLookAtLHD_MakeExpected = [&](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up)
		{
			GW::MATH::GMATRIXD expectedResult = MakeExpectedLookAtD(_eye, _at, _up);
			TestLookAtLHD(_eye, _at, _up, expectedResult);
		};

		TestLookAtLHD_MakeExpected
		(
			{ +0.0, +0.0, +0.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ +0.0, +0.0, +0.0 },
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +0.0, +0.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +0.0, +0.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ -2.0, -2.0, -2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ -1.0, -1.0, -1.0 },
			{ -2.0, -2.0, -2.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ +2.0, +2.0, +2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ +1.0, +1.0, +1.0 },
			{ +2.0, +2.0, +2.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ +2.0, +2.0, +2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +0.0, +1.0 }
		);

		TestLookAtLHD_MakeExpected
		(
			{ -2.0, -2.0, -2.0 },
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +0.0, +1.0 }
		);
	}
}

TEST_CASE("Build right-handed LookAt matrix", "[LookAtRHF], [LookAtRHD], [Math]")
{
	SECTION("Float matrix", "[LookAtRHF]")
	{
		//fail cases
		{
			GW::MATH::GVECTORF eye{ +0.0f, +0.0f, +0.0f }, at{ +0.0f, +0.0f, +0.0f }, up{ +0.0f, +1.0f, +0.0f };
			GW::MATH::GMATRIXF result;

			CHECK(-(GW::MATH::GMatrix::LookAtRHF(eye, at, up, result))); //LookAt should fail if eye == at
			eye = { +1.0f, +1.0f, +1.0f };
			at = { +1.0f, +1.0f, +1.0f };
			CHECK(-(GW::MATH::GMatrix::LookAtRHF(eye, at, up, result))); //LookAt should fail if eye == at
		}

		auto MakeExpectedLookAtF = [&](const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up) -> GW::MATH::GMATRIXF
		{
			GW::MATH::GVECTORF	expectedRight, expectedForward, expectedUp, expectedPosition;

			GW::MATH::GVector::SubtractVectorF(_eye, _at, expectedForward);
			GW::MATH::GVector::NormalizeF(expectedForward, expectedForward);

			GW::MATH::GVector::CrossVector3F(_up, expectedForward, expectedRight);
			GW::MATH::GVector::NormalizeF(expectedRight, expectedRight);

			GW::MATH::GVector::CrossVector3F(expectedForward, expectedRight, expectedUp);
			GW::MATH::GVector::NormalizeF(expectedUp, expectedUp);

			expectedPosition =
			{
				-(expectedRight.x * _eye.x + expectedRight.y * _eye.y + expectedRight.z * _eye.z),
				-(expectedUp.x * _eye.x + expectedUp.y * _eye.y + expectedUp.z * _eye.z),
				-(expectedForward.x * _eye.x + expectedForward.y * _eye.y + expectedForward.z * _eye.z)
			};

			GW::MATH::GMATRIXF out = TRANSFORM_MATRIX_FROM_ORIENTATION_AND_POSITION(expectedRight, expectedForward, expectedUp, expectedPosition);
			return out;
		};

		auto TestLookAtRHF = [](const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::LookAtRHF(_eye, _at, _up, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};


		auto TestLookAtRHF_MakeExpected = [&](const GW::MATH::GVECTORF _eye, const GW::MATH::GVECTORF _at, const GW::MATH::GVECTORF _up)
		{
			GW::MATH::GMATRIXF expectedResult = MakeExpectedLookAtF(_eye, _at, _up);
			TestLookAtRHF(_eye, _at, _up, expectedResult);
		};

		TestLookAtRHF_MakeExpected
		(
			{ +0.0f, +0.0f, +0.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ +0.0f, +0.0f, +0.0f },
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +0.0f, +0.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +0.0f, +0.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ -2.0f, -2.0f, -2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ -1.0f, -1.0f, -1.0f },
			{ -2.0f, -2.0f, -2.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ +2.0f, +2.0f, +2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ +1.0f, +1.0f, +1.0f },
			{ +2.0f, +2.0f, +2.0f },
			{ +0.0f, +1.0f, +0.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ +2.0f, +2.0f, +2.0f },
			{ +1.0f, +1.0f, +1.0f },
			{ +0.0f, +0.0f, +1.0f }
		);

		TestLookAtRHF_MakeExpected
		(
			{ -2.0f, -2.0f, -2.0f },
			{ -1.0f, -1.0f, -1.0f },
			{ +0.0f, +0.0f, +1.0f }
		);

		
		//new test case from Lari 
		{
			GW::MATH::GMATRIXF expectedResult;
			GW::MATH::GMatrix::TranslateGlobalF(GW::MATH::GIdentityMatrixF, GW::MATH::GVECTORF{ 0.0f, 0.5f, 1.5f }, expectedResult); //translate an identity matrix slightly up and backwards
			GW::MATH::GMatrix::InverseF(expectedResult, expectedResult); //inverse the matrix to turn it from a world matrix to a view matrix 
			
			TestLookAtRHF
			(
				{ +0.0f, +0.5f, +1.5f },
				{ +0.0f, +0.5f, +0.0f },
				{ +0.0f, +1.0f, +0.0f },
				expectedResult
			);
		}
	}

	SECTION("Double matrix", "[LookAtLHD]")
	{
		//fail cases
		{
			GW::MATH::GVECTORD eye{ +0.0, +0.0, +0.0 }, at{ +0.0, +0.0, +0.0 }, up{ +0.0, +1.0, +0.0 };
			GW::MATH::GMATRIXD result;

			CHECK(-(GW::MATH::GMatrix::LookAtRHD(eye, at, up, result))); //LookAt should fail if eye == at
			eye = { +1.0, +1.0, +1.0 };
			at  = { +1.0, +1.0, +1.0 };
			CHECK(-(GW::MATH::GMatrix::LookAtRHD(eye, at, up, result))); //LookAt should fail if eye == at
		}

		auto MakeExpectedLookAtD = [&](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up) -> GW::MATH::GMATRIXD
		{
			GW::MATH::GVECTORD	expectedRight, expectedForward, expectedUp, expectedPosition;

			GW::MATH::GVector::SubtractVectorD(_eye, _at, expectedForward);
			GW::MATH::GVector::NormalizeD(expectedForward, expectedForward);

			GW::MATH::GVector::CrossVector3D(_up, expectedForward, expectedRight);
			GW::MATH::GVector::NormalizeD(expectedRight, expectedRight);

			GW::MATH::GVector::CrossVector3D(expectedForward, expectedRight, expectedUp);
			GW::MATH::GVector::NormalizeD(expectedUp, expectedUp);

			expectedPosition =
			{
				-(expectedRight.x * _eye.x + expectedRight.y * _eye.y + expectedRight.z * _eye.z),
				-(expectedUp.x * _eye.x + expectedUp.y * _eye.y + expectedUp.z * _eye.z),
				-(expectedForward.x * _eye.x + expectedForward.y * _eye.y + expectedForward.z * _eye.z)
			};

			GW::MATH::GMATRIXD out = TRANSFORM_MATRIX_FROM_ORIENTATION_AND_POSITION(expectedRight, expectedForward, expectedUp, expectedPosition);
			return out;
		};

		auto TestLookAtRHD = [](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::LookAtRHD(_eye, _at, _up, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};


		auto TestLookAtRHD_MakeExpected = [&](const GW::MATH::GVECTORD _eye, const GW::MATH::GVECTORD _at, const GW::MATH::GVECTORD _up)
		{
			GW::MATH::GMATRIXD expectedResult = MakeExpectedLookAtD(_eye, _at, _up);
			TestLookAtRHD(_eye, _at, _up, expectedResult);
		};

		TestLookAtRHD_MakeExpected
		(
			{ +0.0, +0.0, +0.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ +0.0, +0.0, +0.0 },
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +0.0, +0.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +0.0, +0.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ -2.0, -2.0, -2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ -1.0, -1.0, -1.0 },
			{ -2.0, -2.0, -2.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ +2.0, +2.0, +2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ +1.0, +1.0, +1.0 },
			{ +2.0, +2.0, +2.0 },
			{ +0.0, +1.0, +0.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ +2.0, +2.0, +2.0 },
			{ +1.0, +1.0, +1.0 },
			{ +0.0, +0.0, +1.0 }
		);

		TestLookAtRHD_MakeExpected
		(
			{ -2.0, -2.0, -2.0 },
			{ -1.0, -1.0, -1.0 },
			{ +0.0, +0.0, +1.0 }
		);


		//new test case from Lari 
		{
			GW::MATH::GMATRIXD expectedResult;
			GW::MATH::GMatrix::TranslateGlobalD(GW::MATH::GIdentityMatrixD, GW::MATH::GVECTORD{ 0.0, 0.5, 1.5 }, expectedResult); //translate an identity matrix slightly up and backwards
			GW::MATH::GMatrix::InverseD(expectedResult, expectedResult); //inverse the matrix to turn it from a world matrix to a view matrix 

			TestLookAtRHD
			(
				{ +0.0, +0.5, +1.5 },
				{ +0.0, +0.5, +0.0 },
				{ +0.0, +1.0, +0.0 },
				expectedResult
			);
		}
	}
}


TEST_CASE("Make two matrices relative.", "[MakeRelativeF], [MakeRelativeD], [Math]")
{
	SECTION("Float matrix relativation.", "[MakeRelativeF]") 
	{
		//fail cases
		{
			GW::MATH::GMATRIXF 
				nonInvertibleMatrix
				{
					+1.0f, +1.0f, +1.0f, +1.0f,
					+1.0f, +1.0f, +1.0f, +1.0f,
					+1.0f, +1.0f, +1.0f, +1.0f,
					+1.0f, +1.0f, +1.0f, +1.0f
				}, 
				a, result;
				CHECK(-(GW::MATH::GMatrix::MakeRelativeF(a, nonInvertibleMatrix, result)));
		}

		auto TestMakeRelativeF = [] (const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::MakeRelativeF(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		TestMakeRelativeF
		(
			{
				+5.0f, -1.0f, +2.0f, +4.0f,
				+1.0f, +3.5f, +1.0f, +6.0f,
				+7.1f, +1.2f, -10.1f, +9.6f,
				+5.0f, +63.25f, +6.9f, +2.0f
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f 
			},
			{
				-2.5546855520819f, -0.2279483687834f, +0.4378440322983f, +0.5271965323462f,
				-1.0018428854964f, +0.8031845907921f, +0.254093666069f, -0.0430811273049f,
				-10.3244930990172f, +1.2320650171855f, +0.6374708091302f, +0.7736926359314f,
				-0.3584492480644f, +9.5384370426268f, -3.0884873557487f, +1.3330310357393f 
			}
		);
	}

	SECTION("Double matrix relativation.", "[MakeRelativeD]") 
	{
		//fail cases
		{
			GW::MATH::GMATRIXD
				nonInvertibleMatrix
				{
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0,
					+1.0, +1.0, +1.0, +1.0
				},
				a, result;

			CHECK(-(GW::MATH::GMatrix::MakeRelativeD(a, nonInvertibleMatrix, result)));
		}

		auto TestMakeRelativeD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GMATRIXD _b, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::MakeRelativeD(_a, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _expectedResult);
		};
		TestMakeRelativeD
		(
			{
				+5.0, -1.0, +2.0, +4.0,
				+1.0, +3.5, +1.0, +6.0,
				+7.1, +1.2, -10.1, +9.6,
				+5.0, +63.25, +6.9, +2.0 
			},
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0 
			},
			{
				-2.5546855520819349, -0.22794836878344421, +0.43784403229827962, +0.52719653234622843,
				-1.0018428854963954, +0.80318459079212001, +0.25409366606901573, -0.043081127304929578,
				-10.324493099017149, +1.2320650171854943, +0.63747080913023613, +0.77369263593140636,
				-0.35844924806436157, +9.5384370426267715, -3.0884873557486583, +1.3330310357393467 
			}
		);
	}
}

TEST_CASE("Make two matrices un-relative.", "[MakeSeparateF], [MakeSeparateD], [Math]")
{
	SECTION("Float matrix un-relativation.", "[MakeSeparateF]") 
	{
		auto TestMakeSeparateF = [] (const GW::MATH::GMATRIXF _a, const GW::MATH::GMATRIXF _b)
		{
			GW::MATH::GMATRIXF relativeResult, result;
			GW::MATH::GMatrix::MakeRelativeF(_a, _b, relativeResult);
			CHECK(+(GW::MATH::GMatrix::MakeSeparateF(relativeResult, _b, result)));
			COMPARE_MATRICES_STANDARD_F(result, _a);
		};
		

		for (float i = -10.0f; i < 10.0f; i+= 0.1f)
		{
			float valF = static_cast<float>(i);
			GW::MATH::GMATRIXF a, b, rotation;

			GW::MATH::GMatrix::IdentityF(a);
			GW::MATH::GMatrix::IdentityF(b);

			GW::MATH::GMatrix::RotationYawPitchRollF(valF, valF + 1.0f, valF + 2.0f, rotation);
			GW::MATH::GMatrix::MultiplyMatrixF(a, rotation, a);

			GW::MATH::GMatrix::RotationYawPitchRollF(valF + 3.0f, valF + 4.0f, valF + 5.0f, rotation);
			GW::MATH::GMatrix::MultiplyMatrixF(b, rotation, b);
			
			TestMakeSeparateF(a, b);
		}

		TestMakeSeparateF
		(
			{
				+5.0f, -1.0f, +2.0f, +4.0f,
				+1.0f, +3.5f, +1.0f, +6.0f,
				+7.1f, +1.0f, -1.1f, +9.6f,
				+5.0f, +3.2f, +6.9f, +2.0f 
			},
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +2.4f,
				+7.6f, +5.5f, +9.8f, +1.0f 
			}
		);
	}

	SECTION("Double matrix un-relativation.", "[MakeSeparateD]") 
	{
		auto TestMakeSeparateD = [](const GW::MATH::GMATRIXD _a, const GW::MATH::GMATRIXD _b)
		{
			GW::MATH::GMATRIXD relativeResult, result;
			GW::MATH::GMatrix::MakeRelativeD(_a, _b, relativeResult);
			CHECK(+(GW::MATH::GMatrix::MakeSeparateD(relativeResult, _b, result)));
			COMPARE_MATRICES_STANDARD_D(result, _a);
		};

		for (double i = -10.0f; i < 10.0f; i += 0.1f)
		{
			double valD = static_cast<double>(i);
			GW::MATH::GMATRIXD a, b, rotation;

			GW::MATH::GMatrix::IdentityD(a);
			GW::MATH::GMatrix::IdentityD(b);

			GW::MATH::GMatrix::RotationYawPitchRollD(valD, valD + 1.0, valD + 2.0, rotation);
			GW::MATH::GMatrix::MultiplyMatrixD(a, rotation, a);

			GW::MATH::GMatrix::RotationYawPitchRollD(valD + 3.0, valD + 4.0, valD + 5.0, rotation);
			GW::MATH::GMatrix::MultiplyMatrixD(b, rotation, b);

			TestMakeSeparateD(a, b);
		}

		TestMakeSeparateD
		(
			{
				5.0, -1.0, 2.0, 4.0,
				1.0, 3.5, 1.0, 6.0,
				7.1, 1.0, -10.1, 9.6,
				5.0, 63.25, 6.9, 1.0
			},
			{
				0.2, 1.0, 2.5, 0.4,
				0.7, 5.9, 2.2, 4.1,
				3.8, 0.0, 8.5, 12.4,
				7.6, 5.5, 9.8, 1.0 
			}
		);
	}
}


TEST_CASE("Cast matrices between float and double.", "[Upgrade], [Downgrade], [Math]")
{
	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with 
	// the accuracy of the least accurate type. ex: testing float precision using 
	// double precision will cause false errors. 
	SECTION("Cast float matrix to double matrix.", "[Upgrade]") 
	{
		auto TestUpgrade = [] (const GW::MATH::GMATRIXF _input, const GW::MATH::GMATRIXD _expectedResult)
		{
			GW::MATH::GMATRIXD result;
			CHECK(+(GW::MATH::GMatrix::Upgrade(_input, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		for (int i = -1000; i < 1000; i++)
		{
			GW::MATH::GMATRIXF matrixF;
			GW::MATH::GMATRIXD matrixD;
			float valF = static_cast<float>(i);
			double valD = static_cast<double>(i);
			for (int j = 0; j < 16; j++)
			{
				matrixF.data[j] = valF + j;
				matrixD.data[j] = valD + j;
			}
			TestUpgrade(matrixF, matrixD);
		}
	}

	SECTION("Cast double matrix to float matrix.", "[Downgrade]") 
	{
		auto TestDowngrade = [](const GW::MATH::GMATRIXD _input, const GW::MATH::GMATRIXF _expectedResult)
		{
			GW::MATH::GMATRIXF result;
			CHECK(+(GW::MATH::GMatrix::Downgrade(_input, result)));
			COMPARE_MATRICES_STANDARD_F(result, _expectedResult);
		};

		for (int i = -1000; i < 1000; i++)
		{
			GW::MATH::GMATRIXF matrixF;
			GW::MATH::GMATRIXD matrixD;
			float valF = static_cast<float>(i);
			double valD = static_cast<double>(i);
			for (int j = 0; j < 16; j++)
			{
				matrixF.data[j] = valF + j;
				matrixD.data[j] = valD + j;
			}
			TestDowngrade(matrixD, matrixF);
		}
	}
}
#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GMATRIX) */