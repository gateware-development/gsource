#if defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GVECTOR)
TEST_CASE("Create GVector.", "[CreateGVector], [Math]")
{
	GW::MATH::GVector VectorC;
	//Pass Cases
	REQUIRE(+(VectorC.Create()));
}

TEST_CASE("Add two vectors.", "[AddVectorF], [AddVectorD], [Math]")
{
	SECTION("Float vector addition.", "[AddVectorF]")
	{
		auto AdditionTestF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::AddVectorF(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		AdditionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +2.0f, +3.0f, +4.0f });
		AdditionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, -2.0f, -3.0f, -4.0f });
		AdditionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +0.1f, +0.2f, +0.3f, +0.4f }, { +1.1f, +2.2f, +3.3f, +4.4f });
		AdditionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -0.1f, -0.2f, -0.3f, -0.4f }, { -1.1f, -2.2f, -3.3f, -4.4f });
		AdditionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -0.1f, -0.2f, -0.3f, -0.4f }, { +0.9f, +1.8f, +2.7f, +3.6f });
		AdditionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +0.1f, +0.2f, +0.3f, +0.4f }, { -0.9f, -1.8f, -2.7f, -3.6f });
		AdditionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -1.0f, -2.0f, -3.0f, -4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
	}

	SECTION("Double vector addition.", "[AddVectorD]")
	{
		auto AdditionTestD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::AddVectorD(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		AdditionTestD({ +1.0, +2.0, +3.0, +4.0 }, { +0.0, +0.0, +0.0, +0.0 }, { +1.0, +2.0, +3.0, +4.0 });
		AdditionTestD({ -1.0, -2.0, -3.0, -4.0 }, { +0.0, +0.0, +0.0, +0.0 }, { -1.0, -2.0, -3.0, -4.0 });
		AdditionTestD({ +1.0, +2.0, +3.0, +4.0 }, { +0.1, +0.2, +0.3, +0.4 }, { +1.1, +2.2, +3.3, +4.4 });
		AdditionTestD({ -1.0, -2.0, -3.0, -4.0 }, { -0.1, -0.2, -0.3, -0.4 }, { -1.1, -2.2, -3.3, -4.4 });
		AdditionTestD({ +1.0, +2.0, +3.0, +4.0 }, { -0.1, -0.2, -0.3, -0.4 }, { +0.9, +1.8, +2.7, +3.6 });
		AdditionTestD({ -1.0, -2.0, -3.0, -4.0 }, { +0.1, +0.2, +0.3, +0.4 }, { -0.9, -1.8, -2.7, -3.6 });
		AdditionTestD({ +1.0, +2.0, +3.0, +4.0 }, { -1.0, -2.0, -3.0, -4.0 }, { +0.0, +0.0, +0.0, +0.0 });
	}
}

TEST_CASE("Subtract two vectors.", "[SubtractVectorF], [SubtractVectorD], [Math]")
{
	SECTION("Float vector subtraction.", "[SubtractVectorF]")
	{
		auto SubtractionTestF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::SubtractVectorF(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		SubtractionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +2.0f, +3.0f, +4.0f });
		SubtractionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, -2.0f, -3.0f, -4.0f });
		SubtractionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +1.0f, +2.0f, +3.0f, +4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		SubtractionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -1.0f, -2.0f, -3.0f, -4.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		SubtractionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +0.1f, +0.2f, +0.3f, +0.4f }, { +0.9f, +1.8f, +2.7f, +3.6f });
		SubtractionTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -0.1f, -0.2f, -0.3f, -0.4f }, { +1.1f, +2.2f, +3.3f, +4.4f });
		SubtractionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +0.1f, +0.2f, +0.3f, +0.4f }, { -1.1f, -2.2f, -3.3f, -4.4f });
		SubtractionTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -0.1f, -0.2f, -0.3f, -0.4f }, { -0.9f, -1.8f, -2.7f, -3.6f });
	}

	SECTION("Double vector subtraction.", "[SubtractVectorD]")
	{
		auto SubtractionTestD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::SubtractVectorD(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		SubtractionTestD({ +1.0, +2.0, +3.0, +4.0 }, { +0.0, +0.0, +0.0, +0.0 }, { +1.0, +2.0, +3.0, +4.0 });
		SubtractionTestD({ -1.0, -2.0, -3.0, -4.0 }, { +0.0, +0.0, +0.0, +0.0 }, { -1.0, -2.0, -3.0, -4.0 });
		SubtractionTestD({ +1.0, +2.0, +3.0, +4.0 }, { +1.0, +2.0, +3.0, +4.0 }, { +0.0, +0.0, +0.0, +0.0 });
		SubtractionTestD({ -1.0, -2.0, -3.0, -4.0 }, { -1.0, -2.0, -3.0, -4.0 }, { +0.0, +0.0, +0.0, +0.0 });
		SubtractionTestD({ +1.0, +2.0, +3.0, +4.0 }, { +0.1, +0.2, +0.3, +0.4 }, { +0.9, +1.8, +2.7, +3.6 });
		SubtractionTestD({ +1.0, +2.0, +3.0, +4.0 }, { -0.1, -0.2, -0.3, -0.4 }, { +1.1, +2.2, +3.3, +4.4 });
		SubtractionTestD({ -1.0, -2.0, -3.0, -4.0 }, { +0.1, +0.2, +0.3, +0.4 }, { -1.1, -2.2, -3.3, -4.4 });
		SubtractionTestD({ -1.0, -2.0, -3.0, -4.0 }, { -0.1, -0.2, -0.3, -0.4 }, { -0.9, -1.8, -2.7, -3.6 });
	}
}

TEST_CASE("Scale a vector.", "[ScaleF], [ScaleD], [Math]")
{
	SECTION("Float vector.", "[ScaleF]")
	{
		auto ScaleTestF = [](const GW::MATH::GVECTORF _a, const float _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::ScaleF(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//scaling a vector by 1 shouldn't change anything 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, +1.0f, { +1.0f, +2.0f, +3.0f, +4.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, +1.0f, { -1.0f, -2.0f, -3.0f, -4.0f });

		//scaling a vector by -1 should flip the signs of all its components 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, -1.0f, { -1.0f, -2.0f, -3.0f, -4.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, -1.0f, { +1.0f, +2.0f, +3.0f, +4.0f });

		//scaling a vector by 0 shoud 0 out all of its components 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, +0.0f, { +0.0f, +0.0f, +0.0f, +0.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, +0.0f, { +0.0f, +0.0f, +0.0f, +0.0f });

		//scaling a vector by 2 should double all its components 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, +2.0f, { +2.0f, +4.0f, +6.0f, +8.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, +2.0f, { -2.0f, -4.0f, -6.0f, -8.0f });

		//scaling a vector by -2 should double all of its components and flip their signs 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, -2.0f, { -2.0f, -4.0f, -6.0f, -8.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, -2.0f, { +2.0f, +4.0f, +6.0f, +8.0f });

		//scaling a vector by 0.5 should halve each of its components
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, +0.5f, { +0.5f, +1.0f, +1.5f, +2.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, +0.5f, { -0.5f, -1.0f, -1.5f, -2.0f });

		//scaling a vector by -0.5 should halve each of its components and flip their signs 
		ScaleTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, -0.5f, { -0.5f, -1.0f, -1.5f, -2.0f });
		ScaleTestF({ -1.0f, -2.0f, -3.0f, -4.0f }, -0.5f, { +0.5f, +1.0f, +1.5f, +2.0f });
	}

	SECTION("Double vector.", "[ScaleD]")
	{

		auto ScaleTestD = [](const GW::MATH::GVECTORD _a, const double _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::ScaleD(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//scaling a vector by 1 shouldn't change anything 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, +1.0, { +1.0, +2.0, +3.0, +4.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, +1.0, { -1.0, -2.0, -3.0, -4.0 });

		//scaling a vector by -1 should flip the signs of all its components 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, -1.0, { -1.0, -2.0, -3.0, -4.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, -1.0, { +1.0, +2.0, +3.0, +4.0 });

		//scaling a vector by 0 shoud 0 out all of its components 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, +0.0, { +0.0, +0.0, +0.0, +0.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, +0.0, { +0.0, +0.0, +0.0, +0.0 });

		//scaling a vector by 2 should double all its components 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, +2.0, { +2.0, +4.0, +6.0, +8.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, +2.0, { -2.0, -4.0, -6.0, -8.0 });

		//scaling a vector by -2 should double all of its components and flip their signs 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, -2.0, { -2.0, -4.0, -6.0, -8.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, -2.0, { +2.0, +4.0, +6.0, +8.0 });

		//scaling a vector by 0.5 should halve each of its components
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, +0.5, { +0.5, +1.0, +1.5, +2.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, +0.5, { -0.5, -1.0, -1.5, -2.0 });

		//scaling a vector by -0.5 should halve each of its components and flip their signs 
		ScaleTestD({ +1.0, +2.0, +3.0, +4.0 }, -0.5, { -0.5, -1.0, -1.5, -2.0 });
		ScaleTestD({ -1.0, -2.0, -3.0, -4.0 }, -0.5, { +0.5, +1.0, +1.5, +2.0 });
	}
}

TEST_CASE("Calculate the dot product of two vectors.", "[DotF], [DotD], [Math]")
{
	SECTION("Float vectors.", "[DotF]")
	{
		auto DotProductTestF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const float _expectedResult)
		{
			float result;
			CHECK(+(GW::MATH::GVector::DotF(_a, _b, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_F(result, _expectedResult));
		};

		//Parallel normalized vectors should have a dot product of 1 
		DotProductTestF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, +1.0f);
		DotProductTestF({ -1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, +1.0f);

		//Antiparallel normalized vectors should have a dot product of -1 
		DotProductTestF({ +1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, -1.0f);
		DotProductTestF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, -1.0f);

		//Parallel vectors with magnitude of 2 should have a dot product of 4 
		DotProductTestF({ +2.0f, +0.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, +4.0f);
		DotProductTestF({ -2.0f, +0.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, +4.0f);

		//Antiparallel vectors with magnitude 2 should have a dot product of -4 
		DotProductTestF({ +2.0f, +0.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, -4.0f);
		DotProductTestF({ -2.0f, +0.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, -4.0f);

		//Perpindicular vectors should have a dot product of 0 
		DotProductTestF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, +0.0f);
		DotProductTestF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, +0.0f);
		DotProductTestF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +1.0f }, +0.0f);

		DotProductTestF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +0.1f, +0.2f, +0.3f, +0.4f }, +3.0f);

	}

	SECTION("Double vectors.", "[DotD]")
	{
		auto DotProductTestD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const double _expectedResult)
		{
			double result;
			CHECK(+(GW::MATH::GVector::DotD(_a, _b, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_D(result, _expectedResult));
		};

		//Parallel normalized vectors should have a dot product of 1 
		DotProductTestD({ +1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, +1.0);
		DotProductTestD({ -1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, +1.0);

		//Antiparallel normalized vectors should have a dot product of -1 
		DotProductTestD({ +1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, -1.0);
		DotProductTestD({ -1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, -1.0);

		//Parallel vectors with magnitude of 2 should have a dot product of 4 
		DotProductTestD({ +2.0, +0.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, +4.0);
		DotProductTestD({ -2.0, +0.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, +4.0);

		//Antiparallel vectors with magnitude 2 should have a dot product of -4 
		DotProductTestD({ +2.0, +0.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, -4.0);
		DotProductTestD({ -2.0, +0.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, -4.0);

		//Perpindicular vectors should have a dot product of 0 
		DotProductTestD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, +0.0);
		DotProductTestD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, +0.0);
		DotProductTestD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +1.0 }, +0.0);

		DotProductTestD({ +1.0, +2.0, +3.0, +4.0 }, { +0.1, +0.2, +0.3, +0.4 }, +3.0);
	}
}

TEST_CASE("Calculate the cross product of two 2d vectors.", "[CrossVector2F], [CrossVector2D], [Math]")
{
	SECTION("Float vectors.", "[CrossVector2F]")
	{
		auto TestCrossProduct2F = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const float _expectedResult)
		{
			float result;
			CHECK(+(GW::MATH::GVector::CrossVector2F(_a, _b, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_F(result, _expectedResult));
		};

		//parellel and anti-parallel 2d vectors should have a 2d cross product of 0 
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, +0.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, +0.0f);

		//Right-handed normalized perpendicular vectors should have a cross product of 1 
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, +1.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, +1.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, +1.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, +1.0f);

		//Left-handed normalized perpendicular vectors should have a cross product of -1 
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, -1.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, -1.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, -1.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, -1.0f);

		//Right-handed perpendicular vectors with a magnitude product of 2 should have a cross product of 2 
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ +0.0f, +2.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, +2.0f);
		TestCrossProduct2F({ +0.0f, -2.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, +2.0f);

		//Left-handed perpendicular vectors with a magnitude product of 2 should have a cross product of -2 
		TestCrossProduct2F({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ +0.0f, +1.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ +0.0f, +2.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ +0.0f, -1.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, -2.0f);
		TestCrossProduct2F({ +0.0f, -2.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, -2.0f);

		//Test with random-ish numbers 
		TestCrossProduct2F({ +0.835115f, +0.790102f, +0.0f, +0.0f }, { +0.623082f, +0.029450f, +0.0f, +0.0f }, -0.467704f);
		//Having values in the z and w components shouldn't change anything 
		TestCrossProduct2F({ +0.835115f, +0.790102f, +3.0f, +4.0f }, { +0.623082f, +0.029450f, +0.3f, +0.4f }, -0.467704f);
	}

	SECTION("Double vectors.", "[CrossVector2D]")
	{
		auto TestCrossProduct2D = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const double _expectedResult)
		{
			double result;
			CHECK(+(GW::MATH::GVector::CrossVector2D(_a, _b, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_D(result, _expectedResult));
		};

		//parellel and anti-parallel 2d vectors should have a 2d cross product of 0 
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, +0.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, +0.0);

		//Right-handed normalized perpendicular vectors should have a cross product of 1 
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, +1.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, +1.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, +1.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, +1.0);

		//Left-handed normalized perpendicular vectors should have a cross product of -1 
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, -1.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, -1.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, -1.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, -1.0);

		//Right-handed perpendicular vectors with a magnitude product of 2 should have a cross product of 2 
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ +0.0, +2.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, +2.0);
		TestCrossProduct2D({ +0.0, -2.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, +2.0);

		//Left-handed perpendicular vectors with a magnitude product of 2 should have a cross product of -2 
		TestCrossProduct2D({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ +0.0, +1.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ +0.0, +2.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ +0.0, -1.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, -2.0);
		TestCrossProduct2D({ +0.0, -2.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, -2.0);

		//Test with random-ish numbers 
		TestCrossProduct2D({ +0.835115, +0.790102, +0.0, +0.0 }, { +0.623082, +0.029450, +0.0, +0.0 }, -0.467704197614);
		//Having values in the z and w components shouldn't change anything 
		TestCrossProduct2D({ +0.835115, +0.790102, +3.0, +4.0 }, { +0.623082, +0.029450, +0.3, +0.4 }, -0.467704197614);
	}
}

TEST_CASE("Calculate the cross product of two vectors.", "[CrossVector3F], [CrossVector3D], [Math]")
{
	SECTION("Float vector.", "[CrossVector3F]")
	{
		auto TestCrossProductF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::CrossVector3F(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//Two 0 vectors should have a cross product of 0 
		TestCrossProductF({ +0.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });

		//Any vector crossed with a 0 vector should result in a 0 vector 
		TestCrossProductF({ +1.0f, +1.0f, +1.0f, +1.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ -1.0f, -1.0f, -1.0f, -1.0f }, { +0.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +1.0f, +1.0f, +1.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, -1.0f, -1.0f, -1.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });

		//Parallel vectors should have a 0 vector as their cross product 
		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });

		//Antiparallel vectors should have a 0 vector as their cross product 
		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +0.0f, +0.0f, +0.0f });

		//Normalized perpendicular vectors should have normalized perpendicular cross products 
		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f });	//+I x +J == +K 
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f });	//-I x -J == +K 
		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f });	//+I x -J == -K 
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f });	//-I x +J == -K 

		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f });	//+I x +K == -J 
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f });	//-I x -K == -J 
		TestCrossProductF({ +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f });	//+I x -K == +J 
		TestCrossProductF({ -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f });	//-I x +K == +J 

		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f });	//+J x +K == +I 
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f });	//-J x -K == +I 
		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f });	//+J x -K == -I 
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f });	//-J x +K == -I 

		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f });	//+J x +I == -K 
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -1.0f, +0.0f });	//-J x -I == -K 
		TestCrossProductF({ +0.0f, +1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f });	//+J x -I == +K 
		TestCrossProductF({ +0.0f, -1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +1.0f, +0.0f });	//-J x +I == +K 

		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f });	//+K x +J == -I 
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f });	//-K x -J == -I 
		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f });	//+K x -J == +I 
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f });	//-K x +J == +I 

		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f });	//+K x +I == +J 
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +1.0f, +0.0f, +0.0f });	//-K x -I == +J 
		TestCrossProductF({ +0.0f, +0.0f, +1.0f, +0.0f }, { -1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f });	//+K x -I == -J 
		TestCrossProductF({ +0.0f, +0.0f, -1.0f, +0.0f }, { +1.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -1.0f, +0.0f, +0.0f });	//-K x +I == -J 

		//Perpendicular vectors with a magnitude of 2 should have perpendicular cross products with a magnitude of 4 
		TestCrossProductF({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +4.0f, +0.0f });	//+I x +J == +K 
		TestCrossProductF({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +4.0f, +0.0f });	//-I x -J == +K 
		TestCrossProductF({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -4.0f, +0.0f });	//+I x -J == -K 
		TestCrossProductF({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -4.0f, +0.0f });	//-I x +J == -K 

		TestCrossProductF({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +2.0f, +0.0f }, { +0.0f, -4.0f, +0.0f, +0.0f });	//+I x +K == -J 
		TestCrossProductF({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -2.0f, +0.0f }, { +0.0f, -4.0f, +0.0f, +0.0f });	//-I x -K == -J 
		TestCrossProductF({ +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -2.0f, +0.0f }, { +0.0f, +4.0f, +0.0f, +0.0f });	//+I x -K == +J 
		TestCrossProductF({ -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +2.0f, +0.0f }, { +0.0f, +4.0f, +0.0f, +0.0f });	//-I x +K == +J 

		TestCrossProductF({ +0.0f, +2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +2.0f, +0.0f }, { +4.0f, +0.0f, +0.0f, +0.0f });	//+J x +K == +I 
		TestCrossProductF({ +0.0f, -2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -2.0f, +0.0f }, { +4.0f, +0.0f, +0.0f, +0.0f });	//-J x -K == +I 
		TestCrossProductF({ +0.0f, +2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -2.0f, +0.0f }, { -4.0f, +0.0f, +0.0f, +0.0f });	//+J x -K == -I 
		TestCrossProductF({ +0.0f, -2.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +2.0f, +0.0f }, { -4.0f, +0.0f, +0.0f, +0.0f });	//-J x +K == -I 

		TestCrossProductF({ +0.0f, +2.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -4.0f, +0.0f });	//+J x +I == -K 
		TestCrossProductF({ +0.0f, -2.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, -4.0f, +0.0f });	//-J x -I == -K 
		TestCrossProductF({ +0.0f, +2.0f, +0.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +4.0f, +0.0f });	//+J x -I == +K 
		TestCrossProductF({ +0.0f, -2.0f, +0.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +0.0f, +4.0f, +0.0f });	//-J x +I == +K 

		TestCrossProductF({ +0.0f, +0.0f, +2.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, { -4.0f, +0.0f, +0.0f, +0.0f });	//+K x +J == -I 
		TestCrossProductF({ +0.0f, +0.0f, -2.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, { -4.0f, +0.0f, +0.0f, +0.0f });	//-K x -J == -I 
		TestCrossProductF({ +0.0f, +0.0f, +2.0f, +0.0f }, { +0.0f, -2.0f, +0.0f, +0.0f }, { +4.0f, +0.0f, +0.0f, +0.0f });	//+K x -J == +I 
		TestCrossProductF({ +0.0f, +0.0f, -2.0f, +0.0f }, { +0.0f, +2.0f, +0.0f, +0.0f }, { +4.0f, +0.0f, +0.0f, +0.0f });	//-K x +J == +I 

		TestCrossProductF({ +0.0f, +0.0f, +2.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +4.0f, +0.0f, +0.0f });	//+K x +I == +J 
		TestCrossProductF({ +0.0f, +0.0f, -2.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, +4.0f, +0.0f, +0.0f });	//-K x -I == +J 
		TestCrossProductF({ +0.0f, +0.0f, +2.0f, +0.0f }, { -2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -4.0f, +0.0f, +0.0f });	//+K x -I == -J 
		TestCrossProductF({ +0.0f, +0.0f, -2.0f, +0.0f }, { +2.0f, +0.0f, +0.0f, +0.0f }, { +0.0f, -4.0f, +0.0f, +0.0f });	//-K x +I == -J 


		//Test with random-ish vectors 
		TestCrossProductF({ +5.2f, +3.4f, +2.0f, +0.0f }, { +0.2f, +1.0f, +2.5f, +0.0f }, { +6.5f, -12.6f, +4.52f, +0.0f });
	}

	SECTION("Double vector.", "[CrossVector3D]")
	{
		auto TestCrossProductD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::CrossVector3D(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//Two 0 vectors should have a cross product of 0 
		TestCrossProductD({ +0.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });


		TestCrossProductD({ +1.0, +1.0, +1.0, +1.0 }, { +0.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ -1.0, -1.0, -1.0, -1.0 }, { +0.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, +0.0, +0.0 }, { +1.0, +1.0, +1.0, +1.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, +0.0, +0.0 }, { -1.0, -1.0, -1.0, -1.0 }, { +0.0, +0.0, +0.0, +0.0 });


		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });


		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { +0.0, +0.0, +0.0, +0.0 });


		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 });	//+I x +J == +K 
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 });	//-I x -J == +K 
		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 });	//+I x -J == -K 
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 });	//-I x +J == -K 

		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 });	//+I x +K == -J 
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 });	//-I x -K == -J 
		TestCrossProductD({ +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 });	//+I x -K == +J 
		TestCrossProductD({ -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 });	//-I x +K == +J 

		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 });	//+J x +K == +I 
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 });	//-J x -K == +I 
		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 });	//+J x -K == -I 
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 });	//-J x +K == -I 

		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 });	//+J x +I == -K 
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -1.0, +0.0 });	//-J x -I == -K 
		TestCrossProductD({ +0.0, +1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 });	//+J x -I == +K 
		TestCrossProductD({ +0.0, -1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +1.0, +0.0 });	//-J x +I == +K 

		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 });	//+K x +J == -I 
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 });	//-K x -J == -I 
		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 });	//+K x -J == +I 
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 });	//-K x +J == +I 

		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 });	//+K x +I == +J 
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, +1.0, +0.0, +0.0 });	//-K x -I == +J 
		TestCrossProductD({ +0.0, +0.0, +1.0, +0.0 }, { -1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 });	//+K x -I == -J 
		TestCrossProductD({ +0.0, +0.0, -1.0, +0.0 }, { +1.0, +0.0, +0.0, +0.0 }, { +0.0, -1.0, +0.0, +0.0 });	//-K x +I == -J 


		TestCrossProductD({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, { +0.0, +0.0, +4.0, +0.0 });	//+I x +J == +K 
		TestCrossProductD({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, { +0.0, +0.0, +4.0, +0.0 });	//-I x -J == +K 
		TestCrossProductD({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, { +0.0, +0.0, -4.0, +0.0 });	//+I x -J == -K 
		TestCrossProductD({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, { +0.0, +0.0, -4.0, +0.0 });	//-I x +J == -K 

		TestCrossProductD({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +2.0, +0.0 }, { +0.0, -4.0, +0.0, +0.0 });	//+I x +K == -J 
		TestCrossProductD({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -2.0, +0.0 }, { +0.0, -4.0, +0.0, +0.0 });	//-I x -K == -J 
		TestCrossProductD({ +2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -2.0, +0.0 }, { +0.0, +4.0, +0.0, +0.0 });	//+I x -K == +J 
		TestCrossProductD({ -2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +2.0, +0.0 }, { +0.0, +4.0, +0.0, +0.0 });	//-I x +K == +J 

		TestCrossProductD({ +0.0, +2.0, +0.0, +0.0 }, { +0.0, +0.0, +2.0, +0.0 }, { +4.0, +0.0, +0.0, +0.0 });	//+J x +K == +I 
		TestCrossProductD({ +0.0, -2.0, +0.0, +0.0 }, { +0.0, +0.0, -2.0, +0.0 }, { +4.0, +0.0, +0.0, +0.0 });	//-J x -K == +I 
		TestCrossProductD({ +0.0, +2.0, +0.0, +0.0 }, { +0.0, +0.0, -2.0, +0.0 }, { -4.0, +0.0, +0.0, +0.0 });	//+J x -K == -I 
		TestCrossProductD({ +0.0, -2.0, +0.0, +0.0 }, { +0.0, +0.0, +2.0, +0.0 }, { -4.0, +0.0, +0.0, +0.0 });	//-J x +K == -I 

		TestCrossProductD({ +0.0, +2.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -4.0, +0.0 });	//+J x +I == -K 
		TestCrossProductD({ +0.0, -2.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, -4.0, +0.0 });	//-J x -I == -K 
		TestCrossProductD({ +0.0, +2.0, +0.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +4.0, +0.0 });	//+J x -I == +K 
		TestCrossProductD({ +0.0, -2.0, +0.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, { +0.0, +0.0, +4.0, +0.0 });	//-J x +I == +K 

		TestCrossProductD({ +0.0, +0.0, +2.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, { -4.0, +0.0, +0.0, +0.0 });	//+K x +J == -I 
		TestCrossProductD({ +0.0, +0.0, -2.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, { -4.0, +0.0, +0.0, +0.0 });	//-K x -J == -I 
		TestCrossProductD({ +0.0, +0.0, +2.0, +0.0 }, { +0.0, -2.0, +0.0, +0.0 }, { +4.0, +0.0, +0.0, +0.0 });	//+K x -J == +I 
		TestCrossProductD({ +0.0, +0.0, -2.0, +0.0 }, { +0.0, +2.0, +0.0, +0.0 }, { +4.0, +0.0, +0.0, +0.0 });	//-K x +J == +I 

		TestCrossProductD({ +0.0, +0.0, +2.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, { +0.0, +4.0, +0.0, +0.0 });	//+K x +I == +J 
		TestCrossProductD({ +0.0, +0.0, -2.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, { +0.0, +4.0, +0.0, +0.0 });	//-K x -I == +J 
		TestCrossProductD({ +0.0, +0.0, +2.0, +0.0 }, { -2.0, +0.0, +0.0, +0.0 }, { +0.0, -4.0, +0.0, +0.0 });	//+K x -I == -J 
		TestCrossProductD({ +0.0, +0.0, -2.0, +0.0 }, { +2.0, +0.0, +0.0, +0.0 }, { +0.0, -4.0, +0.0, +0.0 });	//-K x +I == -J 


		//Test with random-ish vectors 
		TestCrossProductD({ +5.2, +3.4, +2.0, +0.0 }, { +0.2, +1.0, +2.5, +0.0 }, { +6.5, -12.6, +4.52, +0.0 });
	}
}

TEST_CASE("Multiply a vector by matrix.", "[VectorXMatrixF], [VectorXMatrixD], [Math]")
{
	SECTION("Float vector.", "[VectorXMatrixF]")
	{
		auto TestVectorXMatrixF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::VectorXMatrixF(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//multiplying by a 0 matrix should give a 0 vector 
		TestVectorXMatrixF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{ +0.0f, +0.0f, +0.0f, +0.0f });


		//multiplying by the identity matrix shouldn't change anything 
		TestVectorXMatrixF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +1.0f, +2.0f, +3.0f, +4.0f });

		TestVectorXMatrixF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -1.0f, -2.0f, -3.0f, -4.0f });

		//multiplying by a negative identity matrix should flip the sign of each component of the vector 
		TestVectorXMatrixF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, -1.0f
			},
			{ -1.0f, -2.0f, -3.0f, -4.0f });

		TestVectorXMatrixF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, -1.0f
			},
			{ +1.0f, +2.0f, +3.0f, +4.0f });




		//test with random-ish values 
		TestVectorXMatrixF(
			{ +5.2f, +3.4f, +2.0f, +1.0f },
			{
				+0.2f, +1.0f, +2.5f, +0.4f,
				+0.7f, +5.9f, +2.2f, +4.1f,
				+3.8f, +0.0f, +8.5f, +12.4f,
				+7.6f, +5.5f, +9.8f, +1.0f
			},
			{ +18.62f, +30.76f, +47.28f, +41.82f });
	}

	SECTION("Double vector.", "[VectorXMatrixD]")
	{
		auto TestVectorXMatrixD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GMATRIXD _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::VectorXMatrixD(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};


		//multiplying by a 0 matrix should give a 0 vector 
		TestVectorXMatrixD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			},
			{ +0.0, +0.0, +0.0, +0.0 });


		//multiplying by the identity matrix shouldn't change anything 
		TestVectorXMatrixD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +1.0, +2.0, +3.0, +4.0 });

		TestVectorXMatrixD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -1.0, -2.0, -3.0, -4.0 });

		//multiplying by a negative identity matrix should flip the sign of each component of the vector 
		TestVectorXMatrixD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				-1.0, +0.0, +0.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, -1.0
			},
			{ -1.0, -2.0, -3.0, -4.0 });

		TestVectorXMatrixD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				-1.0, +0.0, +0.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, -1.0
			},
			{ +1.0, +2.0, +3.0, +4.0 });



		TestVectorXMatrixD(
			{ +5.2, +3.4, +2.0, +1.0 },
			{
				+0.2, +1.0, +2.5, +0.4,
				+0.7, +5.9, +2.2, +4.1,
				+3.8, +0.0, +8.5, +12.4,
				+7.6, +5.5, +9.8, +1.0
			},
			{ +18.62, +30.76, +47.28, +41.82 });
	}
}

TEST_CASE("Transform a vector by matrix.", "[TransformF], [TransformD], [Math]")
{
	SECTION("Float vector.", "[TransformF]")
	{
		auto TestTransformF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GMATRIXF _b, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::TransformF(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};


		//Transforming by a 0 matrix should 0 out the XYZ components of the vector 
		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{ +0.0f, +0.0f, +0.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +0.0f
			},
			{ +0.0f, +0.0f, +0.0f, -4.0f });

		//Transforming by the identity matrix shouldn't do anything 
		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +1.0f, +2.0f, +3.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -1.0f, -2.0f, -3.0f, -4.0f });

		//Transforming by a negative identity matrix should flip the signs ofthe XYZ components of the vector 
		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, -1.0f
			},
			{ -1.0f, -2.0f, -3.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +1.0f, +2.0f, +3.0f, -4.0f });

		//Misc tests 
		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +3.0f, +2.0f, +1.0f, +4.0f });


		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -3.0f, -2.0f, -1.0f, -4.0f });

		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -3.0f, -2.0f, -1.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, -1.0f, +0.0f, +0.0f,
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +3.0f, +2.0f, +1.0f, -4.0f });


		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, +1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +2.0f, +1.0f, +3.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+0.0f, +1.0f, +0.0f, +0.0f,
				+1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, +1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -2.0f, -1.0f, -3.0f, -4.0f });

		TestTransformF(
			{ +1.0f, +2.0f, +3.0f, +4.0f },
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ -2.0f, -1.0f, -3.0f, +4.0f });

		TestTransformF(
			{ -1.0f, -2.0f, -3.0f, -4.0f },
			{
				+0.0f, -1.0f, +0.0f, +0.0f,
				-1.0f, +0.0f, +0.0f, +0.0f,
				+0.0f, +0.0f, -1.0f, +0.0f,
				+0.0f, +0.0f, +0.0f, +1.0f
			},
			{ +2.0f, +1.0f, +3.0f, -4.0f });
	}

	SECTION("Double vector.", "[TransformD]")
	{
		auto TestTransformD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GMATRIXD _b, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::TransformD(_a, _b, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//Transforming by a 0 matrix should 0 out the XYZ components of the vector 
		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			},
			{ +0.0, +0.0, +0.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +0.0
			},
			{ +0.0, +0.0, +0.0, -4.0 });

		//Transforming by the identity matrix shouldn't do anything 
		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +1.0, +2.0, +3.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -1.0, -2.0, -3.0, -4.0 });

		//Transforming by a negative identity matrix should flip the signs ofthe XYZ components of the vector 
		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				-1.0, +0.0, +0.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, -1.0
			},
			{ -1.0, -2.0, -3.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				-1.0, +0.0, +0.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +1.0, +2.0, +3.0, -4.0 });

		//Misc tests 
		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +3.0, +2.0, +1.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -3.0, -2.0, -1.0, -4.0 });

		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, +0.0, -1.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				-1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -3.0, -2.0, -1.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+0.0, +0.0, -1.0, +0.0,
				+0.0, -1.0, +0.0, +0.0,
				-1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +3.0, +2.0, +1.0, -4.0 });

		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, +1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +2.0, +1.0, +3.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+0.0, +1.0, +0.0, +0.0,
				+1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, +1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -2.0, -1.0, -3.0, -4.0 });

		TestTransformD(
			{ +1.0, +2.0, +3.0, +4.0 },
			{
				+0.0, -1.0, +0.0, +0.0,
				-1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ -2.0, -1.0, -3.0, +4.0 });

		TestTransformD(
			{ -1.0, -2.0, -3.0, -4.0 },
			{
				+0.0, -1.0, +0.0, +0.0,
				-1.0, +0.0, +0.0, +0.0,
				+0.0, +0.0, -1.0, +0.0,
				+0.0, +0.0, +0.0, +1.0
			},
			{ +2.0, +1.0, +3.0, -4.0 });
	}
}

TEST_CASE("Calculate the magnitude of the vector.", "[MagnitudeF], [MagnitudeD], [Math]")
{
	SECTION("Float vector.", "[MagnitudeF]")
	{
		auto TestMagnitudeF = [](const GW::MATH::GVECTORF _a, const float _expectedResult)
		{
			float result;
			CHECK(+(GW::MATH::GVector::MagnitudeF(_a, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_F(result, _expectedResult));
		};

		//this is to avoid a million calls to the sqrt function 
		float sqrtOf2 = sqrtf(2.0f);
		float sqrtOf3 = sqrtf(3.0f);
		float sqrtOf4 = sqrtf(4.0f);
		//this function basically assembles every homogenous vector that could possibly have a given magnitude 
		//A homogenous vector in this case is one where the absolute value of each component is either 0 or equal to the absolute value all other components. 
		auto TestAllHomogenousVectorsWithGivenMagnitudeF = [&](const float _intendedMagnitude)
		{
			assert(_intendedMagnitude >= 0 && "Magnitudes cannot be less than 0! Trying to make a vector with a magnitude less than 0 is impossible! Shame on you!");

			//homogenous vectors with one component 
			TestMagnitudeF({ +_intendedMagnitude, 0, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, +_intendedMagnitude, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, 0, +_intendedMagnitude }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude, 0, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, -_intendedMagnitude, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, 0, -_intendedMagnitude }, +_intendedMagnitude);

			//homogenous vectors with two components 
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, 0, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, 0, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf2, 0, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf2, 0, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, 0, +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);

			//homogenous vectors with 3 components 
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeF({ 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);

			//homogenous vectors with 4 components
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeF({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
		};

		//0 vector should have 0 magnitude
		TestMagnitudeF({ +0.0f, +0.0f, +0.0f, +0.0f }, +0.0f);

		//Test a bunch of homogenous vectors 
		for (float curMagnitude = 1.0f; curMagnitude < 10.0f; curMagnitude += 0.1f)
		{
			TestAllHomogenousVectorsWithGivenMagnitudeF(curMagnitude);
		}

		//Test with random-ish values 
		TestMagnitudeF({ +5.2f, +3.4f, +2.0f, +1.0f }, +6.60302960f);
	}

	SECTION("Double vector.", "[MagnitudeD]")
	{
		auto TestMagnitudeD = [](const GW::MATH::GVECTORD _a, const double _expectedResult)
		{
			double result;
			CHECK(+(GW::MATH::GVector::MagnitudeD(_a, result)));
			CHECK(G_WITHIN_STANDARD_DEVIATION_D(result, _expectedResult));
		};

		//this is to avoid a million calls to the sqrt function 
		double sqrtOf2 = sqrt(2.0);
		double sqrtOf3 = sqrt(3.0);
		double sqrtOf4 = sqrt(4.0);
		//this function basically assembles every homogenous vector that could possibly have a given magnitude 
		//A homogenous vector in this case is one where the absolute value of each component is either 0 or equal to the absolute value all other components. 
		auto TestAllHomogenousVectorsWithGivenMagnitudeD = [&](const double _intendedMagnitude)
		{
			//homogenous vectors with one component 
			assert(_intendedMagnitude >= 0 && "Magnitudes cannot be less than 0! Trying to make a vector with a magnitude less than 0 is impossible! Shame on you!");
			TestMagnitudeD({ +_intendedMagnitude, 0, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, +_intendedMagnitude, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, 0, +_intendedMagnitude }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude, 0, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, -_intendedMagnitude, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, 0, -_intendedMagnitude }, +_intendedMagnitude);

			//homogenous vectors with two components 
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, 0, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, 0, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf2, 0, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf2, 0, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf2, 0, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf2, 0, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, +_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, -_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, -_intendedMagnitude / sqrtOf2, +_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, 0, +_intendedMagnitude / sqrtOf2, -_intendedMagnitude / sqrtOf2 }, +_intendedMagnitude);

			//homogenous vectors with 3 components 
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf3, 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, +_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);
			TestMagnitudeD({ 0, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3, -_intendedMagnitude / sqrtOf3 }, +_intendedMagnitude);

			//homogenous vectors with 4 components
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, +_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
			TestMagnitudeD({ -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4, -_intendedMagnitude / sqrtOf4 }, +_intendedMagnitude);
		};

		//0 vector should have 0 magnitude
		TestMagnitudeD({ +0.0f, +0.0f, +0.0f, +0.0f }, +0.0f);

		//Test a bunch of homogenous vectors 
		for (double curMagnitude = 1.0; curMagnitude < 10.0; curMagnitude += 0.1)
		{
			TestAllHomogenousVectorsWithGivenMagnitudeD(curMagnitude);
		}

		//Test with random-ish values
		TestMagnitudeD({ +5.2, +3.4, +2.0, +1.0 }, +6.60302960768767112);
	}
}

TEST_CASE("Normalize the vector.", "[NormalizeF], [NormalizeD], [Math]")
{
	SECTION("Float vector.", "[NormalizeF]")
	{
		//Fail cases
		GW::MATH::GVECTORF
			v0F = { 0.0f, 0.0f, 0.0f, 0.0f },
			resultF;
		//Normalize should fail and output a 0 vector if given a 0 vector. 
		CHECK(-(GW::MATH::GVector::NormalizeF(v0F, resultF)));
		COMPARE_VECTORS_STANDARD_F(resultF, v0F);

		//Pass cases
		auto TestNormalizeF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::NormalizeF(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//these are here to avoid a million calls to the sqrt function 
		float sqrtOf2 = sqrtf(2.0f);
		float sqrtOf3 = sqrtf(3.0f);
		float sqrtOf4 = sqrtf(4.0f);

		//This function makes all possible homogenous vectors with components that have the given value and tests that they are normalized correctly 
		//A homogenous vector in this case is one where the absolute value of each component is either 0 or equal to the absolute value all other components. 
		auto TestAllHomogenousVectorsF = [&](const float _homogenousValue)
		{
			assert(_homogenousValue != 0 && "A 0 vector is considered a fail case for GVector::Normalize!");
			float signOfInput = (1.0f * (_homogenousValue > 0)) + (-1.0f * (_homogenousValue < 0)); //1 if _homogenousValue is positive, -1 if it is negative 

			//homogenous vectors with one component
			TestNormalizeF({ +_homogenousValue, 0, 0, 0 }, { +signOfInput, 0, 0, 0 });
			TestNormalizeF({ 0, +_homogenousValue, 0, 0 }, { 0, +signOfInput, 0, 0 });
			TestNormalizeF({ 0, 0, +_homogenousValue, 0 }, { 0, 0, +signOfInput, 0 });
			TestNormalizeF({ 0, 0, 0, +_homogenousValue }, { 0, 0, 0, +signOfInput });
			TestNormalizeF({ -_homogenousValue, 0, 0, 0 }, { -signOfInput, 0, 0, 0 });
			TestNormalizeF({ 0, -_homogenousValue, 0, 0 }, { 0, -signOfInput, 0, 0 });
			TestNormalizeF({ 0, 0, -_homogenousValue, 0 }, { 0, 0, -signOfInput, 0 });
			TestNormalizeF({ 0, 0, 0, -_homogenousValue }, { 0, 0, 0, -signOfInput });

			//homogenous vectors with two components 
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, 0, 0 }, { +signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, 0, 0 }, { -signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, 0, 0 }, { +signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, 0, 0 }, { -signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeF({ +_homogenousValue, 0, +_homogenousValue, 0 }, { +signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ -_homogenousValue, 0, -_homogenousValue, 0 }, { -signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ +_homogenousValue, 0, -_homogenousValue, 0 }, { +signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ -_homogenousValue, 0, +_homogenousValue, 0 }, { -signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ +_homogenousValue, 0, 0, +_homogenousValue }, { +signOfInput / sqrtOf2, 0, 0, +signOfInput / sqrtOf2 });
			TestNormalizeF({ -_homogenousValue, 0, 0, -_homogenousValue }, { -signOfInput / sqrtOf2, 0, 0, -signOfInput / sqrtOf2 });
			TestNormalizeF({ +_homogenousValue, 0, 0, -_homogenousValue }, { +signOfInput / sqrtOf2, 0, 0, -signOfInput / sqrtOf2 });
			TestNormalizeF({ -_homogenousValue, 0, 0, +_homogenousValue }, { -signOfInput / sqrtOf2, 0, 0, +signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, +_homogenousValue, +_homogenousValue, 0 }, { 0, +signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ 0, -_homogenousValue, -_homogenousValue, 0 }, { 0, -signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ 0, +_homogenousValue, -_homogenousValue, 0 }, { 0, +signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ 0, -_homogenousValue, +_homogenousValue, 0 }, { 0, -signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0 });
			TestNormalizeF({ 0, +_homogenousValue, 0, +_homogenousValue }, { 0, +signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, -_homogenousValue, 0, -_homogenousValue }, { 0, -signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, +_homogenousValue, 0, -_homogenousValue }, { 0, +signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, -_homogenousValue, 0, +_homogenousValue }, { 0, -signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, 0, +_homogenousValue, +_homogenousValue }, { 0, 0, +signOfInput / sqrtOf2, +signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, 0, -_homogenousValue, -_homogenousValue }, { 0, 0, -signOfInput / sqrtOf2, -signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, 0, +_homogenousValue, -_homogenousValue }, { 0, 0, +signOfInput / sqrtOf2, -signOfInput / sqrtOf2 });
			TestNormalizeF({ 0, 0, -_homogenousValue, +_homogenousValue }, { 0, 0, -signOfInput / sqrtOf2, +signOfInput / sqrtOf2 });

			//homogenous vectors with three components 
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, +_homogenousValue, 0 }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, -_homogenousValue, 0 }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, +_homogenousValue, 0 }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, -_homogenousValue, 0 }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, +_homogenousValue, 0 }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, -_homogenousValue, 0 }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, +_homogenousValue, 0 }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, -_homogenousValue, 0 }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, 0, +_homogenousValue }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, 0, -_homogenousValue }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, 0, +_homogenousValue }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, 0, -_homogenousValue }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, 0, +_homogenousValue }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, 0, -_homogenousValue }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, 0, +_homogenousValue }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, 0, -_homogenousValue }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, 0, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, 0, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, 0, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ +_homogenousValue, 0, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, 0, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, 0, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, 0, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ -_homogenousValue, 0, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeF({ 0, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });

			//Homogenous vectors with four components
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ +_homogenousValue, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeF({ -_homogenousValue, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
		};


		//test a bunch of positive homogenous vectors 
		for (float homogenousValue = 1.0f; homogenousValue < 10.0f; homogenousValue += 0.1f)
		{
			TestAllHomogenousVectorsF(homogenousValue);
		}

		//test a bunch of negative homogenous vectors 
		for (float homogenousValue = -1.0f; homogenousValue > -10.0f; homogenousValue -= 0.1f)
		{
			TestAllHomogenousVectorsF(homogenousValue);
		}

		//Test with random-ish values 
		TestNormalizeF({ +5.2f, +3.4f, +2.0f, +1.0f }, { +0.7875172f, +0.5149151f, +0.3028912f, +0.1514456f });

	}

	SECTION("Double vector.", "[NormalizeD]")
	{

		//Fail cases
		GW::MATH::GVECTORD
			v0D = { 0.0, 0.0, 0.0, 0.0 },
			resultD;
		//Normalize should fail and output a 0 vector if given a 0 vector. 
		CHECK(-(GW::MATH::GVector::NormalizeD(v0D, resultD)));
		COMPARE_VECTORS_STANDARD_D(resultD, v0D);

		//Pass cases
		auto TestNormalizeD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::NormalizeD(_a, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//these are here to avoid a million calls to the sqrt function 
		double sqrtOf2 = sqrt(2.0);
		double sqrtOf3 = sqrt(3.0);
		double sqrtOf4 = sqrt(4.0);

		//This function makes all possible homogenous vectors with components that have the given value and tests that they are normalized correctly 
		//A homogenous vector in this case is one where the absolute value of each component is either 0 or equal to the absolute value all other components. 
		auto TestAllHomogenousVectorsD = [&](const double _homogenousValue)
		{
			assert(_homogenousValue != 0 && "A 0 vector is considered a fail case for GVector::Normalize!");
			float signOfInput = (1.0f * (_homogenousValue > 0)) + (-1.0f * (_homogenousValue < 0)); //1 if _homogenousValue is positive, -1 if it is negative 

			//homogenous vectors with one component
			TestNormalizeD({ +_homogenousValue, 0, 0, 0 }, { +signOfInput, 0, 0, 0 });
			TestNormalizeD({ 0, +_homogenousValue, 0, 0 }, { 0, +signOfInput, 0, 0 });
			TestNormalizeD({ 0, 0, +_homogenousValue, 0 }, { 0, 0, +signOfInput, 0 });
			TestNormalizeD({ 0, 0, 0, +_homogenousValue }, { 0, 0, 0, +signOfInput });
			TestNormalizeD({ -_homogenousValue, 0, 0, 0 }, { -signOfInput, 0, 0, 0 });
			TestNormalizeD({ 0, -_homogenousValue, 0, 0 }, { 0, -signOfInput, 0, 0 });
			TestNormalizeD({ 0, 0, -_homogenousValue, 0 }, { 0, 0, -signOfInput, 0 });
			TestNormalizeD({ 0, 0, 0, -_homogenousValue }, { 0, 0, 0, -signOfInput });

			//homogenous vectors with two components 
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, 0, 0 }, { +signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, 0, 0 }, { -signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, 0, 0 }, { +signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, 0, 0 }, { -signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0, 0 });
			TestNormalizeD({ +_homogenousValue, 0, +_homogenousValue, 0 }, { +signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ -_homogenousValue, 0, -_homogenousValue, 0 }, { -signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ +_homogenousValue, 0, -_homogenousValue, 0 }, { +signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ -_homogenousValue, 0, +_homogenousValue, 0 }, { -signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ +_homogenousValue, 0, 0, +_homogenousValue }, { +signOfInput / sqrtOf2, 0, 0, +signOfInput / sqrtOf2 });
			TestNormalizeD({ -_homogenousValue, 0, 0, -_homogenousValue }, { -signOfInput / sqrtOf2, 0, 0, -signOfInput / sqrtOf2 });
			TestNormalizeD({ +_homogenousValue, 0, 0, -_homogenousValue }, { +signOfInput / sqrtOf2, 0, 0, -signOfInput / sqrtOf2 });
			TestNormalizeD({ -_homogenousValue, 0, 0, +_homogenousValue }, { -signOfInput / sqrtOf2, 0, 0, +signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, +_homogenousValue, +_homogenousValue, 0 }, { 0, +signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ 0, -_homogenousValue, -_homogenousValue, 0 }, { 0, -signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ 0, +_homogenousValue, -_homogenousValue, 0 }, { 0, +signOfInput / sqrtOf2, -signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ 0, -_homogenousValue, +_homogenousValue, 0 }, { 0, -signOfInput / sqrtOf2, +signOfInput / sqrtOf2, 0 });
			TestNormalizeD({ 0, +_homogenousValue, 0, +_homogenousValue }, { 0, +signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, -_homogenousValue, 0, -_homogenousValue }, { 0, -signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, +_homogenousValue, 0, -_homogenousValue }, { 0, +signOfInput / sqrtOf2, 0, -signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, -_homogenousValue, 0, +_homogenousValue }, { 0, -signOfInput / sqrtOf2, 0, +signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, 0, +_homogenousValue, +_homogenousValue }, { 0, 0, +signOfInput / sqrtOf2, +signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, 0, -_homogenousValue, -_homogenousValue }, { 0, 0, -signOfInput / sqrtOf2, -signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, 0, +_homogenousValue, -_homogenousValue }, { 0, 0, +signOfInput / sqrtOf2, -signOfInput / sqrtOf2 });
			TestNormalizeD({ 0, 0, -_homogenousValue, +_homogenousValue }, { 0, 0, -signOfInput / sqrtOf2, +signOfInput / sqrtOf2 });

			//homogenous vectors with three components 
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, +_homogenousValue, 0 }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, -_homogenousValue, 0 }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, +_homogenousValue, 0 }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, -_homogenousValue, 0 }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, +_homogenousValue, 0 }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, -_homogenousValue, 0 }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, +_homogenousValue, 0 }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, -_homogenousValue, 0 }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, 0, +_homogenousValue }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, 0, -_homogenousValue }, { +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, 0, +_homogenousValue }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, 0, -_homogenousValue }, { +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, 0, +_homogenousValue }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, 0, -_homogenousValue }, { -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, 0, +_homogenousValue }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, 0, -_homogenousValue }, { -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, 0, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, 0, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, 0, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ +_homogenousValue, 0, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, 0, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, 0, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf3, 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, 0, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ -_homogenousValue, 0, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf3, 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { 0, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { 0, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { 0, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, +signOfInput / sqrtOf3 });
			TestNormalizeD({ 0, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { 0, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3, -signOfInput / sqrtOf3 });

			//Homogenous vectors with four components
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ +_homogenousValue, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { +signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, +_homogenousValue, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  +signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, +_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, +_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, +signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, -_homogenousValue, +_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, +signOfInput / sqrtOf4 });
			TestNormalizeD({ -_homogenousValue, -_homogenousValue, -_homogenousValue, -_homogenousValue }, { -signOfInput / sqrtOf4,  -signOfInput / sqrtOf4, -signOfInput / sqrtOf4, -signOfInput / sqrtOf4 });
		};

		//test a bunch of positive homogenous vectors 
		for (double homogenousValue = 1.0; homogenousValue < 10.0; homogenousValue += 0.1)
		{
			TestAllHomogenousVectorsD(homogenousValue);
		}

		//test a bunch of negative homogenous vectors 
		for (double homogenousValue = -1.0; homogenousValue > -10.0; homogenousValue -= 0.1)
		{
			TestAllHomogenousVectorsD(homogenousValue);
		}


		//Test with random-ish values 
		TestNormalizeD({ +5.2, +3.4, +2.0, +1.0 }, { +0.7875172926599974, +0.5149151528930752, +0.3028912664076913, +0.1514456332038456 });
	}
}

TEST_CASE("Lerp between two vectors.", "[LerpF], [LerpD], [Math]")
{
	SECTION("Float vector.", "[LerpF]")
	{
		auto TestLerpF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const float _lerpAmount, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::LerpF(_a, _b, _lerpAmount, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		//-100% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -1.0f, { +0.0f, +0.0f, +0.00f, +0.00f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -1.0f, { +0.0f, +0.0f, +0.00f, +0.00f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -1.0f, { +4.0f, +8.0f, +12.0f, +16.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -1.0f, { -4.0f, -8.0f, -12.0f, -16.0f });

		//-90% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.9f, { +0.1f, +0.2f, +0.30f, +0.40f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.9f, { -0.1f, -0.2f, -0.30f, -0.40f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.9f, { +3.7f, +7.4f, +11.1f, +14.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.9f, { -3.7f, -7.4f, -11.1f, -14.8f });

		//-80% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.8f, { +0.2f, +0.4f, +0.60f, +0.80f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.8f, { -0.2f, -0.4f, -0.60f, -0.80f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.8f, { +3.4f, +6.8f, +10.2f, +13.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.8f, { -3.4f, -6.8f, -10.2f, -13.6f });

		//-70% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.7f, { +0.3f, +0.6f, +0.9f, +1.20f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.7f, { -0.3f, -0.6f, -0.9f, -1.20f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.7f, { +3.1f, +6.2f, +9.3f, +12.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.7f, { -3.1f, -6.2f, -9.3f, -12.4f });

		//-60% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.6f, { +0.4f, +0.8f, +1.2f, +1.60f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.6f, { -0.4f, -0.8f, -1.2f, -1.60f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.6f, { +2.8f, +5.6f, +8.4f, +11.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.6f, { -2.8f, -5.6f, -8.4f, -11.2f });

		//-50% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.5f, { +0.5f, +1.0f, +1.5f, +2.00f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.5f, { -0.5f, -1.0f, -1.5f, -2.00f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.5f, { +2.5f, +5.0f, +7.5f, +10.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.5f, { -2.5f, -5.0f, -7.5f, -10.0f });

		//-40% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.4f, { +0.6f, +1.2f, +1.8f, +2.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.4f, { -0.6f, -1.2f, -1.8f, -2.4f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.4f, { +2.2f, +4.4f, +6.6f, +8.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.4f, { -2.2f, -4.4f, -6.6f, -8.8f });

		//-30% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.3f, { +0.7f, +1.4f, +2.1f, +2.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.3f, { -0.7f, -1.4f, -2.1f, -2.8f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.3f, { +1.9f, +3.8f, +5.7f, +7.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.3f, { -1.9f, -3.8f, -5.7f, -7.6f });

		//-20% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.2f, { +0.8f, +1.6f, +2.4f, +3.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.2f, { -0.8f, -1.6f, -2.4f, -3.2f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.2f, { +1.6f, +3.2f, +4.8f, +6.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.2f, { -1.6f, -3.2f, -4.8f, -6.4f });

		//-10% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.1f, { +0.9f, +1.8f, +2.7f, +3.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.1f, { -0.9f, -1.8f, -2.7f, -3.6f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, -0.1f, { +1.3f, +2.6f, +3.9f, +5.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, -0.1f, { -1.3f, -2.6f, -3.9f, -5.2f });

		//0% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.0f, { +1.0f, +2.0f, +3.0f, +4.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.0f, { -1.0f, -2.0f, -3.0f, -4.0f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.0f, { +1.0f, +2.0f, +3.0f, +4.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.0f, { -1.0f, -2.0f, -3.0f, -4.0f });

		//10% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.1f, { +1.1f, +2.2f, +3.3f, +4.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.1f, { -1.1f, -2.2f, -3.3f, -4.4f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.1f, { +0.7f, +1.4f, +2.1f, +2.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.1f, { -0.7f, -1.4f, -2.1f, -2.8f });

		//20% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.2f, { +1.2f, +2.4f, +3.6f, +4.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.2f, { -1.2f, -2.4f, -3.6f, -4.8f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.2f, { +0.4f, +0.8f, +1.2f, +1.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.2f, { -0.4f, -0.8f, -1.2f, -1.6f });

		//30% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.3f, { +1.3f, +2.6f, +3.9f, +5.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.3f, { -1.3f, -2.6f, -3.9f, -5.2f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.3f, { +0.1f, +0.2f, +0.3f, +0.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.3f, { -0.1f, -0.2f, -0.3f, -0.4f });

		//40% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.4f, { +1.4f, +2.8f, +4.2f, +5.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.4f, { -1.4f, -2.8f, -4.2f, -5.6f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.4f, { -0.2f, -0.4f, -0.6f, -0.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.4f, { +0.2f, +0.4f, +0.6f, +0.8f });

		//50% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.5f, { +1.5f, +3.0f, +4.5f, +6.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.5f, { -1.5f, -3.0f, -4.5f, -6.0f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.5f, { -0.5f, -1.0f, -1.5f, -2.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.5f, { +0.5f, +1.0f, +1.5f, +2.0f });

		//60% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.6f, { +1.6f, +3.2f, +4.8f, +6.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.6f, { -1.6f, -3.2f, -4.8f, -6.4f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.6f, { -0.8f, -1.6f, -2.4f, -3.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.6f, { +0.8f, +1.6f, +2.4f, +3.2f });

		//70% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.7f, { +1.7f, +3.4f, +5.1f, +6.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.7f, { -1.7f, -3.4f, -5.1f, -6.8f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.7f, { -1.1f, -2.2f, -3.3f, -4.4f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.7f, { +1.1f, +2.2f, +3.3f, +4.4f });

		//80% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.8f, { +1.8f, +3.6f, +5.4f, +7.2f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.8f, { -1.8f, -3.6f, -5.4f, -7.2f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.8f, { -1.4f, -2.8f, -4.2f, -5.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.8f, { +1.4f, +2.8f, +4.2f, +5.6f });

		//90% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.9f, { +1.9f, +3.8f, +5.7f, +7.6f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.9f, { -1.9f, -3.8f, -5.7f, -7.6f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +0.9f, { -1.7f, -3.4f, -5.1f, -6.8f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +0.9f, { +1.7f, +3.4f, +5.1f, +6.8f });

		//100% lerp 
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +1.0f, { +2.0f, +4.0f, +6.0f, +8.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +1.0f, { -2.0f, -4.0f, -6.0f, -8.0f });
		TestLerpF({ +1.0f, +2.0f, +3.0f, +4.0f }, { -2.0f, -4.0f, -6.0f, -8.0f }, +1.0f, { -2.0f, -4.0f, -6.0f, -8.0f });
		TestLerpF({ -1.0f, -2.0f, -3.0f, -4.0f }, { +2.0f, +4.0f, +6.0f, +8.0f }, +1.0f, { +2.0f, +4.0f, +6.0f, +8.0f });



		//test with random-ish values 
		TestLerpF({ +5.2f, +3.4f, +2.0f, +1.0f }, { +0.2f, +1.0f, +2.5f, +0.4f }, +0.5f, { +2.7f, +2.2f, +2.25f, +0.7f });
	}

	SECTION("Double vector.", "[LerpD]")
	{
		auto TestLerpD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const double _lerpAmount, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::LerpD(_a, _b, _lerpAmount, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		//-100% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -1.0, { +0.0, +0.0, +0.00, +0.00 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -1.0, { +0.0, +0.0, +0.00, +0.00 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -1.0, { +4.0, +8.0, +12.0, +16.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -1.0, { -4.0, -8.0, -12.0, -16.0 });

		//-90% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.9, { +0.1, +0.2, +0.30, +0.40 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.9, { -0.1, -0.2, -0.30, -0.40 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.9, { +3.7, +7.4, +11.1, +14.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.9, { -3.7, -7.4, -11.1, -14.8 });

		//-80% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.8, { +0.2, +0.4, +0.60, +0.80 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.8, { -0.2, -0.4, -0.60, -0.80 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.8, { +3.4, +6.8, +10.2, +13.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.8, { -3.4, -6.8, -10.2, -13.6 });

		//-70% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.7, { +0.3, +0.6, +0.9, +1.20 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.7, { -0.3, -0.6, -0.9, -1.20 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.7, { +3.1, +6.2, +9.3, +12.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.7, { -3.1, -6.2, -9.3, -12.4 });

		//-60% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.6, { +0.4, +0.8, +1.2, +1.60 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.6, { -0.4, -0.8, -1.2, -1.60 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.6, { +2.8, +5.6, +8.4, +11.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.6, { -2.8, -5.6, -8.4, -11.2 });

		//-50% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.5, { +0.5, +1.0, +1.5, +2.00 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.5, { -0.5, -1.0, -1.5, -2.00 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.5, { +2.5, +5.0, +7.5, +10.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.5, { -2.5, -5.0, -7.5, -10.0 });

		//-40% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.4, { +0.6, +1.2, +1.8, +2.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.4, { -0.6, -1.2, -1.8, -2.4 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.4, { +2.2, +4.4, +6.6, +8.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.4, { -2.2, -4.4, -6.6, -8.8 });

		//-30% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.3, { +0.7, +1.4, +2.1, +2.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.3, { -0.7, -1.4, -2.1, -2.8 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.3, { +1.9, +3.8, +5.7, +7.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.3, { -1.9, -3.8, -5.7, -7.6 });

		//-20% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.2, { +0.8, +1.6, +2.4, +3.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.2, { -0.8, -1.6, -2.4, -3.2 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.2, { +1.6, +3.2, +4.8, +6.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.2, { -1.6, -3.2, -4.8, -6.4 });

		//-10% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.1, { +0.9, +1.8, +2.7, +3.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.1, { -0.9, -1.8, -2.7, -3.6 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, -0.1, { +1.3, +2.6, +3.9, +5.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, -0.1, { -1.3, -2.6, -3.9, -5.2 });

		//0% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.0, { +1.0, +2.0, +3.0, +4.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.0, { -1.0, -2.0, -3.0, -4.0 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.0, { +1.0, +2.0, +3.0, +4.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.0, { -1.0, -2.0, -3.0, -4.0 });

		//10% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.1, { +1.1, +2.2, +3.3, +4.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.1, { -1.1, -2.2, -3.3, -4.4 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.1, { +0.7, +1.4, +2.1, +2.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.1, { -0.7, -1.4, -2.1, -2.8 });

		//20% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.2, { +1.2, +2.4, +3.6, +4.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.2, { -1.2, -2.4, -3.6, -4.8 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.2, { +0.4, +0.8, +1.2, +1.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.2, { -0.4, -0.8, -1.2, -1.6 });

		//30% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.3, { +1.3, +2.6, +3.9, +5.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.3, { -1.3, -2.6, -3.9, -5.2 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.3, { +0.1, +0.2, +0.3, +0.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.3, { -0.1, -0.2, -0.3, -0.4 });

		//40% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.4, { +1.4, +2.8, +4.2, +5.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.4, { -1.4, -2.8, -4.2, -5.6 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.4, { -0.2, -0.4, -0.6, -0.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.4, { +0.2, +0.4, +0.6, +0.8 });

		//50% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.5, { +1.5, +3.0, +4.5, +6.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.5, { -1.5, -3.0, -4.5, -6.0 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.5, { -0.5, -1.0, -1.5, -2.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.5, { +0.5, +1.0, +1.5, +2.0 });

		//60% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.6, { +1.6, +3.2, +4.8, +6.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.6, { -1.6, -3.2, -4.8, -6.4 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.6, { -0.8, -1.6, -2.4, -3.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.6, { +0.8, +1.6, +2.4, +3.2 });

		//70% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.7, { +1.7, +3.4, +5.1, +6.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.7, { -1.7, -3.4, -5.1, -6.8 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.7, { -1.1, -2.2, -3.3, -4.4 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.7, { +1.1, +2.2, +3.3, +4.4 });

		//80% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.8, { +1.8, +3.6, +5.4, +7.2 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.8, { -1.8, -3.6, -5.4, -7.2 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.8, { -1.4, -2.8, -4.2, -5.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.8, { +1.4, +2.8, +4.2, +5.6 });

		//90% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.9, { +1.9, +3.8, +5.7, +7.6 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.9, { -1.9, -3.8, -5.7, -7.6 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +0.9, { -1.7, -3.4, -5.1, -6.8 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +0.9, { +1.7, +3.4, +5.1, +6.8 });

		//100% lerp 
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +1.0, { +2.0, +4.0, +6.0, +8.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +1.0, { -2.0, -4.0, -6.0, -8.0 });
		TestLerpD({ +1.0, +2.0, +3.0, +4.0 }, { -2.0, -4.0, -6.0, -8.0 }, +1.0, { -2.0, -4.0, -6.0, -8.0 });
		TestLerpD({ -1.0, -2.0, -3.0, -4.0 }, { +2.0, +4.0, +6.0, +8.0 }, +1.0, { +2.0, +4.0, +6.0, +8.0 });

		//Test with random-ish values 
		TestLerpD({ +5.2, +3.4, +2.0, +1.0 }, { +0.2, +1.0, +2.5, +0.4 }, +0.5, { +2.7, +2.2, +2.25, +0.7 });
	}

}

TEST_CASE("Spline between two vectors.", "[SlerpF], [SlerpD], [Math]")
{
	SECTION("Float vector.", "[SlerpF]")
	{
		auto TestSplineF = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORF _b, const GW::MATH::GVECTORF _c, const GW::MATH::GVECTORF _d, const float _ratio, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::SplineF(_a, _b, _c, _d, _ratio, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		TestSplineF(
			{ +0.0f, +0.0f, +0.0f, +0.0f },
			{ +1.0f, +1.0f, +0.0f, +0.0f },
			{ +3.0f, +1.0f, +0.0f, +0.0f },
			{ +4.0f, +0.0f, +0.0f, +0.0f },
			+0.5f,
			{ +2.0f, +1.1614983f, +0.0f, +0.0f });
	}

	SECTION("Double vector.", "[SlerpD]")
	{
		auto TestSplineD = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORD _b, const GW::MATH::GVECTORD _c, const GW::MATH::GVECTORD _d, const double _ratio, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::SplineD(_a, _b, _c, _d, _ratio, result)));
			COMPARE_VECTORS_STANDARD_D(result, _expectedResult);
		};

		TestSplineD(
			{ +0.0, +0.0, +0.0, +0.0 },
			{ +1.0, +1.0, +0.0, +0.0 },
			{ +3.0, +1.0, +0.0, +0.0 },
			{ +4.0, +0.0, +0.0, +0.0 },
			+0.5,
			{ +2.0, +1.1614983745349439, +0.0, +0.0 });
	}
}

TEST_CASE("Cast vectors between float and double.", "[Upgrade], [Downgrade], [Math]")
{

	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with 
	// the accuracy of the least accurate type. ex: testing float precision using 
	// double precision will cause false errors. 
	SECTION("Cast float vector to double vector.", "[Upgrade]")
	{
		auto TestUpgrade = [](const GW::MATH::GVECTORF _a, const GW::MATH::GVECTORD _expectedResult)
		{
			GW::MATH::GVECTORD result;
			CHECK(+(GW::MATH::GVector::Upgrade(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};


		for (int i = -1000; i < 1000; i++)
		{
			double valD = static_cast<double>(i) / 10.0;
			float valF = static_cast<float>(valD);

			TestUpgrade({ valF, valF + 1, valF + 2, valF + 3 }, { valD, valD + 1, valD + 2, valD + 3 });
		}

		TestUpgrade({ +5.0f, -1.0f, +2.0f, +4.0f }, { +5.0, -1.0, +2.0, +4.0 });
	}

	SECTION("Cast double vector to float vector.", "[Downgrade]")
	{
		auto TestDowngrade = [](const GW::MATH::GVECTORD _a, const GW::MATH::GVECTORF _expectedResult)
		{
			GW::MATH::GVECTORF result;
			CHECK(+(GW::MATH::GVector::Downgrade(_a, result)));
			COMPARE_VECTORS_STANDARD_F(result, _expectedResult);
		};

		for (int i = -1000; i < 1000; i++)
		{
			double valD = static_cast<double>(i) / 10.0;
			float valF = static_cast<float>(valD);

			TestDowngrade({ valD, valD + 1, valD + 2, valD + 3 }, { valF, valF + 1, valF + 2, valF + 3 });
		}

		TestDowngrade({ +5.0, -1.0, +2.0, +4.0 }, { +5.0f, -1.0f, +2.0f, +4.0f });
	}
}
#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GVECTOR) */