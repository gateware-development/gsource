#if defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GQUATERNION)
TEST_CASE("Create GQuaternion.", "[CreateGQuaternion], [Math]")
{
	GW::MATH::GQuaternion QuaternionC;

	//Pass cases
	REQUIRE(+(QuaternionC.Create()));
}

TEST_CASE("Add two quaternions.", "[AddQuaternionF], [AddQuaternionD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 1.1f,2.2f,3.3f,4.4f };
	GW::MATH::GQUATERNIONF qF2 = { 0.1f,0.2f,0.3f,0.4f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 1.1,2.2,3.3,4.4 };
	GW::MATH::GQUATERNIOND qD2 = { 0.1,0.2,0.3,0.4 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion addition.", "[AddQuaternionF]")
	{
		CHECK(+(GW::MATH::GQuaternion::AddQuaternionF(qF1, qF2, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 1.2f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 2.4f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 3.6f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 4.8f));
	}
	SECTION("Double quaternion addition.", "[AddQuaternionD]")
	{
		CHECK(+(GW::MATH::GQuaternion::AddQuaternionD(qD1, qD2, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 1.2));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 2.4));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 3.6L));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 4.8));
	}
}

TEST_CASE("Subtract two quaternions.", "[SubtractQuaternionF], [SubtractQuaternionD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 1.1f,2.2f,3.3f,4.4f };
	GW::MATH::GQUATERNIONF qF2 = { 0.1f,0.2f,0.3f,0.4f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 1.1,2.2,3.3,4.4 };
	GW::MATH::GQUATERNIOND qD2 = { 0.1,0.2,0.3,0.4 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion subtraction.", "[SubtractQuaternionF]")
	{
		CHECK(+(GW::MATH::GQuaternion::SubtractQuaternionF(qF1, qF2, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 3.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 4.0f));
	}
	SECTION("Double quaternion subtraction.", "[SubtractQuaternionD]")
	{
		CHECK(+(GW::MATH::GQuaternion::SubtractQuaternionD(qD1, qD2, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 1));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 2));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 3));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 4));
	}
}

TEST_CASE("Multiply two quaternions.", "[MultiplyQuaternionF], [MultiplyQuaternionD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 1.1f,2.2f,3.3f,4.4f };
	GW::MATH::GQUATERNIONF qF2 = { 0.1f,0.2f,0.3f,0.4f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 1.1,2.2,3.3,4.4 };
	GW::MATH::GQUATERNIOND qD2 = { 0.1,0.2,0.3,0.4 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion Multiplication.", "[MultiplyQuaternionF]")
	{
		CHECK(+(GW::MATH::GQuaternion::MultiplyQuaternionF(qF1, qF2, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 0.88f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 1.76f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 2.64f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.22f));

	}
	SECTION("Double quaternion Multiplication.", "[MultiplyQuaternionD]")
	{
		CHECK(+(GW::MATH::GQuaternion::MultiplyQuaternionD(qD1, qD2, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 0.88));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 1.76));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 2.64));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.22));
	}
}

TEST_CASE("Scale two quaternions.", "[ScaleF], [ScaleD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 1.1f,2.2f,3.3f,4.4f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 1.1,2.2,3.3,4.4 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion scaling.", "[ScaleF]")
	{
		CHECK(+(GW::MATH::GQuaternion::ScaleF(qF1, 0.1f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 0.11f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 0.22f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 0.33f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.44f));
	}
	SECTION("Double quaternion scaling.", "[ScaleD]")
	{
		CHECK(+(GW::MATH::GQuaternion::ScaleD(qD1, 0.1, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 0.11));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 0.22));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 0.33));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.44));
	}
}

TEST_CASE("Set a quaternion by a vector and a angle.", "[SetByVectorAngleF], [SetByVectorAngleD], [Math]")
{
	GW::MATH::GVECTORF vF = { 1.0f,1.0f,1.0f,1.0f };
	GW::MATH::GQUATERNIONF resultF;
	float radianF = G_PI_F / 2.0f;
	GW::MATH::GVECTORD vD = { 2.0,2.0,2.0,2.0 };
	double radianD = G_PI_D / 2.0;
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[SetByVectorAngleF]")
	{
		CHECK(+(GW::MATH::GQuaternion::SetByVectorAngleF(vF, radianF, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 1.0f * sqrt(2) / 2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 1.0f * sqrt(2) / 2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 1.0f * sqrt(2) / 2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, sqrt(2) / 2.0f));
	}
	SECTION("Double quaternion.", "[SetByVectorAngleD]")
	{
		CHECK(+(GW::MATH::GQuaternion::SetByVectorAngleD(vD, radianD, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 2.0 * sqrt(2) / 2.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 2.0 * sqrt(2) / 2.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 2.0 * sqrt(2) / 2.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, sqrt(2) / 2.0));
	}
}

TEST_CASE("Set a quaternion by a matrix.", "[SetByMatrixF], [SetByMatrixD], [Math]")
{
	GW::MATH::GMATRIXF mF = { 0,-1,0,0, 1,0,0,0, 0,0,1,0, 0,0,0,1 };
	GW::MATH::GQUATERNIONF resultF;
	GW::MATH::GMATRIXD mD = { 0,-1,0,0, 1,0,0,0, 0,0,1,0, 0,0,0,1 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[SetByMatrixF]")
	{
		CHECK(+(GW::MATH::GQuaternion::SetByMatrixF(mF, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 0.7071067f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.7071067f));
	}
	SECTION("Double quaternion.", "[SetByMatrixD]")
	{
		CHECK(+(GW::MATH::GQuaternion::SetByMatrixD(mD, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 0.7071067811865475));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.7071067811865475));
	}
}

TEST_CASE("Calculate the dot product of a quaternion.", "[DotF], [DotD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 1.1f,2.2f,3.3f,4.4f };
	GW::MATH::GQUATERNIONF qF2 = { 0.1f,0.2f,0.3f,0.4f };
	float resultF;

	GW::MATH::GQUATERNIOND qD1 = { 1.1,2.2,3.3,4.4 };
	GW::MATH::GQUATERNIOND qD2 = { 0.1,0.2,0.3,0.4 };
	double resultD;

	SECTION("Float quaternion dot product.", "[DotF]")
	{
		CHECK(+(GW::MATH::GQuaternion::DotF(qF1, qF2, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF, 3.3f));
	}
	SECTION("Double quaternion dot product.", "[DotD]")
	{
		CHECK(+(GW::MATH::GQuaternion::DotD(qD1, qD2, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD, 3.3));
	}
}

TEST_CASE("Calculate the cross product of a quaternion.", "[CrossF], [CrossD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 5.2f, 3.4f, 2, 6 };
	GW::MATH::GQUATERNIONF qF2 = { 0.2f, 1, 2.5f, 0.4f };
	GW::MATH::GVECTORF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 5.2, 3.4, 2, 6 };
	GW::MATH::GQUATERNIOND qD2 = { 0.2, 1, 2.5, 0.4 };
	GW::MATH::GVECTORD resultD;

	SECTION("Float quaternion subtraction.", "[CrossF]")
	{
		CHECK(+(GW::MATH::GQuaternion::CrossF(qF1, qF2, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 6.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, -12.6f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 4.52f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.0f));

	}
	SECTION("Double quaternion subtraction.", "[CrossD]")
	{
		CHECK(+(GW::MATH::GQuaternion::CrossD(qD1, qD2, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 6.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, -12.6));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 4.52));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.0));
	}
}


TEST_CASE("Set a quaternion to the conjugate of itself.", "[ConjugateF], [ConjugateD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 5.2f, 3.4f, 2, 6 };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 5.2, 3.4, 2, 6 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[ConjugateF]")
	{
		CHECK(+(GW::MATH::GQuaternion::ConjugateF(qF1, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, -5.2f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, -3.4f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, -2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 6.0f));
	}
	SECTION("Double quaternion.", "[ConjugateD]")
	{
		CHECK(+(GW::MATH::GQuaternion::ConjugateD(qD1, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, -5.2));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, -3.4));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, -2.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 6.0));
	}
}

TEST_CASE("Set a quaternion to the inverse of itself.", "[InverseF], [InverseD], [Math]")
{
	GW::MATH::GQUATERNIONF qF0 = { 0.0f, 0.0f, 0.0f, 0.0f };
	GW::MATH::GQUATERNIONF qF1 = { 10.0f, 8.0f, 6.0f, 0.0f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD0 = { 0, 0, 0, 0 };
	GW::MATH::GQUATERNIOND qD1 = { 10, 8, 6, 0 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[InverseF]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::InverseF(qF0, resultF)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::InverseF(qF1, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, -0.05f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, -0.04f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, -0.03f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.0f));
	}
	SECTION("Double quaternion.", "[InverseD]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::InverseD(qD0, resultD)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::InverseD(qD1, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, -0.05));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, -0.04));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, -0.03));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.0));
	}
}

TEST_CASE("Calcuate the magnitude of the quaternion.", "[MagnitudeF], [MagnitudeD], [Math]")
{
	GW::MATH::GQUATERNIONF qF0 = { 0.0f, 0.0f, 0.0f, 0.0f };
	GW::MATH::GQUATERNIONF qF1 = { 10.0f, 8.0f, 6.0f, 0.0f };
	float resultF;

	GW::MATH::GQUATERNIOND qD0 = { 0, 0, 0, 0 };
	GW::MATH::GQUATERNIOND qD1 = { 10, 8, 6, 0 };
	double resultD;

	SECTION("Float quaternion.", "[MagnitudeF]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::MagnitudeF(qF0, resultF)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::MagnitudeF(qF1, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF, sqrt(200)));
	}
	SECTION("Double quaternion.", "[MagnitudeD]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::MagnitudeD(qD0, resultD)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::MagnitudeD(qD1, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD, sqrt(200)));
	}
}

TEST_CASE("Normalize the quaternion.", "[NormalizeF], [NormalizeD], [Math]")
{
	GW::MATH::GQUATERNIONF qF0 = { 0.0f, 0.0f, 0.0f, 0.0f };
	GW::MATH::GQUATERNIONF qF1 = { 10.0f, 8.0f, 6.0f, 0.0f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD0 = { 0, 0, 0, 0 };
	GW::MATH::GQUATERNIOND qD1 = { 10, 8, 6, 0 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[NormalizeF]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::NormalizeF(qF0, resultF)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::NormalizeF(qF1, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 10.0f / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 8.0f / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 6.0f / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.0f / sqrt(200)));
	}
	SECTION("Double quaternion.", "[NormalizeD]")
	{
		//Fail cases
		CHECK(-(GW::MATH::GQuaternion::NormalizeD(qD0, resultD)));

		//Pass cases
		CHECK(+(GW::MATH::GQuaternion::NormalizeD(qD1, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 10 / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 8 / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 6 / sqrt(200)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0));
	}
}

TEST_CASE("Set a identity quaternion.", "[IdentityF], [IdentityD], [Math]")
{
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[IdentityF]")
	{
		CHECK(+(GW::MATH::GQuaternion::IdentityF(resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 1.0f));
	}
	SECTION("Double quaternion.", "[IdentityD]")
	{
		CHECK(+(GW::MATH::GQuaternion::IdentityD(resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 1));
	}
}

TEST_CASE("Calcuate the lerp of the quaternion.", "[LerpF], [LerpD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 0.0f, 0.0f, 0.0f, 0.0f };
	GW::MATH::GQUATERNIONF qF2 = { 3, 5, 7, 9 };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 0, 0, 0, 0 };
	GW::MATH::GQUATERNIOND qD2 = { 3, 5, 7, 9 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[LerpF]")
	{
		CHECK(+(GW::MATH::GQuaternion::LerpF(qF1, qF2, 0.5f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 1.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 2.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 3.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 4.5f));
	}
	SECTION("Double quaternion.", "[LerpD]")
	{
		CHECK(+(GW::MATH::GQuaternion::LerpD(qD1, qD2, 0.5, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 1.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 2.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 3.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 4.5));
	}
}

TEST_CASE("Calcuate the slerp of the quaternion.", "[SlerpF], [SlerpD], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 0.7071067f, 0.0f, 0.0f, 0.7071067f };
	GW::MATH::GQUATERNIONF qF2 = { 0.0f, 0.7071067f, 0.0f, 0.7071067f };
	GW::MATH::GQUATERNIONF resultF;

	GW::MATH::GQUATERNIOND qD1 = { 0.7071067811865475, 0, 0, 0.7071067811865475 };
	GW::MATH::GQUATERNIOND qD2 = { 0, 0.7071067811865475, 0, 0.7071067811865475 };
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[SlerpF]")
	{
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 0.5f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, 0.4082482f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, 0.4082482f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, 0.8164965f));
	}
	SECTION("Double quaternion.", "[SlerpD]")
	{
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 0.5, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, 0.40824829046386296));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, 0.40824829046386296));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, 0.8164965809277259));
	}
}

TEST_CASE("NaN Stress test for Quaternion Slerp.", "[SlerpF], [SlerpD], [Math]")
{
	//two matrices in the same rotation, but different position
	GW::MATH::GQUATERNIONF qF1 = GW::MATH::GIdentityQuaternionF;
	GW::MATH::GQUATERNIONF qF2 = GW::MATH::GIdentityQuaternionF;
	GW::MATH::GQUATERNIONF resultF;

	

	GW::MATH::GQUATERNIOND qD1 = GW::MATH::GIdentityQuaternionD;
	GW::MATH::GQUATERNIOND qD2 = GW::MATH::GIdentityQuaternionD;
	GW::MATH::GQUATERNIOND resultD;

	SECTION("Float quaternion.", "[SlerpF]")
	{
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));

		qF1.w += G_DEVIATION_PRECISE_F;
		qF2.w += G_DEVIATION_PRECISE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, qF1.w));
		qF1.w -= G_DEVIATION_PRECISE_F;
		qF2.w -= G_DEVIATION_PRECISE_F;

		qF1.w += G_DEVIATION_STANDARD_F;
		qF2.w += G_DEVIATION_STANDARD_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, qF1.w));
		qF1.w -= G_DEVIATION_STANDARD_F;
		qF2.w -= G_DEVIATION_STANDARD_F;

		qF1.w += G_DEVIATION_LOOSE_F;
		qF2.w += G_DEVIATION_LOOSE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.w, qF1.w));
		qF1.w -= G_DEVIATION_LOOSE_F;
		qF2.w -= G_DEVIATION_LOOSE_F;

		qF1.x += G_DEVIATION_PRECISE_F;
		qF2.x += G_DEVIATION_PRECISE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, qF1.x));
		qF1.x -= G_DEVIATION_PRECISE_F;
		qF2.x -= G_DEVIATION_PRECISE_F;

		qF1.x += G_DEVIATION_STANDARD_F;
		qF2.x += G_DEVIATION_STANDARD_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, qF1.x));
		qF1.x -= G_DEVIATION_STANDARD_F;
		qF2.x -= G_DEVIATION_STANDARD_F;

		qF1.x += G_DEVIATION_LOOSE_F;
		qF2.x += G_DEVIATION_LOOSE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.x, qF1.x));
		qF1.x -= G_DEVIATION_LOOSE_F;
		qF2.x -= G_DEVIATION_LOOSE_F;

		qF1.y += G_DEVIATION_PRECISE_F;
		qF2.y += G_DEVIATION_PRECISE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, qF1.y));
		qF1.y -= G_DEVIATION_PRECISE_F;
		qF2.y -= G_DEVIATION_PRECISE_F;

		qF1.y += G_DEVIATION_STANDARD_F;
		qF2.y += G_DEVIATION_STANDARD_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, qF1.y));
		qF1.y -= G_DEVIATION_STANDARD_F;
		qF2.y -= G_DEVIATION_STANDARD_F;

		qF1.y += G_DEVIATION_LOOSE_F;
		qF2.y += G_DEVIATION_LOOSE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.y, qF1.y));
		qF1.y -= G_DEVIATION_LOOSE_F;
		qF2.y -= G_DEVIATION_LOOSE_F;

		qF1.z += G_DEVIATION_PRECISE_F;
		qF2.z += G_DEVIATION_PRECISE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, qF1.z));
		qF1.z -= G_DEVIATION_PRECISE_F;
		qF2.z -= G_DEVIATION_PRECISE_F;

		qF1.z += G_DEVIATION_STANDARD_F;
		qF2.z += G_DEVIATION_STANDARD_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, qF1.z));
		qF1.z -= G_DEVIATION_STANDARD_F;
		qF2.z -= G_DEVIATION_STANDARD_F;

		qF1.z += G_DEVIATION_LOOSE_F;
		qF2.z += G_DEVIATION_LOOSE_F;
		CHECK(+(GW::MATH::GQuaternion::SlerpF(qF1, qF2, 1.0f, resultF)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(resultF.z, qF1.z));
		qF1.z -= G_DEVIATION_LOOSE_F;
		qF2.z -= G_DEVIATION_LOOSE_F;
	}
	SECTION("Double quaternion.", "[SlerpD]")
	{
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 0.8, resultD)));

		qD1.w += G_DEVIATION_PRECISE_D;
		qD2.w += G_DEVIATION_PRECISE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, qD1.w));
		qD1.w -= G_DEVIATION_PRECISE_D;
		qD2.w -= G_DEVIATION_PRECISE_D;

		qD1.w += G_DEVIATION_STANDARD_D;
		qD2.w += G_DEVIATION_STANDARD_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, qD1.w));
		qD1.w -= G_DEVIATION_STANDARD_D;
		qD2.w -= G_DEVIATION_STANDARD_D;

		qD1.w += G_DEVIATION_LOOSE_D;
		qD2.w += G_DEVIATION_LOOSE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.w, qD1.w));
		qD1.w -= G_DEVIATION_LOOSE_D;
		qD2.w -= G_DEVIATION_LOOSE_D;

		qD1.x += G_DEVIATION_PRECISE_D;
		qD2.x += G_DEVIATION_PRECISE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, qD1.x));
		qD1.x -= G_DEVIATION_PRECISE_D;
		qD2.x -= G_DEVIATION_PRECISE_D;

		qD1.x += G_DEVIATION_STANDARD_D;
		qD2.x += G_DEVIATION_STANDARD_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, qD1.x));
		qD1.x -= G_DEVIATION_STANDARD_D;
		qD2.x -= G_DEVIATION_STANDARD_D;

		qD1.x += G_DEVIATION_LOOSE_D;
		qD2.x += G_DEVIATION_LOOSE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.x, qD1.x));
		qD1.x -= G_DEVIATION_LOOSE_D;
		qD2.x -= G_DEVIATION_LOOSE_D;

		qD1.y += G_DEVIATION_PRECISE_D;
		qD2.y += G_DEVIATION_PRECISE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, qD1.y));
		qD1.y -= G_DEVIATION_PRECISE_D;
		qD2.y -= G_DEVIATION_PRECISE_D;

		qD1.y += G_DEVIATION_STANDARD_D;
		qD2.y += G_DEVIATION_STANDARD_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, qD1.y));
		qD1.y -= G_DEVIATION_STANDARD_D;
		qD2.y -= G_DEVIATION_STANDARD_D;

		qD1.y += G_DEVIATION_LOOSE_D;
		qD2.y += G_DEVIATION_LOOSE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.y, qD1.y));
		qD1.y -= G_DEVIATION_LOOSE_D;
		qD2.y -= G_DEVIATION_LOOSE_D;

		qD1.z += G_DEVIATION_PRECISE_D;
		qD2.z += G_DEVIATION_PRECISE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, qD1.z));
		qD1.z -= G_DEVIATION_PRECISE_D;
		qD2.z -= G_DEVIATION_PRECISE_D;

		qD1.z += G_DEVIATION_STANDARD_D;
		qD2.z += G_DEVIATION_STANDARD_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, qD1.z));
		qD1.z -= G_DEVIATION_STANDARD_D;
		qD2.z -= G_DEVIATION_STANDARD_D;

		qD1.z += G_DEVIATION_LOOSE_D;
		qD2.z += G_DEVIATION_LOOSE_D;
		CHECK(+(GW::MATH::GQuaternion::SlerpD(qD1, qD2, 1.0f, resultD)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(resultD.z, qD1.z));
		qD1.z -= G_DEVIATION_LOOSE_D;
		qD2.z -= G_DEVIATION_LOOSE_D;
	}
}

TEST_CASE("Cast quaternions between float and double.", "[Upgrade], [Downgrade], [Math]")
{
	GW::MATH::GQUATERNIONF qF1 = { 5.0f, -1.0f, 2.0f, 4.0f };

	GW::MATH::GQUATERNIONF qF2;

	GW::MATH::GQUATERNIOND qD1 = { 5.0, -1.0, 2.0, 4.0 };

	GW::MATH::GQUATERNIOND qD2;


	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with 
	// the accuracy of the least accurate type. ex: testing float precision using 
	// double precision will cause false positive errors. 
	SECTION("Cast float quaternion to double quaterion.", "[Upgrade]") {
		CHECK(+(GW::MATH::GQuaternion::Upgrade(qF1, qD2)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qD2.x, 5.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qD2.y, -1.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qD2.z, 2.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qD2.w, 4.0));
	}
	SECTION("Cast double quaternion to float quaternion.", "[Downgrade]") {
		CHECK(+(GW::MATH::GQuaternion::Downgrade(qD1, qF2)));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qF2.x, 5.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qF2.y, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qF2.z, 2.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(qF2.w, 4.0f));
	}
}
#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GQUATERNION) */