#if defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GCOLLISION)
#include <limits.h>

TEST_CASE("Create GCollision.", "[CreateGCollision], [Math]")
{
	GW::MATH::GCollision CollisionC;

	//Pass cases
	REQUIRE(+(CollisionC.Create()));
}

TEST_CASE("Compute plane.", "[ComputePlaneF], [ComputePlaneD], [Math]")
{
	GW::MATH::GVECTORF aF = {};
	GW::MATH::GVECTORF bF = {};
	GW::MATH::GVECTORF cF = {};
	GW::MATH::GPLANEF planeF = {};

	GW::MATH::GVECTORD aD = {};
	GW::MATH::GVECTORD bD = {};
	GW::MATH::GVECTORD cD = {};
	GW::MATH::GPLANED planeD = {};

	SECTION("F structures compute degenerate plane point fail.", "[ComputePlaneF]")
	{
		aF = {};
		bF = {};
		cF = {};
		planeF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputePlaneF(
			aF,
			bF,
			cF,
			planeF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.z, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.distance, 0.0f));
	}

	SECTION("D structures compute degenerate plane point fail.", "[ComputePlaneD]")
	{
		aD = {};
		bD = {};
		cD = {};
		planeD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputePlaneD(
			aD,
			bD,
			cD,
			planeD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.z, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.distance, 0.0));
	}

	SECTION("F structures compute degenerate plane line fail.", "[ComputePlaneF]")
	{
		aF = {};
		bF = {1.0f};
		cF = {2.0f};
		planeF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputePlaneF(
			aF,
			bF,
			cF,
			planeF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.z, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.distance, 0.0f));
	}

	SECTION("D structures compute degenerate plane line fail.", "[ComputePlaneD]")
	{
		aD = {};
		bD = {1.0};
		cD = {2.0};
		planeD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputePlaneD(
			aD,
			bD,
			cD,
			planeD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.z, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.distance, 0.0));
	}

	SECTION("F structures compute non-degenerate plane.", "[ComputePlaneF]")
	{
		aF =
		{
			5.6211409f,
			5.8501523f,
			7.8089941f,
		};

		bF =
		{
			2.6734894f,
			8.4590874f,
			0.9210452f,
		};

		cF =
		{
			0.6075733f,
			3.0886759f,
			8.4081993f,
		};

		planeF = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputePlaneF(
			aF,
			bF,
			cF,
			planeF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.x, 0.383456796f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.y, -0.797317564f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.z, -0.466096193f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(planeF.distance, -6.14870739f));
	}

	SECTION("D structures compute non-degenerate plane.", "[ComputePlaneD]")
	{
		aD =
		{
			5.6211409,
			5.8501523,
			7.8089941,
		};

		bD =
		{
			2.6734894,
			8.4590874,
			0.9210452,
		};

		cD =
		{
			0.6075733,
			3.0886759,
			8.4081993,
		};

		planeD = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputePlaneD(
			aD,
			bD,
			cD,
			planeD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.x, 0.38345675252352107));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.y, -0.79731755105003410));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.z, -0.46609617219163191));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(planeD.distance, -6.1487069287916203));
	}
}

TEST_CASE("Is Triangle.", "[IsTriangleF], [IsTriangleD], [Math]")
{
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck result;

	SECTION("F structures degenerate triangle point false.", "[IsTriangleF]")
	{
		triangleF = {};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleF(
			triangleF,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate triangle point false.", "[IsTriangleD]")
	{
		triangleD = {};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleD(
			triangleD,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures degenerate triangle line false.", "[IsTriangleF]")
	{
		triangleF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleF(
			triangleF,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate triangle line false.", "[IsTriangleD]")
	{
		triangleD =
		{
			0.0, 0.0, 0.0, 0.0,
			1.0, 0.0, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleD(
			triangleD,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures CW triangle true.", "[IsTriangleF]")
	{
		triangleF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleF(
			triangleF,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures CW triangle true.", "[IsTriangleD]")
	{
		triangleD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			1.0, 0.0, 0.0, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleD(
			triangleD,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures CCW triangle true.", "[IsTriangleF]")
	{
		triangleF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleF(
			triangleF,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures CCW triangle true.", "[IsTriangleD]")
	{
		triangleD =
		{
			0.0, 0.0, 0.0, 0.0,
			1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::IsTriangleD(
			triangleD,
			result)));

		CHECK(result == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test Point to Convex Polygon.", "[TestPointToConvexPolygonF], [TestPointToConvexPolygonD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures nullptr fail", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 5;

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			nullptr,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures nullptr fail", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 5;

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			nullptr,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures degenerate polygon point fail", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 1;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
		};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures degenerate polygon point fail", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 1;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{},
		};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures degenerate polygon line fail", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 2;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
		};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures degenerate polygon line fail", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 2;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
		};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures degenerate 3-gon collinear no collision", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{2.0f,0.0f,0.0f},
		};

		pointF = {1.0F, 0.0F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate 3-gon collinear no collision", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{2.0,0.0,0.0},
		};

		pointD = {1.0, 0.0, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures degenerate 3-gon no collision", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{2.0f,0.0f,0.0f},
		};

		pointF = {0.0F, -1.0F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate 3-gon no collision", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{2.0,0.0,0.0},
		};

		pointD = {0.0, -1.0, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test CCW 3-gon collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
			{1.0f,0.0f,0.0f},
		};

		pointF = {0.2F, 0.2F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures test CCW 3-gon collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{0.0,1.0,0.0},
			{1.0,0.0,0.0},
		};

		pointD = {0.2, 0.2, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures test CCW 3-gon no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
			{1.0f,0.0f,0.0f},
		};


		pointF = {-0.2F, -0.2F, 0.0F};
		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test CCW 3-gon no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{0.0,1.0,0.0},
			{1.0,0.0,0.0},
		};


		pointD = {-0.2, -0.2, 0.0};
		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test CW 3-gon collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {0.2F, 0.2F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures test CW 3-gon collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {0.2, 0.2, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures test CW 3-gon no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {-0.2F, -0.2F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test CW 3-gon no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 3;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {-0.2, -0.2, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test CCW 4-gon collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{1.0f,0.0f,0.0f},
		};

		pointF = {0.5F, 0.5F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures test CCW 4-gon collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{0.0,1.0,0.0},
			{1.0,1.0,0.0},
			{1.0,0.0,0.0},
		};

		pointD = {0.5, 0.5, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures test CCW 4-gon no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{0.0f,1.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{1.0f,0.0f,0.0f},
		};

		pointF = {-0.5F, -0.5F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test CCW 4-gon no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{0.0,1.0,0.0},
			{1.0,1.0,0.0},
			{1.0,0.0,0.0},
		};

		pointD = {-0.5, -0.5, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test CW 4-gon collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {0.5F, 0.5F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures test CW 4-gon collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{1.0,1.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {0.5, 0.5, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures test edge no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {0.5F, 0.0F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test edge no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{1.0,1.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {0.5, 0.0, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test corner no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {1.0F, 0.0F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test corner no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{1.0,1.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {1.0, 0.0, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures test CW 4-gon no collision.", "[TestPointToConvexPolygonF]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{0.0f,0.0f,0.0f},
			{1.0f,0.0f,0.0f},
			{1.0f,1.0f,0.0f},
			{0.0f,1.0f,0.0f},
		};

		pointF = {-0.5F, -0.5F, 0.0F};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonF(
			pointF,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures test CW 4-gon no collision.", "[TestPointToConvexPolygonD]")
	{
		const unsigned int polygon_size = 4;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{0.0,0.0,0.0},
			{1.0,0.0,0.0},
			{1.0,1.0,0.0},
			{0.0,1.0,0.0},
		};

		pointD = {-0.5, -0.5, 0.0};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToConvexPolygonD(
			pointD,
			convex,
			polygon_size,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

// CLOSEST POINT QUERY

TEST_CASE("Closest point to line.", "[ClosestPointToLineF], [ClosestPointToLineD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures degenerate line(point) origin.", "[ClosestPointToLineF]")
	{
		lineF = {};

		pointF = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineF(
			lineF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0f));
	};

	SECTION("D structures degenerate line(point) origin.", "[ClosestPointToLineD]")
	{
		lineD = {};

		pointD = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineD(
			lineD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.0));
	};

	SECTION("F structures degenerate line(point).", "[ClosestPointToLineF]")
	{
		lineF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineF(
			lineF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, lineF.start.z));
	};

	SECTION("D structures degenerate line(point).", "[ClosestPointToLineD]")
	{
		lineD =
		{
			1.42372, 1.2671, 0.860159, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-0.0512187, 0.212759, -0.153923, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineD(
			lineD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, lineD.start.z));
	};

	SECTION("F structures clamp to line start.", "[ClosestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineF(
			lineF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, lineF.start.z));
	};

	SECTION("D structures clamp to line start.", "[ClosestPointToLineD]")
	{
		lineD =
		{
			-1.91171, -0.627527, -1.2929, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-2.76445, -1.60703, -1.91709, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineD(
			lineD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, lineD.start.z));
	};

	SECTION("F structures clamp to line end.", "[ClosestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineF(
			lineF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, lineF.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, lineF.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, lineF.end.z));
	};

	SECTION("D structures clamp to line end.", "[ClosestPointToLineD]")
	{
		lineD =
		{
			-1.91171, -0.627527, -1.2929, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			2.30043, 3.01664, 1.30966, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineD(
			lineD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, lineD.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, lineD.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, lineD.end.z));
	};

	SECTION("F structures closest point on line.", "[ClosestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineF(
			lineF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.144939303f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.376053095f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.152428865f));
	};

	SECTION("D structures closest point on line.", "[ClosestPointToLineD]")
	{
		lineD =
		{
			-1.91171, -0.627527, -1.2929, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-0.0512187, 0.212759, -0.153923, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToLineD(
			lineD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.14493929786797555));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.37605319058061193));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.15242888798095766));
	};
}

TEST_CASE("Closest point to line from line.", "[ClosestPointsToLineFromLineF], [ClosestPointsToLineFromLineD], [Math]")
{
	GW::MATH::GLINEF lineF1 = {};
	GW::MATH::GLINEF lineF2 = {};
	GW::MATH::GVECTORF cPointF1 = {};
	GW::MATH::GVECTORF cPointF2 = {};

	GW::MATH::GLINED lineD1 = {};
	GW::MATH::GLINED lineD2 = {};
	GW::MATH::GVECTORD cPointD1 = {};
	GW::MATH::GVECTORD cPointD2 = {};

	SECTION("F structures degenerate lines(points) origin.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 = {};

		lineF2 = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, 0.0f));
	};

	SECTION("D structures degenerate lines(points) origin.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 = {};

		lineD2 = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, 0.0));
	};

	SECTION("F structures degenerate lines.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
		};

		lineF2 =
		{
			-0.542453f, 1.29884f, 0.450732f, 0.0f,
			-0.542453f, 1.29884f, 0.450732f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, lineF1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, lineF1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, lineF1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, lineF2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, lineF2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, lineF2.start.z));
	};

	SECTION("D structures degenerate lines.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			-2.30285, 1.23042, 0.445423, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		lineD2 =
		{
			-0.542453, 1.29884, 0.450732, 0.0,
			-0.542453, 1.29884, 0.450732, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.start.z));
	};

	SECTION("F structures clamp to line1.start and line2.start.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			-1.05131f, 2.98064f, -1.3871f, 0.0f,
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
		};

		lineF2 =
		{
			-0.556896f, 3.0925f, -1.36733f, 0.0f,
			3.55321f, 1.29884f, 0.450732f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, lineF1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, lineF1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, lineF1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, lineF2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, lineF2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, lineF2.start.z));
	};

	SECTION("D structures clamp to line1.start and line2.start.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			-1.05131, 2.98064, -1.3871, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		lineD2 =
		{
			-0.556896, 3.0925, -1.36733, 0.0,
			3.55321, 1.29884, 0.450732, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.start.z));
	};

	SECTION("F structures clamp to line1.start and line2.end.", "[ClosestPointsToLineFromLineF]")
	{
		lineD1 =
		{
			-1.05131, 2.98064, -1.3871, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		lineD2 =
		{
			-0.556896, 3.0925, -1.36733, 0.0,
			3.55321, 1.29884, 0.450732, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.start.z));
	};

	SECTION("F structures clamp to line1.start and line2.end.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			-0.542453f, 1.29884f, 0.450732f, 0.0f,
			2.5075f, 2.85972f, -2.78909f, 0.0f
		};

		lineF2 =
		{
			-3.96141f, 0.0f, -3.68738f, 0.0f,
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, lineF1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, lineF1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, lineF1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, lineF2.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, lineF2.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, lineF2.end.z));
	};

	SECTION("D structures clamp to line1.start and line2.end.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			-0.542453, 1.29884, 0.450732, 0.0,
			2.5075, 2.85972, -2.78909, 0.0
		};

		lineD2 =
		{
			-3.96141, 0.0, -3.68738, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.end.z));
	};

	SECTION("F structures clamp to line1.end and line2.start.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			-3.96141f, 0.0f, -3.68738f, 0.0f,
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
		};

		lineF2 =
		{
			-0.542453f, 1.29884f, 0.450732f, 0.0f,
			2.5075f, 2.85972f, -2.78909f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, lineF1.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, lineF1.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, lineF1.end.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, lineF2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, lineF2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, lineF2.start.z));
	};

	SECTION("D structures clamp to line1.end and line2.start.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			-3.96141, 0.0, -3.68738, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		lineD2 =
		{
			-0.542453, 1.29884, 0.450732, 0.0,
			2.5075, 2.85972, -2.78909, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.end.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.start.z));
	};

	SECTION("F structures clamp to line1.end and line2.end.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			-3.96141f, 0.0f, -3.68738f, 0.0f,
			-2.30285f, 1.23042f, 0.445423f, 0.0f,
		};

		lineF2 =
		{
			2.5075f, 2.85972f, -2.78909f, 0.0f,
			-0.542453f, 1.29884f, 0.450732f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, lineF1.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, lineF1.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, lineF1.end.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, lineF2.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, lineF2.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, lineF2.end.z));
	};

	SECTION("D structures clamp to line1.end and line2.end.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			-3.96141, 0.0, -3.68738, 0.0,
			-2.30285, 1.23042, 0.445423, 0.0,
		};

		lineD2 =
		{
			2.5075, 2.85972, -2.78909, 0.0,
			-0.542453, 1.29884, 0.450732, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, lineD1.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, lineD1.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, lineD1.end.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, lineD2.end.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, lineD2.end.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, lineD2.end.z));
	};

	SECTION("F structures intervals between both.", "[ClosestPointsToLineFromLineF]")
	{
		lineF1 =
		{
			1.76481f, 3.37185f, -2.10607f, 0.0f,
			0.513268f, 1.62163f, -0.273544f, 0.0f
		};

		lineF2 =
		{
			3.55321f, 1.29884f, 0.450732f, 0.0f,
			-0.556896f, 3.0925f, -1.36733f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineF(
			lineF1,
			lineF2,
			cPointF1,
			cPointF2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.x, 0.944745064f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.y, 2.22502947f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF1.z, -0.905319095f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.x, 0.947004557f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.y, 2.43619442f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF2.z, -0.702095509f));
	};

	SECTION("D structures intervals between both.", "[ClosestPointsToLineFromLineD]")
	{
		lineD1 =
		{
			1.76481, 3.37185, -2.10607, 0.0,
			0.513268, 1.62163, -0.273544, 0.0
		};

		lineD2 =
		{
			3.55321, 1.29884, 0.450732, 0.0,
			-0.556896, 3.0925, -1.36733, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointsToLineFromLineD(
			lineD1,
			lineD2,
			cPointD1,
			cPointD2)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.x, 0.94474511810504891));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.y, 2.2250295516329608));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD1.z, -0.90531907213707008));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.x, 0.94700420419605269));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.y, 2.4361943864079683));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD2.z, -0.70209562087666721));
	};
}

TEST_CASE("Closest point to ray.", "[ClosestPointToRayF], [ClosestPointToRayD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToRayF]")
	{
		rayF = {};

		pointF = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayF(
			rayF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0f));
	};

	SECTION("D structures origin.", "[ClosestPointToRayD]")
	{
		rayD = {};

		pointD = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayD(
			rayD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointF.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointF.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointF.z, 0.0));
	};

	SECTION("F structures degenerate ray direction.", "[ClosestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f
		};

		pointF =
		{
			3.87624f, 1.37115f, 2.50055f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayF(
			rayF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, rayF.position.z));
	};

	SECTION("D structures degenerate ray direction.", "[ClosestPointToRayD]")
	{
		rayD =
		{
			2.21534, -1.48171, -0.921065, 0.0,
			0.0, 0.0, 0.0, 0.0
		};

		pointD =
		{
			3.87624, 1.37115, 2.50055, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayD(
			rayD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, rayD.position.z));
	};

	SECTION("F structures clamp to ray.position.", "[ClosestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.427926868f, 0.678688467f, 0.596875668f, 0.0f
		};

		pointF =
		{
			1.46936f, -2.78284f, -1.75898f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayF(
			rayF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, rayF.position.z));
	};

	SECTION("D structures clamp to ray.position.", "[ClosestPointToRayD]")
	{
		rayD =
		{
			2.21534, -1.48171, -0.921065, 0.0,
			0.427926868, 0.678688467, 0.596875668, 0.0
		};

		pointD =
		{
			1.46936, -2.78284, -1.75898, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayD(
			rayD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, rayD.position.z));
	};

	SECTION("F structures closest point on ray interval.", "[ClosestPointToRayF]")
	{
		rayF =
		{
			0.135887f, -1.18402f, -1.38998f, 0.0f,
			0.509439111f, 0.613975346f, 0.602914631f, 0.0f
		};

		pointF =
		{
			3.10004f, 2.59201f, 1.91623f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayF(
			rayF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.10174322f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 2.39042616f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 2.12007284f));
	};

	SECTION("D structures closest point on ray interval.", "[ClosestPointToRayD]")
	{
		rayD =
		{
			0.135887, -1.18402, -1.38998, 0.0,
			0.509439111, 0.613975346, 0.602914631, 0.0
		};

		pointD =
		{
			3.10004, 2.59201, 1.91623, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToRayD(
			rayD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.1017433154660958));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 2.3904261274285279));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 2.1200727765294820));
	};
}

TEST_CASE("Closest point to triangle.", "[ClosestPointToTriangleF], [ClosestPointToTriangleD], [Math]")
{
	GW::MATH::GTRIANGLEF triangleF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GTRIANGLED triangleD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures degenerate triangle(point).", "[ClosestPointToTriangleF]")
	{
		triangleF = {};

		pointF = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, triangleF.a.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, triangleF.b.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, triangleF.c.z));
	};

	SECTION("D structures degenerate triangle(point).", "[ClosestPointToTriangleD]")
	{
		triangleD = {};

		pointD = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, triangleD.a.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, triangleD.b.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, triangleD.c.z));
	};

	SECTION("F structures degenerate triangle(line).", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
		};

		pointF =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.808485985f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.283262253f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.975065231f));
	};

	SECTION("D structures degenerate triangle(line).", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
		};

		pointD =
		{
			0.285477, -0.1961, -0.103414, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.80848601951536825));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.28326229305706119));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.97506530472105202));
	};

	SECTION("F structures clamp to triangle.a.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF =
		{
			-7.31277f, -4.50326f, -11.595f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, triangleF.a.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, triangleF.a.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, triangleF.a.z));
	};

	SECTION("D structures clamp to triangle.a.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362, -1.29386, -2.07792, 0.0,
			-2.18926, 3.38054, 3.51827, 0.0,
			3.96233, 0.435821, 5.20674, 0.0,
		};

		pointD =
		{
			-7.31277, -4.50326, -11.595, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, triangleD.a.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, triangleD.a.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, triangleD.a.z));
	};

	SECTION("F structures clamp to triangle.b.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF =
		{
			-7.08199f, 9.43563f, 6.66899f, -1.9487f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, triangleF.b.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, triangleF.b.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, triangleF.b.z));
	};

	SECTION("D structures clamp to triangle.b.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362, -1.29386, -2.07792, 0.0,
			-2.18926, 3.38054, 3.51827, 0.0,
			3.96233, 0.435821, 5.20674, 0.0,
		};

		pointD =
		{
			-7.08199, 9.43563, 6.66899, -1.9487
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, triangleD.b.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, triangleD.b.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, triangleD.b.z));

	};

	SECTION("F structures clamp to triangle.c.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-2.12933f, -1.47871f, -1.91205f, 0.0f,
			-1.27235f, 2.27828f, 2.07457f, 0.0f,
			6.31262f, 1.12794f, 3.92865f, 0.0f,
		};

		pointF =
		{
			9.89622f, 1.23486f, 5.13347f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, triangleF.c.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, triangleF.c.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, triangleF.c.z));
	};

	SECTION("D structures clamp to triangle.c.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-2.12933, -1.47871, -1.91205, 0.0,
			-1.27235, 2.27828, 2.07457, 0.0,
			6.31262, 1.12794, 3.92865, 0.0,
		};

		pointD =
		{
			9.89622, 1.23486, 5.13347, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, triangleD.c.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, triangleD.c.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, triangleD.c.z));
	};

	SECTION("F structures clamp to triangle.ab edge.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			-1.47693f, 0.19042f, -1.4358f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.971077561f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.0193960667f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.937451422f));
	};

	SECTION("D structures clamp to triangle.ab edge.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			-1.47693, 0.19042, -1.4358, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.97107751075720716));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.019395932114651071));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.93745148174165649));
	};

	SECTION("F structures clamp to triangle.ac edge.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.347408f, -1.95765f, 0.888979f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.198596954f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -1.08601880f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.563942194f));
	};

	SECTION("D structures clamp to triangle.ac edge.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			0.347408, -1.95765, 0.888979, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.19859710029540278));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -1.0860188636580890));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.56394227357438464));
	};

	SECTION("F structures clamp to triangle.bc edge.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			1.08696f, 1.36493f, -0.103414f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.610254467f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.917297006f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.207013845f));
	};

	SECTION("D structures clamp to triangle.bc edge.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			1.08696, 1.36493, -0.103414, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.61025455930951200));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.91729696008212636));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.20701379564476019));
	};

	SECTION("F structures clamp to triangle face.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.183165312f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.124949217f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0268400311f));
	};

	SECTION("D structures clamp to triangle face.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			0.285477, -0.1961, -0.103414, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.18316527233216262));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.12494923082095633));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.026839953419537488));
	};

	SECTION("F structures on triangle surface.", "[ClosestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.183165312f, -0.124949217f, 0.0268400311f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleF(
			triangleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures on triangle surface.", "[ClosestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			0.18316533417455383, -0.12494923242087830, 0.026840002869380708, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToTriangleD(
			triangleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};
}

TEST_CASE("Closest point to plane.", "[ClosestPointToPlaneF], [ClosestPointToPlaneD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToPlaneF]")
	{
		planeF = {};

		pointF = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneF(
			planeF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0f));
	};

	SECTION("D structures origin.", "[ClosestPointToPlaneD]")
	{
		planeD = {};

		pointD = {};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneD(
			planeD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.0));
	};

	SECTION("F structures below plane clamped.", "[ClosestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			-1.52652f, -0.501646f, -1.48953f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneF(
			planeF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.547565758f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 3.70055580f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.317047000f));
	};

	SECTION("D structures below plane clamped.", "[ClosestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978, 0.939839005, 0.262230426, 3.2749
		};

		pointD =
		{
			-1.52652, -0.501646, -1.48953, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneD(
			planeD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.54756569407112465));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 3.7005560547123508));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.31704702250823780));
	};

	SECTION("F structures above plane clamped.", "[ClosestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			1.07403f, 4.43582f, 2.29149f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneF(
			planeF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.695227504f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 2.80979466f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 1.83780241f));
	};

	SECTION("D structures above plane clamped.", "[ClosestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978, 0.939839005, 0.262230426, 3.2749f
		};

		pointD =
		{
			1.07403, 4.43582, 2.29149, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneD(
			planeD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.69522748685248770));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 2.8097944649590114));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 1.8378023295881125));
	};

	SECTION("F structures on plane surface.", "[ClosestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			-0.547565758f, 3.70055580f, -0.317047000f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneF(
			planeF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));

		pointF =
		{
			0.695227504f, 2.80979466f, 1.83780241f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneF(
			planeF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures on plane surface.", "[ClosestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978, 0.939839005, 0.262230426, 3.2749f
		};

		pointD =
		{
			-0.54756564493848059, 3.7005562853212717, -0.31704686458744191, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneD(
			planeD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));

		pointD =
		{
			0.69522743109726792, 2.8097943524040940, 1.8378023230785427, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToPlaneD(
			planeD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};
}

TEST_CASE("Closest point to sphere.", "[ClosestPointToSphereF], [ClosestPointToSphereD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToSphereF]")
	{
		sphereF = {};

		pointF = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereF(
			sphereF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, sphereF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, sphereF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, sphereF.z));
	};

	SECTION("D structures origin.", "[ClosestPointToSphereD]")
	{
		sphereD = {};

		pointD = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereD(
			sphereD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, sphereD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, sphereD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, sphereD.z));
	};

	SECTION("F structures sphere w/o radius.", "[ClosestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
		};

		pointF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereF(
			sphereF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, sphereF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, sphereF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, sphereF.z));
	};

	SECTION("D structures sphere w/o radius.", "[ClosestPointToSphereD]")
	{
		sphereD =
		{
			2.21534, -1.48171, -0.921065, 0.0,
		};

		pointD =
		{
			2.21534, -1.48171, -0.921065, 0.0,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereD(
			sphereD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, sphereD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, sphereD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, sphereD.z));
	};

	SECTION("F structures center.", "[ClosestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereF(
			sphereF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, sphereF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, sphereF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, sphereF.z));
	};

	SECTION("D structures center.", "[ClosestPointToSphereD]")
	{
		sphereD =
		{
			2.21534, -1.48171, -0.921065, 5.0,
		};

		pointD =
		{
			2.21534, -1.48171, -0.921065, 0.0,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToSphereD(
			sphereD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, sphereD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, sphereD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, sphereD.z));
	};

	SECTION("F structures clamp to sphere surface.", "[ClosestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			-5.21534f, -5.48171f, -0.921065f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToSphereF(
			sphereF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -2.18729615f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -3.85168743f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -0.921064973f));
	};

	SECTION("D structures clamp to sphere surface.", "[ClosestPointToSphereD]")
	{
		sphereD =
		{
			2.21534, -1.48171, -0.921065, 5.0,
		};

		pointD =
		{
			-5.21534, -5.48171, -0.921065, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToSphereD(
			sphereD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -2.1872962784527705));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -3.8516875947572875));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -0.92106500000000002));
	};

	SECTION("F structures point on sphere surface.", "[ClosestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			-2.18729615f, -3.85168743f, -0.921064973f, 0.0f,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToSphereF(
			sphereF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures point on sphere surface.", "[ClosestPointToSphereD]")
	{
		sphereD =
		{
			2.21534, -1.48171, -0.921065, 5.0,
		};

		pointD =
		{
			-2.1872963183571481, -3.8516875206281216, -0.9210649729999990, 0.0,
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToSphereD(
			sphereD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};
}

TEST_CASE("Closest point to capsule.", "[ClosestPointToCapsuleF], [ClosestPointToCapsuleD], [Math]")
{
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToCapsuleF]")
	{
		capsuleF = {};

		pointF = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0f));
	};

	SECTION("D structures origin.", "[ClosestPointToCapsuleD]")
	{
		capsuleD = {};

		pointD = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.0));
	};

	SECTION("F structures degenerate capsule point.", "[ClosestPointToCapsuleF]")
	{
		capsuleF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, capsuleF.data[0].x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, capsuleF.data[0].y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, capsuleF.data[0].z));
	};

	SECTION("D structures degenerate capsule point.", "[ClosestPointToCapsuleD]")
	{
		capsuleD =
		{
			1.42372, 1.2671, 0.860159, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-0.0512187, 0.212759, -0.153923, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, capsuleD.data[0].x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, capsuleD.data[0].y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, capsuleD.data[0].z));
	};

	SECTION("F structures near start.", "[ClosestPointToCapsuleF]")
	{
		capsuleD =
		{
			1.42372, 1.2671, 0.860159, 0.0,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-0.0512187, 0.212759, -0.153923, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, capsuleD.data[0].x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, capsuleD.data[0].y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, capsuleD.data[0].z));
	};

	SECTION("F structures near start.", "[ClosestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -2.20761442f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.967418790f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -1.50949669f));
	};

	SECTION("D structures near start.", "[ClosestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171, -0.627527, -1.2929, 0.5,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			-2.76445, -1.60703, -1.91709, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -2.2076144831822746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.96741879467421210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -1.5094966406613317));
	};

	SECTION("F structures near end.", "[ClosestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 1.64203799f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.70276952f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.972093523f));
	};

	SECTION("D structures near end.", "[ClosestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171, -0.627527, -1.2929, 0.5,
			1.42372, 1.2671, 0.860159, 0.0
		};

		pointD =
		{
			2.30043, 3.01664, 1.30966, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 1.6420378796261108));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.7027695636197442));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.97209351108099185));
	};

	SECTION("F structures clamp to capsule surface.", "[ClosestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.93341f, -1.42002f, 0.759768f, 0.5f,
			1.35794f, 1.0f, 2.93801f, 0.5f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.522357464f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.319830894f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 1.14967895f));
	};

	SECTION("D structures clamp to capsule surface.", "[ClosestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.93341, -1.42002, 0.759768, 0.5,
			1.35794, 1.0, 2.93801, 0.5
		};

		pointD =
		{
			-0.0512187, 0.212759, -0.153923, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.52235738981065427));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.31983089011039539));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 1.1496789471542903));
	};

	SECTION("F structures point on capsule surface.", "[ClosestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.93341f, -1.42002f, 0.759768f, 0.5f,
			1.35794f, 1.0f, 2.93801f, 0.5f
		};

		pointF =
		{
			-0.522357464f, -0.319830894f, 1.14967895f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleF(
			capsuleF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures point on capsule surface.", "[ClosestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.93341, -1.42002, 0.759768, 0.5,
			1.35794, 1.0, 2.93801, 0.5f
		};

		pointD =
		{
			-0.52235745529629152, -0.31983088416105238, 1.1496789259175513, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToCapsuleD(
			capsuleD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};
}

TEST_CASE("Closest point to AABB.", "[ClosestPointToAABBF], [ClosestPointToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GAABBMMF aabbF2 = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GAABBMMD aabbD2 = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToAABBF]")
	{
		aabbF = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, aabbF.center.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, aabbF.center.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, aabbF.center.z));
	};

	SECTION("D structures origin.", "[ClosestPointToAABBD]")
	{
		aabbD = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, aabbD.center.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, aabbD.center.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, aabbD.center.z));
	};

	SECTION("F structures query point is AABB center.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures query point is AABB center.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
		};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	// Top Vertices
	SECTION("F structures top far left vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			3.33115f, 2.3096f, 0.800545f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures top far left vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			3.33115f, 2.3096f, 0.800545f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures top far right vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			5.65776f, 2.3096f, 0.800545f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures top far right vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			5.65776f, 2.3096f, 0.800545f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures top near right vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			5.65776f, 2.3096f, -3.29774f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures top near right vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			5.65776f, 2.3096f, -3.29774f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	SECTION("F structures top near left vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			3.17468f, 2.3096f, -3.29774f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures top near left vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			3.17468f, 2.3096f, -3.29774f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	// Top Edges
	SECTION("F structures top far edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.32461f, 2.3096f, 0.953397f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures top far edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.32461f, 2.3096f, 0.953397f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures top left edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.66331f, 2.3096f, -0.899682f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures top left edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.66331f, 2.3096f, -0.899682f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	SECTION("F structures top right edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.12257f, 2.3096f, -0.899682f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures top right edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.12257f, 2.3096f, -0.899682f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	SECTION("F structures top near edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.29076f, 2.3096f, -3.35117f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures top near edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.29076f, 2.3096f, -3.35117f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	// Top Face
	SECTION("F structures top face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.29076f, 2.3096f, -1.19766f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 1.96605802f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures top face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.29076f, 2.3096f, -1.19766f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 1.9660580158233643));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	// Bottom Vertices
	SECTION("F structures bottom far left vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.77274f, -1.60518f, 2.5085f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures bottom far left vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.77274f, -1.60518f, 2.5085f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures bottom far right vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.13698f, -1.60518f, 2.5085f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures bottom far right vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.13698f, -1.60518f, 2.5085f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures bottom near right vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.13698f, -1.60518f, -2.99763f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures bottom near right vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.13698f, -1.60518f, -2.99763f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));

	};

	SECTION("F structures bottom near left vertex.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.83594f, -1.60518f, -2.99763f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures bottom near left vertex.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.83594f, -1.60518f, -2.99763f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	// Bottom Edges
	SECTION("F structures bottom far edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.21335f, -1.60518f, 2.19551f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures bottom far edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.21335f, -1.60518f, 2.19551f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures bottom right edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.38274f, -1.60518f, -1.06926f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures bottom right edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.38274f, -1.60518f, -1.06926f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	SECTION("F structures bottom near edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.40684f, -1.60518f, -3.60615f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures bottom near edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.40684f, -1.60518f, -3.60615f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	SECTION("F structures bottom left edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.07378f, -1.60518f, -0.666095f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures bottom left edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.07378f, -1.60518f, -0.666095f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	// Bottom Face
	SECTION("F structures bottom face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.32067f, -1.60518f, -0.666095f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, -0.552942038f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures bottom face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.32067f, -1.60518f, -0.666095f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, -0.55294203758239746));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	// Middle Edges
	SECTION("F structures far left edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.78805f, 0.501216f, 1.22653f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures far left edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.78805f, 0.501216f, 1.22653f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures far right edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.12052f, 0.501216f, 1.22653f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures far right edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.12052f, 0.501216f, 1.22653f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures near right edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.12052f, 0.501216f, -3.64089f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures near right edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.12052f, 0.501216f, -3.64089f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	SECTION("F structures near left edge.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.56217f, 0.501216f, -3.64089f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures near left edge.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.56217f, 0.501216f, -3.64089f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	// Middle Faces
	SECTION("F structures middle far face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.42069f, 0.501216f, 1.56862f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.489779949f));
	};

	SECTION("D structures middle far face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.42069f, 0.501216f, 1.56862f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.48977994918823242));
	};

	SECTION("F structures middle right face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			6.2128f, 0.501216f, -1.20501f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 5.37010479f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures middle right face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			6.2128f, 0.501216f, -1.20501f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 5.3701049685478210));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};

	SECTION("F structures middle near face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			4.22441f, 0.501216f, -3.80995f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, pointF.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, -2.66222000f));
	};

	SECTION("D structures middle near face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.22441f, 0.501216f, -3.80995f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	SECTION("F structures middle left face.", "[ClosestPointToAABBF]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			4.22441f, 0.501216f, -3.80995f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, pointD.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, -2.6622200012207031));
	};

	SECTION("F structures middle left face.", "[ClosestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			aabbF2);

		pointF =
		{
			2.32878f, 0.501216f, -0.938057f, 0.0f
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBF(
			aabbF2,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 3.47041488f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, pointF.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, pointF.z));
	};

	SECTION("D structures middle left face.", "[ClosestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			aabbD2);

		pointD =
		{
			2.32878f, 0.501216f, -0.938057f, 0.0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToAABBD(
			aabbD2,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 3.4704149365425110));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, pointD.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, pointD.z));
	};
}

TEST_CASE("Closest point to OBB.", "[ClosestPointToOBBF], [ClosestPointToOBBD], [Math]")
{
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GVECTORF cPointF = {};

	GW::MATH::GOBBD obbD = {};
	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GVECTORD cPointD = {};

	SECTION("F structures origin.", "[ClosestPointToOBBF]")
	{
		obbF = {};

		pointF = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToOBBF(
			obbF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, obbF.center.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, obbF.center.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, obbF.center.z));
	};

	SECTION("D structures origin.", "[ClosestPointToOBBD]")
	{
		obbD = {};

		pointD = {};

		CHECK(-(GW::I::GCollisionImplementation::ClosestPointToOBBD(
			obbD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, obbD.center.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, obbD.center.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, obbD.center.z));
	};

	SECTION("F structures top face.", "[ClosestPointToOBBF]")
	{
		obbF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f, 0.0f,
			0, -0.4871745f, 0, -0.8733046f
		};

		pointF =
		{
			0, 1.00973f, 0, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBF(
			obbF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.0f));
	};

	SECTION("D structures top face.", "[ClosestPointToOBBD]")
	{
		obbD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.5f, 0.5f, 0.5f, 0.0,
			0, -0.4871745f, 0, -0.8733046f
		};

		pointD =
		{
			0, 1.00973f, 0, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBD(
			obbD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.0));
	};

	SECTION("F structures top far vertex.", "[ClosestPointToOBBF]")
	{
		obbF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f, 0.0f,
			0, -0.4871745f, 0, -0.8733046f
		};

		pointF =
		{
			-0.425072f, 1.00973f, 2.32916f, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBF(
			obbF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.162790775f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.688112736f));
	};

	SECTION("D structures top far vertex.", "[ClosestPointToOBBD]")
	{
		obbD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.5f, 0.5f, 0.5f, 0.0,
			0, -0.4871745f, 0, -0.8733046f
		};

		pointD =
		{
			-0.425072f, 1.00973f, 2.32916f, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBD(
			obbD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.16279074821716222));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.5));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.68811273996709588));
	};

	SECTION("F structures far edge.", "[ClosestPointToOBBF]")
	{
		obbF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f, 0.0f,
			0, -0.4871745f, 0, -0.8733046f
		};

		pointF =
		{
			-0.425072f, 0.162642f, 2.32916f, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBF(
			obbF,
			pointF,
			cPointF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.x, -0.162790775f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.y, 0.162642002f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(cPointF.z, 0.688112736f));
	};

	SECTION("D structures far edge.", "[ClosestPointToOBBD]")
	{
		obbD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.5, 0.5, 0.5, 0.0,
			0, -0.4871745, 0, -0.8733046f
		};

		pointD =
		{
			-0.425072, 0.162642, 2.32916, 0
		};

		CHECK(+(GW::I::GCollisionImplementation::ClosestPointToOBBD(
			obbD,
			pointD,
			cPointD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.x, -0.16279072797461924));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.y, 0.16264200000000001));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(cPointD.z, 0.68811274107411924));
	};
}

// COMPUTE FROM POINTS

TEST_CASE("Compute sphere from points.", "[ComputeSphereFromPointsF], [ComputeSphereFromPointsD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};

	GW::MATH::GSPHERED sphereD = {};

	SECTION("F structures null failure", "[ComputeSphereFromPointsF]")
	{
		const unsigned int polygon_size = 5;
		GW::MATH::GVECTORF* convex = nullptr;
		sphereF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsF(
			convex,
			polygon_size,
			sphereF)));
	};

	SECTION("D structures null failure", "[ComputeSphereFromPointsD]")
	{
		const unsigned int polygon_size = 5;
		GW::MATH::GVECTORD* convex = nullptr;
		sphereD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsD(
			convex,
			polygon_size,
			sphereD)));
	};

	SECTION("F structures size failure", "[ComputeSphereFromPointsF]")
	{
		const unsigned int polygon_size = 0;
		GW::MATH::GVECTORF convex[] =
		{
			{},
			{},
			{},
		};
		sphereF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsF(
			convex,
			polygon_size,
			sphereF)));

		const unsigned int polygon_size2 = 1;
		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsF(
			convex,
			polygon_size2,
			sphereF)));
	};

	SECTION("D structures size failure", "[ComputeSphereFromPointsD]")
	{
		const unsigned int polygon_size = 0;
		GW::MATH::GVECTORD convex[] =
		{
			{},
			{},
			{},
		};
		sphereD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsD(
			convex,
			polygon_size,
			sphereD)));

		const unsigned int polygon_size2 = 1;
		CHECK(-(GW::I::GCollisionImplementation::ComputeSphereFromPointsD(
			convex,
			polygon_size2,
			sphereD)));
	};

	SECTION("F structures compute sphere", "[ComputeSphereFromPointsF]")
	{
		const unsigned int polygon_size = 8;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{1.08401f, -1.21947f, -2.89148f, 0.0f},
			{2.1402f, 2.99165f, 1.36817f, 0.0f},
			{-2.51072f, -3.15483f, -1.86752f, 0.0f},
			{4.89387f, 1.88006f, 0.0f, 0.0f},
			{-5.35546f, 0.488701f, -1.41136f, 0.0f},
			{1.46918f, 0.0f, 6.51787f, 0.0f},
			{1.79491f, 0.0f, -7.7501f, 0.0f},
			{-2.52023f, 1.03774f, 1.16184f, 0.0f},
		};

		sphereF = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputeSphereFromPointsF(
			convex,
			polygon_size,
			sphereF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(sphereF.x, 1.63204503f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(sphereF.y, 0.000000000f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(sphereF.z, -0.616115093f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(sphereF.radius, 7.13584375f));
	};

	SECTION("D structures compute sphere", "[ComputeSphereFromPointsD]")
	{
		const unsigned int polygon_size = 8;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{1.08401, -1.21947, -2.89148, 0.0},
			{2.1402, 2.99165, 1.36817, 0.0},
			{-2.51072, -3.15483, -1.86752, 0.0},
			{4.89387, 1.88006, 0.0, 0.0},
			{-5.35546, 0.488701, -1.41136, 0.0},
			{1.46918, 0.0, 6.51787, 0.0},
			{1.79491, 0.0, -7.7501, 0.0},
			{-2.52023, 1.03774, 1.16184, 0.0},
		};

		sphereD = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputeSphereFromPointsD(
			convex,
			polygon_size,
			sphereD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(sphereD.x, 1.6320450000000000));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(sphereD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(sphereD.z, -0.61611499999999975));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(sphereD.radius, 7.1358438175488397));
	};
}

TEST_CASE("Compute AABB from points.", "[ComputeAABBFromPointsF], [ComputeAABBFromPointsD], [Math]")
{
	GW::MATH::GAABBMMF mmF = {};

	GW::MATH::GAABBMMD mmD = {};

	SECTION("F structures null failure", "[ComputeAABBFromPointsF]")
	{
		const unsigned int polygon_size = 5;
		GW::MATH::GVECTORF* convex = nullptr;
		mmF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeAABBFromPointsF(
			convex,
			polygon_size,
			mmF)));
	};

	SECTION("D structures null failure", "[ComputeAABBFromPointsD]")
	{
		const unsigned int polygon_size = 5;
		GW::MATH::GVECTORD* convex = nullptr;
		mmD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeAABBFromPointsD(
			convex,
			polygon_size,
			mmD)));
	};

	SECTION("F structures size 0 failure", "[ComputeAABBFromPointsF]")
	{
		const unsigned int polygon_size = 0;
		GW::MATH::GVECTORF convex[] =
		{
			{},
			{},
			{},
		};
		mmF = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeAABBFromPointsF(
			convex,
			polygon_size,
			mmF)));
	};

	SECTION("D structures size 0 failure", "[ComputeAABBFromPointsD]")
	{
		const unsigned int polygon_size = 0;
		GW::MATH::GVECTORD convex[] =
		{
			{},
			{},
			{},
		};
		mmD = {};

		CHECK(-(GW::I::GCollisionImplementation::ComputeAABBFromPointsD(
			convex,
			polygon_size,
			mmD)));
	};

	SECTION("F structures compute AABBMM", "[ComputeAABBFromPointsF]")
	{
		const unsigned int polygon_size = 8;
		GW::MATH::GVECTORF convex[polygon_size] =
		{
			{1.08401f, -1.21947f, -2.89148f, 0.0f},
			{2.1402f, 2.99165f, 1.36817f, 0.0f},
			{-2.51072f, -3.15483f, -1.86752f, 0.0f},
			{4.89387f, 1.88006f, 0.0f, 0.0f},
			{-5.35546f, 0.488701f, -1.41136f, 0.0f},
			{1.46918f, 0.0f, 6.51787f, 0.0f},
			{1.79491f, 0.0f, -7.7501f, 0.0f},
			{-2.52023f, 1.03774f, 1.16184f, 0.0f},
		};

		mmF = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputeAABBFromPointsF(
			convex,
			polygon_size,
			mmF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.min.x, -5.35546017f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.min.y, -3.15482998f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.min.z, -7.75010014f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.max.x, 4.89386988f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.max.y, 2.99165010f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(mmF.max.z, 6.51786995f));
	};

	SECTION("D structures compute AABBMM", "[ComputeAABBFromPointsD]")
	{
		const unsigned int polygon_size = 8;
		GW::MATH::GVECTORD convex[polygon_size] =
		{
			{1.08401, -1.21947, -2.89148, 0.0},
			{2.1402, 2.99165, 1.36817, 0.0},
			{-2.51072, -3.15483, -1.86752, 0.0},
			{4.89387, 1.88006, 0.0, 0.0},
			{-5.35546, 0.488701, -1.41136, 0.0},
			{1.46918, 0.0, 6.51787, 0.0},
			{1.79491, 0.0, -7.7501, 0.0},
			{-2.52023, 1.03774, 1.16184, 0.0},
		};

		mmD = {};

		CHECK(+(GW::I::GCollisionImplementation::ComputeAABBFromPointsD(
			convex,
			polygon_size,
			mmD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.min.x, -5.3554599999999999));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.min.y, -3.1548300000000000));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.min.z, -7.7500999999999998));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.max.x, 4.8938699999999997));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.max.y, 2.9916499999999999));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(mmD.max.z, 6.5178700000000003));
	};
}

// TEST QUERY

TEST_CASE("Test point to line.", "[TestPointToLineF], [TestPointToLineD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GVECTORF pointF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GVECTORD pointD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToLineF]")
	{
		lineF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToLineD]")
	{
		lineD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate line no collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate line no collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures degenerate line collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures degenerate line collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point close to line start no collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point close to line start no collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on line start collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF = lineF.start;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on line start collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD = lineD.start;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point close to line end no collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point close to line end no collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on line end no collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF = lineF.end;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on line end no collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD = lineD.end;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point close to line no collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point close to line no collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on line interval collision.", "[TestPointToLineF]")
	{
		lineF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.144939303f, 0.376053095f, -0.152428865f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineF(
			pointF,
			lineF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on line interval collision.", "[TestPointToLineD]")
	{
		lineD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-0.144939303f, 0.376053095f, -0.152428865f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToLineD(
			pointD,
			lineD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to ray.", "[TestPointToRayF], [TestPointToRayD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GVECTORF pointF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GVECTORD pointD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToRayF]")
	{
		rayF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToRayD]")
	{
		rayD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate ray no collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f
		};

		pointF =
		{
			3.87624f, 1.37115f, 2.50055f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate ray no collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0,
			0.0, 0.0, 0.0, 0.0
		};

		pointD =
		{
			3.87624f, 1.37115f, 2.50055f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures degenerate ray collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f
		};

		pointF = rayF.position;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures degenerate ray collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0,
			0.0, 0.0, 0.0, 0.0
		};

		pointD = rayD.position;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point near ray position no collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.427926868f, 0.678688467f, 0.596875668f, 0.0f
		};

		pointF =
		{
			1.46936f, -2.78284f, -1.75898f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point near ray position no collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0,
			0.427926868f, 0.678688467f, 0.596875668f, 0.0
		};

		pointD =
		{
			1.46936f, -2.78284f, -1.75898f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on ray position collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
			0.427926868f, 0.678688467f, 0.596875668f, 0.0f
		};

		pointF = rayF.position;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on ray position collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0,
			0.427926868f, 0.678688467f, 0.596875668f, 0.0
		};

		pointD = rayD.position;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point near ray no collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			0.135887f, -1.18402f, -1.38998f, 0.0f,
			0.509439111f, 0.613975346f, 0.602914631f, 0.0f
		};

		pointF =
		{
			3.10004f, 2.59201f, 1.91623f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point near ray no collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			0.135887f, -1.18402f, -1.38998f, 0.0,
			0.509439111f, 0.613975346f, 0.602914631f, 0.0
		};

		pointD =
		{
			3.10004f, 2.59201f, 1.91623f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on ray interval collision.", "[TestPointToRayF]")
	{
		rayF =
		{
			0.135887f, -1.18402f, -1.38998f, 0.0f,
			0.509439111f, 0.613975346f, 0.602914631f, 0.0f
		};

		pointF =
		{
			3.10174322f, 2.39042616f, 2.12007284f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayF(
			pointF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on ray interval collision.", "[TestPointToRayD]")
	{
		rayD =
		{
			0.135887, -1.18402, -1.38998, 0.0,
			0.50943911467148784, 0.61397535042487250, 0.60291463534515866, 0.0
		};

		GW::I::GVectorImplementation::NormalizeD(
			rayD.direction,
			rayD.direction);

		pointD =
		{
			3.1017433154660958, 2.3904261274285279, 2.1200727765294820, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToRayD(
			pointD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to triangle.", "[TestPointToTriangleF], [TestPointToTriangleD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin degenerate triangle(point) failure.", "[TestPointToTriangleF]")
	{
		triangleF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures origin degenerate triangle(point) failure.", "[TestPointToTriangleD]")
	{
		triangleD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures near degenerate triangle(point) failure.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
		};

		pointF =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures near degenerate triangle(point) failure.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			-1.99876f, -1.93239f, -0.699708f, 0.0,
		};

		pointD =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures near degenerate triangle(line) failure.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
		};

		pointF =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures near degenerate triangle(line) failure.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
		};

		pointD =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures on degenerate triangle(line) failure .", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
		};

		pointF =
		{
			-0.808485985f, 0.283262253f, -0.975065231f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures on degenerate triangle(line) failure .", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
		};

		pointD =
		{
			-0.808485985f, 0.283262253f, -0.975065231f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures point on degenerate triangle(point) failure.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
		};

		pointF =
		{
			triangleF.a.x, triangleF.b.y, triangleF.c.z, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("D structures point on degenerate triangle(point) failure.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			-1.99876f, -1.93239f, -0.699708f, 0.0,
		};

		pointD =
		{
			triangleD.a.x, triangleD.b.y, triangleD.c.z, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(-(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT);
	};

	SECTION("F structures close to vertex a no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF =
		{
			-7.31277f, -4.50326f, -11.595f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures close to vertex a no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0,
			-2.18926f, 3.38054f, 3.51827f, 0.0,
			3.96233f, 0.435821f, 5.20674f, 0.0,
		};

		pointD =
		{
			-7.31277f, -4.50326f, -11.595f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures point is on vertex a collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF = triangleF.a;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point is on vertex a collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0,
			-2.18926f, 3.38054f, 3.51827f, 0.0,
			3.96233f, 0.435821f, 5.20674f, 0.0,
		};

		pointD = triangleD.a;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures close to vertex b no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF =
		{
			-7.08199f, 9.43563f, 6.66899f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures close to vertex b no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0,
			-2.18926f, 3.38054f, 3.51827f, 0.0,
			3.96233f, 0.435821f, 5.20674f, 0.0,
		};

		pointD =
		{
			-7.08199f, 9.43563f, 6.66899f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures vertex b collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0f,
			-2.18926f, 3.38054f, 3.51827f, 0.0f,
			3.96233f, 0.435821f, 5.20674f, 0.0f,
		};

		pointF = triangleF.b;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures vertex b collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.55362f, -1.29386f, -2.07792f, 0.0,
			-2.18926f, 3.38054f, 3.51827f, 0.0,
			3.96233f, 0.435821f, 5.20674f, 0.0,
		};

		pointD = triangleD.b;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures close to vertex c no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.12933f, -1.47871f, -1.91205f, 0.0f,
			-1.27235f, 2.27828f, 2.07457f, 0.0f,
			6.31262f, 1.12794f, 3.92865f, 0.0f,
		};

		pointF =
		{
			9.89622f, 1.23486f, 5.13347f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures close to vertex c no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.12933f, -1.47871f, -1.91205f, 0.0,
			-1.27235f, 2.27828f, 2.07457f, 0.0,
			6.31262f, 1.12794f, 3.92865f, 0.0,
		};

		pointD =
		{
			9.89622f, 1.23486f, 5.13347f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures vertex c collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-2.12933f, -1.47871f, -1.91205f, 0.0f,
			-1.27235f, 2.27828f, 2.07457f, 0.0f,
			6.31262f, 1.12794f, 3.92865f, 0.0f,
		};

		pointF = triangleF.c;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures vertex c collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-2.12933f, -1.47871f, -1.91205f, 0.0,
			-1.27235f, 2.27828f, 2.07457f, 0.0,
			6.31262f, 1.12794f, 3.92865f, 0.0,
		};

		pointD = triangleD.c;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures close to edge ab no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			-1.47693f, 0.19042f, -1.4358f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures close to edge ab no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			1.62814f, -0.535392f, 1.38604f, 0.0
		};

		pointD =
		{
			-1.47693f, 0.19042f, -1.4358f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures edge ab collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			-0.971077561f, -0.0193960667f, -0.937451422f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures edge ab collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			-0.97107751075720716, -0.019395932114651071, -0.93745148174165649, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures close to edge ac no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.347408f, -1.95765f, 0.888979f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures close to edge ac no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			1.62814f, -0.535392f, 1.38604f, 0.0
		};

		pointD =
		{
			0.347408f, -1.95765f, 0.888979f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures edge ac collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.198596954f, -1.08601880f, 0.563942194f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures edge ac collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointD =
		{
			0.198596954f, -1.08601880f, 0.563942194f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures close to edge bc no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			1.08696f, 1.36493f, -0.103414f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures close to edge bc no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			1.62814f, -0.535392f, 1.38604f, 0.0
		};

		pointD =
		{
			1.08696f, 1.36493f, -0.103414f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures edge bc collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.610254467f, 0.917297006f, -0.207013845f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures edge bc collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			0.61025455930951200, 0.91729696008212636, -0.20701379564476019, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures within triangle face no collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);

		triangleF =
		{
			-1.96893f, -0.839662f, -1.45804f, 0.0f,
			-1.28642f, 2.31981f, 2.38038f, 0.0f,
			2.18314f, -0.744747f, 1.6146f, 0.0f
		};

		pointF =
		{
			0.342186f, 0.825299f, 0.599023f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures within triangle face no collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0,
			0.0, 1.78823f, -1.1621f, 0.0,
			1.62814f, -0.535392f, 1.38604f, 0.0
		};

		pointD =
		{
			0.285477f, -0.1961f, -0.103414f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);

		triangleD =
		{
			-1.96893f, -0.839662f, -1.45804f, 0.0,
			-1.28642f, 2.31981f, 2.38038f, 0.0,
			2.18314f, -0.744747f, 1.6146f, 0.0
		};

		pointD =
		{
			0.342186f, 0.825299f, 0.599023f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures on triangle face collision.", "[TestPointToTriangleF]")
	{
		triangleF =
		{
			-1.99876f, -1.93239f, -0.699708f, 0.0f,
			0.0f, 1.78823f, -1.1621f, 0.0f,
			1.62814f, -0.535392f, 1.38604f, 0.0f
		};

		pointF =
		{
			0.183165312f, -0.124949217f, 0.0268400311f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures on triangle face collision.", "[TestPointToTriangleD]")
	{
		triangleD =
		{
			-1.99876, -1.93239, -0.699708, 0.0,
			0.0, 1.78823, -1.1621, 0.0,
			1.62814, -0.535392, 1.38604, 0.0
		};

		pointD =
		{
			0.18316533417455383, -0.12494923242087830, 0.026840002869380708, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures barycentric.", "[TestPointToTriangleF]")
	{
		GW::MATH::GVECTORF barycentricF = {};

		triangleF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
			1.73205080757f, -1.0f, 0.0f, 0.0f,
			-1.73205080757f, -1.0f, 0.0f, 0.0f
		};

		pointF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		pointF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			-1.0f, 1.73205080757f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, -1.73205080757f, 0.0f, 0.0f
		};

		pointF =
		{
			2.0f, 0.0f, 0.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			1.0f, 1.73205080757f, 0.0f, 0.0f,
			1.0f, -1.73205080757f, 0.0f, 0.0f,
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		pointF =
		{
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleF(
			pointF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures barycentric.", "[TestPointToTriangleD]")
	{
		GW::MATH::GVECTORD barycentricD = {};

		triangleD =
		{
			0.0, 2.0, 0.0, 0.0,
			1.73205080757, -1.0, 0.0, 0.0,
			-1.73205080757, -1.0, 0.0, 0.0
		};

		pointD =
		{
			0.0, 0.0, 0.0, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.33333333333333343));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		pointD =
		{
			0.0, 2.0, 0.0, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			-1.0, 1.73205080757, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
			-1.0, -1.73205080757, 0.0, 0.0
		};

		pointD =
		{
			2.0f, 0.0, 0.0, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			1.0, 1.73205080757, 0.0, 0.0,
			1.0, -1.73205080757, 0.0, 0.0,
			-2.0, 0.0, 0.0, 0.0,
		};

		pointD =
		{
			-2.0, 0.0, 0.0, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToTriangleD(
			pointD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to plane.", "[TestPointToPlaneF], [TestPointToPlaneD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GPLANEF planeF = {};

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GPLANED planeD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToPlaneF]")
	{
		planeF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneF(
			pointF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToPlaneD]")
	{
		planeD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneD(
			pointD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures below.", "[TestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			-1.52652f, -0.501646f, -1.48953f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneF(
			pointF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below.", "[TestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointD =
		{
			-1.52652f, -0.501646f, -1.48953f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneD(
			pointD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above.", "[TestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			1.07403f, 4.43582f, 2.29149f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneF(
			pointF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above.", "[TestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointD =
		{
			1.07403f, 4.43582f, 2.29149f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneD(
			pointD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestPointToPlaneF]")
	{
		planeF =
		{
			0.218946978f, 0.939839005f, 0.262230426f, 3.2749f
		};

		pointF =
		{
			-0.547565758f, 3.70055580f, -0.317047000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneF(
			pointF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		pointF =
		{
			0.695227504f, 2.80979466f, 1.83780241f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneF(
			pointF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPointToPlaneD]")
	{
		planeD =
		{
			0.218946978, 0.939839005, 0.262230426, 3.2748999595642094
		};

		pointD =
		{
			-0.54756564493848059, 3.7005562853212717, -0.31704686458744191, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneD(
			pointD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		pointD =
		{
			0.69522743109726792, 2.8097943524040940, 1.8378023230785427, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToPlaneD(
			pointD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to sphere.", "[TestPointToSphereF], [TestPointToSphereD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GSPHEREF sphereF = {};

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GSPHERED sphereD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToSphereF]")
	{
		sphereF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereF(
			pointF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToSphereD]")
	{
		sphereD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereD(
			pointD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate sphere.", "[TestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0f,
		};

		pointF = sphereF.data;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereF(
			pointF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures degenerate sphere.", "[TestPointToSphereD]")
	{
		sphereD =
		{
			2.21534f, -1.48171f, -0.921065f, 0.0,
		};

		pointD = sphereD.data;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereD(
			pointD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures center collision.", "[TestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			sphereF.x, sphereF.y, sphereF.z, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereF(
			pointF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures center collision.", "[TestPointToSphereD]")
	{
		sphereD =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointD =
		{
			sphereD.x, sphereD.y, sphereD.z, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereD(
			pointD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point near sphere no collision.", "[TestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			-5.21534f, -5.48171f, -0.921065f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereF(
			pointF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point near sphere no collision.", "[TestPointToSphereD]")
	{
		sphereD =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointD =
		{
			-5.21534f, -5.48171f, -0.921065f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereD(
			pointD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on sphere surface collision.", "[TestPointToSphereF]")
	{
		sphereF =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointF =
		{
			-2.18729615f, -3.85168743f, -0.921064973f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereF(
			pointF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on sphere surface collision.", "[TestPointToSphereD]")
	{
		sphereD =
		{
			2.21534f, -1.48171f, -0.921065f, 5.0f,
		};

		pointD =
		{
			-2.18729615f, -3.85168743f, -0.921064973f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToSphereD(
			pointD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to Capsule.", "[TestPointToCapsuleF], [TestPointToCapsuleD], [Math]")
{
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GVECTORF pointF = {};

	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GVECTORD pointD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToCapsuleF]")
	{
		capsuleF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToCapsuleD]")
	{
		capsuleD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate capsule.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures degenerate capsule.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			1.42372f, 1.2671f, 0.860159f, 0.0,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point is capsule.start collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			capsuleF.start_x, capsuleF.start_y, capsuleF.start_z, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point is capsule.start collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			capsuleF.start_x, capsuleF.start_y, capsuleF.start_z, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point is capsule.end collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			capsuleF.end_x, capsuleF.end_y, capsuleF.end_z, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point is capsule.end collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			capsuleF.end_x, capsuleF.end_y, capsuleF.end_z, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point near capsule.start no collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point near capsule.start no collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-2.76445f, -1.60703f, -1.91709f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures point on surface near capsule.start collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-2.20761442f, -0.967418790f, -1.50949669f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on surface near capsule.start collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-2.20761442f, -0.967418790f, -1.50949669f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

	};

	SECTION("F structures point near capsule.end no collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures point near capsule.end no collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			2.30043f, 3.01664f, 1.30966f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

	};

	SECTION("F structures point on surface near capsule.end collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			1.64203799f, 1.70276952f, 0.972093523f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on surface near capsule.end collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			1.64203799f, 1.70276952f, 0.972093523f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point inside capsule collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point inside capsule collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			-0.0512187f, 0.212759f, -0.153923f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures point on surface of capsule collision.", "[TestPointToCapsuleF]")
	{
		capsuleF =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0f
		};

		pointF =
		{
			0.103942066f, -0.0575853586f, -0.156396657f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleF(
			pointF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures point on surface of capsule collision.", "[TestPointToCapsuleD]")
	{
		capsuleD =
		{
			-1.91171f, -0.627527f, -1.2929f, 0.5f,
			1.42372f, 1.2671f, 0.860159f, 0.0
		};

		pointD =
		{
			0.103942066f, -0.0575853586f, -0.156396657f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToCapsuleD(
			pointD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to AABB.", "[TestPointToAABBF], [TestPointToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GVECTORF pointF = {};

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GVECTORD pointD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToAABBF]")
	{
		aabbF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToAABBD]")
	{
		aabbD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures query point is AABB center collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF = aabbF.center;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures query point is AABB center collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD = aabbD.center;

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Top Vertices
	SECTION("F structures top far left vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.33115f, 2.3096f, 0.800545f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top far left vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.33115f, 2.3096f, 0.800545f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top far left vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 1.96605802f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top far left vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 1.9660580158233643, 0.48977994918823242, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures top far right vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.65776f, 2.3096f, 0.800545f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top far right vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.65776f, 2.3096f, 0.800545f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top far right vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 1.96605802f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top far right vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 1.96605802f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures top near right vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.65776f, 2.3096f, -3.29774f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top near right vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.65776f, 2.3096f, -3.29774f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top near right vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 1.96605802f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top near right vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 1.96605802f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

	};

	SECTION("F structures top near left vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.17468f, 2.3096f, -3.29774f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top near left vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.17468f, 2.3096f, -3.29774f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top near left vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 1.96605802f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top near left vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 1.9660580158233643, -2.6622200012207031, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Top Edges
	SECTION("F structures top far edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.32461f, 2.3096f, 0.953397f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top far edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.32461f, 2.3096f, 0.953397f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top far edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.32461f, 1.96605802f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top far edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.32461f, 1.96605802f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures top left edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.66331f, 2.3096f, -0.899682f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top left edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.66331f, 2.3096f, -0.899682f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top left edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 1.96605802f, -0.899682f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top left edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 1.9660580158233643, -0.899682, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures top right edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.12257f, 2.3096f, -0.899682f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top right edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.12257f, 2.3096f, -0.899682f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

	};
	SECTION("F structures top right edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 1.96605802f, -0.899682f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top right edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 1.96605802f, -0.899682f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

	};

	SECTION("F structures top near edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.29076f, 2.3096f, -3.35117f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top near edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.29076f, 2.3096f, -3.35117f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top near edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.29076f, 1.96605802f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top near edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.29076f, 1.96605802f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Top Face
	SECTION("F structures top face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.29076f, 2.3096f, -1.19766f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures top face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.29076f, 2.3096f, -1.19766f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures top face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.29076f, 1.96605802f, -1.19766f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures top face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.29076f, 1.96605802f, -1.19766f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Bottom Vertices
	SECTION("F structures bottom far left vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.77274f, -1.60518f, 2.5085f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom far left vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.77274f, -1.60518f, 2.5085f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom far left vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, -0.552942038f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom far left vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, -0.55294203758239746, 0.48977994918823242, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom far right vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.13698f, -1.60518f, 2.5085f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom far right vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.13698f, -1.60518f, 2.5085f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom far right vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, -0.552942038f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom far right vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, -0.552942038f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom near right vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.13698f, -1.60518f, -2.99763f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom near right vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.13698f, -1.60518f, -2.99763f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom near right vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, -0.552942038f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom near right vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, -0.552942038f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom near left vertex no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.83594f, -1.60518f, -2.99763f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom near left vertex no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.83594f, -1.60518f, -2.99763f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom near left vertex collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, -0.552942038f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom near left vertex collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, -0.55294203758239746, -2.6622200012207031, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Bottom Edges
	SECTION("F structures bottom far edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.21335f, -1.60518f, 2.19551f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom far edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.21335f, -1.60518f, 2.19551f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom far edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.21335f, -0.552942038f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom far edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.21335f, -0.552942038f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom right edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.38274f, -1.60518f, -1.06926f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom right edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.38274f, -1.60518f, -1.06926f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom right edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, -0.552942038f, -1.06926f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom right edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, -0.552942038f, -1.06926f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom near edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.40684f, -1.60518f, -3.60615f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom near edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.40684f, -1.60518f, -3.60615f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom near edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.40684f, -0.552942038f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom near edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.40684f, -0.552942038f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures bottom left edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.07378f, -1.60518f, -0.666095f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom left edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.07378f, -1.60518f, -0.666095f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom left edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, -0.552942038f, -0.666095f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom left edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, -0.55294203758239746, -0.666095, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Bottom Face
	SECTION("F structures bottom face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.32067f, -1.60518f, -0.666095f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures bottom face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.32067f, -1.60518f, -0.666095f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures bottom face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.32067f, -0.552942038f, -0.666095f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures bottom face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.32067f, -0.552942038f, -0.666095f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Middle Edges
	SECTION("F structures far left edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.78805f, 0.501216f, 1.22653f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures far left edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.78805f, 0.501216f, 1.22653f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures far left edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 0.501216f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures far left edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 0.501216, 0.48977994918823242, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures far right edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.12052f, 0.501216f, 1.22653f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures far right edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.12052f, 0.501216f, 1.22653f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures far right edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 0.501216f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures far right edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 0.501216f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near right edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.12052f, 0.501216f, -3.64089f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near right edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.12052f, 0.501216f, -3.64089f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near right edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 0.501216f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near right edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 0.501216f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near left edge no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.56217f, 0.501216f, -3.64089f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near left edge no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.56217f, 0.501216f, -3.64089f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near left edge collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 0.501216f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near left edge collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 0.501216f, -2.6622200012207031, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	// Middle Faces
	SECTION("F structures middle far face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.42069f, 0.501216f, 1.56862f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures middle far face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.42069f, 0.501216f, 1.56862f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures middle far face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.42069f, 0.501216f, 0.489779949f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures middle far face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.42069f, 0.501216f, 0.489779949f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures middle right face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			6.2128f, 0.501216f, -1.20501f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures middle right face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			6.2128f, 0.501216f, -1.20501f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures middle right face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			5.37010479f, 0.501216f, -1.20501f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures middle right face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			5.37010479f, 0.501216f, -1.20501f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures middle near face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.22441f, 0.501216f, -3.80995f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures middle near face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.22441f, 0.501216f, -3.80995f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures middle near face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			4.22441f, 0.501216f, -2.66222000f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures middle near face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			4.22441f, 0.501216f, -2.66222000f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures middle left face no collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			2.32878f, 0.501216f, -0.938057f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures middle left face no collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			2.32878f, 0.501216f, -0.938057f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures middle left face collision.", "[TestPointToAABBF]")
	{
		aabbF =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0f,
			0.949845f, 1.2595f, 1.576f, 0.0f
		};

		pointF =
		{
			3.47041488f, 0.501216f, -0.938057f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBF(
			pointF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures middle left face collision.", "[TestPointToAABBD]")
	{
		aabbD =
		{
			4.42026f, 0.706558f, -1.08622f, 0.0,
			0.949845f, 1.2595f, 1.576f, 0.0
		};

		pointD =
		{
			3.4704149365425110, 0.501216, -0.938057, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToAABBD(
			pointD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test point to OBB.", "[TestPointToOBBF], [TestPointToOBBD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestPointToOBBF]")
	{
		obbF = {};

		pointF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestPointToOBBD]")
	{
		obbD = {};

		pointD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near center collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			0, 0.259692f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near center collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointD =
		{
			0, 0.259692f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near face collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			-0.547417f, 0.418933f, -0.0957679f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near face collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834, 0, 0.9238796
		};

		pointD =
		{
			-0.547417, 0.418933, -0.0957679, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

	};

	SECTION("F structures near vertex collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			-0.0713045f, -0.45729f, -0.0159291f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834, 0, 0.9238796
		};

		pointD =
		{
			-0.0713045, -0.45729, -0.0159291, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures above no collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			-0.0713045f, 0.661014f, -0.0159291f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures above no collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointD =
		{
			-0.0713045f, 0.661014f, -0.0159291f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

	};

	SECTION("F structures near edge no collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			-0.337221f, 0.513152f, 0.52603f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge no collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointD =
		{
			-0.337221f, 0.513152f, 0.52603f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near face no collision.", "[TestPointToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		pointF =
		{
			-0.337221f, 0.0441168f, 0.52603f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBF(
			pointF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near face no collision.", "[TestPointToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834, 0, 0.9238796
		};

		pointD =
		{
			-0.337221, 0.0441168, 0.52603, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPointToOBBD(
			pointD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test line to line.", "[TestLineToLineF], [TestLineToLineD], [Math]")
{
	GW::MATH::GLINEF lineF1 = {};
	GW::MATH::GLINEF lineF2 = {};

	GW::MATH::GLINED lineD1 = {};
	GW::MATH::GLINED lineD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestLineToLineF]")
	{
		lineF1 =
		{
			0, 0.549085f, 0, 0,
			0, 1.54908f, 0, 0
		};

		lineF2 =
		{
			-0.236693f, 0.257835f, 0, 0,
			-0.101009f, 1.25784f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineF(
			lineF1,
			lineF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToLineD]")
	{
		lineD1 =
		{
			0, 0.549085, 0, 0,
			0, 1.54908, 0, 0
		};

		lineD2 =
		{
			-0.236693, 0.257835, 0, 0,
			-0.101009, 1.25784, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineD(
			lineD1,
			lineD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToLineF]")
	{
		lineF1 =
		{
			0, 0.549085f, 0, 0,
			0, 1.54908f, 0, 0
		};

		lineF2 =
		{
			-0.236693f, 0.257835f, 0, 0,
			0.141922f, 1.25784f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineF(
			lineF1,
			lineF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToLineD]")
	{
		lineD1 =
		{
			0, 0.549085, 0, 0,
			0, 1.54908, 0, 0
		};

		lineD2 =
		{
			-0.236693, 0.257835, 0, 0,
			0.141922, 1.25784, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineD(
			lineD1,
			lineD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures parallel no collision.", "[TestLineToLineF]")
	{
		lineF1 =
		{
			-1.0f, 0.99f, 0, 0,
			1.0f, 0.99f, 0, 0
		};

		lineF2 =
		{
			-1.0f, 1.0f, 0, 0,
			1.0f, 1.0f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineF(
			lineF1,
			lineF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures parallel no collision.", "[TestLineToLineD]")
	{
		lineD1 =
		{
			-1.0f, 0.99f, 0, 0,
			1.0f, 0.99f, 0, 0
		};

		lineD2 =
		{
			-1.0f, 1.0f, 0, 0,
			1.0f, 1.0f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineD(
			lineD1,
			lineD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures parallel collision.", "[TestLineToLineF]")
	{
		lineF1 =
		{
			-1.0f, 1.0f, 0, 0,
			0.0f, 1.0f, 0, 0
		};

		lineF2 =
		{
			0.0f, 1.0f, 0, 0,
			1.0f, 1.0f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineF(
			lineF1,
			lineF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures parallel collision.", "[TestLineToLineD]")
	{
		lineD1 =
		{
			-1.0f, 1.0f, 0, 0,
			0.0, 1.0f, 0, 0
		};

		lineD2 =
		{
			0.0, 1.0f, 0, 0,
			1.0f, 1.0f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToLineD(
			lineD1,
			lineD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to ray.", "[TestLineToRayF], [TestLineToRayD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GRAYF rayF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GRAYD rayD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestLineToRayF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		rayF =
		{
			0, 0, -0.1f, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayF(
			lineF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToRayD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		rayD =
		{
			0, 0, -0.1f, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayD(
			lineD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures ray to line midpoint collision.", "[TestLineToRayF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		rayF =
		{
			0, 0.5f, -0.257382f, 0,
			0, 0, 1, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayF(
			lineF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures ray to line midpoint collision.", "[TestLineToRayD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		rayD =
		{
			0, 0.5, -0.257382, 0,
			0, 0, 1, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayD(
			lineD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line ends on ray.position collision.", "[TestLineToRayF]")
	{
		lineF =
		{
			0, -1, 0, 0,
			0, 0, 0, 0
		};

		rayF =
		{
			0, 0, 0, 0,
			0, 0, 1, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayF(
			lineF,
			rayF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line ends on ray.position collision.", "[TestLineToRayD]")
	{
		lineD =
		{
			0, -1, 0, 0,
			0, 0, 0, 0
		};

		rayD =
		{
			0, 0, 0, 0,
			0, 0, 1, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToRayD(
			lineD,
			rayD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to triangle.", "[TestLineToTriangleF], [TestLineToTriangleD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestLineToTriangleF]")
	{
		lineF = {};

		triangleF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures origin.", "[TestLineToTriangleD]")
	{
		lineD = {};

		triangleD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures no collision.", "[TestLineToTriangleF]")
	{
		lineF =
		{
			0, 0.2543f, 0.124146f, 0,
			0, 1.2543f, 0.124146f, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToTriangleD]")
	{
		lineD =
		{
			0, 0.2543f, 0.124146f, 0,
			0, 1.2543f, 0.124146f, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures coplanar no collision.", "[TestLineToTriangleF]")
	{
		lineF =
		{
			-0.774591f, 0, 0, 0,
			-0.774591f, 1, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures coplanar no collision.", "[TestLineToTriangleD]")
	{
		lineD =
		{
			-0.774591f, 0, 0, 0,
			-0.774591f, 1, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures coplanar no collision.", "[TestLineToTriangleF]")
	{
		lineF =
		{
			0, 0.2543f, 0, 0,
			0, 1.2543f, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures coplanar no collision.", "[TestLineToTriangleD]")
	{
		lineD =
		{
			0, 0.2543f, 0, 0,
			0, 1.2543f, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToTriangleF]")
	{
		lineF =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0.038277f, 0.572692f, -0.565302f, 0,
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		GW::MATH::GPLANEF plane = {};

		GW::MATH::GCollision::ComputePlaneF(
			triangleF.a,
			triangleF.b,
			triangleF.c,
			plane);
		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToTriangleD]")
	{
		lineD =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0.038277f, 0.572692f, -0.565302f, 0,
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line intersects vertex triangle.a collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		lineF =
		{
			-0.5f, 0, 0.5f, 0,
			-0.5f, 0, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			-0.5f, 0.5f, 0.5f, 0,
			-0.5f, -0.5f, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line intersects vertex triangle.a collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		lineD =
		{
			-0.5f, 0, 0.5f, 0,
			-0.5f, 0, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			-0.5f, 0.5f, 0.5f, 0,
			-0.5f, -0.5f, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line near vertex triangle.a no collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		lineF =
		{
			-0.51f, 0, 0.5f, 0,
			-0.51f, 0, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		lineF =
		{
			-0.51f, 0.5f, 0.5f, 0,
			-0.51f, -0.5f, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures line near vertex triangle.a no collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		lineD =
		{
			-0.51f, 0, 0.5f, 0,
			-0.51f, 0, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		lineD =
		{
			-0.51f, 0.5f, 0.5f, 0,
			-0.51f, -0.5f, -0.5f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures line intersects vertex triangle.b collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 0, 1.0f, 0,
			0.5f, 0, 0, 0
		};

		lineF =
		{
			0, -0.5f, 1.0f, 0,
			0, 0.5f, 1.0f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			0.5f, -0.5f, 1.0f, 0,
			-0.5f, 0.5f, 1.0f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line intersects vertex triangle.b collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 0, 1.0f, 0,
			0.5f, 0, 0, 0
		};

		lineD =
		{
			0, -0.5f, 1.0f, 0,
			0, 0.5f, 1.0f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			0.5f, -0.5f, 1.0f, 0,
			-0.5f, 0.5f, 1.0f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line near vertex triangle.b no collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 0, 1.0f, 0,
			0.5f, 0, 0, 0
		};

		lineF =
		{
			0, -0.5f, 1.000001f, 0,
			0, 0.5f, 1.000001f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		lineF =
		{
			0.5f, -0.5f, 1.000001f, 0,
			-0.5f, 0.5f, 1.000001f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures line near vertex triangle.b no collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 0, 1.0f, 0,
			0.5f, 0, 0, 0
		};

		lineD =
		{
			0, -0.5f, 1.000001f, 0,
			0, 0.5f, 1.000001f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		lineD =
		{
			0.5f, -0.5f, 1.000001f, 0,
			-0.5f, 0.5f, 1.000001f, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures line intersects vertex triangle.c collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			0.5f, 0, 0, 0,
			-0.5f, 0, 0, 0,
			0, 0, 1, 0,
		};

		lineF =
		{
			0, -0.5f, 1, 0,
			0, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			0.5f, -0.5f, 1, 0,
			-0.5f, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line intersects vertex triangle.c collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			0.5f, 0, 0, 0,
			-0.5f, 0, 0, 0,
			0, 0, 1, 0,
		};

		lineD =
		{
			0, -0.5f, 1, 0,
			0, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			0.5f, -0.5f, 1, 0,
			-0.5f, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line intersects vertex triangle.c no collision.", "[TestLineToTriangleF]")
	{
		triangleF =
		{
			0.5f, 0, 0, 0,
			-0.5f, 0, 0, 0,
			0, 0, 1, 0,
		};

		lineF =
		{
			0, -0.5f, 1, 0,
			0, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			0.5f, -0.5f, 1, 0,
			-0.5f, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line intersects vertex triangle.c no collision.", "[TestLineToTriangleD]")
	{
		triangleD =
		{
			0.5f, 0, 0, 0,
			-0.5f, 0, 0, 0,
			0, 0, 1, 0,
		};

		lineD =
		{
			0, -0.5f, 1, 0,
			0, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			0.5f, -0.5f, 1, 0,
			-0.5f, 0.5f, 1, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures barycentric.", "[TestLineToTriangleF]")
	{
		GW::MATH::GVECTORF barycentricF = {};

		triangleF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
			1.73205080757f, -1.0f, 0.0f, 0.0f,
			-1.73205080757f, -1.0f, 0.0f, 0.0f
		};

		lineF =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 2.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			-1.0f, 1.73205080757f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, -1.73205080757f, 0.0f, 0.0f
		};

		lineF =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			1.0f, 1.73205080757f, 0.0f, 0.0f,
			1.0f, -1.73205080757f, 0.0f, 0.0f,
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		lineF =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			-2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures barycentric.", "[TestLineToTriangleD]")
	{
		GW::MATH::GVECTORD barycentricD = {};

		triangleD =
		{
			0.0, 2.0, 0.0, 0.0,
			1.73205080757, -1.0, 0.0, 0.0,
			-1.73205080757, -1.0, 0.0, 0.0
		};

		lineD =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.33333333333333343));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 2.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			-1.0, 1.73205080757, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
			-1.0, -1.73205080757, 0.0, 0.0
		};

		lineD =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			1.0, 1.73205080757, 0.0, 0.0,
			1.0, -1.73205080757, 0.0, 0.0,
			-2.0, 0.0, 0.0, 0.0,
		};

		lineD =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			-2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to plane.", "[TestLineToPlaneF], [TestLineToPlaneD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GPLANEF planeF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GPLANED planeD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures origin.", "[TestLineToPlaneF]")
	{
		planeF = {};

		lineF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures origin.", "[TestLineToPlaneD]")
	{
		planeD = {};

		lineD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate plane.", "[TestLineToPlaneF]")
	{
		planeF = {};

		lineF =
		{
			-1, 0, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures degenerate plane.", "[TestLineToPlaneD]")
	{
		planeD = {};

		lineD =
		{
			-1, 0, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures degenerate line.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0,1,0,1
		};

		lineF = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures degenerate line.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0,1,0,1
		};

		lineD = {};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures intersect.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineF =
		{
			-0.701249f, 0.957271f, -1.65821f, 0.0f,
			-0.675908f, 2.72622f, -0.323366f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures intersect.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineD =
		{
			-0.701249f, 0.957271f, -1.65821f, 0.0,
			-0.675908f, 2.72622f, -0.323366f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures above.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineF =
		{
			0.317578f, 1.95881f, 1.89375f, 0.0f,
			0.342918f, 3.72776f, 3.22859f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineD =
		{
			0.317578f, 1.95881f, 1.89375f, 0.0,
			0.342918f, 3.72776f, 3.22859f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures below.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineF =
		{
			-0.073782f, -1.7182f, -1.01143f, 0.0f,
			-0.0484416f, 0.0507505f, 0.323422f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0.377529f, 1.38226f, 0.395688f, 1.45431f
		};

		lineD =
		{
			-0.073782f, -1.7182f, -1.01143f, 0.0,
			-0.0484416f, 0.0507505f, 0.323422f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above parallel to plane no collision.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 1.01f, 0, 0,
			1, 1.01f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures above parallel to plane no collision.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 1.01f, 0, 0,
			1, 1.01f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures below parallel to plane no collision.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 0.99f, 0, 0,
			1, 0.99f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures below parallel to plane no collision.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 0.99f, 0, 0,
			1, 0.99f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures parallel on plane.", "[TestLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 1, 0, 0,
			1, 1, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneF(
			lineF,
			planeF,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures parallel on plane.", "[TestLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 1, 0, 0,
			1, 1, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToPlaneD(
			lineD,
			planeD,
			collisionResult)));

		CHECK(collisionResult < GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to sphere.", "[TestLineToSphereF], [TestLineToSphereD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GSPHEREF sphereF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GSPHERED sphereD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestLineToSphereF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		sphereF =
		{
			0, 1.71246f, 0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereF(
			lineF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToSphereD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		sphereD =
		{
			0, 1.71246, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereD(
			lineD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToSphereF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		sphereF =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereF(
			lineF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToSphereD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		sphereD =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereD(
			lineD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures line near tangent no collision.", "[TestLineToSphereF]")
	{
		lineF =
		{
			1, 1.01f, 0, 0,
			-1, 1.01f, 0, 0
		};

		sphereF =
		{
			0, 0, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereF(
			lineF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures line near tangent no collision.", "[TestLineToSphereD]")
	{
		lineD =
		{
			1, 1.01f, 0, 0,
			-1, 1.01f, 0, 0
		};

		sphereD =
		{
			0, 0, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereD(
			lineD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures line near tangent collision.", "[TestLineToSphereF]")
	{
		lineF =
		{
			1, 1, 0, 0,
			-1, 1, 0, 0
		};

		sphereF =
		{
			0, 0, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereF(
			lineF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures line near tangent collision.", "[TestLineToSphereD]")
	{
		lineD =
		{
			1, 1, 0, 0,
			-1, 1, 0, 0
		};

		sphereD =
		{
			0, 0, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToSphereD(
			lineD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to capsule.", "[TestLineToCapsuleF], [TestLineToCapsuleD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GCAPSULEF capsuleF = {};

	GW::MATH::GLINED lineD = {};
	GW::MATH::GCAPSULED capsuleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestLineToLineF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleF =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToLineD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleD =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestLineToLineF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleF =
		{
			0, 0, 0.721952f, 0.5f,
			0, 1, 0.721952f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToLineD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleD =
		{
			0, 0, 0.721952, 0.5,
			0, 1, 0.721952, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToLineF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleF =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToLineD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleD =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToLineF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleF =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToLineD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		capsuleD =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test line to AABB.", "[TestLineToAABBF], [TestLineToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GLINEF lineF = {};

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GLINED lineD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBF(
			lineF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBD(
			lineD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures center collision.", "[TestLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
		};

		lineF =
		{
			0, -0.205249f, 5, 0,
			0, 0.253327f, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBF(
			lineF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures center collision.", "[TestLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
		};

		lineD =
		{
			0, -0.205249, 5, 0,
			0, 0.253327, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBD(
			lineD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 0.81935f, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBF(
			lineF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 0.81935, 0,
			0.5, 0.5, 0.5, 0
		};

		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToAABBD(
			lineD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test line to OBB.", "[TestLineToOBBF], [TestLineToOBBD], [Math]")
{
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GLINEF lineF = {};

	GW::MATH::GOBBD obbD = {};
	GW::MATH::GLINED lineD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestLineToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineF =
		{
			-0.457369f, -0.205249f, 4.64187f, 0,
			-0.457369f, 0.253327f, 4.64187f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBF(
			lineF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestLineToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineD =
		{
			-0.457369, -0.205249, 4.64187, 0,
			-0.457369, 0.253327, 4.64187, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBD(
			lineD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures center collision.", "[TestLineToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineF =
		{
			0, -0.205249f, 5, 0,
			0, 0.253327f, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBF(
			lineF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures center collision.", "[TestLineToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineD =
		{
			0, -0.205249, 5, 0,
			0, 0.253327, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBD(
			lineD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures collision.", "[TestLineToOBBF]")
	{
		obbF =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineF =
		{
			-0.5f, 0, 0, 0,
			-0.5f, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBF(
			lineF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestLineToOBBD]")
	{
		obbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineD =
		{
			-0.5f, 0, 0, 0,
			-0.5f, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestLineToOBBD(
			lineD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test ray to triangle.", "[TestRayToTriangleF], [TestRayToTriangleD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			0.0f, 0.0f, -3.1861f, 0.0f,
			-0.197843194f , 0.0540606007f , 0.978741825f
		};

		triangleF =
		{
			-1.94704f, -1.56496f, -0.671503f, 0.0f,
			-1.50444f, 2.5805f, 2.24619f, 0.0f,
			2.67279f, 0.872922f, 1.75741f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			0.0, 0.0, -3.1861f, 0.0,
			-0.197843194f , 0.0540606007f , 0.978741825f
		};

		triangleD =
		{
			-1.94704f, -1.56496f, -0.671503f, 0.0,
			-1.50444f, 2.5805f, 2.24619f, 0.0,
			2.67279f, 0.872922f, 1.75741f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-2.67696f, 0.0f, -3.1861f, 0.0f,
			-0.197843194f , 0.0540606007f , 0.978741825f
		};

		triangleF =
		{
			-1.94704f, -1.56496f, -0.671503f, 0.0f,
			-1.50444f, 2.5805f, 2.24619f, 0.0f,
			2.67279f, 0.872922f, 1.75741f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-2.67696f, 0.0, -3.1861f, 0.0,
			-0.197843194f , 0.0540606007f , 0.978741825f
		};

		triangleD =
		{
			-1.94704f, -1.56496f, -0.671503f, 0.0,
			-1.50444f, 2.5805f, 2.24619f, 0.0,
			2.67279f, 0.872922f, 1.75741f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.745917f, 0.702175f, 2.06857f, 0.0f,
			0.0f , 0.529822707f , -0.848108411f
		};

		triangleF =
		{
			-1.14802f, 0.914735f, 1.18031f, 0.0f,
			-0.648021f, 1.91474f, 1.18031f, 0.0f,
			-0.148021f, 0.914735f, 1.18031f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.745917f, 0.702175f, 2.06857f, 0.0,
			0.0 , 0.529822707f , -0.848108411f
		};

		triangleD =
		{
			-1.14802f, 0.914735f, 1.18031f, 0.0,
			-0.648021f, 1.91474f, 1.18031f, 0.0,
			-0.148021f, 0.914735f, 1.18031f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures center above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.27237f, 0.0f, -0.922807f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures center above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.27237f, 0.0, -0.922807f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex a above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.17737f, -0.613326f, -1.60801f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex a above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.17737f, -0.613326f, -1.60801f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex b above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.627824f, 1.34556f, -1.60801f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex b above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.627824f, 1.34556f, -1.60801f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex c above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.39758f, -1.40705f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex c above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.39758f, -1.40705f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge ab above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.10949f, -0.230349f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge ab above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.10949f, -0.230349f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge bc above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			0.538646f, -0.230349f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge bc above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			0.538646f, -0.230349f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge ac above collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.144579f, -1.08426f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge ac above collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.144579f, -1.08426f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures center below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.212212f, -0.265951f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures center below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.212212f, -0.265951f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex a below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.26962f, -0.925388f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex a below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.26962f, -0.925388f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex b below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.581147f, 1.35348f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex b below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.581147f, 1.35348f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near vertex c below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.22655f, -1.31227f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near vertex c below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.22655f, -1.31227f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge ab below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.89804f, 0.561166f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge ab below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.89804f, 0.561166f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge bc below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.074286f, 0.561166f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge bc below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.074286f, 0.561166f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near edge ac below collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.104702f, -1.26413f, 2.86861f, 0.0f,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures near edge ac below collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.104702f, -1.26413f, 2.86861f, 0.0,
			0.0483525582f , 0.0520105772f , -0.997475266f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures near parallel above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.99261f, -0.219349f, 1.09699f, 0.0f,
			-0.832961440f , -0.0612479672f , -0.549930811f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near parallel above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.99261f, -0.219349f, 1.09699f, 0.0,
			-0.832961440f , -0.0612479672f , -0.549930811f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex a above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.50181f, -0.919279f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex a above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.50181f, -0.919279f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex b above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.672307f, 1.66971f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex b above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.672307f, 1.66971f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex c above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			2.30732f, -2.21499f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex c above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			2.30732f, -2.21499f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge ab above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.18154f, 0.0f, -1.58827f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge ab above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.18154f, 0.0, -1.58827f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge bc above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.23195f, -0.402624f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge bc above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.23195f, -0.402624f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge ac above no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.141987f, -1.29771f, -1.96348f, 0.0f,
			0.0f , 0.0604734384f , 0.998169780f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge ac above no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.141987f, -1.29771f, -1.96348f, 0.0,
			0.0 , 0.0604734384f , 0.998169780f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near parallel below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-1.77855f, -0.985311f, -1.39543f, 0.0f,
			0.631963909f , 0.169239372f , 0.756293356f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near parallel below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-1.77855f, -0.985311f, -1.39543f, 0.0,
			0.631963909f , 0.169239372f , 0.756293356f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex a below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-2.37335f, -0.830784f, 2.6839f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex a below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-2.37335f, -0.830784f, 2.6839f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex b below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.70386f, 1.72213f, 2.33371f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex b below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.70386f, 1.72213f, 2.33371f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near vertex c below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.6753f, -1.95179f, 2.6839f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near vertex c below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.6753f, -1.95179f, 2.6839f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge ab below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			1.6753f, -1.95179f, 2.6839f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge ab below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			1.6753f, -1.95179f, 2.6839f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge bc below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			0.745796f, -0.299872f, 2.6839f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge bc below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			0.745796f, -0.299872f, 2.6839f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures near edge ac below no collision.", "[TestRayToTriangleF]")
	{
		rayF =
		{
			-0.338027f, -1.58219f, 2.6839f, 0.0f,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleF =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0f,
			-0.631833f, 1.71567f, 1.92767f, 0.0f,
			1.91193f, -1.73689f, 0.844192f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures near edge ac below no collision.", "[TestRayToTriangleD]")
	{
		rayD =
		{
			-0.338027f, -1.58219f, 2.6839f, 0.0,
			0.200500339f , 0.0536938123f , -0.978221118f
		};

		triangleD =
		{
			-1.32702f, -0.725717f, -1.12747f, 0.0,
			-0.631833f, 1.71567f, 1.92767f, 0.0,
			1.91193f, -1.73689f, 0.844192f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			nullptr)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures barycentric.", "[TestRayToTriangleF]")
	{
		GW::MATH::GVECTORF barycentricF = {};

		triangleF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
			1.73205080757f, -1.0f, 0.0f, 0.0f,
			-1.73205080757f, -1.0f, 0.0f, 0.0f
		};

		rayF =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		rayF =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			-1.0f, 1.73205080757f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, -1.73205080757f, 0.0f, 0.0f
		};

		rayF =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			1.0f, 1.73205080757f, 0.0f, 0.0f,
			1.0f, -1.73205080757f, 0.0f, 0.0f,
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		rayF =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			&barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures barycentric.", "[TestRayToTriangleD]")
	{
		GW::MATH::GVECTORD barycentricD = {};

		triangleD =
		{
			0.0, 2.0, 0.0, 0.0,
			1.73205080757, -1.0, 0.0, 0.0,
			-1.73205080757, -1.0, 0.0, 0.0
		};

		rayD =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.33333333333333343));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.33333333333333331));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		rayD =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			-1.0, 1.73205080757, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
			-1.0, -1.73205080757, 0.0, 0.0
		};

		rayD =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			1.0, 1.73205080757, 0.0, 0.0,
			1.0, -1.73205080757, 0.0, 0.0,
			-2.0, 0.0, 0.0, 0.0,
		};

		rayD =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			&barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test ray to plane.", "[TestRayToPlaneF], [TestRayToPlaneD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GPLANEF planeF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GPLANED planeD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures ray below plane collision.", "[TestRayToPlaneF]")
	{
		rayF =
		{
			0.526694f, 0.378306f, -1.26376f, 0.0f,
			-0.326165855f, 0.607673705f, 0.724119186f, 0.0f
		};

		planeF =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneF(
			rayF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures ray below plane collision.", "[TestRayToPlaneD]")
	{
		rayD =
		{
			0.526694f, 0.378306f, -1.26376f, 0.0,
			-0.326165855f, 0.607673705f, 0.724119186f, 0.0
		};

		planeD =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneD(
			rayD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures ray above plane no collision.", "[TestRayToPlaneF]")
	{
		rayF =
		{
			0.526694f, 2.36723f, 0.965984f, 0.0f,
			-0.313953966f, 0.584921956f, 0.747863054f, 0.0f
		};

		planeF =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneF(
			rayF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures ray above plane no collision.", "[TestRayToPlaneD]")
	{
		rayD =
		{
			0.526694f, 2.36723f, 0.965984f, 0.0,
			-0.313953966f, 0.584921956f, 0.747863054f, 0.0
		};

		planeD =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneD(
			rayD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures ray above plane collision.", "[TestRayToPlaneF]")
	{
		rayF =
		{
			0.526694f, 2.36723f, 0.965984f, 0.0f,
			0.450772673f, -0.113232732f, -0.885427773f, 0.0f
		};

		planeF =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneF(
			rayF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures ray above plane collision.", "[TestRayToPlaneD]")
	{
		rayD =
		{
			0.526694f, 2.36723f, 0.965984f, 0.0,
			0.450772673f, -0.113232732f, -0.885427773f, 0.0
		};

		planeD =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneD(
			rayD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures ray below plane no collision.", "[TestRayToPlaneF]")
	{
		rayF =
		{
			0.526694f, 0.785155f, 0.965984f, 0.0f,
			0.450772673f, -0.113232732f, -0.885427773f, 0.0f
		};

		planeF =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneF(
			rayF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures ray below plane no collision.", "[TestRayToPlaneD]")
	{
		rayD =
		{
			0.526694f, 0.785155f, 0.965984f, 0.0,
			0.450772673f, -0.113232732f, -0.885427773f, 0.0
		};

		planeD =
		{
			0.197758496f, 0.918518007f, 0.342368662f, 1.76336f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToPlaneD(
			rayD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};
}

TEST_CASE("Test ray to sphere.", "[TestRayToSphereF], [TestRayToSphereD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GSPHEREF sphereF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GSPHERED sphereD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures ray outside sphere collision.", "[TestRayToSphereF]")
	{
		rayF =
		{
			1.6482f, 0.614003f, 0.647923f, 0.0f,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		sphereF =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereF(
			rayF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures ray outside sphere collision.", "[TestRayToSphereD]")
	{
		rayD =
		{
			1.6482f, 0.614003f, 0.647923f, 0.0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		sphereD =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereD(
			rayD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures ray inside sphere collision.", "[TestRayToSphereF]")
	{
		rayF =
		{
			1.6482f, 1.40885f, 1.17601f, 0.0f,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		sphereF =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereF(
			rayF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures ray inside sphere collision.", "[TestRayToSphereD]")
	{
		rayD =
		{
			1.6482f, 1.40885f, 1.17601f, 0.0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		sphereD =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereD(
			rayD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures ray outside sphere no collision.", "[TestRayToSphereF]")
	{
		rayF =
		{
			1.70442f, 2.20371f, 1.46429f, 0.0f,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		sphereF =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereF(
			rayF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures ray outside sphere no collision.", "[TestRayToSphereD]")
	{
		rayD =
		{
			1.70442f, 2.20371f, 1.46429f, 0.0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		sphereD =
		{
			1.72295f, 1.58967f, 1.18045f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToSphereD(
			rayD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test ray to capsule.", "[TestRayToCapsuleF], [TestRayToCapsuleD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GCAPSULEF capsuleF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GCAPSULED capsuleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestRayToCapsuleF]")
	{
		rayF =
		{
			4.64784f, 1.30689f, 3.74273f, 0.0f,
			-0.578029394f, -0.442921102f, 0.685348749f, 0.0f
		};

		capsuleF =
		{
			1.99025f, 0.899176f, 3.78454f, 0.5f,
			5.88069f, -0.360054f, 6.65335f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestRayToCapsuleD]")
	{
		rayD =
		{
			4.64784f, 1.30689f, 3.74273f, 0.0,
			-0.578029394f, -0.442921102f, 0.685348749f, 0.0
		};

		capsuleD =
		{
			1.99025f, 0.899176f, 3.78454f, 0.5f,
			5.88069f, -0.360054f, 6.65335f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestRayToCapsuleF]")
	{
		rayF =
		{
			4.64784f, 1.30689f, 3.74273f, 0.0f,
			-0.578029394f, -0.442921102f, 0.685348749f, 0.0f
		};

		GW::I::GVectorImplementation::NormalizeF(
			rayF.direction,
			rayF.direction);

		capsuleF =
		{
			1.99025f, 0.899176f, 3.78454f, 0.5f,
			-2.92191f, 2.45169f, 6.65335f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestRayToCapsuleD]")
	{
		rayD =
		{
			4.64784f, 1.30689f, 3.74273f, 0.0,
			-0.578029394f, -0.442921102f, 0.685348749f, 0.0
		};

		capsuleD =
		{
			1.99025f, 0.899176f, 3.78454f, 0.5f,
			-2.92191f, 2.45169f, 6.65335f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test ray to AABB.", "[TestRayToAABBF], [TestRayToAABBD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GAABBCEF aabbF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GAABBCED aabbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestRayToAABBF]")
	{
		rayF =
		{
			0, 0, 0, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		aabbF =
		{
			0, -0.050899f, 1.22213f, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		CHECK(+(GW::I::GCollisionImplementation::TestRayToAABBF(
			rayF,
			mm,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestRayToAABBD]")
	{
		rayD =
		{
			0, 0, 0, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		aabbD =
		{
			0, -0.050899, 1.22213, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		CHECK(+(GW::I::GCollisionImplementation::TestRayToAABBD(
			rayD,
			mm,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestRayToAABBF]")
	{
		rayF =
		{
			0, 0, 0, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		aabbF =
		{
			0, 0.353694f, 1.22213f, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		CHECK(+(GW::I::GCollisionImplementation::TestRayToAABBF(
			rayF,
			mm,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestRayToAABBD]")
	{
		rayD =
		{
			0, 0, 0, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		aabbD =
		{
			0, 0.353694, 1.22213, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		CHECK(+(GW::I::GCollisionImplementation::TestRayToAABBD(
			rayD,
			mm,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test ray to OBB.", "[TestRayToOBBF], [TestRayToOBBD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestRayToOBBF]")
	{
		rayF =
		{
			-0.750187f, 0, -0.535239f, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToOBBF(
			rayF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestRayToOBBD]")
	{
		rayD =
		{
			-0.750187f, 0, -0.535239f, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToOBBD(
			rayD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestRayToOBBF]")
	{
		rayF =
		{
			0.0358795f, -0.231941f, -0.920343f, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToOBBF(
			rayF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestRayToOBBD]")
	{
		rayD =
		{
			0.0358795f, -0.231941f, -0.920343f, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestRayToOBBD(
			rayD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test triangle to triangle.", "[TestTriangleToTriangleF], [TestTriangleToTriangleD], [Math]")
{
	GW::MATH::GTRIANGLEF triangleF1 = {};
	GW::MATH::GTRIANGLEF triangleF2 = {};

	GW::MATH::GTRIANGLED triangleD1 = {};
	GW::MATH::GTRIANGLED triangleD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures triangle1 center to triangle2 vertex a collision.", "[TestTriangleToTriangleF]")
	{
		triangleF1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0f,
			-1.13363f, 1.34356f, 0.676236f, 0.0f,
			1.64404f, -0.513708f, 1.75248f, 0.0f,
		};

		triangleF2 =
		{
			-0.897173f, -0.348756f, 0.151848f, 0.0f,
			0.613975f, 1.692f, -1.08234f, 0.0f,
			0.946556f, -0.344862f, -1.52261f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures triangle1 center to triangle2 vertex a collision.", "[TestTriangleToTriangleD]")
	{
		triangleD1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0,
			-1.13363f, 1.34356f, 0.676236f, 0.0,
			1.64404f, -0.513708f, 1.75248f, 0.0,
		};

		triangleD2 =
		{
			-0.897173f, -0.348756f, 0.151848f, 0.0,
			0.613975f, 1.692f, -1.08234f, 0.0,
			0.946556f, -0.344862f, -1.52261f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures triangle1 edge bc to triangle2 edge ac collision.", "[TestTriangleToTriangleF]")
	{
		triangleF1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0f,
			-1.13363f, 1.34356f, 0.676236f, 0.0f,
			1.64404f, -0.513708f, 1.75248f, 0.0f,
		};

		triangleF2 =
		{
			-0.400088f, 0.577901f, 2.11203f, 0.0f,
			0.613975f, 1.692f, -1.08234f, 0.0f,
			0.946556f, -0.344862f, -1.52261f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures triangle1 edge bc to triangle2 edge ac collision.", "[TestTriangleToTriangleD]")
	{
		triangleD1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0,
			-1.13363f, 1.34356f, 0.676236f, 0.0,
			1.64404f, -0.513708f, 1.75248f, 0.0,
		};

		triangleD2 =
		{
			-0.400088f, 0.577901f, 2.11203f, 0.0,
			0.613975f, 1.692f, -1.08234f, 0.0,
			0.946556f, -0.344862f, -1.52261f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision1.", "[TestTriangleToTriangleF]")
	{
		triangleF1 =
		{
			-0.400088f, 0.577901f, 0.396328f, 0.0f,
			0.613975f, 1.692f, -1.08234f, 0.0f,
			0.946556f, -0.344862f, -1.52261f, 0.0f,
		};

		triangleF2 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0f,
			-1.13363f, 1.34356f, 0.676236f, 0.0f,
			1.64404f, -0.513708f, 1.75248f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		triangleF1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0f,
			-1.13363f, 1.34356f, 0.676236f, 0.0f,
			1.64404f, -0.513708f, 1.75248f, 0.0f,
		};

		triangleF2 =
		{
			-0.400088f, 0.577901f, 0.396328f, 0.0f,
			0.613975f, 1.692f, -1.08234f, 0.0f,
			0.946556f, -0.344862f, -1.52261f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		triangleF1 =
		{
			-0.323523998f, 1.68264794f, -1.20740700f, 0.0f,
			-0.478354007f, 1.68264794f, -1.15484905f, 0.0f,
			-0.465537012f, 1.80764794f, -1.12390494f, 0.0f,
		};

		triangleF2 =
		{
			1.00000000f, -0.682647943f, -0.999998987f, 0.0f,
			1.00000000f, -2.68264794f, -1.00000000f, 0.0f,
			-1.00000000f, -0.682647943f, -1.00000000f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision1.", "[TestTriangleToTriangleD]")
	{
		triangleD1 =
		{
			-0.400088f, 0.577901f, 0.396328f, 0.0,
			0.613975f, 1.692f, -1.08234f, 0.0,
			0.946556f, -0.344862f, -1.52261f, 0.0,
		};

		triangleD2 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0,
			-1.13363f, 1.34356f, 0.676236f, 0.0,
			1.64404f, -0.513708f, 1.75248f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		triangleD1 =
		{
			-1.67462f, -0.642262f, -1.69651f, 0.0,
			-1.13363f, 1.34356f, 0.676236f, 0.0,
			1.64404f, -0.513708f, 1.75248f, 0.0,
		};

		triangleD2 =
		{
			-0.400088f, 0.577901f, 0.396328f, 0.0,
			0.613975f, 1.692f, -1.08234f, 0.0,
			0.946556f, -0.344862f, -1.52261f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

		triangleD1 =
		{
			-0.323523998f, 1.68264794f, -1.20740700f, 0.0,
			-0.478354007f, 1.68264794f, -1.15484905f, 0.0,
			-0.465537012f, 1.80764794f, -1.12390494f, 0.0,
		};

		triangleD2 =
		{
			1.00000000f, -0.682647943f, -0.999998987f, 0.0,
			1.00000000f, -2.68264794f, -1.00000000f, 0.0,
			-1.00000000f, -0.682647943f, -1.00000000f, 0.0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures coplanar collision.", "[TestTriangleToTriangleF]")
	{
		triangleF1 =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		triangleF2 =
		{
			-0.802076f, 0.29972f, 0, 0,
			-0.302076f, 1.29972f, 0, 0,
			0.197924f, 0.29972f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleF(
			triangleF1,
			triangleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures coplanar collision.", "[TestTriangleToTriangleD]")
	{
		triangleD1 =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		triangleD2 =
		{
			-0.802076, 0.29972, 0, 0,
			-0.302076, 1.29972, 0, 0,
			0.197924, 0.29972, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToTriangleD(
			triangleD1,
			triangleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test triangle to plane.", "[TestTriangleToPlaneF], [TestTriangleToPlaneD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures below no collision.", "[TestTriangleToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 0.8f, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneF(
			triangleF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below no collision.", "[TestTriangleToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 0.8f, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneD(
			triangleD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above no collision.", "[TestTriangleToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		triangleF =
		{
			-0.5, 2, 0, 0,
			0, 3, 0, 0,
			0.5, 2, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneF(
			triangleF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above no collision.", "[TestTriangleToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		triangleD =
		{
			-0.5, 2, 0, 0,
			0, 3, 0, 0,
			0.5, 2, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneD(
			triangleD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestTriangleToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		triangleF =
		{
			-0.5f, 0.216839f, 0, 0,
			0, 1.21684f, 0, 0,
			0.5f, 0.216839f, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneF(
			triangleF,
			planeF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		triangleD =
		{
			-0.5, 0.216839, 0, 0,
			0, 1.21684, 0, 0,
			0.5, 0.216839, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToPlaneD(
			triangleD,
			planeD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test triangle to sphere.", "[TestTriangleToSphereF], [TestTriangleToSphereD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestTriangleToSphereF]")
	{
		sphereF =
		{
			-1.26634f, 1.26723f, -2.42847f, 0.0f,
		};

		triangleF =
		{
			-1.04209f, 0.343466f, 0.0f, 0.0f,
			0.0f, 1.77047f, 0.0f, 0.0f,
			1.8758f, 0.867616f, 0.0f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereF(
			triangleF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestTriangleToSphereD]")
	{
		sphereD =
		{
			-1.26634f, 1.26723f, -2.42847f, 0.0,
		};

		triangleD =
		{
			-1.04209f, 0.343466f, 0.0, 0.0,
			0.0, 1.77047f, 0.0, 0.0,
			1.8758f, 0.867616f, 0.0, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereD(
			triangleD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures no collision.", "[TestTriangleToSphereF]")
	{
		sphereF =
		{
			0, 1.55225f, 0, 0.5f
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereF(
			triangleF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestTriangleToSphereD]")
	{
		sphereD =
		{
			0, 1.55225, 0, 0.5f
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereD(
			triangleD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestTriangleToSphereF]")
	{
		sphereF =
		{
			0, 0, 0, 0.5f
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereF(
			triangleF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToSphereD]")
	{
		sphereD =
		{
			0, 0, 0, 0.5f
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToSphereD(
			triangleD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test triangle to capsule.", "[TestTriangleToCapsuleF], [TestTriangleToCapsuleD], [Math]")
{
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestTriangleToCapsuleF]")
	{
		capsuleF =
		{
			0, 1.62576f, 0, 0.5f,
			0, 2.62576f, 0, 0.0
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToCapsuleF(
			triangleF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestTriangleToCapsuleD]")
	{
		capsuleD =
		{
			0, 1.62576, 0, 0.5,
			0, 2.62576, 0, 0.0
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToCapsuleD(
			triangleD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestTriangleToCapsuleF]")
	{
		capsuleF =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0.0
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToCapsuleF(
			triangleF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToCapsuleD]")
	{
		capsuleD =
		{
			0, 0, 0, 0.5,
			0, 1, 0, 0.0
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToCapsuleD(
			triangleD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test triangle to AABB.", "[TestTriangleToAABBF], [TestTriangleToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestTriangleToAABBF]")
	{
		aabbF =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBF(
			triangleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToAABBD]")
	{
		aabbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBD(
			triangleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures collision.", "[TestTriangleToAABBF]")
	{
		aabbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
		};

		triangleF =
		{
			-1.47545f, 0, 4.91306f, 0,
			-0.975452f, 1, 4.91306f, 0,
			-0.475452f, 0, 4.91306f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBF(
			triangleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToAABBD]")
	{
		aabbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
		};

		triangleD =
		{
			-1.47545f, 0, 4.91306f, 0,
			-0.975452f, 1, 4.91306f, 0,
			-0.475452f, 0, 4.91306f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBD(
			triangleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestTriangleToAABBF]")
	{
		aabbF =
		{
			0, 1.61341f, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBF(
			triangleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestTriangleToAABBD]")
	{
		aabbD =
		{
			0, 1.61341, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToAABBD(
			triangleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test triangle to OBB.", "[TestTriangleToOBBF], [TestTriangleToOBBD], [Math]")
{
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GTRIANGLEF triangleF = {};

	GW::MATH::GOBBD obbD = {};
	GW::MATH::GTRIANGLED triangleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestTriangleToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		triangleF =
		{
			-1.47545f, 0, 4.91306f, 0,
			-0.975452f, 1, 4.91306f, 0,
			-0.525452f, 0, 4.91306f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToOBBF(
			triangleF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestTriangleToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		triangleD =
		{
			-1.47545f, 0, 4.91306f, 0,
			-0.975452f, 1, 4.91306f, 0,
			-0.525452f, 0, 4.91306f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToOBBD(
			triangleD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestTriangleToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		triangleF =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToOBBF(
			triangleF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestTriangleToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		triangleD =
		{
			-0.5, 0, 0, 0,
			0, 1, 0, 0,
			0.5, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestTriangleToOBBD(
			triangleD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test plane to plane.", "[TestPlaneToPlaneF], [TestPlaneToPlaneD], [Math]")
{
	GW::MATH::GPLANEF planeF1 = {};
	GW::MATH::GPLANEF planeF2 = {};

	GW::MATH::GPLANED planeD1 = {};
	GW::MATH::GPLANED planeD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures plane 1 below plane 2 no collision.", "[TestPlaneToPlaneF]")
	{
		planeF1 =
		{
			0, 1, 0, 1
		};

		planeF2 =
		{
			0, 1, 0, 1.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneF(
			planeF1,
			planeF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures plane 1 below plane 2 no collision.", "[TestPlaneToPlaneD]")
	{
		planeD1 =
		{
			0, 1, 0, 1
		};

		planeD2 =
		{
			0, 1, 0, 1.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneD(
			planeD1,
			planeD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures plane 1 above plane 2 no collision.", "[TestPlaneToPlaneF]")
	{
		planeF1 =
		{
			0, 1, 0, 1.5
		};

		planeF2 =
		{
			0, 1, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneF(
			planeF1,
			planeF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures plane 1 above plane 2 no collision.", "[TestPlaneToPlaneD]")
	{
		planeD1 =
		{
			0, 1, 0, 1.5
		};

		planeD2 =
		{
			0, 1, 0, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneD(
			planeD1,
			planeD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[TestPlaneToPlaneF]")
	{
		planeF1 =
		{
			0, 1, 0, 1
		};

		planeF2 =
		{
			0, 1, 0.407585f, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneF(
			planeF1,
			planeF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPlaneToPlaneD]")
	{
		planeD1 =
		{
			0, 1, 0, 1
		};

		planeD2 =
		{
			0, 1, 0.407585, 1
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToPlaneD(
			planeD1,
			planeD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test plane to sphere.", "[TestPlaneToSphereF], [TestPlaneToSphereD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GSPHEREF sphereF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GSPHERED sphereD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures below no collision.", "[TestPlaneToSphereF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		sphereF =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereF(
			planeF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below no collision.", "[TestPlaneToSphereD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		sphereD =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereD(
			planeD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above no collision.", "[TestPlaneToSphereF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		sphereF =
		{
			0, 2, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereF(
			planeF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above no collision.", "[TestPlaneToSphereD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		sphereD =
		{
			0, 2, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereD(
			planeD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures sphere on origin above plane no collision.", "[TestPlaneToSphereF]")
	{
		planeF =
		{
			0, -1, 0, -1
		};

		sphereF =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereF(
			planeF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures sphere on origin above plane no collision.", "[TestPlaneToSphereD]")
	{
		planeD =
		{
			0, -1, 0, -1
		};

		sphereD =
		{
			0, 0, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereD(
			planeD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestPlaneToSphereF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		sphereF =
		{
			0, 0.92606f, 0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereF(
			planeF,
			sphereF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPlaneToSphereD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		sphereD =
		{
			0, 0.92606, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToSphereD(
			planeD,
			sphereD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test plane to capsule.", "[TestPlaneToCapsuleF], [TestPlaneToCapsuleD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GCAPSULEF capsuleF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GCAPSULED capsuleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures below no collision.", "[TestPlaneToCapsuleF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		capsuleF =
		{
			0, -0.617969f, 0, 0.5f,
			0, 0.382031f, 0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleF(
			planeF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below no collision.", "[TestPlaneToCapsuleD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		capsuleD =
		{
			0, -0.617969, 0, 0.5,
			0, 0.382031, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleD(
			planeD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above no collision.", "[TestPlaneToCapsuleF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		capsuleF =
		{
			0, 2.617969f, 0, 0.5f,
			0, 3.382031f, 0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleF(
			planeF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above no collision.", "[TestPlaneToCapsuleD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		capsuleD =
		{
			0, 2.617969, 0, 0.5,
			0, 3.382031, 0, 0.5
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleD(
			planeD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestPlaneToCapsuleF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		capsuleF =
		{
			0, 0, 0, 0.5f,
			0, 1, 0, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleF(
			planeF,
			capsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPlaneToCapsuleD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		capsuleD =
		{
			0, 0, 0, 0.5f,
			0, 1, 0, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToCapsuleD(
			planeD,
			capsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test plane to AABB.", "[TestPlaneToAABBF], [TestPlaneToAABBD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GAABBCEF aabbF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GAABBCED aabbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures below no collision.", "[TestPlaneToAABBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		aabbF =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBF(
			planeF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below no collision.", "[TestPlaneToAABBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		aabbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBD(
			planeD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above no collision.", "[TestPlaneToAABBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		aabbF =
		{
			0, 2, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBF(
			planeF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above no collision.", "[TestPlaneToAABBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		aabbD =
		{
			0, 2, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBD(
			planeD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestPlaneToAABBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		aabbF =
		{
			0, 0.794161f, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBF(
			planeF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPlaneToAABBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		aabbD =
		{
			0, 0.794161, 0, 0,
			0.5, 0.5, 0.5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToAABBD(
			planeD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test plane to OBB.", "[TestPlaneToOBBF], [TestPlaneToOBBD], [Math]")
{
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GPLANED planeD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures below no collision.", "[TestPlaneToOBBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		obbF =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBF(
			planeF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below no collision.", "[TestPlaneToOBBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		obbD =
		{
			0, 0, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBD(
			planeD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above no collision.", "[TestPlaneToOBBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		obbF =
		{
			0, 2, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBF(
			planeF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("D structures above no collision.", "[TestPlaneToOBBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		obbD =
		{
			0, 2, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBD(
			planeD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::ABOVE);
	};

	SECTION("F structures collision.", "[TestPlaneToOBBF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		obbF =
		{
			0, 0.7f, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBF(
			planeF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestPlaneToOBBD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		obbD =
		{
			0, 0.7f, 0, 0,
			0.5, 0.5, 0.5, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestPlaneToOBBD(
			planeD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Test sphere to sphere.", "[TestSphereToSphereF], [TestSphereToSphereD], [Math]")
{
	GW::MATH::GSPHEREF sphereF1 = {};
	GW::MATH::GSPHEREF sphereF2 = {};

	GW::MATH::GSPHERED sphereD1 = {};
	GW::MATH::GSPHERED sphereD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestSphereToSphereF]")
	{
		sphereF1 =
		{
			2.15906f, 1.56701f, -3.73512f, 0.5f
		};

		sphereF2 =
		{
			1.77154f, 2.25537f, -3.38065f, 0.942544f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToSphereF(
			sphereF1,
			sphereF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestSphereToSphereD]")
	{
		sphereD1 =
		{
			2.15906f, 1.56701f, -3.73512f, 0.5f
		};

		sphereD2 =
		{
			1.77154f, 2.25537f, -3.38065f, 0.942544f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToSphereD(
			sphereD1,
			sphereD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestSphereToSphereF]")
	{
		sphereF1 =
		{
			-1.58468f, -2.42917f, -0.672674f, 0.743662f
		};

		sphereF2 =
		{
			1.77154f, 2.25537f, -2.10866f, -0.303412f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToSphereF(
			sphereF1,
			sphereF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestSphereToSphereD]")
	{
		sphereD1 =
		{
			-1.58468f, -2.42917f, -0.672674f, 0.743662f
		};

		sphereD2 =
		{
			1.77154f, 2.25537f, -2.10866f, -0.303412f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToSphereD(
			sphereD1,
			sphereD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test sphere to capsule.", "[TestSphereToCapsuleF], [TestSphereToCapsuleD], [Math]")
{
	GW::MATH::GSPHEREF SphereF = {};
	GW::MATH::GCAPSULEF CapsuleF = {};

	GW::MATH::GSPHERED SphereD = {};
	GW::MATH::GCAPSULED CapsuleD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestSphereToCapsuleF]")
	{
		SphereF =
		{
			-1.90111f, 1.71778f, -1.28587f, 0.88223f
		};

		CapsuleF =
		{
			-0.494471f, -0.438022f, -4.09955f, 0.5f,
			-2.59106f, 2.37294f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToCapsuleF(
			SphereF,
			CapsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestSphereToCapsuleD]")
	{
		SphereD =
		{
			-1.90111f, 1.71778f, -1.28587f, 0.88223f
		};

		CapsuleD =
		{
			-0.494471f, -0.438022f, -4.09955f, 0.5f,
			-2.59106f, 2.37294f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToCapsuleD(
			SphereD,
			CapsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestSphereToCapsuleF]")
	{
		SphereF =
		{
			2.15877f, -0.74062f, -1.28587f, 0.88223f
		};

		CapsuleF =
		{
			-5.65294f, -6.4059f, 0.906548f, 0.5f,
			-2.59106f, -2.93521f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToCapsuleF(
			SphereF,
			CapsuleF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestSphereToCapsuleD]")
	{
		SphereD =
		{
			2.15877f, -0.74062f, -1.28587f, 0.88223f
		};

		CapsuleD =
		{
			-5.65294f, -6.4059f, 0.906548f, 0.5f,
			-2.59106f, -2.93521f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToCapsuleD(
			SphereD,
			CapsuleD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test sphere to AABB.", "[TestSphereToAABBF], [TestSphereToAABBD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GAABBCEF aabbF = {};

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GAABBCED aabbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestSphereToAABBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		aabbF =
		{
			-1.33964f, 0.0f, 3.21097f, 0.0f,
			1.64764f, 1.60093f, 1.8925f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToAABBF(
			sphereF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestSphereToAABBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		aabbD =
		{
			-1.33964f, 0.0, 3.21097f, 0.0,
			1.64764f, 1.60093f, 1.8925f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToAABBD(
			sphereD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestSphereToAABBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		aabbF =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0f,
			0.921908f, 1.17511f, 1.8925f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToAABBF(
			sphereF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestSphereToAABBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		aabbD =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0,
			0.921908f, 1.17511f, 1.8925f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToAABBD(
			sphereD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test sphere to OBB.", "[TestSphereToOBBF], [TestSphereToOBBD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestSphereToOBBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		obbF =
		{
			-1.33964f, 0.0f, 3.21097f, 0.0f,
			1.64764f, 1.60093f, 1.8925f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToOBBF(
			sphereF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestSphereToOBBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		obbD =
		{
			-1.33964f, 0.0, 3.21097f, 0.0,
			1.64764f, 1.60093f, 1.8925f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToOBBD(
			sphereD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestSphereToOBBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		obbF =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0f,
			0.921908f, 1.17511f, 1.8925f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToOBBF(
			sphereF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestSphereToOBBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		obbD =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0,
			0.921908f, 1.17511f, 1.8925f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestSphereToOBBD(
			sphereD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test capsule to capsule.", "[TestCapsuleToCapsuleF], [TestCapsuleToCapsuleD], [Math]")
{
	GW::MATH::GCAPSULEF CapsuleF1 = {};
	GW::MATH::GCAPSULEF CapsuleF2 = {};

	GW::MATH::GCAPSULED CapsuleD1 = {};
	GW::MATH::GCAPSULED CapsuleD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures collision.", "[TestCapsuleToCapsuleF]")
	{
		CapsuleF1 =
		{
			2.41279f, 1.86444f, 0.0f, 0.5f,
			0.0f, 2.44932f, 0.978328f, 0.5f
		};

		CapsuleF2 =
		{
			0.647614f, 4.20551f, 2.41955f, 0.5f,
			0.0f, 1.0f, 0.0f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToCapsuleF(
			CapsuleF1,
			CapsuleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestCapsuleToCapsuleD]")
	{
		CapsuleD1 =
		{
			2.41279f, 1.86444f, 0.0, 0.5f,
			0.0, 2.44932f, 0.978328f, 0.5f
		};

		CapsuleD2 =
		{
			0.647614f, 4.20551f, 2.41955f, 0.5f,
			0.0, 1.0f, 0.0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToCapsuleD(
			CapsuleD1,
			CapsuleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestCapsuleToCapsuleF]")
	{
		CapsuleF1 =
		{
			5.49113f, 3.91621f, -2.22193f, 0.5f,
			2.2171f, 3.72393f, 0.978328f, 0.5f
		};

		CapsuleF2 =
		{
			-2.82114f, -0.915085f, 2.41955f, 0.5f,
			0.0f, 5.15133f, 0.0f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToCapsuleF(
			CapsuleF1,
			CapsuleF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestCapsuleToCapsuleD]")
	{
		CapsuleD1 =
		{
			5.49113f, 3.91621f, -2.22193f, 0.5f,
			2.2171f, 3.72393f, 0.978328f, 0.5f
		};

		CapsuleD2 =
		{
			-2.82114f, -0.915085f, 2.41955f, 0.5f,
			0.0, 5.15133f, 0.0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToCapsuleD(
			CapsuleD1,
			CapsuleD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test capsule to AABB.", "[TestCapsuleToAABBF], [TestCapsuleToAABBD], [Math]")
{
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GAABBCEF aabbF = {};

	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GAABBCED aabbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures collision.", "[TestCapsuleToAABBF]")
	{
		capsuleF =
		{
			0.0f, -2.0476f, 3.34535f, 0.5f,
			-3.77941f, 0.887068f, 3.81508f, 0.0f
		};

		aabbF =
		{
			-2.075f, -0.0144038f, 3.48151f, 1.10904f,
			1.70193f, 0.5f, 1.7403f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBF(
			capsuleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestCapsuleToAABBD]")
	{
		capsuleD =
		{
			0.0, -2.0476f, 3.34535f, 0.5f,
			-3.77941f, 0.887068f, 3.81508f, 0.0
		};

		aabbD =
		{
			-2.075f, -0.0144038f, 3.48151f, 1.10904f,
			1.70193f, 0.5f, 1.7403f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBD(
			capsuleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures capsule contained within AABB collision.", "[TestCapsuleToAABBF]")
	{
		capsuleF =
		{
			-1.0f, 0.0f, 0.0f, 0.5f,
			1.0f, 0.0f, 0.0f, 0.5f
		};

		aabbF =
		{
			0,0,0,0,
			10.0f, 10.0f, 10.0f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBF(
			capsuleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures capsule contained within AABB collision.", "[TestCapsuleToAABBD]")
	{
		capsuleD =
		{
			-1.0f, 0.0, 0.0, 0.5f,
			1.0f, 0.0, 0.0, 0.5f
		};

		aabbD =
		{
			0,0,0,0,
			10.0, 10.0, 10.0, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBD(
			capsuleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures AABB contained within capsule collision.", "[TestCapsuleToAABBF]")
	{
		capsuleF =
		{
			-10.0f, 0.0f, 0.0f, 10.0f,
			10.0f, 0.0f, 0.0f, 10.0f
		};

		aabbF =
		{
			0,0,0,0,
			2.0f, 2.0f, 2.0f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBF(
			capsuleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures AABB contained within capsule collision.", "[TestCapsuleToAABBD]")
	{
		capsuleD =
		{
			-10.0, 0.0, 0.0, 10.0,
			10.0, 0.0, 0.0, 10.0
		};

		aabbD =
		{
			0,0,0,0,
			2.0f, 2.0f, 2.0f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBD(
			capsuleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures capsule corner AABB collision.", "[TestCapsuleToAABBF]")
	{
		capsuleF =
		{
			-0.366748f, 9.82186f, 10.7977f, 0.5f,
			-0.366748f, 10.8219f, 9.83842f, 0.0f
		};

		aabbF =
		{
			0,0,0,0,
			10.0f, 10.0f, 10.0f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBF(
			capsuleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures capsule corner AABB collision.", "[TestCapsuleToAABBD]")
	{
		capsuleD =
		{
			-0.366748, 9.82186, 10.7977, 0.5f,
			-0.366748, 10.8219, 9.83842, 0.0
		};

		aabbD =
		{
			0,0,0,0,
			10.0, 10.0, 10.0, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBD(
			capsuleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestCapsuleToAABBF]")
	{
		capsuleF =
		{
			9.31304f, -4.4508f, 6.09145f, 0.5f,
			7.9674f, -3.22429f, 3.81508f, 0.0f
		};

		aabbF =
		{
			7.9261f, -0.0144038f, -1.23079f, 1.10904f,
			0.509203f, 0.824216f, 0.876336f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBF(
			capsuleF,
			aabbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestCapsuleToAABBD]")
	{
		capsuleD =
		{
			9.31304f, -4.4508f, 6.09145f, 0.5f,
			7.9674f, -3.22429f, 3.81508f, 0.0
		};

		aabbD =
		{
			7.9261f, -0.0144038f, -1.23079f, 1.10904f,
			0.509203f, 0.824216f, 0.876336f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToAABBD(
			capsuleD,
			aabbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test capsule to OBB.", "[TestCapsuleToOBBF], [TestCapsuleToOBBD], [Math]")
{
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures collision.", "[TestCapsuleToOBBF]")
	{
		capsuleF =
		{
			0.0f, -2.0476f, 3.34535f, 0.5f,
			-3.77941f, 0.887068f, 3.81508f, 0.5f
		};

		obbF =
		{
			-2.075f, -0.0144038f, 3.48151f, 1.10904f,
			1.70193f, 0.5f, 1.7403f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToOBBF(
			capsuleF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestCapsuleToOBBD]")
	{
		capsuleD =
		{
			0.0, -2.0476f, 3.34535f, 0.5f,
			-3.77941f, 0.887068f, 3.81508f, 0.5f
		};

		obbD =
		{
			-2.075f, -0.0144038f, 3.48151f, 1.10904f,
			1.70193f, 0.5f, 1.7403f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToOBBD(
			capsuleD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestCapsuleToOBBF]")
	{
		capsuleF =
		{
			9.31304f, -4.4508f, 6.09145f, 0.5f,
			7.9674f, -3.22429f, 3.81508f, 0.5f
		};

		obbF =
		{
			7.9261f, -0.0144038f, -1.23079f, 1.10904f,
			0.509203f, 0.824216f, 0.876336f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToOBBF(
			capsuleF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestCapsuleToOBBD]")
	{
		capsuleD =
		{
			9.31304f, -4.4508f, 6.09145f, 0.5f,
			7.9674f, -3.22429f, 3.81508f, 0.5f
		};

		obbD =
		{
			7.9261f, -0.0144038f, -1.23079f, 1.10904f,
			0.509203f, 0.824216f, 0.876336f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestCapsuleToOBBD(
			capsuleD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test AABB to AABB.", "[TestAABBToAABBF], [TestAABBToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF1 = {};
	GW::MATH::GAABBCEF aabbF2 = {};

	GW::MATH::GAABBCED aabbD1 = {};
	GW::MATH::GAABBCED aabbD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures collision.", "[TestAABBToAABBF]")
	{
		aabbF1 =
		{
			0.0f, -1.80447f, 0.0f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		aabbF2 =
		{
			0.0f, 1.39216f, 1.22501f, 0.0f,
			2.42544f, 2.01903f, 2.54607f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestAABBToAABBD]")
	{
		aabbD1 =
		{
			0.0, -1.80447f, 0.0, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		aabbD2 =
		{
			0.0, 1.39216f, 1.22501f, 0.0,
			2.42544f, 2.01903f, 2.54607f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestAABBToAABBF]")
	{
		aabbF1 =
		{
			0.0f, 3.35517f, -1.27879f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		aabbF2 =
		{
			0.0f, -1.02735f, -5.04044f, 0.0f,
			1.71417f, 0.786928f, 1.78531f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestAABBToAABBD]")
	{
		aabbD1 =
		{
			0.0, 3.35517f, -1.27879f, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		aabbD2 =
		{
			0.0, -1.02735f, -5.04044f, 0.0,
			1.71417f, 0.786928f, 1.78531f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test AABB to OBB.", "[TestAABBToOBBF], [TestAABBToOBBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GOBBF obbF = {};

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GOBBD obbD = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures collision.", "[TestAABBToOBBF]")
	{
		aabbF =
		{
			0.0f, -1.80447f, 0.0f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		obbF =
		{
			0.0f, 1.39216f, 1.22501f, 0.0f,
			2.42544f, 2.01903f, 2.54607f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToOBBF(
			aabbF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestAABBToOBBD]")
	{
		aabbD =
		{
			0.0, -1.80447f, 0.0, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		obbD =
		{
			0.0, 1.39216f, 1.22501f, 0.0,
			2.42544f, 2.01903f, 2.54607f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToOBBD(
			aabbD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestAABBToOBBF]")
	{
		aabbF =
		{
			0.0f, 3.35517f, -1.27879f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		obbF =
		{
			0.0f, -1.02735f, -5.04044f, 0.0f,
			1.71417f, 0.786928f, 1.78531f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToOBBF(
			aabbF,
			obbF,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestAABBToOBBD]")
	{
		aabbD =
		{
			0.0, 3.35517f, -1.27879f, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		obbD =
		{
			0.0, -1.02735f, -5.04044f, 0.0,
			1.71417f, 0.786928f, 1.78531f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestAABBToOBBD(
			aabbD,
			obbD,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Test OBB to OBB.", "[TestOBBToOBBF], [TestOBBToOBBD], [Math]")
{
	GW::MATH::GOBBF obbF1 = {};
	GW::MATH::GOBBF obbF2 = {};

	GW::MATH::GOBBD obbD1 = {};
	GW::MATH::GOBBD obbD2 = {};

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures collision.", "[TestOBBToOBBF]")
	{
		obbF1 =
		{
			0.0f, -1.80447f, 0.0f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		obbF2 =
		{
			0.0f, 1.39216f, 1.22501f, 0.0f,
			2.42544f, 2.01903f, 2.54607f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestOBBToOBBF(
			obbF1,
			obbF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[TestOBBToOBBD]")
	{
		obbD1 =
		{
			0.0, -1.80447f, 0.0, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		obbD2 =
		{
			0.0, 1.39216f, 1.22501f, 0.0,
			2.42544f, 2.01903f, 2.54607f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestOBBToOBBD(
			obbD1,
			obbD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures no collision.", "[TestOBBToOBBF]")
	{
		obbF1 =
		{
			2.0f, 3.35517f, -1.27879f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f,
			0.6240681f, 0.3242635f, -0.498761f, 0.0f
		};

		obbF2 =
		{
			-2.0f, -1.02735f, -5.04044f, 0.0f,
			1.71417f, 0.786928f, 1.78531f, 0.0f,
			0.4060675f, -0.4453288f, -0.5167823f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestOBBToOBBF(
			obbF1,
			obbF2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestOBBToOBBD]")
	{
		obbD1 =
		{
			2.0f, 3.35517f, -1.27879f, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0,
			0.6240681f, 0.3242635f, -0.498761f, 0.0
		};

		obbD2 =
		{
			-2.0f, -1.02735f, -5.04044f, 0.0,
			1.71417f, 0.786928f, 1.78531f, 0.0,
			0.4060675f, -0.4453288f, -0.5167823f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::TestOBBToOBBD(
			obbD1,
			obbD2,
			collisionResult)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

// INTERSECT QUERY

TEST_CASE("Intersect line to triangle.", "[IntersectLineToTriangleF], [IntersectLineToTriangleD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GTRIANGLEF triangleF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GLINED lineD = {};
	GW::MATH::GTRIANGLED triangleD = {};
	GW::MATH::GVECTORD contactD = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0f;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectLineToTriangleF]")
	{
		lineF =
		{
			-0.774591f, 0, 0, 0,
			-0.774591f, 1, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectLineToTriangleD]")
	{
		lineD =
		{
			-0.774591f, 0, 0, 0,
			-0.774591f, 1, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures coplanar collision.", "[IntersectLineToTriangleF]")
	{
		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures coplanar collision.", "[IntersectLineToTriangleD]")
	{
		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectLineToTriangleF]")
	{
		lineF =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0.038277f, 0.572692f, -0.565302f, 0,
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0382770300f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.560288787f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.756633461f));
	};

	SECTION("D structures collision.", "[IntersectLineToTriangleD]")
	{
		lineD =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0.038277f, 0.572692f, -0.565302f, 0,
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.038277000188827570));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.56028881325784508));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.75663346285074518));
	};

	SECTION("F structures barycentric.", "[IntersectLineToTriangleF]")
	{
		GW::MATH::GVECTORF barycentricF = {};

		triangleF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
			1.73205080757f, -1.0f, 0.0f, 0.0f,
			-1.73205080757f, -1.0f, 0.0f, 0.0f
		};

		lineF =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			directionF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineF =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 2.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			directionF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			-1.0f, 1.73205080757f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, -1.73205080757f, 0.0f, 0.0f
		};

		lineF =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			directionF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			1.0f, 1.73205080757f, 0.0f, 0.0f,
			1.0f, -1.73205080757f, 0.0f, 0.0f,
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		lineF =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			-2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleF(
			lineF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			directionF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures barycentric.", "[IntersectLineToTriangleD]")
	{
		GW::MATH::GVECTORD barycentricD = {};

		triangleD =
		{
			0.0, 2.0, 0.0, 0.0,
			1.73205080757, -1.0, 0.0, 0.0,
			-1.73205080757, -1.0, 0.0, 0.0
		};

		lineD =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			directionD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.33333333333333343));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		lineD =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 2.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			directionD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			-1.0, 1.73205080757, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
			-1.0, -1.73205080757, 0.0, 0.0
		};

		lineD =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			directionD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			1.0, 1.73205080757, 0.0, 0.0,
			1.0, -1.73205080757, 0.0, 0.0,
			-2.0, 0.0, 0.0, 0.0,
		};

		lineD =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			-2.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToTriangleD(
			lineD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			directionD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Intersect line to plane.", "[IntersectLineToPlaneF], [IntersectLineToPlaneD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GLINED lineD = {};
	GW::MATH::GPLANED planeD = {};
	GW::MATH::GVECTORD contactD = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures intersect.", "[IntersectLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			0, 0, 0, 0,
			0, 2, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneF(
			lineF,
			planeF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.5f));
	};

	SECTION("D structures intersect.", "[IntersectLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			0, 0, 0, 0,
			0, 2, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneD(
			lineD,
			planeD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.5f));
	};

	SECTION("F structures below.", "[IntersectLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			-0.073782f, -1.7182f, -1.01143f, 0.0f,
			-0.0484416f, 0.0507505f, 0.323422f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneF(
			lineF,
			planeF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("D structures below.", "[IntersectLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			-0.073782f, -1.7182f, -1.01143f, 0.0,
			-0.0484416f, 0.0507505f, 0.323422f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneD(
			lineD,
			planeD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::BELOW);
	};

	SECTION("F structures above parallel to plane no collision.", "[IntersectLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 1.01f, 0, 0,
			1, 1.01f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneF(
			lineF,
			planeF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures above parallel to plane no collision.", "[IntersectLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 1.01f, 0, 0,
			1, 1.01f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneD(
			lineD,
			planeD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures below parallel to plane no collision.", "[IntersectLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 0.99f, 0, 0,
			1, 0.99f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneF(
			lineF,
			planeF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures below parallel to plane no collision.", "[IntersectLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 0.99f, 0, 0,
			1, 0.99f, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneD(
			lineD,
			planeD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures parallel on plane.", "[IntersectLineToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		lineF =
		{
			-1, 1, 0, 0,
			1, 1, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneF(
			lineF,
			planeF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures parallel on plane.", "[IntersectLineToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		lineD =
		{
			-1, 1, 0, 0,
			1, 1, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToPlaneD(
			lineD,
			planeD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Intersect line to sphere.", "[IntersectLineToSphereF], [IntersectLineToSphereD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GLINED lineD = {};
	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GVECTORD contactD = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectLineToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			-2.5, 1, 0, 0,
			-1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereF(
			lineF,
			sphereF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectLineToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			-2.5, 1, 0, 0,
			-1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereD(
			lineD,
			sphereD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures sphere contains line.", "[IntersectLineToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			-0.5, 1, 0, 0,
			0.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereF(
			lineF,
			sphereF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, lineF.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures sphere contains line.", "[IntersectLineToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			-0.5, 1, 0, 0,
			0.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereD(
			lineD,
			sphereD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, lineD.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("F structures sphere contains line start.", "[IntersectLineToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			-0.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereF(
			lineF,
			sphereF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, lineF.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures sphere contains line start.", "[IntersectLineToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			-0.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereD(
			lineD,
			sphereD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, lineD.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("F structures sphere contains line end.", "[IntersectLineToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		lineF =
		{
			-2.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereF(
			lineF,
			sphereF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.5f));
	};

	SECTION("D structures sphere contains line end.", "[IntersectLineToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		lineD =
		{
			-2.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToSphereD(
			lineD,
			sphereD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.5f));
	};
}

TEST_CASE("Intersect line to capsule.", "[IntersectLineToCapsuleF], [IntersectLineToCapsuleD], [Math]")
{
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectLineToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineF =
		{
			-2.5, 1, 0, 0,
			-1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectLineToCapsuleD]")
	{
		GW::MATH::GLINED lineD = {};
		GW::MATH::GCAPSULED capsuleD = {};
		GW::MATH::GVECTORD contactD = {};
		GW::MATH::GVECTORD directionD = {};
		double distanceD = 0.0;

		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineD =
		{
			-2.5, 1, 0, 0,
			-1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures sphere contains line.", "[IntersectLineToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineF =
		{
			-0.5, 1, 0, 0,
			0.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, lineF.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures sphere contains line.", "[IntersectLineToCapsuleD]")
	{
		GW::MATH::GLINED lineD = {};
		GW::MATH::GCAPSULED capsuleD = {};
		GW::MATH::GVECTORD contactD = {};
		GW::MATH::GVECTORD directionD = {};
		double distanceD = 0.0;

		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineD =
		{
			-0.5, 1, 0, 0,
			0.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, lineD.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("F structures sphere contains line start.", "[IntersectLineToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineF =
		{
			-0.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, lineF.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, lineF.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, lineF.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures sphere contains line start.", "[IntersectLineToCapsuleD]")
	{
		GW::MATH::GLINED lineD = {};
		GW::MATH::GCAPSULED capsuleD = {};
		GW::MATH::GVECTORD contactD = {};
		GW::MATH::GVECTORD directionD = {};
		double distanceD = 0.0;

		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineD =
		{
			-0.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, lineD.start.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, lineD.start.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, lineD.start.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("F structures sphere contains line end.", "[IntersectLineToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineF =
		{
			-2.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleF(
			lineF,
			capsuleF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.5f));
	};

	SECTION("D structures sphere contains line end.", "[IntersectLineToCapsuleD]")
	{
		GW::MATH::GLINED lineD = {};
		GW::MATH::GCAPSULED capsuleD = {};
		GW::MATH::GVECTORD contactD = {};
		GW::MATH::GVECTORD directionD = {};
		double distanceD = 0.0;

		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		lineD =
		{
			-2.5, 1, 0, 0,
			1.5, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToCapsuleD(
			lineD,
			capsuleD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.5f));
	};
}

TEST_CASE("Intersect line to AABB.", "[IntersectLineToAABBF], [IntersectLineToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GLINED lineD = {};
	GW::MATH::GVECTORD contactD = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures vertical line no collision.", "[IntersectLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		lineF =
		{
			1, 0, 0, 0,
			1, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBF(
			lineF,
			mm,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures vertical line no collision.", "[IntersectLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		lineD =
		{
			1, 0, 0, 0,
			1, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBD(
			lineD,
			mm,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures horizontal line no collision.", "[IntersectLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		lineF =
		{
			1, 0, 0, 0,
			2, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBF(
			lineF,
			mm,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures horizontal line no collision.", "[IntersectLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		lineD =
		{
			1, 0, 0, 0,
			2, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBD(
			lineD,
			mm,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectLineToAABBF]")
	{
		aabbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBF(
			lineF,
			mm,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.5f));
	};

	SECTION("D structures collision.", "[IntersectLineToAABBD]")
	{
		aabbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToAABBD(
			lineD,
			mm,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.5f));
	};
}

TEST_CASE("Intersect line to OBB.", "IntersectLineToOBBF], [IntersectLineToOBBD], [Math]")
{
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GLINEF lineF = {};
	GW::MATH::GVECTORF contactF = {};
	GW::MATH::GVECTORF directionF = {};
	float distanceF = 0.0f;

	GW::MATH::GOBBD obbD = {};
	GW::MATH::GLINED lineD = {};
	GW::MATH::GVECTORD contactD = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectLineToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToOBBF(
			lineF,
			obbF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectLineToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToOBBD(
			lineD,
			obbD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectLineToOBBF]")
	{
		obbF =
		{
			0, 0, 5, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineF =
		{
			0, 0, 5, 0,
			0, 1, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToOBBF(
			lineF,
			obbF,
			collisionResult,
			contactF,
			directionF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 5.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(directionF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.5f));
	};

	SECTION("D structures no collision.", "[IntersectLineToOBBD]")
	{
		obbD =
		{
			0, 0, 5, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		lineD =
		{
			0, 0, 5, 0,
			0, 1, 5, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectLineToOBBD(
			lineD,
			obbD,
			collisionResult,
			contactD,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 5.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.5f));
	};
}

TEST_CASE("Intersect ray to triangle.", "[IntersectRayToTriangleF], [IntersectRayToTriangleD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GTRIANGLEF triangleF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GTRIANGLED triangleD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0f;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectRayToTriangleF]")
	{
		rayF =
		{
			-0.774591f, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectRayToTriangleD]")
	{
		rayD =
		{
			-0.774591f, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures coplanar collision.", "[IntersectRayToTriangleF]")
	{
		rayF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures coplanar collision.", "[IntersectRayToTriangleD]")
	{
		rayD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectRayToTriangleF]")
	{
		rayF =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0, 0, -1.0f, 0,
		};

		triangleF =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			nullptr,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0382770300f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.521727026f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.75753999f));
	};

	SECTION("D structures collision.", "[IntersectRayToTriangleD]")
	{
		rayD =
		{
			0.038277f, 0.521727f, 1.75754f, 0,
			0, 0, -1.0f, 0,
		};

		triangleD =
		{
			-0.5f, 0, 0, 0,
			0, 1.0f, 0, 0,
			0.5f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			nullptr,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.038277000188827570));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.52172702550888062));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.7575399875640869));
	};

	SECTION("F structures barycentric.", "[IntersectRayToTriangleF]")
	{
		GW::MATH::GVECTORF barycentricF = {};

		triangleF =
		{
			0.0f, 2.0f, 0.0f, 0.0f,
			1.73205080757f, -1.0f, 0.0f, 0.0f,
			-1.73205080757f, -1.0f, 0.0f, 0.0f
		};

		rayF =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		rayF =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			-1.0f, 1.73205080757f, 0.0f, 0.0f,
			2.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, -1.73205080757f, 0.0f, 0.0f
		};

		rayF =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleF =
		{
			1.0f, 1.73205080757f, 0.0f, 0.0f,
			1.0f, -1.73205080757f, 0.0f, 0.0f,
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		rayF =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleF(
			rayF,
			triangleF,
			collisionResult,
			contactF,
			&barycentricF,
			distanceF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.9999999f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 1.0f));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures barycentric.", "[IntersectRayToTriangleD]")
	{
		GW::MATH::GVECTORD barycentricD = {};

		triangleD =
		{
			0.0, 2.0, 0.0, 0.0,
			1.73205080757, -1.0, 0.0, 0.0,
			-1.73205080757, -1.0, 0.0, 0.0
		};

		rayD =
		{
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.33333333333333331));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.33333333333333343));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		rayD =
		{
			0.0f, 2.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			-1.0, 1.73205080757, 0.0, 0.0,
			2.0, 0.0, 0.0, 0.0,
			-1.0, -1.73205080757, 0.0, 0.0
		};

		rayD =
		{
			2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		triangleD =
		{
			1.0, 1.73205080757, 0.0, 0.0,
			1.0, -1.73205080757, 0.0, 0.0,
			-2.0, 0.0, 0.0, 0.0,
		};

		rayD =
		{
			-2.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, -1.0f, 0.0f,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToTriangleD(
			rayD,
			triangleD,
			collisionResult,
			contactD,
			&barycentricD,
			distanceD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 1.0));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

TEST_CASE("Intersect ray to plane.", "[IntersectRayToPlaneF], [IntersectRayToPlaneD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GPLANEF planeF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GPLANED planeD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures intersect.", "[IntersectRayToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1,
		};

		rayF =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneF(
			rayF,
			planeF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.0f));
	};

	SECTION("D structures intersect.", "[IntersectRayToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			0, 0, 0, 0,
			0, 1, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneD(
			rayD,
			planeD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.0f));
	};

	SECTION("F structures below.", "[IntersectRayToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1,
		};

		rayF =
		{
			0, 0, 0, 0,
			0, -1.0f, 0, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneF(
			rayF,
			planeF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures below.", "[IntersectRayToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			0, 0, 0, 0,
			0, -1.0f, 0, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneD(
			rayD,
			planeD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures above parallel to plane no collision.", "[IntersectRayToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		rayF =
		{
			-1, 1.01f, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneF(
			rayF,
			planeF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures above parallel to plane no collision.", "[IntersectRayToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		rayD =
		{
			-1, 1.01f, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneD(
			rayD,
			planeD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures below parallel to plane no collision.", "[IntersectRayToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		rayF =
		{
			-1, 0.99f, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneF(
			rayF,
			planeF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures below parallel to plane no collision.", "[IntersectRayToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		rayD =
		{
			-1, 0.99f, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneD(
			rayD,
			planeD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures parallel on plane.", "[IntersectRayToPlaneF]")
	{
		planeF =
		{
			0, 1, 0, 1
		};

		rayF =
		{
			-1, 1, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneF(
			rayF,
			planeF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures parallel on plane.", "[IntersectRayToPlaneD]")
	{
		planeD =
		{
			0, 1, 0, 1
		};

		rayD =
		{
			-1, 1, 0, 0,
			1, 0, 0, 0,
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToPlaneD(
			rayD,
			planeD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};
}

TEST_CASE("Intersect ray to sphere.", "[IntersectRayToSphereF], [IntersectRayToSphereD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectRayToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		rayF =
		{
			-2.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereF(
			rayF,
			sphereF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectRayToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			-2.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereD(
			rayD,
			sphereD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures sphere contains ray position collision.", "[IntersectRayToSphereF]")
	{
		sphereF =
		{
			0, 1, 0, 1,
		};

		rayF =
		{
			0.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereF(
			rayF,
			sphereF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, rayF.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));

		sphereF =
		{
			0, 1, 0, 1,
		};

		rayF =
		{
			-0.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereF(
			rayF,
			sphereF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, rayF.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures sphere contains ray position collision.", "[IntersectRayToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			0.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereD(
			rayD,
			sphereD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, rayD.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));

		sphereD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			-0.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereD(
			rayD,
			sphereD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, rayD.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("D structures ray hits sphere collision.", "[IntersectRayToSphereD]")
	{
		sphereD =
		{
			0, 1, 0, 1,
		};

		rayD =
		{
			-2.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToSphereD(
			rayD,
			sphereD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.5f));
	};
}

TEST_CASE("Intersect ray to capsule.", "[IntersectRayToCapsuleF], [IntersectRayToCapsuleD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GCAPSULEF capsuleF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GCAPSULED capsuleD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectRayToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayF =
		{
			-2.5, 1, 0, 0,
			-1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectRayToCapsuleD]")
	{
		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayD =
		{
			-2.5, 1, 0, 0,
			-1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures capsule contains ray.", "[IntersectRayToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayF =
		{
			-0.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, rayF.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));

		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayF =
		{
			-0.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, rayF.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, rayF.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, rayF.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.0f));
	};

	SECTION("D structures capsule contains ray.", "[IntersectRayToCapsuleD]")
	{
		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayD =
		{
			-0.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, rayD.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));

		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayD =
		{
			-0.5, 1, 0, 0,
			-1.0f, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, rayD.position.x));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, rayD.position.y));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, rayD.position.z));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.0f));
	};

	SECTION("F structures ray pierces capsule.", "[IntersectRayToCapsuleF]")
	{
		capsuleF =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayF =
		{
			-2.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleF(
			rayF,
			capsuleF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.5f));
	};

	SECTION("D structures ray pierces capsule.", "[IntersectRayToCapsuleD]")
	{
		capsuleD =
		{
			0, 1, 0, 1,
			0, -1, 0, 0
		};

		rayD =
		{
			-2.5, 1, 0, 0,
			1, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToCapsuleD(
			rayD,
			capsuleD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, -1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.5f));
	};
}

TEST_CASE("Intersect ray to AABB.", "[IntersectRayToAABBF], [IntersectRayToAABBD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectRayToAABBF]")
	{
		rayF =
		{
			0, 0, 0, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		aabbF =
		{
			0, -0.050899f, 1.22213f, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToAABBF(
			rayF,
			mm,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectRayToAABBD]")
	{
		rayD =
		{
			0, 0, 0, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		aabbD =
		{
			0, -0.050899, 1.22213, 0,
			0.5, 0.5, 0.5, 0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToAABBD(
			rayD,
			mm,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectRayToAABBF]")
	{
		rayF =
		{
			0, 0, 0, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		aabbF =
		{
			0, 0.353694f, 1.22213f, 0,
			0.5f, 0.5f, 0.5f, 0
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToAABBF(
			rayF,
			mm,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.722129941f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 0.722129941f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 1.02124596f));
	};

	SECTION("D structures collision.", "[IntersectRayToAABBD]")
	{
		rayD =
		{
			0, 0, 0, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		aabbD =
		{
			0, 0.353694, 1.22213, 0,
			0.5, 0.5, 0.5, 0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToAABBD(
			rayD,
			mm,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.72212999999999994));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 0.72212999999999994));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.0212460572743649));
	};
}

TEST_CASE("Intersect ray to OBB.", "[IntersectRayToOBBF], [IntersectRayToOBBD], [Math]")
{
	GW::MATH::GRAYF rayF = {};
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GVECTORF contactF = {};
	float distanceF = 0.0f;

	GW::MATH::GRAYD rayD = {};
	GW::MATH::GOBBD obbD = {};
	GW::MATH::GVECTORD contactD = {};
	double distanceD = 0.0f;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectRayToOBBF]")
	{
		rayF =
		{
			-0.750187f, 0, -0.535239f, 0,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		obbF =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBF(
			rayF,
			obbF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectRayToOBBD]")
	{
		rayD =
		{
			-0.750187f, 0, -0.535239f, 0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		obbD =
		{
			0, 0, 0, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBD(
			rayD,
			obbD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectRayToOBBF]")
	{
		rayF =
		{
			0.0f, 0.0f, 2.08768f, 0.0f,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		obbF =
		{
			0, 0, 3.0f, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBF(
			rayF,
			obbF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 2.98023224e-08f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.205213338f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 2.29289341f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.290215462f));
	};

	SECTION("D structures collision.", "[IntersectRayToOBBD]")
	{
		rayD =
		{
			0.0, 0.0, 2.08768f, 0.0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		obbD =
		{
			0, 0, 3.0f, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBD(
			rayD,
			obbD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 5.5511151231257827e-17));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.20521317256450566));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 2.2928932739591588));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.29021525778512952));
	};

	SECTION("F structures collision.", "[IntersectRayToOBBF]")
	{
		rayF =
		{
			0.0f, 0.0f, 2.08768f, 0.0f,
			0.0f, 0.707106769f, 0.707106769f, 0.0f
		};

		obbF =
		{
			0, 0, 3.0f, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.130526f, 0, 0.9914449f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBF(
			rayF,
			obbF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 1.49011612e-08f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 0.394681871f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, 2.48236203f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.558164477f));
	};

	SECTION("D structures collision.", "[IntersectRayToOBBD]")
	{
		rayD =
		{
			0.0, 0.0, 2.08768f, 0.0,
			0.0, 0.707106769f, 0.707106769f, 0.0
		};

		obbD =
		{
			0, 0, 3.0f, 0,
			0.5f, 0.5f, 0.5f, 0,
			0, 0.130526f, 0, 0.9914449f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBD(
			rayD,
			obbD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 2.7755575615628914e-17));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 0.39468186044167081));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, 2.4823619618363244));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.55816443985680120));
	};

	SECTION("F structures collision.", "[IntersectRayToOBBF]")
	{
		rayF =
		{
			1.2f, 2.66175f, -1.01362f, 0.0f,
			0.608469367f, -0.256068081f, -0.751128554f, 0.0f
		};

		obbF =
		{
			0.7f, 2.87217f, -0.396392f, 0.0f,
			1.0f, 1.0f, 1.0f, 0.0f,
			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBF(
			rayF,
			obbF,
			collisionResult,
			contactF,
			distanceF)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.x, 1.44594836f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.y, 2.55824542f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(contactF.z, -1.31723225f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distanceF, 0.404208034f));
	};

	SECTION("D structures collision.", "[IntersectRayToOBBD]")
	{
		rayD =
		{
			1.2f, 2.66175, -1.01362, 0.0,
			0.608469367f, -0.256068081f, -0.751128554f, 0.0
		};

		obbD =
		{
			0.7f, 2.87217f, -0.396392f, 0.0,
			1.0f, 1.0f, 1.0f, 0.0,
			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectRayToOBBD(
			rayD,
			obbD,
			collisionResult,
			contactD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.x, 1.4459482442422291));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.y, 2.5582452287686683));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(contactD.z, -1.3172321838427241));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.40420801148078184));
	};
}

TEST_CASE("Intersect sphere to sphere.", "[IntersectSphereToSphereF], [IntersectSphereToSphereD], [Math]")
{
	GW::MATH::GSPHEREF sphereF1 = {};
	GW::MATH::GSPHEREF sphereF2 = {};
	GW::MATH::GVECTORF closestF1 = {};
	GW::MATH::GVECTORF closestF2 = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GSPHERED sphereD1 = {};
	GW::MATH::GSPHERED sphereD2 = {};
	GW::MATH::GVECTORD closestD1 = {};
	GW::MATH::GVECTORD closestD2 = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[TestSphereToSphereF]")
	{
		sphereF1 =
		{
			-1.58468f, -2.42917f, -0.672674f, 0.743662f
		};

		sphereF2 =
		{
			1.77154f, 2.25537f, -2.10866f, 0.303412f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereF(
			sphereF1,
			sphereF2,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestSphereToSphereD]")
	{
		sphereD1 =
		{
			-1.58468f, -2.42917f, -0.672674f, 0.743662f
		};

		sphereD2 =
		{
			1.77154f, 2.25537f, -2.10866f, 0.303412f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereD(
			sphereD1,
			sphereD2,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectSphereToSphereF]")
	{
		sphereF1 =
		{
			0, 0, 0, 2.5f
		};

		sphereF2 =
		{
			1, 0, 0, 2.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereF(
			sphereF1,
			sphereF2,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, 2.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, -1.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 4.0f));
	};

	SECTION("D structures collision.", "[IntersectSphereToSphereD]")
	{
		sphereD1 =
		{
			0, 0, 0, 2.5f
		};

		sphereD2 =
		{
			1, 0, 0, 2.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereD(
			sphereD1,
			sphereD2,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, 2.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, -1.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 4.0f));
	};

	SECTION("F structures collision.", "[IntersectSphereToSphereF]")
	{
		sphereF1 =
		{
			2.15906f, 1.56701f, -3.73512f, 0.5f
		};

		sphereF2 =
		{
			1.77154f, 2.25537f, -3.38065f, 0.942544f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereF(
			sphereF1,
			sphereF2,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, 1.93527448f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 1.96452498f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, -3.53042030f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, 2.19339538f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 1.50601935f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, -3.76652718f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, -0.447570980f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.795029759f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.409399539f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 0.576715052f));
	};

	SECTION("D structures collision.", "[IntersectSphereToSphereD]")
	{
		sphereD1 =
		{
			2.15906f, 1.56701f, -3.73512f, 0.5f
		};

		sphereD2 =
		{
			1.77154f, 2.25537f, -3.38065f, 0.942544f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToSphereD(
			sphereD1,
			sphereD2,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, 1.9352745201271107));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 1.9645249072954303));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, -3.5304202957768211));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, 2.1933953636065602));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 1.5060194181697699));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, -3.7665271021981575));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, -0.44757096249236084));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.79502972448771136));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.40939952456574213));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.57671490134674508));
	};
}

TEST_CASE("Intersect sphere to capsule.", "[IntersectSphereToCapsuleF], [IntersectSphereToCapsuleD], [Math]")
{
	GW::MATH::GSPHEREF SphereF = {};
	GW::MATH::GCAPSULEF CapsuleF = {};
	GW::MATH::GVECTORF closestF1 = {};
	GW::MATH::GVECTORF closestF2 = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GSPHERED SphereD = {};
	GW::MATH::GCAPSULED CapsuleD = {};
	GW::MATH::GVECTORD closestD1 = {};
	GW::MATH::GVECTORD closestD2 = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectSphereToCapsuleF]")
	{
		SphereF =
		{
			2.15877f, -0.74062f, -1.28587f, 0.88223f
		};

		CapsuleF =
		{
			-5.65294f, -6.4059f, 0.906548f, 0.5f,
			-2.59106f, -2.93521f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleF(
			SphereF,
			CapsuleF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectSphereToCapsuleD]")
	{
		SphereD =
		{
			2.15877f, -0.74062f, -1.28587f, 0.88223f
		};

		CapsuleD =
		{
			-5.65294f, -6.4059f, 0.906548f, 0.5f,
			-2.59106f, -2.93521f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleD(
			SphereD,
			CapsuleD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures clamp to capsule start collision.", "[IntersectSphereToCapsuleF]")
	{
		SphereF =
		{
			0, 0, 0, 2.5f
		};

		CapsuleF =
		{
			1, 0, 0, 2.5f,
			2, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleF(
			SphereF,
			CapsuleF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, 2.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, -1.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 4.0f));
	};

	SECTION("D structures clamp to capsule start collision.", "[IntersectSphereToCapsuleD]")
	{
		SphereD =
		{
			0, 0, 0, 2.5f
		};

		CapsuleD =
		{
			1, 0, 0, 2.5f,
			2, 0, 0, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleD(
			SphereD,
			CapsuleD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, 2.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, -1.5f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 4.0f));
	};

	SECTION("F structures collision.", "[IntersectSphereToCapsuleF]")
	{
		SphereF =
		{
			-1.90111f, 1.71778f, -1.28587f, 0.88223f
		};

		CapsuleF =
		{
			-0.494471f, -0.438022f, -4.09955f, 0.5f,
			-2.59106f, 2.37294f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleF(
			SphereF,
			CapsuleF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, -2.13673997f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 0.962810874f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, -0.894935310f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, -1.82694960f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 1.95539331f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, -1.40890980f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, -0.267084569f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, -0.855750918f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.443121016f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 1.15989649f));
	};

	SECTION("D structures collision.", "[IntersectSphereToCapsuleD]")
	{
		SphereD =
		{
			-1.90111f, 1.71778f, -1.28587f, 0.88223f
		};

		CapsuleD =
		{
			-0.494471f, -0.438022f, -4.09955f, 0.5f,
			-2.59106f, 2.37294f, 0.0652523f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToCapsuleD(
			SphereD,
			CapsuleD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, -2.1367406929178352));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 0.96281135884788038));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, -0.89493478772287416));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, -1.8269494040892260));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 1.9553932516605756));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, -1.4089100063465934));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, -0.26708527728643566));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, -0.85575037010818533));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.44312160714247689));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 1.1598965393228069));
	};
}

TEST_CASE("Intersect sphere to AABB.", "[IntersectSphereToAABBF], [IntersectSphereToAABBD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GAABBCEF aabbF = {};
	GW::MATH::GVECTORF closestF1 = {};
	GW::MATH::GVECTORF closestF2 = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GAABBCED aabbD = {};
	GW::MATH::GVECTORD closestD1 = {};
	GW::MATH::GVECTORD closestD2 = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectSphereToAABBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		aabbF =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0f,
			0.921908f, 1.17511f, 1.8925f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBF(
			sphereF,
			aabbF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectSphereToAABBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		aabbD =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0,
			0.921908f, 1.17511f, 1.8925f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBD(
			sphereD,
			aabbD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectSphereToAABBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		aabbF =
		{
			-1.33964f, 0.0f, 3.21097f, 0.0f,
			1.64764f, 1.60093f, 1.8925f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBF(
			sphereF,
			aabbF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, -1.10897672f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 1.03144276f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 2.51091623f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, -0.981967986f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 1.60092998f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 2.12544990f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, 0.181620672f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.814358592f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, -0.551211357f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 0.699307680f));
	};

	SECTION("D structures collision.", "[IntersectSphereToAABBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		aabbD =
		{
			-1.33964f, 0.0, 3.21097f, 0.0,
			1.64764f, 1.60093f, 1.8925f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBD(
			sphereD,
			aabbD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, -1.1089767410288225));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 1.0314427860954327));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 2.5109162843194119));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, -0.98196798563003540));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 1.6009299755096436));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 2.1254498958587646));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.18162070940448474));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.81435856145050367));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, -0.55121143974674380));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.69930767154932649));
	};

	SECTION("F structures sphere center within AABB bounds collision.", "[IntersectSphereToAABBF]")
	{
		sphereF =
		{
			0, 0, 0, 5.0f
		};

		aabbF =
		{
			-2.0f, -3.0f, 0.0f, 0.0f,
			4.0f, 4.0f, 4.0f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBF(
			sphereF,
			aabbF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, -2.77350092f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, -4.16025162f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, 0.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, 0.473425329f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.880833983f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.0f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 5.85837030f));
	};

	SECTION("D structures sphere center within AABB bounds collision.", "[IntersectSphereToAABBD]")
	{
		sphereD =
		{
			0, 0, 0, 5.0f
		};

		aabbD =
		{
			-2.0f, -3.0f, 0.0, 0.0,
			4.0f, 4.0f, 4.0f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToAABBD(
			sphereD,
			aabbD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, -2.7735009811261460));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, -4.1602514716892189));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, 0.0));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 1.0f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.47342534255936697));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.88083394860924058));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.0));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 5.8583703317030444));
	};
}

TEST_CASE("Intersect sphere to OBB.", "[IntersectSphereToOBBF], [IntersectSphereToOBBD], [Math]")
{
	GW::MATH::GSPHEREF sphereF = {};
	GW::MATH::GOBBF obbF = {};
	GW::MATH::GVECTORF closestF1 = {};
	GW::MATH::GVECTORF closestF2 = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GSPHERED sphereD = {};
	GW::MATH::GOBBD obbD = {};
	GW::MATH::GVECTORD closestD1 = {};
	GW::MATH::GVECTORD closestD2 = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectSphereToOBBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		obbF =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0f,
			0.921908f, 1.17511f, 1.8925f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToOBBF(
			sphereF,
			obbF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[IntersectSphereToOBBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.215504f
		};

		obbD =
		{
			0.12411f, 1.49679f, -1.40387f, 0.0,
			0.921908f, 1.17511f, 1.8925f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToOBBD(
			sphereD,
			obbD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectSphereToOBBF]")
	{
		sphereF =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		obbF =
		{
			-1.33964f, 0.0f, 3.21097f, 0.0f,
			1.64764f, 1.60093f, 1.8925f, 0.0f,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToOBBF(
			sphereF,
			obbF,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, -1.10897672f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 1.03144276f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 2.51091623f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, -0.981967986f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 1.60092998f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 2.12544990f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.x, 0.181620672f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, 0.814358592f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, -0.551211357f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 0.699307680f));
	};

	SECTION("D structures collision.", "[IntersectSphereToOBBD]")
	{
		sphereD =
		{
			-0.981968f, 1.59938f, 2.12545f, 0.698046f
		};

		obbD =
		{
			-1.33964f, 0.0, 3.21097f, 0.0,
			1.64764f, 1.60093f, 1.8925f, 0.0,
			0, 0.3826834f, 0, 0.9238796f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectSphereToOBBD(
			sphereD,
			obbD,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, -1.1089767392633902));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 1.0314427939898074));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 2.5109162789613970));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, -0.98196798563003518));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 1.6009299755096436));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 2.1254498958587646));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.18162070688448617));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, 0.81435855018203618));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, -0.55121143209865031));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.69930767153182938));
	};
}

TEST_CASE("Intersect capsule to capsule.", "[IntersectCapsuleToCapsuleF], [IntersectCapsuleToCapsuleD], [Math]")
{
	GW::MATH::GCAPSULEF CapsuleF1 = {};
	GW::MATH::GCAPSULEF CapsuleF2 = {};
	GW::MATH::GVECTORF closestF1 = {};
	GW::MATH::GVECTORF closestF2 = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GCAPSULED CapsuleD1 = {};
	GW::MATH::GCAPSULED CapsuleD2 = {};
	GW::MATH::GVECTORD closestD1 = {};
	GW::MATH::GVECTORD closestD2 = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult;

	SECTION("F structures no collision.", "[IntersectCapsuleToCapsuleF]")
	{
		CapsuleF1 =
		{
			5.49113f, 3.91621f, -2.22193f, 0.5f,
			2.2171f, 3.72393f, 0.978328f, 0.5f
		};

		CapsuleF2 =
		{
			-2.82114f, -0.915085f, 2.41955f, 0.5f,
			0.0f, 5.15133f, 0.0f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectCapsuleToCapsuleF(
			CapsuleF1,
			CapsuleF2,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

	};

	SECTION("D structures no collision.", "[IntersectCapsuleToCapsuleD]")
	{
		CapsuleD1 =
		{
			5.49113f, 3.91621f, -2.22193f, 0.5f,
			2.2171f, 3.72393f, 0.978328f, 0.5f
		};

		CapsuleD2 =
		{
			-2.82114f, -0.915085f, 2.41955f, 0.5f,
			0.0, 5.15133f, 0.0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectCapsuleToCapsuleD(
			CapsuleD1,
			CapsuleD2,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);

	};

	SECTION("F structures collision.", "[IntersectCapsuleToCapsuleF]")
	{
		CapsuleF1 =
		{
			2.41279f, 1.86444f, 0.0f, 0.5f,
			0.0f, 2.44932f, 0.978328f, 0.5f
		};

		CapsuleF2 =
		{
			0.647614f, 4.20551f, 2.41955f, 0.5f,
			0.0f, 1.0f, 0.0f, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectCapsuleToCapsuleF(
			CapsuleF1,
			CapsuleF2,
			collisionResult,
			closestF1,
			closestF2,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.x, 0.323313415f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.y, 2.08318448f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF1.z, 1.26601231f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.x, 0.182147980f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.y, 2.61403513f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(closestF2.z, 0.600503564f));

        // on an ARM processor the tollerance for the line below must be loose to pass
		CHECK(G_WITHIN_LOOSE_DEVIATION_F(direction.x, 0.163590327f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.y, -0.615179002f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(direction.z, 0.771228254f));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(distance, 0.862920642f));
	};

	SECTION("D structures collision.", "[IntersectCapsuleToCapsuleD]")
	{
		CapsuleD1 =
		{
			2.41279f, 1.86444f, 0.0, 0.5f,
			0.0, 2.44932f, 0.978328f, 0.5f
		};

		CapsuleD2 =
		{
			0.647614f, 4.20551f, 2.41955f, 0.5f,
			0.0, 1.0f, 0.0, 0.5f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectCapsuleToCapsuleD(
			CapsuleD1,
			CapsuleD2,
			collisionResult,
			closestD1,
			closestD2,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.x, 0.32331306376513114));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.y, 2.0831839583496849));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD1.z, 1.2660118893275847));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.x, 0.18214843841389819));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.y, 2.6140355336181771));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(closestD2.z, 0.60050385780684601));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.x, 0.16358937541151969));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.y, -0.61518016584057411));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(directionD.z, 0.77122751494538411));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(distanceD, 0.86292049832774398));
	};
}

TEST_CASE("Intersect AABB to AABB.", "[IntersectAABBToAABBF], [IntersectAABBToAABBD], [Math]")
{
	GW::MATH::GAABBCEF aabbF1 = {};
	GW::MATH::GAABBCEF aabbF2 = {};
	GW::MATH::GAABBCEF aabbF_out = {};
	GW::MATH::GVECTORF direction = {};
	float distance = 0.0f;

	GW::MATH::GAABBCED aabbD1 = {};
	GW::MATH::GAABBCED aabbD2 = {};
	GW::MATH::GAABBCED aabbD_out = {};
	GW::MATH::GVECTORD directionD = {};
	double distanceD = 0.0;

	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

	SECTION("F structures no collision.", "[TestAABBToAABBF]")
	{
		aabbF1 =
		{
			0.0f, 3.35517f, -1.27879f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		aabbF2 =
		{
			0.0f, -1.02735f, -5.04044f, 0.0f,
			1.71417f, 0.786928f, 1.78531f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult,
			aabbF_out,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("D structures no collision.", "[TestAABBToAABBD]")
	{
		aabbD1 =
		{
			0.0, 3.35517f, -1.27879f, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		aabbD2 =
		{
			0.0, -1.02735f, -5.04044f, 0.0,
			1.71417f, 0.786928f, 1.78531f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult,
			aabbD_out,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
	};

	SECTION("F structures collision.", "[IntersectAABBToAABBF]")
	{
		aabbF1 =
		{
			-5, -5, -5, 0,
			3, 3, 3, 0
		};

		aabbF2 =
		{
			2, 2, 2, 0,
			6, 6, 6, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult,
			aabbF_out,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[IntersectAABBToAABBD]")
	{
		aabbD1 =
		{
			-5, -5, -5, 0,
			3, 3, 3, 0
		};

		aabbD2 =
		{
			2, 2, 2, 0,
			6, 6, 6, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult,
			aabbD_out,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures collision.", "[IntersectAABBToAABBF]")
	{
		aabbF1 =
		{
			2, 2, 2, 0,
			6, 6, 6, 0
		};

		aabbF2 =
		{
			-5, -5, -5, 0,
			3, 3, 3, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult,
			aabbF_out,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[IntersectAABBToAABBD]")
	{
		aabbD1 =
		{
			2, 2, 2, 0,
			6, 6, 6, 0
		};

		aabbD2 =
		{
			-5, -5, -5, 0,
			3, 3, 3, 0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult,
			aabbD_out,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("F structures collision.", "[IntersectAABBToAABBF]")
	{
		aabbF1 =
		{
			0.0f, -1.80447f, 0.0f, 0.0f,
			1.22763f, 1.42751f, 1.24286f, 0.0f
		};

		aabbF2 =
		{
			0.0f, 1.39216f, 1.22501f, 0.0f,
			2.42544f, 2.01903f, 2.54607f, 0.0f
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBF(
			aabbF1,
			aabbF2,
			collisionResult,
			aabbF_out,
			direction,
			distance)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};

	SECTION("D structures collision.", "[IntersectAABBToAABBD]")
	{
		aabbD1 =
		{
			0.0, -1.80447f, 0.0, 0.0,
			1.22763f, 1.42751f, 1.24286f, 0.0
		};

		aabbD2 =
		{
			0.0, 1.39216f, 1.22501f, 0.0,
			2.42544f, 2.01903f, 2.54607f, 0.0
		};

		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToAABBD(
			aabbD1,
			aabbD2,
			collisionResult,
			aabbD_out,
			directionD,
			distanceD)));

		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
	};
}

//TEST_CASE("Intersect AABB to OBB.", "[IntersectAABBToOBBF], [IntersectAABBToOBBD]")
//{
//	GW::MATH::GAABBCEF aabbF = {};
//	GW::MATH::GOBBF obbF = {};
//	GW::MATH::GVECTORF closestF1 = {};
//	GW::MATH::GVECTORF closestF2 = {};
//	GW::MATH::GVECTORF direction = {};
//	float distance = 0.0f;
//
//	GW::MATH::GAABBCED aabbD = {};
//	GW::MATH::GOBBD obbD = {};
//	GW::MATH::GVECTORD closestD1 = {};
//	GW::MATH::GVECTORD closestD2 = {};
//	GW::MATH::GVECTORD directionD = {};
//	double distanceD = 0.0;
//
//	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//	SECTION("F structures no collision.", "[IntersectAABBToOBBF]")
//	{
//		aabbF =
//		{
//			0.0f, 3.35517f, -1.27879f, 0.0f,
//			1.22763f, 1.42751f, 1.24286f, 0.0f
//		};
//
//		obbF =
//		{
//			0.0f, -1.02735f, -5.04044f, 0.0f,
//			1.71417f, 0.786928f, 1.78531f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToOBBF(
//			aabbF,
//			obbF,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
//	};
//
//	SECTION("D structures no collision.", "[IntersectAABBToOBBD]")
//	{
//		aabbD =
//		{
//			0.0, 3.35517f, -1.27879f, 0.0,
//			1.22763f, 1.42751f, 1.24286f, 0.0
//		};
//
//		obbD =
//		{
//			0.0, -1.02735f, -5.04044f, 0.0,
//			1.71417f, 0.786928f, 1.78531f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToOBBD(
//			aabbD,
//			obbD,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
//	};
//
//	SECTION("F structures collision.", "[IntersectAABBToOBBF]")
//	{
//		aabbF =
//		{
//			0.0f, -1.80447f, 0.0f, 0.0f,
//			1.22763f, 1.42751f, 1.24286f, 0.0f
//		};
//
//		obbF =
//		{
//			0.0f, 1.39216f, 1.22501f, 0.0f,
//			2.42544f, 2.01903f, 2.54607f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToOBBF(
//			aabbF,
//			obbF,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures collision.", "[IntersectAABBToOBBD]")
//	{
//		aabbD =
//		{
//			0.0, -1.80447f, 0.0, 0.0,
//			1.22763f, 1.42751f, 1.24286f, 0.0
//		};
//
//		obbD =
//		{
//			0.0, 1.39216f, 1.22501f, 0.0,
//			2.42544f, 2.01903f, 2.54607f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectAABBToOBBD(
//			aabbD,
//			obbD,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//}
//
//TEST_CASE("Intersect OBB to OBB.", "[IntersectOBBToOBBF], [IntersectOBBToOBBD]")
//{
//	GW::MATH::GOBBF obbF1 = {};
//	GW::MATH::GOBBF obbF2 = {};
//	GW::MATH::GVECTORF closestF1 = {};
//	GW::MATH::GVECTORF closestF2 = {};
//	GW::MATH::GVECTORF direction = {};
//	float distance = 0.0f;
//
//	GW::MATH::GOBBD obbD1 = {};
//	GW::MATH::GOBBD obbD2 = {};
//	GW::MATH::GVECTORD closestD1 = {};
//	GW::MATH::GVECTORD closestD2 = {};
//	GW::MATH::GVECTORD directionD = {};
//	double distanceD = 0.0;
//
//	GW::MATH::GCollision::GCollisionCheck collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//	SECTION("F structures no collision.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			2.0f, 3.35517f, -1.27879f, 0.0f,
//			1.22763f, 1.42751f, 1.24286f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbF2 =
//		{
//			-2.0f, -1.02735f, -5.04044f, 0.0f,
//			1.71417f, 0.786928f, 1.78531f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
//	};
//
//	SECTION("D structures no collision.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			2.0f, 3.35517f, -1.27879f, 0.0,
//			1.22763f, 1.42751f, 1.24286f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbD2 =
//		{
//			-2.0f, -1.02735f, -5.04044f, 0.0,
//			1.71417f, 0.786928f, 1.78531f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::NO_COLLISION);
//	};
//
//	SECTION("F structures collision.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			-1.0f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbF2 =
//		{
//			1.0f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures collision.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			-1.0f, 0.0, 0.0, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbD2 =
//		{
//			1.0f, 0.0, 0.0, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("F structures collision2.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			0.5f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbF2 =
//		{
//			1.0f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures collision2.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			0.5f, 0.0, 0.0, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbD2 =
//		{
//			1.0f, 0.0, 0.0, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("F structures trash this test.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			1.0f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		obbF2 =
//		{
//			-1.0f, 0.0f, 0.0f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0, 0.3826834f, 0, 0.9238796f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("F structures random orientation collision.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			0.7f, 2.87217f, -0.396392f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbF2 =
//		{
//			1.2f, 2.66175, -1.01362, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures random orientation collision.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			0.7f, 2.87217f, -0.396392f, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbD2 =
//		{
//			1.2f, 2.66175, -1.01362, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//
//	};
//
//	SECTION("F structures random orientation collision.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			1.2f, 2.87217f, -0.396392f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbF2 =
//		{
//			0.7f, 2.66175, -1.01362, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures random orientation collision.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			1.2f, 2.87217f, -0.396392f, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbD2 =
//		{
//			0.7f, 2.66175, -1.01362, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("F structures random orientation collision.", "[IntersectOBBToOBBF]")
//	{
//		obbF1 =
//		{
//			1.2f, 2.87217f, -0.396392f, 0.0f,
//			1.0f, 1.0f, 1.0f, 0.0f,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbF2 =
//		{
//			0.755372f, 2.40796f, -0.995894f, 0.0f,
//			0.2f, 0.2f, 0.2f, 0.0f,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBF(
//			obbF1,
//			obbF2,
//			collisionResult,
//			closestF1,
//			closestF2,
//			direction,
//			distance)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//
//	SECTION("D structures random orientation collision.", "[IntersectOBBToOBBD]")
//	{
//		obbD1 =
//		{
//			1.2f, 2.87217f, -0.396392f, 0.0,
//			1.0f, 1.0f, 1.0f, 0.0,
//			0.649298f, 0.26848f, 0.5723961f, 0.4227213f
//		};
//
//		obbD2 =
//		{
//			0.755372f, 2.40796f, -0.995894f, 0.0,
//			0.2f, 0.2f, 0.2f, 0.0,
//			-0.5902316f, 0.4401826f, 0.3565393f, -0.5751049f
//		};
//
//		collisionResult = GW::MATH::GCollision::GCollisionCheck::ERROR_NO_RESULT;
//
//		CHECK(+(GW::I::GCollisionImplementation::IntersectOBBToOBBD(
//			obbD1,
//			obbD2,
//			collisionResult,
//			closestD1,
//			closestD2,
//			directionD,
//			distanceD)));
//
//		CHECK(collisionResult == GW::MATH::GCollision::GCollisionCheck::COLLISION);
//	};
//}

// SQ DISTANCE

TEST_CASE("Squared distance point to line.", "[SqDistancePointToLineF], [SqDistancePointToLineD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GLINEF lineF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GLINED lineD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures collision.", "[SqDistancePointToLineF]")
	{
		pointF =
		{
			2.0f, 0.0f, 0.0f, 0.0f,
		};

		lineF =
		{
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToLineF(
			pointF,
			lineF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures collision.", "[SqDistancePointToLineD]")
	{
		pointD =
		{
			2.0f, 0.0, 0.0, 0.0,
		};

		lineD =
		{
			0.0, 1.0f, 0.0, 0.0,
			0.0, -1.0f, 0.0, 0.0
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToLineD(
			pointD,
			lineD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to triangle.", "[SqDistancePointToTriangleF], [SqDistancePointToTriangleD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GTRIANGLEF triangleF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GTRIANGLED triangleD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures collision.", "[SqDistancePointToTriangleF]")
	{
		pointF =
		{
			-2.0f, 0.0f, 0.0f, 0.0f,
		};

		triangleF =
		{
			0.0f, 1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToTriangleF(
			pointF,
			triangleF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures collision.", "[SqDistancePointToTriangleD]")
	{
		pointD =
		{
			-2.0f, 0.0, 0.0, 0.0,
		};

		triangleD =
		{
			0.0f, 1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToTriangleD(
			pointD,
			triangleD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to plane.", "[SqDistancePointToPlaneF], [SqDistancePointToPlaneD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GPLANEF planeF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GPLANED planeD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures collision.", "[SqDistancePointToPlaneF]")
	{
		pointF =
		{
			0.0f, 3.0f, 0.0f, 0.0f,
		};

		planeF =
		{
			0.0f, 1.0f, 0.0f, 1.0f,
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToPlaneF(
			pointF,
			planeF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures collision.", "[SqDistancePointToPlaneD]")
	{
		pointD =
		{
			0.0, 3.0f, 0.0, 0.0,
		};

		planeD =
		{
			0.0, 1.0f, 0.0, 1.0f,
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToPlaneD(
			pointD,
			planeD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to sphere.", "[SqDistancePointToSphereF], [SqDistancePointToSphereD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GSPHEREF sphereF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GSPHERED sphereD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures collision.", "[SqDistancePointToSphereF]")
	{
		pointF =
		{
			0.0f, 4.0f, 0.0f, 0.0f,
		};

		sphereF =
		{
			0.0f, 1.0f, 0.0f, 1.0f,
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToSphereF(
			pointF,
			sphereF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures collision.", "[SqDistancePointToSphereD]")
	{
		pointD =
		{
			0.0, 4.0f, 0.0, 0.0,
		};

		sphereD =
		{
			0.0, 1.0f, 0.0, 1.0f,
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToSphereD(
			pointD,
			sphereD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to capsule.", "[SqDistancePointToCapsuleF], [SqDistancePointToCapsuleD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GCAPSULEF capsuleF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GCAPSULED capsuleD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures collision.", "[SqDistancePointToCapsuleF]")
	{
		pointF =
		{
			0.0f, 4.0f, 0.0f, 0.0f,
		};

		capsuleF =
		{
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToCapsuleF(
			pointF,
			capsuleF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures collision.", "[SqDistancePointToCapsuleD]")
	{
		pointD =
		{
			0.0, 4.0f, 0.0, 0.0,
		};

		capsuleD =
		{
			0.0, 1.0f, 0.0, 1.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToCapsuleD(
			pointD,
			capsuleD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to AABB.", "[SqDistancePointToAABBF], [SqDistancePointToAABBD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GAABBCEF aabbF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GAABBCED aabbD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures.", "[SqDistancePointToAABBF]")
	{
		pointF =
		{
			0.0f, 2.5f, 0.0f, 0.0f,
		};

		aabbF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f, 0.0f
		};

		GW::MATH::GAABBMMF mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMF(
			aabbF,
			mm);

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToAABBF(
			pointF,
			mm,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures.", "[SqDistancePointToAABBD]")
	{
		pointD =
		{
			0.0, 2.5f, 0.0, 0.0,
		};

		aabbD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.5f, 0.5f, 0.5f, 0.0
		};

		GW::MATH::GAABBMMD mm = {};
		GW::I::GCollisionImplementation::ConvertAABBCEToAABBMMD(
			aabbD,
			mm);

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToAABBD(
			pointD,
			mm,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Squared distance point to OBB.", "[SqDistancePointToOBBF], [SqDistancePointToOBBD], [Math]")
{
	GW::MATH::GVECTORF pointF = {};
	GW::MATH::GOBBF obbF = {};
	float outSqDistanceF = 0.0f;

	GW::MATH::GVECTORD pointD = {};
	GW::MATH::GOBBD obbD = {};
	double outSqDistanceD = 0.0;

	SECTION("F structures.", "[SqDistancePointToOBBF]")
	{
		pointF =
		{
			0.0f, 2.5f, 0.0f, 0.0f,
		};

		obbF =
		{
			0.0f, 0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f, 0.0f
		};

		outSqDistanceF = 0.0f;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToOBBF(
			pointF,
			obbF,
			outSqDistanceF)));

		CHECK(outSqDistanceF == 4.0f);
	};

	SECTION("D structures.", "[SqDistancePointToOBBD]")
	{
		pointD =
		{
			0.0, 2.5f, 0.0, 0.0,
		};

		obbD =
		{
			0.0, 0.0, 0.0, 0.0,
			0.5f, 0.5f, 0.5f, 0.0
		};

		outSqDistanceD = 0.0;

		CHECK(+(GW::I::GCollisionImplementation::SqDistancePointToOBBD(
			pointD,
			obbD,
			outSqDistanceD)));

		CHECK(outSqDistanceD == 4.0f);
	};
}

TEST_CASE("Compute barycentric coordinates", "[BarycentricF], [BarycentricD], [Math]")
{
	GW::MATH::GVECTORF aF = {-3.46724318643f, 2.6054413611f, 1.0f};
	GW::MATH::GVECTORF bF = {3.99f, 1.7f, 1.0f};
	GW::MATH::GVECTORF cF = {-0.522756813566f, -4.3054413611f, 1.0f};
	GW::MATH::GVECTORF pF = {};
	GW::MATH::GVECTORF barycentricF = {};

	GW::MATH::GVECTORD aD = {-3.46724318643, 2.6054413611, 1.0};
	GW::MATH::GVECTORD bD = {3.99, 1.7, 1.0};
	GW::MATH::GVECTORD cD = {-0.522756813566, -4.3054413611, 1.0};
	GW::MATH::GVECTORD pD = {};
	GW::MATH::GVECTORD barycentricD = {};

	SECTION("F structures center.", "[TestRayToTriangleF]")
	{
		pF = {0.0f, 0.0f, 1.0f};
		barycentricF = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricF(
			aF,
			bF,
			cF,
			pF,
			barycentricF)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.x, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.y, 0.3333333f));
		CHECK(G_WITHIN_STANDARD_DEVIATION_F(barycentricF.z, 0.3333333f));
	};

	SECTION("D structures center.", "[TestRayToTriangleD]")
	{
		pD = {0.0, 0.0, 1.0};
		barycentricD = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricD(
			aD,
			bD,
			cD,
			pD,
			barycentricD)));

		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.x, 0.333333333333497));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.y, 0.333333333333144));
		CHECK(G_WITHIN_STANDARD_DEVIATION_D(barycentricD.z, 0.333333333333358));
	};

	aF = {0.0f, 0.5f, 1.0f};
	bF = {0.5f, -0.5f, 1.0f};
	cF = {-0.5f, -0.5f, 1.0f};

	aD = {0.0, 0.5, 1.0};
	bD = {0.5, -0.5, 1.0};
	cD = {-0.5, -0.5, 1.0};

	SECTION("F structures x.", "[TestRayToTriangleF]")
	{
		pF = {0.0f, 0.499f, 1.0f};
		barycentricF = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricF(
			aF,
			bF,
			cF,
			pF,
			barycentricF)));

		CHECK(barycentricF.x >= 0.99f);
		CHECK(barycentricF.y <= 0.005f);
		CHECK(barycentricF.z <= 0.005f);
	};

	SECTION("D structures x.", "[TestRayToTriangleD]")
	{
		pD = {0.0, 0.499, 1.0};
		barycentricD = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricD(
			aD,
			bD,
			cD,
			pD,
			barycentricD)));

		CHECK(barycentricD.x >= 0.99);
		CHECK(barycentricD.y <= 0.009);
		CHECK(barycentricD.z <= 0.009);
	};

	SECTION("F structures y.", "[TestRayToTriangleF]")
	{
		pF = {0.499f, -0.499f, 1.0f};
		barycentricF = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricF(
			aF,
			bF,
			cF,
			pF,
			barycentricF)));

		CHECK(barycentricF.x <= 0.009f);
		CHECK(barycentricF.y >= 0.99f);
		CHECK(barycentricF.z <= 0.009f);
	};

	SECTION("D structures y.", "[TestRayToTriangleD]")
	{
		pD = {0.499, -0.499, 1.0f};
		barycentricD = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricD(
			aD,
			bD,
			cD,
			pD,
			barycentricD)));

		CHECK(barycentricD.x <= 0.009);
		CHECK(barycentricD.y >= 0.99f);
		CHECK(barycentricD.z <= 0.009);
	};

	SECTION("F structures z.", "[TestRayToTriangleF]")
	{
		pF = {-0.499f, -0.499f, 1.0f};
		barycentricF = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricF(
			aF,
			bF,
			cF,
			pF,
			barycentricF)));

		CHECK(barycentricF.x <= 0.009f);
		CHECK(barycentricF.y <= 0.009f);
		CHECK(barycentricF.z >= 0.99f);
	};

	SECTION("D structures z.", "[TestRayToTriangleD]")
	{
		pD = {-0.499, -0.499, 1.0};
		barycentricD = {};

		CHECK(+(GW::I::GCollisionImplementation::BarycentricD(
			aD,
			bD,
			cD,
			pD,
			barycentricD)));

		CHECK(barycentricD.x <= 0.009);
		CHECK(barycentricD.y <= 0.009);
		CHECK(barycentricD.z >= 0.99);
	};
}
#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GCOLLISION) */
