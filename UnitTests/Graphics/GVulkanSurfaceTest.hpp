#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GVULKANSURFACE)

#include "../../Interface/Math/GVector.h"
#include "../../Interface/Math/GMatrix.h"
#include "../../Interface/System/GFile.h"
#include "../../UnitTests/Resources/GVulkanSurface_TestFolder/ShipPaintingARGB.h" //for loading the ship texture into the unit test
#include "../../Source/Shared/GVulkanHelper.hpp"

#if TARGET_OS_IOS
#include "../../Source/Shared/iosutils.h"
#endif

struct Vertex {
	GW::MATH::GVECTORF pos;
	GW::MATH::GVECTORF uv;

	static VkVertexInputBindingDescription get_binding_description()
	{
		VkVertexInputBindingDescription vertex_input_binding_description = {};

		vertex_input_binding_description.binding = 0;
		vertex_input_binding_description.stride = sizeof(Vertex);
		vertex_input_binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return vertex_input_binding_description;
	}
	static VkVertexInputAttributeDescription* get_attribute_description()
	{
		VkVertexInputAttributeDescription* attribute_description = new VkVertexInputAttributeDescription[2];

		attribute_description[0].binding = 0;
		attribute_description[0].location = 0;
		attribute_description[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribute_description[0].offset = 0;

		attribute_description[1].binding = 0;
		attribute_description[1].location = 1;
		attribute_description[1].format = VK_FORMAT_R32G32_SFLOAT;
		attribute_description[1].offset = 16;

		return attribute_description;
	}
};
struct UBO {
	GW::MATH::GMATRIXF model;
	GW::MATH::GMATRIXF view;
	GW::MATH::GMATRIXF projection;
};
VkResult CreatePipeline(const VkDevice& _vkDevice, const VkExtent2D _swapchainExtent, const VkSampleCountFlagBits& _MSAABit, const VkDescriptorSetLayout& _descriptorSetLayout, const VkDescriptorSetLayout& _pixeldescriptorSetLayout, const VkRenderPass& _vkRenderPass,
	VkPipelineLayout& _pipelineLayout, VkPipeline& _graphicsPipeline);
void CleanupTriangle(const VkDevice& _device, const uint32_t& _totalFrames, VkPipeline& _pipeline, VkPipelineLayout& _pipelineLayout, VkDescriptorSetLayout& _descriptorSetLayout, VkDescriptorPool& _descriptorPool,
	VkBuffer& _vertexBuffer, VkDeviceMemory& _vertexMemory, VkBuffer& _indexBuffer, VkDeviceMemory& _indexMemory, VkBuffer* _uboBuffer, VkDeviceMemory* _uboMemory);

bool EnableVulkanTests = true; //false by default
TEST_CASE("Check for Vulkan hardware and Window support", "[Graphics]")
{
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GVulkanSurface vkSurface;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		if (vkSurface.Create(window, 0) != GW::GReturn::HARDWARE_UNAVAILABLE)
		{
			EnableVulkanTests = true;
			std::cout << "VULKAN HARDWARE DETECTED: RUNNING ALL VULKAN UNIT TESTS" << std::endl;
		}
		else
			std::cout << "WARNING: VULKAN HARDWARE NOT DETECTED: SKIPPING ALL VULKAN UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL VULKAN UNIT TESTS" << std::endl;
}

TEST_CASE("GVulkanSurface core method test battery", "[Graphics]") {
	if (EnableVulkanTests == false)
	{
		std::cout << "Vulkan/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	//GWindow Setup
	GW::SYSTEM::GWindow GWin;
	GW::GEvent GEvent;
	//    GW::SYSTEM::GWindow::Events GWindowEvents;
	GW::SYSTEM::GWindow::EVENT_DATA GWindowEventData;

	//Properties
	unsigned long long init_mask = 0;
	float aspect_ratio = 0;
	unsigned int max_frames = 0;
	unsigned int current_frame = 0;
	unsigned int qfi[2] = {};
	VkClearValue myVkClearValue[2]{
		{ 0.0f, 1.0f, 0.0f, 1.0f },
		{ 1.0f, 16777216 }
	};

	//Vulkan Surface
	GW::GRAPHICS::GVulkanSurface vkSurface;
	void* vkObj;

	SECTION("Testing empty proxy method calls", "[ALL]") {
		//GEventResponderInterface Methods
		CHECK(vkSurface.Invoke() == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.Invoke(GEvent) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.Assign([](){}) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.Assign([](const GW::GEvent&){}) == GW::GReturn::EMPTY_PROXY);

		//GVulkanSurface Methods (Properties)
		CHECK(vkSurface.GetAspectRatio(aspect_ratio) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchainImageCount(max_frames) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchainCurrentImage(current_frame) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetQueueFamilyIndices(qfi[0], qfi[1]) == GW::GReturn::EMPTY_PROXY);

		//GVulkanSurface Methods (Vulkan Objects)
		CHECK(vkSurface.GetGraphicsQueue(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetPresentQueue(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchainImage(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchainView(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchainFramebuffer(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);

		//GVulkanSurface Methods (Vulkan Main Objects)
		CHECK(vkSurface.GetInstance(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSurface(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetPhysicalDevice(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetDevice(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetCommandPool(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetSwapchain(&vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetCommandBuffer(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetImageAvailableSemaphore(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetRenderFinishedSemaphore(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.GetRenderFence(-1, &vkObj) == GW::GReturn::EMPTY_PROXY);

		//GVulkanSurface Methods (Helper Methods)
		CHECK(vkSurface.StartFrame(2, reinterpret_cast<void**>(&myVkClearValue[2])) == GW::GReturn::EMPTY_PROXY);
		CHECK(vkSurface.EndFrame(false) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls", "[Create]") {
		//Generic Window & Vulkan Testing
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		REQUIRE(+vkSurface.Create(GWin, init_mask));
		vkSurface = nullptr;
		REQUIRE(!vkSurface);
		REQUIRE(+vkSurface.Create(GWin, init_mask));
		vkSurface = nullptr;
		REQUIRE(!vkSurface);
		REQUIRE(+vkSurface.Create(GWin, init_mask));
		GWin = nullptr;
		vkSurface = nullptr;
		REQUIRE(!GWin);
	}

	SECTION("Testing valid proxy method calls", "[ALL]") {
		//Create GWindow and GVulkanSurface
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		REQUIRE(+vkSurface.Create(GWin, init_mask));

		//GEventResponderInterface Methods
		CHECK(+GEvent.Write(GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED, GWindowEventData));
		CHECK(+vkSurface.Invoke());
		CHECK(+vkSurface.Invoke(GEvent));
		CHECK(+vkSurface.Assign([](){}));
		CHECK(+vkSurface.Assign([](const GW::GEvent&){}));
		
		//GVulkanSurface Methods (Properties)
		CHECK(+vkSurface.GetAspectRatio(aspect_ratio));
		CHECK(+vkSurface.GetSwapchainImageCount(max_frames));
		CHECK(+vkSurface.GetSwapchainCurrentImage(current_frame));
		CHECK(+vkSurface.GetQueueFamilyIndices(qfi[0], qfi[1]));

		//GVulkanSurface Methods (Vulkan Objects)
		CHECK(+vkSurface.GetGraphicsQueue(&vkObj));
		CHECK(+vkSurface.GetPresentQueue(&vkObj));
		CHECK(+vkSurface.GetSwapchainImage(-1, &vkObj));
		CHECK(+vkSurface.GetSwapchainView(-1, &vkObj));
		CHECK(+vkSurface.GetSwapchainFramebuffer(-1, &vkObj));

		//GVulkanSurface Methods (Vulkan Main Objects)
		CHECK(+vkSurface.GetInstance(&vkObj));
		CHECK(+vkSurface.GetSurface(&vkObj));
		CHECK(+vkSurface.GetPhysicalDevice(&vkObj));
		CHECK(+vkSurface.GetDevice(&vkObj));
		CHECK(+vkSurface.GetCommandPool(&vkObj));
		CHECK(+vkSurface.GetSwapchain(&vkObj));
		CHECK(+vkSurface.GetCommandBuffer(-1, &vkObj));
		CHECK(+vkSurface.GetImageAvailableSemaphore(-1, &vkObj));
		CHECK(+vkSurface.GetRenderFinishedSemaphore(-1, &vkObj));
		CHECK(+vkSurface.GetRenderFence(-1, &vkObj));

		//Cleanup
		vkSurface = nullptr;
		GWin = nullptr;
	}
}

TEST_CASE("Error Checking", "[Graphics]") {
	if (EnableVulkanTests == false)
	{
		std::cout << "Vulkan/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	//Setup Variables
	GW::SYSTEM::GWindow GWin;
	GW::GRAPHICS::GVulkanSurface vkSurface;
	void* vkObj;
	
	SECTION("Small Create Check", "[Create]") {
		//Setup GWindow
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));

		//Ooh, i want Direct2D With Vulkan [Dont Ask]
		REQUIRE(vkSurface.Create(GWin, GW::GRAPHICS::DIRECT2D_SUPPORT) == GW::GReturn::INVALID_ARGUMENT);

		//Ok.. How about OpenGL Mobile Support With Vulkan! [Again... DONT ASK!]
		REQUIRE(vkSurface.Create(GWin, GW::GRAPHICS::OPENGL_ES_SUPPORT) == GW::GReturn::INVALID_ARGUMENT);

		//Finally Succeed GVulkanSurface!
		REQUIRE(+vkSurface.Create(GWin, 0ull));

		//Cleanup for next test
		vkSurface = nullptr;
		GWin = nullptr;
	}

	SECTION("Test for Bindless Resource Support", "[Create]") {
		//Setup GWindow
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));

		// Test should always succeed unless the platform doesn't support bindless resources
		GW::GReturn result = vkSurface.Create(GWin, GW::GRAPHICS::BINDLESS_SUPPORT);
		if (result != GW::GReturn::HARDWARE_UNAVAILABLE)
			REQUIRE(G_PASS(result));
		else 
			std::cout << "Vulkan bindless hardware not detected, skipping unit test." << std::endl;

		//Cleanup for next test
		vkSurface = nullptr;
		GWin = nullptr;
	}

	SECTION("Big Create Check", "[Create]") {
		//Setup GWindow
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));

		//I can Simulate Small Create!
		REQUIRE(+vkSurface.Create(GWin, 0ull, 0, nullptr, 0, nullptr, 0, nullptr, false));
		vkSurface = nullptr;
		REQUIRE(!vkSurface);

		//Well, it works! Time to.... Darn it! Layers & Extensions!
        // Haha! iOS cannot ave layers, too bad for you!
        #if TARGET_OS_IOS
        const char* inst_layer = nullptr;
        #else
		const char* inst_layer = "VK_LAYER_KHRONOS_validation";
        #endif
		const char* inst_ext = VK_KHR_SURFACE_EXTENSION_NAME;
		const char* dev_ext = VK_KHR_SWAPCHAIN_EXTENSION_NAME;

        #if !TARGET_OS_IOS
		//Instance Layers Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, nullptr, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_ext, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);

		//Instance Extension Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, nullptr, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 0, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_layer, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);

		//Device Extension Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_ext, 1, nullptr, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_ext, 0, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_ext, 1, &inst_layer, false) == GW::GReturn::INVALID_ARGUMENT);
        
        //This one is a success!
        REQUIRE(+vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_ext, 1, &dev_ext, false));
        #else
        //Instance Layers Mismatch
        // These crash on iOS, gonna just skip these for now...
        //REQUIRE(vkSurface.Create(GWin, 0ull, 1, nullptr, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        //REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        //REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_ext, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        
        //Instance Extension Mismatch
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, nullptr, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 0, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &inst_layer, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        
        //Device Extension Mismatch
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &inst_ext, 1, nullptr, false) == GW::GReturn::INVALID_ARGUMENT);
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &inst_ext, 0, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
        REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &inst_ext, 1, &inst_layer, false) == GW::GReturn::INVALID_ARGUMENT);
        
        //This one is a success!
        REQUIRE(+vkSurface.Create(GWin, 0ull, 0, nullptr, 1, &inst_ext, 1, &dev_ext, false));
        #endif


		//Cleanup
		vkSurface = nullptr;
		GWin = nullptr;
	}

	SECTION("Mega Create Check", "[Create]") {
		//Setup GWindow
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));

		// Simulate Small Create!
		REQUIRE(+vkSurface.Create(GWin, 0ull, 0, nullptr, 0, nullptr, 0, nullptr, 0, nullptr, false));
		vkSurface = nullptr;
		REQUIRE(!vkSurface);

#if TARGET_OS_IOS
		const char* inst_layer = nullptr;
#else
		const char* inst_layer = "VK_LAYER_KHRONOS_validation";
#endif
		const char* inst_ext = VK_KHR_SURFACE_EXTENSION_NAME;
		
		// IMPORTANT NOTE: VK_EXT_LAYER_SETTINGS_EXTENSION_NAME is an extension advertised by a layer, not a device extension.
		// If your card supports "VK_LAYER_KHRONOS_validation" and the latest Vulkan SDK is installed, you should have this feature.
		// Therefore we recommend NOT including it in the device extension list, as it is not technically a driver reported extension.
		// https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/8760
		const char* dev_ext = VK_KHR_SWAPCHAIN_EXTENSION_NAME;// VK_EXT_LAYER_SETTINGS_EXTENSION_NAME; (should work regardless)

#if defined(VK_EXT_layer_settings)
		// test by enabling debug action log msg + break
        const std::array<const char *, 2> setting_debug_action{"VK_DBG_LAYER_ACTION_LOG_MSG",
                                                               "VK_DBG_LAYER_ACTION_BREAK"};
        const VkLayerSettingEXT lyr_sett = {
			inst_layer, "debug_action", VK_LAYER_SETTING_TYPE_STRING_EXT,
			static_cast<uint32_t>(setting_debug_action.size()), setting_debug_action.data()
		};
#else
		const void* lyr_sett = nullptr;
#endif

// layers were not supported on iOS but this may have changed
#if !TARGET_OS_IOS
		//Instance Layers Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, nullptr, 1, &lyr_sett, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 0, &inst_layer, 1, &lyr_sett, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_ext, 1, &lyr_sett, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);

		//Instance Extension Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 1, nullptr, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 0, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 1, &inst_layer, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);

		//Device Extension Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 1, &inst_ext, 1, nullptr, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 1, &inst_ext, 0, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, &lyr_sett, 1, &inst_ext, 1, &inst_layer, false) == GW::GReturn::INVALID_ARGUMENT);

		// Layer Settings Mismatch
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 1, nullptr, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.Create(GWin, 0ull, 1, &inst_layer, 0, &lyr_sett, 1, &inst_ext, 1, &dev_ext, false) == GW::GReturn::INVALID_ARGUMENT);

#if defined(VK_EXT_layer_settings)
		// Inform the user of intended behavior
		std::cout << "\nIMPORTANT: The following Vulkan Message/Warning is Expected/Intended behavior:\n" << std::endl;

		// test by enabling performance and info printouts + sync validation
		const VkBool32 setting_validate_sync = VK_TRUE;
        const std::array<const char *, 2> setting_report_flags{"info", "perf"};
        const VkLayerSettingEXT lyr_sett_pair[] = {
			{
				inst_layer, "report_flags", VK_LAYER_SETTING_TYPE_STRING_EXT,
				static_cast<uint32_t>(setting_report_flags.size()), setting_report_flags.data()
			},
			{
				inst_layer, "validate_sync", VK_LAYER_SETTING_TYPE_BOOL32_EXT, 1, &setting_validate_sync
			}
		};
#else
		const void* lyr_sett_pair[] = { nullptr, nullptr };
#endif

		GW::GReturn capture = vkSurface.Create(GWin, 0ull, 1, &inst_layer, 2, lyr_sett_pair, 1, &inst_ext, 1, &dev_ext, false);

#if defined(VK_EXT_layer_settings)
		REQUIRE(capture == GW::GReturn::SUCCESS);
#else
		REQUIRE(capture == GW::GReturn::SOFTWARE_UNAVAILABLE); // VK_EXT_layer_settings not supported by compiled Vulkan SDK
#endif

		//message spacing
		std::cout << std::endl;
#endif

		//Cleanup
		vkSurface = nullptr;
		GWin = nullptr;
	}

	SECTION("Aspect Ratio Check", "[GetAspectRatio]") {
		//Setup Window, Surface and Aspect Ratio
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		REQUIRE(+vkSurface.Create(GWin, 0ull));
		float aspect_ratio = 0.0f;

		//Oops, lost the GWindow.
		GWin = nullptr;
        
        //Oops, can't lose the window in iOS
        #if !TARGET_OS_IOS
		CHECK(vkSurface.GetAspectRatio(aspect_ratio) == GW::GReturn::PREMATURE_DEALLOCATION);
        #endif
		//Cleanup
		vkSurface = nullptr;
	}

	//Setup Window and Surface
	//Well, it works! Time to.... Darn it! Layers & Extensions!
    
    
    // iOS and tvOS doesn't have support for Vulkan layers since it bypasses the Vulkan Loader completely.
    // The following comments were taken straight from the mobile section under macOS getting started from the LunarG website.
    //      "Linking directly to MoltenVK bypasses the Vulkan Loader and does not allow the use of implicit or explicit Vulkan Layers.
    //      At this time the Vulkan Loader is also not supported on iOS or tvOS devices, and this method is the only means of using Vulkan on those platforms."
    #if !TARGET_OS_IOS
    
	const char* inst_layer = "VK_LAYER_KHRONOS_validation";
	REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
	REQUIRE(+vkSurface.Create(GWin, GW::GRAPHICS::DEPTH_BUFFER_SUPPORT, 1, &inst_layer, 0, nullptr, 0, nullptr, true));

	SECTION("Swapchain Images Tests", "[GetSwapchainImage], [GetSwapchainView], [GetSwapchainFramebuffer],  [GetSwapchainDepthBufferImage],  [GetSwapchainDepthBufferView], [GetCommandBuffer], [GetImageAvailableSemaphore], [GetRenderFinishedSemaphore], [GetRenderFence]") {
		//Again? Forgot to use &vkObj Darn It!
		CHECK(vkSurface.GetSwapchainImage(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainView(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainFramebuffer(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferImage(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferView(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetCommandBuffer(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetImageAvailableSemaphore(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFinishedSemaphore(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFence(-1, nullptr) == GW::GReturn::INVALID_ARGUMENT);

		//Ok... Maybe This should [not] work! (Less than -1) [Note: -1 IS VALID. It returns current index of that object]
		CHECK(vkSurface.GetSwapchainImage(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainView(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainFramebuffer(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferImage(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferView(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetCommandBuffer(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetImageAvailableSemaphore(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFinishedSemaphore(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFence(-2, &vkObj) == GW::GReturn::INVALID_ARGUMENT);

		//So that didn't work, lets get the highest index! (greater than count)
		unsigned int count;
		REQUIRE(+vkSurface.GetSwapchainImageCount(count));
		count++;

		CHECK(vkSurface.GetSwapchainImage(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainView(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainFramebuffer(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferImage(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferView(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetCommandBuffer(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetImageAvailableSemaphore(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFinishedSemaphore(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFence(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);

		//Whoops, I Post Increment on accident, butter fingers! (equal to count.)
		REQUIRE(+vkSurface.GetSwapchainImageCount(count));

		CHECK(vkSurface.GetSwapchainImage(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainView(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainFramebuffer(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferImage(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchainDepthBufferView(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetCommandBuffer(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetImageAvailableSemaphore(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFinishedSemaphore(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetRenderFence(count, &vkObj) == GW::GReturn::INVALID_ARGUMENT);

		//Ok... Arrays start at 0, and -1 returns current object. got it. (Success Chain)
		for (int i = -1; i < static_cast<int>(count); ++i) {
			CHECK(+vkSurface.GetSwapchainImage(i, &vkObj));
			CHECK(+vkSurface.GetSwapchainView(i, &vkObj));
			CHECK(+vkSurface.GetSwapchainFramebuffer(i, &vkObj));
			CHECK(+vkSurface.GetSwapchainDepthBufferImage(i, &vkObj));
			CHECK(+vkSurface.GetSwapchainDepthBufferView(i, &vkObj));
			CHECK(+vkSurface.GetCommandBuffer(i, &vkObj));
			CHECK(+vkSurface.GetImageAvailableSemaphore(i, &vkObj));
			CHECK(+vkSurface.GetRenderFinishedSemaphore(i, &vkObj));
			CHECK(+vkSurface.GetRenderFence(i, &vkObj));
		}
	}

	SECTION("Vulkan Object Getters Test", "[GetInstance], [GetSurface], [GetPhysicalDevice], [GetDevice], [GetCommandPool], [GetSwapchain]") {
		//Create GWindow and GVulkanSurface
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		const char* inst_layer = "VK_LAYER_KHRONOS_validation";
		REQUIRE(+vkSurface.Create(GWin, 0ull, 1, &inst_layer, 0, nullptr, 0, nullptr, true));

		//Arg.... I forgot to use &vkObj Again!
		CHECK(vkSurface.GetInstance(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSurface(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetPhysicalDevice(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetDevice(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetCommandPool(nullptr) == GW::GReturn::INVALID_ARGUMENT);
		CHECK(vkSurface.GetSwapchain(nullptr) == GW::GReturn::INVALID_ARGUMENT);


		//When will i remember to use that on these functions... (Success)
		CHECK(+vkSurface.GetInstance(&vkObj));
		CHECK(+vkSurface.GetSurface(&vkObj));
		CHECK(+vkSurface.GetPhysicalDevice(&vkObj));
		CHECK(+vkSurface.GetDevice(&vkObj));
		CHECK(+vkSurface.GetCommandPool(&vkObj));
		CHECK(+vkSurface.GetSwapchain(&vkObj));

		vkSurface = nullptr;
		GWin = nullptr;
	}
    
    #endif

	SECTION("Start and End Frames", "[StartFrame], [EndFrame]") {
		//Create GWindow and GVulkanSurface
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		REQUIRE(+vkSurface.Create(GWin, GW::GRAPHICS::TRIPLE_BUFFER | GW::GRAPHICS::MSAA_4X_SUPPORT)); 

		//Setup Variables
		VkClearValue myVkClearValue[2]{
			{ 0.0f, 1.0f, 0.0f, 1.0f },
			{ 1.0f, 16777216 }
		};

		//My first time attempting to clear my screen (Failure: count is 0 yet 2nd parameter exists)
		REQUIRE(vkSurface.StartFrame(0, reinterpret_cast<void**>(&myVkClearValue)) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.EndFrame(false) == GW::GReturn::FAILURE);

		//Ok... Second Time! (Failure: count is more than 0 yet 2nd parameter is nullptr)
		REQUIRE(vkSurface.StartFrame(2, nullptr) == GW::GReturn::INVALID_ARGUMENT);
		REQUIRE(vkSurface.EndFrame(false) == GW::GReturn::FAILURE);

		//I am gonna clear my screen! (Success, Black Screen)
		REQUIRE(+vkSurface.StartFrame(0, nullptr));
		REQUIRE(+vkSurface.EndFrame(false));

		//I should draw a screen! (Success, Green Screen)
		REQUIRE(+vkSurface.StartFrame(1, reinterpret_cast<void*>(&myVkClearValue[0])));
		REQUIRE(+vkSurface.EndFrame(false));

		//Play Around (Manual!)
		#if !defined(DISABLE_USER_INPUT_TESTS)
		GW::GEvent GEvent;
		GW::CORE::GEventResponder r;
		auto go_on = [&](const GW::GEvent& event) {
			GW::GRAPHICS::GVulkanSurface::Events windowEvent;
			GW::GRAPHICS::GVulkanSurface::EVENT_DATA windowEventData;
			event.Read(windowEvent, windowEventData);

			switch (windowEvent) {
			case GW::GRAPHICS::GVulkanSurface::Events::REBUILD_PIPELINE:
				printf("VkResult Value: %d\n", windowEventData.eventResult);
				printf("New Surface: (%d, %d)\n", windowEventData.surfaceExtent[0], windowEventData.surfaceExtent[1]);
				break;
			default:
				printf("Not being sent in . . .\n");
			}
		};
		r.Create(go_on);
		vkSurface.Register(r);

		GW::GRAPHICS::GVulkanSurface::Events my_event = GW::GRAPHICS::GVulkanSurface::Events::REBUILD_PIPELINE;
		GW::GRAPHICS::GVulkanSurface::EVENT_DATA event_data;
		event_data.eventResult = VK_ERROR_OUT_OF_DATE_KHR;
		event_data.surfaceExtent[0] = 1920;
		event_data.surfaceExtent[1] = 1080;
		GEvent.Write(my_event, event_data);
		while (+GWin.ProcessWindowEvents()) {
			if (+vkSurface.StartFrame(1, reinterpret_cast<void**>(&myVkClearValue))) {
				vkSurface.EndFrame(false);
			}
		}
		#endif
		vkSurface = nullptr;
		GWin = nullptr;
	}

}

TEST_CASE("The Ultimate Test: Drawing a Triangle [Can Be Manual]", "[Graphics]") {
	if (EnableVulkanTests == false)
	{
		std::cout << "Vulkan/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	//GWindow & Receiver
	GW::SYSTEM::GWindow GWin;
	GW::CORE::GEventResponder GResponder;

	//VkSurface
	GW::GRAPHICS::GVulkanSurface vkSurface;
	std::vector<const char*> layer_sent;
	std::vector<const char*> instExt_sent;
	uint64_t initMask = GW::GRAPHICS::MSAA_4X_SUPPORT | GW::GRAPHICS::TRIPLE_BUFFER;
	GW::CORE::GEventResponder vkResponder;
	VkPhysicalDevice vkPDev;
	VkDevice vkDev;
	VkCommandPool vkCmdPool;
	VkQueue vkQGX;
	uint32_t vkTotalFrames;
	VkSampleCountFlagBits vkMSAA = VK_SAMPLE_COUNT_4_BIT;
	VkRenderPass vkRenderpass;
	VkCommandBuffer vkCommandBuffer;

	//Object
	std::vector<Vertex> Obj_V;
	VkBuffer Obj_VB;
	VkDeviceMemory Obj_VM; //cleaned on line 1150

	std::vector<uint32_t> Obj_I;
	VkBuffer Obj_IB;
	VkDeviceMemory Obj_IM; //cleaned on line 1152

	VkBuffer textureBuffer;
	VkDeviceMemory textureMemory; //cleaned on line 552

	UBO mvp;
	std::vector<VkBuffer> vkMVP_B;
	std::vector<VkDeviceMemory> vkMVP_M; //cleaned on line 1160
	VkDescriptorPool vkDescriptorPool;
	VkDescriptorSetLayout vkDescriptorSetLayout;
	std::vector<VkDescriptorSet> vkDescriptorSet;
	GW::MATH::GMATRIXF model = GW::MATH::GIdentityMatrixF;
	GW::MATH::GMatrix gMatrix;
	VkImage textureImage;
	VkImageView textureImageView;
	VkSampler textureSampler;
	// note that unlike uniform buffers, we don't need one for each "in-flight" frame
	VkDescriptorSet textureDescriptorSet = nullptr; // std::vector<> not required
	VkDescriptorSetLayout textureDescriptorSetLayout = nullptr;

	//Pipeline
	VkPipelineLayout vkPipelineLayout;
	VkPipeline vkPipeline;

	SECTION("Grab the layers I expect will be in all platforms", "[GetPhysicalDevice], [Create]") {
		//GWindow stuff
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));

		GW::GRAPHICS::GVulkanSurface::GVulkanSurfaceQueryInfo* query_info = {};
		vkSurface.Create(GWin, &query_info);

		VkLayerProperties* instance_layers = static_cast<VkLayerProperties*>(query_info->instanceLayerProperties);
		VkExtensionProperties* instance_extensions = static_cast<VkExtensionProperties*>(query_info->instanceExtensionProperties);
		VkExtensionProperties* device_extensions = static_cast<VkExtensionProperties*>(query_info->deviceExtensionProperties);

		//Show Initialization Mask
		printf("Initialization Mask: %llu\n", query_info->initializationMask);

		//Loop through Instance Layers
		printf("Instance Layer Count: %u\n", query_info->instanceLayerCount);
		for (unsigned int i = 0; i < query_info->instanceLayerCount; ++i)
			printf("\t- %s [Impl Version: %u]\n", query_info->instanceLayers[i], instance_layers[i].implementationVersion);

		//Loop through Instance Extensions
		printf("Instance Extension Count: %u\n", query_info->instanceExtensionCount);
		for (unsigned int i = 0; i < query_info->instanceExtensionCount; ++i)
			printf("\t- %s [Spec Version: %u]\n", query_info->instanceExtensions[i], instance_extensions[i].specVersion);

		//Loop through the Device Extensions;
		printf("Device Extension Count: %u\n", query_info->deviceExtensionCount);
		for (unsigned int i = 0; i < query_info->deviceExtensionCount; ++i)
			printf("\t- %s [Spec Version: %u]\n", query_info->deviceExtensions[i], device_extensions[i].specVersion);

		//Clean it up (As this is a test)
		vkSurface = nullptr;
	}

	SECTION("Lets get Drawing!", "[Start Frame], [End Frame]") {
		//Create Window
		REQUIRE(+GWin.Create(0, 0, 640, 480, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
		
		//Setup Receiver [THIS MUST HAPPEN FIRST!]
		GW::CORE::GEventResponder WinResponder;
		WinResponder.Create([&](const GW::GEvent& gEvent) {
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
			gEvent.Read(windowEvent, windowEventData);

			switch (windowEvent) {
			case GW::SYSTEM::GWindow::Events::DESTROY: {
				//Destroy Current Pipeline & Layout
				vkDeviceWaitIdle(vkDev);
				CleanupTriangle(vkDev, vkTotalFrames, vkPipeline, vkPipelineLayout, vkDescriptorSetLayout, vkDescriptorPool, Obj_VB, Obj_VM, Obj_IB, Obj_IM, &vkMVP_B[0], &vkMVP_M[0]);
				//texture cleanup since helper function doesn't take in multiple buffers as of adding texture support
				
				vkDestroyImage(vkDev, textureImage, nullptr);
				vkDestroyImageView(vkDev, textureImageView, nullptr);
				vkDestroyDescriptorSetLayout(vkDev, textureDescriptorSetLayout, nullptr);
				vkDestroySampler(vkDev, textureSampler, nullptr);
				vkDestroyBuffer(vkDev, textureBuffer, nullptr);
				vkFreeMemory(vkDev, textureMemory, nullptr); //texture memory freed

			} break;
			}
        });
        GWin.Register(WinResponder);

		//Query for all things possible
		GW::GRAPHICS::GVulkanSurface::GVulkanSurfaceQueryInfo* qInfo = {};
		vkSurface.Create(GWin, &qInfo);

		//Take whatever is needed
        #if TARGET_OS_IOS // Layers not supported on iOS
        const uint32_t maxLayers = 0;
        const char* tempLayers[maxLayers] = { };
        #else
		const uint32_t maxLayers = 2;
		const char* tempLayers[maxLayers] = {
			"VK_LAYER_KHRONOS_validation",
			"VK_LAYER_LUNARG_standard_validation",
		};
		for (unsigned int i = 0; i < maxLayers; ++i) {
			for (unsigned int j = 0; j < qInfo->instanceLayerCount; ++j)	
				if (!strcmp(tempLayers[i], qInfo->instanceLayers[j])) {
					layer_sent.push_back(tempLayers[i]);
					break;
				}
		}
		#endif
		const uint32_t maxInstExt = 2;
		const char* tempInstExt[maxInstExt] = {
			"VK_EXT_debug_report",
			"VK_EXT_debug_utils"
		};
		for (unsigned int i = 0; i < maxInstExt; ++i) {
			for (unsigned int j = 0; j < qInfo->instanceExtensionCount; ++j)
				if (!strcmp(tempInstExt[i], qInfo->instanceExtensions[j])) {
					instExt_sent.push_back(tempInstExt[i]);
					break;
				}
		}


		//Create a new Vulkan Surface with Everything that is grabbed
		REQUIRE(+vkSurface.Create(GWin, initMask, static_cast<uint32_t>(layer_sent.size()), layer_sent.data(), static_cast<uint32_t>(instExt_sent.size()), instExt_sent.data(), 0, nullptr, true));

		//Gather all the necessary Vulkan objects
		REQUIRE(+vkSurface.GetPhysicalDevice(reinterpret_cast<void**>(&vkPDev)));
		REQUIRE(+vkSurface.GetDevice(reinterpret_cast<void**>(&vkDev)));
		REQUIRE(+vkSurface.GetCommandPool(reinterpret_cast<void**>(&vkCmdPool)));
		REQUIRE(+vkSurface.GetGraphicsQueue(reinterpret_cast<void**>(&vkQGX)));
		REQUIRE(+vkSurface.GetSwapchainImageCount(vkTotalFrames));
		REQUIRE(+vkSurface.GetRenderPass(reinterpret_cast<void**>(&vkRenderpass)));

		//Set up Uniform Buffer Object
		mvp.model = mvp.view = mvp.projection = GW::MATH::GIdentityMatrixF;
		vkMVP_B.resize(vkTotalFrames);
		vkMVP_M.resize(vkTotalFrames);
		for (uint32_t i = 0; i < vkTotalFrames; ++i)
			REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, sizeof(UBO),
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				&vkMVP_B[i], &vkMVP_M[i]) == VK_SUCCESS);

		//Setup all vertices

		Obj_V.resize(4);
		Obj_V[0].pos = { 0.5f, -0.5f,  0.0f,  1.0f };
		Obj_V[1].pos = { 0.5f, 0.5f,  0.0f,  1.0f };
		Obj_V[2].pos = { -0.5f, -0.5f,  0.0f,  1.0f };
		Obj_V[3].pos = { -0.5f, 0.5f,  0.0f,  1.0f };
		//uvs
		Obj_V[0].uv = { 1.0f,  1.0f,  0.0f,  0.0f };
		Obj_V[1].uv = { 1.0f,  0.0f,  0.0f,  0.0f };
		Obj_V[2].uv = { 0.0f,  1.0f,  0.0f,  0.0f };
		Obj_V[3].uv = { 0.0f,  0.0f,  0.0f,  0.0f };

		//Set up staging buffer
		VkDeviceSize buffer_size_VB = sizeof(Vertex) * Obj_V.size();
		VkBuffer staging_buffer_VB;
		VkDeviceMemory staging_buffer_memory_VB; //cleaned on line 644

		//Create the staging buffers
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, buffer_size_VB, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			&staging_buffer_VB, &staging_buffer_memory_VB) == VK_SUCCESS);

		//Copy the staging information over
		void* data_VB;
		REQUIRE(vkMapMemory(vkDev, staging_buffer_memory_VB, 0, buffer_size_VB, 0, &data_VB) == VK_SUCCESS);
		memcpy(data_VB, Obj_V.data(), buffer_size_VB);
		vkUnmapMemory(vkDev, staging_buffer_memory_VB);

		//Create the new Buffer
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, buffer_size_VB, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &Obj_VB, &Obj_VM) == VK_SUCCESS);

		//Copy the staging buffer data to the new buffer
		REQUIRE(GvkHelper::copy_buffer(vkDev, vkCmdPool, vkQGX, staging_buffer_VB, Obj_VB, buffer_size_VB) == VK_SUCCESS);

		vkDestroyBuffer(vkDev, staging_buffer_VB, nullptr);
		vkFreeMemory(vkDev, staging_buffer_memory_VB, nullptr); //staging_buffer_memory_VB cleaned

		Obj_I.resize(6);
		Obj_I[0] = 0;
		Obj_I[1] = 1;
		Obj_I[2] = 2;
		Obj_I[3] = 1;
		Obj_I[4] = 3;
		Obj_I[5] = 2;

		//Set up Vertex staging buffer
		VkDeviceSize buffer_size = sizeof(Vertex) * Obj_I.size();
		VkBuffer staging_buffer;
		VkDeviceMemory staging_buffer_memory;

		//Create the staging buffers
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			&staging_buffer, &staging_buffer_memory) == VK_SUCCESS);

		//Copy the staging information over
		void* data;
		REQUIRE(vkMapMemory(vkDev, staging_buffer_memory, 0, buffer_size, 0, &data) == VK_SUCCESS);
		memcpy(data, Obj_I.data(), buffer_size);
		vkUnmapMemory(vkDev, staging_buffer_memory); //cleaned on line 674

		//Create the new Buffer
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &Obj_IB, &Obj_IM) == VK_SUCCESS);

		//Copy the staging buffer data to the new buffer
		REQUIRE(GvkHelper::copy_buffer(vkDev, vkCmdPool, vkQGX, staging_buffer, Obj_IB, buffer_size) == VK_SUCCESS);

		vkDestroyBuffer(vkDev, staging_buffer, nullptr);
		vkFreeMemory(vkDev, staging_buffer_memory, nullptr); //staging_buffer_memory cleaned

		//Set up Texture staging buffer
		VkDeviceSize imageSize = ShipPaintingARGB_width * ShipPaintingARGB_height * 4;
		VkBuffer staging_bufferIM;
		VkDeviceMemory staging_buffer_memoryIM; //cleaned on line 744
		VkDeviceMemory transitionMemory; //cleaned on line 744

		//Create the staging buffers
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, imageSize, 
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			&staging_bufferIM, &staging_buffer_memoryIM) == VK_SUCCESS);

		//Copy the staging information over
		GvkHelper::write_to_buffer(vkDev, staging_buffer_memoryIM, ShipPaintingARGB_pixels, static_cast<unsigned int>(imageSize));
		//Create the new Buffer
		REQUIRE(GvkHelper::create_buffer(vkPDev, vkDev, imageSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &textureBuffer, &transitionMemory) == VK_SUCCESS);

		//Copy the staging buffer data to the new buffer
		REQUIRE(GvkHelper::copy_buffer(vkDev, vkCmdPool, vkQGX, staging_bufferIM, textureBuffer, imageSize) == VK_SUCCESS);

		VkExtent3D tempextent = {};
		tempextent.width = ShipPaintingARGB_width;
		tempextent.height = ShipPaintingARGB_height;
		tempextent.depth = 1;
		uint32_t miplvls = static_cast<uint32_t>(floor(log2(std::max(ShipPaintingARGB_width, ShipPaintingARGB_height))) + 1);
		REQUIRE(GvkHelper::create_image(vkPDev, vkDev, tempextent, miplvls, VK_SAMPLE_COUNT_1_BIT, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, nullptr,
			&textureImage, &textureMemory) == VK_SUCCESS);

		
		//transition
		GvkHelper::transition_image_layout(vkDev, vkCmdPool, vkQGX, miplvls, textureImage, VK_FORMAT_R8G8B8A8_UNORM,
			VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

		REQUIRE(GvkHelper::copy_buffer_to_image(vkDev, vkCmdPool, vkQGX, staging_bufferIM, textureImage, tempextent) == VK_SUCCESS);

		//create mipmaps

		GvkHelper::create_mipmaps(vkDev, vkCmdPool, vkQGX, textureImage, ShipPaintingARGB_width, ShipPaintingARGB_height, ShipPaintingARGB_numlevels);

		// create the the image view and sampler
		VkSamplerCreateInfo samplerInfo = {};
		// Set the struct values
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.flags = 0;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER; // REPEAT IS COMMON
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0;
		samplerInfo.minLod = 0;
		samplerInfo.maxLod = ShipPaintingARGB_numlevels;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 4.0;
		samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_LESS;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.pNext = nullptr;
		VkResult vr = vkCreateSampler(vkDev, &samplerInfo, nullptr, &textureSampler);

		REQUIRE(GvkHelper::create_image_view(vkDev, textureImage, VK_FORMAT_R8G8B8A8_UNORM,
			VK_IMAGE_ASPECT_COLOR_BIT, miplvls, nullptr, &textureImageView) == VK_SUCCESS);

		

		vkDestroyBuffer(vkDev, staging_bufferIM, nullptr);
		vkFreeMemory(vkDev, staging_buffer_memoryIM, nullptr); //staging buffer IM cleaned
		vkFreeMemory(vkDev, transitionMemory, nullptr); //staging buffer IM cleaned

		//Descriptor Pool
		std::array<VkDescriptorPoolSize, 2> dpsSizes = {};
		dpsSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		dpsSizes[0].descriptorCount = 0xFF; 
		dpsSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		dpsSizes[1].descriptorCount = 0xFF;

		VkDescriptorPoolCreateInfo dp_create_info = {};
		dp_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		dp_create_info.poolSizeCount = dpsSizes.size(); //prevents hardcoding, other wise it would've been 2 anyway
		dp_create_info.pPoolSizes = dpsSizes.data();
		dp_create_info.maxSets = 0xFF;
		vkCreateDescriptorPool(vkDev, &dp_create_info, nullptr, &vkDescriptorPool);

		//Descriptor Set Layout
		VkDescriptorSetLayoutBinding vs_ubo = {};
		vs_ubo.binding = 0;
		vs_ubo.descriptorCount = 1;
		vs_ubo.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		vs_ubo.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

		//Descriptor Set for Textures
		VkDescriptorSetLayoutBinding samplerLayoutBinding{};
		samplerLayoutBinding.binding = 0;
		samplerLayoutBinding.descriptorCount = 1;
		samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		samplerLayoutBinding.pImmutableSamplers = nullptr;

		VkDescriptorSetLayoutCreateInfo dsl_create_info = {};
		dsl_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		dsl_create_info.bindingCount = 1;
		dsl_create_info.pBindings = &vs_ubo;

		VkDescriptorSetLayoutCreateInfo tex_dsl_create_info = {};
		tex_dsl_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		tex_dsl_create_info.bindingCount = 1;
		tex_dsl_create_info.pBindings = &samplerLayoutBinding; //access image sampler
		//Create descriptor set layouts!
		vkCreateDescriptorSetLayout(vkDev, &dsl_create_info, nullptr, &vkDescriptorSetLayout);
		vkCreateDescriptorSetLayout(vkDev, &tex_dsl_create_info, nullptr, &textureDescriptorSetLayout);

		//Descriptor Sets
		vkDescriptorSet.resize(vkTotalFrames);
		std::vector<VkDescriptorSetLayout> dsl_list(vkTotalFrames, vkDescriptorSetLayout);
		VkDescriptorSetAllocateInfo ds_allocate_info = {};
		ds_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		ds_allocate_info.descriptorSetCount = vkTotalFrames;
		ds_allocate_info.descriptorPool = vkDescriptorPool;
		ds_allocate_info.pSetLayouts = &dsl_list[0];
		vkAllocateDescriptorSets(vkDev, &ds_allocate_info, vkDescriptorSet.data());
		// Create a descriptor set for our texture!
		VkDescriptorSetAllocateInfo tex_ds_allocate_info = {};
		tex_ds_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		tex_ds_allocate_info.descriptorSetCount = 1;
		tex_ds_allocate_info.pSetLayouts = &textureDescriptorSetLayout;
		tex_ds_allocate_info.descriptorPool = vkDescriptorPool;
		tex_ds_allocate_info.pNext = nullptr;
		vkAllocateDescriptorSets(vkDev, &tex_ds_allocate_info, &textureDescriptorSet);



		for (uint32_t i = 0; i < vkTotalFrames; ++i)
		{
			VkDescriptorBufferInfo dbi = {};
			dbi.buffer = vkMVP_B[i];
			dbi.offset = 0;
			dbi.range = sizeof(UBO);

			VkWriteDescriptorSet wds = {};
			//vertex
			wds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			wds.dstSet = vkDescriptorSet[i];
			wds.dstBinding = 0; //has to be 0?
			wds.dstArrayElement = 0;
			wds.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			wds.descriptorCount = 1;
			wds.pBufferInfo = &dbi;
			wds.pImageInfo = nullptr;
			wds.pTexelBufferView = nullptr;
			wds.pNext = nullptr;

			vkUpdateDescriptorSets(vkDev, 1, &wds, 0, nullptr); //0 has the vertex data
		}
		VkDescriptorImageInfo imi{};
		imi.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imi.imageView = textureImageView;
		imi.sampler = textureSampler;

		//image sampler
		VkWriteDescriptorSet imwds = {};
		imwds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		imwds.dstSet = textureDescriptorSet;
		imwds.dstBinding = 0; // has to be 0 because we're using a different descriptor set
		imwds.dstArrayElement = 0;
		imwds.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		imwds.descriptorCount = 1;
		imwds.pBufferInfo = nullptr;
		imwds.pImageInfo = &imi;
		imwds.pTexelBufferView = nullptr;
		imwds.pNext = nullptr;


		vkUpdateDescriptorSets(vkDev, 1, &imwds, 0, nullptr); //1 is for the image sampler

		VkExtent2D swapchainExtent = {};
		GWin.GetClientWidth(swapchainExtent.width);
		GWin.GetClientHeight(swapchainExtent.height);
		CreatePipeline(vkDev, swapchainExtent, vkMSAA, vkDescriptorSetLayout, textureDescriptorSetLayout, vkRenderpass, vkPipelineLayout, vkPipeline);

		//Receiver for when to rebuild pipeline
		GResponder.Create([&](const GW::GEvent& gEvent) {
			GW::GRAPHICS::GVulkanSurface::Events windowEvent;
            GW::GRAPHICS::GVulkanSurface::EVENT_DATA windowEventData;
			gEvent.Read(windowEvent, windowEventData);

			switch (windowEvent) {
			case GW::GRAPHICS::GVulkanSurface::Events::REBUILD_PIPELINE: {
                //Destroy Current Pipeline & Layout
				if (vkPipeline) vkDestroyPipeline(vkDev, vkPipeline, nullptr);
				if (vkPipelineLayout) vkDestroyPipelineLayout(vkDev, vkPipelineLayout, nullptr);
				//if (textureSampler) vkDestroySampler(vkDev, textureSampler, nullptr);
                
				//Re-obtain RenderPass
				REQUIRE(+vkSurface.GetRenderPass(reinterpret_cast<void**>(&vkRenderpass)));

				//Recreate Graphic Pipeline
				CreatePipeline(vkDev, { windowEventData.surfaceExtent[0], windowEventData.surfaceExtent[1] }, vkMSAA, vkDescriptorSetLayout, textureDescriptorSetLayout, vkRenderpass, vkPipelineLayout, vkPipeline);
			} break;
			default:
				printf("Not being sent in . . .\n");
			}
        });
        vkSurface.Register(GResponder);

		uint32_t current_frame = 0;
		VkDeviceSize offset = 0;
		gMatrix.Create();
		GW::MATH::GMATRIXF rotz;
		gMatrix.RotateZLocalF(GW::MATH::GIdentityMatrixF, 0.785398f * 0.00001f, rotz); //Make Rotation Matrix

		// stop the test after 3 sec
		auto start = std::chrono::steady_clock::now();
        
        VkClearValue clear_color = {0.0f, 0.0f, 0.0f, 1.0f};
        
		while (+GWin.ProcessWindowEvents()) {
#ifdef DISABLE_USER_INPUT_TESTS
			auto end = std::chrono::steady_clock::now();
			auto lapse = std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
			if (lapse > 3) break; //disable if you want to debug with renderdoc
#endif
			if (+vkSurface.StartFrame(0, nullptr)) {
                //Get Command Buffer & Current Frame
                GW::GReturn greturn = vkSurface.GetCommandBuffer(-1, reinterpret_cast<void**>(&vkCommandBuffer));
				vkSurface.GetSwapchainCurrentImage(current_frame);

				//Update UBO
				gMatrix.MultiplyMatrixF(model, rotz, model);

				UBO new_ubo;
				new_ubo.model = model;
				new_ubo.view = GW::MATH::GIdentityMatrixF;
				new_ubo.projection = GW::MATH::GIdentityMatrixF;

				GvkHelper::write_to_buffer(vkDev, vkMVP_M[current_frame], &new_ubo, sizeof(UBO));

				vkCmdBindPipeline(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vkPipeline);
				vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vkPipelineLayout, 0, 1, &vkDescriptorSet[current_frame], 0, nullptr);
				vkCmdBindVertexBuffers(vkCommandBuffer, 0, 1, &Obj_VB, &offset);
				vkCmdBindIndexBuffer(vkCommandBuffer, Obj_IB, 0, VK_INDEX_TYPE_UINT32);

				vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
					vkPipelineLayout, 1, 1, &textureDescriptorSet, 0, nullptr);

				vkCmdDrawIndexed(vkCommandBuffer, 6, 1, 0, 0, 0);
 
				vkSurface.EndFrame(true);
			}
		}

		//Cleanup Surface
		GWin = nullptr;
		vkSurface = nullptr;
	}

}

VkResult CreatePipeline(const VkDevice& _vkDevice, const VkExtent2D _swapchainExtent, const VkSampleCountFlagBits &_MSAABit, const VkDescriptorSetLayout& _descriptorSetLayout, const VkDescriptorSetLayout& _pixelDescriptorLayout, const VkRenderPass& _vkRenderPass,
	VkPipelineLayout& _pipelineLayout, VkPipeline& _graphicsPipeline) {
	//Setup Shader Info
	VkShaderModule shader[2] = {};
	VkPipelineShaderStageCreateInfo stage_create_info[2] = {};
	
	//Create the GFile
	const char* vsFilename = "../../../UnitTests/Resources/GVulkanSurface_TestFolder/vert.spv";
	const char* psFilename = "../../../UnitTests/Resources/GVulkanSurface_TestFolder/frag.spv";
//#endif

	GW::SYSTEM::GFile ShaderFile; ShaderFile.Create();
    
#if TARGET_OS_IOS
    // DON'T DO THIS
    // This is done just because we do not have a unified system for getting the UnitTests folder on every platform.
    // The best solution would be to use GFile.GetInstallFolder() and GFile.SetWorkingDirectory()
    std::string vsLocation = [[[NSBundle mainBundle] bundlePath] UTF8String];
    vsLocation.append("/UnitTests/Resources/");
    const char* vsFilename = "basic.vert.spv";
    const char* psFilename = "basic.frag.spv";
    ShaderFile.SetCurrentWorkingDirectory(vsLocation.c_str());
#endif

	//Get the size of the Vertex Shader
	uint32_t vsFileSize;
	ShaderFile.GetFileSize(vsFilename, vsFileSize);

	//Open the Vertex Shader
	if (-ShaderFile.OpenBinaryRead(vsFilename))
		return VK_ERROR_FEATURE_NOT_PRESENT;

	//Copy the Contents of the Vertex Shader
	char* tempShaderFile = new char[vsFileSize];
	ShaderFile.Read(tempShaderFile, vsFileSize);

	//Create Shader Module for Vertex Shader
	VkShaderModuleCreateInfo vsModuleCreateInfo = {};
	vsModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	vsModuleCreateInfo.codeSize = vsFileSize;
	vsModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(tempShaderFile);
	vkCreateShaderModule(_vkDevice, &vsModuleCreateInfo, VK_NULL_HANDLE, &shader[0]);

	//Create Stage Info for Vertex Shader
	stage_create_info[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	stage_create_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	stage_create_info[0].module = shader[0];
	stage_create_info[0].pName = "main";

	//Cleanup
	delete[] tempShaderFile;
	ShaderFile.CloseFile();

	//Get the size of the Fragment Shader
	uint32_t psFileSize;
	ShaderFile.GetFileSize(psFilename, psFileSize);

	//Open the Fragment Shader
	if (-ShaderFile.OpenBinaryRead(psFilename))
		return VK_ERROR_FEATURE_NOT_PRESENT;

	//Copy the Contents of the Fragment Shader
	tempShaderFile = new char[psFileSize];
	ShaderFile.Read(tempShaderFile, psFileSize);

	//Create Shader Module for Fragment Shader
	VkShaderModuleCreateInfo psModuleCreateInfo = {};
	psModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	psModuleCreateInfo.codeSize = psFileSize;
	psModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(tempShaderFile);
	vkCreateShaderModule(_vkDevice, &psModuleCreateInfo, VK_NULL_HANDLE, &shader[1]);

	//Create Stage Info for Fragment Shader
	stage_create_info[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	stage_create_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	stage_create_info[1].module = shader[1];
	stage_create_info[1].pName = "main";

	//Cleanup
	delete[] tempShaderFile;
	ShaderFile.CloseFile();

	//Assembly State
	VkPipelineInputAssemblyStateCreateInfo assembly_create_info = {};
	assembly_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	assembly_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
	assembly_create_info.primitiveRestartEnable = false;

	//Vertex Input State
	VkVertexInputBindingDescription vertex_binding_description = Vertex::get_binding_description();
	VkVertexInputAttributeDescription* vertex_attribute_description = Vertex::get_attribute_description();

	VkPipelineVertexInputStateCreateInfo input_vertex_info = {};
	input_vertex_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	input_vertex_info.vertexBindingDescriptionCount = 1;
	input_vertex_info.pVertexBindingDescriptions = &vertex_binding_description;
	input_vertex_info.vertexAttributeDescriptionCount = 2;
	input_vertex_info.pVertexAttributeDescriptions = vertex_attribute_description;

	//Viewport State
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(_swapchainExtent.width);
	viewport.height = static_cast<float>(_swapchainExtent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset = { 0,0 };
	scissor.extent = _swapchainExtent;

	VkPipelineViewportStateCreateInfo viewport_create_info = {};
	viewport_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewport_create_info.viewportCount = 1;
	viewport_create_info.pViewports = &viewport;
	viewport_create_info.scissorCount = 1;
	viewport_create_info.pScissors = &scissor;

	//Rasterizer State
	VkPipelineRasterizationStateCreateInfo rasterization_create_info = {};
	rasterization_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterization_create_info.rasterizerDiscardEnable = VK_FALSE;
	rasterization_create_info.polygonMode = VK_POLYGON_MODE_FILL;
	rasterization_create_info.lineWidth = 1.0f;
	rasterization_create_info.cullMode = VK_CULL_MODE_NONE;
	rasterization_create_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterization_create_info.depthClampEnable = VK_FALSE;
	rasterization_create_info.depthBiasEnable = VK_FALSE;
	rasterization_create_info.depthBiasClamp = 0.0f;
	rasterization_create_info.depthBiasConstantFactor = 0.0f;
	rasterization_create_info.depthBiasSlopeFactor = 0.0f;

	//Multisampling State
	VkPipelineMultisampleStateCreateInfo multisample_create_info = {};
	multisample_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisample_create_info.sampleShadingEnable = VK_TRUE; 
	multisample_create_info.rasterizationSamples = _MSAABit;
	multisample_create_info.minSampleShading = 1.0f;
	multisample_create_info.pSampleMask = VK_NULL_HANDLE;
	multisample_create_info.alphaToCoverageEnable = VK_FALSE;
	multisample_create_info.alphaToOneEnable = VK_FALSE;

	//Depth-Stencil State
	VkPipelineDepthStencilStateCreateInfo depth_stencil_create_info = {};
	depth_stencil_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depth_stencil_create_info.depthTestEnable = VK_FALSE;
	depth_stencil_create_info.depthWriteEnable = VK_FALSE;
	depth_stencil_create_info.depthCompareOp = VK_COMPARE_OP_LESS;
	depth_stencil_create_info.depthBoundsTestEnable = VK_FALSE;
	depth_stencil_create_info.minDepthBounds = 0.0f;
	depth_stencil_create_info.maxDepthBounds = 1.0f;
	depth_stencil_create_info.stencilTestEnable = VK_FALSE;

	//Color Blending Attachment & State
	VkPipelineColorBlendAttachmentState color_blend_attachment_state = {};
	color_blend_attachment_state.colorWriteMask = 0xF; //<-- RGBA Flags on... although blend is disabled
	color_blend_attachment_state.blendEnable = VK_FALSE;
	color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
	color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_DST_COLOR;
	color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
	color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_DST_ALPHA;
	color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo color_blend_create_info = {};
	color_blend_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	color_blend_create_info.logicOpEnable = VK_FALSE;
	color_blend_create_info.logicOp = VK_LOGIC_OP_COPY;
	color_blend_create_info.attachmentCount = 1;
	color_blend_create_info.pAttachments = &color_blend_attachment_state;
	color_blend_create_info.blendConstants[0] = 0.0f;
	color_blend_create_info.blendConstants[1] = 0.0f;
	color_blend_create_info.blendConstants[2] = 0.0f;
	color_blend_create_info.blendConstants[3] = 0.0f;

	//Dynamic State [DISABLED.... But still showing for tutorial reasons that it exists]
	VkPipelineDynamicStateCreateInfo dynamic_create_info = {};
	dynamic_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamic_create_info.dynamicStateCount = 0;
	dynamic_create_info.pDynamicStates = VK_NULL_HANDLE;

	//Descriptor pipeline layout [NOTE: NEEDED FOR UNIFORM BUFFERS!, but for now not using.]
	VkDescriptorSetLayout layouts[2] = { _descriptorSetLayout, _pixelDescriptorLayout };
	VkPipelineLayoutCreateInfo pipeline_layout_create_info = {};
	pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipeline_layout_create_info.setLayoutCount = 2;
	pipeline_layout_create_info.pSetLayouts = layouts;
	pipeline_layout_create_info.pushConstantRangeCount = 0;
	pipeline_layout_create_info.pPushConstantRanges = nullptr;
	VkResult r = vkCreatePipelineLayout(_vkDevice, &pipeline_layout_create_info, nullptr, &_pipelineLayout);
	if (r) {
		delete[] vertex_attribute_description;
		vkDestroyShaderModule(_vkDevice, shader[0], nullptr);
		vkDestroyShaderModule(_vkDevice, shader[1], nullptr);
		return r;
	}

	VkGraphicsPipelineCreateInfo pipeline_create_info = {};
	pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipeline_create_info.stageCount = 2;
	pipeline_create_info.pStages = stage_create_info;
	pipeline_create_info.pInputAssemblyState = &assembly_create_info;
	pipeline_create_info.pVertexInputState = &input_vertex_info;
	pipeline_create_info.pViewportState = &viewport_create_info;
	pipeline_create_info.pRasterizationState = &rasterization_create_info;
	pipeline_create_info.pMultisampleState = &multisample_create_info;
	pipeline_create_info.pDepthStencilState = &depth_stencil_create_info;
	pipeline_create_info.pColorBlendState = &color_blend_create_info;
	pipeline_create_info.pDynamicState = VK_NULL_HANDLE;

	pipeline_create_info.layout = _pipelineLayout;
	pipeline_create_info.renderPass = _vkRenderPass;
	pipeline_create_info.subpass = 0;

	pipeline_create_info.basePipelineHandle = VK_NULL_HANDLE;
	pipeline_create_info.basePipelineIndex = -1;

	r = vkCreateGraphicsPipelines(_vkDevice, VK_NULL_HANDLE, 1, &pipeline_create_info, nullptr, &_graphicsPipeline);

	//Cleanup
	delete[] vertex_attribute_description;
	vkDestroyShaderModule(_vkDevice, shader[0], nullptr);
	vkDestroyShaderModule(_vkDevice, shader[1], nullptr);
	return r;
}

void CleanupTriangle(const VkDevice& _device, const uint32_t& _totalFrames, VkPipeline& _pipeline, VkPipelineLayout& _pipelineLayout, VkDescriptorSetLayout& _descriptorSetLayout, VkDescriptorPool& _descriptorPool,
	VkBuffer& _vertexBuffer, VkDeviceMemory& _vertexMemory, VkBuffer& _indexBuffer, VkDeviceMemory& _indexMemory, VkBuffer* _uboBuffer, VkDeviceMemory* _uboMemory) {
	//Cleanup VBO, and IBO
	vkDestroyBuffer(_device, _vertexBuffer, nullptr);
	vkFreeMemory(_device, _vertexMemory, nullptr);
	vkDestroyBuffer(_device, _indexBuffer, nullptr);
	vkFreeMemory(_device, _indexMemory, nullptr);

	//Cleanup Pipeline & Layout
	if (_pipeline) vkDestroyPipeline(_device, _pipeline, nullptr);
	if (_pipelineLayout) vkDestroyPipelineLayout(_device, _pipelineLayout, nullptr);

	//Cleanup Descriptor Set Layout and Pool
	if (_descriptorSetLayout) vkDestroyDescriptorSetLayout(_device, _descriptorSetLayout, nullptr);
	if (_descriptorPool) vkDestroyDescriptorPool(_device, _descriptorPool, nullptr);

	//Cleanup Uniform Buffer
	for (uint32_t i = 0; i < _totalFrames; ++i) {
		vkDestroyBuffer(_device, _uboBuffer[i], nullptr);
		vkFreeMemory(_device, _uboMemory[i], nullptr);
	}
}
#endif /* defined(GATEWARE_ENABLE_GRAPHICS) & !defined(GATEWARE_DISABLE_GVULKANSURFACE) */
