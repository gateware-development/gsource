#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GDIRECTX11SURFACE)

#include "../../Source/Shared/GEnvironment.hpp"

#if defined(_WIN32)

#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")


bool EnableD3D11Tests = false;
TEST_CASE("Check for D3D11 hardware and Window support", "[Graphics]")
{
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface d3dSurface;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		if (d3dSurface.Create(window, 0) != GW::GReturn::HARDWARE_UNAVAILABLE)
		{
			EnableD3D11Tests = true;
			std::cout << "D3D11 HARDWARE DETECTED: RUNNING ALL D3D11 UNIT TESTS" << std::endl;
		}
		else
			std::cout << "WARNING: D3D11 HARDWARE NOT DETECTED: SKIPPING ALL D3D11 UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL D3D11 UNIT TESTS" << std::endl;
}

TEST_CASE("GDirectX11Surface core method test battery", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	GW::GEvent event;
	GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;
	float aspectRatio = 0.0f;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;
	ID3D11RenderTargetView* rtv;
	ID3D11DepthStencilView* dsv;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(dx11Surface.Assign([]() {}) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.Invoke() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetAspectRatio(aspectRatio) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetRenderTargetView((void**)&rtv) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx11Surface.GetDepthStencilView((void**)&dsv) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);
		dx11Surface = nullptr;
		window = nullptr;
		REQUIRE(!dx11Surface);
		REQUIRE(!window);
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

		REQUIRE(event.Write(GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED, windowEventData) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.Assign([]() {}) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.Invoke() == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetAspectRatio(aspectRatio) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetRenderTargetView((void**)&rtv) == GW::GReturn::SUCCESS);
		REQUIRE(dx11Surface.GetDepthStencilView((void**)&dsv) == GW::GReturn::SUCCESS);

		device->Release();
		context->Release();
		swapchain->Release();
		rtv->Release();
		dsv->Release();
	}
}

TEST_CASE("Querying DXSurface Information.", "[GetDevice], [GetImmediateContext], [GetSwapchain], [Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;
	ID3D11DepthStencilView* dsv;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	DXGI_SWAP_CHAIN_DESC tempDesc;
	swapchain->GetDesc(&tempDesc);
	const char* format = nullptr;

	if (tempDesc.BufferDesc.Format == DXGI_FORMAT_R8G8B8A8_UNORM)
		format = "8-BIT COLOR (DXGI_FORMAT_R8G8B8A8_UNORM)";
	else if (tempDesc.BufferDesc.Format == DXGI_FORMAT_R10G10B10A2_UNORM)
		format = "10-BIT COLOR (DXGI_FORMAT_R10G10B10A2_UNORM)";
	else if (tempDesc.BufferDesc.Format == DXGI_FORMAT_B8G8R8A8_UNORM)
		format = "DIRECT2D SUPPORT (DXGI_FORMAT_B8G8R8A8_UNORM)";

	std::cout << "\n" << "DIRECTX 11 INFORMATION" << "\n";
	std::cout << "COLOR FORMAT: " << format << "\n";
	std::cout << "DEPTH BUFFER ENABLED: ";

	if (dx11Surface.GetDepthStencilView((void**)&dsv) == GW::GReturn::SUCCESS)
		std::cout << "YES" << "\n";
	else
		std::cout << "NO" << "\n";
	// release DX handles as soon as we no longer need them
	device->Release();
	context->Release();
	swapchain->Release();
	dsv->Release();
}

#if defined(GATEWARE_ENV_DESKTOP)

TEST_CASE("Direct2D Support", "[GetFactory], [GetHwndRenderTarget], [Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	DXGI_SWAP_CHAIN_DESC tempDesc;
	swapchain->GetDesc(&tempDesc);

	if (tempDesc.BufferDesc.Format == DXGI_FORMAT_B8G8R8A8_UNORM)
	{
		GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH;
		window.GetWindowHandle(UWH);
		RECT windowRect;
		GetWindowRect((HWND)UWH.window, &windowRect);

		ID2D1Factory* factory;

		HRESULT hr = D2D1CreateFactory(
			D2D1_FACTORY_TYPE_MULTI_THREADED,
			&factory
		);
		std::cout << "Factory Created \n";

		ID2D1HwndRenderTarget* hrt;

		hr = factory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(
				(HWND)UWH.window, D2D1::SizeU(windowRect.right, windowRect.bottom)),
			&hrt);
		std::cout << "HwndRenderTarget Created \n";


		D2D1::ColorF clear = D2D1::ColorF::CornflowerBlue;
		D2D1::ColorF red = D2D1::ColorF::Red;

		hrt->BeginDraw();
		hrt->Clear(clear);

		ID2D1SolidColorBrush* brush;
		hrt->CreateSolidColorBrush(red, &brush);

		hrt->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(300.0f, 300.0f), 100.0f, 100.0f), brush, 5.0f);

		hrt->EndDraw();

		if (hrt) hrt->Release();
		if (brush) brush->Release();
		if (factory) factory->Release();

	}
	device->Release();
	context->Release();
	swapchain->Release();
}

TEST_CASE("Testing Window Events.", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;
	ID3D11RenderTargetView* rtv;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	unsigned int testWidth;
	unsigned int testHeight;

	// Checking Current Size of Window
	window.GetClientWidth(testWidth);
	window.GetClientHeight(testHeight);

	float color[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
	dx11Surface.GetRenderTargetView((void**)&rtv);

	context->ClearRenderTargetView(rtv, color);
	swapchain->Present(0, 0);
	// Immediately release RTV when done using
	rtv->Release();

	// Resizing Window
	window.ResizeWindow(500, 500);

	// Checking New Size of Window After Resize
	window.GetClientWidth(testWidth);
	window.GetClientHeight(testHeight);

	if (swapchain != nullptr)
	{
		ID3D11Texture2D* testTex;
		HRESULT result = swapchain->GetBuffer(0, __uuidof(testTex), reinterpret_cast<void**>(&testTex));

		D3D11_TEXTURE2D_DESC testTexDesc;
		testTex->GetDesc(&testTexDesc);

		unsigned int surfaceWidth = testTexDesc.Width;
		unsigned int surfaceHeight = testTexDesc.Height;


		REQUIRE(surfaceWidth == testWidth);
		REQUIRE(surfaceHeight == testHeight);


		if (context != nullptr)
		{
			color[0] = 0.0f;
			color[1] = 1.0f;
			// Surface must be re-aquired after resize above
			dx11Surface.GetRenderTargetView((void**)&rtv);

			context->ClearRenderTargetView(rtv, color);
			// again get rid of your handles as soon as you are done with them
			rtv->Release();

			swapchain->Present(0, 0);
		}

		testTex->Release();

	}
	// release all main test handles
	context->Release();
	device->Release();
	swapchain->Release();
}

TEST_CASE("DirectX HDR10/MSAA Not Supported", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// Only need to test for these two flags, as the rest are optional.
	constexpr unsigned long long initMask = GW::GRAPHICS::MSAA_4X_SUPPORT | GW::GRAPHICS::COLOR_10_BIT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::FEATURE_UNSUPPORTED);
}

TEST_CASE("DirectX No MSAA", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x2 MSAA Support", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_2X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x4 MSAA Support", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_4X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x8 MSAA Support", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_8X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x16 MSAA Support", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_16X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

#elif defined(GATEWARE_ENV_APP)
TEST_CASE("Direct2D Support", "[GetFactory], [GetHwndRenderTarget], [Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain1* swapchain;
	//winrt::com_ptr<IDXGISwapChain1> swapchainPtr;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	DXGI_SWAP_CHAIN_DESC1 tempDesc;
	swapchain->GetDesc1(&tempDesc);

	if (tempDesc.Format == DXGI_FORMAT_B8G8R8A8_UNORM)
	{
		GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH;
		window.GetWindowHandle(UWH);

		ID2D1Factory7* factory;

		HRESULT hr = D2D1CreateFactory(
			D2D1_FACTORY_TYPE_MULTI_THREADED,
			&factory
		);
		std::cout << "Factory Created \n";
		winrt::com_ptr<IDXGISurface> surface;
		swapchain->GetBuffer(0, __uuidof(&surface), reinterpret_cast<void**>(&surface));
		//swapchainPtr.attach(swapchain);
		//surface.capture(swapchainPtr, &IDXGISwapChain1::GetBuffer, 0);
		float dpi = 0;
		auto process = winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher().RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal,
			[&]()
			{
				// this needs to run on the UI thread
				auto currentDisplayInfo = winrt::Windows::Graphics::Display::DisplayInformation::GetForCurrentView();
				dpi = currentDisplayInfo.LogicalDpi();
			});
		process.get();

		D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(
			D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN,
				D2D1_ALPHA_MODE_PREMULTIPLIED), dpi, dpi);

		ID2D1RenderTarget* hrt = nullptr;

		factory->CreateDxgiSurfaceRenderTarget(surface.get(), &props, &hrt);

		
		std::cout << "DXGI Surface RenderTarget Created \n";


		D2D1::ColorF clear = D2D1::ColorF::CornflowerBlue;
		D2D1::ColorF red = D2D1::ColorF::Red;

		hrt->BeginDraw();
		hrt->Clear(clear);

		ID2D1SolidColorBrush* brush = nullptr;
		hrt->CreateSolidColorBrush(red, &brush);

		hrt->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(300.0f, 300.0f), 100.0f, 100.0f), brush, 5.0f);

		hrt->EndDraw();

		swapchain->Present(1, 0);

		if (hrt) hrt->Release();
		if (brush) brush->Release();
		if (factory) factory->Release();
		if (surface) surface->Release();
		
	}
	device->Release();
	context->Release();
	swapchain->Release();
}

TEST_CASE("Testing Window Events.", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::DIRECT2D_SUPPORT;

	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	GW::SYSTEM::GWindow window;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;
	ID3D11RenderTargetView* rtv;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	unsigned int testWidth;
	unsigned int testHeight;

	// Checking Current Size of Window
	window.GetClientWidth(testWidth);
	window.GetClientHeight(testHeight);

	float color[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
	dx11Surface.GetRenderTargetView((void**)&rtv);

	context->ClearRenderTargetView(rtv, color);
	swapchain->Present(0, 0);
	// Immediately release RTV when done using
	rtv->Release();

	// Resizing Window
	window.ResizeWindow(500, 500);

	// Checking New Size of Window After Resize
	window.GetClientWidth(testWidth);
	window.GetClientHeight(testHeight);

	if (swapchain != nullptr)
	{
		ID3D11Texture2D* testTex;
		HRESULT result = swapchain->GetBuffer(0, __uuidof(testTex), reinterpret_cast<void**>(&testTex));

		D3D11_TEXTURE2D_DESC testTexDesc;
		testTex->GetDesc(&testTexDesc);

		unsigned int surfaceWidth = testTexDesc.Width;
		unsigned int surfaceHeight = testTexDesc.Height;

		GAMING_DEVICE_MODEL_INFORMATION info = {};
		GetGamingDeviceModelInformation(&info);
		if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
		{
			switch (info.deviceId)
			{
			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:

			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:
				REQUIRE(surfaceWidth == 1920);
				REQUIRE(surfaceHeight == 1080);
				break;
			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:

			case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
			default:
				REQUIRE(surfaceWidth == 3840);
				REQUIRE(surfaceHeight == 2160);
				break;
			}
		}
		else
		{
			REQUIRE(surfaceWidth == testWidth);
			REQUIRE(surfaceHeight == testHeight);
		}

		if (context != nullptr)
		{
			color[0] = 0.0f;
			color[1] = 1.0f;
			// Surface must be re-aquired after resize above
			dx11Surface.GetRenderTargetView((void**)&rtv);

			context->ClearRenderTargetView(rtv, color);
			// again get rid of your handles as soon as you are done with them
			rtv->Release();

			swapchain->Present(0, 0);
		}

		testTex->Release();

	}
	// release all main test handles
	context->Release();
	device->Release();
	swapchain->Release();
}

TEST_CASE("DirectX HDR10/MSAA Not Supported UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// Only need to test for these two flags, as the rest are optional.
	constexpr unsigned long long initMask = GW::GRAPHICS::MSAA_4X_SUPPORT | GW::GRAPHICS::COLOR_10_BIT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::FEATURE_UNSUPPORTED);
}

TEST_CASE("DirectX No MSAA UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x2 MSAA Support UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_2X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x4 MSAA Support UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_4X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x8 MSAA Support UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_8X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}

TEST_CASE("DirectX x16 MSAA Support UWP", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_16X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	ID3D11RenderTargetView* view;
	ID3D11DepthStencilView* depth;
	if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
		+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
	{
		context->ClearRenderTargetView(view, clr);
		context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

		// ==================RENDER HERE==================

		// grab the context & render target
		ID3D11DeviceContext* con;
		ID3D11RenderTargetView* targetView;
		ID3D11DepthStencilView* stencilView;
		dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
		dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
		dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

		// setup the pipeline
		ID3D11RenderTargetView *const views[] = { targetView };
		con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
		constexpr UINT strides[] = { sizeof(float) * 2 };
		constexpr UINT offsets[] = { 0 };
		ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
		con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
		con->VSSetShader(vertexShader.Get(), nullptr, 0);
		con->PSSetShader(pixelShader.Get(), nullptr, 0);
		con->IASetInputLayout(vertexFormat.Get());

		// now we can draw
		con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		con->Draw(3, 0);

		// release temp handles
		con->Release();
		view->Release();
		depth->Release();
		targetView->Release();
		stencilView->Release();

		// ===============================================


		swapchain->Present(1, 0);
		// release incremented COM reference counts
		swapchain->Release();
	}
}
#endif

#if !defined(DISABLE_USER_INPUT_TESTS)
TEST_CASE("GDirectX11Surface DEMO Unit Test", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	GW::GRAPHICS::GDirectX11Surface surface;
	surface.Create(window, 0);

	static float t = 0;
	std::cout << "CLOSE THE WINDOW TO STOP THE EPILEPSY AND CONTINUE THE TEST\n";
	while (+window.ProcessWindowEvents())
	{
		ID3D11DeviceContext* context = nullptr;
		ID3D11RenderTargetView* rtv = nullptr;
		IDXGISwapChain* sc = nullptr;
		surface.GetImmediateContext((void**)&context);
		surface.GetRenderTargetView((void**)&rtv);
		surface.GetSwapchain((void**)&sc);

		t += 0.01f;
		FLOAT color[4] = { sinf(t), cosf(t), tanf(t), 1.0f };
		context->ClearRenderTargetView(rtv, color);
		sc->Present(0, 0);

		context->Release();
		rtv->Release();
		sc->Release();
	}
}

TEST_CASE("DirectX No MSAA with Input", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	while (+window.ProcessWindowEvents())
	{
		ID3D11RenderTargetView* view;
		ID3D11DepthStencilView* depth;
		if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
			+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
		{
			context->ClearRenderTargetView(view, clr);
			context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

			// ==================RENDER HERE==================

			// grab the context & render target
			ID3D11DeviceContext* con;
			ID3D11RenderTargetView* targetView;
			ID3D11DepthStencilView* stencilView;
			dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
			dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
			dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

			// setup the pipeline
			ID3D11RenderTargetView *const views[] = { targetView };
			con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
			constexpr UINT strides[] = { sizeof(float) * 2 };
			constexpr UINT offsets[] = { 0 };
			ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
			con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
			con->VSSetShader(vertexShader.Get(), nullptr, 0);
			con->PSSetShader(pixelShader.Get(), nullptr, 0);
			con->IASetInputLayout(vertexFormat.Get());

			// now we can draw
			con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			con->Draw(3, 0);

			// release temp handles
			con->Release();
			view->Release();
			depth->Release();
			targetView->Release();
			stencilView->Release();

			// ===============================================


			swapchain->Present(1, 0);
		}
	}

	// release incremented COM reference counts
	swapchain->Release();
}

TEST_CASE("DirectX x2 MSAA Support with Input", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_2X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	while (+window.ProcessWindowEvents())
	{
		ID3D11RenderTargetView* view;
		ID3D11DepthStencilView* depth;
		if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
			+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
		{
			context->ClearRenderTargetView(view, clr);
			context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

			// ==================RENDER HERE==================

			// grab the context & render target
			ID3D11DeviceContext* con;
			ID3D11RenderTargetView* targetView;
			ID3D11DepthStencilView* stencilView;
			dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
			dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
			dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

			// setup the pipeline
			ID3D11RenderTargetView *const views[] = { targetView };
			con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
			constexpr UINT strides[] = { sizeof(float) * 2 };
			constexpr UINT offsets[] = { 0 };
			ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
			con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
			con->VSSetShader(vertexShader.Get(), nullptr, 0);
			con->PSSetShader(pixelShader.Get(), nullptr, 0);
			con->IASetInputLayout(vertexFormat.Get());

			// now we can draw
			con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			con->Draw(3, 0);

			// release temp handles
			con->Release();
			view->Release();
			depth->Release();
			targetView->Release();
			stencilView->Release();

			// ===============================================


			swapchain->Present(1, 0);
		}
	}

	// release incremented COM reference counts
	swapchain->Release();
}

TEST_CASE("DirectX x4 MSAA Support with Input", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_4X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	while (+window.ProcessWindowEvents())
	{
		ID3D11RenderTargetView* view;
		ID3D11DepthStencilView* depth;
		if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
			+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
		{
			context->ClearRenderTargetView(view, clr);
			context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

			// ==================RENDER HERE==================

			// grab the context & render target
			ID3D11DeviceContext* con;
			ID3D11RenderTargetView* targetView;
			ID3D11DepthStencilView* stencilView;
			dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
			dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
			dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

			// setup the pipeline
			ID3D11RenderTargetView *const views[] = { targetView };
			con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
			constexpr UINT strides[] = { sizeof(float) * 2 };
			constexpr UINT offsets[] = { 0 };
			ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
			con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
			con->VSSetShader(vertexShader.Get(), nullptr, 0);
			con->PSSetShader(pixelShader.Get(), nullptr, 0);
			con->IASetInputLayout(vertexFormat.Get());

			// now we can draw
			con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			con->Draw(3, 0);

			// release temp handles
			con->Release();
			view->Release();
			depth->Release();
			targetView->Release();
			stencilView->Release();

			// ===============================================


			swapchain->Present(1, 0);
		}
	}

	// release incremented COM reference counts
	swapchain->Release();
}

TEST_CASE("DirectX x8 MSAA Support with Input", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_8X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	while (+window.ProcessWindowEvents())
	{
		ID3D11RenderTargetView* view;
		ID3D11DepthStencilView* depth;
		if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
			+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
		{
			context->ClearRenderTargetView(view, clr);
			context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

			// ==================RENDER HERE==================

			// grab the context & render target
			ID3D11DeviceContext* con;
			ID3D11RenderTargetView* targetView;
			ID3D11DepthStencilView* stencilView;
			dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
			dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
			dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

			// setup the pipeline
			ID3D11RenderTargetView *const views[] = { targetView };
			con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
			constexpr UINT strides[] = { sizeof(float) * 2 };
			constexpr UINT offsets[] = { 0 };
			ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
			con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
			con->VSSetShader(vertexShader.Get(), nullptr, 0);
			con->PSSetShader(pixelShader.Get(), nullptr, 0);
			con->IASetInputLayout(vertexFormat.Get());

			// now we can draw
			con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			con->Draw(3, 0);

			// release temp handles
			con->Release();
			view->Release();
			depth->Release();
			targetView->Release();
			stencilView->Release();

			// ===============================================


			swapchain->Present(1, 0);
		}
	}

	// release incremented COM reference counts
	swapchain->Release();
}

TEST_CASE("DirectX x16 MSAA Support with Input", "[Graphics]")
{
	if (EnableD3D11Tests == false)
	{
		std::cout << "Direct3D11/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	initMask |= GW::GRAPHICS::MSAA_16X_SUPPORT;

	// Window and surface setup.
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX11Surface dx11Surface;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	IDXGISwapChain* swapchain;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);

	const GW::GReturn result = dx11Surface.Create(window, initMask);
	if (result == GW::GReturn::HARDWARE_UNAVAILABLE)
	{
		return; // This is just a hardware limitation, not a failure.
	}

	// If this isn't a hardware limitation, we should just ensure that it succeeds.
	REQUIRE(result == GW::GReturn::SUCCESS);

	REQUIRE(dx11Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetImmediateContext((void**)&context) == GW::GReturn::SUCCESS);
	REQUIRE(dx11Surface.GetSwapchain((void**)&swapchain) == GW::GReturn::SUCCESS);

	// what we need at a minimum to draw a triangle
	Microsoft::WRL::ComPtr<ID3D11Buffer>		vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11VertexShader>	vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>	pixelShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>	vertexFormat;

	// Simple Vertex Shader
	const char* vertexShaderSource = R"(
	// an ultra simple hlsl vertex shader
	float4 main(float2 inputVertex : POSITION) : SV_POSITION
	{
		return float4(inputVertex, 0, 1);
	}
	)";

	// Simple Pixel Shader
	const char* pixelShaderSource = R"(
	// an ultra simple hlsl pixel shader
	float4 main() : SV_TARGET
	{
		return float4(0.25f,0.0f,1.0f,0);
	}
	)";

	// Create Vertex Buffer
	float verts[] = {
		0,   0.5f,
	  0.5f, -0.5f,
	 -0.5f, -0.5f
	};

	D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
	CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER);
	device->CreateBuffer(&bDesc, &bData, vertexBuffer.GetAddressOf());

	// Create Vertex Shader
	UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	compilerFlags |= D3DCOMPILE_DEBUG;
#endif

	Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
	if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource), nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0, vsBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Pixel Shader
	Microsoft::WRL::ComPtr<ID3DBlob> psBlob; errors.Reset();
	if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource), nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0, psBlob.GetAddressOf(), errors.GetAddressOf())))
	{
		device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr, pixelShader.GetAddressOf());
	}
	else
	{
		std::cout << static_cast<char*>(errors->GetBufferPointer()) << std::endl;
	}

	// Create Input Layout
	D3D11_INPUT_ELEMENT_DESC format[] = {
		{
			"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
		}
	};
	device->CreateInputLayout(format, ARRAYSIZE(format),
		vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
		vertexFormat.GetAddressOf());

	// free temporary handle
	device->Release();
	context->Release();

	float clr[] = { 57/255.0f, 1.0f, 20/255.0f, 1 }; // start with a neon green

	while (+window.ProcessWindowEvents())
	{
		ID3D11RenderTargetView* view;
		ID3D11DepthStencilView* depth;
		if (+dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&view)) &&
			+dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&depth)))
		{
			context->ClearRenderTargetView(view, clr);
			context->ClearDepthStencilView(depth, D3D11_CLEAR_DEPTH, 1, 0);

			// ==================RENDER HERE==================

			// grab the context & render target
			ID3D11DeviceContext* con;
			ID3D11RenderTargetView* targetView;
			ID3D11DepthStencilView* stencilView;
			dx11Surface.GetImmediateContext(reinterpret_cast<void**>(&con));
			dx11Surface.GetRenderTargetView(reinterpret_cast<void**>(&targetView));
			dx11Surface.GetDepthStencilView(reinterpret_cast<void**>(&stencilView));

			// setup the pipeline
			ID3D11RenderTargetView *const views[] = { targetView };
			con->OMSetRenderTargets(ARRAYSIZE(views), views, stencilView);
			constexpr UINT strides[] = { sizeof(float) * 2 };
			constexpr UINT offsets[] = { 0 };
			ID3D11Buffer* const buffs[] = { vertexBuffer.Get() };
			con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
			con->VSSetShader(vertexShader.Get(), nullptr, 0);
			con->PSSetShader(pixelShader.Get(), nullptr, 0);
			con->IASetInputLayout(vertexFormat.Get());

			// now we can draw
			con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			con->Draw(3, 0);

			// release temp handles
			con->Release();
			view->Release();
			depth->Release();
			targetView->Release();
			stencilView->Release();

			// ===============================================


			swapchain->Present(1, 0);
		}
	}

	// release incremented COM reference counts
	swapchain->Release();
}
#endif _DISABLE_USER_INPUT_TESTS
#endif /* _WIN32 */
#endif /* defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GDIRECTX11SURFACE) */