#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GRASTERSURFACE)

#include "../../Interface/System/GDaemon.h"
#include <bitset>
#include <chrono>


//#define GRASTERSURFACE_TEST_DEBUG_VERBOSE_NEW					// uncomment this line to override the "new" keyword with a macro that stores additional data to track down memory leaks more easily (WIN32 Debug only)
//#define GRASTERSURFACE_TEST_DEBUG_PRINTS						// uncomment this line to enable debug messages to be printed to the console
//#define GRASTERSURFACE_TEST_DEBUG_PRINT_PRIORITY_LEVEL 1		// use this to set the priority level of debug messages to print (lower number = higher priority, any priority levels greater than this will be ignored)

#if defined(_WIN32) && !defined(NDEBUG) && defined(GRASTERSURFACE_TEST_DEBUG_VERBOSE_NEW)
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif


// macro to suppress deprecation warnings for strcpy and sprintf on one line, since strcpy_s and sprintf_s can't be used on mac or linux
/* usage:
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
	line of code involving strcpy and/or sprintf
*/
#if defined(_WIN32)
#define GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING warning(suppress : 4996)
#elif defined(__APPLE__) || defined(__linux__)
#define GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING GCC diagnostic suppress "-Wdeprecated"
#endif


bool enableRasterSurfaceTests = false;
TEST_CASE("Check for OS GUI RasterSurface support", "[Graphics]")
{
	GW::SYSTEM::GWindow window;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		enableRasterSurfaceTests = true;
		std::cout << "OS GUI DETECTED: RUNNING ALL GRASTERSURFACE UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL GRASTERSURFACE UNIT TESTS" << std::endl;
}

bool enableRasterSurfaceVisualTest = true;
TEST_CASE("GRasterSurface - check for GInput enabled", "[Graphics]")
{
#if !defined(GATEWARE_ENABLE_INPUT) || defined(GATEWARE_DISABLE_GINPUT)
	enableRasterSurfaceVisualTest = false;
	std::cout << "WARNING: GINPUT NOT ENABLED; SKIPPING GRASTERSURFACE VISUAL TEST" << std::endl;
	return;
#endif
	std::cout << "GINPUT ENABLED: RUNNING GRASTERSURFACE VISUAL TEST" << std::endl;
}

// use these to choose which tests to run
#define GRASTERSURFACE_TEST_EMPTY_PROXY
#define GRASTERSURFACE_TEST_CREATE
#define GRASTERSURFACE_TEST_RESIZE
#define GRASTERSURFACE_TEST_CLEAR
#define GRASTERSURFACE_TEST_UPDATE
#define GRASTERSURFACE_TEST_UPDATE_SUBSET
#define GRASTERSURFACE_TEST_SMART_UPDATE
#define GRASTERSURFACE_TEST_LOCK_WRITE
#define GRASTERSURFACE_TEST_LOCK_READ
#define GRASTERSURFACE_TEST_UNLOCK_WRITE
#define GRASTERSURFACE_TEST_UNLOCK_READ
#define GRASTERSURFACE_TEST_PRESENT
#define GRASTERSURFACE_TEST_VISUAL
// VISUAL TEST DOCUMENTATION
/*
	This test's purpose is visual confirmation of drawing and other functionality that cannot be verified with a REQUIRE.
	! EPILEPSY WARNING ! This test can cause rapidly flickering lights. Use it at your own risk.


	CONTROLS:

	Close window						: ESC

	--- Surface clearing ---
	Enabled								: Z
	Disabled							: X

	--- Surface presenting ---
	Enabled								: C
	Disabled							: V

	--- Surface update modes ---
	UpdateSurface						: Q
	UpdateSurfaceSubset					: W
	SmartUpdateSurface					: E
	LockUpdateBufferWrite				: R

	--- Input buffer drawing modes ---
	None								: A
	Solid color							: S
	Fixed pattern						: D
	Randomized							: F
	Image								: G

	-- Smart update input data size --
	Relative (proportional to window)	: ;
	Fixed								: '

	-- Smart update relative-size input data scaling --
	1/20 of client area width			: M
	1/10 of client area width			: ,
	1/20 of client area height			: .
	1/10 of client area height			: /

	-- X alignment --
	Left								: I
	Center								: O
	Right								: P

	-- Y alignment --
	Top									: [
	Center								: ]
	Bottom								: \

	-- Upscaling --
	1x									: 1
	2x									: 2
	3x									: 3
	4x									: 4
	8x									: 5
	16x									: 6
	Stretch-to-fit						: 7

	-- Interpolation type --
	Nearest								: 9
	Bilinear							: 0

	--- Framerate ---
	Capped								: -
	Unlimited							: =
*/


// Prints a debug message to the console if GRASTERSURFACE_TEST_DEBUG_PRINTS is defined.
/*
*	Used to assist with debugging, since multi-threading makes debugging tools harder to use.
*	If int arguments are passed, they will be inserted into the output if _msg is a format string.
*/
static inline void RasterSurfaceTest_DebugPrint(int _priorityLevel, const char* _msg,
	long long _intArg0 = 0, long long _intArg1 = 0, long long _intArg2 = 0, long long _intArg3 = 0,
	long long _intArg4 = 0, long long _intArg5 = 0, long long _intArg6 = 0, long long _intArg7 = 0)
{
#if defined(GRASTERSURFACE_TEST_DEBUG_PRINTS) && defined(GRASTERSURFACE_TEST_DEBUG_PRINT_PRIORITY_LEVEL)
	if (_priorityLevel <= GRASTERSURFACE_TEST_DEBUG_PRINT_PRIORITY_LEVEL)
		std::printf(_msg, _intArg0, _intArg1, _intArg2, _intArg3, _intArg4, _intArg5, _intArg6, _intArg7);
#endif
}

// returns tick count since program startup in milliseconds
static inline unsigned long long RasterSurfaceTest_GetTickCountMs()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}


#if defined(GRASTERSURFACE_TEST_EMPTY_PROXY)
TEST_CASE("GRasterSurface core method test battery", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface core method test battery ===\n");

	GW::GRAPHICS::GRasterSurface		surface;
	unsigned int*						pixelBuffer = nullptr;
	unsigned int						numPixels = 0;
	unsigned short						numRows = 0;
	unsigned short						rowWidth = 0;
	unsigned short						rowStride = 0;
	int									destX = 0;
	int									destY = 0;
	unsigned int						clearColor = 0x00000000;
	unsigned int						updateFlags = 0;
	unsigned int*						memoryBuffer = nullptr;
	unsigned int**						memoryBufferWrite = &memoryBuffer;
	const unsigned int**				memoryBufferRead = const_cast<const unsigned int**>(&memoryBuffer);
	unsigned short						outWidth = 0;
	unsigned short						outHeight = 0;

	WHEN("Surface has not been created")
	{
		RasterSurfaceTest_DebugPrint(1, "WHEN: Surface has not been created");

		THEN("All functions should return EMPTY_PROXY")
		{
			REQUIRE(surface.Clear(clearColor) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.UpdateSurface(pixelBuffer, numPixels) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, rowWidth, updateFlags) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.UnlockUpdateBufferWrite() == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.UnlockUpdateBufferRead() == GW::GReturn::EMPTY_PROXY);
			REQUIRE(surface.Present() == GW::GReturn::EMPTY_PROXY);
		}
	}
}
#endif // GRASTERSURFACE_TEST_EMPTY_PROXY

#if defined(GRASTERSURFACE_TEST_CREATE)
TEST_CASE("GRasterSurface Create checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface Create checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;

	GW::GRAPHICS::GDirectX11Surface testSurface;
	testSurface.Create(window, 0);
	testSurface = nullptr;

	SECTION("Create positive checks")
	{
		WHEN("All arguments are valid")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: All arguments are valid\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

			THEN("Surface should be created successfully")
			{
				REQUIRE(surface.Create(window) == GW::GReturn::SUCCESS);
			}
		}
	}

	SECTION("Create negative checks")
	{
		WHEN("Window is deallocated before Create is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before Create is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should reject argument")
			{
				REQUIRE(surface.Create(window) == GW::GReturn::INVALID_ARGUMENT);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_CREATE

#if defined(GRASTERSURFACE_TEST_RESIZE)
TEST_CASE("GRasterSurface Resizing checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface Resizing checks ===\n");

	GW::SYSTEM::GWindow				window;
	GW::GRAPHICS::GRasterSurface	surface;
	const unsigned int				initialClientWidth = 800;
	const unsigned int				initialClientHeight = 600;
	unsigned int					downsizeAmount = 100;
	unsigned int					clientWidth = 0;
	unsigned int					clientHeight = 0;
	unsigned int*					memoryBuffer = nullptr;
	const unsigned int**			memoryBufferRead = const_cast<const unsigned int**>(&memoryBuffer);
	unsigned short					outWidth = 0;
	unsigned short					outHeight = 0;

	auto GiveWindowTimeToTransition = [&]()
	{
#if defined(__APPLE__)
		constexpr int delay = 2500; // windows with a resizing animation need more time to finish
#else
		constexpr int delay = 250;
#endif
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();

		do
		{
			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		} while (+window.ProcessWindowEvents() && lapsedTime < delay);
	};

	WHEN("Window is resized")
	{
		window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
		surface.Create(window);
		window.GetClientWidth(clientWidth);
		window.GetClientHeight(clientHeight);

		THEN("Surface should resize to match")
		{
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);
			surface.UnlockUpdateBufferRead();
			REQUIRE(outWidth == clientWidth);
			REQUIRE(outHeight == clientHeight);

			window.ResizeWindow(initialClientWidth - downsizeAmount, initialClientHeight - downsizeAmount);
			GiveWindowTimeToTransition();
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);
			surface.UnlockUpdateBufferRead();
			REQUIRE(outWidth == clientWidth);
			REQUIRE(outHeight == clientHeight);
		}
	}
}
#endif // GRASTERSURFACE_TEST_RESIZE

#if defined(GRASTERSURFACE_TEST_CLEAR)
TEST_CASE("GRasterSurface Clear checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface Clear checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clearColor = 0x12345678;

	SECTION("Clear positive checks")
	{
		WHEN("Clear is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Clear is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);

			THEN("Surface should clear its back buffer successfully")
			{
				REQUIRE(surface.Clear(clearColor) == GW::GReturn::SUCCESS);
			}
		}
	}

	SECTION("Clear negative checks")
	{
		WHEN("Window is deallocated before Clear is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before Clear is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.Clear(clearColor) == GW::GReturn::PREMATURE_DEALLOCATION);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_CLEAR

#if defined(GRASTERSURFACE_TEST_UPDATE)
TEST_CASE("GRasterSurface UpdateSurface checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface UpdateSurface checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clientWidth = 0;
	unsigned int						clientHeight = 0;
	unsigned int*						pixelBuffer = nullptr;
	unsigned int						numPixels = 0;

	SECTION("UpdateSurface positive checks")
	{
		WHEN("All arguments are valid")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: All arguments are valid\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Surface should update successfully")
			{
				REQUIRE(surface.UpdateSurface(pixelBuffer, numPixels) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}
	}

	SECTION("UpdateSurface negative checks")
	{
		WHEN("Window is deallocated before UpdateSurface is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before UpdateSurface is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.UpdateSurface(pixelBuffer, numPixels) == GW::GReturn::PREMATURE_DEALLOCATION);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel data is nullptr")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel data is nullptr\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;

			pixelBuffer = nullptr;

			THEN("Surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurface(nullptr, numPixels) == GW::GReturn::INVALID_ARGUMENT);
			}
		}

		WHEN("Pixel count is zero")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel count is zero\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurface(pixelBuffer, 0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel count exceeds surface bounds")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel count exceeds surface bounds\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurface(pixelBuffer, numPixels + 1) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}
	}
}
#endif // GRASTERSURFACE_TEST_UPDATE

#if defined(GRASTERSURFACE_TEST_UPDATE_SUBSET)
TEST_CASE("GRasterSurface UpdateSurfaceSubset checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface UpdateSurfaceSubset checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clientWidth = 0;
	unsigned int						clientHeight = 0;
	unsigned int						numPixels = 0;
	unsigned int*						pixelBuffer = nullptr;
	unsigned short						numRows = 0;
	unsigned short						rowWidth = 0;
	unsigned short						rowStride = 0;
	int									destX = 0;
	int									destY = 0;

	SECTION("UpdateSurfaceSubset positive checks")
	{
		WHEN("All arguments are valid")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: All arguments are valid\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should update successfully")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel data with a row stride is passed")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel data with a row stride is passed\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = clientWidth;
			destX = 1;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should update successfully")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset overflows right edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset overflows right edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = clientWidth / 2;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Subset should be clipped to surface's edge")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset overflows left edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset overflows left edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = -static_cast<int>(clientWidth / 2);
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Subset should be clipped to surface's edge")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset overflows bottom edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset overflows bottom edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = clientHeight / 2;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Subset should be clipped to surface's edge")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset overflows top edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset overflows top edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = -static_cast<int>(clientHeight / 2);

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Subset should be clipped to surface's edge")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset is beyond right edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset is beyond right edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = clientWidth;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Function should succeed but indicate that nothing was drawn")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::REDUNDANT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset is beyond left edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset is beyond left edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = -static_cast<int>(clientWidth);
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Function should succeed but indicate that nothing was drawn")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::REDUNDANT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset is beyond bottom edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset is beyond bottom edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = clientHeight;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Function should succeed but indicate that nothing was drawn")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::REDUNDANT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Subset is beyond top edge of surface")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Subset is beyond top edge of surface\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = -static_cast<int>(clientHeight);

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("Function should succeed but indicate that nothing was drawn")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::REDUNDANT);
			}

			delete[] pixelBuffer;
		}
	}

	SECTION("UpdateSurfaceSubset negative checks")
	{
		WHEN("Window is deallocated before UpdateSurfaceSubset is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before UpdateSurfaceSubset is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::PREMATURE_DEALLOCATION);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel data is nullptr")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel data is nullptr\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = 1;

			pixelBuffer = nullptr;

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurfaceSubset(nullptr, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::INVALID_ARGUMENT);
			}
		}

		WHEN("Row count is zero")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Row count is zero\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = 0;
			rowWidth = clientWidth - 2;
			rowStride = 0;
			destX = 1;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Row width is zero")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Row width is zero\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			numPixels = clientWidth * clientHeight;
			numRows = clientHeight - 2;
			rowWidth = 0;
			rowStride = 0;
			destX = 1;
			destY = 1;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.UpdateSurfaceSubset(pixelBuffer, numRows, rowWidth, rowStride, destX, destY) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

	}
}
#endif // GRASTERSURFACE_TEST_UPDATE_SUBSET

#if defined(GRASTERSURFACE_TEST_SMART_UPDATE)
TEST_CASE("GRasterSurface SmartUpdateSurface checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface SmartUpdateSurface checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clientWidth = 0;
	unsigned int						clientHeight = 0;
	unsigned int*						pixelBuffer = nullptr;
	unsigned short						dataWidth = 0;
	unsigned short						dataHeight = 0;
	unsigned int						numPixels = 0;
	unsigned int						presentOptions;

	SECTION("SmartUpdateSurface positive checks")
	{
		WHEN("All arguments are valid")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: All arguments are valid\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should update successfully")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel count passed to smart update exceeds surface bounds")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel count passed to smart update exceeds surface bounds\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth )+ 1;
			dataHeight = static_cast<unsigned short>(clientHeight )+ 1;
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels];
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should update successfully")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::SUCCESS);
			}

			delete[] pixelBuffer;
		}
	}

	SECTION("SmartUpdateSurface negative checks")
	{
		WHEN("Window is deallocated before SmartUpdateSurface is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before SmartUpdateSurface is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::PREMATURE_DEALLOCATION);
			}

			delete[] pixelBuffer;
		}

		WHEN("Pixel data is nullptr")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel data is nullptr\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = nullptr;

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(nullptr, numPixels, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}
		}

		WHEN("Pixel count is zero")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Pixel count is zero\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, 0, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Row width is zero")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Row width is zero\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, 0, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Conflicting X alignment flags are set")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Conflicting X alignment flags are set\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_LEFT
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_RIGHT
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Conflicting Y alignment flags are set")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Conflicting Y alignment flags are set\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_TOP
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_BOTTOM
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Conflicting upscaling flags are set")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Conflicting upscaling flags are set\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_2X
				| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_3X
				| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_4X
				| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_8X
				| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_16X
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}

		WHEN("Conflicting interpolation flags are set")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Conflicting interpolation flags are set\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			dataWidth = static_cast<unsigned short>(clientWidth);
			dataHeight = static_cast<unsigned short>(clientHeight);
			numPixels = static_cast<unsigned int>(dataWidth * dataHeight);
			presentOptions =
				  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
				| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST
				| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_BILINEAR;

			pixelBuffer = new unsigned int[numPixels]; // this will leak if test fails
			memset(pixelBuffer, 0xff, numPixels * sizeof(unsigned int));

			THEN("The surface should indicate that an argument is invalid")
			{
				REQUIRE(surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, presentOptions) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] pixelBuffer;
		}
	}
}
#endif // GRASTERSURFACE_TEST_SMART_UPDATE

#if defined(GRASTERSURFACE_TEST_LOCK_WRITE)
TEST_CASE("GRasterSurface LockUpdateBufferWrite checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface LockUpdateBufferWrite checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clientWidth = 0;
	unsigned int						clientHeight = 0;
	unsigned int*						memoryBuffer = nullptr;
	unsigned int**						memoryBufferWrite = &memoryBuffer;
	unsigned short						outWidth = 0;
	unsigned short						outHeight = 0;

	SECTION("LockUpdateBufferWrite positive checks")
	{
		WHEN("Surface is not locked")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is not locked\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);

			THEN("Surface should lock for writing and valid data should be returned")
			{
				REQUIRE(surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight) == GW::GReturn::SUCCESS);
				REQUIRE(memoryBuffer != nullptr);
				REQUIRE(outWidth == clientWidth);
				REQUIRE(outHeight == clientHeight);
			}
		}

		WHEN("Surface is inactive")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is inactive\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.Minimize();

			THEN("Surface should lock for writing")
			{
				REQUIRE(surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight) == GW::GReturn::SUCCESS);
				REQUIRE(memoryBuffer != nullptr);
			}
		}
	}

	SECTION("LockUpdateBufferWrite negative checks")
	{
		WHEN("Window is deallocated before LockUpdateBufferWrite is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before LockUpdateBufferWrite is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight) == GW::GReturn::PREMATURE_DEALLOCATION);
				REQUIRE(memoryBuffer == nullptr);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}

		WHEN("Invalid memory buffer is passed")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Invalid memory buffer is passed\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);

			THEN("Surface should indicate that an invalid argument was passed")
			{
				REQUIRE(surface.LockUpdateBufferWrite(nullptr, outWidth, outHeight) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}

		WHEN("Surface is already locked for writing")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is already locked for writing\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight);
			memoryBuffer = nullptr;
			outWidth = 0;
			outHeight = 0;

			THEN("Lock should be rejected")
			{
				REQUIRE(surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight) == GW::GReturn::FAILURE);
				REQUIRE(memoryBuffer == nullptr);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_LOCK_WRITE

#if defined(GRASTERSURFACE_TEST_LOCK_READ)
TEST_CASE("GRasterSurface LockUpdateBufferRead checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface LockUpdateBufferRead checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int						clientWidth = 0;
	unsigned int						clientHeight = 0;
	unsigned int*						memoryBuffer = nullptr;
	const unsigned int**				memoryBufferRead = const_cast<const unsigned int**>(&memoryBuffer);
	unsigned short						outWidth = 0;
	unsigned short						outHeight = 0;

	SECTION("LockUpdateBufferRead positive checks")
	{
		WHEN("Surface is not locked")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is not locked\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);

			THEN("Surface should lock for reading and valid data should be returned")
			{
				REQUIRE(surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight) == GW::GReturn::SUCCESS);
				REQUIRE(memoryBuffer != nullptr);
				REQUIRE(outWidth == clientWidth);
				REQUIRE(outHeight == clientHeight);
			}
		}

		WHEN("Surface is inactive")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is inactive\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.Minimize();

			THEN("Surface should lock for reading")
			{
				REQUIRE(surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight) == GW::GReturn::SUCCESS);
				REQUIRE(memoryBuffer != nullptr);
			}
		}
	}

	SECTION("LockUpdateBufferRead negative checks")
	{
		WHEN("Window is deallocated before LockUpdateBufferRead is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before LockUpdateBufferRead is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight) == GW::GReturn::PREMATURE_DEALLOCATION);
				REQUIRE(memoryBuffer == nullptr);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}

		WHEN("Invalid memory buffer is passed")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Invalid memory buffer is passed\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);

			THEN("Surface should indicate that an invalid argument was passed")
			{
				REQUIRE(surface.LockUpdateBufferRead(nullptr, outWidth, outHeight) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}

		WHEN("Surface is already locked for reading")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is already locked for reading\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);
			memoryBuffer = nullptr;
			outWidth = 0;
			outHeight = 0;

			THEN("Lock should be rejected")
			{
				REQUIRE(surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight) == GW::GReturn::FAILURE);
				REQUIRE(memoryBuffer == nullptr);
				REQUIRE(outWidth == 0);
				REQUIRE(outHeight == 0);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_LOCK_READ

#if defined(GRASTERSURFACE_TEST_UNLOCK_WRITE)
TEST_CASE("GRasterSurface UnlockUpdateBufferWrite checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface UnlockUpdateBufferWrite checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int*						memoryBuffer = nullptr;
	unsigned int**						memoryBufferWrite = &memoryBuffer;
	unsigned short						outWidth = 0;
	unsigned short						outHeight = 0;

	SECTION("UnlockUpdateBufferWrite positive checks")
	{
		WHEN("Surface is locked for writing")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is locked for writing\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight);

			THEN("Buffer should unlock")
			{
				REQUIRE(surface.UnlockUpdateBufferWrite() == GW::GReturn::SUCCESS);
			}
		}

		WHEN("Surface is locked for writing and is inactive")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is locked for writing and is inactive\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight);
			window.Minimize();

			THEN("Surface should unlock")
			{
				REQUIRE(surface.UnlockUpdateBufferWrite() == GW::GReturn::SUCCESS);
			}
		}

		WHEN("Surface is not locked")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is not locked\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);

			THEN("Surface should indicate that unlock is redundant")
			{
				REQUIRE(surface.UnlockUpdateBufferWrite() == GW::GReturn::REDUNDANT);
			}
		}
	}

	SECTION("UnlockUpdateBufferWrite negative checks")
	{
		WHEN("Window is deallocated before UnlockUpdateBufferWrite is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before UnlockUpdateBufferWrite is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferWrite(memoryBufferWrite, outWidth, outHeight);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.UnlockUpdateBufferWrite() == GW::GReturn::PREMATURE_DEALLOCATION);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_UNLOCK_WRITE

#if defined(GRASTERSURFACE_TEST_UNLOCK_READ)
TEST_CASE("GRasterSurface UnlockUpdateBufferRead checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface UnlockUpdateBufferRead checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;
	unsigned int*						memoryBuffer = nullptr;
	const unsigned int**				memoryBufferRead = const_cast<const unsigned int**>(&memoryBuffer);
	unsigned short						outWidth = 0;
	unsigned short						outHeight = 0;

	SECTION("UnlockUpdateBufferRead positive checks")
	{
		WHEN("Surface is locked for reading")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is locked for reading\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);

			THEN("Surface should unlock")
			{
				REQUIRE(surface.UnlockUpdateBufferRead() == GW::GReturn::SUCCESS);
			}
		}

		WHEN("Surface is locked for reading and surface is inactive")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is locked for reading and surface is inactive\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);
			window.Minimize();

			THEN("Surface should unlock")
			{
				REQUIRE(surface.UnlockUpdateBufferRead() == GW::GReturn::SUCCESS);
			}
		}

		WHEN("Surface is not locked")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Surface is not locked\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);

			THEN("Surface should indicate that unlock is redundant")
			{
				REQUIRE(surface.UnlockUpdateBufferRead() == GW::GReturn::REDUNDANT);
			}
		}
	}

	SECTION("UnlockUpdateBufferRead negative checks")
	{
		WHEN("Window is deallocated before UnlockUpdateBufferRead is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before UnlockUpdateBufferRead is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			surface.LockUpdateBufferRead(memoryBufferRead, outWidth, outHeight);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.UnlockUpdateBufferRead() == GW::GReturn::PREMATURE_DEALLOCATION);
			}
		}
	}
}
#endif // GRASTERSURFACE_TEST_UNLOCK_READ

#if defined(GRASTERSURFACE_TEST_PRESENT)
TEST_CASE("GRasterSurface Present checks", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface Present checks ===\n");

	GW::SYSTEM::GWindow					window;
	GW::GRAPHICS::GRasterSurface		surface;
	const unsigned int					initialClientWidth = 800;
	const unsigned int					initialClientHeight = 600;

	SECTION("Present positive checks")
	{
		WHEN("Present is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Present is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);

			THEN("Surface should present successfully")
			{
				REQUIRE(surface.Present() == GW::GReturn::SUCCESS);
			}
		}
	}

	SECTION("Present negative checks")
	{
		WHEN("Window is deallocated before Present is called")
		{
			RasterSurfaceTest_DebugPrint(1, "WHEN: Window is deallocated before Present is called\n");
			window.Create(0, 0, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			//Sleep(1);
			window = nullptr; // everything above this line should be identical to the basic success test

			THEN("Surface should indicate that it is no longer valid")
			{
				REQUIRE(surface.Present() == GW::GReturn::PREMATURE_DEALLOCATION);
			}
		}

		// FAILURE cannot be reliably tested, since it is only returned if an internal GConcurrent Converge() call fails
	}
}
#endif // GRASTERSURFACE_TEST_PRESENT

#if defined(GRASTERSURFACE_TEST_VISUAL) && !defined(DISABLE_USER_INPUT_TESTS)

#include "../Resources/GRasterSurface_TestFolder/rastersurface_image.h"

struct RasterData
{
	unsigned int				dataWidth = 0;
	const unsigned int*			imageBuffer = nullptr;
};

static inline void DrawPixelRowSolid(const void* _unused, unsigned int* _output, unsigned int _y, const void* _data)
{
	const RasterData data = *(reinterpret_cast<const RasterData*>(_data));
	for (unsigned int x = 0; x < data.dataWidth; ++x)
		_output[x] = 0xffff0000;
}
static inline void DrawPixelRowPattern(const void* _unused, unsigned int* _output, unsigned int _y, const void* _data)
{
	const RasterData data = *(reinterpret_cast<const RasterData*>(_data));
	for (unsigned int x = 0; x < data.dataWidth; ++x)
		switch (((x + _y) / 2) % 4)
		{
		case 0:
			_output[x] = 0xffffffff;
			break;
		case 1:
			_output[x] = 0xffff0000;
			break;
		case 2:
			_output[x] = 0xff00ff00;
			break;
		case 3:
			_output[x] = 0xff0000ff;
			break;
		}
}
static inline void DrawPixelRowRandom(const void* _unused, unsigned int* _output, unsigned int _y, const void* _data)
{
	const RasterData data = *(reinterpret_cast<const RasterData*>(_data));
	for (unsigned int x = 0; x < data.dataWidth; ++x)
	{
		int value = std::rand();
		_output[x] = ((value & 0x0f00) << 12) | ((value & 0x00f0) << 8) | ((value & 0x000f) << 4);
		//                     xx00 0000 0000 0000
		//                       7  f    f    f    (range of std::rand)
		//                     xxxx rrrr gggg bbbb (treated as color channels)
		// xxxx 0000 rrrr 0000 gggg 0000 bbbb 0000 (where color channels need to end up at)
		// 0000 0000 <<12 0000 << 8 0000 << 4 0000 (amount of bitshift needed)
	}
}
static inline void DrawPixelRowImage(const void* _unused, unsigned int* _output, unsigned int _y, const void* _data)
{
	const RasterData data = *(reinterpret_cast<const RasterData*>(_data));
	// clear to black
	memset(_output, 0x00, data.dataWidth * sizeof(unsigned int));
	// get width to fill
	unsigned int width = G_SMALLER(rastersurface_image_width, data.dataWidth);
	// copy a row from the image if this row is within the image's height
	if (_y < rastersurface_image_height)
		memcpy(_output, &data.imageBuffer[_y * rastersurface_image_width], width * sizeof(unsigned int));
}

TEST_CASE("GRasterSurface visual drawing test", "[Graphics]")
{
	if (enableRasterSurfaceTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	if (enableRasterSurfaceVisualTest == false)
	{
		std::cout << "GInput not enabled, skipping unit test." << std::endl;
		return; // ignore this test
	}
	RasterSurfaceTest_DebugPrint(0, "\n\n=== GRasterSurface visual drawing test\n");

	// input names/indices
	enum GRASTERSURFACE_INPUTS
	{
		EXIT = 0,
		// toggle clearing
		CLEAR_ENABLE,
		CLEAR_DISABLE,
		// toggle presenting
		PRESENT_ENABLE,
		PRESENT_DISABLE,
		// update mode
		UPDATE_MODE_BASIC,
		UPDATE_MODE_SUBSET,
		UPDATE_MODE_SMART,
		UPDATE_MODE_DIRECT,
		// draw mode
		DRAW_MODE_NONE,
		DRAW_MODE_SOLID,
		DRAW_MODE_PATTERN,
		DRAW_MODE_RANDOM,
		DRAW_MODE_IMAGE,
		// smart update data size
		SMART_DATA_SIZE_RELATIVE,
		SMART_DATA_SIZE_FIXED,
		// smart update data scale
		SMART_DATA_SCALE_SMALL_X,
		SMART_DATA_SCALE_LARGE_X,
		SMART_DATA_SCALE_SMALL_Y,
		SMART_DATA_SCALE_LARGE_Y,
		// x align
		ALIGN_X_LEFT,
		ALIGN_X_CENTER,
		ALIGN_X_RIGHT,
		// y align
		ALIGN_Y_TOP,
		ALIGN_Y_CENTER,
		ALIGN_Y_BOTTOM,
		// smart update upscale
		UPSCALE_1X,
		UPSCALE_2X,
		UPSCALE_3X,
		UPSCALE_4X,
		UPSCALE_8X,
		UPSCALE_16X,
		UPSCALE_STRETCH,
		// smart update interpolation
		INTERP_NEAREST,
		INTERP_BILINEAR,
		// framerate
		FRAMERATE_CAPPED,
		FRAMERATE_UNLIMITED,

		COUNT
	};
	// input G_KEY codes
	static unsigned int GRASTERSURFACE_INPUT_KEYS[GRASTERSURFACE_INPUTS::COUNT] =
	{
		G_KEY_ESCAPE,
		// toggle clearing
		G_KEY_Z,
		G_KEY_X,
		// toggle presenting
		G_KEY_C,
		G_KEY_V,
		// update mode
		G_KEY_Q,
		G_KEY_W,
		G_KEY_E,
		G_KEY_R,
		// draw mode
		G_KEY_A,
		G_KEY_S,
		G_KEY_D,
		G_KEY_F,
		G_KEY_G,
		// smart update data size
		G_KEY_SEMICOLON,
		G_KEY_QUOTE,
		// smart update data scale
		G_KEY_M,
		G_KEY_COMMA,
		G_KEY_PERIOD,
		G_KEY_FORWARDSLASH,
		// x align
		G_KEY_I,
		G_KEY_O,
		G_KEY_P,
		// y align
		G_KEY_BRACKET_OPEN,
		G_KEY_BRACKET_CLOSE,
		G_KEY_BACKSLASH,
		// smart update upscale
		G_KEY_1,
		G_KEY_2,
		G_KEY_3,
		G_KEY_4,
		G_KEY_5,
		G_KEY_6,
		G_KEY_7,
		// smart update interpolation
		G_KEY_9,
		G_KEY_0,
		// framerate
		G_KEY_MINUS,
		G_KEY_EQUALS,
	};

	// bitmasks to use for SmartUpdate option flag changes
	static constexpr unsigned int bitmaskAlignX =
		  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_LEFT
		| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
		| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_RIGHT;
	static constexpr unsigned int bitmaskAlignY =
		  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_TOP
		| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
		| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_BOTTOM;
	static constexpr unsigned int bitmaskUpscale =
		  GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_2X
		| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_3X
		| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_4X
		| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_8X
		| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_16X
		| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT;
	static constexpr unsigned int bitmaskInterpolate =
		  GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST
		| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_BILINEAR;

	static constexpr int FRAMERATE_CAP = 10;
	static constexpr int FRAMERATE_MS = 1000 / FRAMERATE_CAP;
	static constexpr float SMART_DATA_SCALE_SMALL = 0.05f;
	static constexpr float SMART_DATA_SCALE_LARGE = 0.1f;

	class Test
	{
	public:
		GW::SYSTEM::GWindow					window;
		GW::INPUT::GInput					input;
		GW::GRAPHICS::GRasterSurface		surface;
		GW::SYSTEM::GConcurrent				gClearThread;
		std::bitset<GRASTERSURFACE_INPUTS::COUNT>	inputStates;

		unsigned int						initialClientWidth = 800;
		unsigned int						initialClientHeight = 600;
		unsigned int						clientWidth = 0;
		unsigned int						clientHeight = 0;
		unsigned int						fixedDataWidth = 500;
		unsigned int						fixedDataHeight = 500;
		unsigned short						dataWidth = 0;
		unsigned short						dataHeight = 0;
		int									destX = 0;
		int									destY = 0;

		unsigned int*						pixelBuffer = nullptr;
		const unsigned int					bufferWidth = 4100;
		const unsigned int					bufferHeight = 2200;
		const unsigned int					bufferSize = bufferWidth * bufferHeight; // large enough for just over 4k resolution
		unsigned int						numPixels = 0;

		const unsigned int*					imageBuffer = nullptr;

		unsigned int						clearColor = 0xff3f3f3f;
		int									updateMode = GRASTERSURFACE_INPUTS::UPDATE_MODE_BASIC;
		int									drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_SOLID;

		float								smartUpdateDataScaleX = SMART_DATA_SCALE_SMALL;
		float								smartUpdateDataScaleY = SMART_DATA_SCALE_SMALL;
		unsigned int						updateFlags = 0;

		bool								clearSurface = true;
		bool								presentSurface = true;
		bool								useFixedSize = false;
		bool								capFps = false;
		bool								isRunning = true;

		unsigned long long					frameCount = 0;
		unsigned long long					frameCountPrev = 0;
		unsigned long long					tickCountPrev = 0;
		int									framerate = 0;

		~Test()
		{
		}
		Test()
		{
			window.Create(100, 100, initialClientWidth, initialClientHeight, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
			surface.Create(window);
			gClearThread.Create(false);
			input.Create(window);
			pixelBuffer = new unsigned int[bufferSize];

			// convert image data so it can be copied using memcpy during update
			//   alpha channel is stored last instead of first in files converted by TGA_Pow2_32bit_To_TextureArray.exe, so the channels need to be rearranged to display colors correctly
			imageBuffer = new unsigned int[rastersurface_image_numpixels];
			for (unsigned int i = 0; i < rastersurface_image_numpixels; ++i)
				const_cast<unsigned int*>(imageBuffer)[i] = ((rastersurface_image_pixels[i] & 0xffffff00) >> 8) | ((rastersurface_image_pixels[i] & 0x000000ff) << 24);
		}

		GW::GReturn Update()
		{
			RasterSurfaceTest_DebugPrint(0, "\nvisual test - frame %llu started\n\n", frameCount);

			// read/respond to inputs
			static bool windowIsFocused = false;
			window.IsFocus(windowIsFocused);
			if (windowIsFocused)
			{
				// read inputs
				for (int i = 0; i < GRASTERSURFACE_INPUTS::COUNT; ++i)
				{
					float keyState = 0.0f;
					input.GetState(GRASTERSURFACE_INPUT_KEYS[i], keyState);
					inputStates.set(i, static_cast<bool>(keyState));
				}

				// respond to inputs
				// exit
				if		(inputStates.test(GRASTERSURFACE_INPUTS::EXIT))						{ isRunning = false; return GW::GReturn::FAILURE; }
				// toggle clearing
				if		(inputStates.test(GRASTERSURFACE_INPUTS::CLEAR_ENABLE))				clearSurface = true;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::CLEAR_DISABLE))			clearSurface = false;
				// toggle presenting
				if		(inputStates.test(GRASTERSURFACE_INPUTS::PRESENT_ENABLE))			presentSurface = true;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::PRESENT_DISABLE))			presentSurface = false;
				// update mode
				if		(inputStates.test(GRASTERSURFACE_INPUTS::UPDATE_MODE_BASIC))		updateMode = GRASTERSURFACE_INPUTS::UPDATE_MODE_BASIC;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPDATE_MODE_SUBSET))		updateMode = GRASTERSURFACE_INPUTS::UPDATE_MODE_SUBSET;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPDATE_MODE_SMART))		updateMode = GRASTERSURFACE_INPUTS::UPDATE_MODE_SMART;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPDATE_MODE_DIRECT))		updateMode = GRASTERSURFACE_INPUTS::UPDATE_MODE_DIRECT;
				// draw mode
				if		(inputStates.test(GRASTERSURFACE_INPUTS::DRAW_MODE_NONE))			drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_NONE;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::DRAW_MODE_SOLID))			drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_SOLID;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::DRAW_MODE_PATTERN))		drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_PATTERN;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::DRAW_MODE_RANDOM))			drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_RANDOM;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::DRAW_MODE_IMAGE))			drawMode = GRASTERSURFACE_INPUTS::DRAW_MODE_IMAGE;
				// smart update data size
				if		(inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SIZE_FIXED))	useFixedSize = true;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SIZE_RELATIVE))	useFixedSize = false;
				// smart update data scale
				if		(inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SCALE_SMALL_X))	smartUpdateDataScaleX = SMART_DATA_SCALE_SMALL;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SCALE_LARGE_X))	smartUpdateDataScaleX = SMART_DATA_SCALE_LARGE;
				if		(inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SCALE_SMALL_Y))	smartUpdateDataScaleY = SMART_DATA_SCALE_SMALL;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::SMART_DATA_SCALE_LARGE_Y))	smartUpdateDataScaleY = SMART_DATA_SCALE_LARGE;
				// x align
				if		(inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_X_LEFT))				updateFlags = (updateFlags & ~bitmaskAlignX) | GW::GRAPHICS::ALIGN_X_LEFT;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_X_CENTER))			updateFlags = (updateFlags & ~bitmaskAlignX) | GW::GRAPHICS::ALIGN_X_CENTER;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_X_RIGHT))			updateFlags = (updateFlags & ~bitmaskAlignX) | GW::GRAPHICS::ALIGN_X_RIGHT;
				// y align
				if		(inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_Y_TOP))				updateFlags = (updateFlags & ~bitmaskAlignY) | GW::GRAPHICS::ALIGN_Y_TOP;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_Y_CENTER))			updateFlags = (updateFlags & ~bitmaskAlignY) | GW::GRAPHICS::ALIGN_Y_CENTER;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::ALIGN_Y_BOTTOM))			updateFlags = (updateFlags & ~bitmaskAlignY) | GW::GRAPHICS::ALIGN_Y_BOTTOM;
				// smart update upscale
				if		(inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_1X))				updateFlags = (updateFlags & ~bitmaskUpscale);
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_2X))				updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::UPSCALE_2X;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_3X))				updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::UPSCALE_3X;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_4X))				updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::UPSCALE_4X;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_8X))				updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::UPSCALE_8X;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_16X))				updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::UPSCALE_16X;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::UPSCALE_STRETCH))			updateFlags = (updateFlags & ~bitmaskUpscale) | GW::GRAPHICS::STRETCH_TO_FIT;
				// smart update interpolation
				if		(inputStates.test(GRASTERSURFACE_INPUTS::INTERP_NEAREST))			updateFlags = (updateFlags & ~bitmaskInterpolate) | GW::GRAPHICS::INTERPOLATE_NEAREST;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::INTERP_BILINEAR))			updateFlags = (updateFlags & ~bitmaskInterpolate) | GW::GRAPHICS::INTERPOLATE_BILINEAR;
				// framerate
				if		(inputStates.test(GRASTERSURFACE_INPUTS::FRAMERATE_CAPPED))			capFps = true;
				else if (inputStates.test(GRASTERSURFACE_INPUTS::FRAMERATE_UNLIMITED))		capFps = false;
			} // end read/respond to inputs

			// get surface dimensions
			window.GetClientWidth(clientWidth);
			window.GetClientHeight(clientHeight);

			// only update input data if window client area is nonzero
			if (clientWidth > 0 && clientHeight > 0)
			{
				// determine data size based on update mode and mode-specific settings
				switch (updateMode)
				{
					case GRASTERSURFACE_INPUTS::UPDATE_MODE_SUBSET:
						dataWidth = fixedDataWidth >> 1;
						dataHeight = fixedDataHeight >> 1;
						break;

					case GRASTERSURFACE_INPUTS::UPDATE_MODE_SMART:
						dataWidth = useFixedSize ? fixedDataWidth : static_cast<unsigned int>(clientWidth  * smartUpdateDataScaleX);
						dataHeight = useFixedSize ? fixedDataHeight : static_cast<unsigned int>(clientHeight * smartUpdateDataScaleY);
						break;

					default:
						dataWidth = clientWidth;
						dataHeight = clientHeight;
						break;
				}
				numPixels = dataWidth * dataHeight;

				// clear surface if enabled
				if (clearSurface) surface.Clear(clearColor);
				// get canvas to draw to
				static unsigned int* raster;
				if (updateMode != GRASTERSURFACE_INPUTS::UPDATE_MODE_DIRECT
					|| (updateMode == GRASTERSURFACE_INPUTS::UPDATE_MODE_DIRECT && -surface.LockUpdateBufferWrite(&raster, dataWidth, dataHeight))) // if set to direct update mode, try to get surface's update buffer
				{
					raster = pixelBuffer; // if not in direct mode or failed to get surface's buffer, use test's buffer instead
				}

				// draw new data to surface
				// calculate how many rows of the bitmap fit into 256 KB
				unsigned int rows = rows = 262144 / (dataWidth * sizeof(unsigned int));
				if (rows == 0) rows = 1; // min of 1
				// store data in struct
				RasterData rasterData = {};
				rasterData.dataWidth = dataWidth;
				rasterData.imageBuffer = imageBuffer;
				// branch on rows and draw
				char drawModeBuffer[32];
				switch (drawMode)
				{
					case GRASTERSURFACE_INPUTS::DRAW_MODE_NONE:
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(drawModeBuffer, "None");
						break;

					case GRASTERSURFACE_INPUTS::DRAW_MODE_SOLID:
						gClearThread.BranchParallel(DrawPixelRowSolid,
							rows, dataHeight, reinterpret_cast<const void*>(&rasterData), 0, static_cast<const void*>(nullptr), static_cast<int>(dataWidth * sizeof(unsigned int)), raster);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(drawModeBuffer, "Solid");
						break;

					case GRASTERSURFACE_INPUTS::DRAW_MODE_PATTERN:
						gClearThread.BranchParallel(DrawPixelRowPattern,
							rows, dataHeight, reinterpret_cast<const void*>(&rasterData), 0, static_cast<const void*>(nullptr), static_cast<int>(dataWidth * sizeof(unsigned int)), raster);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(drawModeBuffer, "Pattern");
						break;

					case GRASTERSURFACE_INPUTS::DRAW_MODE_RANDOM:
						gClearThread.BranchParallel(DrawPixelRowRandom,
							rows, dataHeight, reinterpret_cast<const void*>(&rasterData), 0, static_cast<const void*>(nullptr), static_cast<int>(dataWidth * sizeof(unsigned int)), raster);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(drawModeBuffer, "Random");
						break;

					case GRASTERSURFACE_INPUTS::DRAW_MODE_IMAGE:
						gClearThread.BranchParallel(DrawPixelRowImage,
							rows, dataHeight, reinterpret_cast<const void*>(&rasterData), 0, static_cast<const void*>(nullptr), static_cast<int>(dataWidth * sizeof(unsigned int)), raster);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(drawModeBuffer, "Image");
						break;

					default:
						break;
				}
				if (-gClearThread.Converge(0))
				{
					RasterSurfaceTest_DebugPrint(0, "visual test - converge failed\n");
					isRunning = false;
					return GW::GReturn::FAILURE;
				}

				// update surface
				char updateModeBuffer[32];
				switch (updateMode)
				{
					case GRASTERSURFACE_INPUTS::UPDATE_MODE_BASIC:
						surface.UpdateSurface(pixelBuffer, numPixels);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(updateModeBuffer, "Basic");
						break;

					case GRASTERSURFACE_INPUTS::UPDATE_MODE_SUBSET:
						switch (updateFlags & bitmaskAlignX)
						{
							case GW::GRAPHICS::ALIGN_X_LEFT:
								destX = -(dataWidth >> 1);
								break;
							case GW::GRAPHICS::ALIGN_X_RIGHT:
								destX = clientWidth - (dataWidth >> 1);
								break;
							case GW::GRAPHICS::ALIGN_X_CENTER:
							default:
								destX = (clientWidth >> 1) - (dataWidth >> 1);
								break;
						}
						switch (updateFlags & bitmaskAlignY)
						{
							case GW::GRAPHICS::ALIGN_Y_TOP:
								destY = -(dataHeight >> 1);
								break;
							case GW::GRAPHICS::ALIGN_Y_BOTTOM:
								destY = clientHeight - (dataHeight >> 1);
								break;
							case GW::GRAPHICS::ALIGN_Y_CENTER:
							default:
								destY = (clientHeight >> 1) - (dataHeight >> 1);
								break;
						}
						surface.UpdateSurfaceSubset(pixelBuffer, dataWidth, dataHeight, 0, destX, destY);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(updateModeBuffer, "Subset");
						break;

					case GRASTERSURFACE_INPUTS::UPDATE_MODE_SMART:
						surface.SmartUpdateSurface(pixelBuffer, numPixels, dataWidth, updateFlags);
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(updateModeBuffer, "Smart");
						break;

					case GRASTERSURFACE_INPUTS::UPDATE_MODE_DIRECT:
						surface.UnlockUpdateBufferWrite();
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
						strcpy(updateModeBuffer, "Direct");
						break;

					default:
						break;
				}

				// present to window
				if (presentSurface)
					surface.Present();

				// update framerate
				if (RasterSurfaceTest_GetTickCountMs() - tickCountPrev > 1000) // only update once per second
				{
					framerate = static_cast<int>(frameCount - frameCountPrev);
					// store frame and tick counts from this title update
					frameCountPrev = frameCount;
					tickCountPrev = RasterSurfaceTest_GetTickCountMs();
				}

				// update window title with relevant information
				char titleBuffer[256];
				//		char count:  |0   |5   |10  |15  |20  |25  |30  |35  |40  |45  |50  |55  |60  |65  |70  |75  |80  |85  |90  |95  |100 |105 |110 |115 |120 |125 |130 |135 |140 |145 |150
#pragma GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
				sprintf(titleBuffer, "GRasterSurface | %dx%d | Clear: %c | Present: %c | Draw: %s | Update: %s | FPS: %d (Capped: %c)",
					clientWidth,
					clientHeight,
					(clearSurface ? 'Y' : 'N'),
					(presentSurface ? 'Y' : 'N'),
					drawModeBuffer,
					updateModeBuffer,
					framerate,
					(capFps ? 'Y' : 'N')
				);
				window.SetWindowName(titleBuffer);
			}

			RasterSurfaceTest_DebugPrint(0, "\nvisual test - frame %llu finished\n\n", frameCount);
			++frameCount;
			return GW::GReturn::SUCCESS;
		}
		bool Run()
		{
			return (+window.ProcessWindowEvents() && isRunning);
		}
	} test;

	RasterSurfaceTest_DebugPrint(0, "\nStarting run daemon\n\n");
	GW::SYSTEM::GDaemon runDaemon;
	runDaemon.Create(FRAMERATE_MS, [&test, runDaemon]() mutable
		{
			if (test.capFps)
				test.Update();
		});

	while (test.Run())
	{
		if (test.capFps)
			std::this_thread::yield();
		else
			test.Update();
	}

	// due to stack unwind order we need to shutdown the daemon before
	//   "test" falls off the stack (otherwise BranchParallel can access things it shouldn't)
	runDaemon = nullptr; // the destructor forces a wait on all outstanding jobs
	// now "test" is safe to destruct
}
#endif // GRASTERSURFACE_TEST_VISUAL


#if defined(_WIN32) || defined(__APPLE__) || defined(__linux__)
#undef GRASTERSURFACE_TEST_SUPPRESS_DEPRECATION_WARNING
#endif


#if defined(_WIN32) && !defined(NDEBUG) && defined(GRASTERSURFACE_TEST_DEBUG_VERBOSE_NEW)
#undef new
#endif

#if defined(GRASTERSURFACE_TEST_DEBUG_VERBOSE_NEW)
#undef GRASTERSURFACE_TEST_DEBUG_VERBOSE_NEW
#endif
#if defined(GRASTERSURFACE_TEST_DEBUG_PRINTS)
#undef GRASTERSURFACE_TEST_DEBUG_PRINTS
#endif
#if defined(GRASTERSURFACE_TEST_DEBUG_PRINT_PRIORITY_LEVEL)
#undef GRASTERSURFACE_TEST_DEBUG_PRINT_PRIORITY_LEVEL
#endif


#endif // defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GRASTERSURFACE)
