#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GBLITTER)
#include <bitset> // needed for visual test
#include <chrono> // needed for blitter_GetTickCountMs()

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
static const char* resourceFolder = "/Resources/GBlitter_TestFolder/";
#else
static const char* resourceFolder = "../../../UnitTests/Resources/GBlitter_TestFolder/";
#endif
static const int maxPathLength = 260; // maximum filepath length on win32 by default

bool enableBlitterVisualTest = true;
TEST_CASE("GBlitter - check for GInput enabled & OS GUI support", "[Graphics]")
{
#if !defined(GATEWARE_ENABLE_INPUT) || defined(GATEWARE_DISABLE_GINPUT)
	enableBlitterVisualTest = false;
	std::cout << "WARNING: GINPUT NOT ENABLED; SKIPPING GBLITTER VISUAL TEST" << std::endl;
	return;
#endif
	GW::SYSTEM::GWindow window;
	if (-window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		enableBlitterVisualTest = false;
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING GBLITTER VISUAL TEST" << std::endl;
		return;
	}
	else
		std::cout << "GINPUT ENABLED & OS WINDOWING SUPPORT DETECTED: RUNNING GBLITTER VISUAL TEST" << std::endl;
}


#define GBLITTER_TEST_VERBOSE_NEW				// uncomment this line to override the "new" keyword with a macro that stores additional data to track down memory leaks more easily (WIN32 Debug only)

#if defined(_WIN32) && !defined(NDEBUG) && defined(GBLITTER_TEST_VERBOSE_NEW)
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif


#define GBLITTER_TEST_EMPTY_PROXY
#define GBLITTER_TEST_CREATE
#define GBLITTER_TEST_LOAD_SOURCE
#define GBLITTER_TEST_IMPORT_SOURCE
#define GBLITTER_TEST_IMPORT_SOURCE_COMPLEX
#define GBLITTER_TEST_DEFINE_TILES
#define GBLITTER_TEST_SET_TILE_MASK_VALUES
#define GBLITTER_TEST_DISCARD_SOURCES
#define GBLITTER_TEST_DISCARD_TILES
#define GBLITTER_TEST_CLEAR
#define GBLITTER_TEST_CLEAR_COLOR
#define GBLITTER_TEST_CLEAR_LAYER
#define GBLITTER_TEST_CLEAR_STENCIL
#define GBLITTER_TEST_DRAW_DEFERRED
#define GBLITTER_TEST_DRAW_IMMEDIATE
#define GBLITTER_TEST_FLUSH
#define GBLITTER_TEST_EXPORT_RESULT
#define GBLITTER_TEST_EXPORT_RESULT_COMPLEX
#define GBLITTER_TEST_VISUAL


// VISUAL TEST DOCUMENTATION BELOW
/*
	This test's purpose is visual confirmation of drawing and other functionality that cannot practically be verified with a REQUIRE.
	Information is displayed in the window title.
		DrawOptions flag states are displayed as 0000000ATMSLCHVR; Caps = on, lowercase = off.
			0 : 				- flag_15
			0 : 				- flag_14
			0 : 				- flag_13
			0 : 				- flag_12
			0 : 				- flag_11
			0 : 				- flag_10
			0 : 				- flag_9
			A : (Alpha)			- USE_TRANSPARENCY
			T : (Transform)		- USE_TRANSFORMATIONS
			M : (Mask)			- USE_MASKING
			S : (Stencils)		- USE_SOURCE_STENCILS
			L : (Layers)		- USE_SOURCE_LAYERS
			C : (Colors)		- IGNORE_SOURCE_COLORS
			H : (Horizontal)	- MIRROR_HORIZONTAL
			V : (Vertical)		- MIRROR_VERTICAL
			R : (Rotate)		- ROTATE


	CONTROLS :

	Close window								: ESC

	--- Output upscaling ---
	1x											: 1
	2x											: 2
	4x											: 3
	8x											: 4
	16x											: 5

	--- Output panning ---
	Pan left									: LEFTARROW
	Pan right									: RIGHTARROW
	Pan up										: UPARROW
	Pan down									: DOWNARROW

	--- Draw mode ---
	DrawDeferred								: BACKSPACE (temporarily DELETE instead until bug with GInput is fixed)
	DrawImmediate								: \

	--- Toggle post processing effects ---
	Layer values								: `
	Stencil values								: TAB

	--- Pipeline stage to display ---
	Procedurally generated sources				: 7
	Exported blitter result						: 8

	--- Procedural source to display ---
	Previous source								: [
	Next source									: ]

	--- Test set to draw ---
	Previous set								: 9
	Next set									: 0

	--- Test to draw from test set ---
	Previous test								: -
	Next test									: =

	--- Freeform test controls ---
	Previous tile								: Q
	Next tile									: E

	Control position							: T
	Control pivot								: Y
	Control scale								: U
	Control shear								: I
	Control rotation							: O

	Active control X axis -						: A
	Active control X axis +						: D
	Active control Y axis -						: W
	Active control Y axis +						: S
	Active control Z axis -						: F
	Active control Z axis +						: R

	Decrease speed of active control (gross)	: END
	Increase speed of active control (gross)	: HOME
	Decrease speed of active control (fine)		: PAGEDOWN
	Increase speed of active control (fine)		: PAGEUP

	Reset active control axes					: ENTER
	Reset active control speed					: SPACE

	Toggle DrawOptions::ROTATE					: /
	Toggle DrawOptions::MIRROR_HORIZONTAL		: .
	Toggle DrawOptions::MIRROR_VERTICAL			: ,
	Toggle DrawOptions::IGNORE_SOURCE_COLORS	: M
	Toggle DrawOptions::USE_SOURCE_LAYERS		: N
	Toggle DrawOptions::USE_SOURCE_COLORS		: B
	Toggle DrawOptions::USE_MASKING				: V
	Toggle DrawOptions::USE_TRANSFORMATIONS		: C
	Toggle DrawOptions::USE_TRANSPARENCY		: X
	Toggle DrawOptions::[flag_9]				: Z
	Toggle DrawOptions::[flag_10]				: '
	Toggle DrawOptions::[flag_11]				: ;
	Toggle DrawOptions::[flag_12]				: L
	Toggle DrawOptions::[flag_13]				: K
	Toggle DrawOptions::[flag_14]				: J
	Toggle DrawOptions::[flag_15]				: H
*/


// returns tick count since program startup in milliseconds
static inline unsigned long long blitter_GetTickCountMs()
{
	return std::chrono::duration_cast<std::chrono::milliseconds> (std::chrono::steady_clock::now().time_since_epoch()).count();
}

// returns full filepath of a file within the tests' resource folder
static inline void blitter_FilenameToFilepath(const char* _inFilename, char* _outFilepath)
{
//	strcpy_s(_outFilepath, maxPathLength, resourceFolder);
//	strcat_s(_outFilepath, maxPathLength, _inFilename);
	strcpy(_outFilepath, resourceFolder);
	strcat(_outFilepath, _inFilename);
}


#if defined(GBLITTER_TEST_EMPTY_PROXY)
TEST_CASE("GBlitter core method test battery", "[Graphics]")
{
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();

	WHEN("Blitter has not been created")
	{
		THEN("All functions should return EMPTY_PROXY")
		{
			REQUIRE(blitter.LoadSource(nullptr, nullptr, nullptr, sourceIndex) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.ImportSource(nullptr, nullptr, nullptr, 0, 0, 0, 0, sourceIndex) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.ImportSourceComplex(nullptr, 0, 0, nullptr, 0, 0, nullptr, 0, 0, 0, 0, sourceIndex) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.DefineTiles(nullptr, 0, nullptr) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.DiscardSources(nullptr, 0) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.DiscardTiles(nullptr, 0) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.Clear(0, 0, 0) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.DrawDeferred(nullptr, 0) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.Flush() == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.ExportResult(false, 0, 0, 0, 0, nullptr, nullptr, nullptr) == GW::GReturn::EMPTY_PROXY);
			REQUIRE(blitter.ExportResultComplex(false, 0, 0, 0, 0, 0, 0, 0, 0, nullptr, nullptr, nullptr) == GW::GReturn::EMPTY_PROXY);
		}
	}
}
#endif // GBLITTER_TEST_EMPTY_PROXY

#if defined(GBLITTER_TEST_CREATE)
TEST_CASE("GBlitter Create checks", "[Graphics]")
{
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	SECTION("Create positive checks")
	{
		REQUIRE(blitter.Create(canvasWidth, canvasHeight) == GW::GReturn::SUCCESS);
	}
}
#endif // GBLITTER_TEST_CREATE

#if defined(GBLITTER_TEST_LOAD_SOURCE)
TEST_CASE("GBlitter LoadSource checks", "[Graphics]")
{
	enum class FormatTestType {
		// TGA
		// uncompressed			runtime-length encoded
		TGA_LUMINENCE,			TGA_LUMINENCE_RLE,
		TGA_ALPHA_LUMINENCE,	TGA_ALPHA_LUMINENCE_RLE,
		TGA_RGB_24,				TGA_RGB_24_RLE,
		TGA_ARGB_32,			TGA_ARGB_32_RLE,
		TGA_LUT_8,				TGA_LUT_8_RLE,

		// BMP 
		BMP_RGB_24,				BMP_RGB_24_RLE,
		BMP_RGBA_32
	};

	struct FormatTestInfo {
		const char* colorsFilepath = nullptr;
		const char* layersFilepath = nullptr;
		const char* stencilsFilepath = nullptr;
	};

	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	const unsigned short						maxSources = 48;
	unsigned short								sourceIndices[maxSources];

	unsigned short								bmp_source_buffer;

	// load the table.. 
	// tests will run on dataType respectivly and if all 3 are passed in then 
	// it will run all 3 at onces.

	// TGA Tests...
	FormatTestInfo tga_luminence;
	tga_luminence.colorsFilepath = "tga_test_files/64_64_L_8.tga"; // colors filepath
	tga_luminence.layersFilepath = "tga_test_files/64_64_L_8.tga";	// layers filepath 
	tga_luminence.stencilsFilepath = "tga_test_files/64_64_L_8.tga";	// stencils filepath 

	FormatTestInfo tga_alpha_luminence;
	tga_alpha_luminence.colorsFilepath = "tga_test_files/64_64_AL_16.tga"; // colors filepath
	tga_alpha_luminence.layersFilepath = "tga_test_files/64_64_AL_16.tga";	// layers filepath 
	tga_alpha_luminence.stencilsFilepath = "tga_test_files/64_64_AL_16.tga";	// stencils filepath 

	FormatTestInfo tga_rgb_24;
	tga_rgb_24.colorsFilepath = "tga_test_files/64_64_RGB_24.tga";
	tga_rgb_24.layersFilepath = "tga_test_files/64_64_RGB_24.tga";
	tga_rgb_24.stencilsFilepath = "tga_test_files/64_64_RGB_24.tga";

	FormatTestInfo tga_argb_32;
	tga_argb_32.colorsFilepath = "tga_test_files/64_64_ARGB_32.tga";
	tga_argb_32.layersFilepath = "tga_test_files/64_64_ARGB_32.tga";
	tga_argb_32.stencilsFilepath = "tga_test_files/64_64_ARGB_32.tga";


	
	FormatTestInfo tga_rgb_lut;
	tga_rgb_lut.colorsFilepath = "tga_test_files/64_64_RGB_LUT_8.tga";
	tga_rgb_lut.layersFilepath = "tga_test_files/64_64_RGB_LUT_8.tga";
	tga_rgb_lut.stencilsFilepath = "tga_test_files/64_64_RGB_LUT_8.tga";
	
	FormatTestInfo tga_luminence_RLE;
	tga_luminence_RLE.colorsFilepath = "tga_test_files/64_64_L_8_RLE.tga"; // colors filepath
	tga_luminence_RLE.layersFilepath = "tga_test_files/64_64_L_8_RLE.tga";	// layers filepath 
	tga_luminence_RLE.stencilsFilepath = "tga_test_files/64_64_L_8_RLE.tga";	// stencils filepath 

	FormatTestInfo tga_alpha_luminence_RLE;
	tga_alpha_luminence_RLE.colorsFilepath = "tga_test_files/64_64_AL_16_RLE.tga"; // colors filepath
	tga_alpha_luminence_RLE.layersFilepath = "tga_test_files/64_64_AL_16_RLE.tga";	// layers filepath 
	tga_alpha_luminence_RLE.stencilsFilepath = "tga_test_files/64_64_AL_16_RLE.tga";	// stencils filepath 

	FormatTestInfo tga_rgb_24_RLE;
	tga_rgb_24_RLE.colorsFilepath = "tga_test_files/64_64_RGB_24_RLE.tga";
	tga_rgb_24_RLE.layersFilepath = "tga_test_files/64_64_RGB_24_RLE.tga";
	tga_rgb_24_RLE.stencilsFilepath = "tga_test_files/64_64_RGB_24_RLE.tga";

	FormatTestInfo tga_argb_32_RLE;
	tga_argb_32_RLE.colorsFilepath = "tga_test_files/64_64_ARGB_32_RLE.tga";
	tga_argb_32_RLE.layersFilepath = "tga_test_files/64_64_ARGB_32_RLE.tga";
	tga_argb_32_RLE.stencilsFilepath = "tga_test_files/64_64_ARGB_32_RLE.tga";

	FormatTestInfo tga_rgb_lut_RLE;
	tga_rgb_lut_RLE.colorsFilepath = "tga_test_files/64_64_RGB_LUT_8_RLE.tga";
	tga_rgb_lut_RLE.layersFilepath = "tga_test_files/64_64_RGB_LUT_8_RLE.tga";
	tga_rgb_lut_RLE.stencilsFilepath = "tga_test_files/64_64_RGB_LUT_8_RLE.tga";



	// BMP Tests...
	FormatTestInfo bmp_rgb_24;
	bmp_rgb_24.colorsFilepath = "bmp_test_files/64_64_rgb24.bmp";
	bmp_rgb_24.layersFilepath = "bmp_test_files/64_64_rgb24.bmp";
	bmp_rgb_24.stencilsFilepath = "bmp_test_files/64_64_rgb24.bmp";
	
	FormatTestInfo bmp_rgba_32;
	bmp_rgba_32.colorsFilepath = "bmp_test_files/64_64_rgba32.bmp";
	bmp_rgba_32.layersFilepath = "bmp_test_files/64_64_rgba32.bmp";
	bmp_rgba_32.stencilsFilepath = "bmp_test_files/64_64_rgba32.bmp";



	std::map<FormatTestType, FormatTestInfo> imageFormatTests;
	imageFormatTests.emplace(std::make_pair(FormatTestType::TGA_LUMINENCE, tga_luminence));
	imageFormatTests.emplace(std::make_pair(FormatTestType::TGA_ALPHA_LUMINENCE, tga_alpha_luminence));
	imageFormatTests.emplace(std::make_pair(FormatTestType::TGA_RGB_24, tga_rgb_24));
	imageFormatTests.emplace(std::make_pair(FormatTestType::TGA_ARGB_32,tga_argb_32));
	imageFormatTests.emplace(std::make_pair(FormatTestType::TGA_LUT_8, tga_rgb_lut));
	imageFormatTests.emplace(std::make_pair(FormatTestType::BMP_RGB_24, bmp_rgb_24));
	imageFormatTests.emplace(std::make_pair(FormatTestType::BMP_RGBA_32, bmp_rgba_32));
	
	std::map<FormatTestType, FormatTestInfo> imageFormatTestsRLE;
	imageFormatTestsRLE.emplace(std::make_pair(FormatTestType::TGA_LUMINENCE_RLE, tga_luminence_RLE));
	imageFormatTestsRLE.emplace(std::make_pair(FormatTestType::TGA_ALPHA_LUMINENCE_RLE, tga_alpha_luminence_RLE));
	imageFormatTestsRLE.emplace(std::make_pair(FormatTestType::TGA_RGB_24_RLE, tga_rgb_24_RLE));
	imageFormatTestsRLE.emplace(std::make_pair(FormatTestType::TGA_ARGB_32_RLE, tga_argb_32_RLE));
	imageFormatTestsRLE.emplace(std::make_pair(FormatTestType::TGA_LUT_8_RLE, tga_rgb_lut_RLE));


	SECTION("LoadSource positive checks")
	{
		WHEN("Valid uncompressed filepaths are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Sources should load and indices should be set")
			{
				unsigned short s = 0;

				for (const std::pair<FormatTestType, FormatTestInfo>& entry : imageFormatTests)
				{
					FormatTestInfo test = entry.second;

					int i = 0;
					
					// test each individually.

					if (test.colorsFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.colorsFilepath, filepath);
						REQUIRE(blitter.LoadSource(filepath, nullptr, nullptr, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}
					if (test.layersFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.colorsFilepath, filepath);
						REQUIRE(blitter.LoadSource(nullptr, filepath, nullptr, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}
					if (test.stencilsFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.colorsFilepath, filepath);
						REQUIRE(blitter.LoadSource(nullptr, nullptr, filepath, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}

					// is this test valid to test all 3 
					if (i == 3) {
						char color[maxPathLength];
						char layer[maxPathLength];
						char stencil[maxPathLength];
						
						blitter_FilenameToFilepath(test.colorsFilepath, color);
						blitter_FilenameToFilepath(test.layersFilepath, layer);
						blitter_FilenameToFilepath(test.stencilsFilepath, stencil);

						REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);
					}

				}


				// mixed channel tests...
				char color[maxPathLength];
				char layer[maxPathLength];
				char stencil[maxPathLength];

				blitter_FilenameToFilepath(tga_luminence.colorsFilepath, color);
				blitter_FilenameToFilepath(tga_alpha_luminence.layersFilepath, layer);
				blitter_FilenameToFilepath(tga_rgb_24.stencilsFilepath, stencil);

				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);
				

				blitter_FilenameToFilepath(tga_rgb_24.colorsFilepath, color);
				blitter_FilenameToFilepath(tga_argb_32.layersFilepath, layer);
				blitter_FilenameToFilepath(tga_rgb_lut.stencilsFilepath, stencil);
				
				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);

				blitter_FilenameToFilepath(tga_rgb_lut.colorsFilepath, color);
				blitter_FilenameToFilepath(tga_luminence.layersFilepath, layer);
				blitter_FilenameToFilepath(tga_alpha_luminence.stencilsFilepath, stencil);

				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);

				blitter_FilenameToFilepath(tga_alpha_luminence.colorsFilepath, color);
				blitter_FilenameToFilepath(tga_rgb_24.layersFilepath, layer);
				blitter_FilenameToFilepath(tga_argb_32.stencilsFilepath, stencil);

				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);

				blitter_FilenameToFilepath(tga_luminence.colorsFilepath, color);
				blitter_FilenameToFilepath(bmp_rgba_32.layersFilepath, layer);
				blitter_FilenameToFilepath(bmp_rgb_24.stencilsFilepath, stencil);

				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);

				blitter_FilenameToFilepath(bmp_rgba_32.colorsFilepath, color);
				blitter_FilenameToFilepath(tga_luminence.layersFilepath, layer);
				blitter_FilenameToFilepath(bmp_rgb_24.stencilsFilepath, stencil);

				REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == i);
			}

		}

		WHEN("Valid run-length encoded filepaths are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Sources should load and indices should be set")
			{
				unsigned short s = 0;


				for (const std::pair<FormatTestType, FormatTestInfo>& entry : imageFormatTestsRLE)
				{
					FormatTestInfo test = entry.second;

					int i = 0;

					// test each individually.

					if (test.colorsFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.colorsFilepath, filepath);
						REQUIRE(blitter.LoadSource(filepath, nullptr, nullptr, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}
					if (test.layersFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.layersFilepath, filepath);
						REQUIRE(blitter.LoadSource(nullptr, filepath, nullptr, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}
					if (test.stencilsFilepath != nullptr) {
						char filepath[maxPathLength];
						blitter_FilenameToFilepath(test.stencilsFilepath, filepath);
						REQUIRE(blitter.LoadSource(nullptr, nullptr, filepath, sourceIndices[s++]) == GW::GReturn::SUCCESS);
						i++;
					}

					// is this test valid to test all 3 
					if (i == 3) {
						char color[maxPathLength];
						char layer[maxPathLength];
						char stencil[maxPathLength];

						blitter_FilenameToFilepath(test.colorsFilepath, color);
						blitter_FilenameToFilepath(test.layersFilepath, layer);
						blitter_FilenameToFilepath(test.stencilsFilepath, stencil);

						REQUIRE(blitter.LoadSource(color, layer, stencil, sourceIndices[s++]) == GW::GReturn::SUCCESS);
					}

				}

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == i);
			}
		}
	}

	// negative tests... more direct.
	char										colors_ARGB_32[maxPathLength];
	char										layers_ARGB_32[maxPathLength];
	char										stencils_ARGB_32[maxPathLength];

	SECTION("LoadSource negative checks")
	{
		WHEN("Invalid color filepath is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("invalid",									colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that a path was invalid")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			nullptr,				sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Invalid layer filepath is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("invalid",									layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that a path was invalid")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Invalid stencil filepath is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("invalid",									stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that a path was invalid")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}
		
		WHEN("Path to nonexistent color file is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("nonexistent.tga",							colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should not find a source to load")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			nullptr,				sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}
		
		WHEN("Path to nonexistent layer file is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("nonexistent.tga",							layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should not find a source to load")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}
		
		WHEN("Path to nonexistent stencil file is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("nonexistent.tga",							stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should not find a source to load")
			{
				unsigned short s = 0;
				REQUIRE(blitter.LoadSource(nullptr,				nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FILE_NOT_FOUND);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Color file with unsupported extension is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("file.unsupported_extension",				colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should reject the unsupported file extension")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Layer file with unsupported extension is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("file.unsupported_extension",				layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should reject the unsupported file extension")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Stencil file with unsupported extension is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("file.unsupported_extension",				stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should reject the unsupported file extension")
			{
				unsigned short s = 0;
				
				REQUIRE(blitter.LoadSource(nullptr,				nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("TGA file wider than max width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/1_65536_ARGB_32.tga",		colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/1_65536_ARGB_32.tga",		layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/1_65536_ARGB_32.tga",		stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that the source is invalid")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(nullptr,				nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("TGA file taller than max height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/65536_1_ARGB_32.tga",		colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/65536_1_ARGB_32.tga",		layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/65536_1_ARGB_32.tga",		stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that the source is invalid")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);
				REQUIRE(blitter.LoadSource(nullptr,				nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::FORMAT_UNSUPPORTED);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Color and layer/stencil files with mismatched widths are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/63_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/63_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that an invalid argument was passed")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Layer and stencil files with mismatched widths are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/63_64_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that an invalid argument was passed")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Color and layer/stencil files with mismatched heights are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			colors_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_63_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_63_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that an invalid argument was passed")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(colors_ARGB_32,		layers_ARGB_32,		nullptr,				sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(blitter.LoadSource(colors_ARGB_32,		nullptr,			stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Layer and stencil files with mismatched heights are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			blitter_FilenameToFilepath("tga_test_files/64_64_ARGB_32.tga",			layers_ARGB_32);
			blitter_FilenameToFilepath("tga_test_files/64_63_ARGB_32.tga",			stencils_ARGB_32);

			for (unsigned int i = 0; i < maxSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("The blitter should indicate that an invalid argument was passed")
			{
				unsigned short s = 0;

				REQUIRE(blitter.LoadSource(nullptr,				layers_ARGB_32,		stencils_ARGB_32,		sourceIndices[s++]) == GW::GReturn::INVALID_ARGUMENT);

				for (unsigned int i = 0; i < s; ++i)
					REQUIRE(sourceIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		// Test cases for exceeding maximum number of sources are not practical due to the massive amount of overhead required.


	}

}
#endif // GBLITTER_TEST_LOAD_SOURCE

#if defined(GBLITTER_TEST_IMPORT_SOURCE)
TEST_CASE("GBlitter ImportSource checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned char								sourceLayerValue = 0xac;
	unsigned char								sourceStencilValue = 0xbd;

	unsigned int*								sourceColors = nullptr;
	unsigned char*								sourceLayers = nullptr;
	unsigned char*								sourceStencils = nullptr;
	const unsigned short						numSources = 4;
	unsigned short								sourceIndices[numSources];

	SECTION("ImportSource positive checks")
	{
		WHEN("Valid data is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			sourceLayers = new unsigned char[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			sourceStencils = new unsigned char[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Sources should be imported and indices returned")
			{
				REQUIRE(blitter.ImportSource(sourceColors,	nullptr,	nullptr,	canvasWidth, canvasHeight, 0, 0, sourceIndices[0]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSource(nullptr,	sourceLayers,		nullptr,	canvasWidth, canvasHeight, 0, 0, sourceIndices[1]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSource(nullptr,	nullptr,	sourceStencils,	canvasWidth, canvasHeight, 0, 0, sourceIndices[2]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSource(sourceColors,	sourceLayers,		sourceStencils,	canvasWidth, canvasHeight, 0, 0, sourceIndices[3]) == GW::GReturn::SUCCESS);
				for (unsigned short i = 0; i < numSources; ++i)
					REQUIRE(sourceIndices[i] == i);
			}

			delete[] sourceColors;
			delete[] sourceLayers;
			delete[] sourceStencils;
		}
	}

	SECTION("ImportSource negative checks")
	{
		WHEN("No pixel data is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSource(nullptr, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Invalid width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSource(sourceColors, nullptr, nullptr, 0, canvasHeight, 0, 0, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Invalid height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, 0, 0, 0, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Nonzero stride that is less than width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-03-07
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, canvasWidth - 1, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] sourceColors;
		}

		// Test cases for exceeding maximum number of sources are not practical due to the massive amount of overhead required.
	}
}
#endif // GBLITTER_TEST_IMPORT_SOURCE

#if defined(GBLITTER_TEST_IMPORT_SOURCE_COMPLEX)
TEST_CASE("GBlitter ImportSourceComplex checks", "[Graphics]")
{
	struct Pixel
	{
		unsigned int	color;
		unsigned char	layer;
		unsigned char	stencil;
		unsigned char	pad[2];
	};

	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned char								sourceLayerValue = 0xac;
	unsigned char								sourceStencilValue = 0xbd;

	unsigned char*								byteData = nullptr;
	Pixel*										pixelData = nullptr;
	unsigned int*								sourceColors = nullptr;
	unsigned char*								sourceLayers = nullptr;
	unsigned char*								sourceStencils = nullptr;
	unsigned short								colorStride = 0;
	unsigned short								colorRowStride = 0;
	unsigned short								layerStride = 0;
	unsigned short								layerRowStride = 0;
	unsigned short								stencilStride = 0;
	unsigned short								stencilRowStride = 0;

	const unsigned short						numSources = 4;
	unsigned short								sourceIndices[numSources];

	SECTION("ImportSourceComplex positive checks")
	{
		WHEN("Valid data arranged in separate arrays is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-05-04
			sourceLayers = new unsigned char[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-05-04
			sourceStencils = new unsigned char[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Sources should be imported and indices returned")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					nullptr, 0, 0, nullptr,
					0, 0,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					sourceLayers, layerStride, layerRowStride,
					nullptr, 0, 0,
					canvasWidth, canvasHeight, sourceIndices[1]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					nullptr, 0, 0,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[2]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[3]) == GW::GReturn::SUCCESS);
				for (unsigned short i = 0; i < numSources; ++i)
					REQUIRE(sourceIndices[i] == i);
			}

			delete[] sourceColors;
			delete[] sourceLayers;
			delete[] sourceStencils;
		}

		WHEN("Valid data arranged in one large array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-04
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Sources should be imported and indices returned")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					nullptr, 0, 0, nullptr,
					0, 0,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					sourceLayers, layerStride, layerRowStride,
					nullptr, 0, 0,
					canvasWidth, canvasHeight, sourceIndices[1]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					nullptr, 0, 0,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[2]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[3]) == GW::GReturn::SUCCESS);
				for (unsigned short i = 0; i < numSources; ++i)
					REQUIRE(sourceIndices[i] == i);
			}

			delete[] byteData;
		}

		WHEN("Valid data arranged in a mixed array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			pixelData = new Pixel[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-05-04
			sourceColors = reinterpret_cast<unsigned int*>(&pixelData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&pixelData[0] + 4);
			sourceStencils = reinterpret_cast<unsigned char*>(&pixelData[0] + 5);
			for (unsigned int i = 0; i < static_cast<unsigned int>(canvasWidth * canvasHeight); ++i)
				pixelData[i] = { sourceColorValue, sourceLayerValue, sourceStencilValue };
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 8;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 8;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 8;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Sources should be imported and indices returned")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					nullptr, 0, 0, nullptr,
					0, 0,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					sourceLayers, layerStride, layerRowStride,
					nullptr, 0, 0,
					canvasWidth, canvasHeight, sourceIndices[1]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					nullptr, 0, 0,
					nullptr, 0, 0,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[2]) == GW::GReturn::SUCCESS);
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[3]) == GW::GReturn::SUCCESS);
				for (unsigned short i = 0; i < numSources; ++i)
					REQUIRE(sourceIndices[i] == i);
			}

			delete[] pixelData;
		}
	}

	SECTION("ImportSourceComplex negative checks")
	{
		WHEN("No pixel data is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			sourceColors = nullptr;
			sourceLayers = nullptr;
			sourceStencils = nullptr;

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					0, 0, sourceIndices[2]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}
		}

		WHEN("Zero width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 4]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					nullptr, 0, 0,
					nullptr, 0, 0,
					0, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 4]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					nullptr, 0, 0,
					nullptr, 0, 0,
					canvasWidth, 0, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero color byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, 0, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero color row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, 0,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero layer byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, 0, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero layer row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, 0,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero stencil byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, 0, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Zero stencil row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, 0,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Color row stride that is less than width * color byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth - 1;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Layer row stride that is less than width * layer byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth - 1;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Stencil row stride that is less than width * stencil byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth - 1;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Color byte stride that is greater than color row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorRowStride + 1, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Layer byte stride that is greater than layer row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerRowStride + 1, layerRowStride,
					sourceStencils, stencilStride, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		WHEN("Stencil byte stride that is greater than stencil row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			byteData = new unsigned char[canvasWidth * canvasHeight * 6]; // this will leak if test fails; solution unknown as of 2020-05-02
			sourceColors = reinterpret_cast<unsigned int*>(&byteData[0]);
			sourceLayers = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 4]);
			sourceStencils = reinterpret_cast<unsigned char*>(&byteData[canvasWidth * canvasHeight * 5]);
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			memset(sourceLayers, sourceLayerValue, canvasWidth * canvasHeight);
			memset(sourceStencils, sourceStencilValue, canvasWidth * canvasHeight);
			for (unsigned short i = 0; i < numSources; ++i)
				sourceIndices[i] = (std::numeric_limits<unsigned short>::max)();

			colorStride = 4;
			colorRowStride = colorStride * canvasWidth;
			layerStride = 1;
			layerRowStride = layerStride * canvasWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * canvasWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ImportSourceComplex(
					sourceColors, colorStride, colorRowStride,
					sourceLayers, layerStride, layerRowStride,
					sourceStencils, stencilRowStride + 1, stencilRowStride,
					canvasWidth, canvasHeight, sourceIndices[0]) == GW::GReturn::INVALID_ARGUMENT);
				REQUIRE(sourceIndices[0] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] byteData;
		}

		// Test cases for exceeding maximum number of sources are not practical due to the massive amount of overhead required.
	}
}
#endif // GBLITTER_TEST_IMPORT_SOURCE_COMPLEX

#if defined(GBLITTER_TEST_DEFINE_TILES)
TEST_CASE("GBlitter DefineTiles checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 3;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];

	SECTION("DefineTiles positive checks")
	{
		WHEN("Valid list of definitions and correct count are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			sourceIndex = (std::numeric_limits<unsigned short>::max)();
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Tiles should be defined and list of indices should be returned")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::SUCCESS);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == i);
			}

			delete[] sourceColors;
		}
	}

	SECTION("DefineTiles negative checks")
	{
		WHEN("Invalid definition array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(nullptr, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}
		}

		WHEN("Invalid index array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, nullptr) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Invalid count is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, 0, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with unset source index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { (std::numeric_limits<unsigned short>::max)(), 0, 10, 10, 10 }; // it's more compact to use an initializer list and set source_id manually back to the default value than it is to set every other value individually
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with invalid source index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { static_cast<unsigned short>(sourceIndex + 1), 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned short>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned short>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with x out of bounds is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 1000, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with y out of bounds is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 1000, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with zero width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 0, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with zero height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 0 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with oversized width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1000, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		WHEN("Definition with oversized height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 1000 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			tileDefinitions[2] = { sourceIndex, 0, 10, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();

			THEN("Blitter should indcate that an invalid argument was passed")
			{
				REQUIRE(blitter.DefineTiles(tileDefinitions, numTiles, tileIndices) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int i = 0; i < numTiles; ++i)
					REQUIRE(tileIndices[i] == (std::numeric_limits<unsigned int>::max)());
			}

			delete[] sourceColors;
		}

		// Test cases for exceeding maximum number of tiles are not practical due to the massive amount of overhead required.
	}
}
#endif // GBLITTER_TEST_DEFINE_TILES

#if defined(GBLITTER_TEST_SET_TILE_MASK_VALUES)
TEST_CASE("GBlitter SetTileMaskValues checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned char								sourceLayerValue = 0xac;
	unsigned char								sourceStencilValue = 0xbd;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 2;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];
	unsigned int								setInds[numTiles];

	SECTION("SetTileMaskValues positive checks")
	{
		WHEN("Valid array of tile indices is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			setInds[0] = tileIndices[0];
			setInds[1] = tileIndices[1];

			THEN("Blitter should remove the specified tileDefinitions")
			{
				REQUIRE(blitter.SetTileMaskValues(setInds, numTiles, sourceColorValue, sourceLayerValue, sourceStencilValue) == GW::GReturn::SUCCESS);
			}

			delete[] sourceColors;
		}
	}

	SECTION("SetTileMaskValues negative checks")
	{
		WHEN("Invalid array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.SetTileMaskValues(nullptr, numTiles, sourceColorValue, sourceLayerValue, sourceStencilValue) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count less than one is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			setInds[0] = tileIndices[0];
			setInds[1] = tileIndices[1];

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.SetTileMaskValues(setInds, 0, sourceColorValue, sourceLayerValue, sourceStencilValue) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count greater than number of tiles is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			setInds[0] = tileIndices[0];
			setInds[1] = tileIndices[1];

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.SetTileMaskValues(setInds, numTiles + 1, sourceColorValue, sourceLayerValue, sourceStencilValue) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Invalid index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			setInds[0] = tileIndices[0];
			setInds[1] = tileIndices[1] + 1;

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.SetTileMaskValues(setInds, numTiles, sourceColorValue, sourceLayerValue, sourceStencilValue) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_SET_TILE_MASK_VALUES

#if defined(GBLITTER_TEST_DISCARD_SOURCES)
TEST_CASE("GBlitter DiscardSources checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;

	unsigned int*								sourceColors = nullptr;
	const unsigned short						numSources = 2;
	unsigned short								sourceIndices[numSources];
	unsigned short								discardIndices[numSources];

	SECTION("DiscardSources positive checks")
	{
		WHEN("Valid array of source indices is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;

			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]);
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[1]);

			discardIndices[0] = sourceIndices[0];
			discardIndices[1] = sourceIndices[1];

			THEN("Blitter should remove the specified sources")
			{
				REQUIRE(blitter.DiscardSources(discardIndices, numSources) == GW::GReturn::SUCCESS);
			}

			delete[] sourceColors;
		}
	}

	SECTION("DiscardSources negative checks")
	{
		WHEN("Invalid array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;

			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]);
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[1]);

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.DiscardSources(nullptr, numSources) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count less than one is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;

			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]);
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[1]);

			discardIndices[0] = sourceIndices[0];
			discardIndices[1] = sourceIndices[1];

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.DiscardSources(discardIndices, 0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count greater than number of sources is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;

			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]);
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[1]);

			discardIndices[0] = sourceIndices[0];
			discardIndices[1] = sourceIndices[1];

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.DiscardSources(discardIndices, numSources + 1) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Invalid index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;

			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[0]);
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndices[1]);

			discardIndices[0] = sourceIndices[0];
			discardIndices[1] = sourceIndices[1] + 1;

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.DiscardSources(discardIndices, numSources) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_DISCARD_SOURCES

#if defined(GBLITTER_TEST_DISCARD_TILES)
TEST_CASE("GBlitter DiscardTile checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 2;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];
	unsigned int								discardIndices[numTiles];

	SECTION("DiscardTiles positive checks")
	{
		WHEN("Valid array of tile indices is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			discardIndices[0] = tileIndices[0];
			discardIndices[1] = tileIndices[1];

			THEN("Blitter should remove the specified tileDefinitions")
			{
				REQUIRE(blitter.DiscardTiles(discardIndices, numTiles) == GW::GReturn::SUCCESS);
			}

			delete[] sourceColors;
		}
	}

	SECTION("DiscardTiles negative checks")
	{
		WHEN("Invalid array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DiscardTiles(nullptr, numTiles) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count less than one is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			discardIndices[0] = tileIndices[0];
			discardIndices[1] = tileIndices[1];

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DiscardTiles(discardIndices, 0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Index count greater than number of tiles is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			discardIndices[0] = tileIndices[0];
			discardIndices[1] = tileIndices[1];

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DiscardTiles(discardIndices, numTiles + 1) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Invalid index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			
			tileDefinitions[0] = { sourceIndex, 0, 0, 10, 10 };
			tileDefinitions[1] = { sourceIndex, 10, 0, 10, 10 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			discardIndices[0] = tileIndices[0];
			discardIndices[1] = tileIndices[1] + 1;

			THEN("Blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DiscardTiles(discardIndices, numTiles) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_DISCARD_TILES

#if defined(GBLITTER_TEST_CLEAR)
TEST_CASE("GBlitter Clear checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;
	unsigned int								clearColor = 0x12345678;
	unsigned char								clearLayer = 0xac;
	unsigned char								clearStencil = 0xbd;

	SECTION("Clear positive checks")
	{
		WHEN("Color, layer, and stencil are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			THEN("Blitter canvas should be cleared to passed values")
			{
				REQUIRE(blitter.Clear(clearColor, clearLayer, clearStencil) == GW::GReturn::SUCCESS);
			}
		}
	}
}
#endif // GBLITTER_TEST_CLEAR

#if defined(GBLITTER_TEST_CLEAR_COLOR)
TEST_CASE("GBlitter ClearColor checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;
	unsigned int								clearColor = 0x12345678;

	SECTION("ClearColor positive checks")
	{
		WHEN("Color is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			THEN("Blitter canvas should be cleared to passed color")
			{
				REQUIRE(blitter.ClearColor(clearColor) == GW::GReturn::SUCCESS);
			}
		}
	}
}
#endif // GBLITTER_TEST_CLEAR_COLOR

#if defined(GBLITTER_TEST_CLEAR_LAYER)
TEST_CASE("GBlitter ClearLayer checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;
	unsigned char								clearLayer = 0xac;

	SECTION("ClearLayer positive checks")
	{
		WHEN("Layer is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			THEN("Blitter canvas should be cleared to passed layer")
			{
				REQUIRE(blitter.ClearLayer(clearLayer) == GW::GReturn::SUCCESS);
			}
		}
	}
}
#endif // GBLITTER_TEST_CLEAR_LAYER

#if defined(GBLITTER_TEST_CLEAR_STENCIL)
TEST_CASE("GBlitter ClearStencil checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;
	unsigned char								clearStencil = 0xbd;

	SECTION("ClearStencil positive checks")
	{
		WHEN("Stencil is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);

			THEN("Blitter canvas should be cleared to passed stencil")
			{
				REQUIRE(blitter.ClearStencil(clearStencil) == GW::GReturn::SUCCESS);
			}
		}
	}
}
#endif // GBLITTER_TEST_CLEAR_STENCIL

#if defined(GBLITTER_TEST_DRAW_DEFERRED)
TEST_CASE("GBlitter DrawDeferred checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 5;
	unsigned short								canvasHeight = 5;

	unsigned int								clearColor = 0x00000000;
	unsigned char								clearLayer = 0xff;
	unsigned char								clearStencil = 0x00;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned int								maskColor = 0x44444444;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 2;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];

	const unsigned int							numInstructions = 2926; // this is one more than will fit in a result block bin as of 2020-06-23
	const unsigned int							numDraws = 4;
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstructions[numInstructions];

	unsigned int*								resultColors = nullptr;

	SECTION("DrawDeferred positive checks")
	{
		WHEN("Valid draw instructions and count are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t
			drawInstructions[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions[3] = { tileIndices[1],	0,		0,		0,		{ 1, 2, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be stored for later drawing")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, numDraws) == GW::GReturn::SUCCESS);
				// verify that the canvas is clear before flushing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// export again with flush and verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(true, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if ((x == 0 && y == 0)
							|| (x == 1 && y == 1)
							|| (x == 2 && y == 1)
							|| (x == 1 && y == 2))
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("A result block in the blitter is saturated with instructions")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-23
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			for (unsigned int i = 0; i < numInstructions - 1; ++i)
			//											tile index		flags	layer	stencil	t
				drawInstructions[i]					= { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } }; // all of these duplicate instructions will be stacked in the same place
			drawInstructions[numInstructions - 1]	= { tileIndices[0],	0,		0,		0,		{ 1, 0, 0 } }; // this instruction should be rejected, since the ones before it will saturate the result block

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-23
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("All instructions that can fit should be stored for later drawing, and any excess should be rejected")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, numInstructions) == GW::GReturn::SUCCESS);
				// verify that the canvas is clear before flushing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// export again with flush
				REQUIRE(blitter.ExportResult(true, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				// verify that all stacked instructions were drawn
				REQUIRE(resultColors[0 + 0 * canvasWidth] == sourceColorValue);
				// verify that the excess instruction was not drawn
				REQUIRE(resultColors[1 + 0 * canvasWidth] == clearColor);
				// verify that the rest of the canvas is unchanged
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x < 2 && y == 0)
							continue;
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;

		}
		
		WHEN("Bitmasked instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			sourceColors[1] = maskColor; // set pixel at (1, 0) to the mask color to test masking
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			//					   source id	x  y  w  h  mask color
			tileDefinitions[0] = { sourceIndex, 0, 0, 2, 1, maskColor };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t
			// this instruction should be masked and only some color should be drawn
			drawInstructions[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_MASKING,
															0,		0,		{ 0, 0, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be stored for later drawing")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, 1) == GW::GReturn::SUCCESS);
				// verify that the canvas is clear before flushing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// export again with flush and verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(true, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x == 0 && y == 0) // of the 2-pixel wide tile, only the first pixel should be drawn due to bitmasking
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("Transformed instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t					m
			// this instruction should be scaled by 2 in both axes
			drawInstructions[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSFORMATIONS,
															0,		0,		{ 0, 0, 0 },		{ 2, 0, 0, 2 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be stored for later drawing")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, 1) == GW::GReturn::SUCCESS);
				// verify that the canvas is clear before flushing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// export again with flush and verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(true, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x < 2 && y < 2)
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("Transparent instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-10-20
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t
			// this instruction should be drawn with alpha blending
			drawInstructions[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSPARENCY,
															0,		0,		{ 0, 0, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-10-20
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be stored for later drawing")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, 1) == GW::GReturn::SUCCESS);
				// verify that the canvas is clear before flushing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// export again with flush and verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(true, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x == 0 && y == 0)
							CHECK(resultColors[x + y * canvasWidth] == 0x01030608); // color 0x12345678 drawn over color 0x00000000 with alpha blending
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}
	}

	SECTION("DrawDeferred negative checks")
	{
		WHEN("Invalid array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawDeferred(nullptr, numDraws) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Zero count is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t
			drawInstructions[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions[3] = { tileIndices[1],	0,		0,		0,		{ 1, 2, 0 } };

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, 0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Draw instruction with unset tile index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index		flags	layer	stencil	t
			drawInstructions[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions[3] = { (std::numeric_limits<unsigned int>::max)(),		0,		0,		0,		{ 1, 2, 0 } }; // it's more compact to use an initializer list and set tile_id manually back to the default value than it is to set every other value individually

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, numDraws) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Draw instruction with invalid tile index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						tile index			flags	layer	stencil	t
			drawInstructions[0] = { tileIndices[0],		0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions[1] = { tileIndices[0],		0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions[2] = { tileIndices[1],		0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions[3] = { static_cast<unsigned int>(tileIndices[1] + 1),
														0,		0,		0,		{ 1, 2, 0 } };

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawDeferred(drawInstructions, numDraws) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_DRAW_DEFERRED

#if defined(GBLITTER_TEST_DRAW_IMMEDIATE)
TEST_CASE("GBlitter DrawImmediate checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 5;
	unsigned short								canvasHeight = 5;

	unsigned int								clearColor = 0x00000000;
	unsigned char								clearLayer = 0xff;
	unsigned char								clearStencil = 0x00;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned int								maskColor = 0x44444444;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 2;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];

	const unsigned int							numInstructions = 1861 * 2 + 1; // this is one more than double the size of a result block bin as of 2021-03-05; this should cause at least two automatic flushes during function call
	const unsigned int							numDraws_0 = 4;
	const unsigned int							numDraws_1 = 2;
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstructions_0[numInstructions];
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstructions_1[numDraws_1];

	unsigned int*								resultColors = nullptr;

	SECTION("DrawImmediate positive checks")
	{
		WHEN("Valid draw instructions and count are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			drawInstructions_0[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions_0[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions_0[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions_0[3] = { tileIndices[1],	0,		0,		0,		{ 1, 2, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, numDraws_0) == GW::GReturn::SUCCESS);
				// verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if ((x == 0 && y == 0)
							|| (x == 1 && y == 1)
							|| (x == 2 && y == 1)
							|| (x == 1 && y == 2))
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("Deferred instructions are already in blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			drawInstructions_0[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions_0[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions_0[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions_0[3] = { tileIndices[1],	0,		0,		0,		{ 1, 2, 0 } };

			drawInstructions_1[0] = { tileIndices[1],	0,		0,		0,		{ 3, 4, 0 }	};
			drawInstructions_1[1] = { tileIndices[1],	0,		0,		0,		{ 4, 3, 0 }	};

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);
			
			gr = blitter.DrawDeferred(drawInstructions_1, numDraws_1);

			THEN("Deferred instructions should be discarded and instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing (deferred instructions are not flushed yet, so nothing should be drawn to it yet)
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, numDraws_0) == GW::GReturn::SUCCESS);
				// verify that immediate instructions were drawn and the rest of the canvas is unchanged (deferred instructions should not be drawn)
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if ((x == 0 && y == 0)
							|| (x == 1 && y == 1)
							|| (x == 2 && y == 1)
							|| (x == 1 && y == 2))
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}
		
		WHEN("A result block in the blitter is saturated with instructions")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-23
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			for (unsigned int i = 0; i < numInstructions - 1; ++i)
			//											tile index		flags	layer	stencil	t
				drawInstructions_0[i]				= { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } }; // all of these duplicate instructions will be stacked in the same place
			drawInstructions_0[numInstructions - 1]	= { tileIndices[0],	0,		0,		0,		{ 1, 0, 0 } }; // this instruction should still be drawn, even though the ones before it will saturate the result block at least twice

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-23
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, numInstructions) == GW::GReturn::SUCCESS);
				// verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				// verify that all stacked instructions were drawn
				REQUIRE(resultColors[0 + 0 * canvasWidth] == sourceColorValue);
				// verify that the excess instruction was drawn
				REQUIRE(resultColors[1 + 0 * canvasWidth] == sourceColorValue);
				// verify that the rest of the canvas is unchanged
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x < 2 && y == 0)
							continue;
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}
		
		WHEN("Bitmasked instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			sourceColors[1] = maskColor; // set pixel at (1, 0) to the mask color to test masking
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			//					   source id	x  y  w  h  mask color
			tileDefinitions[0] = { sourceIndex, 0, 0, 2, 1, maskColor };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			// this instruction should be masked and only some color should be drawn
			drawInstructions_1[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_MASKING,
																0,		0,		{ 0, 0, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_1, 1) == GW::GReturn::SUCCESS);
				// verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x == 0 && y == 0) // of the 2-pixel wide tile, only the first pixel should be drawn due to bitmasking
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("Transformed instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t					m
			// this instruction should be scaled by 2 in both axes
			drawInstructions_1[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSFORMATIONS,
																0,		0,		{ 0, 0, 0 },		{ 2, 0, 0, 2 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-06-22
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_1, 1) == GW::GReturn::SUCCESS);
				// verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x < 2 && y < 2)
							CHECK(resultColors[x + y * canvasWidth] == sourceColorValue);
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}

		WHEN("Transparent instructions are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-10-20
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			// this instruction should be drawn with alpha blending
			drawInstructions_1[0] = { tileIndices[0],	GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSPARENCY,
																0,		0,		{ 0, 0, 0 } };

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-10-20
			memset(resultColors, 0x99, canvasWidth * canvasHeight * 4);

			THEN("Instructions should be drawn to the canvas")
			{
				// verify that the canvas is clear before drawing
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
						REQUIRE(resultColors[x + y * canvasWidth] == clearColor);
				// draw instructions to the canvas
				REQUIRE(blitter.DrawImmediate(drawInstructions_1, 1) == GW::GReturn::SUCCESS);
				// verify that instructions were drawn and the rest of the canvas is unchanged
				REQUIRE(blitter.ExportResult(false, canvasWidth, canvasHeight, 0, 0, resultColors, nullptr, nullptr) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < canvasHeight; ++y)
					for (unsigned int x = 0; x < canvasHeight; ++x)
					{
						if (x == 0 && y == 0)
							CHECK(resultColors[x + y * canvasWidth] == 0x01030608); // color 0x12345678 drawn over color 0x00000000 with alpha blending
						else
							CHECK(resultColors[x + y * canvasWidth] == clearColor);
					}
			}

			delete[] sourceColors;
			delete[] resultColors;
		}
	}

	SECTION("DrawImmediate negative checks")
	{
		WHEN("Invalid array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawImmediate(nullptr, numDraws_0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Zero count is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			drawInstructions_0[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions_0[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions_0[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions_0[3] = { tileIndices[1],	0,		0,		0,		{ 1, 2, 0 } };

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, 0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Draw instruction with unset tile index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			drawInstructions_0[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions_0[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions_0[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions_0[3] = { (std::numeric_limits<unsigned int>::max)(),		0,		0,		0,		{ 1, 2, 0 } }; // it's more compact to use an initializer list and set tile_id manually back to the default value than it is to set every other value individually

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, numDraws_0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Draw instruction with invalid tile index is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-03-08
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			tileDefinitions[1] = { sourceIndex, 1, 1, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			//						  tile index		flags	layer	stencil	t
			drawInstructions_0[0] = { tileIndices[0],	0,		0,		0,		{ 0, 0, 0 } };
			drawInstructions_0[1] = { tileIndices[0],	0,		0,		0,		{ 1, 1, 0 } };
			drawInstructions_0[2] = { tileIndices[1],	0,		0,		0,		{ 2, 1, 0 } };
			drawInstructions_0[3] = { static_cast<unsigned int>(tileIndices[1] + 1),
														0,		0,		0,		{ 1, 2, 0 } };

			THEN("The blitter should indicate that an argument was invalid")
			{
				REQUIRE(blitter.DrawImmediate(drawInstructions_0, numDraws_0) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_DRAW_IMMEDIATE

#if defined(GBLITTER_TEST_FLUSH)
TEST_CASE("GBlitter Flush checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 500;
	unsigned short								canvasHeight = 500;

	unsigned int								sourceColorValue = 0x12345678;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	GW::GRAPHICS::GBlitter::TileDefinition		tile;
	unsigned int								tileIndex = (std::numeric_limits<unsigned int>::max)();
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstruction;

	SECTION("Flush positive checks")
	{
		WHEN("Draw instructions are present")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-06-17
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			tile = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			gr = blitter.DefineTiles(&tile, 1, &tileIndex);
			drawInstruction = { tileIndex, 0, 255, 0, { 0, 0, 0 } };
			gr = blitter.DrawDeferred(&drawInstruction, 1);

			THEN("Draw instructions should be flushed to result")
			{
				REQUIRE(blitter.Flush() == GW::GReturn::SUCCESS);
			}

			delete[] sourceColors;
		}

		WHEN("No draw instructions are present")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-06-17
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);
			tile = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			gr = blitter.DefineTiles(&tile, 1, &tileIndex);

			THEN("Blitter should indicate that operation was redundant")
			{
				REQUIRE(blitter.Flush() == GW::GReturn::REDUNDANT);
			}

			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_FLUSH

#if defined(GBLITTER_TEST_EXPORT_RESULT)
TEST_CASE("GBlitter ExportResult checks", "[Graphics]")
{
	GW::GReturn									gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 5;
	unsigned short								canvasHeight = 5;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned char								sourceLayerValue = 0xac;
	unsigned char								sourceStencilValue = 0xbd;

	unsigned int								clearColor = 0x00000000;
	unsigned char								clearLayer = 0xff;
	unsigned char								clearStencil = 0x00;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 1;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];
	const unsigned int							numDraws = 1;
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstructions[numDraws];

	unsigned int*								resultColors = nullptr;
	unsigned char*								resultLayers = nullptr;
	unsigned char*								resultStencils = nullptr;
	unsigned short								bufferDataWidth = 0;
	unsigned short								bufferDataHeight = 0;
	unsigned short								bufferWidth = 0;
	unsigned short								bufferHeight = 0;
	unsigned short								bufferOffset = 0;
	unsigned short								bufferStride = 0;

	SECTION("ExportResult positive checks")
	{
		WHEN("Valid data is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == sourceColorValue);
						REQUIRE(resultLayers[resultIndex] == sourceLayerValue);
						REQUIRE(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Valid buffers with padding are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth + 1;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			bufferOffset = bufferDataWidth - bufferWidth;
			bufferStride = bufferDataWidth;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferStride;
						// make sure data was NOT written to padding but WAS written to everywhere else
						unsigned int matchColor = x >= bufferOffset ? sourceColorValue : 0x00000000;
						unsigned char matchLayer = x >= bufferOffset ? sourceLayerValue : 0x00;
						unsigned char matchStencil = x >= bufferOffset ? sourceStencilValue : 0x00;
						REQUIRE(resultColors[resultIndex] == matchColor);
						REQUIRE(resultLayers[resultIndex] == matchLayer);
						REQUIRE(resultStencils[resultIndex] == matchStencil);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Valid buffer larger than result is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth + 1;
			bufferDataHeight = canvasHeight + 1;
			bufferWidth = bufferDataWidth;
			bufferHeight = bufferDataHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Result color data should be transferred to portion of output buffer")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						// make sure data was NOT written to excess but WAS written to everywhere else
						unsigned int matchColor = x < canvasWidth && y < canvasHeight ? sourceColorValue : 0x00000000;
						unsigned char matchLayer = x < canvasWidth && y < canvasHeight ? sourceLayerValue : 0x00;
						unsigned char matchStencil = x < canvasWidth && y < canvasHeight ? sourceStencilValue : 0x00;
						REQUIRE(resultColors[resultIndex] == matchColor);
						REQUIRE(resultLayers[resultIndex] == matchLayer);
						REQUIRE(resultStencils[resultIndex] == matchStencil);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Valid buffer smaller than result is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth - 1;
			bufferDataHeight = canvasHeight - 1;
			bufferWidth = bufferDataWidth;
			bufferHeight = bufferDataHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Portion of result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == sourceColorValue);
						REQUIRE(resultLayers[resultIndex] == sourceLayerValue);
						REQUIRE(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}
		
		WHEN("Instructions are not flushed during export")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Result color data should be transferred to output buffer, but without new instructions drawn")
			{
				REQUIRE(blitter.ExportResult(false, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == clearColor);
						REQUIRE(resultLayers[resultIndex] == clearLayer);
						REQUIRE(resultStencils[resultIndex] == clearStencil);
					}

				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == sourceColorValue);
						REQUIRE(resultLayers[resultIndex] == sourceLayerValue);
						REQUIRE(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}
	}

	SECTION("ExportResult negative checks")
	{
		WHEN("No buffers are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = nullptr;
			resultLayers = nullptr;
			resultStencils = nullptr;

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
			}

			delete[] sourceColors;
		}

		WHEN("Buffer with zero width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = 0;
			bufferHeight = canvasHeight;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == 0x00000000);
						REQUIRE(resultLayers[resultIndex] == 0x00);
						REQUIRE(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Buffer with zero height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = 0;
			bufferOffset = 0;
			bufferStride = 0;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == 0x00000000);
						REQUIRE(resultLayers[resultIndex] == 0x00);
						REQUIRE(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Nonzero stride less than width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-03
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, 1, 1 };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferDataWidth = canvasWidth;
			bufferDataHeight = canvasHeight;
			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			bufferOffset = 0;
			bufferStride = bufferDataWidth - 1;

			resultColors = new unsigned int[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultLayers = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			resultStencils = new unsigned char[bufferDataWidth * bufferDataHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-03
			memset(resultColors, 0x00, bufferDataWidth * bufferDataHeight * 4);
			memset(resultLayers, 0x00, bufferDataWidth * bufferDataHeight);
			memset(resultStencils, 0x00, bufferDataWidth * bufferDataHeight);

			THEN("Blitter should indicate that an argument is invalid")
			{
				REQUIRE(blitter.ExportResult(true, bufferWidth, bufferHeight, bufferOffset, bufferStride, resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferDataHeight; ++y)
					for (unsigned int x = 0; x < bufferDataWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferDataWidth;
						REQUIRE(resultColors[resultIndex] == 0x00000000);
						REQUIRE(resultLayers[resultIndex] == 0x00);
						REQUIRE(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_EXPORT_RESULT

#if defined(GBLITTER_TEST_EXPORT_RESULT_COMPLEX)
TEST_CASE("GBlitter ExportResultComplex checks", "[Graphics]")
{
	struct Pixel
	{
		unsigned int	color;
		unsigned char	layer;
		unsigned char	stencil;
		unsigned char	pad[2];
	};

	GW::GReturn gr;
	GW::GRAPHICS::GBlitter						blitter;
	unsigned short								canvasWidth = 5;
	unsigned short								canvasHeight = 5;

	unsigned int								sourceColorValue = 0x12345678;
	unsigned char								sourceLayerValue = 0xac;
	unsigned char								sourceStencilValue = 0xbd;

	unsigned int								clearColor = 0x00000000;
	unsigned char								clearLayer = 0xff;
	unsigned char								clearStencil = 0x00;

	unsigned int*								sourceColors = nullptr;
	unsigned short								sourceIndex = (std::numeric_limits<unsigned short>::max)();
	const unsigned int							numTiles = 1;
	GW::GRAPHICS::GBlitter::TileDefinition		tileDefinitions[numTiles];
	unsigned int								tileIndices[numTiles];
	const unsigned int							numDraws = 1;
	GW::GRAPHICS::GBlitter::DrawInstruction		drawInstructions[numDraws];

	unsigned char*								resultByteData = nullptr;
	Pixel*										resultPixelData = nullptr;
	unsigned int*								resultColors = nullptr;
	unsigned char*								resultLayers = nullptr;
	unsigned char*								resultStencils = nullptr;
	unsigned short								bufferWidth = 0;
	unsigned short								bufferHeight = 0;
	unsigned short								colorStride = 0;
	unsigned short								colorRowStride = 0;
	unsigned short								layerStride = 0;
	unsigned short								layerRowStride = 0;
	unsigned short								stencilStride = 0;
	unsigned short								stencilRowStride = 0;

	SECTION("ExportResultComplex positive checks")
	{
		WHEN("Valid data arranged in separate arrays is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			resultLayers = new unsigned char[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			resultStencils = new unsigned char[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultColors, 0x00, bufferWidth * bufferHeight * 4);
			memset(resultLayers, 0x00, bufferWidth * bufferHeight);
			memset(resultStencils, 0x00, bufferWidth * bufferHeight);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == sourceColorValue);
						CHECK(resultLayers[resultIndex] == sourceLayerValue);
						CHECK(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}

		WHEN("Valid data arranged in one large array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == sourceColorValue);
						CHECK(resultLayers[resultIndex] == sourceLayerValue);
						CHECK(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Valid data arranged in a mixed array is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;
			resultPixelData = new Pixel[canvasWidth * canvasHeight]; // this will leak if test fails; solution unknown as of 2020-05-04
			memset(resultPixelData, 0x00, canvasWidth * canvasHeight * sizeof(Pixel));
			resultColors = reinterpret_cast<unsigned int*>(resultPixelData);
			resultLayers = reinterpret_cast<unsigned char*>(resultPixelData) + 4;
			resultStencils = reinterpret_cast<unsigned char*>(resultPixelData) + 5;

			colorStride = sizeof(Pixel);
			colorRowStride = colorStride * bufferWidth;
			layerStride = sizeof(Pixel);
			layerRowStride = layerStride * bufferWidth;
			stencilStride = sizeof(Pixel);
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultPixelData[resultIndex].color == sourceColorValue);
						CHECK(resultPixelData[resultIndex].layer == sourceLayerValue);
						CHECK(resultPixelData[resultIndex].stencil == sourceStencilValue);
					}
			}

			delete[] resultPixelData;
			delete[] sourceColors;
		}

		WHEN("Valid buffers with padding are passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth + 1;
			bufferHeight = canvasHeight + 1;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					canvasWidth, canvasHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						// make sure data was NOT written to padding but WAS written to everywhere else
						unsigned int matchColor = (x < canvasWidth && y < canvasHeight) ? sourceColorValue : 0x00000000;
						unsigned char matchLayer = (x < canvasWidth && y < canvasHeight) ? sourceLayerValue : 0x00;
						unsigned char matchStencil = (x < canvasWidth && y < canvasHeight) ? sourceStencilValue : 0x00;
						CHECK(resultColors[resultIndex] == matchColor);
						CHECK(resultLayers[resultIndex] == matchLayer);
						CHECK(resultStencils[resultIndex] == matchStencil);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Valid buffer larger than result is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth + 1;
			bufferHeight = canvasHeight + 1;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						// make sure data was NOT written to excess but WAS written to everywhere else
						unsigned int matchColor = (x < canvasWidth && y < canvasHeight) ? sourceColorValue : 0x00000000;
						unsigned char matchLayer = (x < canvasWidth && y < canvasHeight) ? sourceLayerValue : 0x00;
						unsigned char matchStencil = (x < canvasWidth && y < canvasHeight) ? sourceStencilValue : 0x00;
						CHECK(resultColors[resultIndex] == matchColor);
						CHECK(resultLayers[resultIndex] == matchLayer);
						CHECK(resultStencils[resultIndex] == matchStencil);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Valid buffer smaller than result is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth - 1;
			bufferHeight = canvasHeight - 1;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[bufferWidth * bufferHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == sourceColorValue);
						CHECK(resultLayers[resultIndex] == sourceLayerValue);
						CHECK(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}
		
		WHEN("Instructions are not flushed during export")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultColors = new unsigned int[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			resultLayers = new unsigned char[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			resultStencils = new unsigned char[canvasWidth * canvasHeight]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultColors, 0x00, bufferWidth * bufferHeight * 4);
			memset(resultLayers, 0x00, bufferWidth * bufferHeight);
			memset(resultStencils, 0x00, bufferWidth * bufferHeight);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Result color data should be transferred to output buffer, but without new instructions drawn")
			{
				REQUIRE(blitter.ExportResultComplex(
					false,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == clearColor);
						CHECK(resultLayers[resultIndex] == clearLayer);
						CHECK(resultStencils[resultIndex] == clearStencil);
					}

				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::SUCCESS);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == sourceColorValue);
						CHECK(resultLayers[resultIndex] == sourceLayerValue);
						CHECK(resultStencils[resultIndex] == sourceStencilValue);
					}
			}

			delete[] resultColors;
			delete[] resultLayers;
			delete[] resultStencils;
			delete[] sourceColors;
		}
	}

	SECTION("ExportResultComplex negative checks")
	{
		WHEN("No buffers are passed to blitter")
		{
		gr = blitter.Create(canvasWidth, canvasHeight);
		gr = blitter.Clear(clearColor, clearLayer, clearStencil);

		sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
		for (int i = 0; i < canvasWidth * canvasHeight; ++i)
			sourceColors[i] = sourceColorValue;
		gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

		tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
		for (unsigned int i = 0; i < numTiles; ++i)
			tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
		gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

		drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
		gr = blitter.DrawDeferred(drawInstructions, numDraws);

		bufferWidth = canvasWidth;
		bufferHeight = canvasHeight;

		resultColors = nullptr;
		resultLayers = nullptr;
		resultStencils = nullptr;

		colorStride = 4;
		colorRowStride = colorStride * bufferWidth;
		layerStride = 1;
		layerRowStride = layerStride * bufferWidth;
		stencilStride = 1;
		stencilRowStride = stencilStride * bufferWidth;

		THEN("Result color data should be transferred to output buffer")
		{
			REQUIRE(blitter.ExportResultComplex(
				true,
				colorStride, colorRowStride,
				layerStride, layerRowStride,
				stencilStride, stencilRowStride,
				bufferWidth, bufferHeight,
				resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
		}

		delete[] sourceColors;
		}

		WHEN("Buffer with zero width is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					0, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Buffer with zero height is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, 0,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero color byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					0, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero color row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, 0,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero layer byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					0, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero layer row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, 0,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero stencil byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					0, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Zero stencil row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, 0,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Color row stride that is less than width * color byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth - 1;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Layer row stride that is less than width * layer byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth - 1;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Stencil row stride that is less than width * stencil byte stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth - 1;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Color byte stride that is greater than color row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorRowStride + 1, colorRowStride,
					layerStride, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Layer byte stride that is greater than layer row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerRowStride + 1, layerRowStride,
					stencilStride, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}

		WHEN("Stencil byte stride that is greater than stencil row stride is passed to blitter")
		{
			gr = blitter.Create(canvasWidth, canvasHeight);
			gr = blitter.Clear(clearColor, clearLayer, clearStencil);

			sourceColors = new unsigned int[canvasWidth * canvasHeight]; // this will leak if test fails; fix unknown as of 2020-05-04
			for (int i = 0; i < canvasWidth * canvasHeight; ++i)
				sourceColors[i] = sourceColorValue;
			gr = blitter.ImportSource(sourceColors, nullptr, nullptr, canvasWidth, canvasHeight, 0, 0, sourceIndex);

			tileDefinitions[0] = { sourceIndex, 0, 0, canvasWidth, canvasHeight };
			for (unsigned int i = 0; i < numTiles; ++i)
				tileIndices[i] = (std::numeric_limits<unsigned int>::max)();
			gr = blitter.DefineTiles(tileDefinitions, numTiles, tileIndices);

			drawInstructions[0] = { tileIndices[0], 0, sourceLayerValue, sourceStencilValue, {} };
			gr = blitter.DrawDeferred(drawInstructions, numDraws);

			bufferWidth = canvasWidth;
			bufferHeight = canvasHeight;

			resultByteData = new unsigned char[bufferWidth * bufferHeight * 6]; // will leak if test fails, as a failed REQUIRE will exit immediately; fix unknown as of 2020-05-04
			memset(resultByteData, 0x00, bufferWidth * bufferHeight * 6);
			resultColors = reinterpret_cast<unsigned int*>(&resultByteData[0]);
			resultLayers = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 4]);
			resultStencils = reinterpret_cast<unsigned char*>(&resultByteData[canvasWidth * canvasHeight * 5]);

			colorStride = 4;
			colorRowStride = colorStride * bufferWidth;
			layerStride = 1;
			layerRowStride = layerStride * bufferWidth;
			stencilStride = 1;
			stencilRowStride = stencilStride * bufferWidth;

			THEN("Blitter should indicate that an invalid argument was passed")
			{
				REQUIRE(blitter.ExportResultComplex(
					true,
					colorStride, colorRowStride,
					layerStride, layerRowStride,
					stencilRowStride + 1, stencilRowStride,
					bufferWidth, bufferHeight,
					resultColors, resultLayers, resultStencils) == GW::GReturn::INVALID_ARGUMENT);
				for (unsigned int y = 0; y < bufferHeight; ++y)
					for (unsigned int x = 0; x < bufferWidth; ++x)
					{
						unsigned int resultIndex = x + y * bufferWidth;
						CHECK(resultColors[resultIndex] == 0x00000000);
						CHECK(resultLayers[resultIndex] == 0x00);
						CHECK(resultStencils[resultIndex] == 0x00);
					}
			}

			delete[] resultByteData;
			delete[] sourceColors;
		}
	}
}
#endif // GBLITTER_TEST_EXPORT_RESULT_COMPLEX


#if defined(GBLITTER_TEST_VISUAL) && !defined(DISABLE_USER_INPUT_TESTS) && defined(GATEWARE_ENABLE_INPUT)
#pragma pack(push, 1)
// represents a 2x2 matrix in row-major (ie, [a, b] represents row 0, [c, d] represents row 1)
struct Matrix2x2
{
	union
	{
		struct
		{
			float a;
			float b;
			float c;
			float d;
		};
		float m[2][2]; // values in matrix form
		float data[4]; // values in list form
	};
};
#pragma pack(pop)

static inline Matrix2x2 MultiplyMatrixAndScalar(Matrix2x2 _m, float _s)
{
	return
	{
		_m.a * _s,		_m.b * _s,
		_m.c * _s,		_m.d * _s
	};
}
static inline Matrix2x2 MultiplyMatrixAndMatrix(Matrix2x2 _a, Matrix2x2 _b)
{
	return
	{
		_a.a * _b.a + _a.b * _b.c,		_a.a * _b.b + _a.b * _b.d,
		_a.c * _b.a + _a.d * _b.c,		_a.c * _b.b + _a.d * _b.d,
	};
}

static inline float MatrixDeterminant(Matrix2x2 _m)
{
	return (_m.a * _m.d) - (_m.c * _m.b);
}
static inline Matrix2x2 MatrixInverse(Matrix2x2 _m)
{
	float det = MatrixDeterminant(_m);
	if (det == 0.0f)
	{
		_m.a += 0.000001f;
		det = MatrixDeterminant(_m);
	}
	det = 1.0f / det;
	return
	{
		_m.d * det,		-_m.b * det,
		-_m.c * det,	_m.a * det
	};
}

static inline Matrix2x2 MatrixIdentity()
{
	return
	{
		1.0f,	0.0f,
		0.0f,	1.0f
	};
}
static inline Matrix2x2 MatrixScaling(float _x, float _y)
{
	return
	{
		_x,		0.0f,
		0.0f,	_y
	};
}
static inline Matrix2x2 MatrixShearing(float _x, float _y)
{
	return
	{
		1.0f,	_x,
		_y,		1.0f
	};
}
static inline Matrix2x2 MatrixRotation(float _r)
{
	return
	{
		cosf(_r),	sinf(_r),
		-sinf(_r),	cosf(_r)
	};
}
static inline Matrix2x2 MatrixMirrorX()
{
	return
	{
		-1.0f,	0.0f,
		0.0f,	1.0f
	};
}
static inline Matrix2x2 MatrixMirrorY()
{
	return
	{
		1.0f,	0.0f,
		0.0f,	-1.0f
	};
}

static inline void ToggleFlagBitsWithMask(unsigned short& _flags, unsigned short _mask)
{
	_flags = ((_flags & ~_mask) | ((_flags & _mask) ^ _mask));
}

// shorthand version of GBlitter::DrawOptions to save space
enum DrawFlag
{
	ROT = GW::GRAPHICS::GBlitter::DrawOptions::ROTATE,
	VRT = GW::GRAPHICS::GBlitter::DrawOptions::MIRROR_VERTICAL,
	HRZ = GW::GRAPHICS::GBlitter::DrawOptions::MIRROR_HORIZONTAL,
	ISC = GW::GRAPHICS::GBlitter::DrawOptions::IGNORE_SOURCE_COLORS,
	USL = GW::GRAPHICS::GBlitter::DrawOptions::USE_SOURCE_LAYERS,
	USS = GW::GRAPHICS::GBlitter::DrawOptions::USE_SOURCE_STENCILS,
	MSK = GW::GRAPHICS::GBlitter::DrawOptions::USE_MASKING,
	TSF = GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSFORMATIONS,
	TSP = GW::GRAPHICS::GBlitter::DrawOptions::USE_TRANSPARENCY,
};

TEST_CASE("GBlitter visual test", "[Graphics]")
{
	GW::GReturn gr;
	class WINDOW
	{
	public:
		GW::SYSTEM::GWindow				gWindow;
		GW::GRAPHICS::GRasterSurface	gRasterSurface;
		unsigned int					alignFlags = GW::GRAPHICS::ALIGN_X_LEFT | GW::GRAPHICS::ALIGN_Y_TOP;
		unsigned int					updateFlags = alignFlags;
		const unsigned int				initialX = 100;
		const unsigned int				initialY = 100;
		const unsigned int				initialW = 800;
		const unsigned int				initialH = 800;
		const unsigned int				bgColor = 0xff1f1f1f;

		void Init()
		{
			REQUIRE(+gWindow.Create(initialX, initialY, initialW, initialH, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED));
			REQUIRE(+gRasterSurface.Create(gWindow));
		}
		void Cleanup()
		{
			gWindow = nullptr;
		}
	} window;
	class CONTROL
	{
	public:
		struct Input
		{
			unsigned int			keycode;
			unsigned int			cooldownMs;
			unsigned long long		lastInputFrame;

			Input(unsigned int _keycode, unsigned int _cooldownMs = 0)
			{
				keycode = _keycode;
				cooldownMs = _cooldownMs;
				lastInputFrame = 0;
			}
		};

		enum INPUT_NAME
		{
			EXIT = 0,

			SCALE_1,
			SCALE_2,
			SCALE_4,
			SCALE_8,
			SCALE_16,

			PAN_LEFT,
			PAN_RIGHT,
			PAN_UP,
			PAN_DOWN,

			DRAW_MODE_DEFERRED,
			DRAW_MODE_IMMEDIATE,

			TOGGLE_LAYER_POST_PROCESSING,
			TOGGLE_STENCIL_POST_PROCESSING,

			PIPELINE_STAGE_PROCEDURAL_SOURCES,
			PIPELINE_STAGE_RESULT,

			PREV_PROCEDURAL_SOURCE,
			NEXT_PROCEDURAL_SOURCE,

			PREV_TEST_SET,
			NEXT_TEST_SET,

			PREV_TEST,
			NEXT_TEST,

			FREEFORM_TILE_MINUS,
			FREEFORM_TILE_PLUS,

			FREEFORM_CONTROL_POSITION,
			FREEFORM_CONTROL_PIVOT,
			FREEFORM_CONTROL_SCALE,
			FREEFORM_CONTROL_SHEAR,
			FREEFORM_CONTROL_ROTATION,

			FREEFORM_X_MINUS,
			FREEFORM_X_PLUS,
			FREEFORM_Y_MINUS,
			FREEFORM_Y_PLUS,
			FREEFORM_Z_MINUS,
			FREEFORM_Z_PLUS,

			FREEFORM_ADJUST_SPEED_GROSS_MINUS,
			FREEFORM_ADJUST_SPEED_GROSS_PLUS,
			FREEFORM_ADJUST_SPEED_FINE_MINUS,
			FREEFORM_ADJUST_SPEED_FINE_PLUS,

			FREEFORM_RESET_AXES,
			FREEFORM_RESET_SPEED,

			FREEFORM_TOGGLE_FLAG_ROTATE,
			FREEFORM_TOGGLE_FLAG_MIRROR_V,
			FREEFORM_TOGGLE_FLAG_MIRROR_H,
			FREEFORM_TOGGLE_FLAG_IGNORE_COLORS,
			FREEFORM_TOGGLE_FLAG_USE_LAYERS,
			FREEFORM_TOGGLE_FLAG_USE_STENCILS,
			FREEFORM_TOGGLE_FLAG_BITMASKING,
			FREEFORM_TOGGLE_FLAG_TRANSFORMATION,
			FREEFORM_TOGGLE_FLAG_TRANSPARENCY,
			FREEFORM_TOGGLE_FLAG_9,
			FREEFORM_TOGGLE_FLAG_10,
			FREEFORM_TOGGLE_FLAG_11,
			FREEFORM_TOGGLE_FLAG_12,
			FREEFORM_TOGGLE_FLAG_13,
			FREEFORM_TOGGLE_FLAG_14,
			FREEFORM_TOGGLE_FLAG_15,

			COUNT
		};

		const unsigned int defaultInputCooldownMs = 100;
		Input inputs[INPUT_NAME::COUNT] =
		{
			// exit
			{ G_KEY_ESCAPE },

			// scale
			{ G_KEY_1 },
			{ G_KEY_2 },
			{ G_KEY_3 },
			{ G_KEY_4 },
			{ G_KEY_5 },

			// pan
			{ G_KEY_LEFT },
			{ G_KEY_RIGHT },
			{ G_KEY_UP },
			{ G_KEY_DOWN },

			// draw mode
			{ G_KEY_DELETE },
			{ G_KEY_BACKSLASH },

			// post processing toggles
			{ G_KEY_TILDE,	200 },
			{ G_KEY_TAB,	200 },

			// pipeline stage to display
			{ G_KEY_7 },
			{ G_KEY_8 },

			// procedural source to display
			{ G_KEY_BRACKET_OPEN,	defaultInputCooldownMs },
			{ G_KEY_BRACKET_CLOSE,	defaultInputCooldownMs },

			// instruction set to draw
			{ G_KEY_9, defaultInputCooldownMs },
			{ G_KEY_0, defaultInputCooldownMs },

			// number of draws
			{ G_KEY_MINUS,	defaultInputCooldownMs },
			{ G_KEY_EQUALS,	defaultInputCooldownMs },

			// freeform test - tile index
			{ G_KEY_Q, defaultInputCooldownMs },
			{ G_KEY_E, defaultInputCooldownMs },

			// freeform test - select active control
			{ G_KEY_T },
			{ G_KEY_Y },
			{ G_KEY_U },
			{ G_KEY_I },
			{ G_KEY_O },

			// freeform test - axes
			{ G_KEY_A },
			{ G_KEY_D },
			{ G_KEY_W },
			{ G_KEY_S },
			{ G_KEY_F },
			{ G_KEY_R },

			// freeform test - adjust speed
			{ G_KEY_END },
			{ G_KEY_HOME },
			{ G_KEY_PAGEDOWN },
			{ G_KEY_PAGEUP },

			// freeform test - reset axes/speed
			{ G_KEY_ENTER },
			{ G_KEY_SPACE },

			// freeform test - flag toggles
			{ G_KEY_FORWARDSLASH,	200 },
			{ G_KEY_PERIOD,			200 },
			{ G_KEY_COMMA,			200 },
			{ G_KEY_M,				200 },
			{ G_KEY_N,				200 },
			{ G_KEY_B,				200 },
			{ G_KEY_V,				200 },
			{ G_KEY_C,				200 },
			{ G_KEY_X,				200 },
			{ G_KEY_Z,				200 },
			{ G_KEY_QUOTE,			200 },
			{ G_KEY_SEMICOLON,		200 },
			{ G_KEY_L,				200 },
			{ G_KEY_K,				200 },
			{ G_KEY_J,				200 },
			{ G_KEY_H,				200 },
		};

		GW::INPUT::GInput						gInput;
		std::bitset<INPUT_NAME::COUNT>			inputStates;

		void Init(GW::SYSTEM::GWindow _gWindow)
		{
			REQUIRE(+gInput.Create(_gWindow));
		}
		void ReadInputStates()
		{
			float keyState = 0.0f;
			for (int i = 0; i < INPUT_NAME::COUNT; ++i)
			{
				gInput.GetState(inputs[i].keycode, keyState);
				inputStates.set(i, (bool)keyState);
			}
		}
		void ResetInputStates()
		{
			for (unsigned int i = 0; i < INPUT_NAME::COUNT; ++i)
				inputStates.set(i, false);
		}
	} control;
	class BLITTER
	{
	public:
		struct SOURCE_NAME
		{
			enum
			{
				PROC_HEATMAP = 0,
				PROC_HEATMAP_BLOCKS,
				PROC_SOURCE_GRID_C,
				PROC_SOURCE_GRID_L,
				PROC_SOURCE_GRID_S,
				PROC_SOURCE_GRID_CL,
				PROC_SOURCE_GRID_CS,
				PROC_SOURCE_GRID_LS,
				PROC_SOURCE_GRID_CLS,
				PROC_RESULT_GRID,

				PROCEDURAL_COUNT,

				TGA_L_8 = PROCEDURAL_COUNT,
				TGA_AL_16,
				TGA_RGB_24,
				TGA_ARGB_32,
				TGA_RGB_LUT_8,
				TGA_L_8_RLE,
				TGA_AL_16_RLE,
				TGA_RGB_24_RLE,
				TGA_ARGB_32_RLE,
				TGA_RGB_LUT_8_RLE,

				BMP_RGB_24,
				BMP_RGBA_32,

				AXIS_BLOCKS,
				BOUNDS_BLOCKS,
				GROUND_TILES,
				TREES,
				CHARACTERS,
				FLAMES,

				COUNT,
				LOADED_COUNT = COUNT - PROCEDURAL_COUNT,
				FIRST_LOADED_SOURCE = PROCEDURAL_COUNT,
			};
		};
		struct TILE_NAME
		{
			enum
			{
				PROC_HEATMAP = 0,
				PROC_HEATMAP_BLOCKS,
				PROC_SOURCE_GRID_C,
				PROC_SOURCE_GRID_L,
				PROC_SOURCE_GRID_S,
				PROC_SOURCE_GRID_CL,
				PROC_SOURCE_GRID_CS,
				PROC_SOURCE_GRID_LS,
				PROC_SOURCE_GRID_CLS,
				PROC_RESULT_GRID,

				TGA_L_8,
				TGA_AL_16,
				TGA_RGB_24,
				TGA_ARGB_32,
				TGA_RGB_LUT_8,
				TGA_L_8_RLE,
				TGA_AL_16_RLE,
				TGA_RGB_24_RLE,
				TGA_ARGB_32_RLE,
				TGA_RGB_LUT_8_RLE,
				
				BMP_RGB_24,
				BMP_RGBA_32,

				AXIS_BLOCKS,
				BOUNDS_BLOCKS,
				GROUND_TILES,
				TREES,
				CHARACTERS,
				FLAMES,

				AXIS_BLOCK_1X1,
				AXIS_BLOCK_2X2,
				AXIS_BLOCK_3X3,
				AXIS_BLOCK_4X4,
				AXIS_BLOCK_CENTER,
				AXIS_BLOCK_RED_MASKED,

				BOUNDS_BLOCK_1X1,
				BOUNDS_BLOCK_2X2,
				BOUNDS_BLOCK_3X3,
				BOUNDS_BLOCK_4X4,
				BOUNDS_BLOCK_5X5,
				BOUNDS_BLOCK_6X6,
				BOUNDS_BLOCK_7X7,
				BOUNDS_BLOCK_8X8,
				BOUNDS_BLOCK_12X12,
				BOUNDS_BLOCK_16X16,
				BOUNDS_BLOCK_24X24,
				BOUNDS_BLOCK_32X32,
				BOUNDS_BLOCK_48X48,
				BOUNDS_BLOCK_64X64,
				BOUNDS_BLOCK_96X96,
				BOUNDS_BLOCK_128X128,
				BOUNDS_BLOCK_192X192,
				BOUNDS_BLOCK_3X5,
				BOUNDS_BLOCK_5X3,
				BOUNDS_BLOCK_4X6,
				BOUNDS_BLOCK_6X4,
				BOUNDS_BLOCK_3X15,
				BOUNDS_BLOCK_15X3,
				BOUNDS_BLOCK_4X16,
				BOUNDS_BLOCK_16X4,
				BOUNDS_BLOCK_MULTI_EVEN,
				BOUNDS_BLOCK_MULTI_ODD,

				GROUND_TILE_DIRT,
				GROUND_TILE_GRASS,
				GROUND_TILE_SHRUB,

				TREE_SMALL,
				TREE_LARGE,

				CHARACTER_MARIO,
				CHARACTER_SONIC,
				CHARACTER_LINK,

				FLAME_SINGLE,
				FLAME_MULTI,

				MULTI_BLOCK_SPLIT_TOP_LEFT,
				MULTI_BLOCK_SPLIT_TOP_RIGHT,
				MULTI_BLOCK_SPLIT_BOTTOM_RIGHT,
				MULTI_BLOCK_SPLIT_BOTTOM_LEFT,

				COUNT,
			};
		};
		enum class TEST_SET : int
		{
			PRESET_TESTS = 0,
			FREEFORM_TEST,
			FIRST_SET = PRESET_TESTS,
			LAST_SET = FREEFORM_TEST,
		};

		// contains color/layer/stencil filepaths (both standard and compressed) for a source, if they exist
		struct SourceFilenameSet
		{
			const char* c;		// color
			const char* c_cmp;	// color (compressed)
			const char* l;		// layer
			const char* l_cmp;	// layer (compressed)
			const char* s;		// stencil
			const char* s_cmp;	// stencil (compressed)
		};
		SourceFilenameSet SourceFilenames[SOURCE_NAME::COUNT] =
		{
			// procedural sources (only included to make SOURCE_NAME enum able to index into list)
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },
			{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr },

			// TGA L_8
			{
				"tga_test_files/64_64_L_8.tga", nullptr,
				nullptr, nullptr,
				"tga_test_files/64_64_L_8.tga", nullptr,
			},
			// TGA AL_16
			{
				"tga_test_files/64_64_AL_16.tga", nullptr,
				nullptr, nullptr,
				"tga_test_files/64_64_AL_16.tga", nullptr,
			},
			// TGA RGB_24
			{
				"tga_test_files/64_64_RGB_24.tga", nullptr,
				nullptr, nullptr,
				"tga_test_files/64_64_RGB_24.tga", nullptr,
			},
			// TGA ARGB_32
			{
				"tga_test_files/64_64_ARGB_32.tga", nullptr,
				nullptr, nullptr,
				"tga_test_files/64_64_ARGB_32.tga", nullptr,
			},
			// TGA RGB_LUT_8
			{
				"tga_test_files/64_64_RGB_LUT_8.tga", nullptr,
				nullptr, nullptr,
				"tga_test_files/64_64_RGB_LUT_8.tga", nullptr,
			},
			// TGA L_8_RLE
			{
				nullptr, "tga_test_files/64_64_L_8_RLE.tga",
				nullptr, nullptr,
				nullptr, "tga_test_files/64_64_L_8_RLE.tga",
			},
			// TGA AL_16_RLE
			{
				nullptr, "tga_test_files/64_64_AL_16_RLE.tga",
				nullptr, nullptr,
				nullptr, "tga_test_files/64_64_AL_16_RLE.tga",
			},
			// TGA RGB_24_RLE
			{
				nullptr, "tga_test_files/64_64_RGB_24_RLE.tga",
				nullptr, nullptr,
				nullptr, "tga_test_files/64_64_RGB_24_RLE.tga",
			},
			// TGA ARGB_32_RLE
			{
				nullptr, "tga_test_files/64_64_ARGB_32_RLE.tga",
				nullptr, nullptr,
				nullptr, "tga_test_files/64_64_ARGB_32_RLE.tga",
			},
			// TGA RGB_LUT_8_RLE
			{
				nullptr, "tga_test_files/64_64_RGB_LUT_8_RLE.tga",
				nullptr, nullptr,
				nullptr, "tga_test_files/64_64_RGB_LUT_8_RLE.tga",
			},

			// BMP RGB 
			{
				"bmp_test_files/64_64_rgb24.bmp", nullptr,
				nullptr, nullptr, 
				nullptr, nullptr,
			},
			// BMP RGBA
			{
				"bmp_test_files/64_64_rgba32.bmp", nullptr,
				nullptr, nullptr,
				nullptr, nullptr,
			},	


			
			// visual information blocks
			// axis blocks
			{
				nullptr, "visual_test_sources/axis_blocks_color_ARGB_32_RLE.tga",
				nullptr, nullptr,
				nullptr, nullptr,
			},
			// bounds blocks
			{
				"visual_test_sources/bounds_blocks_color_RGB_LUT_8.tga", "visual_test_sources/bounds_blocks_color_RGB_LUT_8_RLE.tga",
				nullptr, nullptr,
				nullptr, nullptr,
			},

			// sprites
			// ground tiles
			{
				"visual_test_sources/ground_tiles_color_ARGB_32.tga", "visual_test_sources/ground_tiles_color_ARGB_32_RLE.tga",
				nullptr, nullptr,
				nullptr, nullptr,
			},
			// trees
			{
				"visual_test_sources/trees_color_ARGB_32.tga", "visual_test_sources/trees_color_ARGB_32_RLE.tga",
				nullptr, nullptr,
				nullptr, nullptr,
			},
			// characters
			{
				"visual_test_sources/characters_color_ARGB_32.tga", "visual_test_sources/characters_color_ARGB_32_RLE.tga",
				"visual_test_sources/characters_layer_L_8.tga", "visual_test_sources/characters_layer_L_8_RLE.tga",
				"visual_test_sources/characters_stencil_L_8.tga", "visual_test_sources/characters_stencil_L_8_RLE.tga",
			},
			// flames
			{
				"visual_test_sources/flames_color_ARGB_32.tga", "visual_test_sources/flames_color_ARGB_32_RLE.tga",
				"visual_test_sources/flames_layer_L_8.tga", "visual_test_sources/flames_layer_L_8_RLE.tga",
				"visual_test_sources/flames_stencil_L_8.tga", "visual_test_sources/flames_stencil_L_8_RLE.tga",
			},
		};

		// container for procedurally generated source data
		struct ProceduralSource
		{
			unsigned short		w;
			unsigned short		h;
			unsigned int*		colors;
		};

		GW::GRAPHICS::GBlitter									gBlitter;
		const unsigned short									w = 1000; // in pixels
		const unsigned short									h = 1000; // in pixels
		const unsigned int										size = w * h; // in pixels
		unsigned int											clearColor = 0xff7f7f7f;
		unsigned char											clearLayer = 255;
		unsigned char											clearStencil = 0;
		unsigned int											procSrc_colorBG = 0xff2f2f2f;
		unsigned int											procSrc_colorGridLineDefault = 0xff9f9f9f;
		unsigned int											procSrc_colorGridLineC = 0xff9f0000;
		unsigned int											procSrc_colorGridLineL = 0xff009f00;
		unsigned int											procSrc_colorGridLineS = 0xff00009f;

		ProceduralSource										proceduralSources[SOURCE_NAME::PROCEDURAL_COUNT] = {};
		unsigned short											sourceIndices[SOURCE_NAME::COUNT] = {};
		GW::GRAPHICS::GBlitter::TileDefinition					tileDefinitions[TILE_NAME::COUNT] = {};
		unsigned int											tileIndices[TILE_NAME::COUNT] = {};

		GW::GRAPHICS::GBlitter::DrawInstruction*				draws = nullptr;
		unsigned int											numDraws = 0;
		unsigned int											drawMode = CONTROL::INPUT_NAME::DRAW_MODE_DEFERRED;
		TEST_SET												testSetToDraw = TEST_SET::FIRST_SET;

		struct TEST
		{
			std::vector<GW::GRAPHICS::GBlitter::DrawInstruction>	draws;
			const char*												name;
			
			TEST(std::vector<GW::GRAPHICS::GBlitter::DrawInstruction> _draws, const char* _name = "")
			{
				draws = _draws;
				name = _name;
			}
		};

		class PRESET_TESTS
		{
		public:
			std::vector<TEST>			tests;
			unsigned int				testToDraw = 0;

			// adds a test to the test set with the passed-in list of draw instructios and name, then clears the instruction list
			void AddTest(std::vector<GW::GRAPHICS::GBlitter::DrawInstruction>& _draws, const char* _name)
			{
				tests.push_back({ _draws, _name });
				_draws.clear();
			}
		} presetTests;
		class FREEFORM_TEST
		{
		public:
			enum class ACTIVE_CONTROL : int
			{
				POSITION,
				PIVOT,
				SCALE,
				SHEAR,
				ROTATION,
			};

			const float												speedXY_default = 0.5f; // in pixels per frame
			const float												speedZ_default = 0.1f; // in blitter canvas sourceLayers per frame
			const float												speedScale_default = 0.025f; // in proportion of tile size per frame
			const float												speedShear_default = 0.025f; // in proportion of tile size per frame
			const float												speedRotate_default = G_TAU_F / 200; // in radians per frame
			float													speedXY = speedXY_default; // in pixels per frame
			float													speedZ = speedZ_default; // in blitter canvas sourceLayers per frame
			float													speedScale = speedScale_default; // in proportion of tile size per frame
			float													speedShear = speedShear_default; // in proportion of tile size per frame
			float													speedRotate = speedRotate_default; // in radians per frame

			const float												speedXY_adjust_gross = 0.005f; // in pixels per frame per frame
			const float												speedZ_adjust_gross = 0.001f; // in blitter canvas sourceLayers per frame per frame
			const float												speedScale_adjust_gross = 0.00025f; // in proportion of tile size per frame per frame
			const float												speedShear_adjust_gross = 0.00025f; // in proportion of tile size per frame per frame
			const float												speedRotate_adjust_gross = G_TAU_F / 10000; // in radians per frame per frame
			const float												speedXY_adjust_fine = 0.0001f; // in pixels per frame per frame
			const float												speedZ_adjust_fine = 0.00005f; // in blitter canvas sourceLayers per frame per frame
			const float												speedScale_adjust_fine = 0.000025f; // in proportion of tile size per frame per frame
			const float												speedShear_adjust_fine = 0.000025f; // in proportion of tile size per frame per frame
			const float												speedRotate_adjust_fine = G_TAU_F / 50000; // in radians per frame per frame

			unsigned int											tileIndex = TILE_NAME::BOUNDS_BLOCK_8X8;
			unsigned short											flags = 0;
			float													positionX = 0; // in pixels
			float													positionY = 0; // in pixels
			float													positionZ = 0; // in pixels
			float													pivotX = 0; // in pixels
			float													pivotY = 0; // in pixels
			float													scaleX = 1; // relative to tile width
			float													scaleY = 1; // relative to tile height
			float													shearX = 0;
			float													shearY = 0;
			float													rotation = 0; // in radians

			bool													update = false;
			ACTIVE_CONTROL											activeControl = ACTIVE_CONTROL::POSITION;
			std::vector<GW::GRAPHICS::GBlitter::DrawInstruction>	draws;
			unsigned int											numDraws = 0;
		} freeformTest;

		void LoadSource(int _sourceid, bool _compressedColor, bool _compressedLayer, bool _compressedStencil)
		{
			const char* c = _compressedColor ? SourceFilenames[_sourceid].c_cmp : SourceFilenames[_sourceid].c;
			const char* l = _compressedLayer ? SourceFilenames[_sourceid].l_cmp : SourceFilenames[_sourceid].l;
			const char* s = _compressedStencil ? SourceFilenames[_sourceid].s_cmp : SourceFilenames[_sourceid].s;
			char colorPath[maxPathLength];
			char layerPath[maxPathLength];
			char stencilPath[maxPathLength];
			if (c)
			{
				blitter_FilenameToFilepath(c, colorPath);
				c = colorPath;
			}
			if (l)
			{
				blitter_FilenameToFilepath(l, layerPath);
				l = layerPath;
			}
			if (s)
			{
				blitter_FilenameToFilepath(s, stencilPath);
				s = stencilPath;
			}
			REQUIRE(+gBlitter.LoadSource(c, l, s, sourceIndices[_sourceid]));
		}
		void InitSources()
		{
			proceduralSources[SOURCE_NAME::PROC_HEATMAP]			= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_HEATMAP_BLOCKS]		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_C] 		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_L] 		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_S] 		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CL]		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CS]		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_LS]		= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CLS]	= { w, h, new unsigned int[size] };
			proceduralSources[SOURCE_NAME::PROC_RESULT_GRID]		= { w, h, new unsigned int[size] };

			// generate procedural source pixel data
			for (unsigned int s = 0; s < SOURCE_NAME::PROCEDURAL_COUNT; ++s)
			{
				ProceduralSource& src = proceduralSources[s];

				for (unsigned int y = 0; y < src.h; ++y)
					for (unsigned int x = 0; x < src.w; ++x)
					{
						unsigned int& pixel = src.colors[x + y * src.w]; // store pixel directly for quick access
						unsigned int xr, yr; // ratios used to fill sources

						switch (s) // this switch statement allows all source Init logic to be in one loop
						{
							case SOURCE_NAME::PROC_HEATMAP:
								// x/y heatmap
								xr = static_cast<unsigned int>(((float)x / (float)src.w) * 255);
								yr = static_cast<unsigned int>(((float)y / (float)src.h) * 255);
								pixel = 0xff000000 | (xr << 16) | (yr << 8);
								break;
							case SOURCE_NAME::PROC_HEATMAP_BLOCKS:
								// x/y heatmap quantized by position in source block grid
								xr = static_cast<unsigned int>(((float)((x / 256) * 256) / (float)src.w) * 255);
								yr = static_cast<unsigned int>(((float)((y / 256) * 256) / (float)src.h) * 255);
								pixel = 0xff000000 | (xr << 16) | (yr << 8);
								break;
							// for source block grids: red = color data, green = layer data, blue = stencil data for block height representations
							case SOURCE_NAME::PROC_SOURCE_GRID_C:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 256 == 0)
									pixel = procSrc_colorGridLineC;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_L:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 1024 == 0)
									pixel = procSrc_colorGridLineL;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_S:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 1024 == 0)
									pixel = procSrc_colorGridLineS;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_CL:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 204 == 0)
									pixel = procSrc_colorGridLineC | procSrc_colorGridLineL;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_CS:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 204 == 0)
									pixel = procSrc_colorGridLineC | procSrc_colorGridLineS;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_LS:
								if (x % 256 == 0)
									pixel = procSrc_colorGridLineDefault;
								else if (y % 512 == 0)
									pixel = procSrc_colorGridLineL | procSrc_colorGridLineS;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_SOURCE_GRID_CLS:
								if (x % 256 == 0 || y % 170 == 0)
									pixel = procSrc_colorGridLineC | procSrc_colorGridLineL | procSrc_colorGridLineS;
								else
									pixel = procSrc_colorBG;
								break;
							case SOURCE_NAME::PROC_RESULT_GRID:
								if (x % 128 == 0 || y % 128 == 0)
									pixel = procSrc_colorGridLineDefault;
								else
									pixel = procSrc_colorBG;
								break;
							default: break;
						} // end switch s
					} // end iterate through source pixels
			} // end generate source pixel data
			// import procedural sources into blitter
			for (unsigned int i = 0; i < SOURCE_NAME::PROCEDURAL_COUNT; ++i)
				REQUIRE(+gBlitter.ImportSource(proceduralSources[i].colors, nullptr, nullptr, proceduralSources[i].w, proceduralSources[i].h, 0, 0, sourceIndices[i]));

			// load remaining sources into blitter
			// TGA format tests
			LoadSource(SOURCE_NAME::TGA_RGB_LUT_8, false, false, false);
			LoadSource(SOURCE_NAME::TGA_L_8, false, false, false);
			LoadSource(SOURCE_NAME::TGA_AL_16, false, false, false);
			LoadSource(SOURCE_NAME::TGA_RGB_24, false, false, false);
			LoadSource(SOURCE_NAME::TGA_ARGB_32, false, false, false);
			LoadSource(SOURCE_NAME::TGA_RGB_LUT_8_RLE, true, true, true);
			LoadSource(SOURCE_NAME::TGA_L_8_RLE, true, true, true);
			LoadSource(SOURCE_NAME::TGA_AL_16_RLE, true, true, true);
			LoadSource(SOURCE_NAME::TGA_RGB_24_RLE, true, true, true);
			LoadSource(SOURCE_NAME::TGA_ARGB_32_RLE, true, true, true);
			
			// BMP format tests
			LoadSource(SOURCE_NAME::BMP_RGB_24, false, false, false);
			LoadSource(SOURCE_NAME::BMP_RGBA_32, false, false, false);


			// visual information blocks
			LoadSource(SOURCE_NAME::AXIS_BLOCKS, true, true, true);
			//LoadSource(SOURCE_NAME::BOUNDS_BLOCKS, true, true, true);
			LoadSource(SOURCE_NAME::BOUNDS_BLOCKS, false, true, true); // delete this and comment the above back in once temporary version of multi-block tile isn't needed anymore

			// sprites
			LoadSource(SOURCE_NAME::GROUND_TILES, true, true, true);
			LoadSource(SOURCE_NAME::TREES, true, true, true);
			LoadSource(SOURCE_NAME::CHARACTERS, true, true, true);
			LoadSource(SOURCE_NAME::FLAMES, true, true, true);
		}
		void InitTiles()
		{
			// source index, x, y, w, h, mask (C/L/S)
#pragma region entire procedural sources

			tileDefinitions[TILE_NAME::PROC_HEATMAP]			= { sourceIndices[SOURCE_NAME::PROC_HEATMAP],			0, 0, proceduralSources[SOURCE_NAME::PROC_HEATMAP].w,			proceduralSources[SOURCE_NAME::PROC_HEATMAP].h };
			tileDefinitions[TILE_NAME::PROC_HEATMAP_BLOCKS]		= { sourceIndices[SOURCE_NAME::PROC_HEATMAP_BLOCKS],	0, 0, proceduralSources[SOURCE_NAME::PROC_HEATMAP_BLOCKS].w,	proceduralSources[SOURCE_NAME::PROC_HEATMAP_BLOCKS].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_C]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_C], 	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_C].w,		proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_C].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_L]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_L], 	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_L].w,		proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_L].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_S]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_S], 	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_S].w,		proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_S].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_CL]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_CL],	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CL].w,	proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CL].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_CS]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_CS],	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CS].w,	proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CS].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_LS]		= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_LS],	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_LS].w,	proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_LS].h };
			tileDefinitions[TILE_NAME::PROC_SOURCE_GRID_CLS]	= { sourceIndices[SOURCE_NAME::PROC_SOURCE_GRID_CLS],	0, 0, proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CLS].w,	proceduralSources[SOURCE_NAME::PROC_SOURCE_GRID_CLS].h };
			tileDefinitions[TILE_NAME::PROC_RESULT_GRID]		= { sourceIndices[SOURCE_NAME::PROC_RESULT_GRID],		0, 0, proceduralSources[SOURCE_NAME::PROC_RESULT_GRID].w,		proceduralSources[SOURCE_NAME::PROC_RESULT_GRID].h };
			
#pragma endregion // entire procedural sources
#pragma region entire loaded sources

			// TGA format tests
			tileDefinitions[TILE_NAME::TGA_RGB_LUT_8] =
			{
				sourceIndices[SOURCE_NAME::TGA_RGB_LUT_8],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_L_8] =
			{
				sourceIndices[SOURCE_NAME::TGA_L_8],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_AL_16] =
			{
				sourceIndices[SOURCE_NAME::TGA_AL_16],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_RGB_24] =
			{
				sourceIndices[SOURCE_NAME::TGA_RGB_24],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_ARGB_32] =
			{
				sourceIndices[SOURCE_NAME::TGA_ARGB_32],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_RGB_LUT_8_RLE] =
			{
				sourceIndices[SOURCE_NAME::TGA_RGB_LUT_8_RLE],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_L_8_RLE] =
			{
				sourceIndices[SOURCE_NAME::TGA_L_8_RLE],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_AL_16_RLE] =
			{
				sourceIndices[SOURCE_NAME::TGA_AL_16_RLE],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_RGB_24_RLE] =
			{
				sourceIndices[SOURCE_NAME::TGA_RGB_24_RLE],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::TGA_ARGB_32_RLE] =
			{
				sourceIndices[SOURCE_NAME::TGA_ARGB_32_RLE],
				0,
				0,
				64,
				64
			};

			tileDefinitions[TILE_NAME::BMP_RGB_24] = {
				sourceIndices[SOURCE_NAME::BMP_RGB_24],
				0,
				0,
				64,
				64
			};
			tileDefinitions[TILE_NAME::BMP_RGBA_32] = {
				sourceIndices[SOURCE_NAME::BMP_RGBA_32],
				0,
				0,
				64,
				64
			};

			// visual information blocks
			tileDefinitions[TILE_NAME::AXIS_BLOCKS] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				0,
				0,
				1024,
				1024
			};
			tileDefinitions[TILE_NAME::BOUNDS_BLOCKS] =
			{
				sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],
				0,
				0,
				517,
				261
			};

			// sprites
			tileDefinitions[TILE_NAME::GROUND_TILES] =
			{
				sourceIndices[SOURCE_NAME::GROUND_TILES],
				0,
				0,
				144,
				112
			};
			tileDefinitions[TILE_NAME::TREES] =
			{
				sourceIndices[SOURCE_NAME::TREES],
				0,
				0,
				499,
				363
			};
			tileDefinitions[TILE_NAME::CHARACTERS] =
			{
				sourceIndices[SOURCE_NAME::CHARACTERS],
				0,
				0,
				188,
				161
			};
			tileDefinitions[TILE_NAME::FLAMES] =
			{
				sourceIndices[SOURCE_NAME::FLAMES],
				0,
				0,
				285,
				192
			};

#pragma endregion // entire loaded sources
#pragma region individual tiles

			// visual information blocks

			// axis blocks
#define GB_VT_AXIS_BLOCK_W		256
#define GB_VT_AXIS_BLOCK_H		256
#define GB_VT_AXIS_BLOCK_M_C	0x00000000
#define GB_VT_AXIS_BLOCK_M_L	255
#define GB_VT_AXIS_BLOCK_M_S	0
			tileDefinitions[TILE_NAME::AXIS_BLOCK_1X1] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				0,
				0,
				GB_VT_AXIS_BLOCK_W,
				GB_VT_AXIS_BLOCK_H,
				GB_VT_AXIS_BLOCK_M_C, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
			tileDefinitions[TILE_NAME::AXIS_BLOCK_2X2] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				0,
				0,
				GB_VT_AXIS_BLOCK_W * 2,
				GB_VT_AXIS_BLOCK_H * 2,
				GB_VT_AXIS_BLOCK_M_C, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
			tileDefinitions[TILE_NAME::AXIS_BLOCK_3X3] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				0,
				0,
				GB_VT_AXIS_BLOCK_W * 3,
				GB_VT_AXIS_BLOCK_H * 3,
				GB_VT_AXIS_BLOCK_M_C, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
			tileDefinitions[TILE_NAME::AXIS_BLOCK_4X4] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				0,
				0,
				GB_VT_AXIS_BLOCK_W * 4,
				GB_VT_AXIS_BLOCK_H * 4,
				GB_VT_AXIS_BLOCK_M_C, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
#define GB_VT_AXIS_BLOCK_CENTER_W	(GB_VT_AXIS_BLOCK_W / 2)
#define GB_VT_AXIS_BLOCK_CENTER_H	(GB_VT_AXIS_BLOCK_H / 2)
			tileDefinitions[TILE_NAME::AXIS_BLOCK_CENTER] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				static_cast<unsigned short>(GB_VT_AXIS_BLOCK_W * 1.75f),
				static_cast<unsigned short>(GB_VT_AXIS_BLOCK_H * 1.75f),
				GB_VT_AXIS_BLOCK_CENTER_W,
				GB_VT_AXIS_BLOCK_CENTER_H,
				GB_VT_AXIS_BLOCK_M_C, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
			tileDefinitions[TILE_NAME::AXIS_BLOCK_RED_MASKED] =
			{
				sourceIndices[SOURCE_NAME::AXIS_BLOCKS],
				static_cast<unsigned short>(GB_VT_AXIS_BLOCK_W * 1.75f),
				static_cast<unsigned short>(GB_VT_AXIS_BLOCK_H * 1.75f),
				GB_VT_AXIS_BLOCK_CENTER_W,
				GB_VT_AXIS_BLOCK_CENTER_H,
				0xffff0000, GB_VT_AXIS_BLOCK_M_L, GB_VT_AXIS_BLOCK_M_S
			};
#undef GB_VT_AXIS_BLOCK_W
#undef GB_VT_AXIS_BLOCK_H
#undef GB_VT_AXIS_BLOCK_M_C
#undef GB_VT_AXIS_BLOCK_M_L
#undef GB_VT_AXIS_BLOCK_M_S
#undef GB_VT_AXIS_BLOCK_CENTER_W
#undef GB_VT_AXIS_BLOCK_CENTER_H

			// bounds blocks
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_1X1]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	1,		1,		1,		1 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_2X2]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	3,		1,		2,		2 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_3X3]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	6,		1,		3,		3 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_4X4]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	10,		1,		4,		4 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_5X5]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	15,		1,		5,		5 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_6X6]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	21,		1,		6,		6 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_7X7]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	28,		1,		7,		7 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_8X8]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	36,		1,		8,		8 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_12X12]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	45,		1,		12,		12 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_16X16]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	58,		1,		16,		16 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_24X24]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	75,		1,		24,		24 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_32X32]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	100,	1,		32,		32 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_48X48]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	133,	1,		48,		48 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_64X64]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	182,	1,		64,		64 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_96X96]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	130,	66,		96,		96 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_128X128]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	1,		34,		128,	128 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_192X192]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	257,	1,		192,	192 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_3X5]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	1,		13,		3,		5 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_5X3]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	10,		13,		5,		3 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_4X6]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	5,		13,		4,		6 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_6X4]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	10,		17,		6,		4 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_3X15]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	17,		16,		3,		15 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_15X3]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	26,		16,		15,		3 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_4X16]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	21,		16,		4,		16 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_16X4]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	26,		20,		16,		4 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_MULTI_EVEN]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	252,	252,	8,		8 };
			tileDefinitions[TILE_NAME::BOUNDS_BLOCK_MULTI_ODD]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS],	507,	251,	9,		9 };

			tileDefinitions[TILE_NAME::MULTI_BLOCK_SPLIT_TOP_LEFT]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS], 252, 252, 4, 4 };
			tileDefinitions[TILE_NAME::MULTI_BLOCK_SPLIT_TOP_RIGHT]		= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS], 256, 252, 4, 4 };
			tileDefinitions[TILE_NAME::MULTI_BLOCK_SPLIT_BOTTOM_RIGHT]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS], 256, 256, 4, 4 };
			tileDefinitions[TILE_NAME::MULTI_BLOCK_SPLIT_BOTTOM_LEFT]	= { sourceIndices[SOURCE_NAME::BOUNDS_BLOCKS], 252, 256, 4, 4 };

			// end visual information blocks
			// sprites

			// ground tiles
#define GB_VT_GROUND_TILE_W		16
#define GB_VT_GROUND_TILE_H		16
#define GB_VT_GROUND_TILE_M_C	0x00000000
#define GB_VT_GROUND_TILE_M_L	255
#define GB_VT_GROUND_TILE_M_S	0
			tileDefinitions[TILE_NAME::GROUND_TILE_GRASS] =
			{
				sourceIndices[SOURCE_NAME::GROUND_TILES],
				3 * GB_VT_GROUND_TILE_W,
				0 * GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_W,
				GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_M_C,
				GB_VT_GROUND_TILE_M_L,
				GB_VT_GROUND_TILE_M_S
			};
			tileDefinitions[TILE_NAME::GROUND_TILE_DIRT] =
			{
				sourceIndices[SOURCE_NAME::GROUND_TILES],
				3 * GB_VT_GROUND_TILE_W,
				1 * GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_W,
				GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_M_C,
				GB_VT_GROUND_TILE_M_L,
				GB_VT_GROUND_TILE_M_S
			};
			tileDefinitions[TILE_NAME::GROUND_TILE_SHRUB] =
			{
				sourceIndices[SOURCE_NAME::GROUND_TILES],
				6 * GB_VT_GROUND_TILE_W,
				2 * GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_W,
				GB_VT_GROUND_TILE_H,
				GB_VT_GROUND_TILE_M_C,
				GB_VT_GROUND_TILE_M_L,
				GB_VT_GROUND_TILE_M_S
			};
#undef GB_VT_GROUND_TILE_W
#undef GB_VT_GROUND_TILE_H
#undef GB_VT_GROUND_TILE_M_C
#undef GB_VT_GROUND_TILE_M_L
#undef GB_VT_GROUND_TILE_M_S

			// trees
			tileDefinitions[TILE_NAME::TREE_SMALL] =
			{
				sourceIndices[SOURCE_NAME::TREES],
				0,
				0,
				170,
				209
			};
			tileDefinitions[TILE_NAME::TREE_LARGE] =
			{
				sourceIndices[SOURCE_NAME::TREES],
				170,
				0,
				328,
				362
			};

			// characters
			tileDefinitions[TILE_NAME::CHARACTER_MARIO] =
			{
				sourceIndices[SOURCE_NAME::CHARACTERS],
				0,
				0,
				14,
				29,
				0x00000000,
				255,
				0
			};
			tileDefinitions[TILE_NAME::CHARACTER_SONIC] =
			{ sourceIndices[SOURCE_NAME::CHARACTERS],
				14,
				0,
				27,
				39,
				0x00000000,
				255,
				0
			};
			tileDefinitions[TILE_NAME::CHARACTER_LINK] =
			{
				sourceIndices[SOURCE_NAME::CHARACTERS],
				41,
				0,
				147,
				161,
				0x00000000,
				255,
				0
			};

			// flames
			tileDefinitions[TILE_NAME::FLAME_SINGLE] =
			{
				sourceIndices[SOURCE_NAME::FLAMES],
				0,
				0,
				30,
				52,
				0x00000000,
				255,
				0
			};
			tileDefinitions[TILE_NAME::FLAME_MULTI] =
			{
				sourceIndices[SOURCE_NAME::FLAMES],
				255,
				140,
				30,
				52,
				0x00000000,
				255,
				0
			};

			// end sprites

#pragma endregion // individual tiles

			// pass tile definitions to blitter
			REQUIRE(+gBlitter.DefineTiles(tileDefinitions, TILE_NAME::COUNT, tileIndices));
		}
		void InitTestSets()
		{
			/*
			draw instructions that are too long to be readable on one line or have flags that need to be easily changeable for testing are formatted as follows,
			  with everything aligned vertically being part of the same variable:

				tile index
					flags
						layer
							stencil
								t
									m
										p
			*/

			unsigned int tileId = 0xffffffff;
			unsigned short flags = 0;
			float offsetX = 0.0f, offsetY = 0.0f, spacingX = 0.0f, spacingY = 0.0f;
			int x = 0, y = 0;

			std::vector<GW::GRAPHICS::GBlitter::DrawInstruction> draws;
			presetTests.AddTest(draws, "default");
#pragma region preset tests - full sources
			draws.push_back({ tileIndices[TILE_NAME::PROC_HEATMAP] });
			presetTests.AddTest(draws, "src_proc_heatmap");
			draws.push_back({ tileIndices[TILE_NAME::PROC_HEATMAP_BLOCKS] });
			presetTests.AddTest(draws, "src_proc_heatmap_blocks");

			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_C] });
			presetTests.AddTest(draws, "src_proc_src_grid_c");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_L] });
			presetTests.AddTest(draws, "src_proc_src_grid_l");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_S] });
			presetTests.AddTest(draws, "src_proc_src_grid_s");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_CL] });
			presetTests.AddTest(draws, "src_proc_src_grid_cl");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_CS] });
			presetTests.AddTest(draws, "src_proc_src_grid_cs");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_LS] });
			presetTests.AddTest(draws, "src_proc_src_grid_ls");
			draws.push_back({ tileIndices[TILE_NAME::PROC_SOURCE_GRID_CLS] });
			presetTests.AddTest(draws, "src_proc_src_grid_cls");
			draws.push_back({ tileIndices[TILE_NAME::PROC_RESULT_GRID] });
			presetTests.AddTest(draws, "src_proc_res_grid");

			draws.push_back({ tileIndices[TILE_NAME::AXIS_BLOCKS] });
			presetTests.AddTest(draws, "src_axis_blocks");
			draws.push_back({ tileIndices[TILE_NAME::BOUNDS_BLOCKS] });
			presetTests.AddTest(draws, "src_bounds_blocks");

			draws.push_back({ tileIndices[TILE_NAME::GROUND_TILES] });
			presetTests.AddTest(draws, "src_ground_tiles");
			draws.push_back({ tileIndices[TILE_NAME::TREES] });
			presetTests.AddTest(draws, "src_trees");
			draws.push_back({ tileIndices[TILE_NAME::CHARACTERS] });
			presetTests.AddTest(draws, "src_characters");
			draws.push_back({ tileIndices[TILE_NAME::FLAMES] });
			presetTests.AddTest(draws, "src_flames");
#pragma endregion preset tests - full sources
#pragma region preset tests - TGA format load test
			flags = DrawFlag::USS | DrawFlag::TSP;
			unsigned short formatTest_pos_x = 0, formatTest_pos_y = 0;
			unsigned short formatTest_spacing_x = 96, formatTest_spacing_y = 96;
			unsigned short formatTest_offset_x = 32, formatTest_offset_y = 32;

			draws.push_back({
				tileIndices[TILE_NAME::TGA_L_8]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TGA_AL_16]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_RGB_24]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_ARGB_32]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_RGB_LUT_8]
					, flags
						, 255
							, 0 | 0x7f
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				});
			formatTest_pos_x = 0; ++formatTest_pos_y;
			draws.push_back({
				tileIndices[TILE_NAME::TGA_L_8_RLE]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_AL_16_RLE]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_RGB_24_RLE]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_ARGB_32_RLE]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });
			draws.push_back({
				tileIndices[TILE_NAME::TGA_RGB_LUT_8_RLE]
					, flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				 });

			presetTests.AddTest(draws, "tga_format_load");
#pragma endregion preset tests - TGA format load test
#pragma region preset tests - BMP load test

			formatTest_pos_x = 0, formatTest_pos_y = 0;
			formatTest_spacing_x = 96, formatTest_spacing_y = 96;
			formatTest_offset_x = 32, formatTest_offset_y = 32;

			flags = DrawFlag::USS |  DrawFlag::TSP;

			/// COME BACK TO ME...
			draws.push_back({
				tileIndices[TILE_NAME::BMP_RGB_24]
					,flags
						, 255
							, 0		
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
				});

			draws.push_back({
				tileIndices[TILE_NAME::BMP_RGBA_32]
					,flags
						, 255
							, 0
								, { float(formatTest_pos_x++ * formatTest_spacing_x + formatTest_offset_x), float(formatTest_pos_y * formatTest_spacing_y + formatTest_offset_y) }
							});


			presetTests.AddTest(draws, "bmp_format_load");

#pragma endregion preset tests - BMP load test

			// result block grid
			draws.push_back({ tileIndices[TILE_NAME::PROC_RESULT_GRID], 0, 255, 0 });

			// full tile
			draws.push_back({ tileIndices[TILE_NAME::BOUNDS_BLOCK_MULTI_EVEN], 0, 255, 0, { 50, 50 } });
			draws.push_back({ tileIndices[TILE_NAME::MULTI_BLOCK_SPLIT_TOP_LEFT], 0, 255, 0, { 50, 60 } });
			draws.push_back({ tileIndices[TILE_NAME::MULTI_BLOCK_SPLIT_TOP_RIGHT], 0, 255, 0, { 54, 60 } });
			draws.push_back({ tileIndices[TILE_NAME::MULTI_BLOCK_SPLIT_BOTTOM_RIGHT], 0, 255, 0, { 54, 64 } });
			draws.push_back({ tileIndices[TILE_NAME::MULTI_BLOCK_SPLIT_BOTTOM_LEFT], 0, 255, 0, { 50, 64 } });

			presetTests.AddTest(draws, "split_multi_block");
#pragma region preset tests - basic draws - static

			// result block grid
			draws.push_back({ tileIndices[TILE_NAME::PROC_RESULT_GRID], 0, 255, 0 });

			// single source block
			tileId = TILE_NAME::FLAME_SINGLE;
			offsetX = 5;
			offsetY = 5;
			spacingX = tileDefinitions[tileId].h + offsetX;
			spacingY = tileDefinitions[tileId].h + offsetY;
			x = 0;
			y = 0;

			// no flags set
			draws.push_back({
				tileIndices[tileId]
					, 0
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// ignore source colors
			//   (use source stencils is also on to prove the draw happened, as long as that flag is working)
			//   this instruction should have no color data drawn
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::ISC
					| DrawFlag::USS
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use source layer values
			//   this instruction should have portions of it replaced in post-processing based on layer values
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::USL
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use source stencil values
			//   this instruction should have portions of it replaced in post-processing based on stencil values
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::USS
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use bitmasking
			//   this instruction should have the background color removed
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::MSK
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use transparency
			//   this instruction should have alpha blending applied
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::TSP
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use source sourceLayers, use source stencils
			//   this instruction should have portions of it replaced in post-processing based on layer and stencil values
			draws.push_back({
				tileIndices[tileId]
					, 0
					| DrawFlag::USL
					| DrawFlag::USS
						, 20
							, 0
								, { offsetX + (x++ * spacingX), offsetY + (y * spacingY) }
				});

			// use source layers, use bitmasking

			// use source layers, use transparency

			// use source layers, use interpolation

			// use source layers, sort per pixel

			// use source stencils, use bitmasking

			// use source stencils, use transparency

			// use source stencils, use interpolation

			// use source stencils, sort per pixel

			// use bitmasking, use transparency

			// use bitmasking, use interpolation

			// use bitmasking, sort per pixel

			// use transparency, 

			// use transparency, 


			presetTests.AddTest(draws, "basic_draws_static");
#pragma endregion preset tests - basic draws - static
#pragma region preset tests - basic draws - dynamic
			// XZ
			// YZ
			// XY
#pragma endregion preset tests - basic draws - dynamic
#pragma region preset tests - basic draws - basic transf flags - static
#pragma endregion preset tests - basic draws - basic transf flags - static
#pragma region preset tests - basic draws - basic transf flags - dynamic
			// X
			// Y
			// XY
#pragma endregion preset tests - basic draws - basic transf flags - dynamic
#pragma region preset tests - transf draws - static
#pragma endregion preset tests - transf draws - static
#pragma region preset tests - transf draws - dynamic
			// XZ
			// YZ
			// XY
#pragma endregion preset tests - transf draws - dynamic
#pragma region preset tests - transf draws - basic transf flags - static
#pragma endregion preset tests - transf draws - basic transf flags - static
#pragma region preset tests - transf draws - basic transf flags - dynamic
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - basic transf flags - dynamic
#pragma region preset tests - transf draws - scale - dynamic transf
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - scale - dynamic transf
#pragma region preset tests - transf draws - scale - dynamic pos
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - scale - dynamic pos
#pragma region preset tests - transf draws - shear - dynamic transf
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - shear - dynamic transf
#pragma region preset tests - transf draws - shear - dynamic pos
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - shear - dynamic pos
#pragma region preset tests - transf draws - rotation - dynamic transf
#pragma endregion preset tests - transf draws - rotation - dynamic transf
#pragma region preset tests - transf draws - rotation - dynamic pos
			// X
			// Y
			// XY
#pragma endregion preset tests - transf draws - rotation - dynamic pos

#pragma region preset tests - scratch test
			// scratch test, needs to be replaced with new tests
			// result block grid
			draws.push_back({ tileIndices[TILE_NAME::PROC_RESULT_GRID], 0, 255, 0 });

#define GBLITTER_VT_TEST_BASIC_INSTRUCTIONS // uncomment this line to test basic instructions with no transformations
#if defined(GBLITTER_VT_TEST_BASIC_INSTRUCTIONS)
			// this instruction should have portions of it replaced in post-processing based on stencil values
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::USS
						, 20
							, 0
								, { 70, 512 }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_MARIO]
					, 0
					| DrawFlag::MSK
						, 20
							, 0
								, { 110, 523 }
				});
			// this instruction should be drawn behind the previous ones
			draws.push_back({
				tileIndices[TILE_NAME::TREE_SMALL]
					, 0
					| DrawFlag::MSK
						, 21
							, 0
								, { 20, 350 }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_LINK]
					, 0
					| DrawFlag::MSK
						, 20
							, 0
								, { 350, 420 }
				});
			// this instruction should be drawn behind the previous one
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
						, 30
							, 0
								, { 220, 225 }
				});
			// this instruction should be clipped along the top and left edges of the result
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
						, 30
							, 0
								, { -float(tileDefinitions[TILE_NAME::TREE_LARGE].w / 2)
								, -float(tileDefinitions[TILE_NAME::TREE_LARGE].h / 2) }
				});
			// this instruction should be clipped along the bottom and right edges of the result
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
						, 19
							, 0
								, { float(w) - (tileDefinitions[TILE_NAME::TREE_LARGE].w / 2)
								, float(h) - (tileDefinitions[TILE_NAME::TREE_LARGE].h / 2) }
				});

			// ground tileDefinitions (demonstrates creating instructions programatically in a loop)
			unsigned short gt_xOrigin = 20, gt_yOrigin = 535;	// ground tile block x/y origin in result
			unsigned short gt_x = 0, gt_y = 0;					// x/y index of ground tile in block
			unsigned short gt_tilesWide = 10;					// how many tileDefinitions wide the block of tileDefinitions is
			for (gt_x = 0; gt_x < gt_tilesWide; ++gt_x)
				draws.push_back({
					tileIndices[TILE_NAME::GROUND_TILE_SHRUB]
						, 0
						| DrawFlag::MSK
							, 19
								, 0
									, { float(gt_xOrigin + gt_x * tileDefinitions[TILE_NAME::GROUND_TILE_SHRUB].w)
									, float(gt_yOrigin + gt_y * tileDefinitions[TILE_NAME::GROUND_TILE_SHRUB].h) }
					});
			++gt_y;
			for (gt_x = 0; gt_x < gt_tilesWide; ++gt_x)
				draws.push_back({
					tileIndices[TILE_NAME::GROUND_TILE_GRASS]
						, 0
							, 21
								, 0
									, { float(gt_xOrigin + gt_x * tileDefinitions[TILE_NAME::GROUND_TILE_GRASS].w)
									, float(gt_yOrigin + gt_y * tileDefinitions[TILE_NAME::GROUND_TILE_GRASS].h) }
					});
			++gt_y;
			for (gt_x = 0; gt_x < gt_tilesWide; ++gt_x)
				draws.push_back({
					tileIndices[TILE_NAME::GROUND_TILE_DIRT]
						, 0
							, 21
								, 0
									, { float(gt_xOrigin + gt_x * tileDefinitions[TILE_NAME::GROUND_TILE_DIRT].w)
									, float(gt_yOrigin + gt_y * tileDefinitions[TILE_NAME::GROUND_TILE_DIRT].h) }
					});
#undef GBLITTER_VT_TEST_BASIC_INSTRUCTIONS
#endif // GBLITTER_VT_TEST_BASIC_INSTRUCTIONS

#define GBLITTER_VT_TEST_BASIC_TRANSFORMATIONS // uncomment this line to test basic transformations (ROTATE/MIRROR_VERTICAL/MIRROR_HORIZONTAL)
#if defined(GBLITTER_VT_TEST_BASIC_TRANSFORMATIONS)
	// tileDefinitions defined across only one source block
			float basic_transf_offset_single_x = 112;
			float basic_transf_offset_single_y = 110;
			float basic_transf_spacing_single_x = 128;
			float basic_transf_spacing_single_y = 128;
			unsigned short basic_transf_x, basic_transf_y;

			basic_transf_x = 0; basic_transf_y = 0;
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::VRT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::ROT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::ROT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::VRT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::VRT
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::CHARACTER_SONIC]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { basic_transf_x++ * basic_transf_spacing_single_x + basic_transf_offset_single_x
								, basic_transf_y * basic_transf_spacing_single_y + basic_transf_offset_single_y }
				});

			// tileDefinitions defined across multiple source blocks
			float basic_transf_offset_multi_x = float(tileDefinitions[TILE_NAME::TREE_LARGE].w / 10);
			float basic_transf_offset_multi_y = float(tileDefinitions[TILE_NAME::TREE_LARGE].h / 10);
			float basic_transf_spacing_multi_x = float(w - tileDefinitions[TILE_NAME::TREE_LARGE].w - basic_transf_offset_multi_x * 2);
			float basic_transf_spacing_multi_y = float(h - tileDefinitions[TILE_NAME::TREE_LARGE].h - basic_transf_offset_multi_y * 2);
			basic_transf_x = 0; basic_transf_y = 0;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::VRT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y = 0;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::ROT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::ROT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y = 0;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::VRT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y = 0;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::VRT
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});

			basic_transf_x = 0; basic_transf_y++;
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
					| DrawFlag::MSK
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 30
							, 0
								, { basic_transf_x++ * basic_transf_spacing_multi_x + basic_transf_offset_multi_x
								, basic_transf_y * basic_transf_spacing_multi_y + basic_transf_offset_multi_y }
				});
#undef GBLITTER_VT_TEST_BASIC_TRANSFORMATIONS
#endif // GBLITTER_VT_TEST_BASIC_TRANSFORMATIONS

#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS // uncomment this line to test arbitrary transformation matrices
#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS)

			Matrix2x2	transf_m;
			bool		transf_transform = true;	// use this to toggle transformations for these instructions
			bool		transf_mask = false;		// use this to toggle masking for these instructions

#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_IDENTITY					// uncomment this line to test an identity matrix
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SCALING					// uncomment this line to test scaling matrices
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SHEARING					// uncomment this line to test shearing matrices
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_ROTATION					// uncomment this line to test rotation matrices
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIRRORING					// uncomment this line to test mirroring matrices
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_BASIC_PARITY				// uncomment this line to test basic transformation flag behavior parity with basic instructions
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIXED						// uncomment this line to test several different transformations mixed together
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MULTIPLE_SOURCE_BLOCKS	// uncomment this line to test transformations applied to tileDefinitions spanning multiple source blocks
#define GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SOURCE_FORMATS			// uncomment this line to test different source pixel data formats

			float transf_offset_x;
			float transf_offset_y;
			float transf_spacing_x;
			float transf_spacing_y;
			int transf_x, transf_y;

			// change these to change which tile is used for most tests
#define GBLITTER_VT_TRANSFORMED_TILE_INDEX (TILE_NAME::BOUNDS_BLOCK_24X24)
#define GBLITTER_VT_TRANSFORMED_TILE_W (24)
#define GBLITTER_VT_TRANSFORMED_TILE_H (24)

			transf_offset_x = GBLITTER_VT_TRANSFORMED_TILE_W * 2;
			transf_offset_y = GBLITTER_VT_TRANSFORMED_TILE_H * 2;
			transf_spacing_x = GBLITTER_VT_TRANSFORMED_TILE_W * 4;
			transf_spacing_y = GBLITTER_VT_TRANSFORMED_TILE_H * 2.5f;

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_IDENTITY)
			// identity matrix
			transf_x = 0; transf_y = 0;
			// this instruction should use the default identity matrix and be identical to a basic draw of the same tile
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_m = MatrixIdentity();
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// basic draw for reference
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_IDENTITY
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_IDENTITY

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SCALING)
			transf_x = 0; transf_y = 1;
			// this instruction should be scaled down in x and y
			transf_m = MatrixScaling(0.5f, 0.5f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			// this instruction should be scaled up in x and y
			transf_m = MatrixScaling(2.0f, 2.0f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			// this instruction should be scaled to up in x and down in y
			transf_m = MatrixScaling(2.0f, 0.5f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SCALING
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SCALING

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SHEARING)
			transf_x = 0; transf_y = 2;
			// this instruction should be sheared in x
			transf_m = MatrixShearing(0.33f, 0.0f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			// this instruction should be sheared in y
			transf_m = MatrixShearing(0.0f, 0.67f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			// this instruction should be sheared in x and y
			transf_m = MatrixShearing(-0.33f, -0.67f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SHEARING
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SHEARING

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_ROTATION)
			transf_x = 0; transf_y = 3;
			// this instruction should be rotated 45* (ie 1/8 rotation)
			transf_m = MatrixRotation(G_TAU_F / 8.0f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be rotated -45* (ie -1/8 rotation)
			transf_m = MatrixRotation(G_TAU_F / -8.0f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be rotated 90* (ie 1/4 rotation); it should be identical to a basic draw with the ROTATE flag set
			transf_m = MatrixRotation(G_TAU_F / 4.0f);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_ROTATION
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_ROTATION

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIRRORING)
			transf_x = 0; transf_y = 4;
			// this instruction should be mirrored in x and identical to a basic draw with the MIRROR_HORIZONTAL flag set
			transf_m = MatrixMirrorX();
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be mirrored in y and identical to a basic draw with the MIRROR_VERTICAL flag set
			transf_m = MatrixMirrorY();
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be mirrored in x and y and identical to a basic draw with the MIRROR_VERTICAL and MIRROR_HORIZONTAL flags set
			transf_m = MultiplyMatrixAndMatrix(MatrixMirrorY(), MatrixMirrorX());
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIRRORING
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIRRORING

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIXED)
			transf_x = 0; transf_y = 5;
			transf_m = { 1, 0, 0, 1 };
			// these instructions should build up the following transformations one at a time (all applied locally):
			//   rotated in the positive direction, mirrored in x, sheared in y, scaled in x, mirrored in y
			transf_m = MultiplyMatrixAndMatrix(MatrixRotation(G_TAU_F / 6), transf_m);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			transf_m = MultiplyMatrixAndMatrix(MatrixMirrorX(), transf_m);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			transf_m = MultiplyMatrixAndMatrix(MatrixShearing(0, 0.67f), transf_m);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			transf_m = MultiplyMatrixAndMatrix(MatrixScaling(2, 1), transf_m);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
			transf_m = MultiplyMatrixAndMatrix(MatrixMirrorY(), transf_m);
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIXED
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MIXED

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_BASIC_PARITY)
			transf_m = MatrixShearing(0.25f, 0.5f);

			transf_x = 0; transf_y = 6;
			// basic testCases for reference
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::HRZ
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::VRT
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			transf_x = 0; transf_y++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::ROT
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::ROT
					| DrawFlag::HRZ
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			transf_x++;

			transf_x = 0; transf_y++;
			// transformed testCases that should match the behavior of the basic ones above
			// no basic transformation flags set
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be mirrored horizontally in place because of the MIRROR_HORIZONTAL flag
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be mirrored vertically in place because of the MIRROR_VERTICAL flag
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be mirrored horizontally and vertically in place because of the MIRROR_VERTICAL and MIRROR_HORIZONTAL flags
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			transf_x = 0; transf_y++;
			// this instruction should be rotated 90* in place because of the ROTATE flag
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be rotated 90* and mirrored horizontally in place because of the ROTATE and MIRROR_HORIZONTAL flags
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be rotated 90* and mirrored vertically in place because of the ROTATE and MIRROR_VERTICAL flags
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
			// this instruction should be rotaed 90* and mirrored horizontally and vertically because of the ROTATE, MIRROR_VERTICAL, and MIRROR_HORIZONTAL flags
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			draws.push_back({
				tileIndices[GBLITTER_VT_TRANSFORMED_TILE_INDEX]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					| DrawFlag::ROT
					| DrawFlag::VRT
					| DrawFlag::HRZ
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { GBLITTER_VT_TRANSFORMED_TILE_W / 2.0f
										, GBLITTER_VT_TRANSFORMED_TILE_H / 2.0f }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_BASIC_PARITY
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_BASIC_PARITY

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MULTIPLE_SOURCE_BLOCKS)
			transf_offset_x = 100;
			transf_offset_y = 100;
			transf_spacing_x = 100;
			transf_spacing_y = 100;
			transf_x = 0; transf_y = 0;

			transf_m = MatrixShearing(0.1f, 0.2f);
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, 0
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { 1, 0, 0, 1 }
										, { tileDefinitions[TILE_NAME::TREE_LARGE].w / 2.0f,
										tileDefinitions[TILE_NAME::TREE_LARGE].h / 2.0f }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
										, { tileDefinitions[TILE_NAME::TREE_LARGE].w / 2.0f,
										tileDefinitions[TILE_NAME::TREE_LARGE].h / 2.0f }
				});
			transf_x++;

#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MULTIPLE_SOURCE_BLOCKS
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_MULTIPLE_SOURCE_BLOCKS

#if defined(GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SOURCE_FORMATS)
			transf_offset_x = 25;
			transf_offset_y = 25;
			transf_spacing_x = 25;
			transf_spacing_y = 25;
			transf_x = 0; transf_y = 0;

			transf_m = MatrixScaling(2, 2);
			// source with only color data
			draws.push_back({
				tileIndices[TILE_NAME::GROUND_TILE_SHRUB]
					, static_cast<unsigned short>(0
					| (transf_transform ? DrawFlag::TSF : 0)
					| (transf_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { transf_x * transf_spacing_x + transf_offset_x
								, transf_y * transf_spacing_y + transf_offset_y }
									, { transf_m.a, transf_m.b, transf_m.c, transf_m.d }
				});
			transf_x++;
#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SOURCE_FORMATS
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS_SOURCE_FORMATS

#undef GBLITTER_VT_TRANSFORMED_TILE_INDEX
#undef GBLITTER_VT_TRANSFORMED_TILE_W
#undef GBLITTER_VT_TRANSFORMED_TILE_H

#undef GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS
#endif // GBLITTER_VT_TEST_TRANSFORMED_INSTRUCTIONS

#define GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS // uncomment this line to test semitransparent alpha blending
#if defined(GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS)

			Matrix2x2	transp_m;
			bool			transp_transparent = true;	// use this to toggle transparency for these instructions
			bool			transp_transform = true;	// use this to toggle transformations for these instructions
			bool			transp_mask = true;			// use this to toggle masking for these instructions

#define GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC					// uncomment this line to test untransformed alpha blending
#define GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED			// uncomment this line to test transformed alpha blending
#define GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC_MASKED			// uncomment this line to test untransformed bitmasked alpha blending
#define GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED_MASKED	// uncomment this line to test transformed bitmasked alpha blending

#if defined(GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC)
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					)
						, 0
							, 0
								, { 50, 50 }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					)
						, 0
							, 0
								, { 75, 75 }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					)
						, 0
							, 0
								, { 100, 100 }
				});
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					)
						, 0
							, 0
								, { 125, 125 }
				});
#undef GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC
#endif // GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC

#if defined(GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED)
			transp_m = MatrixShearing(0.5f, 0.25f);
			draws.push_back({
				tileIndices[TILE_NAME::TREE_LARGE]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					| (transp_transparent ? DrawFlag::TSF : 0)
					)
						, 0
							, 0
								, { 500, 50 }
									, { transp_m.a, transp_m.b, transp_m.c, transp_m.d }
				});
#undef GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED
#endif // GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED

#if defined(GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC_MASKED)
			draws.push_back({
				tileIndices[TILE_NAME::AXIS_BLOCK_RED_MASKED]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					| (transp_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { 50, 550 }
				});
#undef GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC_MASKED
#endif // GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_BASIC_MASKED

#if defined(GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED_MASKED)
			transp_m = MatrixShearing(0.5f, 0.25f);
			draws.push_back({
				tileIndices[TILE_NAME::AXIS_BLOCK_RED_MASKED]
					, static_cast<unsigned short>(0
					| (transp_transparent ? DrawFlag::TSP : 0)
					| (transp_transparent ? DrawFlag::TSF : 0)
					| (transp_mask ? DrawFlag::MSK : 0)
					)
						, 0
							, 0
								, { 500, 550 }
									, { transp_m.a, transp_m.b, transp_m.c, transp_m.d }
				});
#undef GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED_MASKED
#endif // GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS_TRANSFORMED_MASKED

#undef GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS
#endif // GBLITTER_VT_TEST_TRANSPARENT_INSTRUCTIONS

			presetTests.AddTest(draws, "scratch");
			// end scratch test

#pragma endregion preset tests - scratch test





#pragma region freeform test

			// result block grid
			freeformTest.draws.push_back({ tileIndices[TILE_NAME::PROC_RESULT_GRID],		0,		255,	0 });
			// runtime-generated instruction initial state
			freeformTest.draws.push_back({ freeformTest.tileIndex,		freeformTest.flags,		1,		0 });
			// tile origin marker
			freeformTest.draws.push_back({ tileIndices[TILE_NAME::BOUNDS_BLOCK_1X1],		0,		0,		0 });
			// tile pivot marker
			freeformTest.draws.push_back({ tileIndices[TILE_NAME::BOUNDS_BLOCK_1X1],		0,		0,		0 });
			// tile extent marker
			freeformTest.draws.push_back({ tileIndices[TILE_NAME::BOUNDS_BLOCK_1X1],		0,		0,		0 });

			// additional insructions to show layer sorting changes
			freeformTest.draws.push_back({
				tileIndices[TILE_NAME::GROUND_TILE_GRASS]
					, 0
						, 5
							, 0
								, { 100, 232 }
				});
			freeformTest.draws.push_back({
				tileIndices[TILE_NAME::GROUND_TILE_GRASS]
					, 0
						, 5
							, 0
								, { 116, 232 }
				});
			freeformTest.draws.push_back({
				tileIndices[TILE_NAME::GROUND_TILE_GRASS]
					, 0
						, 5
							, 0
								, { 132, 232 }
				});
			freeformTest.draws.push_back({
				tileIndices[TILE_NAME::GROUND_TILE_SHRUB]
					, DrawFlag::MSK
						, 10
							, 0
								, { 116, 216 }
				});
			freeformTest.draws.push_back({
				tileIndices[TILE_NAME::TREE_SMALL]
					, DrawFlag::TSP
						, 15
							, 0
								, { 40, 35 }
				});

			// store number of instructions in dynamic test
			freeformTest.numDraws = static_cast<unsigned int>(freeformTest.draws.size());
			
#pragma endregion freeform test
		}
		void Init()
		{
			REQUIRE(+gBlitter.Create(w, h));
			InitSources();
			InitTiles();
			InitTestSets();
		}
		void UpdateFreeformTest()
		{
			freeformTest.update = false;

			GW::GRAPHICS::GBlitter::DrawInstruction* instr = nullptr;
			unsigned int tileId = 0;

			Matrix2x2 m = MatrixIdentity();
			m = MultiplyMatrixAndMatrix(MatrixRotation(freeformTest.rotation), m);
			m = MultiplyMatrixAndMatrix(MatrixShearing(freeformTest.shearX, freeformTest.shearY), m);
			m = MultiplyMatrixAndMatrix(MatrixScaling(freeformTest.scaleX, freeformTest.scaleY), m);

			// result block grid; no changes at runtime

			// tile
			++tileId; instr = &freeformTest.draws[tileId];
			instr->tile_id = freeformTest.tileIndex;
			instr->flags = freeformTest.flags;
			instr->t[0] = freeformTest.positionX;
			instr->t[1] = freeformTest.positionY;
			instr->t[2] = freeformTest.positionZ;
			instr->m[0][0] = m.m[0][0];
			instr->m[0][1] = m.m[0][1];
			instr->m[1][0] = m.m[1][0];
			instr->m[1][1] = m.m[1][1];
			instr->p[0] = freeformTest.pivotX;
			instr->p[1] = freeformTest.pivotY;

			// origin marker
			++tileId; instr = &freeformTest.draws[tileId];
			instr->t[0] = freeformTest.positionX;
			instr->t[1] = freeformTest.positionY;

			// pivot marker
			++tileId; instr = &freeformTest.draws[tileId];
			instr->t[0] = freeformTest.positionX + freeformTest.pivotX;
			instr->t[1] = freeformTest.positionY + freeformTest.pivotY;

			// extent marker
			++tileId; instr = &freeformTest.draws[tileId];
			instr->t[0] = freeformTest.positionX + tileDefinitions[freeformTest.tileIndex].w - 1;
			instr->t[1] = freeformTest.positionY + tileDefinitions[freeformTest.tileIndex].h - 1;
		}
		void ResetFreeformTestActiveControl()
		{
			switch (freeformTest.activeControl)
			{
				case FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
					freeformTest.positionX = 0;
					freeformTest.positionY = 0;
					freeformTest.positionZ = 0;
					break;
				case FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
					freeformTest.pivotX = 0;
					freeformTest.pivotY = 0;
					break;
				case FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
					freeformTest.scaleX = 1;
					freeformTest.scaleY = 1;
					break;
				case FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
					freeformTest.shearX = 0;
					freeformTest.shearY = 0;
					break;
				case FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
					freeformTest.rotation = 0;
					break;
			}
		}
		GW::GReturn DrawInstructionsToCanvas()
		{
			GW::GReturn result = GW::GReturn::FAILURE;

			gBlitter.Clear(clearColor, clearLayer, clearStencil);
			switch (testSetToDraw)
			{
				case BLITTER::TEST_SET::PRESET_TESTS:
					numDraws = static_cast<unsigned int>(presetTests.tests[presetTests.testToDraw].draws.size());
					draws = (numDraws > 0) ? &presetTests.tests[presetTests.testToDraw].draws[0] : nullptr;
					break;
				case BLITTER::TEST_SET::FREEFORM_TEST:
					numDraws = freeformTest.numDraws;
					draws = &freeformTest.draws[0];
					break;
				default: break;
			}
			switch (drawMode)
			{
				case CONTROL::INPUT_NAME::DRAW_MODE_DEFERRED:
					result = gBlitter.DrawDeferred(draws, numDraws);
					break;
				case CONTROL::INPUT_NAME::DRAW_MODE_IMMEDIATE:
					result = gBlitter.DrawImmediate(draws, numDraws);
					break;
				default: break;
			}
			return result;
		}
		void Cleanup()
		{
			for (int i = 0; i < SOURCE_NAME::PROCEDURAL_COUNT; ++i)
				delete[] proceduralSources[i].colors;
		}
	} blitter;
	class OUTPUT
	{
	public:
		enum class PIPELINE_STAGE : int
		{
			PROCEDURAL_SOURCES = 0,
			BLITTER_RESULT,
		};

		const unsigned int				w = 1050; // in pixels
		const unsigned int				h = 1050; // in pixels
		const unsigned int				size = w * h; // in pixels
		unsigned int*					outputColors = nullptr;
		unsigned char*					outputLayers = nullptr;
		unsigned char*					outputStencils = nullptr;
		unsigned int					proceduralSourceToDisplay = 0; // source to display to the window if viewing raw procedural source data
		PIPELINE_STAGE					pipelineStageToDisplay = PIPELINE_STAGE::PROCEDURAL_SOURCES;

		unsigned int*					displayColors = nullptr; // array of colors to display in the window; necessary for panning output, since panning left/right requires narrowing pixel rows
		unsigned int					w_display; // in pixels
		unsigned int					h_display; // in pixels
		const unsigned int				panAmount = 3; // in pixels per frame
		int								panH = 0; // in pixels
		int								panV = 0; // in pixels

		int								framerate = 0; // in frames per second
		bool							layerPostProcess = true;
		bool							stencilPostProcess = true;

		void Init()
		{
			outputColors = new unsigned int[size];
			outputLayers = new unsigned char[size];
			outputStencils = new unsigned char[size];
			displayColors = new unsigned int[size];
			memset(outputColors, 0x00, size << 2);
			memset(outputLayers, 0x00, size);
			memset(outputStencils, 0x00, size);
		}
		GW::GReturn DrawToScreen(WINDOW& _window, BLITTER& _blitter)
		{
			GW::GReturn result = GW::GReturn::FAILURE;
			switch (pipelineStageToDisplay)
			{
				case OUTPUT::PIPELINE_STAGE::PROCEDURAL_SOURCES:
					result = _window.gRasterSurface.SmartUpdateSurface(
						_blitter.proceduralSources[proceduralSourceToDisplay].colors,
						_blitter.proceduralSources[proceduralSourceToDisplay].w * _blitter.proceduralSources[proceduralSourceToDisplay].h,
						_blitter.proceduralSources[proceduralSourceToDisplay].w, _window.updateFlags
					);
					break;
				case OUTPUT::PIPELINE_STAGE::BLITTER_RESULT:
					// post processing
					for (unsigned short y = 0; y < h; ++y)
						for (unsigned short x = 0; x < w; ++x)
						{
							unsigned int index = x + y * w;
							// any pixel with a matching layer or stencil value is modified; values are hardcoded instead of on a macro or variable because this is a very specific test anyway so it's whatever
							if (layerPostProcess)
								switch (outputLayers[index])
								{
									case 0x7f: // this value is present in the sprites source
										outputColors[index] = 0xffffff00;
										break;
									default: break;
								}
							if (stencilPostProcess)
								switch (outputStencils[index])
								{
									case 0x05: // this value is the background in the sprites source
										outputColors[index] = 0xff000a00;
										break;
									case 0x0f: // this value is present in the sprites source
										outputColors[index] = 0xff001e00;
										break;
									case 0x14: // this value is present in the sprites source
										outputColors[index] = 0xff002800;
										break;
									case 0x7f: // this value is present in the w_h_format sources (grey text/checkered pattern)
										outputColors[index] = 0xff00bfff;
										break;
									default: break;
								}
						}
					w_display = w - panH;
					h_display = h - panV;
					// copy output data rows into display buffer (necessary to allow for horizontal panning)
					for (unsigned int y = 0; y < h_display; ++y)
						memcpy(&displayColors[y * w_display], &outputColors[panH + ((panV + y) * w)], w_display << 2);
					result = _window.gRasterSurface.SmartUpdateSurface(displayColors, w_display * h_display, w_display, _window.updateFlags);
					break;
			}
			if (G_FAIL(result))
				return result;
			result = _window.gRasterSurface.Present();
			return result;
		}
		void UpdateWindowTitle(WINDOW& _window, BLITTER& _blitter)
		{
			// set strings to fill in
			const char* pipelineStage = "";
			switch (pipelineStageToDisplay)
			{
				case OUTPUT::PIPELINE_STAGE::PROCEDURAL_SOURCES:
					pipelineStage = "Proc src data";
					break;
				case OUTPUT::PIPELINE_STAGE::BLITTER_RESULT:
					pipelineStage = "Blitter result";
					break;
			}
			char postproc[5];
			sprintf(postproc, "%c%c"
				, layerPostProcess		? 'L' : 'l'
				, stencilPostProcess	? 'S' : 's'
			);
			const char* testSet = "";
			switch (_blitter.testSetToDraw)
			{
				case BLITTER::TEST_SET::PRESET_TESTS:
					testSet = "Preset tests";
					break;
				case BLITTER::TEST_SET::FREEFORM_TEST:
					testSet = "Freeform test";
					break;
			}
			const char* test = "default";
			if (_blitter.testSetToDraw == BLITTER::TEST_SET::PRESET_TESTS)
				test = _blitter.presetTests.tests[_blitter.presetTests.testToDraw].name;
			char flags[17];
			sprintf(flags, "0000000%c%c%c%c%c%c%c%c%c"
				, (_blitter.freeformTest.flags & DrawFlag::TSP) ? 'A' : 'a'
				, (_blitter.freeformTest.flags & DrawFlag::TSF) ? 'T' : 't'
				, (_blitter.freeformTest.flags & DrawFlag::MSK) ? 'M' : 'm'
				, (_blitter.freeformTest.flags & DrawFlag::USS) ? 'S' : 's'
				, (_blitter.freeformTest.flags & DrawFlag::USL) ? 'L' : 'l'
				, (_blitter.freeformTest.flags & DrawFlag::ISC) ? 'C' : 'c'
				, (_blitter.freeformTest.flags & DrawFlag::HRZ) ? 'H' : 'h'
				, (_blitter.freeformTest.flags & DrawFlag::VRT) ? 'V' : 'v'
				, (_blitter.freeformTest.flags & DrawFlag::ROT) ? 'R' : 'r'
				);
			const char* activeControl = "";
			switch (_blitter.freeformTest.activeControl)
			{
				case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
					activeControl = "Pos";
					break;
				case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
					activeControl = "Pivot";
					break;
				case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
					activeControl = "Scale";
					break;
				case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
					activeControl = "Shear";
					break;
				case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
					activeControl = "Rotate";
					break;
			}
			
			// fill in values and update title
			char title[512];
			sprintf(title,
				"GB VT | Mode %c | FPS %04d | %s | Postproc: %s | %s set, %s test, %d draws | Freeform { flags %s; control %s; pos(%.2f|%.2f|%.2f) pivot(%.2f|%.2f) scale(%.2f|%.2f) shear(%.2f|%.2f) rot(%.3f) }"
				, (_blitter.drawMode == CONTROL::INPUT_NAME::DRAW_MODE_DEFERRED) ? 'D' : 'I'
				, framerate
				, pipelineStage
				, postproc
				, testSet
				, test
				, _blitter.numDraws
				, flags
				, activeControl
				, _blitter.freeformTest.positionX
				, _blitter.freeformTest.positionY
				, _blitter.freeformTest.positionZ
				, _blitter.freeformTest.pivotX
				, _blitter.freeformTest.pivotY
				, _blitter.freeformTest.scaleX
				, _blitter.freeformTest.scaleY
				, _blitter.freeformTest.shearX
				, _blitter.freeformTest.shearY
				, _blitter.freeformTest.rotation
			);
			_window.gWindow.SetWindowName(title);
		}
		void Cleanup()
		{
			delete[] outputColors;
			delete[] outputLayers;
			delete[] outputStencils;
			delete[] displayColors;
		}
	} output;

	window.Init();
	blitter.Init();
	output.Init();
	control.Init(window.gWindow);

	// drawing loop
	bool running = true;
	bool windowFocused = false;
	while (running && +window.gWindow.ProcessWindowEvents())
	{
		window.gWindow.IsFocus(windowFocused);
		if (windowFocused)
		{
			static unsigned long long tickCount = 0; tickCount = blitter_GetTickCountMs();
			static unsigned long long tickCountPrev = 0;

			// process inputs
			control.ReadInputStates();
			for (unsigned int i = 0; i < CONTROL::INPUT_NAME::COUNT; ++i)
			{
				// test if input is active
				if (control.inputStates.test(i))
				{
					// respond to input if it is not on cooldown
					CONTROL::Input& input = control.inputs[i];
					if (tickCount - input.lastInputFrame > input.cooldownMs)
					{
						input.lastInputFrame = tickCount;
						float* speed = nullptr;
						const float* speedAdjust = nullptr;
						switch (i)
						{
							// exit
							case CONTROL::INPUT_NAME::EXIT:
								running = false;
								break;

							// scaling
							case CONTROL::INPUT_NAME::SCALE_1:
								window.updateFlags = window.alignFlags;
								break;
							case CONTROL::INPUT_NAME::SCALE_2:
								window.updateFlags = window.alignFlags | GW::GRAPHICS::UPSCALE_2X;
								break;
							case CONTROL::INPUT_NAME::SCALE_4:
								window.updateFlags = window.alignFlags | GW::GRAPHICS::UPSCALE_4X;
								break;
							case CONTROL::INPUT_NAME::SCALE_8:
								window.updateFlags = window.alignFlags | GW::GRAPHICS::UPSCALE_8X;
								break;
							case CONTROL::INPUT_NAME::SCALE_16:
								window.updateFlags = window.alignFlags | GW::GRAPHICS::UPSCALE_16X;
								break;

							// pan view
							case CONTROL::INPUT_NAME::PAN_LEFT:
								if (output.panH >= int(output.panAmount))
									output.panH -= output.panAmount;
								break;
							case CONTROL::INPUT_NAME::PAN_RIGHT:
								if (output.panH < int(blitter.w - output.panAmount))
									output.panH += output.panAmount;
								break;
							case CONTROL::INPUT_NAME::PAN_UP:
								if (output.panV >= int(output.panAmount))
									output.panV -= output.panAmount;
								break;
							case CONTROL::INPUT_NAME::PAN_DOWN:
								if (output.panV < int(blitter.h - output.panAmount))
									output.panV += output.panAmount;
								break;

							// blitter drawing mode to use
							case CONTROL::INPUT_NAME::DRAW_MODE_DEFERRED:
								blitter.drawMode = CONTROL::INPUT_NAME::DRAW_MODE_DEFERRED;
								break;
							case CONTROL::INPUT_NAME::DRAW_MODE_IMMEDIATE:
								blitter.drawMode = CONTROL::INPUT_NAME::DRAW_MODE_IMMEDIATE;
								break;

							// post processing toggles
							case CONTROL::INPUT_NAME::TOGGLE_LAYER_POST_PROCESSING:
								output.layerPostProcess = !output.layerPostProcess;
								break;
							case CONTROL::INPUT_NAME::TOGGLE_STENCIL_POST_PROCESSING:
								output.stencilPostProcess = !output.stencilPostProcess;
								break;

							// pipeline stage to display
							case CONTROL::INPUT_NAME::PIPELINE_STAGE_PROCEDURAL_SOURCES:
								output.pipelineStageToDisplay = OUTPUT::PIPELINE_STAGE::PROCEDURAL_SOURCES;
								break;
							case CONTROL::INPUT_NAME::PIPELINE_STAGE_RESULT:
								output.pipelineStageToDisplay = OUTPUT::PIPELINE_STAGE::BLITTER_RESULT;
								break;

							// procedural source to display
							case CONTROL::INPUT_NAME::PREV_PROCEDURAL_SOURCE:
								if (output.proceduralSourceToDisplay > 0)
									--output.proceduralSourceToDisplay;
								break;
							case CONTROL::INPUT_NAME::NEXT_PROCEDURAL_SOURCE:
								if (output.proceduralSourceToDisplay < BLITTER::SOURCE_NAME::PROCEDURAL_COUNT - 1)
									++output.proceduralSourceToDisplay;
								break;

							// set of instructions to draw
							case CONTROL::INPUT_NAME::PREV_TEST_SET:
								if (blitter.testSetToDraw > BLITTER::TEST_SET::FIRST_SET)
									blitter.testSetToDraw = static_cast<BLITTER::TEST_SET>(static_cast<int>(blitter.testSetToDraw) - 1);
								break;
							case CONTROL::INPUT_NAME::NEXT_TEST_SET:
								if (blitter.testSetToDraw < BLITTER::TEST_SET::LAST_SET)
									blitter.testSetToDraw = static_cast<BLITTER::TEST_SET>(static_cast<int>(blitter.testSetToDraw) + 1);
								break;

							// test to draw
							case CONTROL::INPUT_NAME::PREV_TEST:
								if (blitter.presetTests.testToDraw > 0)
									--blitter.presetTests.testToDraw;
								break;
							case CONTROL::INPUT_NAME::NEXT_TEST:
								if (blitter.presetTests.testToDraw < blitter.presetTests.tests.size() - 1)
									++blitter.presetTests.testToDraw;
								break;

							// dynamic test controls
							case CONTROL::INPUT_NAME::FREEFORM_TILE_MINUS:
								if (blitter.freeformTest.tileIndex > 0)
									--blitter.freeformTest.tileIndex;
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TILE_PLUS:
								if (blitter.freeformTest.tileIndex < BLITTER::TILE_NAME::COUNT - 1)
									++blitter.freeformTest.tileIndex;
								blitter.freeformTest.update = true;
								break;

							case CONTROL::INPUT_NAME::FREEFORM_CONTROL_POSITION:
								blitter.freeformTest.activeControl = BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION;
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_CONTROL_PIVOT:
								blitter.freeformTest.activeControl = BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT;
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_CONTROL_SCALE:
								blitter.freeformTest.activeControl = BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE;
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_CONTROL_SHEAR:
								blitter.freeformTest.activeControl = BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR;
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_CONTROL_ROTATION:
								blitter.freeformTest.activeControl = BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION;
								blitter.freeformTest.update = true;
								break;

							case CONTROL::INPUT_NAME::FREEFORM_X_MINUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionX -= blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										blitter.freeformTest.pivotX -= blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										blitter.freeformTest.scaleX -= blitter.freeformTest.speedScale;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										blitter.freeformTest.shearX -= blitter.freeformTest.speedShear;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										blitter.freeformTest.rotation -= blitter.freeformTest.speedRotate;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_X_PLUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionX += blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										blitter.freeformTest.pivotX += blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										blitter.freeformTest.scaleX += blitter.freeformTest.speedScale;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										blitter.freeformTest.shearX += blitter.freeformTest.speedShear;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										blitter.freeformTest.rotation += blitter.freeformTest.speedRotate;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_Y_MINUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionY -= blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										blitter.freeformTest.pivotY -= blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										blitter.freeformTest.scaleY -= blitter.freeformTest.speedScale;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										blitter.freeformTest.shearY -= blitter.freeformTest.speedShear;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_Y_PLUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionY += blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										blitter.freeformTest.pivotY += blitter.freeformTest.speedXY;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										blitter.freeformTest.scaleY += blitter.freeformTest.speedScale;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										blitter.freeformTest.shearY += blitter.freeformTest.speedShear;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_Z_MINUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionZ -= blitter.freeformTest.speedZ;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_Z_PLUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
										blitter.freeformTest.positionZ += blitter.freeformTest.speedZ;
										break;
									default: break;
								}
								blitter.freeformTest.update = true;
								break;

							case CONTROL::INPUT_NAME::FREEFORM_ADJUST_SPEED_GROSS_MINUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										speed = &blitter.freeformTest.speedXY;
										speedAdjust = &blitter.freeformTest.speedXY_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										speed = &blitter.freeformTest.speedScale;
										speedAdjust = &blitter.freeformTest.speedScale_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										speed = &blitter.freeformTest.speedShear;
										speedAdjust = &blitter.freeformTest.speedShear_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										speed = &blitter.freeformTest.speedRotate;
										speedAdjust = &blitter.freeformTest.speedRotate_adjust_gross;
										break;
								}
								*speed = G_CLAMP_MIN(*speed - *speedAdjust, 0);
								break;
							case CONTROL::INPUT_NAME::FREEFORM_ADJUST_SPEED_GROSS_PLUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										speed = &blitter.freeformTest.speedXY;
										speedAdjust = &blitter.freeformTest.speedXY_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										speed = &blitter.freeformTest.speedScale;
										speedAdjust = &blitter.freeformTest.speedScale_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										speed = &blitter.freeformTest.speedShear;
										speedAdjust = &blitter.freeformTest.speedShear_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										speed = &blitter.freeformTest.speedRotate;
										speedAdjust = &blitter.freeformTest.speedRotate_adjust_gross;
										break;
								}
								*speed += *speedAdjust;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_ADJUST_SPEED_FINE_MINUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										speed = &blitter.freeformTest.speedXY;
										speedAdjust = &blitter.freeformTest.speedXY_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										speed = &blitter.freeformTest.speedScale;
										speedAdjust = &blitter.freeformTest.speedScale_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										speed = &blitter.freeformTest.speedShear;
										speedAdjust = &blitter.freeformTest.speedShear_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										speed = &blitter.freeformTest.speedRotate;
										speedAdjust = &blitter.freeformTest.speedRotate_adjust_gross;
										break;
								}
								*speed = G_CLAMP_MIN(*speed - *speedAdjust, 0);
								break;
							case CONTROL::INPUT_NAME::FREEFORM_ADJUST_SPEED_FINE_PLUS:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										speed = &blitter.freeformTest.speedXY;
										speedAdjust = &blitter.freeformTest.speedXY_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										speed = &blitter.freeformTest.speedScale;
										speedAdjust = &blitter.freeformTest.speedScale_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										speed = &blitter.freeformTest.speedShear;
										speedAdjust = &blitter.freeformTest.speedShear_adjust_gross;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										speed = &blitter.freeformTest.speedRotate;
										speedAdjust = &blitter.freeformTest.speedRotate_adjust_gross;
										break;
								}
								*speed += *speedAdjust;
								break;

							case CONTROL::INPUT_NAME::FREEFORM_RESET_AXES:
								blitter.ResetFreeformTestActiveControl();
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_RESET_SPEED:
								switch (blitter.freeformTest.activeControl)
								{
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::POSITION:
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::PIVOT:
										blitter.freeformTest.speedXY = blitter.freeformTest.speedXY_default;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SCALE:
										blitter.freeformTest.speedScale = blitter.freeformTest.speedScale_default;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::SHEAR:
										blitter.freeformTest.speedShear = blitter.freeformTest.speedShear_default;
										break;
									case BLITTER::FREEFORM_TEST::ACTIVE_CONTROL::ROTATION:
										blitter.freeformTest.speedRotate = blitter.freeformTest.speedRotate_default;
										break;
								}
								break;

							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_ROTATE:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::ROT);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_MIRROR_V:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::VRT);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_MIRROR_H:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::HRZ);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_IGNORE_COLORS:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::ISC);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_USE_LAYERS:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::USL);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_USE_STENCILS:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::USS);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_BITMASKING:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::MSK);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_TRANSFORMATION:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::TSF);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_TRANSPARENCY:
								ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::TSP);
								blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_9:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_9]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_10:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_10]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_11:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_11]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_12:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_12]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_13:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_13]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_14:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_14]);
								//blitter.freeformTest.update = true;
								break;
							case CONTROL::INPUT_NAME::FREEFORM_TOGGLE_FLAG_15:
								//ToggleFlagBitsWithMask(blitter.freeformTest.flags, DrawFlag::[flag_15]);
								//blitter.freeformTest.update = true;
								break;
							default:
								break;
						}; // end switch (i)
					} // end respond to input
				} // end test if input is active
			} // end process inputs

			// clear window
			gr = window.gRasterSurface.Clear(0x00000000);
			// update tests and draw instructions to blitter
			switch (blitter.testSetToDraw)
			{
				case BLITTER::TEST_SET::FREEFORM_TEST:
					if (blitter.freeformTest.update)
						blitter.UpdateFreeformTest();
					break;
			}
			gr = blitter.DrawInstructionsToCanvas();
			// export blitter result to draw to screen
			gr = blitter.gBlitter.ExportResult(true, output.w, output.h, 0, 0, output.outputColors, output.outputLayers, output.outputStencils);
			// draw output to screen
			gr = output.DrawToScreen(window, blitter);

			// update framerate
			static unsigned long long frameCount = 0; ++frameCount;
			static unsigned long long frameCountPrev = 0;
			if (tickCount - tickCountPrev > 1000) // only update every second
			{
				// calculate current framerate
				output.framerate = static_cast<int>(frameCount - frameCountPrev);
				// store frame and tick counts from this framerate update
				frameCountPrev = frameCount;
				tickCountPrev = tickCount;
			}

			// update window title with current information
			output.UpdateWindowTitle(window, blitter);
		}
		else
			control.ResetInputStates();
	} // end drawing loop

	output.Cleanup();
	blitter.Cleanup();
	window.Cleanup();
}
#endif // GBLITTER_TEST_VISUAL


#if defined(_WIN32) && !defined(NDEBUG) && defined(GBLITTER_TEST_VERBOSE_NEW)
#undef new
#endif

#if defined(GBLITTER_TEST_VERBOSE_NEW)
#undef GBLITTER_TEST_VERBOSE_NEW
#endif

#endif // defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GBLITTER)

