#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GOPENGLSURFACE)
#if defined(_WIN32) || defined(__linux__)

bool EnableOpenGLTests = false;
TEST_CASE("Check for OpenGL hardware and Window support", "[Graphics]")
{
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GOpenGLSurface oglSurface;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		if (oglSurface.Create(window, 0) != GW::GReturn::HARDWARE_UNAVAILABLE)
		{
			EnableOpenGLTests = true;
			std::cout << "OPENGL HARDWARE DETECTED: RUNNING ALL OGL UNIT TESTS" << std::endl;
		}
		else
			std::cout << "WARNING: ADEQUATE OPENGL HARDWARE(3.0+) NOT DETECTED; SKIPPING ALL OGL UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL OGL UNIT TESTS" << std::endl;
}

TEST_CASE("GOpenGLSurface core method test battery", "[Graphics]")
{
	if (EnableOpenGLTests == false)
	{
		std::cout << "OpenGL/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	PFNGLCREATESHADERPROC glCreateShader = nullptr;

	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	
	GW::GRAPHICS::GOpenGLSurface oglSurface;

	GW::SYSTEM::GWindow window;
	GW::GEvent gEvent;
	GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;
	float aspectRatio = 0.0f;
#if defined(_WIN32)
	HGLRC* context;
#else
	GLXContext context;
#endif

	SECTION("Testing empty proxy method calls")
	{
		// GEventResponderInterface
		REQUIRE(oglSurface.Invoke() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.Invoke(gEvent) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.Assign([]() {}) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.Assign([](const GW::GEvent&) {}) == GW::GReturn::EMPTY_PROXY);
		
		// GOpenGLSurface 
		REQUIRE(oglSurface.GetAspectRatio(aspectRatio) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.GetContext((void**)&context) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.UniversalSwapBuffers() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.QueryExtensionFunction(nullptr, "glCreateShader", (void**)&glCreateShader) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(oglSurface.EnableSwapControl(false) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(oglSurface.Create(window, initMask) == GW::GReturn::SUCCESS);
		oglSurface = nullptr;
		window = nullptr;
		REQUIRE(!oglSurface);
		REQUIRE(!window);
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(oglSurface.Create(window, initMask) == GW::GReturn::SUCCESS);

		REQUIRE(gEvent.Write(GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED, windowEventData) == GW::GReturn::SUCCESS);

		// GEventResponderInterface
		REQUIRE(+oglSurface.Invoke());
		REQUIRE(+oglSurface.Invoke(gEvent));
		REQUIRE(+oglSurface.Assign([]() {}));
		REQUIRE(+oglSurface.Assign([](const GW::GEvent&) {}));
		REQUIRE(+oglSurface.GetAspectRatio(aspectRatio));
		REQUIRE(+oglSurface.GetContext((void**)&context));
		REQUIRE(+oglSurface.UniversalSwapBuffers());
		REQUIRE(+oglSurface.QueryExtensionFunction(nullptr, "glCreateShader", (void**)&glCreateShader));
		REQUIRE(+oglSurface.EnableSwapControl(false));
	}
}

TEST_CASE("Querying OGLSurface Information.", "[GetContext], [Graphics]")
{
	if (EnableOpenGLTests == false)
	{
		std::cout << "OpenGL/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	
	GW::GRAPHICS::GOpenGLSurface oglSurface;

	GW::SYSTEM::GWindow window;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(oglSurface.Create(window, initMask) == GW::GReturn::SUCCESS);

#if defined(_WIN32)
	HGLRC* context;
#else
	GLXContext context;
#endif

#if defined(_WIN32)
	std::cout << "\n" << "OPENGL INFORMATION" << std::endl;
	std::cout << "OPENGL VERSION: " << (char*)glGetString(GL_VERSION) << "\n";

	REQUIRE(oglSurface.GetContext((void**)&context) == GW::GReturn::SUCCESS);
#elif defined(__linux__)
	GLint red, green, blue, alpha, depth, stencil;

	glGetIntegerv(GL_RED_BITS, &red);
	glGetIntegerv(GL_GREEN_BITS, &green);
	glGetIntegerv(GL_BLUE_BITS, &blue);
	glGetIntegerv(GL_ALPHA_BITS, &alpha);
	glGetIntegerv(GL_DEPTH_BITS, &depth);
	glGetIntegerv(GL_STENCIL_BITS, &stencil);

	printf("%s \n", "OPENGL INFORMATON");
	printf("%s %s \n", "OPENGL VERSION: ", (char*)glGetString(GL_VERSION));

	printf("RED BITS: %d \n", red);
	printf("GREEN BITS: %d \n", green);
	printf("BLUE BITS: %d \n", blue);
	printf("ALPHA BITS: %d \n", alpha);
	printf("DEPTH BITS: %d \n", depth);
	printf("STENCIL BITS: %d \n", stencil);

	REQUIRE(oglSurface.GetContext((void**)&context) == GW::GReturn::SUCCESS);
#endif
}

TEST_CASE("Testing OGLSurface Events", "[Graphics]")
{
	if (EnableOpenGLTests == false)
	{
		std::cout << "OpenGL/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	unsigned long long initMask = 0;
	initMask |= GW::GRAPHICS::DEPTH_BUFFER_SUPPORT;
	initMask |= GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;
	
	GW::GRAPHICS::GOpenGLSurface oglSurface;

	GW::SYSTEM::GWindow window;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(oglSurface.Create(window, initMask) == GW::GReturn::SUCCESS);

	unsigned int clientX, clientY, width, height;
	window.GetClientTopLeft(clientX, clientY);
	window.GetWidth(width);
	window.GetHeight(height);
#if defined(_WIN32)
	// Test OGL Functions with current settings.
	glViewport(clientX, clientY, width, height);
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	oglSurface.UniversalSwapBuffers();
#elif defined(__linux__)
	// Test OGL Functions with current settings.
	glViewport(clientX, clientY, width, height);
	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	oglSurface.UniversalSwapBuffers();
#endif
}

#if !defined(DISABLE_USER_INPUT_TESTS)
TEST_CASE("GOpenGLSurface DEMO Unit Test", "[Graphics]")
{
	if (EnableOpenGLTests == false)
	{
		std::cout << "OpenGL/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	GW::SYSTEM::GWindow window;
	window.Create(500, 500, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	GW::GRAPHICS::GOpenGLSurface surface;
	surface.Create(window, 0);

	static float t = 0;
	std::cout << "OPENGL DEMO BEGIN\n";
	std::cout << "CLOSE THE WINDOW TO STOP THE EPILEPSY AND CONTINUE THE TEST\n";
	std::cout << "(NOTE: Closing the demo will cause some freezes to VM, this is normal behavior), this is because GWindow Test is runnning\n";
	while (+window.ProcessWindowEvents())
	{
		unsigned int clientX, clientY, width, height;
		window.GetClientTopLeft(clientX, clientY);
		window.GetWidth(width);
		window.GetHeight(height);
		t += 0.01f;

		glViewport(clientX, clientY, width, height);
		glClearColor(sinf(t), cosf(t), tanf(t), 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		surface.UniversalSwapBuffers();
	}
	std::cout << "OPENGL DEMO EXITED\n";
}
#endif /* DISABLE_USER_INPUT_TESTS */ 
#endif /* Win32 || Linux */
#endif /* defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GOPENGLSURFACE) */