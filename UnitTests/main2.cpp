// This file is used to test the API being included into multiple translation units.

#include "API.h" // Unit tests don't need to work across translation units but the API does.