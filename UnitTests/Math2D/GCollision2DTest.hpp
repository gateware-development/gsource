#if defined(GATEWARE_ENABLE_MATH2D) && !defined(GATEWARE_DISABLE_GCOLLISION2D)

#include <chrono>

TEST_CASE("Create GCollision2D.", "[CreateGCollision2D], [Math2D]")
{
	GW::MATH2D::GCollision2D Collision2DC;

	//Pass cases
	REQUIRE(+(Collision2DC.Create()));
}

TEST_CASE("do the implicit line equation with a point and a line", "[ImplicitLineEquationF], [ImplicitLineEquationD], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GLINE2F lineF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GLINE2D lineD = {};

	float resultF;
	double resultD;

	SECTION("Float above line", "[ImplicitLineEquationF]")
	{
		pointF = { 1.0f, 2.0f };
		lineF = { 0.0f, 0.0f, 2.0f, 0.0f };

		resultF = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationF(pointF, lineF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 4.0f));
	}

	SECTION("Double above line", "[ImplicitLineEquationD]")
	{
		pointD = { 1.0, 2.0 };
		lineD = { 0.0, 0.0, 2.0, 0.0 };

		resultD = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationD(pointD, lineD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 4.0));
	}

	SECTION("Float below line", "[ImplicitLineEquationF]")
	{
		pointF = { 1.0f, -2.0f };
		lineF = { 0.0f, 0.0f, 2.0f, 0.0f };

		resultF = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationF(pointF, lineF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, -4.0f));
	}

	SECTION("Double below line", "[ImplicitLineEquationD]")
	{
		pointD = { 1.0, -2.0 };
		lineD = { 0.0, 0.0, 2.0, 0.0 };

		resultD = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationD(pointD, lineD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, -4.0));
	}

	SECTION("Float on line", "[ImplicitLineEquationF]")
	{
		pointF = { 1.0f, 0.0f };
		lineF = { 0.0f, 0.0f, 2.0f, 0.0f };

		resultF = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationF(pointF, lineF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 0.0f));
	}

	SECTION("Double on line", "[ImplicitLineEquationD]")
	{
		pointD = { 1.0, 0.0 };
		lineD = { 0.0, 0.0, 2.0, 0.0 };

		resultD = -10;

		CHECK(+(GW::I::GCollision2DImplementation::ImplicitLineEquationD(pointD, lineD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 0.0));
	}
}

TEST_CASE("squared distance from a point to a line", "[SqDistancePointToLine2F], [SqDistancePointToLine2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GLINE2F lineF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GLINE2D lineD = {};

	float distanceF;
	double distanceD;

	SECTION("F point above line", "[SqDistancePointToLine2F]")
	{
		pointF = { -2.0f, 0.0f };
		lineF = { 0.0f, -2.0f, 0.0f, 2.0f };

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToLine2F(pointF, lineF, distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 4.0f));
	}

	SECTION("D point above line", "[SqDistancePointToLine2D]")
	{
		pointD = { -2.0, 0.0 };
		lineD = { 0.0, -2.0, 0.0, 2.0 };

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToLine2D(pointD, lineD, distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 4.0));
	}

	SECTION("F point below line", "[SqDistancePointToLine2F]")
	{
		pointF = { 2.0f, 0.0f };
		lineF = { 0.0f, -2.0f, 0.0f, 2.0f };

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToLine2F(pointF, lineF, distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 4.0f));
	}

	SECTION("D point below line", "[SqDistancePointToLine2D]")
	{
		pointD = { 2.0, 0.0 };
		lineD = { 0.0, -2.0, 0.0, 2.0 };

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToLine2D(pointD, lineD, distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 4.0));
	}
}

TEST_CASE("squared distance from a point to a rectangle", "[SqDistancePointToRectangle2F], [SqDistancePointToRectangle2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GRECTANGLE2F rectangleF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GRECTANGLE2D rectangleD = {};

	float distanceF;
	double distanceD;

	SECTION("F point to rectangle", "[SqDistancePointToRectangle2F]")
	{
		pointF = { -1.0f, 3.0f };
		rectangleF = { 1.0f, 1.0f, 3.0f, 5.0f };

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToRectangle2F(pointF, rectangleF, distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 4.0f));
	}

	SECTION("D point to rectangle", "[SqDistancePointToRectangle2D]")
	{
		pointD = { -1.0, 3.0 };
		rectangleD = { 1.0, 1.0, 3.0, 5.0 };

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToRectangle2D(pointD, rectangleD, distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 4.0));
	}

	SECTION("F point is inside rectangle", "[SqDistancePointToRectangle2F]")
	{
		pointF = { 2.0f, 3.0f };
		rectangleF = { 1.0f, 1.0f, 3.0f, 5.0f };

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToRectangle2F(pointF, rectangleF, distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 0.0f));
	}

	SECTION("D point is inside rectangle", "[SqDistancePointToRectangle2D]")
	{
		pointD = { 2.0, 3.0 };
		rectangleD = { 1.0, 1.0, 3.0, 5.0 };

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToRectangle2D(pointD, rectangleD, distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 0.0));
	}
}

TEST_CASE("squared distance from a point to a polygon", "[SqDistancePointToPolygon2F], [SqDistancePointToPolygon2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	std::vector<GW::MATH2D::GVECTOR2F> polygonF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	std::vector<GW::MATH2D::GVECTOR2D> polygonD = {};

	float distanceF;
	double distanceD;

	SECTION("F point to polygon", "[SqDistancePointToPolygon2F]")
	{
		pointF = { 5.0f, 2.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToPolygon2F(pointF, polygonF.data(), polygonF.size(), distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 4.0f));
	}

	SECTION("D point to polygon", "[SqDistancePointToPolygon2D]")
	{
		pointD = { 5.0, 2.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToPolygon2D(pointD, polygonD.data(), polygonD.size(), distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 4.0));
	}

	SECTION("F point is inside polygon", "[SqDistancePointToPolygon2F]")
	{
		pointF = { 0.0f, 0.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		distanceF = -1.0f;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToPolygon2F(pointF, polygonF.data(), polygonF.size(), distanceF)));
		CHECK(G2D_COMPARISON_STANDARD_F(distanceF, 0.0f));
	}

	SECTION("D point is inside polygon", "[SqDistancePointToPolygon2D]")
	{
		pointD = { 0.0, 0.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		distanceD = -1.0;

		CHECK(+(GW::I::GCollision2DImplementation::SqDistancePointToPolygon2D(pointD, polygonD.data(), polygonD.size(), distanceD)));
		CHECK(G2D_COMPARISON_STANDARD_D(distanceD, 0.0));
	}
}

TEST_CASE("point to line collision check", "[TestPointToLine2F], [TestPointToLine2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GLINE2F lineF = {};


	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GLINE2D lineD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F point not on line but is inline with line. NO_COLLISION", "[TestPointToLine2F], [Math2D]")
	{
		lineF.start = { 0.0f, 0.0f };
		lineF.end = { 0.0f, 1.0f };

		pointF = { 0.0f, 2.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D point not on line but is inline with line. NO_COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 0.0, 0.0 };
		lineD.end = { 0.0, 1.0 };

		pointD = { 0.0, 2.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F point above line relative to line normal. ABOVE", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 5.0f, 5.0f };

		pointF = { 3.0f, 5.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::ABOVE);
	}

	SECTION("D point above line relative to line normal. ABOVE", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 5.0, 5.0 };

		pointD = { 3.0, 5.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::ABOVE);
	}

	SECTION("F point above line relative to negative line normal. ABOVE", "[TestPointToLine2F]")
	{
		lineF.start = { -1.0f, -1.0f };
		lineF.end = { -5.0f, -5.0f };

		pointF = { -3.0f, -5.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::ABOVE);
	}

	SECTION("D point above line relative to negative line normal. ABOVE", "[TestPointToLine2D]")
	{
		lineD.start = { -1.0, -1.0 };
		lineD.end = { -5.0, -5.0 };

		pointD = { -3.0, -5.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::ABOVE);
	}

	SECTION("F point below line relative to line normal. BELOW", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 5.0f, 5.0f };

		pointF = { 3.0f, -5.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::BELOW);
	}

	SECTION("D point below line relative to line normal. BELOW", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 5.0, 5.0 };

		pointD = { 3.0, -5.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::BELOW);
	}

	SECTION("F point below line relative to negative line normal. BELOW", "[TestPointToLine2F]")
	{
		lineF.start = { -1.0f, -1.0f };
		lineF.end = { -5.0f, -5.0f };

		pointF = { -3.0f, 5.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::BELOW);
	}

	SECTION("D point below line relative to negative line normal. BELOW", "[TestPointToLine2D]")
	{
		lineD.start = { -1.0, -1.0 };
		lineD.end = { -5.0, -5.0 };

		pointD = { -3.0, 5.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::BELOW);
	}

	SECTION("F point is line start. COLLISION", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 5.0f, 5.0f };

		pointF = { 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is line start. COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 5.0, 5.0 };

		pointD = { 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is line end. COLLISION", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 5.0f, 5.0f };

		pointF = { 5.0f, 5.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is line end. COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 5.0, 5.0 };

		pointD = { 5.0, 5.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is on line. COLLISION", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 5.0f, 5.0f };

		pointF = { 3.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is on line. COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 5.0, 5.0 };

		pointD = { 3.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line degenerates to a point. NO_COLLISION", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 1.0f, 1.0f };

		pointF = { 3.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D line degenerates to a point. NO_COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 1.0, 1.0 };

		pointD = { 3.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F line degenerates to a point. COLLISION", "[TestPointToLine2F]")
	{
		lineF.start = { 1.0f, 1.0f };
		lineF.end = { 1.0f, 1.0f };

		pointF = { 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2F(pointF, lineF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line degenerates to a point. COLLISION", "[TestPointToLine2D]")
	{
		lineD.start = { 1.0, 1.0 };
		lineD.end = { 1.0, 1.0 };

		pointD = { 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToLine2D(pointD, lineD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("point to circle collision check", "[TestPointToCircle2F], [TestPointToCircle2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GCIRCLE2F circleF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GCIRCLE2D circleD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F point is not in circle. NO_COLLISION", "[TestPointToCircle2F]")
	{
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		pointF = { -1.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2F(pointF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D point is not in circle. NO_COLLISION", "[TestPointToCircle2D]")
	{
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = 1.0f;

		pointD = { -1.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2D(pointD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F point is in circle. COLLISION", "[TestPointToCircle2F]")
	{
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		pointF = { 1.0f, 0.5f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2F(pointF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is in circle. COLLISION", "[TestPointToCircle2D]")
	{
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = 1.0f;

		pointD = { 1.0, 0.5 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2D(pointD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is on edge of circle. COLLISION", "[TestPointToCircle2F]")
	{
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		pointF = { 1.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2F(pointF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is on edge of circle. COLLISION", "[TestPointToCircle2D]")
	{
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = 1.0f;

		pointD = { 1.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2D(pointD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is at center of circle. COLLISION", "[TestPointToCircle2F]")
	{
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		pointF = circleF.pos;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2F(pointF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is at center of circle. COLLISION", "[TestPointToCircle2D]")
	{
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = 1.0f;

		pointD = circleD.pos;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToCircle2D(pointD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("point to rectangle collision check", "[TestPointToRectangle2F], [TestPointToRectangle2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	GW::MATH2D::GRECTANGLE2F rectangleF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	GW::MATH2D::GRECTANGLE2D rectangleD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F point is not within rectangle. NO_COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = { 3.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D point is not within rectangle. NO_COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = { 3.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F point is within rectangle. COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = { 0.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is within rectangle. COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = { 0.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is min point of rectangle. COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = rectangleF.min;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is min point of rectangle. COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = rectangleD.min;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is max point of rectangle. COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = rectangleF.max;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is max point of rectangle. COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = rectangleD.max;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is inferred corner of rectangle. COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = { -1.0f, 2.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is inferred corner of rectangle. COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = { -1.0, 2.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is on the edge of rectangle. COLLISION", "[TestPointToRectangle2F]")
	{
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 1.0f, 2.0f };

		pointF = { -1.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2F(pointF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is on the edge of rectangle. COLLISION", "[TestPointToRectangle2D]")
	{
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 1.0, 2.0 };

		pointD = { -1.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToRectangle2D(pointD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("point to polygon collision check", "[TestPointToPolygon2F], [TestPointToPolygon2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	std::vector<GW::MATH2D::GVECTOR2F> polygonF;

	GW::MATH2D::GVECTOR2D pointD = {};
	std::vector<GW::MATH2D::GVECTOR2D> polygonD;

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F point is not within polygon. NO_COLLISION", "[TestPointToPolygon2F]")
	{
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		pointF = { -3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2F(pointF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D point is not within polygon. NO_COLLISION", "[TestPointToPolygon2D]")
	{
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		pointD = { -3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2D(pointD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F point is within polygon. COLLISION", "[TestPointToPolygon2F]")
	{
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		pointF = { 0.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2F(pointF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is within polygon. COLLISION", "[TestPointToPolygon2D]")
	{
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		pointD = { 0.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2D(pointD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is on edge of polygon. COLLISION", "[TestPointToPolygon2F]")
	{
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		pointF = { 1.0f, -2.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2F(pointF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is on edge of polygon. COLLISION", "[TestPointToPolygon2D]")
	{
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		pointD = { 1.0, -2.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2D(pointD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is a vertex of polygon. COLLISION", "[TestPointToPolygon2F]")
	{
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		pointF = polygonF[4];

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2F(pointF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D point is a vertex of polygon. COLLISION", "[TestPointToPolygon2D]")
	{
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		pointD = polygonD[4];

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2D(pointD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F point is verticaly inline with a vertical edge of polygon. potential devide-by-zero check. NO_COLLISION", "[TestPointToPolygon2F]")
	{
		// create pentagon with a vertical edge
		polygonF.push_back({ 0.0f,  2.0f });
		polygonF.push_back({ 2.0f,  1.0f });
		polygonF.push_back({ 2.0f, -1.0f });
		polygonF.push_back({ 0.0f, -2.0f });
		polygonF.push_back({ -2.0f,  0.0f });

		pointF = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2F(pointF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D point is verticaly inline with a vertical edge of polygon. potential devide-by-zero check. NO_COLLISION", "[TestPointToPolygon2D]")
	{
		// create pentagon with a vertical edge
		polygonD.push_back({ 0.0,  2.0 });
		polygonD.push_back({ 2.0,  1.0 });
		polygonD.push_back({ 2.0, -1.0 });
		polygonD.push_back({ 0.0, -2.0 });
		polygonD.push_back({ -2.0,  0.0 });

		pointD = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPointToPolygon2D(pointD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}
}

TEST_CASE("line to line collision check", "[TestLineToLine2F], [TestLineToLine2D], [Math2D]")
{
	GW::MATH2D::GLINE2F line1F = {};
	GW::MATH2D::GLINE2F line2F = {};

	GW::MATH2D::GLINE2D line1D = {};
	GW::MATH2D::GLINE2D line2D = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F line1 does not collide with line2. NO_COLLISION", "[TestLineToLine2F]")
	{
		line1F = { 0.0f, 0.0f, 2.0f, 0.0f };
		line2F = { -1.0f, 1.0f, 1.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D line1 does not collide with line2. NO_COLLISION", "[TestLineToLine2D]")
	{
		line1D = { 0.0, 0.0, 2.0, 0.0 };
		line2D = { -1.0, 1.0, 1.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F one of line1's points matches one of line2's points. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { 0.0f, 0.0f, 0.0f, 2.0f };
		line2F = { line1F.start, 1.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D one of line1's points matches one of line2's points. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { 0.0, 0.0, 0.0, 2.0 };
		line2D = { line2D.start, 1.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F one of line1's points is on line2. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { 0.0f, 0.0f, 2.0f, 0.0f };
		line2F = { 1.0f, 0.0f, 1.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D one of line1's points is on line2. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { 0.0, 0.0, 2.0, 0.0 };
		line2D = { 1.0, 0.0, 1.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line1 intersects line2. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { -1.0f, 0.0f, 1.0f, 0.0f };
		line2F = { 0.0f, -1.0f, 0.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line1 intersects line2. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { -1.0, 0.0, 1.0, 0.0 };
		line2D = { 0.0, -1.0, 0.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line1 is contained in line2. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { -1.0f, 0.0f, 1.0f, 0.0f };
		line2F = { -2.0f, 0.0f, 2.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line1 is contained in line2. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { -1.0, 0.0, 1.0, 0.0 };
		line2D = { -2.0, 0.0, 2.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line2 is contained in line1. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { -2.0f, 0.0f, 2.0f, 0.0f };
		line2F = { -1.0f, 0.0f, 1.0f, 0.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line2 is contained in line1. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { -2.0, 0.0, 2.0, 0.0 };
		line2D = { -1.0, 0.0, 1.0, 0.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line1 and line2 are the same line. COLLISION", "[TestLineToLine2F]")
	{
		line1F = { -2.0f, 0.0f, 2.0f, 0.0f };
		line2F = line1F;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2F(line1F, line2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line1 and line2 are the same line. COLLISION", "[TestLineToLine2D]")
	{
		line1D = { -2.0, 0.0, 2.0, 0.0 };
		line2D = line1D;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToLine2D(line1D, line2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("line to circle collision check", "[TestLineToCircle2F], [TestLineToCircle2D], [Math2D]")
{
	GW::MATH2D::GLINE2F lineF = {};
	GW::MATH2D::GCIRCLE2F circleF = {};

	GW::MATH2D::GLINE2D lineD = {};
	GW::MATH2D::GCIRCLE2D circleD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F line does not collide with circle. NO_COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { -2.0f, 0.0f, 0.0f, 0.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D line does not collide with circle. NO_COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { -2.0, 0.0, 0.0, 0.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F line intersects circle with both points outside circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { -1.0f, 1.0f, 3.0f, 1.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects circle with both points outside circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { -1.0, 1.0, 3.0, 1.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line intersects circle with one point inside circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { -1.0f, 1.0f, 1.0f, 1.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects circle with one point inside circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { -1.0, 1.0, 1.0, 1.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F one point on line in on the edge of circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { -1.0f, 1.0f, 0.0f, 1.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects circle with one point inside circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { -1.0, 1.0, 0.0, 1.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F both points in line are inside circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { 0.5f, 1.0f, 1.5f, 1.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D both points in line are inside circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { 0.5, 1.0, 1.5, 1.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F both points in line are inside circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { 0.5f, 1.0f, 1.5f, 1.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D both points in line are inside circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { 0.5, 1.0, 1.5, 1.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line is tangent to circle. COLLISION", "[TestLineToCircle2F]")
	{
		lineF = { 0.0f, 0.0f, 2.0f, 0.0f };
		circleF.pos = { 1.0f, 1.0f };
		circleF.radius = 1.0f;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2F(lineF, circleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line is tangent to circle. COLLISION", "[TestLineToCircle2D]")
	{
		lineD = { 0.0, 0.0, 2.0, 0.0 };
		circleD.pos = { 1.0, 1.0 };
		circleD.radius = { 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToCircle2D(lineD, circleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("line to rectangle collision check", "[TestLineToRectangle2F], [TestLineToRectangle2D], [Math2D]")
{
	GW::MATH2D::GLINE2F lineF = {};
	GW::MATH2D::GRECTANGLE2F rectangleF = {};

	GW::MATH2D::GLINE2D lineD = {};
	GW::MATH2D::GRECTANGLE2D rectangleD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F line does not collide with rectangle. NO_COLLISION", "[TestLineToRectangle2F]")
	{
		lineF = { -2.0f, 4.0f, 6.0f, 4.0f };
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2F(lineF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D line does not collide with rectangle. NO_COLLISION", "[TestLineToRectangle2D]")
	{
		lineD = { -2.0, 4.0, 6.0, 4.0 };
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2D(lineD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F line intersects rectangle at two points. COLLISION", "[TestLineToRectangle2F]")
	{
		lineF = { -2.0f, 2.0f, 2.0f, 4.0f };
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2F(lineF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects rectangle at two points. COLLISION", "[TestLineToRectangle2D]")
	{
		lineD = { -2.0, 2.0, 2.0, 4.0 };
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2D(lineD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line intersects rectangle at one point. COLLISION", "[TestLineToRectangle2F]")
	{
		lineF = { -2.0f, 2.0f, 2.0f, 4.0f };
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2F(lineF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects rectangle at one point. COLLISION", "[TestLineToRectangle2D]")
	{
		lineD = { -2.0, 2.0, 2.0, 4.0 };
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2D(lineD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line is contained within rectangle. COLLISION", "[TestLineToRectangle2F]")
	{
		lineF = { 0.0f, 0.0f, 0.0f, 1.0f };
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2F(lineF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line is contained within rectangle. COLLISION", "[TestLineToRectangle2D]")
	{
		lineD = { 0.0, 0.0, 0.0, 1.0 };
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2D(lineD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line overlays one of rectangles sides. COLLISION", "[TestLineToRectangle2F]")
	{
		lineF = { -2.0f, -1.0f, 3.0f, -1.0f };
		rectangleF.min = { -1.0f, -1.0f };
		rectangleF.max = { 2.0f, 3.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2F(lineF, rectangleF, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line overlays one of rectangles sides. COLLISION", "[TestLineToRectangle2D]")
	{
		lineD = { 2.0, -1.0, 3.0, -1.0 };
		rectangleD.min = { -1.0, -1.0 };
		rectangleD.max = { 2.0, 3.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToRectangle2D(lineD, rectangleD, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("line to polygon collision check", "[TestLineToPolygon2F], [TestLineToPolygon2D], [Math2D]")
{
	GW::MATH2D::GLINE2F lineF = {};
	std::vector<GW::MATH2D::GVECTOR2F> polygonF = {};

	GW::MATH2D::GLINE2D lineD = {};
	std::vector<GW::MATH2D::GVECTOR2D> polygonD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F line does not collide with polygon. NO_COLLISION", "[TestLineToPolygon2F]")
	{
		lineF = { -10.0f, -10.0f, 10.0f, -10.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2F(lineF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D line does not collide with circle. NO_COLLISION", "[TestLineToPolygon2D]")
	{
		lineD = { -10.0, -10.0, 10.0, -10.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2D(lineD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F line intersects polygon at two points. COLLISION", "[TestLineToPolygon2F]")
	{
		lineF = { -5.0f, 0.0f, 5.0f, 0.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2F(lineF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects polygon at two points. COLLISION", "[TestLineToPolygon2D]")
	{
		lineD = { -5.0, 0.0, 5.0, 0.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2D(lineD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line intersects polygon at one point. COLLISION", "[TestLineToPolygon2F]")
	{
		lineF = { -5.0f, 0.0f, 0.0f, 0.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2F(lineF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line intersects polygon at one point. COLLISION", "[TestLineToPolygon2D]")
	{
		lineD = { -5.0, 0.0, 0.0, 0.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2D(lineD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line is within polygon. COLLISION", "[TestLineToPolygon2F]")
	{
		lineF = { -1.0f, 0.0f, 1.0f, 0.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2F(lineF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line is within polygon. COLLISION", "[TestLineToPolygon2D]")
	{
		lineD = { -1.0, 0.0, 1.0, 0.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2D(lineD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F line overlays one of polygons sides. COLLISION", "[TestLineToPolygon2F]")
	{
		lineF = { -4.0f, -2.0f, 4.0f, -2.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2F(lineF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D line overlays one of polygons sides. COLLISION", "[TestLineToPolygon2D]")
	{
		lineD = { -4.0, -2.0, 4.0, -2.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestLineToPolygon2D(lineD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("circle to circle collision check", "[TestCircleToCircle2F], [TestCircleToCircle2D], [Math2D]")
{
	GW::MATH2D::GCIRCLE2F circle1F = {};
	GW::MATH2D::GCIRCLE2F circle2F = {};

	GW::MATH2D::GCIRCLE2D circle1D = {};
	GW::MATH2D::GCIRCLE2D circle2D = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F circle1 does not collide with circle2. NO_COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 1.0f, 1.0f, 1.0f };
		circle2F = { -1.0f, -1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D circle1 does not collide with circle2. NO_COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 1.0, 1.0, 1.0 };
		circle2D = { -1.0, -1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F circle1 intersects circle2 at two points. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 1.0f, 1.0f, 1.0f };
		circle2F = { 0.0f, 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle1 intersects circle2 at two points. NCOLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 1.0, 1.0, 1.0 };
		circle2D = { 0.0, 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F circle1 intersects circle2 at one point. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 1.0f, 1.0f, 1.0f };
		circle2F = { -1.0f, 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle1 intersects circle2 at one point. COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 1.0, 1.0, 1.0 };
		circle2D = { -1.0, 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F circle1 intersects circle2 at one point. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 1.0f, 1.0f, 1.0f };
		circle2F = { -1.0f, 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle1 intersects circle2 at one point. COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 1.0, 1.0, 1.0 };
		circle2D = { -1.0, 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F circle1 is inside circle2. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 1.0f, 1.0f, 1.0f };
		circle2F = { 2.0f, 2.0f, 2.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle1 is inside circle2. COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 1.0, 1.0, 1.0 };
		circle2D = { 2.0, 2.0, 2.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F circle2 is inside circle1. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 2.0f, 2.0f, 2.0f };
		circle2F = { 1.0f, 1.0f, 1.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle2 is inside circle1. COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 2.0, 2.0, 2.0 };
		circle2D = { 1.0, 1.0, 1.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F circle1 and circle2 are the same circle. COLLISION", "[TestCircleToCircle2F]")
	{
		circle1F = { 2.0f, 2.0f, 2.0f };
		circle2F = circle1F;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2F(circle1F, circle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D circle1 and circle2 are the same circle. COLLISION", "[TestCircleToCircle2D]")
	{
		circle1D = { 2.0, 2.0, 2.0 };
		circle2D = circle1D;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestCircleToCircle2D(circle1D, circle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("rectangle to rectangle collision check", "[TestRectangleToRectangle2F], [TestRectangleToRectangle2D], [Math2D]")
{
	GW::MATH2D::GRECTANGLE2F rectangle1F = {};
	GW::MATH2D::GRECTANGLE2F rectangle2F = {};

	GW::MATH2D::GRECTANGLE2D rectangle1D = {};
	GW::MATH2D::GRECTANGLE2D rectangle2D = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F rectangle1 does not collide with rectangle2. NO_COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { -2.0f, 1.0f, -1.0f, 3.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D rectangle1 does not collide with rectangle2. NO_COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { -2.0, 1.0, -1.0, 3.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F rectangle1 intersects rectangle2 at two points. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 0.0f, 1.0f, 2.0f, 2.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle1 intersects rectangle2 at two points. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 0.0, 1.0, 2.0, 2.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F two edges of rectangle1 are contained within two edges of rectangle2. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 1.0f, 1.0f, 3.0f, 2.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D two edges of rectangle1 are contained within two edges of rectangle2. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 1.0, 1.0, 3.0, 2.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle1 and rectangle2 share a corner. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 1.0f, 0.0f, -1.0f, -1.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle1 and rectangle2 share a corner. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 1.0, 0.0, -1.0, -1.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle1 is within rectangle2. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 1.5f, 0.5f, 2.5f, 2.5f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle1 is within rectangle2. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 1.5, 0.5, 2.5, 2.5 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle2 is within rectangle1. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 0.0f, -1.0f, 4.0f, 6.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle2 is within rectangle1. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 0.0, -1.0, 4.0, 6.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F one of rectangle1's sides is contained within one of rectangle2's sides. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 0.0f, 1.0f, 1.0f, 3.0f };
		rectangle2F = { 1.0f, 0.0f, 3.0f, 4.0f };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D one of rectangle1's sides is contained within one of rectangle2's sides. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 0.0, 1.0, 1.0, 3.0 };
		rectangle2D = { 1.0, 0.0, 3.0, 4.0 };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle1 and rectangle2 are the same rectangle. COLLISION", "[TestRectangleToRectangle2F]")
	{
		rectangle1F = { 0.0f, 1.0f, 1.0f, 3.0f };
		rectangle2F = rectangle1F;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2F(rectangle1F, rectangle2F, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle1 and rectangle2 are the same rectangle. COLLISION", "[TestRectangleToRectangle2D]")
	{
		rectangle1D = { 0.0, 1.0, 1.0, 3.0 };
		rectangle2D = rectangle1D;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToRectangle2D(rectangle1D, rectangle2D, collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("rectangle to polygon collision check", "[TestRectangleToPolygon2F], [TestRectangleToPolygon2D], [Math2D]")
{
	GW::MATH2D::GRECTANGLE2F rectangleF = {};
	std::vector<GW::MATH2D::GVECTOR2F> polygonF = {};

	GW::MATH2D::GRECTANGLE2D rectangleD = {};
	std::vector<GW::MATH2D::GVECTOR2D> polygonD = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F rectangle does not collide with polygon. NO_COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -7.0f, 3.0f, -3.0f, 5.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D rectangle does not collide with polygon. NO_COLLISION", "[TestRectangleToPolygon2D]")
	{
		// create pentagon
		rectangleD = { -7.0, 3.0, -3.0, 5.0 };
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F rectangle intersects polygon at two points. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -5.0f, 3.0f, -1.0f, 5.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle does not collide with polygon. COLLISION", "[TestRectangleToPolygon2D]")
	{
		// create pentagon
		rectangleD = { -5.0, 3.0, -1.0, 5.0 };
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle intersects polygon at two points. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -5.0f, 3.0f, -1.0f, 5.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle does not collide with polygon. COLLISION", "[TestRectangleToPolygon2D]")
	{
		// create pentagon
		rectangleD = { -5.0, 3.0, -1.0, 5.0 };
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle and polygon share a corner. COLLISION", "[TestRectangleToPolygon2F]")
	{
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		rectangleF = { -5.0f, 3.0f, polygonF[0] };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle does not collide with polygon. COLLISION", "[TestRectangleToPolygon2D]")
	{
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		rectangleD = { -5.0, 3.0, polygonD[0] };

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle intersects polygon at four points. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -4.0f, -1.0f, 4.0f, 1.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle intersects polygon at four points. COLLISION", "[TestRectangleToPolygon2D]")
	{
		rectangleD = { -4.0, -1.0, 4.0, 1.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle is within polygon. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -2.0f, -1.0f, 2.0f, 1.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle is within polygon. COLLISION", "[TestRectangleToPolygon2D]")
	{
		rectangleD = { -2.0, 1.0, 2.0, 1.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F polygon is within rectangle. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -10.0f, -10.0f, 10.0f, 10.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D polygon is within rectangle. COLLISION", "[TestRectangleToPolygon2D]")
	{
		rectangleD = { -10.0, -10.0, 10.0, 10.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F rectangle shares a side with polygon. COLLISION", "[TestRectangleToPolygon2F]")
	{
		rectangleF = { -3.0f, -4.0f, 3.0f, -2.0f };
		// create pentagon
		polygonF.push_back({ 0.0f,  4.0f });
		polygonF.push_back({ 3.0f,  2.0f });
		polygonF.push_back({ 2.0f, -2.0f });
		polygonF.push_back({ -2.0f, -2.0f });
		polygonF.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2F(rectangleF, polygonF.data(), polygonF.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D rectangle shares a side with polygon. COLLISION", "[TestRectangleToPolygon2D]")
	{
		rectangleD = { -3.0, -4.0, 3.0, -2.0 };
		// create pentagon
		polygonD.push_back({ 0.0,  4.0 });
		polygonD.push_back({ 3.0,  2.0 });
		polygonD.push_back({ 2.0, -2.0 });
		polygonD.push_back({ -2.0, -2.0 });
		polygonD.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestRectangleToPolygon2D(rectangleD, polygonD.data(), polygonD.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("polygon to polygon collision check", "[TestPolygonToPolygon2F], [TestPolygonToPolygon2D], [Math2D]")
{
	std::vector<GW::MATH2D::GVECTOR2F> polygon1F = {};
	std::vector<GW::MATH2D::GVECTOR2F> polygon2F = {};

	std::vector<GW::MATH2D::GVECTOR2D> polygon1D = {};
	std::vector<GW::MATH2D::GVECTOR2D> polygon2D = {};

	GW::MATH2D::GCollision2D::GCollisionCheck2D collisionResult;

	SECTION("F polygon1 does not collide with polygon2. NO_COLLISION", "[TestPolygonToPolygon2F]")
	{
		// create pentagon
		polygon1F.push_back({ -6.0f,  2.0f });
		polygon1F.push_back({ -4.0f,  1.0f });
		polygon1F.push_back({ -5.0f, -1.0f });
		polygon1F.push_back({ -7.0f, -1.0f });
		polygon1F.push_back({ -8.0f,  1.0f });
		// create pentagon
		polygon2F.push_back({ 0.0f,  4.0f });
		polygon2F.push_back({ 3.0f,  2.0f });
		polygon2F.push_back({ 2.0f, -2.0f });
		polygon2F.push_back({ -2.0f, -2.0f });
		polygon2F.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2F(polygon1F.data(), polygon1F.size(), polygon2F.data(), polygon2F.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("D polygon1 does not collide with polygon2. NO_COLLISION", "[TestPolygonToPolygon2D]")
	{
		// create pentagon
		polygon1D.push_back({ -6.0,  2.0 });
		polygon1D.push_back({ -4.0,  1.0 });
		polygon1D.push_back({ -5.0, -1.0 });
		polygon1D.push_back({ -7.0, -1.0 });
		polygon1D.push_back({ -8.0,  1.0 });
		// create pentagon
		polygon2D.push_back({ 0.0,  4.0 });
		polygon2D.push_back({ 3.0,  2.0 });
		polygon2D.push_back({ 2.0, -2.0 });
		polygon2D.push_back({ -2.0, -2.0 });
		polygon2D.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2D(polygon1D.data(), polygon1D.size(), polygon2D.data(), polygon2D.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::NO_COLLISION);
	}

	SECTION("F polygon1 intersects polygon2 at one or more points. COLLISION", "[TestPolygonToPolygon2F]")
	{
		// create pentagon
		polygon1F.push_back({ -2.0f,  2.0f });
		polygon1F.push_back({ -4.0f,  1.0f });
		polygon1F.push_back({ -5.0f, -1.0f });
		polygon1F.push_back({ -7.0f, -1.0f });
		polygon1F.push_back({ -8.0f,  1.0f });
		// create pentagon
		polygon2F.push_back({ 0.0f,  4.0f });
		polygon2F.push_back({ 3.0f,  2.0f });
		polygon2F.push_back({ 2.0f, -2.0f });
		polygon2F.push_back({ -2.0f, -2.0f });
		polygon2F.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2F(polygon1F.data(), polygon1F.size(), polygon2F.data(), polygon2F.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D polygon1 intersects polygon2 at one or more points. COLLISION", "[TestPolygonToPolygon2D]")
	{
		// create pentagon
		polygon1D.push_back({ -2.0,  2.0 });
		polygon1D.push_back({ -4.0,  1.0 });
		polygon1D.push_back({ -5.0, -1.0 });
		polygon1D.push_back({ -7.0, -1.0 });
		polygon1D.push_back({ -8.0,  1.0 });
		// create pentagon
		polygon2D.push_back({ 0.0,  4.0 });
		polygon2D.push_back({ 3.0,  2.0 });
		polygon2D.push_back({ 2.0, -2.0 });
		polygon2D.push_back({ -2.0, -2.0 });
		polygon2D.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2D(polygon1D.data(), polygon1D.size(), polygon2D.data(), polygon2D.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F polygon1 is inside polygon2. COLLISION", "[TestPolygonToPolygon2F]")
	{
		// create pentagon
		polygon1F.push_back({ 0.0f,  2.0f });
		polygon1F.push_back({ 2.0f,  1.0f });
		polygon1F.push_back({ 1.0f, -1.0f });
		polygon1F.push_back({ -1.0f, -1.0f });
		polygon1F.push_back({ -2.0f,  1.0f });
		// create pentagon
		polygon2F.push_back({ 0.0f,  4.0f });
		polygon2F.push_back({ 3.0f,  2.0f });
		polygon2F.push_back({ 2.0f, -2.0f });
		polygon2F.push_back({ -2.0f, -2.0f });
		polygon2F.push_back({ -3.0f,  2.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2F(polygon1F.data(), polygon1F.size(), polygon2F.data(), polygon2F.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D polygon1 is inside polygon2. COLLISION", "[TestPolygonToPolygon2D]")
	{
		// create pentagon
		polygon1D.push_back({ 0.0,  2.0 });
		polygon1D.push_back({ 2.0,  1.0 });
		polygon1D.push_back({ 1.0, -1.0 });
		polygon1D.push_back({ -1.0, -1.0 });
		polygon1D.push_back({ -2.0,  1.0 });
		// create pentagon
		polygon2D.push_back({ 0.0,  4.0 });
		polygon2D.push_back({ 3.0,  2.0 });
		polygon2D.push_back({ 2.0, -2.0 });
		polygon2D.push_back({ -2.0, -2.0 });
		polygon2D.push_back({ -3.0,  2.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2D(polygon1D.data(), polygon1D.size(), polygon2D.data(), polygon2D.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F polygon2 is inside polygon1. COLLISION", "[TestPolygonToPolygon2F]")
	{
		// create pentagon
		polygon1F.push_back({ 0.0f,  4.0f });
		polygon1F.push_back({ 3.0f,  2.0f });
		polygon1F.push_back({ 2.0f, -2.0f });
		polygon1F.push_back({ -2.0f, -2.0f });
		polygon1F.push_back({ -3.0f,  2.0f });
		// create pentagon
		polygon2F.push_back({ 0.0f,  2.0f });
		polygon2F.push_back({ 2.0f,  1.0f });
		polygon2F.push_back({ 1.0f, -1.0f });
		polygon2F.push_back({ -1.0f, -1.0f });
		polygon2F.push_back({ -2.0f,  1.0f });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2F(polygon1F.data(), polygon1F.size(), polygon2F.data(), polygon2F.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D polygon2 is inside polygon1. COLLISION", "[TestPolygonToPolygon2D]")
	{
		// create pentagon
		polygon1D.push_back({ 0.0,  4.0 });
		polygon1D.push_back({ 3.0,  2.0 });
		polygon1D.push_back({ 2.0, -2.0 });
		polygon1D.push_back({ -2.0, -2.0 });
		polygon1D.push_back({ -3.0,  2.0 });
		// create pentagon
		polygon2D.push_back({ 0.0,  2.0 });
		polygon2D.push_back({ 2.0,  1.0 });
		polygon2D.push_back({ 1.0, -1.0 });
		polygon2D.push_back({ -1.0, -1.0 });
		polygon2D.push_back({ -2.0,  1.0 });

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2D(polygon1D.data(), polygon1D.size(), polygon2D.data(), polygon2D.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("F polygon1 and polygon2 are the same polygon. COLLISION", "[TestPolygonToPolygon2F]")
	{
		// create pentagon
		polygon1F.push_back({ 0.0f,  4.0f });
		polygon1F.push_back({ 3.0f,  2.0f });
		polygon1F.push_back({ 2.0f, -2.0f });
		polygon1F.push_back({ -2.0f, -2.0f });
		polygon1F.push_back({ -3.0f,  2.0f });
		// create pentagon
		polygon2F = polygon1F;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2F(polygon1F.data(), polygon1F.size(), polygon2F.data(), polygon2F.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}

	SECTION("D polygon1 and polygon2 are the same polygon. COLLISION", "[TestPolygonToPolygon2D]")
	{
		// create pentagon
		polygon1D.push_back({ 0.0,  4.0 });
		polygon1D.push_back({ 3.0,  2.0 });
		polygon1D.push_back({ 2.0, -2.0 });
		polygon1D.push_back({ -2.0, -2.0 });
		polygon1D.push_back({ -3.0,  2.0 });
		// create pentagon
		polygon2D = polygon1D;

		collisionResult = GW::MATH2D::GCollision2D::GCollisionCheck2D::ERROR_NO_RESULT;

		CHECK(+(GW::I::GCollision2DImplementation::TestPolygonToPolygon2D(polygon1D.data(), polygon1D.size(), polygon2D.data(), polygon2D.size(), collisionResult)));
		CHECK(collisionResult == GW::I::GCollision2DImplementation::GCollisionCheck2D::COLLISION);
	}
}

TEST_CASE("convert the cartesian coordinates of a point to barycentric coordinates relative to three other points", "[FindBarycentricF], [FindBarycentricD], [Math2D]")
{
	GW::MATH2D::GVECTOR2F pointF = {};
	std::vector<GW::MATH2D::GVECTOR2F> triangleF = {};

	GW::MATH2D::GVECTOR2D pointD = {};
	std::vector<GW::MATH2D::GVECTOR2D> triangleD = {};

	GW::MATH2D::GBARYCENTRICF barycentricF;
	GW::MATH2D::GBARYCENTRICD barycentricD;

	// clockwise winding
	SECTION("Float point on corner, symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 2.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.0f));
	}

	SECTION("Double point on corner, symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 2.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.0));
	}

	SECTION("Float point inside triangle, symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 1.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.25f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.25f));
	}

	SECTION("Double point inside triangle, symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 1.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.25));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.25));
	}

	SECTION("Float point outside triangle, symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 3.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, -0.25f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, -0.25f));
	}

	SECTION("Double point outside triangle, symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 3.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, -0.25));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, -0.25));
	}

	SECTION("Float point on triangle edge, symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 0.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.5f));
	}

	SECTION("Double point on triangle edge, symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 0.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.5));
	}



	// same tests but with counterclockwise winding
	SECTION("Float point on corner, symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 2.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.0f));
	}

	SECTION("Double point on corner, symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 2.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.0));
	}

	SECTION("Float point inside triangle, symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 1.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.25f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.25f));
	}

	SECTION("Double point inside triangle, symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 1.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.25));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.25));
	}

	SECTION("Float point outside triangle, symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 3.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, -0.25f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, -0.25f));
	}

	SECTION("Double point outside triangle, symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 3.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, -0.25));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, -0.25));
	}

	SECTION("Float point on triangle edge, symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 0.0f };
		triangleF.push_back({ 0.0f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.5f));
	}

	SECTION("Double point on triangle edge, symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 0.0 };
		triangleD.push_back({ 0.0, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.5));
	}



	// non-symetric triangle tests
	// clockwise winding
	SECTION("Float point on corner, non-symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.5f, 2.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.0f));
	}

	SECTION("Double point on corner, non-symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.5, 2.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.0));
	}

	SECTION("Float point inside triangle, non-symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 1.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.125f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.375f));
	}

	SECTION("Double point inside triangle, non-symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 1.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.125));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.375));
	}

	SECTION("Float point outside triangle, non-symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 3.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, -0.625f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.125f));
	}

	SECTION("Double point outside triangle, non-symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 3.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, -0.625));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.125));
	}

	SECTION("Float point on triangle edge, non-symetric clockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 0.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ 1.0f, 0.0f });
		triangleF.push_back({ -1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.5f));
	}

	SECTION("Double point on triangle edge, non-symetric clockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 0.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ 1.0, 0.0 });
		triangleD.push_back({ -1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.5));
	}



	// same non-symetric tests but with counterclockwise winding
	SECTION("Float point on corner, non-symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.5f, 2.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.0f));
	}

	SECTION("Double point on corner, non-symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.5, 2.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.0));
	}

	SECTION("Float point inside triangle, non-symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 1.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.375f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.125f));
	}

	SECTION("Double point inside triangle, non-symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 1.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.375));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.125));
	}

	SECTION("Float point outside triangle, non-symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 3.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.125f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, -0.625f));
	}

	SECTION("Double point outside triangle, non-symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 3.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.125));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, -0.625));
	}

	SECTION("Float point on triangle edge, non-symetric counterclockwise", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 0.0f };
		triangleF.push_back({ 0.5f, 2.0f });
		triangleF.push_back({ -1.0f, 0.0f });
		triangleF.push_back({ 1.0f, 0.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.alpha, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.beta, 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(barycentricF.gamma, 0.5f));
	}

	SECTION("Double point on triangle edge, non-symetric counterclockwise", "[FindBarycentricD]")
	{
		pointD = { 0.0, 0.0 };
		triangleD.push_back({ 0.5, 2.0 });
		triangleD.push_back({ -1.0, 0.0 });
		triangleD.push_back({ 1.0, 0.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(+(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.alpha, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.beta, 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(barycentricD.gamma, 0.5));
	}


	// fail tests
	SECTION("Float failure", "[FindBarycentricF]")
	{
		pointF = { 0.0f, 2.0f };
		triangleF.push_back({ 0.0f, 0.0f });
		triangleF.push_back({ 0.0f, 4.0f });
		triangleF.push_back({ 0.0f, 2.0f });

		barycentricF = { -1.0f, -1.0f, -1.0f };

		CHECK(-(GW::I::GCollision2DImplementation::FindBarycentricF(pointF, triangleF[0], triangleF[1], triangleF[2], barycentricF)));
	}

	SECTION("Double failure", "[FindBarycentricD]")
	{
		pointD = { 0.0, 2.0 };
		triangleD.push_back({ 0.0, 0.0 });
		triangleD.push_back({ 0.0, 4.0 });
		triangleD.push_back({ 0.0, 2.0 });

		barycentricD = { -1.0, -1.0, -1.0 };

		CHECK(-(GW::I::GCollision2DImplementation::FindBarycentricD(pointD, triangleD[0], triangleD[1], triangleD[2], barycentricD)));
	}
}
#endif