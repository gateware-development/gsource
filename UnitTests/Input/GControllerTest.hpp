#if defined(GATEWARE_ENABLE_INPUT) && !defined(GATEWARE_DISABLE_GCONTROLLER)
#include "../../Interface/System/GWindow.h"
#include "../../Interface/Core/GEventReceiver.h"

namespace GControllerTest
{
	struct TestController
	{
		int isConnected = -1;
		float input[G_RY_AXIS + 1] = { 0.0f, }; // Holds the value of the different controller inputs
	};

	void WaitForConnection(GW::INPUT::GController controller, const int controllerIndex, const int timeout, bool& isConnected)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(500));

		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();

		do
		{
			controller.IsConnected(controllerIndex, isConnected);

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();
		} while (!isConnected && lapsedTime < timeout);
	}

	void WaitForDisconnect(GW::INPUT::GController controller, const int controllerIndex, const int timeout, bool& isConnected)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(500));

		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();

		do
		{
			controller.IsConnected(controllerIndex, isConnected);

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();
		} while (isConnected && lapsedTime < timeout);
	}
}

bool EnableControllerTests = false;
TEST_CASE("Check for OS GUI & Controller support", "[Input]")
{
	GW::SYSTEM::GWindow window;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		EnableControllerTests = true;
		std::cout << "OS GUI DETECTED: RUNNING ALL GCONTROLLER UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL GCONTROLLER UNIT TESTS" << std::endl;
}

TEST_CASE("GController core test battery", "[Input]")
{
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::INPUT::GController controller;
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
	REQUIRE(controller);
}

TEST_CASE("Pre-plugged in GController test", "[Input]")
{
	// This is a local test definition that will enable test printouts in case further information is needed.
	// This should be useful if you need to see what the test sees, and if you think the test may be wrong.
#define PRE_PLUGGED_CONTROLLER_PRINTOUT 1
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	GW::INPUT::GController controller;
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
	REQUIRE(controller);

	int maxIndex = -1;
	int numReportedConnected = -1;
	int numConfirmedConnected = 0;

	REQUIRE(G_PASS(controller.GetMaxIndex(maxIndex)));
	REQUIRE(G_PASS(controller.GetNumConnected(numReportedConnected)));

#if PRE_PLUGGED_CONTROLLER_PRINTOUT
	std::cout << "Controllers Connected: " << numReportedConnected << std::endl;
	std::cout << "Max Controller Index: " << maxIndex << std::endl;
#endif

	// This is added so that the even callback for XInput has enough time to be called and initialized. This also makes
	// the test specialized for XInput's controller detection.
	std::this_thread::sleep_for(std::chrono::seconds(1));

	bool isConnected = false;
	for (int i = 0; i <= maxIndex; ++i)
	{
		REQUIRE(G_PASS(controller.IsConnected(i, isConnected)));
		if (isConnected)
			++numConfirmedConnected;
	}

#if PRE_PLUGGED_CONTROLLER_PRINTOUT
	std::cout << "Confirmed Controllers Connected: " << numConfirmedConnected << std::endl;
#endif


	CHECK(numConfirmedConnected == numReportedConnected);
#undef PRE_PLUGGED_CONTROLLER_PRINTOUT
}

#define MANUAL_INPUT 0
#define MANUAL_CONNECTION_EVENTS 0
#define MANUAL_VIBRATION_TEST 0 // is not supported on Linux or Mac
#define MANUAL_USER_INPUT_TEST 0

#if MANUAL_INPUT
TEST_CASE("GController Manual controller connection test", "[Input]")
{
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::INPUT::GController controller;
#if defined(_WIN32)
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#else
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#endif
	REQUIRE(controller);
    
    bool isConnected = false;
    
	std::cout << "Please, plug in a physical controller...\n";
	GControllerTest::WaitForConnection(controller, 0, 7, isConnected);

	int maxIndex = -1;
	int numReportedConnected = -1;
	int numConfirmedConnected = 0;
	// Test invaild arguments
	CHECK(controller.IsConnected(-1, isConnected) == GW::GReturn::INVALID_ARGUMENT);
	// end test
	REQUIRE(G_PASS(controller.GetMaxIndex(maxIndex)));
	REQUIRE(G_PASS(controller.GetNumConnected(numReportedConnected)));

	for (int i = 0; i <= maxIndex; ++i)
	{
		REQUIRE(G_PASS(controller.IsConnected(i, isConnected)));
		if (isConnected)
			++numConfirmedConnected;
	}

	CHECK(numConfirmedConnected == numReportedConnected);
}
#endif

#if MANUAL_CONNECTION_EVENTS
TEST_CASE("GController Manual controller connection event test", "[Input]")
{
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::INPUT::GController controller;
#if defined(_WIN32)
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#else
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#endif
	REQUIRE(controller);

	GControllerTest::TestController event_controllers[4];
	GW::CORE::GEventReceiver receiver;
	GW::GReturn code = receiver.Create(controller, [&]()
	{
		GW::GEvent eventParser;
		receiver.Pop(eventParser);
		GW::INPUT::GController::Events controllerEvent;
		GW::INPUT::GController::EVENT_DATA data;
		eventParser.Read(controllerEvent, data);
		switch (controllerEvent)
		{
		case GW::INPUT::GController::Events::CONTROLLERCONNECTED:
		{
			event_controllers[data.controllerIndex].isConnected = data.isConnected;
			break;
		}
		case GW::INPUT::GController::Events::CONTROLLERDISCONNECTED:
		{
			event_controllers[data.controllerIndex].isConnected = data.isConnected;
			break;
		}
		}
	});
    
    bool isConnected = false;

	printf("Please, unplug the physical controller...\n");
	GControllerTest::WaitForDisconnect(controller, 0, 10, isConnected);
	CHECK(isConnected == 0);
	printf("Please, plug the physical controller back in...\n");
	GControllerTest::WaitForConnection(controller, 0, 10, isConnected);
	CHECK(isConnected == 1);
}
#endif

#if MANUAL_VIBRATION_TEST
TEST_CASE("GController Manual controller vibration test", "[Input]")
{
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
#if defined(_WIN32)
	GW::INPUT::GController controller;
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
	REQUIRE(controller);
    
    bool isConnected = false;
    
	std::cout << "Please, plug in a physical controller...\n";
	GControllerTest::WaitForConnection(controller, 0, 7, isConnected);

	bool isVibrating = false;
	controller.IsConnected(0, isConnected);
	REQUIRE(isConnected);

	GW::SYSTEM::GWindow window;
	CHECK(G_PASS(window.Create(200, 200, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
	while (+window.ProcessWindowEvents())
	{
		if (isConnected)
		{
			// Checking invailds
			CHECK(controller.IsVibrating(-1, isVibrating) == GW::GReturn::INVALID_ARGUMENT);
			CHECK(controller.StopVibration(-1) == GW::GReturn::INVALID_ARGUMENT);
			CHECK(controller.StartVibration(-1, -1.0f, -1.0f, -2.0f) == GW::GReturn::INVALID_ARGUMENT);

			std::cout << "Vibration both sides duration .5 seconds\n";
			CHECK(G_PASS(controller.StartVibration(0, 0.0f, 1.0f, 0.5f)));
			std::this_thread::sleep_for(std::chrono::seconds(2));
			controller.IsVibrating(0, isVibrating);
			CHECK(isVibrating == false);
			std::this_thread::sleep_for(std::chrono::seconds(1));


			std::cout << ("Vibration Left side duration .5 seconds\n");
			CHECK(G_PASS(controller.StartVibration(0, -1.0f, 1.0f, 0.5f)));
			std::this_thread::sleep_for(std::chrono::seconds(2));
			controller.IsVibrating(0, isVibrating);
			CHECK(isVibrating == false);
			std::this_thread::sleep_for(std::chrono::seconds(1));


			std::cout << ("Vibration Right side duration .5 seconds\n");
			CHECK(G_PASS(controller.StartVibration(0, 1.0f, 1.0f, 0.5f)));
			std::this_thread::sleep_for(std::chrono::seconds(2));
			controller.IsVibrating(0, isVibrating);
			CHECK(isVibrating == false);
			std::this_thread::sleep_for(std::chrono::seconds(1));

			CHECK(G_PASS(controller.StartVibration(0, 0.0f, 1.0f, 0.5f)));
			CHECK(G_PASS(controller.StopVibration(0)));
			controller.IsVibrating(0, isVibrating);
			CHECK(isVibrating == false);
			std::this_thread::sleep_for(std::chrono::seconds(1));

			CHECK(G_PASS(controller.StartVibration(0, 0.0f, 1.0f, 0.5f)));
			CHECK(G_PASS(controller.StopAllVibrations()));
			controller.IsVibrating(0, isVibrating);
			CHECK(isVibrating == false);
			std::this_thread::sleep_for(std::chrono::seconds(1));

			CHECK(G_PASS(controller.StartVibration(0, 0.0f, 1.0f, 0.5f)));
			CHECK(controller.StartVibration(0, 0.0f, 1.0f, 0.5f) == GW::GReturn::REDUNDANT);
			CHECK(G_PASS(controller.StopVibration(0)));
		}
	}
#endif
}
#endif

#if MANUAL_USER_INPUT_TEST
TEST_CASE("GController Manual input test", "[Input]")
{
	if (EnableControllerTests == false)
	{
		std::cout << "GUI/Controller support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	GW::INPUT::GController controller;
#if defined(_WIN32)
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#else
	CHECK(controller.Create() == GW::GReturn::SUCCESS);
#endif
	REQUIRE(controller);

	constexpr int SELECTED_CONTROLLER = 0;
	constexpr int TEST_TIMEOUT = 5;

	GControllerTest::TestController event_controllers[4];

	GW::CORE::GEventReceiver receiver;
	GW::GReturn code = receiver.Create(controller, [&]()
	{
		GW::GEvent eventParser;
		receiver.Pop(eventParser);
		GW::INPUT::GController::Events controllerEvent;
		GW::INPUT::GController::EVENT_DATA data;
		eventParser.Read(controllerEvent, data);

		if (data.inputCode >= G_SOUTH_BTN && data.inputCode <= G_RY_AXIS)
		{
			switch (controllerEvent)
			{
			case GW::INPUT::GController::Events::CONTROLLERBUTTONVALUECHANGED:
				event_controllers[data.controllerIndex].input[data.inputCode] = data.inputValue;
				break;
			case GW::INPUT::GController::Events::CONTROLLERAXISVALUECHANGED:
				event_controllers[data.controllerIndex].input[data.inputCode] = data.inputValue;
				break;
			case GW::INPUT::GController::Events::CONTROLLERCONNECTED:
				event_controllers[data.controllerIndex].isConnected = data.isConnected;
				break;
			case GW::INPUT::GController::Events::CONTROLLERDISCONNECTED:
				event_controllers[data.controllerIndex].isConnected = data.isConnected;
				break;
			}
		}
	});

	enum TestIf
	{
		GREATER_THAN,
		EQUAL_TO,
		LESS_THAN
	};
	
	auto RunInputTest = [&](int inputCode, TestIf testif, float targetValue, const char* msg, bool& pollTestPass, bool& eventTestPass, float& pollValue, float& eventValue)
	{
		std::cout << msg << std::endl;

		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();

		do
		{
			if (!pollTestPass)
			{
				controller.GetState(SELECTED_CONTROLLER, inputCode, pollValue); // Test polling

				switch (testif)
				{
				case TestIf::GREATER_THAN: pollTestPass = pollValue > targetValue; break;
				case TestIf::EQUAL_TO: pollTestPass = pollValue == targetValue; break;
				case TestIf::LESS_THAN: pollTestPass = pollValue < targetValue; break;
				}
			}

			if (!eventTestPass)
			{
				eventValue = event_controllers[SELECTED_CONTROLLER].input[inputCode]; // Test events

				switch (testif)
				{
				case TestIf::GREATER_THAN: eventTestPass = eventValue > targetValue; break;
				case TestIf::EQUAL_TO: eventTestPass = eventValue == targetValue; break;
				case TestIf::LESS_THAN: eventTestPass = eventValue < targetValue; break;
				}
			}

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();
		} while ((!pollTestPass || !eventTestPass) && lapsedTime < TEST_TIMEOUT);
	};

#define TestInput(inputCode, testif, targetValue, msg)\
	if (true) /* Xcode complains about anonomous code blocks */\
	{\
		bool pollTestPass = false;\
		bool eventTestPass = false;\
		float pollValue = 0.0f;\
		float eventValue = 0.0f;\
		\
		RunInputTest(inputCode, testif, targetValue, msg, pollTestPass, eventTestPass, pollValue, eventValue);\
		\
		if (pollTestPass && eventTestPass)\
			std::cout << "TEST PASSED\n";\
		else\
		{\
			std::cout << "TEST FAILED\n";\
			\
			if (!pollTestPass)\
			{\
			std::cout << "pollTestPass\n";\
				switch (testif)\
				{\
				case TestIf::GREATER_THAN: CHECK(pollValue > targetValue); break; \
				case TestIf::EQUAL_TO: CHECK(pollValue == targetValue); break; \
				case TestIf::LESS_THAN: CHECK(pollValue < targetValue); break; \
				}\
			}\
			else if (!eventTestPass)\
			{\
			std::cout << "eventTestPass\n";\
				switch (testif)\
				{\
				case TestIf::GREATER_THAN: CHECK(eventValue > targetValue); break; \
				case TestIf::EQUAL_TO: CHECK(eventValue == targetValue); break; \
				case TestIf::LESS_THAN: CHECK(eventValue < targetValue); break; \
				}\
			}\
		}\
	}
    
    bool isConnected = false;
    
	std::cout << "Please, plug in a physical controller...\n";
	GControllerTest::WaitForConnection(controller, SELECTED_CONTROLLER, 7, isConnected);

	float outState = 0.0f;
	int numConnected = 0;
	// test invaild arguments
	CHECK(controller.GetState(-1, G_SOUTH_BTN, outState) == GW::GReturn::INVALID_ARGUMENT);
	CHECK(controller.GetState(SELECTED_CONTROLLER, -1, outState) == GW::GReturn::INVALID_ARGUMENT);
	//end test

	controller.IsConnected(SELECTED_CONTROLLER, isConnected);

	REQUIRE(isConnected);
	int count = -1;
	controller.GetNumConnected(count);
	printf("Controllers Connected %u\n", count);

	TestInput(G_DPAD_UP_BTN, TestIf::EQUAL_TO, 1.0f, "Press D-PAD UP button");
	TestInput(G_DPAD_UP_BTN, TestIf::EQUAL_TO, 0.0f, "Release D-PAD UP button");
	TestInput(G_DPAD_DOWN_BTN, TestIf::EQUAL_TO, 1.0f, "Press D-PAD DOWN button");
	TestInput(G_DPAD_DOWN_BTN, TestIf::EQUAL_TO, 0.0f, "Release D-PAD DOWN button");
	TestInput(G_DPAD_LEFT_BTN, TestIf::EQUAL_TO, 1.0f, "Press D-PAD LEFT button");
	TestInput(G_DPAD_LEFT_BTN, TestIf::EQUAL_TO, 0.0f, "Release D-PAD LEFT button");
	TestInput(G_DPAD_RIGHT_BTN, TestIf::EQUAL_TO, 1.0f, "Press D-PAD RIGHT button");
	TestInput(G_DPAD_RIGHT_BTN, TestIf::EQUAL_TO, 0.0f, "Release D-PAD RIGHT button");
	
	TestInput(G_NORTH_BTN, TestIf::EQUAL_TO, 1.0f, "Press NORTH button");
	TestInput(G_NORTH_BTN, TestIf::EQUAL_TO, 0.0f, "Release NORTH button");
	TestInput(G_SOUTH_BTN, TestIf::EQUAL_TO, 1.0f, "Press SOUTH button");
	TestInput(G_SOUTH_BTN, TestIf::EQUAL_TO, 0.0f, "Release SOUTH button");
	TestInput(G_WEST_BTN, TestIf::EQUAL_TO, 1.0f, "Press WEST button");
	TestInput(G_WEST_BTN, TestIf::EQUAL_TO, 0.0f, "Release WEST button");
	TestInput(G_EAST_BTN, TestIf::EQUAL_TO, 1.0f, "Press EAST button");
	TestInput(G_EAST_BTN, TestIf::EQUAL_TO, 0.0f, "Release EAST button");
	
	TestInput(G_LEFT_SHOULDER_BTN, TestIf::EQUAL_TO, 1.0f, "Press LEFT SHOULDER button");
	TestInput(G_LEFT_SHOULDER_BTN, TestIf::EQUAL_TO, 0.0f, "Release LEFT SHOULDER button");
	TestInput(G_RIGHT_SHOULDER_BTN, TestIf::EQUAL_TO, 1.0f, "Press RIGHT SHOULDER button");
	TestInput(G_RIGHT_SHOULDER_BTN, TestIf::EQUAL_TO, 0.0f, "Release RIGHT SHOULDER button");

	TestInput(G_LEFT_TRIGGER_AXIS, TestIf::EQUAL_TO, 1.0f, "Press LEFT TRIGGER button");
	TestInput(G_LEFT_TRIGGER_AXIS, TestIf::EQUAL_TO, 0.0f, "Release LEFT TRIGGER button");
	TestInput(G_RIGHT_TRIGGER_AXIS, TestIf::EQUAL_TO, 1.0f, "Press RIGHT TRIGGER button");
	TestInput(G_RIGHT_TRIGGER_AXIS, TestIf::EQUAL_TO, 0.0f, "Release RIGHT TRIGGER button");

	TestInput(G_LY_AXIS, TestIf::GREATER_THAN, 0.5f, "Move LEFT STICK UP");
	TestInput(G_LY_AXIS, TestIf::LESS_THAN, -0.5f, "Move LEFT STICK DOWN");
	TestInput(G_LX_AXIS, TestIf::LESS_THAN, -0.5f, "Move LEFT STICK LEFT");
	TestInput(G_LX_AXIS, TestIf::GREATER_THAN, 0.5f, "Move LEFT STICK RIGHT");
	TestInput(G_LEFT_THUMB_BTN, TestIf::EQUAL_TO, 1.0f, "Press LEFT STICK button");
	TestInput(G_LEFT_THUMB_BTN, TestIf::EQUAL_TO, 0.0f, "Release LEFT STICK button");

	TestInput(G_RY_AXIS, TestIf::GREATER_THAN, 0.5f, "Move RIGHT STICK UP");
	TestInput(G_RY_AXIS, TestIf::LESS_THAN, -0.5f, "Move RIGHT STICK DOWN");
	TestInput(G_RX_AXIS, TestIf::LESS_THAN, -0.5f, "Move RIGHT STICK LEFT");
	TestInput(G_RX_AXIS, TestIf::GREATER_THAN, 0.5f, "Move RIGHT STICK RIGHT");
	TestInput(G_RIGHT_THUMB_BTN, TestIf::EQUAL_TO, 1.0f, "Press RIGHT STICK button");
	TestInput(G_RIGHT_THUMB_BTN, TestIf::EQUAL_TO, 0.0f, "Release RIGHT STICK button");
	
	TestInput(G_START_BTN, TestIf::EQUAL_TO, 1.0f, "Press START button");
	TestInput(G_START_BTN, TestIf::EQUAL_TO, 0.0f, "Release START button");
	TestInput(G_SELECT_BTN, TestIf::EQUAL_TO, 1.0f, "Press SELECT button");
	TestInput(G_SELECT_BTN, TestIf::EQUAL_TO, 0.0f, "Release SELECT button");

#undef TestInput
}
#endif /* MANUAL_USER_INPUT_TEST */
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GCONTROLLER) */
