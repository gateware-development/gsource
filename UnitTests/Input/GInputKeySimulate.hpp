// Need include guard for this because GInput and GBufferedInput uses this file
#ifndef GINPUTKEYSIMULATE_HPP
#define GINPUTKEYSIMULATE_HPP

#ifdef __APPLE__
#include <TargetConditionals.h>
#if !TARGET_OS_IOS
@import Foundation;
@import Cocoa;
#endif
#endif

namespace GInputTest
{
	bool SimulateInput(bool _isPressed)
	{
#ifdef _WIN32
#include <winapifamily.h>
#if (WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP)
		//Create 15 mock inputs
		INPUT key[23];
		ZeroMemory(&key, sizeof(INPUT) * 23);//Zero the structure.

		DWORD testFlags = KEYEVENTF_SCANCODE;
		if (_isPressed == false)
			testFlags |= KEYEVENTF_KEYUP;

		//Key 1
		key[0].type = INPUT_KEYBOARD;
		key[0].ki.dwExtraInfo = GetMessageExtraInfo();
		key[0].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x31, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[0].ki.dwFlags = testFlags;

		//Key 2
		key[1].type = INPUT_KEYBOARD;
		key[1].ki.dwExtraInfo = GetMessageExtraInfo();
		key[1].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x32, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[1].ki.dwFlags = testFlags;

		//Key 3
		key[2].type = INPUT_KEYBOARD;
		key[2].ki.dwExtraInfo = GetMessageExtraInfo();
		key[2].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x33, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[2].ki.dwFlags = testFlags;

		//Key 4
		key[3].type = INPUT_KEYBOARD;
		key[3].ki.dwExtraInfo = GetMessageExtraInfo();
		key[3].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x34, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[3].ki.dwFlags = testFlags;

		//Key A
		key[4].type = INPUT_KEYBOARD;
		key[4].ki.dwExtraInfo = GetMessageExtraInfo();
		key[4].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x41, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[4].ki.dwFlags = testFlags;

		//Key S
		key[5].type = INPUT_KEYBOARD;
		key[5].ki.dwExtraInfo = GetMessageExtraInfo();
		key[5].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x53, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[5].ki.dwFlags = testFlags;

		//Key D
		key[6].type = INPUT_KEYBOARD;
		key[6].ki.dwExtraInfo = GetMessageExtraInfo();
		key[6].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x44, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[6].ki.dwFlags = testFlags;

		//Key W
		key[7].type = INPUT_KEYBOARD;
		key[7].ki.dwExtraInfo = GetMessageExtraInfo();
		key[7].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(0x57, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[7].ki.dwFlags = testFlags;

		///Mouse Buttons
		//Mouse Button Right
		key[8].type = INPUT_MOUSE;
		key[8].ki.dwExtraInfo = GetMessageExtraInfo();
		key[8].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_RBUTTON, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[8].ki.dwFlags = testFlags;

		//Mouse Button Left
		key[9].type = INPUT_MOUSE;
		key[9].ki.dwExtraInfo = GetMessageExtraInfo();
		key[9].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_LBUTTON, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[9].ki.dwFlags = testFlags;

		//Mouse Button Middle
		key[10].type = INPUT_MOUSE;
		key[10].ki.dwExtraInfo = GetMessageExtraInfo();
		key[10].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_MBUTTON, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[10].ki.dwFlags = testFlags;

		/* Testing the Lock Keys
		//Caps Lock
		key[15].type = INPUT_KEYBOARD;
		key[15].ki.dwExtraInfo = GetMessageExtraInfo();
		key[15].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_CAPITAL, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[15].ki.dwFlags = testFlags;

		//Num Lock
		key[16].type = INPUT_KEYBOARD;
		key[16].ki.dwExtraInfo = GetMessageExtraInfo();
		key[16].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_NUMLOCK, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[16].ki.dwFlags = testFlags;

		//Scroll Lock
		key[17].type = INPUT_KEYBOARD;
		key[17].ki.dwExtraInfo = GetMessageExtraInfo();
		key[17].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_SCROLL, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[17].ki.dwFlags = testFlags;
		*/

		////JWEST///line115 ish 

		//F1
		key[11].type = INPUT_KEYBOARD;
		key[11].ki.dwExtraInfo = GetMessageExtraInfo();
		key[11].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F1, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[11].ki.dwFlags = testFlags;

		//F2
		key[12].type = INPUT_KEYBOARD;
		key[12].ki.dwExtraInfo = GetMessageExtraInfo();
		key[12].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F2, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[12].ki.dwFlags = testFlags;

		//F3
		key[13].type = INPUT_KEYBOARD;
		key[13].ki.dwExtraInfo = GetMessageExtraInfo();
		key[13].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F3, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[13].ki.dwFlags = testFlags;

		//F4
		key[14].type = INPUT_KEYBOARD;
		key[14].ki.dwExtraInfo = GetMessageExtraInfo();
		key[14].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F4, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[14].ki.dwFlags = testFlags;

		//F5
		key[15].type = INPUT_KEYBOARD;
		key[15].ki.dwExtraInfo = GetMessageExtraInfo();
		key[15].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F5, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[15].ki.dwFlags = testFlags;

		//F6
		key[16].type = INPUT_KEYBOARD;
		key[16].ki.dwExtraInfo = GetMessageExtraInfo();
		key[16].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F6, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[16].ki.dwFlags = testFlags;

		//F7
		key[17].type = INPUT_KEYBOARD;
		key[17].ki.dwExtraInfo = GetMessageExtraInfo();
		key[17].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F7, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[17].ki.dwFlags = testFlags;

		//F8
		key[18].type = INPUT_KEYBOARD;
		key[18].ki.dwExtraInfo = GetMessageExtraInfo();
		key[18].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F8, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[18].ki.dwFlags = testFlags;

		//F9
		key[19].type = INPUT_KEYBOARD;
		key[19].ki.dwExtraInfo = GetMessageExtraInfo();
		key[19].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F9, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[19].ki.dwFlags = testFlags;

		//F10
		key[20].type = INPUT_KEYBOARD;
		key[20].ki.dwExtraInfo = GetMessageExtraInfo();
		key[20].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F10, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[20].ki.dwFlags = testFlags;

		//F11
		key[21].type = INPUT_KEYBOARD;
		key[21].ki.dwExtraInfo = GetMessageExtraInfo();
		key[21].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F11, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[21].ki.dwFlags = testFlags;

		//F12
		key[22].type = INPUT_KEYBOARD;
		key[22].ki.dwExtraInfo = GetMessageExtraInfo();
		key[22].ki.wScan = static_cast<WORD>(MapVirtualKeyEx(VK_F12, MAPVK_VK_TO_VSC, GetKeyboardLayout(0)));
		key[22].ki.dwFlags = testFlags;


		//Send the inputs
		SendInput(23, key, sizeof(INPUT));
		return true;

#elif (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	return false;
#endif

#elif __linux__
		//int r = 0;

		//	//Numbers
		//r = system("xdotool search --name Gateware keydown 1");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown 2");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown 3");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown 4");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		////Letters
		//r = system("xdotool search --name Gateware keydown A");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown S");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown D");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown W");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		////Arrow keys
		//r = system("xdotool search --name Gateware keydown Left");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown Right");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown Up");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		//r = system("xdotool search --name Gateware keydown Down");
		//std::this_thread::sleep_for(std::chrono::milliseconds(2));

		/// Simulating input is not supported on linux (X11 is not sufficient for our system)
		/// Returns false to skip
		return false;

#elif __APPLE__
    #if !TARGET_OS_IOS
		NSEvent* fakeEvent = nil;
		NSEventType testEventType = (_isPressed) ? NSEventTypeKeyDown : NSEventTypeKeyUp;

		//Key 1
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 18];

		[NSApp sendEvent : fakeEvent] ;

		//Key 2
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 19];

		[NSApp sendEvent : fakeEvent] ;

		//Key 3
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 20];

		[NSApp sendEvent : fakeEvent] ;

		//Key 4
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 21];

		[NSApp sendEvent : fakeEvent] ;

		//Key A
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 0];

		[NSApp sendEvent : fakeEvent] ;

		//Key S
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 1];

		[NSApp sendEvent : fakeEvent] ;

		//Key D
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 2];

		[NSApp sendEvent : fakeEvent] ;

		//Key W
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 13];

		[NSApp sendEvent : fakeEvent] ;

		//Key Left Arreow
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 123];

		[NSApp sendEvent : fakeEvent] ;

		//Key Right Arrow
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 124];

		[NSApp sendEvent : fakeEvent] ;

		//Key up arrow
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 126];

		[NSApp sendEvent : fakeEvent] ;

		//Key down arrow
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 125];

		[NSApp sendEvent : fakeEvent] ;

		///////JWEST//////
		//F1
		fakeEvent = [NSEvent keyEventWithType : testEventType 
			location : NSZeroPoint 
			modifierFlags : 0
			timestamp : 0 
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 122];

		[NSApp sendEvent : fakeEvent] ;

		//F2
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 120];

		[NSApp sendEvent : fakeEvent] ;

		//F3
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 99];

		[NSApp sendEvent : fakeEvent] ;

		//F4
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 118];

		[NSApp sendEvent : fakeEvent] ;

		//F5
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 96];

		[NSApp sendEvent : fakeEvent] ;

		//F6
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 97];

		[NSApp sendEvent : fakeEvent] ;

		//F7
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 98];

		[NSApp sendEvent : fakeEvent] ;

		//F8
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 100];

		[NSApp sendEvent : fakeEvent] ;

		//F9
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 101];

		[NSApp sendEvent : fakeEvent] ;

		//F10
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 109];

		[NSApp sendEvent : fakeEvent] ;

		//F11
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 103];

		[NSApp sendEvent : fakeEvent] ;

		//F12
		fakeEvent = [NSEvent keyEventWithType : testEventType
			location : NSZeroPoint
			modifierFlags : 0
			timestamp : 0
			windowNumber : [[NSApp mainWindow]windowNumber]
			context : nil
			characters : @"1"
			charactersIgnoringModifiers : @"a"
			isARepeat : NO
			keyCode : 111];

		[NSApp sendEvent : fakeEvent] ;

		return true;
        //return false;
#endif /* TARGET_OS_IOS */
#endif
	}
}
#endif
