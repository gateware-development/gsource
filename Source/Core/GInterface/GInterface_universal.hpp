// Universal implementation of GInterface

// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

	// Comments in these files are up to the developer but should not be part of doxygen
	class GInterfaceImplementation : public virtual GInterfaceInterface
	{
		// If we had any internal variables or functions we would define them here.
	public:
		// GInterface doesn't do anything... but you can make one as that is consistent with the API
		// ALL Implementations MUST have a "Create" function. You may pass any arguments you need.
		// ALL Implementations support only the default constructor, Use "Create" to actually initialize your class.
		GReturn Create()
		{
			return GReturn::SUCCESS;
		}

		// A pure virtual destructor requires an implementation in C++  
//		~GInterfaceImplementation() override {};
	};
	// End Gateware namespaces
	} // end CORE
} // end GW
