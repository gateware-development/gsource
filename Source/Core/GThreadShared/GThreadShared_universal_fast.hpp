// This files implements read & write locks through a generic mutex.
// Ideally you should replace this implementation with an optimized platform specific one.
// No error checking is present in this variant to maximize performance in release.
// **NEW** Updated with c++14 shared mutex.(if available)
#if __cplusplus >= 201402L // shared_mutex requires c++14 support
	#include <shared_mutex>
	#define G_LOCK_OBJ std::shared_mutex
	#define G_LOCK_ASYNC lock_shared
	#define G_UNLOCK_ASYNC unlock_shared
#else
	#include <mutex>
	#define G_LOCK_OBJ std::mutex
	#define G_LOCK_ASYNC lock
	#define G_UNLOCK_ASYNC unlock
#endif
// Make the implentation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Comments in these files are up to the developer but should not be part of doxygen
		// The implementation of this class should use the fastest synchronization primitive per-platform
		// The private inheritance of "GInterfaceImplementation" is not strictly necessary here but it
		// does demonstrate how to reuse Gateware implementation code in future interface implementations. 
		class GThreadSharedImplementation :	public virtual GThreadSharedInterface, // <-- Always inherit an interface this way
											protected GInterfaceImplementation // <-- Always reuse implementation code like so
		{
			// If C++14 support is detected we enable asynchronous reads for improved performance
			G_LOCK_OBJ async; // hopefully supports asynchronous reads and synchronous writes
		public:
			// ALL Implementations MUST have a "Create" function. You may pass any arguments you need.
			// ALL Implementations support only the default constructor, Use "Create" to actually initialize your class.
			GReturn Create()
			{
				// Init anything you cannot or do not wish to init inline here (c++11)
				return GReturn::SUCCESS;
			}
			// overload utility functions, these will be utilized by derived implementations of the parent interface
			GReturn LockAsyncRead() const override
			{
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				const_cast<G_LOCK_OBJ*>(&async)->G_LOCK_ASYNC();
				// we have locked down this thread
				return GReturn::SUCCESS;
			}
			GReturn UnlockAsyncRead() const override
			{
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				const_cast<G_LOCK_OBJ*>(&async)->G_UNLOCK_ASYNC();
				return GReturn::SUCCESS;
			}
			GReturn LockSyncWrite() override
			{
				async.lock();
				// we have locked down this thread
				return GReturn::SUCCESS;
			}
			GReturn UnlockSyncWrite() override
			{
				async.unlock();
				return GReturn::SUCCESS;
			}
			// release any outstanding locks on destruction
			~GThreadSharedImplementation()
			{
				UnlockAsyncRead();
				UnlockSyncWrite();
			}
		};
		// End Gateware namespaces
	} // end CORE
} // end GW
#undef G_LOCK_OBJ
#undef G_LOCK_ASYNC
#undef G_UNLOCK_ASYNC