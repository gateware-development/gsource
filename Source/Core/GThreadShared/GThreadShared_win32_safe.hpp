// Windows specific version uses SRWLocks for highly optimized read/write access to resources
// This variant can detect deadlocks and is generally quite safe, however to be able to effectively
// protect from deadlocks Asynchronous locking is limited to 63 different threads and each thread can
// only support 63 simultaneous locks. If a free Async slot is not available it will fall back to a Sync lock.
/*********************  THIS IS PLACEHOLDER CODE, MORE RESEARCH REQUIRED SEE BELOW ***********************/
#define WIN32_LEAN_AND_MEAN
#include <windows.h> // used for fast locks and synchronous reads
#include <atomic>
#include <thread>
// used to store thread_local data across translation units (c++11 limitation)
namespace internal_gw // internal API data, do not access externally
{
	// used to track who currently has locked a specific thread
	struct GTHREADSHARED_GLOBAL 
	{
		// NOTE: conceptually this would be used to track open "slots" on a thread that can be asynchronously locked.
		// each thread would have 63 slots due to thread local storage.
		// each thread shared would have its own bits to indicate which slots it had acquired across various threads.
		// basically a thread and a ThreadShared would negate and then & their bits to find a compatible opening.
		// a bit scan would then grab one of these bits and reserve it on both.
		// In the case where no matching async slots were open the code could fallback to a sync lock.
		// the "64th" bit would be toggled in the GThreadShared indicating that a fallback had occurred.
		// Synced locks would do normal detection as found in the "universal_safe" approach.
		// If an async lock was acquired then you can use the reserved bit to safely check if a particular thread
		// was or was not locked by you already. (assuming you did not fallback and no threadID was present)
		// thus (limited) parallel deadlock detection would be possible in asynchronous mode.
		unsigned long long read_slots = 0; // last bit reserved for tracking synchronous fallback
	};
	static GTHREADSHARED_GLOBAL& GThreadSharedGlobal()
	{
		static thread_local GTHREADSHARED_GLOBAL thread_info;
		return thread_info; // this variable is unique to each running thread
	}
}
// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Comments in these files are up to the developer but should not be part of doxygen
		// The implementation of this class should use the fastest synchronization primitive per-platform
		// The private inheritance of "GInterfaceImplementation" is not strictly necessary here but it
		// does demonstrate how to reuse Gateware implementation code in future interface implementations. 
		class GThreadSharedImplementation :	public virtual GThreadSharedInterface, // <-- Always inherit an interface this way
											private GInterfaceImplementation // <-- Always reuse implementation code like so
		{
			// Since we fully support C++11 you may inline initialize variables as needed.
			SRWLOCK sync = { nullptr }; // used to synchronize reads & writes, initializing this to something other than null/nothing causes issues
			std::atomic_ullong read_locks; // the async lock slots you own on various threads
			std::thread::id sync_thread; // ID of the thread that currently has a Sync lock.
		public:
			// ALL Implementations MUST have a "Create" function. You may pass any arguments you need.
			// ALL Implementations support only the default constructor, Use "Create" to actually initialize your class.
			GReturn Create()
			{
				// Init anything you cannot or do not wish to init inline here (c++11)
				InitializeSRWLock(&sync); // The return of this may be null so we cannot use that for error checking
				return GReturn::SUCCESS;
			}
			// overload utility functions, these will be utilized by derived implementations of the parent interface
			GReturn LockAsyncRead() const override
			{
				//if (internal_gw::GThreadSharedGlobal().owner == this) // ensure we don't try to lockdown our own thread we already locked
				//	return GReturn::DEADLOCK;
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				AcquireSRWLockShared(const_cast<PSRWLOCK>(&sync));
				//internal_gw::GThreadSharedGlobal() = { 
				//	const_cast<GThreadSharedImplementation*>(this), 1 // read mode
				//}; // we have locked down this thread
				return GReturn::SUCCESS;
			}
			GReturn UnlockAsyncRead() const override
			{
				//if (internal_gw::GThreadSharedGlobal().owner != this || // this thread was never locked
				//	internal_gw::GThreadSharedGlobal().mode != 1) // this thread was not in Read mode
				//	return GReturn::FAILURE;
				//internal_gw::GThreadSharedGlobal() = { nullptr, 0 }; // this thread is no longer locked by this object
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				ReleaseSRWLockShared(const_cast<PSRWLOCK>(&sync));
				return GReturn::SUCCESS;
			}
			GReturn LockSyncWrite() override
			{
				//if (internal_gw::GThreadSharedGlobal().owner == this) // ensure we don't try to lockdown our own thread we already locked
				//	return GReturn::DEADLOCK;
				AcquireSRWLockExclusive(&sync);
				//internal_gw::GThreadSharedGlobal() = {
				//	const_cast<GThreadSharedImplementation*>(this), 2 // write mode
				//}; // we have locked down this thread
				return GReturn::SUCCESS;
			}

			// it is the end-users responsibility to make sure they lock before they unlock
			// this disables the warning C26110 that is associated with this, so the end-user doesn't see it in Gateware.h
#pragma warning( push )
#pragma warning( disable : 26110 )
			GReturn UnlockSyncWrite() override
			{
				//if (internal_gw::GThreadSharedGlobal().owner != this || // this thread was never locked
				//	internal_gw::GThreadSharedGlobal().mode != 2) // this thread was not in Write mode
				//	return GReturn::FAILURE;
				//internal_gw::GThreadSharedGlobal() = { nullptr, 0 }; // this thread is no longer locked by this object
				ReleaseSRWLockExclusive(&sync);
				return GReturn::SUCCESS;
			}
			// this returns the pragma to its previous state, effectively un-disabling C26110 for the rest of Gateware.h
#pragma warning( pop )
			// release any outstanding locks on destruction
			~GThreadSharedImplementation()
			{
				//if (internal_gw::GThreadSharedGlobal().owner == this) {
				//	// this thread should no longer locked by this object
				//	unsigned mode = internal_gw::GThreadSharedGlobal().mode;
				//	internal_gw::GThreadSharedGlobal() = { nullptr, 0 };
				//	if (mode == 1) 
				//		ReleaseSRWLockShared(const_cast<PSRWLOCK>(&sync));
				//	else if (mode == 2)
				//		ReleaseSRWLockExclusive(&sync);
				//}
			}
		};
		// End Gateware namespaces
	} // end CORE
} // end GW