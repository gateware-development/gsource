// Windows specific version uses SRWLocks for highly optimized read/write access to resources
// This variant has NO safety mechanisms at all, only use it if you need every last shred of performance and
// are certain you have no threading logic errors of any kind.
#define WIN32_LEAN_AND_MEAN
#include <windows.h> // used for fast locks and synchronous reads
// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Comments in these files are up to the developer but should not be part of doxygen
		// The implementation of this class should use the fastest synchronization primitive per-platform
		// The private inheritance of "GInterfaceImplementation" is not strictly necessary here but it
		// does demonstrate how to reuse Gateware implementation code in future interface implementations. 
		class GThreadSharedImplementation :	public virtual GThreadSharedInterface, // <-- Always inherit an interface this way
											private GInterfaceImplementation // <-- Always reuse implementation code like so
		{
			// Since we fully support C++11 you may inline initialize variables as needed.
			SRWLOCK sync = { nullptr }; // used to synchronize reads & writes, initializing this to something other than null/nothing causes issues
		public:
			// ALL Implementations MUST have a "Create" function. You may pass any arguments you need.
			// ALL Implementations support only the default constructor, Use "Create" to actually initialize your class.
			GReturn Create()
			{
				// Init anything you cannot or do not wish to init inline here (c++11)
				InitializeSRWLock(&sync); // The return of this may be null so we cannot use that for error checking
				return GReturn::SUCCESS;
			}
			// overload utility functions, these will be utilized by derived implementations of the parent interface
			GReturn LockAsyncRead() const override
			{
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				AcquireSRWLockShared(const_cast<PSRWLOCK>(&sync));
				return GReturn::SUCCESS;
			}
			GReturn UnlockAsyncRead() const override
			{
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				ReleaseSRWLockShared(const_cast<PSRWLOCK>(&sync));
				return GReturn::SUCCESS;
			}
			GReturn LockSyncWrite() override
			{
				AcquireSRWLockExclusive(&sync);
				return GReturn::SUCCESS;
			}

			// it is the end-users responsibility to make sure they lock before they unlock
			// this disables the warning C26110 that is associated with this, so the end-user doesn't see it in Gateware.h
#pragma warning( push )
#pragma warning( disable : 26110 )
			GReturn UnlockSyncWrite() override
			{
				ReleaseSRWLockExclusive(&sync);
				return GReturn::SUCCESS;
			}
			// this returns the pragma to its previous state, effectively un-disabling C26110 for the rest of Gateware.h
#pragma warning( pop )
		};
		// End Gateware namespaces
	} // end CORE
} // end GW