namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GEventGeneratorImplementation :	public virtual GEventGeneratorInterface,
												public GThreadSharedImplementation
		{
		public:
			GReturn Create() {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Register(CORE::GEventCache _recorder) override {
				return GReturn::FAILURE;
			}
			GReturn Register(CORE::GEventResponder _responder) override {
				return GReturn::FAILURE;
			}
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
				return GReturn::FAILURE;
			}
			GReturn Deregister(CORE::GInterface _observer) override {
				return GReturn::FAILURE;
			}
			GReturn Observers(unsigned int& _outCount) const override {
				return GReturn::FAILURE;
			}
			GReturn Push(const GEvent& _newEvent) override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW