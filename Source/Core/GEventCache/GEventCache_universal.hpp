// Make the implentation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {
		// Just like GEventReceiver except it can store multiple events in a queue so you can delay processing
		class GEventCacheImplementation :	public virtual GEventCacheInterface,
											private GThreadSharedImplementation // resource locking				
		{
			// internal data members
			int enque = 0; // where items are enqued
			int deque = 0; // where items are dequed
			unsigned int maxEvents = 0; // size of the queue
			unsigned int missedEvents = 0; // increases if the queue is at capacity during append
			unsigned int eventsWaiting = 0; // number of events currently queued
			GEvent* circularQueue = nullptr; // where events are stored (ring buffer)
			
			// functions for circular queue (since std does not seem to provide one)
			// NOTE: we may want to shift this into "key" as an ADT if it sees more widespread application
			
			// returns false if queue is full
			bool Enque(const GEvent& _push) 
			{
				if (eventsWaiting >= maxEvents)
					return false;
				circularQueue[enque] = _push;
				enque = (enque + 1) % maxEvents;
				++eventsWaiting;
				return true;
			}
			// returns false if queue is empty
			bool Deque(GEvent* _pop) // optional
			{
				if (eventsWaiting == 0)
					return false;
				if(_pop != nullptr)
					*_pop = circularQueue[deque];
				deque = (deque + 1) % maxEvents;
				--eventsWaiting;
				return true;
			}
			// required to fully implement the "Find" routines
			// while collapsing the queue is not optimal we go for the closest point to minimize copies
			bool Erase(unsigned int _offset) // offset from dequqe point
			{
				if (eventsWaiting == 0 || _offset >= eventsWaiting) // should we bother?
					return false;
				// which is closer enque or deque?
				int dir = (_offset < (eventsWaiting >> 1)) ? maxEvents - 1 : 1;
				int stop = (dir == 1) ? enque : (deque + dir) % maxEvents; // stop here
				_offset = (deque + _offset) % maxEvents; // this starting spot should be valid now
				for (int i = (_offset + dir) % maxEvents; i != stop; i = (i + dir) % maxEvents)
				{
					circularQueue[_offset] = circularQueue[i]; // copy & move
					_offset = i; // next
				}
				// update enque or deque based on which way we closed the gap
				(dir == 1) ? enque = _offset : deque = ((deque + 1) % maxEvents);
				--eventsWaiting; // reduce overall size of queue
				return true; // hopefully the logic above is good we will test it
			}
			// queue theory:
			// 0 1 2 3 4 5 6
			// - - D + + + E
			// + E - - D + +
			// + E - - - - D
			// D E - - - - -
			// - - - D E - -
			// + + E D + + +
			// - D + + E - -
			// + + + + + E D

		public:
			// with dynamic memory present we must free any allocated resources
			~GEventCacheImplementation()
			{
				delete[] circularQueue; // free allocated queue
			}
			// You must select a maximum size of the queue as for efficiency it is implemented as a circular array
			GReturn Create(unsigned int _cacheSize) 
			{
				if (_cacheSize < 1) // needs to be at least 1
					return GReturn::INVALID_ARGUMENT;
				// generate circular queue
				if ((circularQueue = new GEvent[_cacheSize]) == nullptr)
					return GReturn::FAILURE; // new[] failed
				maxEvents = _cacheSize; // store maximum allowable event data
				return GReturn::SUCCESS;
			}
			GReturn Max(unsigned int& _outSize) const override 
			{
				LockAsyncRead();
				_outSize = maxEvents; // get maximum size of queue
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}
			// Override from GEventReceiver
			GReturn Append(const GEvent& _inEvent) override 
			{
				if (+LockSyncWrite()) // making changes
				{
					if (eventsWaiting >= maxEvents) // are we are full?
					{
						Deque(nullptr); // remove an existing event so we have room
						++missedEvents; // notify we missed an event
					}
					// append new event (should always happen)
					if (Enque(_inEvent) && +UnlockSyncWrite()) // unlock 
						return GReturn::SUCCESS;
				}
				return GReturn::UNEXPECTED_RESULT; // should never get here
			}
			GReturn Waiting(unsigned int& _outCount) const override 
			{
				LockAsyncRead();
				_outCount = eventsWaiting; // get current size of queue
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}
			GReturn Pop(GEvent& _outEvent) override 
			{
				GReturn result = GReturn::FAILURE;
				if (+LockSyncWrite())
				{
					if (Deque(&_outEvent))
						result = GReturn::SUCCESS;
					UnlockSyncWrite();
				}
				return result;
			}
			GReturn Peek(GEvent& _outEvent) const override
			{
				GReturn result = GReturn::FAILURE;
				if (+LockAsyncRead())
				{
					if (eventsWaiting)
					{
						_outEvent = circularQueue[deque];
						result = GReturn::SUCCESS;
					}
					UnlockAsyncRead();
				}
				return result;
			}
			GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const override
			{
				// Use deque + _eventIndex % maxEvents to find the right spot
				GReturn result = GReturn::FAILURE;
				if (+LockAsyncRead())
				{
					if (eventsWaiting)
					{
						if (_eventIndex < eventsWaiting) // must be a valid index
						{
							_outEvent = circularQueue[(deque + _eventIndex) % maxEvents];
							result = GReturn::SUCCESS;
						}
						else
							result = GReturn::INVALID_ARGUMENT; // out of range
					}
					UnlockAsyncRead();
				}
				return result;
			}
			GReturn Missed(unsigned int& _outCount) const override
			{
				LockAsyncRead();
				_outCount = missedEvents; // how many events were ignored
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}
			GReturn Clear() override 
			{
				GReturn result = GReturn::FAILURE;
				if (+LockSyncWrite())
				{
					if (eventsWaiting)
					{
						enque = deque = eventsWaiting = 0;
						result = GReturn::SUCCESS;
					}
					UnlockSyncWrite();
				}
				return result;
			}
			// Overload Find templates
			template<class eventType>
			GReturn Find(eventType _check, bool _remove) 
			{
				eventType event;
				GReturn result = GReturn::FAILURE;
				if ((_remove) ? +LockSyncWrite() : +LockAsyncRead())
				{
					if (eventsWaiting)
					{
						// loop looking for the event in question starting with the oldest
						int i = deque, x = 0;
						do // we use a do while because in a full queue enque and deque are equal
						{
							if (G_PASS(circularQueue[i].Read(event)))
							{
								if (event == _check) // found it?
								{
									result = GReturn::SUCCESS;
									if (_remove)
										result = (Erase(x)) ? GReturn::SUCCESS 
															: GReturn::MEMORY_CORRUPTION;
									break; // we found it!
								}
							}// move to the next item
							i = (i + 1) % maxEvents, ++x;
						}while (i != enque); // stop when we hit the enque
					}
					(_remove) ? UnlockSyncWrite() : UnlockAsyncRead();
				}
				return result;
			}
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData) 
			{
				eventType event;
				GReturn result = GReturn::FAILURE;
				if ((_remove) ? +LockSyncWrite() : +LockAsyncRead())
				{
					if (eventsWaiting)
					{
						// loop looking for the event in question starting with the oldest
						int i = deque, x = 0;
						do // we use a do while because in a full queue enque and deque are equal
						{
							if (G_PASS(circularQueue[i].Read(event, _outData)))
							{
								if (event == _check) // found it?
								{
									result = GReturn::SUCCESS;
									if (_remove)
										result = (Erase(x)) ? GReturn::SUCCESS
															: GReturn::MEMORY_CORRUPTION;
									break; // we found it!
								}
							}// move to the next item
							i = (i + 1) % maxEvents, ++x;
						} while (i != enque); // stop when we hit the enque
					}
					(_remove) ? UnlockSyncWrite() : UnlockAsyncRead();
				}
				return result;
			}
		};
	} // end CORE
} // end GW
