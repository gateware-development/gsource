namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GLogicImplementation : public virtual GLogicInterface,
									 protected GThreadSharedImplementation
		{
		public:
			GReturn Create(std::function<void()> _logic) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Assign(std::function<void()> _newLogic) override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW