namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GEventResponderImplementation :	public virtual GEventResponderInterface,
												protected GThreadSharedImplementation
		{
		public:
			GReturn Create(std::function<void(const GEvent&)> _handler) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return GReturn::FAILURE;
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return GReturn::FAILURE;
			}
			GReturn Assign(std::function<void()> _newLogic) override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW