// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Simply holds a std::function<void(const GEvent&)> and allows it to be updated/invoked safely
		class GEventResponderImplementation :	public virtual GEventResponderInterface,
												protected GThreadSharedImplementation // resource locking
		{
			// internal data members
			std::function<void(const GEvent&)> handler = nullptr;
		public:
			// Must match Proxy argument list
			GReturn Create(std::function<void(const GEvent&)> _handler) {
				handler = _handler; // nullptr is ok, optional.	
				return GReturn::SUCCESS;
			}
			// custom variants here
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				GReturn result;
				if (G_PASS(result = LockSyncWrite()))
				{
					handler = _newEventHandler; // routine updated
					return UnlockSyncWrite();
				}
				return result;
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				GReturn result = GReturn::IGNORED;
				thread_local bool recursing = false;
				if (recursing || G_PASS(result = LockAsyncRead()))
				{
					recursing = true; // recursion is allowed on this thread, bypass locks
					// run logic if available, allowing recursion on this thread
					if (handler) handler(_incomingEvent); // routine called with actual event
					// if this was a recursive call ignore the Unlock since the stack unwind will handle it
					if (result != GReturn::IGNORED)
					{
						recursing = false;
						UnlockAsyncRead();
					}
					// report the result of the function call
					result = (handler) ? GReturn::SUCCESS : GReturn::FAILURE;
				}
				return result;
			}
			// base interface variant internals overwritten below
			GReturn Assign(std::function<void()> _newLogic) override {
				GReturn result;
				if (G_PASS(result = LockSyncWrite()))
				{
					handler = std::bind(_newLogic); // routine updated to ignore GEvents
					return UnlockSyncWrite();
				}
				return result;
			}
			GReturn Invoke() const override {
				GReturn result = GReturn::IGNORED;
				thread_local bool recursing = false;
				if (recursing || G_PASS(result = LockAsyncRead()))
				{
					recursing = true; // recursion is allowed on this thread, bypass locks
					// run logic if available, allowing recursion on this thread
					if (handler) handler(GEvent()); // routine called with non-event
					// if this was a recursive call ignore the Unlock since the stack unwind will handle it
					if (result != GReturn::IGNORED)
					{
						recursing = false;
						UnlockAsyncRead();
					}
					// report the result of the function call
					result = (handler) ? GReturn::SUCCESS : GReturn::FAILURE;
				}
				return result;
			}
		};

	} // end CORE
} // end GW
