#include <cmath>

namespace GW
{
	namespace I
	{
		class GVector2DImplementation : public virtual GVector2DInterface, private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}

			// Floats
			static GReturn Add2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;

				return GReturn::SUCCESS;
			}

			static GReturn Add3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;
				_outVector.z = _vector1.z + _vector2.z;

				return GReturn::SUCCESS;
			}

			static GReturn Subtract2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;

				return GReturn::SUCCESS;
			}

			static GReturn Subtract3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;
				_outVector.z = _vector1.z - _vector2.z;

				return GReturn::SUCCESS;
			}

			static GReturn Scale2F(MATH2D::GVECTOR2F _vector, float _scalar, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;

				return GReturn::SUCCESS;
			}

			static GReturn Scale3F(MATH2D::GVECTOR3F _vector, float _scalar, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;
				_outVector.z = _scalar * _vector.z;

				return GReturn::SUCCESS;
			}

			static GReturn Dot2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y);

				return GReturn::SUCCESS;
			}

			static GReturn Dot3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y) + (_vector1.z * _vector2.z);

				return GReturn::SUCCESS;
			}

			static GReturn Cross2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue)
			{
				_outValue = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			}

			static GReturn Cross3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = (_vector1.y * _vector2.z) - (_vector1.z * _vector2.y);
				_outVector.y = (_vector1.z * _vector2.x) - (_vector1.x * _vector2.z);
				_outVector.z = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			}

			static GReturn VectorXMatrix2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y);

				return GReturn::SUCCESS;
			}

			static GReturn VectorXMatrix3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x) + (_vector.z * _matrix.row3.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y) + (_vector.z * _matrix.row3.y);
				_outVector.z = (_vector.x * _matrix.row1.z) + (_vector.y * _matrix.row2.z) + (_vector.z * _matrix.row3.z);

				return GReturn::SUCCESS;
			}

			static GReturn Transform2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector)
			{
				VectorXMatrix2F(_vector, _matrix, _outVector);

				return GReturn::SUCCESS;
			}

			static GReturn Transform3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector)
			{
				VectorXMatrix3F(_vector, _matrix, _outVector);

				return GReturn::SUCCESS;
			}

			static GReturn Magnitude2F(MATH2D::GVECTOR2F _vector, float& _outMagnitude)
			{
				_outMagnitude = sqrt((_vector.x * _vector.x) + (_vector.y * _vector.y));
				if (G2D_COMPARISON_STANDARD_F(_outMagnitude, 0.0f))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}

			static GReturn Magnitude3F(MATH2D::GVECTOR3F _vector, float& _outMagnitude)
			{
				_outMagnitude = sqrt((_vector.x * _vector.x) + (_vector.y * _vector.y) + (_vector.z * _vector.z));
				if (G2D_COMPARISON_STANDARD_F(_outMagnitude, 0.0f))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}

			static GReturn Normalize2F(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector)
			{
				float magnitude;
				if (-Magnitude2F(_vector, magnitude))
					return GReturn::FAILURE;

				_outVector.x = _vector.x / magnitude;
				_outVector.y = _vector.y / magnitude;

				return GReturn::SUCCESS;
			}

			static GReturn Normalize3F(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector)
			{
				float magnitude;
				if (-Magnitude3F(_vector, magnitude))
					return GReturn::FAILURE;

				_outVector.x = _vector.x / magnitude;
				_outVector.y = _vector.y / magnitude;
				_outVector.z = _vector.z / magnitude;

				return GReturn::SUCCESS;
			}

			static GReturn LerpF(float _value1, float _value2, float _ratio, float& _outValue)
			{
				_outValue = G2D_LERP(_value1, _value2, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn Lerp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float _ratio, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = G2D_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G2D_LERP(_vector1.y, _vector2.y, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn Lerp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float _ratio, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = G2D_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G2D_LERP(_vector1.y, _vector2.y, _ratio);
				_outVector.z = G2D_LERP(_vector1.z, _vector2.z, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn BerpF(float _value1, float _value2, float _value3, MATH2D::GBARYCENTRICF _barycentric, float& _outValue)
			{
				_outValue = (_value1 * _barycentric.alpha) + (_value2 * _barycentric.beta) + (_value3 * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Berp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = (_vector1.x * _barycentric.alpha) + (_vector2.x * _barycentric.beta) + (_vector3.x * _barycentric.gamma);
				_outVector.y = (_vector1.y * _barycentric.alpha) + (_vector2.y * _barycentric.beta) + (_vector3.y * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Berp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = (_vector1.x * _barycentric.alpha) + (_vector2.x * _barycentric.beta) + (_vector3.x * _barycentric.gamma);
				_outVector.y = (_vector1.y * _barycentric.alpha) + (_vector2.y * _barycentric.beta) + (_vector3.y * _barycentric.gamma);
				_outVector.z = (_vector1.z * _barycentric.alpha) + (_vector2.z * _barycentric.beta) + (_vector3.z * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Spline2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GVECTOR2F _vector4, float _ratio, MATH2D::GVECTOR2F& _outVector)
			{
				MATH2D::GVECTOR2F p0 = _vector1;
				MATH2D::GVECTOR2F p1 = _vector2;
				MATH2D::GVECTOR2F p2 = _vector3;
				MATH2D::GVECTOR2F p3 = _vector4;

				float t0 = 0.0f;
				float t1 = pow(sqrt(pow((p1.x - p0.x), 2.0f) + pow((p1.y - p0.y), 2.0f)), 0.5f) + t0;
				float t2 = pow(sqrt(pow((p2.x - p1.x), 2.0f) + pow((p2.y - p1.y), 2.0f)), 0.5f) + t1;
				float t3 = pow(sqrt(pow((p3.x - p2.x), 2.0f) + pow((p3.y - p2.y), 2.0f)), 0.5f) + t2;

				MATH2D::GVECTOR2F A1;
				MATH2D::GVECTOR2F A2;
				MATH2D::GVECTOR2F A3;
				MATH2D::GVECTOR2F B1;
				MATH2D::GVECTOR2F B2;

				float t = t1 + (t2 - t1) * _ratio;

				for (int i = 0; i < 2; i++)
				{
					A1.data[i] = (t1 - t) / (t1 - t0) * p0.data[i] + (t - t0) / (t1 - t0) * p1.data[i];
					A2.data[i] = (t2 - t) / (t2 - t1) * p1.data[i] + (t - t1) / (t2 - t1) * p2.data[i];
					A3.data[i] = (t3 - t) / (t3 - t2) * p2.data[i] + (t - t2) / (t3 - t2) * p3.data[i];

					B1.data[i] = (t2 - t) / (t2 - t0) * A1.data[i] + (t - t0) / (t2 - t0) * A2.data[i];
					B2.data[i] = (t3 - t) / (t3 - t1) * A2.data[i] + (t - t1) / (t3 - t1) * A3.data[i];

					_outVector.data[i] = (t2 - t) / (t2 - t1) * B1.data[i] + (t - t1) / (t2 - t1) * B2.data[i];
				}

				return GReturn::SUCCESS;
			}

			static GReturn GradientF(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _line.start.y - _line.end.y;
				_outVector.y = _line.end.x - _line.start.x;

				return GReturn::SUCCESS;
			}

			static GReturn AngleBetweenVectorsF(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outRadians)
			{
				float dot;
				Dot2F(_vector1, _vector2, dot);

				float magnitude1;
				float magnitude2;
				if (-(Magnitude2F(_vector1, magnitude1)) || -(Magnitude2F(_vector2, magnitude2)))
					return GReturn::FAILURE;

				_outRadians = acos(G2D_ABS(dot) / (magnitude1 * magnitude2));

				return GReturn::SUCCESS;
			}

			static GReturn AngleBetweenLinesF(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, float& _outRadians)
			{
				// get vectors parallel to the lines. represented as single points because we assume the vector starts at the origin
				GW::MATH2D::GVECTOR2F u;
				GW::MATH2D::GVECTOR2F v;

				u.x = _line1.end.x - _line1.start.x;
				u.y = _line1.end.y - _line1.start.y;

				v.x = _line2.end.x - _line2.start.x;
				v.y = _line2.end.y - _line2.start.y;

				// now that we have the 2 vectors parallel to the 2 lines, we can perform the actual calculation
				if (-AngleBetweenVectorsF(u, v, _outRadians))
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}

			static GReturn Upgrade2(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = static_cast<double>(_vector.x);
				_outVector.y = static_cast<double>(_vector.y);

				return GReturn::SUCCESS;
			}

			static GReturn Upgrade3(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = static_cast<double>(_vector.x);
				_outVector.y = static_cast<double>(_vector.y);
				_outVector.z = static_cast<double>(_vector.z);

				return GReturn::SUCCESS;
			}



			// Doubles
			static GReturn Add2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;

				return GReturn::SUCCESS;
			}

			static GReturn Add3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;
				_outVector.z = _vector1.z + _vector2.z;

				return GReturn::SUCCESS;
			}

			static GReturn Subtract2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;

				return GReturn::SUCCESS;
			}

			static GReturn Subtract3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;
				_outVector.z = _vector1.z - _vector2.z;

				return GReturn::SUCCESS;
			};

			static GReturn Scale2D(MATH2D::GVECTOR2D _vector, double _scalar, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;

				return GReturn::SUCCESS;
			};

			static GReturn Scale3D(MATH2D::GVECTOR3D _vector, double _scalar, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;
				_outVector.z = _scalar * _vector.z;

				return GReturn::SUCCESS;
			};

			static GReturn Dot2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y);

				return GReturn::SUCCESS;
			};

			static GReturn Dot3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y) + (_vector1.z * _vector2.z);

				return GReturn::SUCCESS;
			};

			static GReturn Cross2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue)
			{
				_outValue = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			};

			static GReturn Cross3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = (_vector1.y * _vector2.z) - (_vector1.z * _vector2.y);
				_outVector.y = (_vector1.z * _vector2.x) - (_vector1.x * _vector2.z);
				_outVector.z = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			};

			static GReturn VectorXMatrix2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y);

				return GReturn::SUCCESS;
			};

			static GReturn VectorXMatrix3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x) + (_vector.z * _matrix.row3.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y) + (_vector.z * _matrix.row3.y);
				_outVector.z = (_vector.x * _matrix.row1.z) + (_vector.y * _matrix.row2.z) + (_vector.z * _matrix.row3.z);

				return GReturn::SUCCESS;
			};

			static GReturn Transform2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector)
			{
				VectorXMatrix2D(_vector, _matrix, _outVector);

				return GReturn::SUCCESS;
			}

			static GReturn Transform3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector)
			{
				VectorXMatrix3D(_vector, _matrix, _outVector);

				return GReturn::SUCCESS;
			};

			static GReturn Magnitude2D(MATH2D::GVECTOR2D _vector, double& _outMagnitude)
			{
				_outMagnitude = sqrt((_vector.x * _vector.x) + (_vector.y * _vector.y));
				if (G2D_COMPARISON_STANDARD_D(_outMagnitude, 0.0))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}

			static GReturn Magnitude3D(MATH2D::GVECTOR3D _vector, double& _outMagnitude)
			{
				_outMagnitude = sqrt((_vector.x * _vector.x) + (_vector.y * _vector.y) + (_vector.z * _vector.z));
				if (G2D_COMPARISON_STANDARD_D(_outMagnitude, 0.0))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}

			static GReturn Normalize2D(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector)
			{
				double magnitude;
				if (-Magnitude2D(_vector, magnitude))
					return GReturn::FAILURE;

				_outVector.x = _vector.x / magnitude;
				_outVector.y = _vector.y / magnitude;

				return GReturn::SUCCESS;
			}

			static GReturn Normalize3D(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector)
			{
				double magnitude;
				if (-Magnitude3D(_vector, magnitude))
					return GReturn::FAILURE;

				_outVector.x = _vector.x / magnitude;
				_outVector.y = _vector.y / magnitude;
				_outVector.z = _vector.z / magnitude;

				return GReturn::SUCCESS;
			}

			static GReturn LerpD(double _value1, double _value2, double _ratio, double& _outValue)
			{
				_outValue = G2D_LERP(_value1, _value2, _ratio);

				return GReturn::SUCCESS;
			};

			static GReturn Lerp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double _ratio, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = G2D_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G2D_LERP(_vector1.y, _vector2.y, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn Lerp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double _ratio, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = G2D_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G2D_LERP(_vector1.y, _vector2.y, _ratio);
				_outVector.z = G2D_LERP(_vector1.z, _vector2.z, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn BerpD(double _value1, double _value2, double _value3, MATH2D::GBARYCENTRICD _barycentric, double& _outValue)
			{
				_outValue = (_value1 * _barycentric.alpha) + (_value2 * _barycentric.beta) + (_value3 * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Berp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = (_vector1.x * _barycentric.alpha) + (_vector2.x * _barycentric.beta) + (_vector3.x * _barycentric.gamma);
				_outVector.y = (_vector1.y * _barycentric.alpha) + (_vector2.y * _barycentric.beta) + (_vector3.y * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Berp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = (_vector1.x * _barycentric.alpha) + (_vector2.x * _barycentric.beta) + (_vector3.x * _barycentric.gamma);
				_outVector.y = (_vector1.y * _barycentric.alpha) + (_vector2.y * _barycentric.beta) + (_vector3.y * _barycentric.gamma);
				_outVector.z = (_vector1.z * _barycentric.alpha) + (_vector2.z * _barycentric.beta) + (_vector3.z * _barycentric.gamma);

				return GReturn::SUCCESS;
			}

			static GReturn Spline2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GVECTOR2D _vector4, double _ratio, MATH2D::GVECTOR2D& _outVector)
			{
				MATH2D::GVECTOR2D p0 = _vector1;
				MATH2D::GVECTOR2D p1 = _vector2;
				MATH2D::GVECTOR2D p2 = _vector3;
				MATH2D::GVECTOR2D p3 = _vector4;

				double t0 = 0.0;
				double t1 = pow(sqrt(pow((p1.x - p0.x), 2) + pow((p1.y - p0.y), 2)), 0.5) + t0;
				double t2 = pow(sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2)), 0.5) + t1;
				double t3 = pow(sqrt(pow((p3.x - p2.x), 2) + pow((p3.y - p2.y), 2)), 0.5) + t2;

				MATH2D::GVECTOR2D A1;
				MATH2D::GVECTOR2D A2;
				MATH2D::GVECTOR2D A3;
				MATH2D::GVECTOR2D B1;
				MATH2D::GVECTOR2D B2;

				double t = t1 + (t2 - t1) * _ratio;

				for (int i = 0; i < 2; i++)
				{
					A1.data[i] = (t1 - t) / (t1 - t0) * p0.data[i] + (t - t0) / (t1 - t0) * p1.data[i];
					A2.data[i] = (t2 - t) / (t2 - t1) * p1.data[i] + (t - t1) / (t2 - t1) * p2.data[i];
					A3.data[i] = (t3 - t) / (t3 - t2) * p2.data[i] + (t - t2) / (t3 - t2) * p3.data[i];

					B1.data[i] = (t2 - t) / (t2 - t0) * A1.data[i] + (t - t0) / (t2 - t0) * A2.data[i];
					B2.data[i] = (t3 - t) / (t3 - t1) * A2.data[i] + (t - t1) / (t3 - t1) * A3.data[i];

					_outVector.data[i] = (t2 - t) / (t2 - t1) * B1.data[i] + (t - t1) / (t2 - t1) * B2.data[i];
				}

				return GReturn::SUCCESS;
			}

			static GReturn GradientD(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _line.start.y - _line.end.y;
				_outVector.y = _line.end.x - _line.start.x;

				return GReturn::SUCCESS;
			}

			static GReturn AngleBetweenVectorsD(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outRadians)
			{
				double dot;
				Dot2D(_vector1, _vector2, dot);

				double magnitude1;
				double magnitude2;
				if (-(Magnitude2D(_vector1, magnitude1)) || -(Magnitude2D(_vector2, magnitude2)))
					return GReturn::FAILURE;

				_outRadians = acos(G2D_ABS(dot) / (magnitude1 * magnitude2));

				return GReturn::SUCCESS;
			}

			static GReturn AngleBetweenLinesD(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, double& _outRadians)
			{
				// get vectors parallel to the lines. represented as single points because we assume the vector starts at the origin
				GW::MATH2D::GVECTOR2D u;
				GW::MATH2D::GVECTOR2D v;

				u.x = _line1.end.x - _line1.start.x;
				u.y = _line1.end.y - _line1.start.y;

				v.x = _line2.end.x - _line2.start.x;
				v.y = _line2.end.y - _line2.start.y;

				// now that we have the 2 vectors parallel to the 2 lines, we can perform the actual calculation
				if (-AngleBetweenVectorsD(u, v, _outRadians))
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}

			static GReturn Downgrade2(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = static_cast<float>(_vector.x);
				_outVector.y = static_cast<float>(_vector.y);

				return GReturn::SUCCESS;
			}

			static GReturn Downgrade3(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = static_cast<float>(_vector.x);
				_outVector.y = static_cast<float>(_vector.y);
				_outVector.z = static_cast<float>(_vector.z);

				return GReturn::SUCCESS;
			}
		};
	}
}