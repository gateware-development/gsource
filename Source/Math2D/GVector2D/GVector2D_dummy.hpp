namespace GW
{
	namespace I
	{
		class GVector2DImplementation : public virtual GVector2DInterface, private GInterfaceImplementation
		{
		public:
			GReturn Create() { return GReturn::INTERFACE_UNSUPPORTED; }
			// Floats
			static GReturn Add2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Add3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Subtract2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Subtract3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Scale2F(MATH2D::GVECTOR2F _vector, float _scalar, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Scale3F(MATH2D::GVECTOR3F _vector, float _scalar, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Dot2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::FAILURE; }
			static GReturn Dot3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float& _outValue) { return GReturn::FAILURE; }
			static GReturn Cross2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::FAILURE; }
			static GReturn Cross3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn VectorXMatrix2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn VectorXMatrix3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Transform2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Transform3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Magnitude2F(MATH2D::GVECTOR2F _vector, float& _outMagnitude) { return GReturn::FAILURE; }
			static GReturn Magnitude3F(MATH2D::GVECTOR3F _vector, float& _outMagnitude) { return GReturn::FAILURE; }
			static GReturn Normalize2F(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Normalize3F(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn LerpF(float _value1, float _value2, float _ratio, float& _outValue) { return GReturn::FAILURE; }
			static GReturn Lerp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float _ratio, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Lerp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float _ratio, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn BerpF(float _value1, float _value2, float _value3, MATH2D::GBARYCENTRICF _barycentric, float& _outValue) { return GReturn::FAILURE; }
			static GReturn Berp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn Berp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn Spline2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GVECTOR2F _vector4, float _ratio, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn GradientF(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn AngleBetweenVectorsF(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outRadians) { return GReturn::FAILURE; }
			static GReturn AngleBetweenLinesF(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, float& _outRadians) { return GReturn::FAILURE; }
			static GReturn Upgrade2(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Upgrade3(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			// Doubles
			static GReturn Add2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Add3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Subtract2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Subtract3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Scale2D(MATH2D::GVECTOR2D _vector, double _scalar, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Scale3D(MATH2D::GVECTOR3D _vector, double _scalar, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Dot2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::FAILURE; }
			static GReturn Dot3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double& _outValue) { return GReturn::FAILURE; }
			static GReturn Cross2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::FAILURE; }
			static GReturn Cross3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double& _outValue) { return GReturn::FAILURE; }
			static GReturn VectorXMatrix2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn VectorXMatrix3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Transform2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Transform3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Magnitude2D(MATH2D::GVECTOR2D _vector, double& _outMagnitude) { return GReturn::FAILURE; }
			static GReturn Magnitude3D(MATH2D::GVECTOR3D _vector, double& _outMagnitude) { return GReturn::FAILURE; }
			static GReturn Normalize2D(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Normalize3D(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn LerpD(double _value1, double _value2, double _ratio, double& _outValue) { return GReturn::FAILURE; }
			static GReturn Lerp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Lerp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double _ratio, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn BerpD(double _value1, double _value2, double _value3, MATH2D::GBARYCENTRICD _barycentric, double& _outValue) { return GReturn::FAILURE; }
			static GReturn Berp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Berp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn Spline2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GVECTOR2D _vector4, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn GradientD(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn AngleBetweenVectorsD(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outRadians) { return GReturn::FAILURE; }
			static GReturn AngleBetweenLinesD(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, double& _outRadians) { return GReturn::FAILURE; }
			static GReturn Downgrade2(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn Downgrade3(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
		};
	}
}