namespace GW
{
	namespace I
	{
		class GCollision2DImplementation : public virtual GCollision2DInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}

			// Floats
			static GReturn ImplicitLineEquationF(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outEquationResult) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outDistance) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, float& _outDistance) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, float& _outDistance) { return GReturn::FAILURE; }
			static GReturn TestPointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToCircle2F(MATH2D::GVECTOR2F _point, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToLine2F(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToCircle2F(MATH2D::GLINE2F _line, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToRectangle2F(MATH2D::GLINE2F _line, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToPolygon2F(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestCircleToCircle2F(MATH2D::GCIRCLE2F _circle1, MATH2D::GCIRCLE2F _circle2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestRectangleToRectangle2F(MATH2D::GRECTANGLE2F _rectangle1, MATH2D::GRECTANGLE2F _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestRectangleToPolygon2F(MATH2D::GRECTANGLE2F _rectangle, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPolygonToPolygon2F(MATH2D::GVECTOR2F* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2F* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn FindBarycentricF(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F _trianglePoint1, MATH2D::GVECTOR2F _trianglePoint2, MATH2D::GVECTOR2F _trianglePoint3, MATH2D::GBARYCENTRICF& _outBarycentric) { return GReturn::FAILURE; }
			// Doubles
			static GReturn ImplicitLineEquationD(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outEquationResult) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outDistance) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, double& _outDistance) { return GReturn::FAILURE; }
			static GReturn SqDistancePointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, double& _outDistance) { return GReturn::FAILURE; }
			static GReturn TestPointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToCircle2D(MATH2D::GVECTOR2D _point, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToLine2D(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToCircle2D(MATH2D::GLINE2D _line, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToRectangle2D(MATH2D::GLINE2D _line, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestLineToPolygon2D(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestCircleToCircle2D(MATH2D::GCIRCLE2D _circle1, MATH2D::GCIRCLE2D _circle2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestRectangleToRectangle2D(MATH2D::GRECTANGLE2D _rectangle1, MATH2D::GRECTANGLE2D _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestRectangleToPolygon2D(MATH2D::GRECTANGLE2D _rectangle, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn TestPolygonToPolygon2D(MATH2D::GVECTOR2D* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2D* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::FAILURE; }
			static GReturn FindBarycentricD(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D _trianglePoint1, MATH2D::GVECTOR2D _trianglePoint2, MATH2D::GVECTOR2D _trianglePoint3, MATH2D::GBARYCENTRICD& _outBarycentric) { return GReturn::FAILURE; }
		};
	}
}