//! The namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware internal interfaces must belong.
	namespace I
	{
		class GCollision2DImplementation : public virtual GCollision2DInterface, private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}

			// Floats
			static GReturn ImplicitLineEquationF(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outEquationResult)
			{
				//									A				      x                       B                    y                                       C
				_outEquationResult = ((_line.start.y - _line.end.y) * _point.x) + ((_line.end.x - _line.start.x) * _point.y) + ((_line.start.x * _line.end.y) - (_line.start.y * _line.end.x));

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outDistance)
			{
				float sqLineLength = SqDistancePointToPointF(_line.start, _line.end);

				// _line.start == _line.end case
				if (G2D_COMPARISON_STANDARD_F(sqLineLength, 0.0f))
				{
					_outDistance = SqDistancePointToPointF(_point, _line.start);
					return GReturn::SUCCESS;
				}

				// Consider the line extending the segment vw, parameterized as: v + t(w - v).
				// We find projection of _point onto the line.
				// It falls at t where t = dot((p-v), (w-v)) / ||w-v||^2
				// We clamp t from 0-1 to handle points outside the segment vw.

				MATH2D::GVECTOR2F PminusV{ _point.x - _line.start.x, _point.y - _line.start.y };
				MATH2D::GVECTOR2F WminusV{ _line.end.x - _line.start.x, _line.end.y - _line.start.y };

				float dot = (PminusV.x * WminusV.x) + (PminusV.y * WminusV.y);

				// clamp t
				float t = G2D_MAX(0.0f, G2D_MIN(1.0f, dot / sqLineLength));

				// projection falls on the segment
				MATH2D::GVECTOR2F projection{ (WminusV.x * t) + _line.start.x, (WminusV.y * t) + _line.start.y };

				_outDistance = SqDistancePointToPointF(_point, projection);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, float& _outDistance)
			{
				// implementation is based around the fact that our rectangles are axis-aligned

				// center of rectangle
				MATH2D::GVECTOR2F c = { (_rectangle.min.x + _rectangle.max.x) / 2.0f, (_rectangle.min.y + _rectangle.max.y) / 2.0f };
				float width = G2D_ABS(_rectangle.min.x - _rectangle.max.x);
				float height = G2D_ABS(_rectangle.min.y - _rectangle.min.y);

				float dx = G2D_MAX(G2D_ABS(_point.x - c.x) - width / 2.0f, 0.0f);
				float dy = G2D_MAX(G2D_ABS(_point.y - c.y) - height / 2.0f, 0.0f);

				_outDistance = dx * dx + dy * dy;

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, float& _outDistance)
			{
				GCollisionCheck2D collisionResult;
				TestPointToPolygon2F(_point, _polygon, _numVerts, collisionResult);

				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outDistance = 0.0f;
					return GReturn::SUCCESS;
				}

				unsigned int j = _numVerts - 1;
				float smallestDistance = 3.402823466e+38F; // float max
				float temp;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					SqDistancePointToLine2F(_point, { _polygon[i], _polygon[j] }, temp);
					smallestDistance = G2D_MIN(smallestDistance, temp);
				}

				_outDistance = smallestDistance;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, GCollisionCheck2D& _outResult)
			{
				// handle cases where point is at start or end of line and where line is actually a point
				if (PointsAreEqualF(_point, _line.start) || PointsAreEqualF(_point, _line.end))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				float equationResult;
				ImplicitLineEquationF(_point, _line, equationResult);

				GCollisionCheck2D collisionResult = GCollisionCheck2D::NO_COLLISION;
				if (G2D_COMPARISON_STANDARD_F(equationResult, 0.0f)) // if the point is on the vector of the line
				{
					if (_line.start.x == _line.end.x) // if its a vertical line with undefined slope
					{
						if (_line.start.y < _line.end.y) // if the slope would be positive
						{
							if (_line.start.y < _point.y && _point.y < _line.end.y) // if our point is between the start and end points, its on the line segment
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
						else // the slope would be negative so swap the start and end points in the next comparision
						{
							if (_line.end.y < _point.y && _point.y < _line.start.y)
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
					}
					else // if its a horizontal line with 0 slope, or any other line
					{
						if (_line.start.x < _line.end.x) // if the slope would be positive
						{
							if (_line.start.x < _point.x && _point.x < _line.end.x) // if our point is between the start and end points, its on the line segment
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
						else // the slope would be negative so swap the start and end points in the next comparision
						{
							if (_line.end.x < _point.x && _point.x < _line.start.x)
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
					}
				}
				else if (equationResult > 0.0f) { collisionResult = GCollisionCheck2D::ABOVE; }
				else { collisionResult = GCollisionCheck2D::BELOW; }

				_outResult = collisionResult;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToCircle2F(MATH2D::GVECTOR2F _point, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult)
			{
				_outResult = SqDistancePointToPointF(_point, _circle.pos) <= (_circle.radius * _circle.radius)
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult)
			{
				// find bottom left and top right corners of rectangle
				MATH2D::GVECTOR2F botLeft{ G2D_MIN(_rectangle.min.x, _rectangle.max.x), G2D_MIN(_rectangle.min.y, _rectangle.max.y) };
				MATH2D::GVECTOR2F topRight{ G2D_MAX(_rectangle.min.x, _rectangle.max.x), G2D_MAX(_rectangle.min.y, _rectangle.max.y) };

				if (_point.x >= botLeft.x &&
					_point.x <= topRight.x &&
					_point.y >= botLeft.y &&
					_point.y <= topRight.y)
				{
					_outResult = GCollisionCheck2D::COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck2D::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				bool collision = false;
				unsigned int j = _numVerts - 1;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					// in any compiler that implements C, when executing the code a && b, if a is false, then b must not be evaluated, which means we don't have to check for divide-by-zero
					if (((_polygon[i].y > _point.y) != (_polygon[j].y > _point.y)) &&
						(_point.x < (_polygon[j].x - _polygon[i].x) * (_point.y - _polygon[i].y) / (_polygon[j].y - _polygon[i].y) + _polygon[i].x))
					{
						collision = !collision;
					}
				}

				_outResult = collision == true ? GCollisionCheck2D::COLLISION : GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToLine2F(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, GCollisionCheck2D& _outResult)
			{
				// line equation for line1: Ax + By = C
				float A1 = _line1.end.y - _line1.start.y;
				float B1 = _line1.start.x - _line1.end.x;
				float C1 = A1 * _line1.start.x + B1 * _line1.start.y;

				// line equation for line2
				float A2 = _line2.end.y - _line2.start.y;
				float B2 = _line2.start.x - _line2.end.x;
				float C2 = A2 * _line2.start.x + B2 * _line2.start.y;

				float det = A1 * B2 - A2 * B1;

				if (det == 0) // lines are parallel. need to check if one line is a sub-segment of the other
				{
					if (A1 * _line2.start.x + B1 * _line2.start.y == C1 ||
						A1 * _line2.end.x + B1 * _line2.end.y == C1 ||
						A2 * _line1.start.x + B2 * _line1.start.y == C2 ||
						A2 * _line1.end.x + B2 * _line1.end.y == C2)
					{
						_outResult = GCollisionCheck2D::COLLISION;
					}
					else
					{
						_outResult = GCollisionCheck2D::NO_COLLISION;
					}
				}
				else
				{
					// lines are not parallel, find the intersection
					float x = (B2 * C1 - B1 * C2) / det;
					float y = (A1 * C2 - A2 * C1) / det;

					// find minXY and maxXY for both lines
					MATH2D::GVECTOR2F min1{ G2D_MIN(_line1.start.x, _line1.end.x), G2D_MIN(_line1.start.y, _line1.end.y) };
					MATH2D::GVECTOR2F max1{ G2D_MAX(_line1.start.x, _line1.end.x), G2D_MAX(_line1.start.y, _line1.end.y) };
					MATH2D::GVECTOR2F min2{ G2D_MIN(_line2.start.x, _line2.end.x), G2D_MIN(_line2.start.y, _line2.end.y) };
					MATH2D::GVECTOR2F max2{ G2D_MAX(_line2.start.x, _line2.end.x), G2D_MAX(_line2.start.y, _line2.end.y) };

					// test if the intersection falls within both line segments
					if (x >= min1.x &&
						x >= min2.x &&
						x <= max1.x &&
						x <= max2.x &&
						y >= min1.y &&
						y >= min2.y &&
						y <= max1.y &&
						y <= max2.y)
					{
						_outResult = GCollisionCheck2D::COLLISION;
					}
					else
					{
						_outResult = GCollisionCheck2D::NO_COLLISION;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToCircle2F(MATH2D::GLINE2F _line, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult)
			{
				float sqDistanceToLine;
				SqDistancePointToLine2F(_circle.pos, _line, sqDistanceToLine);

				_outResult = sqDistanceToLine <= _circle.radius * _circle.radius
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToRectangle2F(MATH2D::GLINE2F _line, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult)
			{
				// handle case where both start and/or end of _line are inside _rectangle
				float distanceFromStartToRect, distanceFromEndToRect;

				SqDistancePointToRectangle2F(_line.start, _rectangle, distanceFromStartToRect);
				if (G2D_COMPARISON_STANDARD_F(distanceFromStartToRect, 0.0f))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				SqDistancePointToRectangle2F(_line.end, _rectangle, distanceFromEndToRect);
				if (G2D_COMPARISON_STANDARD_F(distanceFromEndToRect, 0.0f))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}



				// handle everything else
				GCollisionCheck2D left, right, top, bottom;

				TestLineToLine2F(_line, { _rectangle.min, {_rectangle.min.x, _rectangle.max.y} }, left);
				if (left == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2F(_line, { {_rectangle.max.x, _rectangle.min.y}, _rectangle.max }, right);
				if (right == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2F(_line, { {_rectangle.min.x, _rectangle.max.y}, _rectangle.max }, top);
				if (top == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2F(_line, { _rectangle.min, {_rectangle.max.x, _rectangle.min.y} }, bottom);
				if (bottom == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToPolygon2F(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				// handle case where start and/or end of _line are inside _polygon
				GCollisionCheck2D cStart, cEnd;

				TestPointToPolygon2F(_line.start, _polygon, _numVerts, cStart);
				if (cStart == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestPointToPolygon2F(_line.start, _polygon, _numVerts, cEnd);
				if (cEnd == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}


				// handle everything else
				unsigned int j = _numVerts - 1;
				GCollisionCheck2D lineCollision;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					TestLineToLine2F(_line, { _polygon[i], _polygon[j] }, lineCollision);
					if (lineCollision == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCircleToCircle2F(MATH2D::GCIRCLE2F _circle1, MATH2D::GCIRCLE2F _circle2, GCollisionCheck2D& _outResult)
			{
				_outResult = SqDistancePointToPointF(_circle1.pos, _circle2.pos) <= (_circle1.radius + _circle2.radius) * (_circle1.radius + _circle2.radius)
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestRectangleToRectangle2F(MATH2D::GRECTANGLE2F _rectangle1, MATH2D::GRECTANGLE2F _rectangle2, GCollisionCheck2D& _outResult)
			{
				// find bottom left and top right corners of rectangles and width and height
				MATH2D::GVECTOR2F botLeft1{ G2D_MIN(_rectangle1.min.x, _rectangle1.max.x), G2D_MIN(_rectangle1.min.y, _rectangle1.max.y) };
				MATH2D::GVECTOR2F topRight1{ G2D_MAX(_rectangle1.min.x, _rectangle1.max.x), G2D_MAX(_rectangle1.min.y, _rectangle1.max.y) };
				MATH2D::GVECTOR2F botLeft2{ G2D_MIN(_rectangle2.min.x, _rectangle2.max.x), G2D_MIN(_rectangle2.min.y, _rectangle2.max.y) };
				MATH2D::GVECTOR2F topRight2{ G2D_MAX(_rectangle2.min.x, _rectangle2.max.x), G2D_MAX(_rectangle2.min.y, _rectangle2.max.y) };

				if (botLeft1.x > topRight2.x ||
					topRight1.x < botLeft2.x ||
					botLeft1.y > topRight2.y ||
					topRight1.y < botLeft2.y)
				{
					_outResult = GCollisionCheck2D::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck2D::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestRectangleToPolygon2F(MATH2D::GRECTANGLE2F _rectangle, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				GCollisionCheck2D collisionResult;
				// rectangle could be inside polygon
				TestPointToPolygon2F(_rectangle.min, _polygon, _numVerts, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// polygon could be inside rectangle
				TestPointToRectangle2F(_polygon[0], _rectangle, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// test the rectangle to every edge of the polygon
				unsigned int j = _numVerts - 1;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					TestLineToRectangle2F({ _polygon[i], _polygon[j] }, _rectangle, collisionResult);
					if (collisionResult == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPolygonToPolygon2F(MATH2D::GVECTOR2F* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2F* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult)
			{
				GCollisionCheck2D collisionResult;
				// polygon2 could be inside polygon1
				TestPointToPolygon2F(_polygon2[0], _polygon1, _numVerts1, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// test the edges of polygon1 to polygon2
				unsigned int j = _numVerts1 - 1;
				for (unsigned int i = 0; i < _numVerts1; j = i++)
				{
					TestLineToPolygon2F({ _polygon1[i], _polygon1[j] }, _polygon2, _numVerts2, collisionResult);
					if (collisionResult == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn FindBarycentricF(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F _trianglePoint1, MATH2D::GVECTOR2F _trianglePoint2, MATH2D::GVECTOR2F _trianglePoint3, MATH2D::GBARYCENTRICF& _outBarycentric)
			{
				float rectArea;
				ImplicitLineEquationF(_trianglePoint1, { _trianglePoint3, _trianglePoint2 }, rectArea);

				if (G2D_COMPARISON_STANDARD_F(rectArea, 0.0f))
				{
					return GReturn::FAILURE; // would get a divide-by-zero error
				}

				float a, b;
				ImplicitLineEquationF(_point, { _trianglePoint3, _trianglePoint2 }, a);
				ImplicitLineEquationF(_point, { _trianglePoint1, _trianglePoint3 }, b);

				_outBarycentric.alpha = a / rectArea;
				_outBarycentric.beta = b / rectArea;
				_outBarycentric.gamma = 1.0f - _outBarycentric.alpha - _outBarycentric.beta;

				return GReturn::SUCCESS;
			}




			// Doubles
			static GReturn ImplicitLineEquationD(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outEquationResult)
			{
				//									A				      x                       B                    y                                       C
				_outEquationResult = ((_line.start.y - _line.end.y) * _point.x) + ((_line.end.x - _line.start.x) * _point.y) + ((_line.start.x * _line.end.y) - (_line.start.y * _line.end.x));

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outDistance)
			{
				double sqLineLength = SqDistancePointToPointD(_line.start, _line.end);

				// _line.start == _line.end case
				if (G2D_COMPARISON_STANDARD_D(sqLineLength, 0.0))
				{
					_outDistance = SqDistancePointToPointD(_point, _line.start);
					return GReturn::SUCCESS;
				}

				// Consider the line extending the segment vw, parameterized as: v + t(w - v).
				// We find projection of _point onto the line.
				// It falls at t where t = dot((p-v), (w-v)) / ||w-v||^2
				// We clamp t from 0-1 to handle points outside the segment vw.

				MATH2D::GVECTOR2D PminusV{ _point.x - _line.start.x, _point.y - _line.start.y };
				MATH2D::GVECTOR2D WminusV{ _line.end.x - _line.start.x, _line.end.y - _line.start.y };

				double dot = (PminusV.x * WminusV.x) + (PminusV.y * WminusV.y);

				// clamp t
				double t = G2D_MAX(0.0, G2D_MIN(1.0, dot / sqLineLength));

				// projection falls on the segment
				MATH2D::GVECTOR2D projection{ (WminusV.x * t) + _line.start.x, (WminusV.y * t) + _line.start.y };

				_outDistance = SqDistancePointToPointD(_point, projection);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, double& _outDistance)
			{
				// implementation is based around the fact that our rectangles are axis-aligned

				// center of rectangle
				MATH2D::GVECTOR2D c = { (_rectangle.min.x + _rectangle.max.x) / 2.0, (_rectangle.min.y + _rectangle.max.y) / 2.0 };
				double width = G2D_ABS(_rectangle.min.x - _rectangle.max.x);
				double height = G2D_ABS(_rectangle.min.y - _rectangle.min.y);

				double dx = G2D_MAX(G2D_ABS(_point.x - c.x) - width / 2.0, 0.0);
				double dy = G2D_MAX(G2D_ABS(_point.y - c.y) - height / 2.0, 0.0);

				_outDistance = dx * dx + dy * dy;

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, double& _outDistance)
			{
				GCollisionCheck2D collisionResult;
				TestPointToPolygon2D(_point, _polygon, _numVerts, collisionResult);

				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outDistance = 0.0;
					return GReturn::SUCCESS;
				}

				unsigned int j = _numVerts - 1;
				double smallestDistance = 1.7976931348623158e+308; // double max
				double temp;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					SqDistancePointToLine2D(_point, { _polygon[i], _polygon[j] }, temp);
					smallestDistance = G2D_MIN(smallestDistance, temp);
				}

				_outDistance = smallestDistance;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, GCollisionCheck2D& _outResult)
			{
				if (PointsAreEqualD(_point, _line.start) || PointsAreEqualD(_point, _line.end))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				double equationResult;
				ImplicitLineEquationD(_point, _line, equationResult);

				GCollisionCheck2D collisionResult = GCollisionCheck2D::NO_COLLISION;
				if (G2D_COMPARISON_STANDARD_D(equationResult, 0.0)) // if the point is on the vector of the line
				{
					if (_line.start.x == _line.end.x) // if its a vertical line with undefined slope
					{
						if (_line.start.y < _line.end.y) // if the slope would be positive
						{
							if (_line.start.y < _point.y && _point.y < _line.end.y) // if our point is between the start and end points, its on the line segment
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
						else // the slope would be negative so swap the start and end points in the next comparision
						{
							if (_line.end.y < _point.y && _point.y < _line.start.y)
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
					}
					else // if its a horizontal line with 0 slope, or any other line
					{
						if (_line.start.x < _line.end.x) // if the slope would be positive
						{
							if (_line.start.x < _point.x && _point.x < _line.end.x) // if our point is between the start and end points, its on the line segment
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
						else // the slope would be negative so swap the start and end points in the next comparision
						{
							if (_line.end.x < _point.x && _point.x < _line.start.x)
							{
								collisionResult = GCollisionCheck2D::COLLISION;
							}
						}
					}
				}
				else if (equationResult > 0.0) { collisionResult = GCollisionCheck2D::ABOVE; }
				else { collisionResult = GCollisionCheck2D::BELOW; }

				_outResult = collisionResult;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToCircle2D(MATH2D::GVECTOR2D _point, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult)
			{
				_outResult = SqDistancePointToPointD(_point, _circle.pos) <= (_circle.radius * _circle.radius)
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult)
			{
				// find bottom left and top right corners of rectangle
				MATH2D::GVECTOR2D botLeft{ G2D_MIN(_rectangle.min.x, _rectangle.max.x), G2D_MIN(_rectangle.min.y, _rectangle.max.y) };
				MATH2D::GVECTOR2D topRight{ G2D_MAX(_rectangle.min.x, _rectangle.max.x), G2D_MAX(_rectangle.min.y, _rectangle.max.y) };

				if (_point.x >= botLeft.x &&
					_point.x <= topRight.x &&
					_point.y >= botLeft.y &&
					_point.y <= topRight.y)
				{
					_outResult = GCollisionCheck2D::COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck2D::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				bool collision = false;
				unsigned int j = _numVerts - 1;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					// when executing the code a && b, if a is false, then b must not be evaluated, which means we dont have to check for devide-by-zero
					if (((_polygon[i].y > _point.y) != (_polygon[j].y > _point.y)) &&
						(_point.x < (_polygon[j].x - _polygon[i].x) * (_point.y - _polygon[i].y) / (_polygon[j].y - _polygon[i].y) + _polygon[i].x))
					{
						collision = !collision;
					}
				}

				_outResult = collision == true ? GCollisionCheck2D::COLLISION : GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToLine2D(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, GCollisionCheck2D& _outResult)
			{
				// line equation for line1: Ax + By = C
				double A1 = _line1.end.y - _line1.start.y;
				double B1 = _line1.start.x - _line1.end.x;
				double C1 = A1 * _line1.start.x + B1 * _line1.start.y;

				// line equation for line2
				double A2 = _line2.end.y - _line2.start.y;
				double B2 = _line2.start.x - _line2.end.x;
				double C2 = A2 * _line2.start.x + B2 * _line2.start.y;

				double det = A1 * B2 - A2 * B1;

				if (det == 0) // lines are parallel. need to check if one line is a sub-segment of the other
				{
					if (A1 * _line2.start.x + B1 * _line2.start.y == C1 ||
						A1 * _line2.end.x + B1 * _line2.end.y == C1 ||
						A2 * _line1.start.x + B2 * _line1.start.y == C2 ||
						A2 * _line1.end.x + B2 * _line1.end.y == C2)
					{
						_outResult = GCollisionCheck2D::COLLISION;
					}
					else
					{
						_outResult = GCollisionCheck2D::NO_COLLISION;
					}
				}
				else
				{
					// lines are not parallel, find the intersection
					double x = (B2 * C1 - B1 * C2) / det;
					double y = (A1 * C2 - A2 * C1) / det;

					// find minXY and maxXY for both lines
					MATH2D::GVECTOR2D min1{ G2D_MIN(_line1.start.x, _line1.end.x), G2D_MIN(_line1.start.y, _line1.end.y) };
					MATH2D::GVECTOR2D max1{ G2D_MAX(_line1.start.x, _line1.end.x), G2D_MAX(_line1.start.y, _line1.end.y) };
					MATH2D::GVECTOR2D min2{ G2D_MIN(_line2.start.x, _line2.end.x), G2D_MIN(_line2.start.y, _line2.end.y) };
					MATH2D::GVECTOR2D max2{ G2D_MAX(_line2.start.x, _line2.end.x), G2D_MAX(_line2.start.y, _line2.end.y) };

					// test if the intersection falls within both line segments
					if (x >= min1.x &&
						x >= min2.x &&
						x <= max1.x &&
						x <= max2.x &&
						y >= min1.y &&
						y >= min2.y &&
						y <= max1.y &&
						y <= max2.y)
					{
						_outResult = GCollisionCheck2D::COLLISION;
					}
					else
					{
						_outResult = GCollisionCheck2D::NO_COLLISION;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToCircle2D(MATH2D::GLINE2D _line, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult)
			{
				double sqDistanceToLine;
				SqDistancePointToLine2D(_circle.pos, _line, sqDistanceToLine);

				_outResult = sqDistanceToLine <= _circle.radius * _circle.radius
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToRectangle2D(MATH2D::GLINE2D _line, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult)
			{
				// handle case where both start and/or end of _line are inside _rectangle
				double distanceFromStartToRect, distanceFromEndToRect;

				SqDistancePointToRectangle2D(_line.start, _rectangle, distanceFromStartToRect);
				if (G2D_COMPARISON_STANDARD_D(distanceFromStartToRect, 0.0))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				SqDistancePointToRectangle2D(_line.end, _rectangle, distanceFromEndToRect);
				if (G2D_COMPARISON_STANDARD_D(distanceFromEndToRect, 0.0))
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}


				// handle everything else
				GCollisionCheck2D left, right, top, bottom;

				TestLineToLine2D(_line, { _rectangle.min, {_rectangle.min.x, _rectangle.max.y} }, left);
				if (left == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2D(_line, { {_rectangle.max.x, _rectangle.min.y}, _rectangle.max }, right);
				if (right == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2D(_line, { {_rectangle.min.x, _rectangle.max.y}, _rectangle.max }, top);
				if (top == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestLineToLine2D(_line, { _rectangle.min, {_rectangle.max.x, _rectangle.min.y} }, bottom);
				if (bottom == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToPolygon2D(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				// handle case where start and/or end of _line are inside _polygon
				GCollisionCheck2D cStart, cEnd;

				TestPointToPolygon2D(_line.start, _polygon, _numVerts, cStart);
				if (cStart == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				TestPointToPolygon2D(_line.start, _polygon, _numVerts, cEnd);
				if (cEnd == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}


				// handle everything else
				unsigned int j = _numVerts - 1;
				GCollisionCheck2D lineCollision;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					TestLineToLine2D(_line, { _polygon[i], _polygon[j] }, lineCollision);
					if (lineCollision == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCircleToCircle2D(MATH2D::GCIRCLE2D _circle1, MATH2D::GCIRCLE2D _circle2, GCollisionCheck2D& _outResult)
			{
				_outResult = SqDistancePointToPointD(_circle1.pos, _circle2.pos) <= (_circle1.radius + _circle2.radius) * (_circle1.radius + _circle2.radius)
					? GCollisionCheck2D::COLLISION
					: GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestRectangleToRectangle2D(MATH2D::GRECTANGLE2D _rectangle1, MATH2D::GRECTANGLE2D _rectangle2, GCollisionCheck2D& _outResult)
			{
				// find bottom left and top right corners of rectangles and width and height
				MATH2D::GVECTOR2D botLeft1{ G2D_MIN(_rectangle1.min.x, _rectangle1.max.x), G2D_MIN(_rectangle1.min.y, _rectangle1.max.y) };
				MATH2D::GVECTOR2D topRight1{ G2D_MAX(_rectangle1.min.x, _rectangle1.max.x), G2D_MAX(_rectangle1.min.y, _rectangle1.max.y) };
				MATH2D::GVECTOR2D botLeft2{ G2D_MIN(_rectangle2.min.x, _rectangle2.max.x), G2D_MIN(_rectangle2.min.y, _rectangle2.max.y) };
				MATH2D::GVECTOR2D topRight2{ G2D_MAX(_rectangle2.min.x, _rectangle2.max.x), G2D_MAX(_rectangle2.min.y, _rectangle2.max.y) };

				if (botLeft1.x > topRight2.x ||
					topRight1.x < botLeft2.x ||
					botLeft1.y > topRight2.y ||
					topRight1.y < botLeft2.y)
				{
					_outResult = GCollisionCheck2D::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck2D::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestRectangleToPolygon2D(MATH2D::GRECTANGLE2D _rectangle, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult)
			{
				GCollisionCheck2D collisionResult;
				// rectangle could be inside polygon
				TestPointToPolygon2D(_rectangle.min, _polygon, _numVerts, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// polygon could be inside rectangle
				TestPointToRectangle2D(_polygon[0], _rectangle, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// test the rectangle to every edge of the polygon
				unsigned int j = _numVerts - 1;
				for (unsigned int i = 0; i < _numVerts; j = i++)
				{
					TestLineToRectangle2D({ _polygon[i], _polygon[j] }, _rectangle, collisionResult);
					if (collisionResult == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPolygonToPolygon2D(MATH2D::GVECTOR2D* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2D* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult)
			{
				GCollisionCheck2D collisionResult;
				// polygon2 could be inside polygon1
				TestPointToPolygon2D(_polygon2[0], _polygon1, _numVerts1, collisionResult);
				if (collisionResult == GCollisionCheck2D::COLLISION)
				{
					_outResult = GCollisionCheck2D::COLLISION;
					return GReturn::SUCCESS;
				}

				// test the edges of polygon1 to polygon2
				unsigned int j = _numVerts1 - 1;
				for (unsigned int i = 0; i < _numVerts1; j = i++)
				{
					TestLineToPolygon2D({ _polygon1[i], _polygon1[j] }, _polygon2, _numVerts2, collisionResult);
					if (collisionResult == GCollisionCheck2D::COLLISION)
					{
						_outResult = GCollisionCheck2D::COLLISION;
						return GReturn::SUCCESS;
					}
				}

				_outResult = GCollisionCheck2D::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn FindBarycentricD(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D _trianglePoint1, MATH2D::GVECTOR2D _trianglePoint2, MATH2D::GVECTOR2D _trianglePoint3, MATH2D::GBARYCENTRICD& _outBarycentric)
			{
				double rectArea;
				ImplicitLineEquationD(_trianglePoint1, { _trianglePoint3, _trianglePoint2 }, rectArea);

				if (G2D_COMPARISON_STANDARD_D(rectArea, 0.0))
				{
					return GReturn::FAILURE; // would get a divide-by-zero error
				}

				double a, b;
				ImplicitLineEquationD(_point, { _trianglePoint3, _trianglePoint2 }, a);
				ImplicitLineEquationD(_point, { _trianglePoint1, _trianglePoint3 }, b);

				_outBarycentric.alpha = a / rectArea;
				_outBarycentric.beta = b / rectArea;
				_outBarycentric.gamma = 1.0 - _outBarycentric.alpha - _outBarycentric.beta;

				return GReturn::SUCCESS;

				return GReturn::SUCCESS;
			}

		private:
			static bool PointsAreEqualF(MATH2D::GVECTOR2F _p1, MATH2D::GVECTOR2F _p2)
			{
				return (G2D_COMPARISON_STANDARD_F(_p1.x, _p2.x) && G2D_COMPARISON_STANDARD_F(_p1.y, _p2.y));
			}

			static bool PointsAreEqualD(MATH2D::GVECTOR2D _p1, MATH2D::GVECTOR2D _p2)
			{
				return (G2D_COMPARISON_STANDARD_D(_p1.x, _p2.x) && G2D_COMPARISON_STANDARD_D(_p1.y, _p2.y));
			}

			static float SqDistancePointToPointF(MATH2D::GVECTOR2F _p1, MATH2D::GVECTOR2F _p2)
			{
				return ((_p2.x - _p1.x) * (_p2.x - _p1.x)) + ((_p2.y - _p1.y) * (_p2.y - _p1.y));
			}

			static double SqDistancePointToPointD(MATH2D::GVECTOR2D _p1, MATH2D::GVECTOR2D _p2)
			{
				return ((_p2.x - _p1.x) * (_p2.x - _p1.x)) + ((_p2.y - _p1.y) * (_p2.y - _p1.y));
			}
		};
	}
}