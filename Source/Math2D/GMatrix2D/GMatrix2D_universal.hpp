#include <cmath>

namespace GW
{
	namespace I
	{
		class GMatrix2DImplementation : public virtual GMatrix2DInterface, private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}

			// Floats
			static GReturn Add2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] + _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Add3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] + _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Subtract2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] - _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Subtract3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] - _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Multiply2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix)
			{
				_outMatrix.data[0] = _matrix1.data[0] * _matrix2.data[0] + _matrix1.data[1] * _matrix2.data[2];
				_outMatrix.data[1] = _matrix1.data[0] * _matrix2.data[1] + _matrix1.data[1] * _matrix2.data[3];

				_outMatrix.data[2] = _matrix1.data[2] * _matrix2.data[0] + _matrix1.data[3] * _matrix2.data[2];
				_outMatrix.data[3] = _matrix1.data[2] * _matrix2.data[1] + _matrix1.data[3] * _matrix2.data[3];

				return GW::GReturn::SUCCESS;
			}

			static GReturn Multiply3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix)
			{
				_outMatrix.data[0] = _matrix1.data[0] * _matrix2.data[0] + _matrix1.data[1] * _matrix2.data[3] + _matrix1.data[2] * _matrix2.data[6];
				_outMatrix.data[1] = _matrix1.data[0] * _matrix2.data[1] + _matrix1.data[1] * _matrix2.data[4] + _matrix1.data[2] * _matrix2.data[7];
				_outMatrix.data[2] = _matrix1.data[0] * _matrix2.data[2] + _matrix1.data[1] * _matrix2.data[5] + _matrix1.data[2] * _matrix2.data[8];

				_outMatrix.data[3] = _matrix1.data[3] * _matrix2.data[0] + _matrix1.data[4] * _matrix2.data[3] + _matrix1.data[5] * _matrix2.data[6];
				_outMatrix.data[4] = _matrix1.data[3] * _matrix2.data[1] + _matrix1.data[4] * _matrix2.data[4] + _matrix1.data[5] * _matrix2.data[7];
				_outMatrix.data[5] = _matrix1.data[3] * _matrix2.data[2] + _matrix1.data[4] * _matrix2.data[5] + _matrix1.data[5] * _matrix2.data[8];

				_outMatrix.data[6] = _matrix1.data[6] * _matrix2.data[0] + _matrix1.data[7] * _matrix2.data[3] + _matrix1.data[8] * _matrix2.data[6];
				_outMatrix.data[7] = _matrix1.data[6] * _matrix2.data[1] + _matrix1.data[7] * _matrix2.data[4] + _matrix1.data[8] * _matrix2.data[7];
				_outMatrix.data[8] = _matrix1.data[6] * _matrix2.data[2] + _matrix1.data[7] * _matrix2.data[5] + _matrix1.data[8] * _matrix2.data[8];

				return GW::GReturn::SUCCESS;
			}

			static GReturn MatrixXVector2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _vector.data[0] * _matrix.data[0] + _vector.data[1] * _matrix.data[2];
				_outVector.y = _vector.data[0] * _matrix.data[1] + _vector.data[1] * _matrix.data[3];

				return GReturn::SUCCESS;
			}

			static GReturn MatrixXVector3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector)
			{
				_outVector.x = _vector.data[0] * _matrix.data[0] + _vector.data[1] * _matrix.data[3] + _vector.data[2] * _matrix.data[6];
				_outVector.y = _vector.data[0] * _matrix.data[1] + _vector.data[1] * _matrix.data[4] + _vector.data[2] * _matrix.data[7];
				_outVector.z = _vector.data[0] * _matrix.data[2] + _vector.data[1] * _matrix.data[5] + _vector.data[2] * _matrix.data[8];

				return GReturn::SUCCESS;
			}

			static GReturn MultiplyNum2F(MATH2D::GMATRIX2F _matrix, float _scalar, MATH2D::GMATRIX2F& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix.data[i] * _scalar;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn MultiplyNum3F(MATH2D::GMATRIX3F _matrix, float _scalar, MATH2D::GMATRIX3F& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix.data[i] * _scalar;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Determinant2F(MATH2D::GMATRIX2F _matrix, float& _outDeterminant)
			{
				_outDeterminant = _matrix.data[0] * _matrix.data[3] - _matrix.data[1] * _matrix.data[2];

				return GW::GReturn::SUCCESS;
			}

			static GReturn Determinant3F(MATH2D::GMATRIX3F _matrix, float& _outDeterminant)
			{
				float a = (_matrix.data[0] * _matrix.data[4] * _matrix.data[8]) + (_matrix.data[1] * _matrix.data[5] * _matrix.data[6]) + (_matrix.data[2] * _matrix.data[3] * _matrix.data[7]);
				float b = (_matrix.data[6] * _matrix.data[4] * _matrix.data[2]) + (_matrix.data[7] * _matrix.data[5] * _matrix.data[0]) + (_matrix.data[8] * _matrix.data[3] * _matrix.data[1]);

				_outDeterminant = a - b;

				return GReturn::SUCCESS;
			}

			static GReturn Transpose2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix)
			{
				_outMatrix.data[0] = _matrix.data[0];
				_outMatrix.data[1] = _matrix.data[2];
				_outMatrix.data[2] = _matrix.data[1];
				_outMatrix.data[3] = _matrix.data[3];

				return GReturn::SUCCESS;
			}

			static GReturn Transpose3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix)
			{
				_outMatrix.data[0] = _matrix.data[0];
				_outMatrix.data[1] = _matrix.data[3];
				_outMatrix.data[2] = _matrix.data[6];
				_outMatrix.data[3] = _matrix.data[1];
				_outMatrix.data[4] = _matrix.data[4];
				_outMatrix.data[5] = _matrix.data[7];
				_outMatrix.data[6] = _matrix.data[2];
				_outMatrix.data[7] = _matrix.data[5];
				_outMatrix.data[8] = _matrix.data[8];

				return GReturn::SUCCESS;
			}

			static GReturn Inverse2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix)
			{
				float determinant;

				Determinant2F(_matrix, determinant);

				if (G2D_COMPARISON_STANDARD_F(determinant, 0.0f))
				{
					return GReturn::FAILURE;
				}

				_outMatrix.data[0] = _matrix.data[3];
				_outMatrix.data[1] = -_matrix.data[1];
				_outMatrix.data[2] = -_matrix.data[2];
				_outMatrix.data[3] = _matrix.data[0];

				MultiplyNum2F(_outMatrix, 1.0f / determinant, _outMatrix);

				return GReturn::SUCCESS;
			}

			static GReturn Inverse3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix)
			{
				float determinant;

				Determinant3F(_matrix, determinant);

				if (G2D_COMPARISON_STANDARD_F(determinant, 0.0f))
				{
					return GReturn::FAILURE;
				}

				// simultaneously calculate the matrix of minors, convert it to the matrix of cofactors, and then transpose it
				_outMatrix.data[0] = (_matrix.data[4] * _matrix.data[8] - _matrix.data[5] * _matrix.data[7]); // 0
				_outMatrix.data[1] = -(_matrix.data[1] * _matrix.data[8] - _matrix.data[2] * _matrix.data[7]); // 3
				_outMatrix.data[2] = (_matrix.data[1] * _matrix.data[5] - _matrix.data[2] * _matrix.data[4]); // 6

				_outMatrix.data[3] = -(_matrix.data[3] * _matrix.data[8] - _matrix.data[5] * _matrix.data[6]); // 1
				_outMatrix.data[4] = (_matrix.data[0] * _matrix.data[8] - _matrix.data[2] * _matrix.data[6]); // 4
				_outMatrix.data[5] = -(_matrix.data[0] * _matrix.data[5] - _matrix.data[2] * _matrix.data[3]); // 7

				_outMatrix.data[6] = (_matrix.data[3] * _matrix.data[7] - _matrix.data[4] * _matrix.data[6]); // 2
				_outMatrix.data[7] = -(_matrix.data[0] * _matrix.data[7] - _matrix.data[1] * _matrix.data[6]); // 5
				_outMatrix.data[8] = (_matrix.data[0] * _matrix.data[4] - _matrix.data[1] * _matrix.data[3]); // 8

				MultiplyNum3F(_outMatrix, 1.0f / determinant, _outMatrix);

				return GReturn::SUCCESS;
			}

			static GReturn GetRotation2F(MATH2D::GMATRIX2F _matrix, float& _outRadians)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outRadians = _matrix.data[1] > 0.0f ? acos(_matrix.data[0] / r) : -acos(_matrix.data[0] / r);
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[2], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f))
				{
					float s = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3]);
					_outRadians = G2D_PI_F / 2.0f - (_matrix.data[3] > 0.0f ? acos(-_matrix.data[3] / s) : -acos(_matrix.data[3] / s));
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetRotation3F(MATH2D::GMATRIX3F _matrix, float& _outRadians)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outRadians = _matrix.data[1] > 0.0f ? acos(_matrix.data[0] / r) : -acos(_matrix.data[0] / r);
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[4], 0.0f))
				{
					float s = sqrt(_matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4]);
					_outRadians = G2D_PI_F / 2.0f - (_matrix.data[4] > 0.0f ? acos(-_matrix.data[4] / s) : -acos(_matrix.data[4] / s));
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetTranslation3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outVector)
			{
				_outVector.x = _matrix.row3.x;
				_outVector.y = _matrix.row3.y;

				return GReturn::SUCCESS;
			}

			static GReturn GetScale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outScale)
			{
				float delta = _matrix.data[0] * _matrix.data[3] - _matrix.data[1] * _matrix.data[2];

				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outScale = { r, delta / r };
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[2], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f))
				{
					float s = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3]);
					_outScale = { delta / s, s };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetScale3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outScale)
			{
				float delta = _matrix.data[0] * _matrix.data[4] - _matrix.data[1] * _matrix.data[3];

				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outScale = { r, delta / r };
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[4], 0.0f))
				{
					float s = sqrt(_matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4]);
					_outScale = { delta / s, s };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetSkew2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outSkew)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = _matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1];
					_outSkew = { atanf((_matrix.data[0] * _matrix.data[1] + _matrix.data[2] * _matrix.data[3]) / r), 0.0f };
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[2], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f))
				{
					float s = _matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3];
					_outSkew = { 0.0f, atanf((_matrix.data[0] * _matrix.data[2] + _matrix.data[1] * _matrix.data[3]) / s) };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetSkew3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outSkew)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_F(_matrix.data[0], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[1], 0.0f))
				{
					float r = _matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1];
					_outSkew = { atanf((_matrix.data[0] * _matrix.data[1] + _matrix.data[3] * _matrix.data[4]) / r), 0.0f };
				}
				else if (!G2D_COMPARISON_STANDARD_F(_matrix.data[3], 0.0f) || !G2D_COMPARISON_STANDARD_F(_matrix.data[4], 0.0f))
				{
					float s = _matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4];
					_outSkew = { 0.0f, atanf((_matrix.data[0] * _matrix.data[3] + _matrix.data[1] * _matrix.data[4]) / s) };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn Rotate2F(MATH2D::GMATRIX2F _matrix, float _radians, MATH2D::GMATRIX2F& _outMatrix)
			{
				float c = cos(_radians);
				float s = sin(_radians);
				MATH2D::GMATRIX2F rotationMatrix = MATH2D::GIdentityMatrix2F;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[2] = -s;
				rotationMatrix.data[3] = c;

				Multiply2F(_matrix, rotationMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateGlobal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix)
			{
				float c = cos(_radians);
				float s = sin(_radians);
				MATH2D::GMATRIX3F rotationMatrix = MATH2D::GIdentityMatrix3F;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[3] = -s;
				rotationMatrix.data[4] = c;

				// store translation
				MATH2D::GVECTOR3F translation = _matrix.row3;

				Multiply3F(_matrix, rotationMatrix, _outMatrix);

				// restore translation
				_outMatrix.row3 = translation;

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateLocal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix)
			{
				float c = cos(_radians);
				float s = sin(_radians);
				MATH2D::GMATRIX3F rotationMatrix = MATH2D::GIdentityMatrix3F;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[3] = -s;
				rotationMatrix.data[4] = c;

				Multiply3F(rotationMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix)
			{
				MATH2D::GMATRIX3F translationMatrix = MATH2D::GIdentityMatrix3F;
				translationMatrix.data[6] = _vector.x;
				translationMatrix.data[7] = _vector.y;

				Multiply3F(_matrix, translationMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix)
			{
				MATH2D::GMATRIX3F translationMatrix = MATH2D::GIdentityMatrix3F;
				translationMatrix.data[6] = _vector.x;
				translationMatrix.data[7] = _vector.y;

				Multiply3F(translationMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Scale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F& _outMatrix)
			{
				MATH2D::GMATRIX2F scaleMatrix = MATH2D::GIdentityMatrix2F;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[3] = _vector.y;

				Multiply2F(_matrix, scaleMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix)
			{
				MATH2D::GMATRIX3F scaleMatrix = MATH2D::GIdentityMatrix3F;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[4] = _vector.y;

				// store translation
				MATH2D::GVECTOR3F translation = _matrix.row3;

				Multiply3F(_matrix, scaleMatrix, _outMatrix);

				// restore translation
				_outMatrix.row3 = translation;

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix)
			{
				MATH2D::GMATRIX3F scaleMatrix = MATH2D::GIdentityMatrix3F;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[4] = _vector.y;

				Multiply3F(scaleMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Lerp2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, float _ratio, MATH2D::GMATRIX2F& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = G2D_LERP(_matrix1.data[i], _matrix2.data[i], _ratio);
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Lerp3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, float _ratio, MATH2D::GMATRIX3F& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = G2D_LERP(_matrix1.data[i], _matrix2.data[i], _ratio);
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelative2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix)
			{
				MATH2D::GMATRIX2F inverseMatrix2;
				Inverse2F(_matrix2, inverseMatrix2);

				Multiply2F(_matrix1, inverseMatrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelative3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix)
			{
				MATH2D::GMATRIX3F inverseMatrix2;
				Inverse3F(_matrix2, inverseMatrix2);

				Multiply3F(_matrix1, inverseMatrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeSeparate2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix)
			{
				Multiply2F(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeSeparate3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix)
			{
				Multiply3F(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Upgrade2(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2D& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = static_cast<double>(_matrix.data[i]);
				}

				return GReturn::SUCCESS;
			}

			static GReturn Upgrade3(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3D& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = static_cast<double>(_matrix.data[i]);
				}

				return GReturn::SUCCESS;
			}











			// Doubles
			static GReturn Add2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] + _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Add3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] + _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Subtract2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] - _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Subtract3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix1.data[i] - _matrix2.data[i];
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Multiply2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix)
			{
				_outMatrix.data[0] = _matrix1.data[0] * _matrix2.data[0] + _matrix1.data[1] * _matrix2.data[2];
				_outMatrix.data[1] = _matrix1.data[0] * _matrix2.data[1] + _matrix1.data[1] * _matrix2.data[3];

				_outMatrix.data[2] = _matrix1.data[2] * _matrix2.data[0] + _matrix1.data[3] * _matrix2.data[2];
				_outMatrix.data[3] = _matrix1.data[2] * _matrix2.data[1] + _matrix1.data[3] * _matrix2.data[3];

				return GW::GReturn::SUCCESS;
			}

			static GReturn Multiply3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix)
			{
				_outMatrix.data[0] = _matrix1.data[0] * _matrix2.data[0] + _matrix1.data[1] * _matrix2.data[3] + _matrix1.data[2] * _matrix2.data[6];
				_outMatrix.data[1] = _matrix1.data[0] * _matrix2.data[1] + _matrix1.data[1] * _matrix2.data[4] + _matrix1.data[2] * _matrix2.data[7];
				_outMatrix.data[2] = _matrix1.data[0] * _matrix2.data[2] + _matrix1.data[1] * _matrix2.data[5] + _matrix1.data[2] * _matrix2.data[8];

				_outMatrix.data[3] = _matrix1.data[3] * _matrix2.data[0] + _matrix1.data[4] * _matrix2.data[3] + _matrix1.data[5] * _matrix2.data[6];
				_outMatrix.data[4] = _matrix1.data[3] * _matrix2.data[1] + _matrix1.data[4] * _matrix2.data[4] + _matrix1.data[5] * _matrix2.data[7];
				_outMatrix.data[5] = _matrix1.data[3] * _matrix2.data[2] + _matrix1.data[4] * _matrix2.data[5] + _matrix1.data[5] * _matrix2.data[8];

				_outMatrix.data[6] = _matrix1.data[6] * _matrix2.data[0] + _matrix1.data[7] * _matrix2.data[3] + _matrix1.data[8] * _matrix2.data[6];
				_outMatrix.data[7] = _matrix1.data[6] * _matrix2.data[1] + _matrix1.data[7] * _matrix2.data[4] + _matrix1.data[8] * _matrix2.data[7];
				_outMatrix.data[8] = _matrix1.data[6] * _matrix2.data[2] + _matrix1.data[7] * _matrix2.data[5] + _matrix1.data[8] * _matrix2.data[8];

				return GW::GReturn::SUCCESS;
			}

			static GReturn MatrixXVector2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _vector.data[0] * _matrix.data[0] + _vector.data[1] * _matrix.data[2];
				_outVector.y = _vector.data[0] * _matrix.data[1] + _vector.data[1] * _matrix.data[3];

				return GReturn::SUCCESS;
			}

			static GReturn MatrixXVector3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector)
			{
				_outVector.x = _vector.data[0] * _matrix.data[0] + _vector.data[1] * _matrix.data[3] + _vector.data[2] * _matrix.data[6];
				_outVector.y = _vector.data[0] * _matrix.data[1] + _vector.data[1] * _matrix.data[4] + _vector.data[2] * _matrix.data[7];
				_outVector.z = _vector.data[0] * _matrix.data[2] + _vector.data[1] * _matrix.data[5] + _vector.data[2] * _matrix.data[8];

				return GReturn::SUCCESS;
			}

			static GReturn MultiplyNum2D(MATH2D::GMATRIX2D _matrix, double _scalar, MATH2D::GMATRIX2D& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = _matrix.data[i] * _scalar;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn MultiplyNum3D(MATH2D::GMATRIX3D _matrix, double _scalar, MATH2D::GMATRIX3D& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = _matrix.data[i] * _scalar;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Determinant2D(MATH2D::GMATRIX2D _matrix, double& _outDeterminant)
			{
				_outDeterminant = _matrix.data[0] * _matrix.data[3] - _matrix.data[1] * _matrix.data[2];

				return GW::GReturn::SUCCESS;
			}

			static GReturn Determinant3D(MATH2D::GMATRIX3D _matrix, double& _outDeterminant)
			{
				double a = (_matrix.data[0] * _matrix.data[4] * _matrix.data[8]) + (_matrix.data[1] * _matrix.data[5] * _matrix.data[6]) + (_matrix.data[2] * _matrix.data[3] * _matrix.data[7]);
				double b = (_matrix.data[6] * _matrix.data[4] * _matrix.data[2]) + (_matrix.data[7] * _matrix.data[5] * _matrix.data[0]) + (_matrix.data[8] * _matrix.data[3] * _matrix.data[1]);

				_outDeterminant = a - b;

				return GReturn::SUCCESS;
			}

			static GReturn Transpose2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix)
			{
				_outMatrix.data[0] = _matrix.data[0];
				_outMatrix.data[1] = _matrix.data[2];
				_outMatrix.data[2] = _matrix.data[1];
				_outMatrix.data[3] = _matrix.data[3];

				return GReturn::SUCCESS;
			}

			static GReturn Transpose3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix)
			{
				_outMatrix.data[0] = _matrix.data[0];
				_outMatrix.data[1] = _matrix.data[3];
				_outMatrix.data[2] = _matrix.data[6];
				_outMatrix.data[3] = _matrix.data[1];
				_outMatrix.data[4] = _matrix.data[4];
				_outMatrix.data[5] = _matrix.data[7];
				_outMatrix.data[6] = _matrix.data[2];
				_outMatrix.data[7] = _matrix.data[5];
				_outMatrix.data[8] = _matrix.data[8];

				return GReturn::SUCCESS;
			}

			static GReturn Inverse2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix)
			{
				double determinant;

				Determinant2D(_matrix, determinant);

				if (G2D_COMPARISON_STANDARD_D(determinant, 0.0))
				{
					return GReturn::FAILURE;
				}

				_outMatrix.data[0] = _matrix.data[3];
				_outMatrix.data[1] = -_matrix.data[1];
				_outMatrix.data[2] = -_matrix.data[2];
				_outMatrix.data[3] = _matrix.data[0];

				MultiplyNum2D(_outMatrix, 1.0 / determinant, _outMatrix);

				return GReturn::SUCCESS;
			}

			static GReturn Inverse3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix)
			{
				double determinant;

				Determinant3D(_matrix, determinant);

				if (G2D_COMPARISON_STANDARD_D(determinant, 0.0))
				{
					return GReturn::FAILURE;
				}

				// simultaneously calculate the matrix of minors, convert it to the matrix of cofactors, and then transpose it
				_outMatrix.data[0] = (_matrix.data[4] * _matrix.data[8] - _matrix.data[5] * _matrix.data[7]); // 0
				_outMatrix.data[1] = -(_matrix.data[1] * _matrix.data[8] - _matrix.data[2] * _matrix.data[7]); // 3
				_outMatrix.data[2] = (_matrix.data[1] * _matrix.data[5] - _matrix.data[2] * _matrix.data[4]); // 6

				_outMatrix.data[3] = -(_matrix.data[3] * _matrix.data[8] - _matrix.data[5] * _matrix.data[6]); // 1
				_outMatrix.data[4] = (_matrix.data[0] * _matrix.data[8] - _matrix.data[2] * _matrix.data[6]); // 4
				_outMatrix.data[5] = -(_matrix.data[0] * _matrix.data[5] - _matrix.data[2] * _matrix.data[3]); // 7

				_outMatrix.data[6] = (_matrix.data[3] * _matrix.data[7] - _matrix.data[4] * _matrix.data[6]); // 2
				_outMatrix.data[7] = -(_matrix.data[0] * _matrix.data[7] - _matrix.data[1] * _matrix.data[6]); // 5
				_outMatrix.data[8] = (_matrix.data[0] * _matrix.data[4] - _matrix.data[1] * _matrix.data[3]); // 8

				MultiplyNum3D(_outMatrix, 1.0 / determinant, _outMatrix);

				return GReturn::SUCCESS;
			}

			static GReturn GetRotation2D(MATH2D::GMATRIX2D _matrix, double& _outRadians)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outRadians = _matrix.data[1] > 0.0 ? acos(_matrix.data[0] / r) : -acos(_matrix.data[0] / r);
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[2], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0))
				{
					double s = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3]);
					_outRadians = G2D_PI_D / 2.0 - (_matrix.data[3] > 0.0 ? acos(-_matrix.data[3] / s) : -acos(_matrix.data[3] / s));
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetRotation3D(MATH2D::GMATRIX3D _matrix, double& _outRadians)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outRadians = _matrix.data[1] > 0.0 ? acos(_matrix.data[0] / r) : -acos(_matrix.data[0] / r);
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[4], 0.0))
				{
					double s = sqrt(_matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4]);
					_outRadians = G2D_PI_D / 2.0 - (_matrix.data[4] > 0.0 ? acos(-_matrix.data[4] / s) : -acos(_matrix.data[4] / s));
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetTranslation3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outVector)
			{
				_outVector.x = _matrix.row3.x;
				_outVector.y = _matrix.row3.y;

				return GReturn::SUCCESS;
			}

			static GReturn GetScale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outScale)
			{
				double delta = _matrix.data[0] * _matrix.data[3] - _matrix.data[1] * _matrix.data[2];

				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outScale = { r, delta / r };
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[2], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0))
				{
					double s = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3]);
					_outScale = { delta / s, s };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetScale3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outScale)
			{
				double delta = _matrix.data[0] * _matrix.data[4] - _matrix.data[1] * _matrix.data[3];

				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1]);
					_outScale = { r, delta / r };
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[4], 0.0))
				{
					double s = sqrt(_matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4]);
					_outScale = { delta / s, s };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetSkew2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outSkew)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = _matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1];
					_outSkew = { atan((_matrix.data[0] * _matrix.data[1] + _matrix.data[2] * _matrix.data[3]) / r), 0.0 };
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[2], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0))
				{
					double s = _matrix.data[2] * _matrix.data[2] + _matrix.data[3] * _matrix.data[3];
					_outSkew = { 0.0, atan((_matrix.data[0] * _matrix.data[2] + _matrix.data[1] * _matrix.data[3]) / s) };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn GetSkew3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outSkew)
			{
				// Apply the QR-like decomposition.
				if (!G2D_COMPARISON_STANDARD_D(_matrix.data[0], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[1], 0.0))
				{
					double r = _matrix.data[0] * _matrix.data[0] + _matrix.data[1] * _matrix.data[1];
					_outSkew = { atan((_matrix.data[0] * _matrix.data[1] + _matrix.data[3] * _matrix.data[4]) / r), 0.0 };
				}
				else if (!G2D_COMPARISON_STANDARD_D(_matrix.data[3], 0.0) || !G2D_COMPARISON_STANDARD_D(_matrix.data[4], 0.0))
				{
					double s = _matrix.data[3] * _matrix.data[3] + _matrix.data[4] * _matrix.data[4];
					_outSkew = { 0.0, atan((_matrix.data[0] * _matrix.data[3] + _matrix.data[1] * _matrix.data[4]) / s) };
				}
				else
				{
					// x1 = x2 = y1 = y2 = 0
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn Rotate2D(MATH2D::GMATRIX2D _matrix, double _radians, MATH2D::GMATRIX2D& _outMatrix)
			{
				double c = cos(_radians);
				double s = sin(_radians);
				MATH2D::GMATRIX2D rotationMatrix = MATH2D::GIdentityMatrix2D;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[2] = -s;
				rotationMatrix.data[3] = c;

				Multiply2D(_matrix, rotationMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateGlobal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix)
			{
				double c = cos(_radians);
				double s = sin(_radians);
				MATH2D::GMATRIX3D rotationMatrix = MATH2D::GIdentityMatrix3D;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[3] = -s;
				rotationMatrix.data[4] = c;

				// store translation
				MATH2D::GVECTOR3D translation = _matrix.row3;

				Multiply3D(_matrix, rotationMatrix, _outMatrix);

				// restore translation
				_outMatrix.row3 = translation;

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateLocal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix)
			{
				double c = cos(_radians);
				double s = sin(_radians);
				MATH2D::GMATRIX3D rotationMatrix = MATH2D::GIdentityMatrix3D;
				rotationMatrix.data[0] = c;
				rotationMatrix.data[1] = s;
				rotationMatrix.data[3] = -s;
				rotationMatrix.data[4] = c;

				Multiply3D(rotationMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix)
			{
				MATH2D::GMATRIX3D translationMatrix = MATH2D::GIdentityMatrix3D;
				translationMatrix.data[6] = _vector.x;
				translationMatrix.data[7] = _vector.y;

				Multiply3D(_matrix, translationMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix)
			{
				MATH2D::GMATRIX3D translationMatrix = MATH2D::GIdentityMatrix3D;
				translationMatrix.data[6] = _vector.x;
				translationMatrix.data[7] = _vector.y;

				Multiply3D(translationMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Scale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D& _outMatrix)
			{
				MATH2D::GMATRIX2D scaleMatrix = MATH2D::GIdentityMatrix2D;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[3] = _vector.y;

				Multiply2D(_matrix, scaleMatrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix)
			{
				MATH2D::GMATRIX3D scaleMatrix = MATH2D::GIdentityMatrix3D;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[4] = _vector.y;

				// store translation
				MATH2D::GVECTOR3D translation = _matrix.row3;

				Multiply3D(_matrix, scaleMatrix, _outMatrix);

				// restore translation
				_outMatrix.row3 = translation;

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix)
			{
				MATH2D::GMATRIX3D scaleMatrix = MATH2D::GIdentityMatrix3D;
				scaleMatrix.data[0] = _vector.x;
				scaleMatrix.data[4] = _vector.y;

				Multiply3D(scaleMatrix, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Lerp2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, double _ratio, MATH2D::GMATRIX2D& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = G2D_LERP(_matrix1.data[i], _matrix2.data[i], _ratio);
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn Lerp3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, double _ratio, MATH2D::GMATRIX3D& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = G2D_LERP(_matrix1.data[i], _matrix2.data[i], _ratio);
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelative2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix)
			{
				MATH2D::GMATRIX2D inverseMatrix2;
				Inverse2D(_matrix2, inverseMatrix2);

				Multiply2D(_matrix1, inverseMatrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelative3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix)
			{
				MATH2D::GMATRIX3D inverseMatrix2;
				Inverse3D(_matrix2, inverseMatrix2);

				Multiply3D(_matrix1, inverseMatrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeSeparate2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix)
			{
				Multiply2D(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeSeparate3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix)
			{
				Multiply3D(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Downgrade2(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2F& _outMatrix)
			{
				for (int i = 0; i < 4; i++)
				{
					_outMatrix.data[i] = static_cast<float>(_matrix.data[i]);
				}

				return GReturn::SUCCESS;
			}

			static GReturn Downgrade3(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3F& _outMatrix)
			{
				for (int i = 0; i < 9; i++)
				{
					_outMatrix.data[i] = static_cast<float>(_matrix.data[i]);
				}

				return GReturn::SUCCESS;
			}
		};
	}
}
