namespace GW
{
	namespace I
	{
		class GMatrix2DImplementation : public virtual GMatrix2DInterface, private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}
			// Floats
			static GReturn Add2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Add3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Subtract2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Subtract3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Multiply2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Multiply3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MatrixXVector2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn MatrixXVector3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::FAILURE; }
			static GReturn MultiplyNum2F(MATH2D::GMATRIX2F _matrix, float _scalar, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyNum3F(MATH2D::GMATRIX3F _matrix, float _scalar, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Determinant2F(MATH2D::GMATRIX2F _matrix, float& _outDeterminant) { return GReturn::FAILURE; }
			static GReturn Determinant3F(MATH2D::GMATRIX3F _matrix, float& _outDeterminant) { return GReturn::FAILURE; }
			static GReturn Transpose2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Transpose3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Inverse2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Inverse3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn GetRotation2F(MATH2D::GMATRIX2F _matrix, float& _outRadians) { return GReturn::FAILURE; }
			static GReturn GetRotation3F(MATH2D::GMATRIX3F _matrix, float& _outRadians) { return GReturn::FAILURE; }
			static GReturn GetTranslation3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::FAILURE; }
			static GReturn GetScale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::FAILURE; }
			static GReturn GetScale3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::FAILURE; }
			static GReturn GetSkew2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::FAILURE; }
			static GReturn GetSkew3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::FAILURE; }
			static GReturn Rotate2F(MATH2D::GMATRIX2F _matrix, float _radians, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateGlobal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateLocal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Scale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Lerp2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, float _ratio, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Lerp3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, float _ratio, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeRelative2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeRelative3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparate2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparate3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Upgrade2(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Upgrade3(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			// Doubles
			static GReturn Add2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Add3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Subtract2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Subtract3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Multiply2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Multiply3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MatrixXVector2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn MatrixXVector3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::FAILURE; }
			static GReturn MultiplyNum2D(MATH2D::GMATRIX2D _matrix, double _scalar, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyNum3D(MATH2D::GMATRIX3D _matrix, double _scalar, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Determinant2D(MATH2D::GMATRIX2D _matrix, double& _outDeterminant) { return GReturn::FAILURE; }
			static GReturn Determinant3D(MATH2D::GMATRIX3D _matrix, double& _outDeterminant) { return GReturn::FAILURE; }
			static GReturn Transpose2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Transpose3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Inverse2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Inverse3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn GetRotation2D(MATH2D::GMATRIX2D _matrix, double& _outRadians) { return GReturn::FAILURE; }
			static GReturn GetRotation3D(MATH2D::GMATRIX3D _matrix, double& _outRadians) { return GReturn::FAILURE; }
			static GReturn GetTranslation3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::FAILURE; }
			static GReturn GetScale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::FAILURE; }
			static GReturn GetScale3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::FAILURE; }
			static GReturn GetSkew2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::FAILURE; }
			static GReturn GetSkew3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::FAILURE; }
			static GReturn Rotate2D(MATH2D::GMATRIX2D _matrix, double _radians, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateGlobal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateLocal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Scale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Lerp2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, double _ratio, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Lerp3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, double _ratio, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeRelative2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeRelative3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparate2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparate3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Downgrade2(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::FAILURE; }
			static GReturn Downgrade3(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::FAILURE; }
		};
	}
}