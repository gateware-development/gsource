#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <unistd.h>
#include "OpenGL/glxext.hpp"
#include <cstring>
#include <string>

class glXQueryVersion;
namespace GW
{
	namespace I
	{
		class GOpenGLSurfaceImplementation : public virtual GOpenGLSurfaceInterface,
			private virtual GEventResponderImplementation
		{
		private:
			GW::SYSTEM::GWindow gwindow;
			GW::CORE::GEventResponder responder;
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH;
			unsigned int clientX;
			unsigned int clientY;
			unsigned int width = 0;
			unsigned int height = 0;
			float aspectRatio = 0;

			GLint extensionCount = 0;
			const char* glExtensions;

#define GLX_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB 0x2092

			// GLX FUNCTION POINTERS //
			PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB;
			PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT;

			GLint attributes[5] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
			GLXContext OGLXContext;
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask)
			{

				if (!_gwindow)
					return GReturn::INVALID_ARGUMENT;

				gwindow = _gwindow;
				//Check if valid _initMask was passed in
				unsigned long long allowed = ~(GW::GRAPHICS::COLOR_10_BIT | GW::GRAPHICS::DEPTH_BUFFER_SUPPORT | GW::GRAPHICS::DEPTH_STENCIL_SUPPORT | GW::GRAPHICS::OPENGL_ES_SUPPORT);
				if (allowed & _initMask)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				gwindow.GetWindowHandle(UWH);
				gwindow.GetClientTopLeft(clientX, clientY);
				gwindow.GetClientWidth(width);
				gwindow.GetClientHeight(height);
				aspectRatio = static_cast<float>(width) / static_cast<float>(height);

				static int frameBufferAttributes[] =
				{
					GLX_RENDER_TYPE, GLX_RGBA_BIT,
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
					GLX_DOUBLEBUFFER, true,
					GLX_RED_SIZE, 8,
					GLX_GREEN_SIZE, 8,
					GLX_BLUE_SIZE, 8,
					GLX_ALPHA_SIZE, 8,
					GLX_DEPTH_SIZE, 0,
					GLX_STENCIL_SIZE, 0,
					None
				};

				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
				{
					frameBufferAttributes[7] = 10;
					frameBufferAttributes[9] = 10;
					frameBufferAttributes[11] = 10;
					frameBufferAttributes[13] = 2;
				}

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
					frameBufferAttributes[15] = 24;

				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
				{
					frameBufferAttributes[15] = 24;
					frameBufferAttributes[17] = 8;
				}

				// Select the default Frame buffer configuration //
				XLockDisplay(static_cast<Display*>(UWH.display));
				// use null attribs to scan for available formats and check against request
				int queryCount = 0;
				bool formatMatch = false;
				GLXFBConfig* queryConfig;
				if (queryConfig = glXGetFBConfigs(static_cast<Display*>(UWH.display),
					DefaultScreen(static_cast<Display*>(UWH.display)), &queryCount))
				{
					int numAttrib2Search = sizeof(frameBufferAttributes) / sizeof(int) / 2;
					// scan for matching frame attributes, fail if not found
					for (int i = 0; i < queryCount; ++i)
					{
						// search attrib pairs to see if all items match
						int attribMatch = 0;
						for (int j = 0; j < numAttrib2Search; ++j)
						{
							int attribVal; int errCode;
							// if the attribute we are looking for is DC we don't care about it, mark it anyway
							if (frameBufferAttributes[j * 2 + 1] != GLX_DONT_CARE)
							{
								errCode = glXGetFBConfigAttrib(static_cast<Display*>(UWH.display), queryConfig[i],
									frameBufferAttributes[j * 2], &attribVal);
								if (errCode != GLX_NO_EXTENSION && errCode != GLX_BAD_ATTRIBUTE)
								{
									if (frameBufferAttributes[j * 2] == GLX_DRAWABLE_TYPE ||
										frameBufferAttributes[j * 2] == GLX_RENDER_TYPE)
									{
										if ((frameBufferAttributes[j * 2 + 1] & attribVal) ==
											frameBufferAttributes[j * 2 + 1])
											++attribMatch;
									}
									else if (frameBufferAttributes[j * 2 + 1] == attribVal)
										++attribMatch;
								}
							}
							else // this is not something we intend to use so whatever it is, is fine
								++attribMatch;
						}
						//std::cout << "OGL FBCONFIG: " << attribMatch << " ATTRIBS MATCHED" << std::endl;
						if (attribMatch == numAttrib2Search)
						{
							formatMatch = true;
							queryCount = i; // save the first match
							break;
						}
					}
				}
				// fail if no matching format found
				if (!queryConfig || formatMatch == false)
					return GReturn::HARDWARE_UNAVAILABLE;
				/*
				int ver[2];// does not find ogl version
				if (glXQueryVersion(static_cast<Display*>(UWH.display),&ver[0],&ver[1]) == false) // is OpenGL available?
					return GReturn::HARDWARE_UNAVAILABLE;
				// I need to detect OpenGL 3.3 if we request HDR 1010102

				int frameBufferCount;
				GLXFBConfig* frameBufferConfig = glXChooseFBConfig(static_cast<Display*>(UWH.display), DefaultScreen(static_cast<Display*>(UWH.display)), frameBufferAttributes, &frameBufferCount);

				if (!frameBufferConfig)
					return GReturn::HARDWARE_UNAVAILABLE;
				*/
				XVisualInfo* visualInfo = glXGetVisualFromFBConfig(
					static_cast<Display*>(UWH.display), queryConfig[queryCount]);
				//Colormap colorMap = XCreateColormap(static_cast<Display*>(UWH.display), 
				//	RootWindow(static_cast<Display*>(UWH.display), visualInfo->screen), visualInfo->visual, AllocNone);

				XWindowAttributes curWinAttrib; // Current window attributes
				XGetWindowAttributes(static_cast<Display*>(UWH.display), *(static_cast<Window*>(UWH.window)), &curWinAttrib);

				XSetWindowAttributes swa;
				swa.event_mask = SubstructureNotifyMask | PropertyChangeMask | ExposureMask | curWinAttrib.all_event_masks;

				unsigned long valueMask = CWEventMask;

				XChangeWindowAttributes(static_cast<Display*>(UWH.display), *(static_cast<Window*>(UWH.window)), valueMask, &swa);
				GLXContext oldContext = glXCreateContext(static_cast<Display*>(UWH.display), visualInfo, 0, GL_TRUE);

				XFree(visualInfo);

				// Load GLX Extensions //
				glExtensions = glXQueryExtensionsString(static_cast<Display*>(UWH.display), DefaultScreen(static_cast<Display*>(UWH.display)));

				glXCreateContextAttribsARB = (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");
				glXSwapIntervalEXT = (PFNGLXSWAPINTERVALEXTPROC)glXGetProcAddressARB((const GLubyte*)"glXSwapIntervalEXT");

				if (!glXMakeCurrent(static_cast<Display*>(UWH.display), *(static_cast<Window*>(UWH.window)), oldContext))
				{
					XUnlockDisplay(static_cast<Display*>(UWH.display));
					XFree(queryConfig);
					return GReturn::FAILURE;
				}

				// print openGL version
				std::string ver = reinterpret_cast<const char*>(glGetString(GL_VERSION));
				//std::cout << ver << std::endl;

				glXDestroyContext(static_cast<Display*>(UWH.display), oldContext);

				// Extract the version number only continue if 3.2 or better
				float glVer = std::stof(ver) + 0.000001f; // remove round down
				if (glVer < 3.2f)
				{
					XFree(queryConfig);
					return GReturn::HARDWARE_UNAVAILABLE;
				}

				// Create new context //
				int contextAttribs[] =
				{
					GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
					GLX_CONTEXT_MINOR_VERSION_ARB, 2,
					GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
					None
				};

				if (_initMask & GW::GRAPHICS::OPENGL_ES_SUPPORT)
					contextAttribs[5] = GLX_CONTEXT_ES2_PROFILE_BIT_EXT;

				OGLXContext = glXCreateContextAttribsARB(static_cast<Display*>(UWH.display),
					queryConfig[queryCount], NULL, true, contextAttribs);

				XFree(queryConfig);

				if (!glXMakeCurrent(static_cast<Display*>(UWH.display),
					*static_cast<Window*>(UWH.window), OGLXContext))
				{
					XUnlockDisplay(static_cast<Display*>(UWH.display));
					return GReturn::FAILURE;
				}

				XUnlockDisplay(static_cast<Display*>(UWH.display));

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
					glEnable(GL_DEPTH_TEST);

				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
					glEnable(GL_STENCIL_TEST);

				// CHECK IF INIT FLAGS WERE MET //
				// 10 BIT COLOR //
				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
				{
					GLint red, green, blue;
					glGetIntegerv(GL_RED_BITS, &red);
					glGetIntegerv(GL_GREEN_BITS, &green);
					glGetIntegerv(GL_BLUE_BITS, &blue);
					if (red != 0 && green != 10 && blue != 10)
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DEPTH BUFFER SUPPORT //
				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
				{
					GLint depth;
					glGetIntegerv(GL_DEPTH_BITS, &depth);
					if (depth == 0 || !glIsEnabled(GL_DEPTH_TEST))
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DEPTH STENCIL SUPPORT //
				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
				{
					GLint stencil;
					glGetIntegerv(GL_STENCIL_BITS, &stencil);
					if (stencil == 0 || !glIsEnabled(GL_STENCIL_TEST))
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// ES CONTEXT SUPPORT //
				if (_initMask & GW::GRAPHICS::OPENGL_ES_SUPPORT)
				{
					char* version = (char*)glGetString(GL_VERSION);

					if (strstr(version, "OpenGL ES") == NULL)
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DIRECT2D SUPPORT //
				if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				//XFree(frameBufferConfig);

				// Call back event handler for OpenGL
				GReturn result = responder.Create([&](const GEvent& g)
					{
						GW::SYSTEM::GWindow::Events e;
						GW::SYSTEM::GWindow::EVENT_DATA d;
						if (+g.Read(e, d))
						{
							switch (e)
							{
							case GW::SYSTEM::GWindow::Events::MINIMIZE: {} break;
							case GW::SYSTEM::GWindow::Events::DESTROY:
							{
								gwindow.GetWindowHandle(UWH);
								glXMakeCurrent(static_cast<Display*>(UWH.display), None, NULL);
								glXDestroyContext(static_cast<Display*>(UWH.display), OGLXContext);
								OGLXContext = nullptr;
							}
							break;

							case GW::SYSTEM::GWindow::Events::MAXIMIZE:
							case GW::SYSTEM::GWindow::Events::RESIZE:
							case GW::SYSTEM::GWindow::Events::MOVE:
							{
								gwindow.GetClientWidth(width);
								gwindow.GetClientHeight(height);
								aspectRatio = static_cast<float>(width) / static_cast<float>(height);
								glViewport(0, 0, width, height);
							}
							break;
							}
						}
					});
				if (G_PASS(result)) {
					gwindow.Register(responder);
				}

				return result;
			}

			GReturn GetAspectRatio(float& _outRatio) const override
			{
				if (!gwindow)
					return GReturn::FAILURE;

				_outRatio = aspectRatio;
				return GReturn::SUCCESS;
			}

			GReturn GetContext(void** _outContext) const override
			{
				if (!OGLXContext)
					return GReturn::FAILURE;

				*_outContext = OGLXContext;
				return GReturn::SUCCESS;
			}

			GReturn UniversalSwapBuffers() override
			{
				if (!gwindow)
					return GReturn::FAILURE;

				if (-gwindow.GetWindowHandle(UWH))
					return GReturn::FAILURE;

				if (!static_cast<Display*>(UWH.display) || *static_cast<Window*>(UWH.window) == 0)
					return GReturn::FAILURE;

				glXSwapBuffers(static_cast<Display*>(UWH.display), *static_cast<Window*>(UWH.window));
				return GReturn::SUCCESS;
			}

			GReturn QueryExtensionFunction(const char* _extension, const char* _funcName, void** _outFuncAddress) override
			{
				if ((_funcName == nullptr && _outFuncAddress != nullptr) ||
					(_funcName != nullptr && _outFuncAddress == nullptr) ||
					_extension == nullptr && _funcName == nullptr)
					return GReturn::INVALID_ARGUMENT;

				// User only passed in function name, without extension //
				if (_extension == nullptr && _funcName != nullptr && _outFuncAddress != nullptr)
				{
					*_outFuncAddress = (void**)glXGetProcAddress((const GLubyte*)_funcName);
					if (*_outFuncAddress == nullptr)
						return GReturn::FAILURE;
					return GReturn::SUCCESS;
				}

				// User only passed in extension name, without function //
				if (_funcName == nullptr && _outFuncAddress == nullptr)
				{
					if (strstr(glExtensions, _extension) != NULL)
						return GReturn::FAILURE;
					return GReturn::SUCCESS;
				}

				// User passed in extension name and function name //
				if (strstr(glExtensions, _extension) != NULL)
				{
					if (_funcName != NULL)
						*_outFuncAddress = (void**)glXGetProcAddress((const GLubyte*)_funcName);
					else
						*_outFuncAddress = (void**)glXGetProcAddress((const GLubyte*)_extension);
					return GReturn::SUCCESS;
				}
				return GReturn::FAILURE;
			}

			GReturn EnableSwapControl(bool _setSwapControl)
			{
				if (!glXSwapIntervalEXT)
					return GW::GReturn::FEATURE_UNSUPPORTED;

				if (!OGLXContext)
					return GW::GReturn::FAILURE;

				if (!gwindow)
					return GW::GReturn::FAILURE;

				gwindow.GetWindowHandle(UWH);
				XLockDisplay(static_cast<Display*>(UWH.display));
				if (_setSwapControl)
					glXSwapIntervalEXT(static_cast<Display*>(UWH.display), *static_cast<Window*>(UWH.window), 1);
				else
					glXSwapIntervalEXT(static_cast<Display*>(UWH.display), *static_cast<Window*>(UWH.window), 0);
				XUnlockDisplay(static_cast<Display*>(UWH.display));
				return GReturn::SUCCESS;
			}

			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return responder.Assign(_newHandler);
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return responder.Assign(_newEventHandler);
			}
			GReturn Invoke() const override {
				return responder.Invoke();
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return responder.Invoke(_incomingEvent);
			}
		};
	}
}
