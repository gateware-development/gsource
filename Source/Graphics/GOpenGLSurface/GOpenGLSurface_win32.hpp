#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <gl/GL.h>
#include <string>

#ifndef GATEWARE_FORCE_OPENGLES
	#include "OpenGL/glcorearb.hpp"
#else
	#include "OpenGL/gl3.hpp"
#endif

#include "OpenGL/wgl.hpp"
#pragma comment(lib, "OpenGL32.lib")

namespace GW
{
	namespace I
	{
		class GOpenGLSurfaceImplementation : public virtual GOpenGLSurfaceInterface,
			private virtual GEventResponderImplementation, private virtual GThreadSharedImplementation
		{
		private:
			GW::SYSTEM::GWindow gwindow;
			GW::CORE::GEventResponder responder;
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH = {nullptr, nullptr};
			unsigned int width = 0;
			unsigned int height = 0;
			float aspectRatio = 0;

			GLint extensionCount = 0;
			const char* glExtensions = nullptr;

			HDC                         hdc			= NULL;
			HGLRC                       OGLcontext	= NULL;
			//				 WGL FUNCTION POINTERS				   //
			PFNWGLGETEXTENSIONSSTRINGARBPROC	wglGetExtensionsStringARB = NULL;
			PFNWGLGETEXTENSIONSSTRINGEXTPROC	wglGetExtensionsStringEXT = NULL;
			PFNWGLCREATECONTEXTATTRIBSARBPROC	wglCreateContextAttribsARB = NULL;
			PFNWGLSWAPINTERVALEXTPROC			wglSwapIntervalEXT = NULL;
			PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB = NULL;

		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask)
			{
				if (!_gwindow)
					return GReturn::INVALID_ARGUMENT;

				gwindow = _gwindow;
				//Check if valid _initMask was passed in
				unsigned long long allowed = ~(GW::GRAPHICS::COLOR_10_BIT | GW::GRAPHICS::DEPTH_BUFFER_SUPPORT | GW::GRAPHICS::DEPTH_STENCIL_SUPPORT | GW::GRAPHICS::OPENGL_ES_SUPPORT);
				if (allowed & _initMask)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				gwindow.GetWindowHandle(UWH);
				gwindow.GetClientWidth(width);
				gwindow.GetClientHeight(height);
				aspectRatio = static_cast<float>(width) / static_cast<float>(height);
				hdc = GetDC(static_cast<HWND>(UWH.window));

				PIXELFORMATDESCRIPTOR pfd =
				{
					sizeof(PIXELFORMATDESCRIPTOR),
					1,
					PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
					PFD_TYPE_RGBA,
					32,
					8, 0,
					8, 0,
					8, 0,
					8, 0,
					0,
					0, 0, 0, 0,
					32,
					0,
					0,
					PFD_MAIN_PLANE,
					0,
					0, 0, 0
				};

				int pixelFormat = ChoosePixelFormat(hdc, &pfd);
				bool ret = SetPixelFormat(hdc, pixelFormat, &pfd);

				OGLcontext = wglCreateContext(hdc);
				if (OGLcontext == NULL) // is OpenGL functional?
					return GReturn::HARDWARE_UNAVAILABLE;

				wglMakeCurrent(hdc, OGLcontext);

				wglGetExtensionsStringARB = (PFNWGLGETEXTENSIONSSTRINGARBPROC)wglGetProcAddress("wglGetExtensionsStringARB");
				wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)wglGetProcAddress("wglGetExtensionsStringEXT");
				wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
				wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
				wglGetPixelFormatAttribivARB = (PFNWGLGETPIXELFORMATATTRIBIVARBPROC)wglGetProcAddress("wglGetPixelFormatAttribivARB");

				int pixelAttributes[] =
				{
					WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
					WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
					WGL_COLOR_BITS_ARB, 32,
					WGL_RED_BITS_ARB, 8,
					WGL_GREEN_BITS_ARB, 8,
					WGL_BLUE_BITS_ARB, 8,
					WGL_ALPHA_BITS_ARB, 8,
					WGL_DEPTH_BITS_ARB, 0,
					WGL_STENCIL_BITS_ARB, 0,
					WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
					WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
					0, 0
				};

				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
				{
					pixelAttributes[7] = 10;
					pixelAttributes[9] = 10;
					pixelAttributes[11] = 10;
					pixelAttributes[13] = 2;
				}

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
					pixelAttributes[15] = 32;

				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
				{
					pixelAttributes[15] = 24;
					pixelAttributes[17] = 8;
				}

				UINT pixelCount = 0;

				PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = nullptr;
				QueryExtensionFunction("WGL_ARB_extensions_string", "wglChoosePixelFormatARB", (void**)&wglChoosePixelFormatARB);

				ret = wglChoosePixelFormatARB(hdc, pixelAttributes, NULL, 1, &pixelFormat, &pixelCount);
				ret = SetPixelFormat(hdc, pixelFormat, &pfd);

				// get openGL version
				std::string ver = reinterpret_cast<const char*>(glGetString(GL_VERSION));

				wglMakeCurrent(NULL, NULL);
				ReleaseDC(static_cast<HWND>(UWH.window), hdc);
				wglDeleteContext(OGLcontext);
				// Extract the version number only continue if 3.2 or better
				float glVer = std::stof(ver) + 0.000001f; // remove round down
				if (glVer < 3.2f)
				{
					return GReturn::HARDWARE_UNAVAILABLE;
				}

				// Create an OpenGL 3.2 Context //
				int contextAttributes[] =
				{
					WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
					WGL_CONTEXT_MINOR_VERSION_ARB, 2,
					WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
					0
				};

				if (_initMask & GW::GRAPHICS::OPENGL_ES_SUPPORT)
				{
					// Create an OpenGL ES 3.2 Context //
					contextAttributes[5] = WGL_CONTEXT_ES2_PROFILE_BIT_EXT;
				}

				OGLcontext = wglCreateContextAttribsARB(hdc, 0, contextAttributes);
				wglMakeCurrent(hdc, OGLcontext);

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
					glEnable(GL_DEPTH_TEST);

				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
					glEnable(GL_STENCIL_TEST);

				int pfValues[6] = { 0 };
				int pfQuery[] =
				{
					WGL_RED_BITS_ARB,
					WGL_GREEN_BITS_ARB,
					WGL_BLUE_BITS_ARB,
					WGL_ALPHA_BITS_ARB,
					WGL_DEPTH_BITS_ARB,
					WGL_STENCIL_BITS_ARB
				};

				ret = wglGetPixelFormatAttribivARB(hdc, pixelFormat, PFD_MAIN_PLANE, 6, pfQuery, pfValues);

				// CHECK IF INIT FLAGS WERE MET //
				// 10 BIT COLOR //
				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
				{
					if (pfValues[0] != 10 && pfValues[1] != 10 && pfValues[2] != 10)
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DEPTH BUFFER SUPPORT //
				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
				{
					if (pfValues[4] == 0 || !glIsEnabled(GL_DEPTH_TEST))
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DEPTH STENCIL SUPPORT //
				if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
				{
					if (pfValues[5] == 0 || !glIsEnabled(GL_STENCIL_TEST))
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// ES CONTEXT SUPPORT //
				if (_initMask & GW::GRAPHICS::OPENGL_ES_SUPPORT)
				{
					char* version = (char*)glGetString(GL_VERSION);

					if (strstr(version, "OpenGL ES") == NULL)
						return GReturn::FEATURE_UNSUPPORTED;
				}

				// DIRECT2D SUPPORT //
				if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				// Call back event handler for OpenGL
				GReturn result = responder.Create([&](const GEvent& g)
				{
					GW::SYSTEM::GWindow::Events e;
					GW::SYSTEM::GWindow::EVENT_DATA d;
					if (+g.Read(e, d)) 	
					{
						switch (e)
						{
							case GW::SYSTEM::GWindow::Events::MINIMIZE:
							case GW::SYSTEM::GWindow::Events::DESTROY:
								break;

							case GW::SYSTEM::GWindow::Events::MAXIMIZE:
							case GW::SYSTEM::GWindow::Events::RESIZE:
							case GW::SYSTEM::GWindow::Events::MOVE:
							{
								gwindow.GetClientWidth(width);
								gwindow.GetClientHeight(height);
								aspectRatio = static_cast<float>(width) / static_cast<float>(height);
								glViewport(0, 0, width, height);
							}
							break;
						}
					}
				});
				if (G_PASS(result)) 
				{
					gwindow.Register(responder);
				}

				return result;
			}

			GReturn GetAspectRatio(float& _outRatio) const override
			{
				if (!gwindow)
					return GReturn::FAILURE;

				_outRatio = aspectRatio;
				return GReturn::SUCCESS;
			}

			GReturn GetContext(void** _outContext) const override
			{
				if (!OGLcontext)
					return GReturn::FAILURE;

				*_outContext = OGLcontext;
				return GReturn::SUCCESS;
			}

			GReturn UniversalSwapBuffers() override
			{
				if (!hdc)
					return GReturn::FAILURE;

				SwapBuffers(hdc);
				return GReturn::SUCCESS;
			}

			GReturn QueryExtensionFunction(const char* _extension, const char* _funcName, void** _outFuncAddress) override
			{
				if ((_funcName == nullptr && _outFuncAddress != nullptr) ||
					(_funcName != nullptr && _outFuncAddress == nullptr) ||
					_extension == nullptr && _funcName == nullptr)
					return GReturn::INVALID_ARGUMENT;

				// User only passed in function name, without extension //
				if (_extension == nullptr && _funcName != nullptr && _outFuncAddress != nullptr)
				{
					*_outFuncAddress = wglGetProcAddress(_funcName);
					if (*_outFuncAddress == nullptr)
						return GReturn::FAILURE;
					return GReturn::SUCCESS;
				}

				// User only passed in extension name, without function //
				if (_funcName == nullptr && _outFuncAddress == nullptr)
				{
					if (wglGetExtensionsStringEXT)
					{
						glExtensions = wglGetExtensionsStringEXT();
						if (strstr(glExtensions, _extension) != NULL)
							return GReturn::SUCCESS;
					}
					if (wglGetExtensionsStringARB)
					{
						glExtensions = wglGetExtensionsStringARB(hdc);
						if (strstr(glExtensions, _extension) != NULL)
							return GReturn::SUCCESS;
					}
					return GReturn::FAILURE;
				}

				// User passed in extension name and function name //
				if (wglGetExtensionsStringEXT)
				{
					glExtensions = wglGetExtensionsStringEXT();
					if (strstr(glExtensions, _extension) != NULL)
					{
						if (_funcName != NULL)
							*_outFuncAddress = wglGetProcAddress(_funcName);
						else
							*_outFuncAddress = wglGetProcAddress(_extension);
						return GReturn::SUCCESS;
					}
				}
				if (wglGetExtensionsStringARB)
				{
					glExtensions = wglGetExtensionsStringARB(hdc);
					if (strstr(glExtensions, _extension) != NULL)
					{
						if (_funcName != NULL)
							_outFuncAddress = (void**)wglGetProcAddress(_funcName);
						else
							_outFuncAddress = (void**)wglGetProcAddress(_extension);
						return GReturn::SUCCESS;
					}
				}
				return GReturn::FAILURE;
			}

			GReturn EnableSwapControl(bool _setSwapControl)
			{
				if (!wglSwapIntervalEXT)
					return GReturn::FEATURE_UNSUPPORTED;
				if (!OGLcontext)
					return GReturn::FAILURE;

				if (_setSwapControl == true)
					wglSwapIntervalEXT(1);
				else
					wglSwapIntervalEXT(0);

				return GReturn::SUCCESS;
			}

			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return responder.Assign(_newHandler);
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return responder.Assign(_newEventHandler);
			}
			GReturn Invoke() const override {
				return responder.Invoke();
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return responder.Invoke(_incomingEvent);
			}
		};
	}
}