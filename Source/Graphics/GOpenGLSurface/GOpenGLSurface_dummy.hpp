namespace GW {
	namespace I 	{
		class GOpenGLSurfaceImplementation : public virtual GOpenGLSurfaceInterface 		{
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask) {
				return GReturn::FEATURE_UNSUPPORTED;
			}
			GReturn GetAspectRatio(float& _outRatio) const override {
				return GReturn::FAILURE;
			}
			GReturn GetContext(void** _outContext) const override {
				return GReturn::FAILURE;
			}
			GReturn UniversalSwapBuffers() override {
				return GReturn::FAILURE;
			}
			GReturn QueryExtensionFunction(const char* _extension, const char* _funcName,
										   void** _outFuncAddress) override {
				return GReturn::FAILURE;
			}
			GReturn EnableSwapControl(bool _setSwapControl) override {
				return GReturn::FAILURE;
			}

			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return GReturn::FAILURE;
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return GReturn::FAILURE;
			}
		};
	}
}
