
#include <wrl/client.h>
#include <d3d11_3.h>
#include <d2d1_3.h>
#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.Graphics.Display.h>
#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.Foundation.h>
#include "../../Shared/GEnvironment.hpp"


#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d2d1.lib")

namespace GW
{
	namespace I
	{
		class GDirectX11SurfaceImplementation : public virtual GDirectX11SurfaceInterface,
			private virtual GEventResponderImplementation, private virtual GThreadSharedImplementation
		{
		private:
			GW::SYSTEM::GWindow gwindow;
			GW::CORE::GEventResponder responder;
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH;
			winrt::com_ptr<ID3D11Device3> pDevice = nullptr;
			winrt::com_ptr<ID3D11DeviceContext3> pImmediateContext = nullptr;
			winrt::com_ptr<IDXGISwapChain1> pSwapChain = nullptr;
			Microsoft::WRL::ComPtr<ID3D11RenderTargetView> pBackBufferView = nullptr;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDepthStencilView = nullptr;
			Microsoft::WRL::ComPtr<ID3D11RasterizerState> pRasterizerState = nullptr;
			std::atomic_bool m_Deallocated = false;
			unsigned int width = 0;
			unsigned int height = 0;
			float aspectRatio = 0.0f;
			// Cached reference to the Window
			winrt::Windows::UI::Core::CoreWindow* m_window = nullptr;
			winrt::event_token trim;
			//winrt::Windows::Graphics::Display::DisplayInformation currentDisplayInfo{ nullptr };
			//winrt::Windows::Foundation::Size m_logicalSize;
			//winrt::Windows::Graphics::Display::DisplayOrientations m_nativeOrientation;
			//winrt::Windows::Graphics::Display::DisplayOrientations m_currentOrientation;
			float m_dpi;
			//std::mutex contextMutex;
			std::atomic_int resizeCounter = 0;

			// for orientation changing

			// 0-degree Z-rotation
			//const DirectX::XMFLOAT4X4 Rotation0{
			//	1.0f, 0.0f, 0.0f, 0.0f,
			//	0.0f, 1.0f, 0.0f, 0.0f,
			//	0.0f, 0.0f, 1.0f, 0.0f,
			//	0.0f, 0.0f, 0.0f, 1.0f
			//};

			//// 90-degree Z-rotation
			//const DirectX::XMFLOAT4X4 Rotation90{
			//	0.0f, 1.0f, 0.0f, 0.0f,
			//	-1.0f, 0.0f, 0.0f, 0.0f,
			//	0.0f, 0.0f, 1.0f, 0.0f,
			//	0.0f, 0.0f, 0.0f, 1.0f
			//};

			//// 180-degree Z-rotation
			//const DirectX::XMFLOAT4X4 Rotation180{
			//	-1.0f, 0.0f, 0.0f, 0.0f,
			//	0.0f, -1.0f, 0.0f, 0.0f,
			//	0.0f, 0.0f, 1.0f, 0.0f,
			//	0.0f, 0.0f, 0.0f, 1.0f
			//};

			//// 270-degree Z-rotation
			//const DirectX::XMFLOAT4X4 Rotation270{
			//	0.0f, -1.0f, 0.0f, 0.0f,
			//	1.0f, 0.0f, 0.0f, 0.0f,
			//	0.0f, 0.0f, 1.0f, 0.0f,
			//	0.0f, 0.0f, 0.0f, 1.0f
			//};

			/*winrt::Windows::Foundation::IAsyncAction CallOnUiThreadAsync(winrt::Windows::UI::Core::CoreDispatcher dispatcher, winrt::Windows::UI::Core::DispatchedHandler handler) const
			{
				co_await dispatcher.RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal, handler);
			};*/

			// This should only be used if there is only one view
			winrt::Windows::Foundation::IAsyncAction CallOnMainViewUiThreadAsync(winrt::Windows::UI::Core::DispatchedHandler handler) const
			{
				//CallOnUiThreadAsync(winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher(), handler);
				co_await winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher().RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal, handler);
			}

			void SetWindowDX11()
			{
				auto process = CallOnMainViewUiThreadAsync([&]()
					{
						//currentDisplayInfo = winrt::Windows::Graphics::Display::DisplayInformation::GetForCurrentView();
						m_window = reinterpret_cast<winrt::Windows::UI::Core::CoreWindow*>(UWH.window);
						//m_logicalSize = winrt::Windows::Foundation::Size(m_window->Bounds().Width, m_window->Bounds().Height);
						//m_nativeOrientation = currentDisplayInfo.NativeOrientation();
						//m_currentOrientation = currentDisplayInfo.CurrentOrientation();
						//m_dpi = currentDisplayInfo.LogicalDpi();
					});

				process.get();
			}

		public:
			~GDirectX11SurfaceImplementation()
			{
				winrt::Windows::ApplicationModel::Core::CoreApplication::Suspending(trim);
			}
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask)
			{
				if (!_gwindow)
					return GReturn::INVALID_ARGUMENT;

				
				trim = winrt::Windows::ApplicationModel::Core::CoreApplication::Suspending([&]
				(winrt::Windows::Foundation::IInspectable const&,
					winrt::Windows::ApplicationModel::SuspendingEventArgs const& args)
					{
						winrt::Windows::ApplicationModel::SuspendingDeferral deferral = args.SuspendingOperation().GetDeferral();
						winrt::com_ptr<IDXGIDevice3> dxgiDevice;
						dxgiDevice = pDevice.as<IDXGIDevice3>();

						dxgiDevice->Trim();
						deferral.Complete();
					});
					

				gwindow = _gwindow;
				//Check if valid _initMask was passed in
				unsigned long long allowed = ~(
					GW::GRAPHICS::COLOR_10_BIT |
					GW::GRAPHICS::DEPTH_BUFFER_SUPPORT |
					GW::GRAPHICS::DEPTH_STENCIL_SUPPORT |
					GW::GRAPHICS::DIRECT2D_SUPPORT |
					GW::GRAPHICS::MSAA_2X_SUPPORT |
					GW::GRAPHICS::MSAA_4X_SUPPORT |
					GW::GRAPHICS::MSAA_8X_SUPPORT |
					GW::GRAPHICS::MSAA_16X_SUPPORT
					);
				if (allowed & _initMask)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				// If COLOR_10_BIT and any MSAA flag is set, return FEATURE_UNSUPPORTED
				if ((_initMask & GW::GRAPHICS::COLOR_10_BIT) &&
					(_initMask & GW::GRAPHICS::MSAA_2X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_4X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_8X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_16X_SUPPORT))
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				short msaaSample = 1;
				if (_initMask & GW::GRAPHICS::MSAA_2X_SUPPORT)
					msaaSample = 2;
				else if (_initMask & GW::GRAPHICS::MSAA_4X_SUPPORT)
					msaaSample = 4;
				else if (_initMask & GW::GRAPHICS::MSAA_8X_SUPPORT)
					msaaSample = 8;
				else if (_initMask & GW::GRAPHICS::MSAA_16X_SUPPORT)
					msaaSample = 16;

				gwindow.GetWindowHandle(UWH);
				gwindow.GetClientWidth(width);
				gwindow.GetClientHeight(height);
				aspectRatio = static_cast<float>(width) / static_cast<float>(height);

				SetWindowDX11();

				D3D_FEATURE_LEVEL featureLevels[] =
				{
					D3D_FEATURE_LEVEL_11_1,
					D3D_FEATURE_LEVEL_11_0,
					D3D_FEATURE_LEVEL_10_1,
					D3D_FEATURE_LEVEL_10_0,
					D3D_FEATURE_LEVEL_9_3,
					D3D_FEATURE_LEVEL_9_2,
					D3D_FEATURE_LEVEL_9_1
				};

				// if xbox
				GAMING_DEVICE_MODEL_INFORMATION info = {};
				GetGamingDeviceModelInformation(&info);
				if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
				{
					switch (info.deviceId)
					{
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:

					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:
						// keep swapchain 1080p
						width = 1920;
						height = 1080;
						break;
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:

					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
					default:
						// forward compat
						// 4K
						width = 3840;
						height = 2160;
						break;
					}
				}

				D3D11_CREATE_DEVICE_FLAG deviceFlag = D3D11_CREATE_DEVICE_FLAG(0);

#ifdef _DEBUG
				deviceFlag = D3D11_CREATE_DEVICE_FLAG(deviceFlag | D3D11_CREATE_DEVICE_DEBUG);
#endif

				if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
				{
					deviceFlag = D3D11_CREATE_DEVICE_FLAG(deviceFlag | D3D11_CREATE_DEVICE_BGRA_SUPPORT); // | D3D11_CREATE_DEVICE_VIDEO_SUPPORT);
				}

				DXGI_SWAP_CHAIN_DESC1 swapChainStruct;

				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
					swapChainStruct.Format = DXGI_FORMAT_R10G10B10A2_UNORM;
				else if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
					swapChainStruct.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
				else
					swapChainStruct.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

				swapChainStruct.Width = width;
				swapChainStruct.Height = height;
				swapChainStruct.Stereo = FALSE;
				swapChainStruct.Scaling = DXGI_SCALING_NONE;
				swapChainStruct.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainStruct.BufferCount = 2u;
				swapChainStruct.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
				swapChainStruct.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL; // DXGI_SWAP_EFFECT_FLIP_DISCARD works as well ???
				swapChainStruct.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED; // MIGHT NEED TO CHANGE THIS*******************

				swapChainStruct.SampleDesc.Count = msaaSample;
				swapChainStruct.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

				// Create the Direct3D 11 API device object and a corresponding context.
				winrt::com_ptr<ID3D11Device> device;
				winrt::com_ptr<ID3D11DeviceContext> context;

				HRESULT hr = D3D11CreateDevice(
					nullptr,
					D3D_DRIVER_TYPE_HARDWARE,
					nullptr,
					deviceFlag,
					featureLevels,
					ARRAYSIZE(featureLevels),
					D3D11_SDK_VERSION,
					device.put(),
					nullptr,
					context.put()
				);
				// if init fails, fall back to software
				if (FAILED(hr))
				{
					winrt::check_hresult(
						D3D11CreateDevice(
							nullptr,
							D3D_DRIVER_TYPE_WARP,
							nullptr,
							deviceFlag,
							featureLevels,
							ARRAYSIZE(featureLevels),
							D3D11_SDK_VERSION,
							device.put(),
							nullptr,
							context.put()
						)
					);
				}

				// Store pointers to the Direct3D 11.1 API device and immediate context.
				pDevice = device.as<ID3D11Device3>();
				pImmediateContext = context.as<ID3D11DeviceContext3>();

				// Obtain the DXGI factory that was used to create the Direct3D device.
				winrt::com_ptr<IDXGIDevice3> dxgiDevice;
				dxgiDevice = pDevice.as<IDXGIDevice3>();

				winrt::com_ptr<IDXGIAdapter> dxgiAdapter;
				winrt::check_hresult(
					dxgiDevice->GetAdapter(dxgiAdapter.put())
				);

				winrt::com_ptr<IDXGIFactory3> dxgiFactory;
				winrt::check_hresult(
					dxgiAdapter->GetParent(IID_PPV_ARGS(&dxgiFactory))
				);
				auto process = CallOnMainViewUiThreadAsync([&]()
					{
						hr = dxgiFactory->CreateSwapChainForCoreWindow(
							pDevice.get(),
							winrt::get_unknown(*m_window),
							&swapChainStruct,
							nullptr,
							pSwapChain.put()
						);
					});

				process.get();

				if (hr != S_OK)
					return GReturn::HARDWARE_UNAVAILABLE;

				// Create the rasterizer state
				D3D11_RASTERIZER_DESC rasterizerDesc;
				ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
				rasterizerDesc.AntialiasedLineEnable = true;
				rasterizerDesc.CullMode = D3D11_CULL_BACK;
				rasterizerDesc.DepthBias = 0;
				rasterizerDesc.DepthBiasClamp = 0.0f;
				rasterizerDesc.DepthClipEnable = true;
				rasterizerDesc.FillMode = D3D11_FILL_SOLID;
				rasterizerDesc.FrontCounterClockwise = false;
				rasterizerDesc.MultisampleEnable = true;
				rasterizerDesc.ScissorEnable = false;
				rasterizerDesc.SlopeScaledDepthBias = 0.0f;

				// Create the rasterizer state object
				hr = pDevice->CreateRasterizerState(&rasterizerDesc, &pRasterizerState);
				if (hr != S_OK)
					return GReturn::FAILURE;

				// Set the rasterizer state
				pImmediateContext->RSSetState(pRasterizerState.Get());

				ID3D11Resource* buffer;
				pSwapChain->GetBuffer(0, __uuidof(buffer), reinterpret_cast<void**>(&buffer));

				if (buffer)
					pDevice->CreateRenderTargetView(buffer, nullptr, &pBackBufferView);

				buffer->Release();

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
				{
					/////////////////////////////////
					// Create Depth Buffer Texture //
					/////////////////////////////////

					D3D11_TEXTURE2D_DESC depthTextureDesc = { 0 };
					depthTextureDesc.Width = width;
					depthTextureDesc.Height = height;
					depthTextureDesc.ArraySize = 1;
					depthTextureDesc.MipLevels = 1;

					depthTextureDesc.SampleDesc.Count = msaaSample;
					depthTextureDesc.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

					if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
						depthTextureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
					else
						depthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;

					depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

					ID3D11Texture2D* depthBuffer;
					pDevice->CreateTexture2D(&depthTextureDesc, nullptr, &depthBuffer);

					///////////////////////////////
					// Create Depth Stencil View //
					///////////////////////////////

					D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
					ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

					if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
						depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
					else
						depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

					if (msaaSample > 1)
						depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
					else
						depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

					if (depthBuffer)
						hr = pDevice->CreateDepthStencilView(depthBuffer, &depthStencilViewDesc, &pDepthStencilView);

					depthBuffer->Release();
				}

				/////////////////////////
				// Initialize Viewport //
				/////////////////////////

				D3D11_VIEWPORT viewport;
				viewport.Width = static_cast<float>(width);
				viewport.Height = static_cast<float>(height);
				viewport.MinDepth = 0.0f;
				viewport.MaxDepth = 1.0f;

				unsigned int nTopLeftX = 0;
				unsigned int nTopLeftY = 0;
				gwindow.GetClientTopLeft(nTopLeftX, nTopLeftY);

				viewport.TopLeftX = 0;// static_cast<float>(nTopLeftX);
				viewport.TopLeftY = 0;// static_cast<float>(nTopLeftY);

				
				pImmediateContext->RSSetViewports(1, &viewport);
				
				// Call back event handler for DX11
				GReturn gr = responder.Create([&](const GEvent& event)
					{
						GW::SYSTEM::GWindow::Events windowEvent;
						GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
						event.Read(windowEvent, windowEventData);
						switch (windowEvent)
						{
						case GW::SYSTEM::GWindow::Events::MINIMIZE: {} break;
						case GW::SYSTEM::GWindow::Events::DESTROY: { m_Deallocated = true; } break;

						case GW::SYSTEM::GWindow::Events::MAXIMIZE:
						case GW::SYSTEM::GWindow::Events::RESIZE:
						{
							// resize event has fired
							// what would happen here has moved to EVENTS_PROCESSED due to it needing to be run on a non UI thread
							resizeCounter++;
						}
						break;

						case GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED:
						{
							if (resizeCounter > 0)
							{
								resizeCounter--;

								gwindow.GetClientWidth(width);
								gwindow.GetClientHeight(height);

								aspectRatio = static_cast<float>(width) / static_cast<float>(height);

								if (pSwapChain)
								{
									pBackBufferView.Reset();

									HRESULT hr = pSwapChain->ResizeBuffers(0, width, height, DXGI_FORMAT_UNKNOWN, 0);
									if (hr != S_OK)
										return;

									ID3D11Texture2D* newRTVBuffer;
									hr = pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&newRTVBuffer));

									if (hr != S_OK)
										return;

									hr = pDevice->CreateRenderTargetView(newRTVBuffer, nullptr, &pBackBufferView);
									newRTVBuffer->Release();

									if (hr != S_OK)
										return;

									D3D11_TEXTURE2D_DESC depthTextureDesc = { 0 };
									depthTextureDesc.Width = width;
									depthTextureDesc.Height = height;
									depthTextureDesc.ArraySize = 1;
									depthTextureDesc.MipLevels = 1;
									depthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
									depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

									depthTextureDesc.SampleDesc.Count = msaaSample;
									depthTextureDesc.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

									if (pDepthStencilView)
									{
										pDepthStencilView.Reset();
										ID3D11Texture2D* depthBuffer;
										pDevice->CreateTexture2D(&depthTextureDesc, nullptr, &depthBuffer);

										D3D11_DEPTH_STENCIL_VIEW_DESC newDSVdesc;
										ZeroMemory(&newDSVdesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
										newDSVdesc.Format = DXGI_FORMAT_D32_FLOAT;
										newDSVdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

										if (depthBuffer)
											pDevice->CreateDepthStencilView(depthBuffer, &newDSVdesc, &pDepthStencilView);

										depthBuffer->Release();
									}
									D3D11_VIEWPORT viewport;
									viewport.TopLeftX = 0;
									viewport.TopLeftY = 0;
									viewport.Width = static_cast<float>(width);
									viewport.Height = static_cast<float>(height);
									viewport.MinDepth = 0.0f;
									viewport.MaxDepth = 1.0f;

									pImmediateContext->RSSetViewports(1, &viewport);
								}
							}
						}
						break;

						case GW::SYSTEM::GWindow::Events::MOVE:
						{
							gwindow.GetClientWidth(width);
							gwindow.GetClientHeight(height);

							D3D11_VIEWPORT viewport;
							viewport.TopLeftX = 0;
							viewport.TopLeftY = 0;
							viewport.Width = static_cast<float>(width);
							viewport.Height = static_cast<float>(height);
							viewport.MinDepth = 0.0f;
							viewport.MaxDepth = 1.0f;

							pImmediateContext->RSSetViewports(1, &viewport);
						}
						break;
						}
					});
				return gwindow.Register(responder);
			}

			GReturn GetAspectRatio(float& _outRatio) const override
			{
				if (!gwindow)
					return GReturn::FAILURE;

				_outRatio = aspectRatio;
				return GReturn::SUCCESS;
			}

			GReturn GetDevice(void** _outDevice) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				*_outDevice = pDevice.get();
				pDevice->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetImmediateContext(void** _outContext) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				*_outContext = pImmediateContext.get();
				pImmediateContext->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchain(void** _outSwapchain) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				*_outSwapchain = pSwapChain.get();
				pSwapChain->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetRenderTargetView(void** _outRenderTarget) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				*_outRenderTarget = pBackBufferView.Get();
				pBackBufferView->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetDepthStencilView(void** _outDepthStencilView) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				*_outDepthStencilView = pDepthStencilView.Get();
				pDepthStencilView->AddRef();
				return GReturn::SUCCESS;
			}
			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return responder.Assign(_newHandler);
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return responder.Assign(_newEventHandler);
			}
			GReturn Invoke() const override {
				return responder.Invoke();
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return responder.Invoke(_incomingEvent);
			}
		};
	}
}