#include <wrl/client.h>
#include <d3d11.h>
#include <d2d1.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d2d1.lib")

namespace GW
{
	namespace I
	{
		class GDirectX11SurfaceImplementation : public virtual GDirectX11SurfaceInterface,
			private virtual GEventResponderImplementation, private virtual GThreadSharedImplementation
		{
		private:
			GW::SYSTEM::GWindow gwindow;
			GW::CORE::GEventResponder responder;
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE UWH = { nullptr, nullptr };
			Microsoft::WRL::ComPtr<ID3D11Device> pDevice = nullptr;
			Microsoft::WRL::ComPtr<ID3D11DeviceContext> pImmediateContext = nullptr;
			Microsoft::WRL::ComPtr<IDXGISwapChain> pSwapChain = nullptr;
			Microsoft::WRL::ComPtr<ID3D11RenderTargetView> pBackBufferView = nullptr;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDepthStencilView = nullptr;
			Microsoft::WRL::ComPtr<ID3D11RasterizerState> pRasterizerState = nullptr;
			unsigned int width = 0;
			unsigned int height = 0;
			float aspectRatio = 0.0f;
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask)
			{
				if (!_gwindow)
					return GReturn::INVALID_ARGUMENT;

				gwindow = _gwindow;
				//Check if valid _initMask was passed in
				unsigned long long allowed = ~(
					GW::GRAPHICS::COLOR_10_BIT |
					GW::GRAPHICS::DEPTH_BUFFER_SUPPORT |
					GW::GRAPHICS::DEPTH_STENCIL_SUPPORT |
					GW::GRAPHICS::DIRECT2D_SUPPORT |
					GW::GRAPHICS::MSAA_2X_SUPPORT |
					GW::GRAPHICS::MSAA_4X_SUPPORT |
					GW::GRAPHICS::MSAA_8X_SUPPORT |
					GW::GRAPHICS::MSAA_16X_SUPPORT
					);
				if (allowed & _initMask)
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				// If COLOR_10_BIT and any MSAA flag is set, return FEATURE_UNSUPPORTED
				if ((_initMask & GW::GRAPHICS::COLOR_10_BIT) &&
					(_initMask & GW::GRAPHICS::MSAA_2X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_4X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_8X_SUPPORT ||
					_initMask & GW::GRAPHICS::MSAA_16X_SUPPORT))
				{
					return GReturn::FEATURE_UNSUPPORTED;
				}

				short msaaSample = 1;
				if (_initMask & GW::GRAPHICS::MSAA_2X_SUPPORT)
					msaaSample = 2;
				else if (_initMask & GW::GRAPHICS::MSAA_4X_SUPPORT)
					msaaSample = 4;
				else if (_initMask & GW::GRAPHICS::MSAA_8X_SUPPORT)
					msaaSample = 8;
				else if (_initMask & GW::GRAPHICS::MSAA_16X_SUPPORT)
					msaaSample = 16;

				gwindow.GetWindowHandle(UWH);
				gwindow.GetClientWidth(width);
				gwindow.GetClientHeight(height);
				aspectRatio = static_cast<float>(width) / static_cast<float>(height);

				D3D_FEATURE_LEVEL featureLevels[] =
				{
					D3D_FEATURE_LEVEL_11_1,
					D3D_FEATURE_LEVEL_11_0,
					D3D_FEATURE_LEVEL_10_1,
					D3D_FEATURE_LEVEL_10_0,
					D3D_FEATURE_LEVEL_9_3,
					D3D_FEATURE_LEVEL_9_2,
					D3D_FEATURE_LEVEL_9_1
				};

				D3D11_CREATE_DEVICE_FLAG deviceFlag = D3D11_CREATE_DEVICE_FLAG(0);

#ifdef _DEBUG
				deviceFlag = D3D11_CREATE_DEVICE_FLAG(deviceFlag | D3D11_CREATE_DEVICE_DEBUG);
#endif

				if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
				{
					deviceFlag = D3D11_CREATE_DEVICE_FLAG(deviceFlag | D3D11_CREATE_DEVICE_BGRA_SUPPORT); // | D3D11_CREATE_DEVICE_VIDEO_SUPPORT);
				}

				DXGI_SWAP_CHAIN_DESC swapChainStruct;
				swapChainStruct.BufferCount = 1;

				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
					swapChainStruct.BufferDesc.Format = DXGI_FORMAT_R10G10B10A2_UNORM;
				else if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
					swapChainStruct.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
				else
					swapChainStruct.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

				swapChainStruct.BufferDesc.Width = width;
				swapChainStruct.BufferDesc.Height = height;
				ZeroMemory(&swapChainStruct.BufferDesc.RefreshRate, sizeof(swapChainStruct.BufferDesc.RefreshRate));
				ZeroMemory(&swapChainStruct.BufferDesc.Scaling, sizeof(swapChainStruct.BufferDesc.Scaling));
				ZeroMemory(&swapChainStruct.BufferDesc.ScanlineOrdering, sizeof(swapChainStruct.BufferDesc.ScanlineOrdering));
				swapChainStruct.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainStruct.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
				swapChainStruct.OutputWindow = static_cast<HWND>(UWH.window);
				swapChainStruct.Windowed = TRUE;
				swapChainStruct.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

				swapChainStruct.SampleDesc.Count = msaaSample;
				swapChainStruct.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

				HRESULT hr = D3D11CreateDeviceAndSwapChain(
					nullptr,
					D3D_DRIVER_TYPE_HARDWARE,
					nullptr,
					deviceFlag,
					featureLevels,
					ARRAYSIZE(featureLevels),
					D3D11_SDK_VERSION,
					&swapChainStruct,
					&pSwapChain,
					&pDevice,
					nullptr,
					&pImmediateContext
				);
				if (hr != S_OK)
					return GReturn::HARDWARE_UNAVAILABLE;

				// Create the rasterizer state
				D3D11_RASTERIZER_DESC rasterizerDesc;
				ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
				rasterizerDesc.AntialiasedLineEnable = true;
				rasterizerDesc.CullMode = D3D11_CULL_BACK;
				rasterizerDesc.DepthBias = 0;
				rasterizerDesc.DepthBiasClamp = 0.0f;
				rasterizerDesc.DepthClipEnable = true;
				rasterizerDesc.FillMode = D3D11_FILL_SOLID;
				rasterizerDesc.FrontCounterClockwise = false;
				rasterizerDesc.MultisampleEnable = true;
				rasterizerDesc.ScissorEnable = false;
				rasterizerDesc.SlopeScaledDepthBias = 0.0f;

				// Create the rasterizer state object
				hr = pDevice->CreateRasterizerState(&rasterizerDesc, &pRasterizerState);
				if (hr != S_OK)
					return GReturn::FAILURE;

				// Set the rasterizer state
				pImmediateContext->RSSetState(pRasterizerState.Get());

				ID3D11Resource* buffer;
				pSwapChain->GetBuffer(0, __uuidof(buffer), reinterpret_cast<void**>(&buffer));

				if (buffer)
					pDevice->CreateRenderTargetView(buffer, nullptr, &pBackBufferView);

				buffer->Release();

				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT)
				{
					/////////////////////////////////
					// Create Depth Buffer Texture //
					/////////////////////////////////

					D3D11_TEXTURE2D_DESC depthTextureDesc = { 0 };
					depthTextureDesc.Width = width;
					depthTextureDesc.Height = height;
					depthTextureDesc.ArraySize = 1;
					depthTextureDesc.MipLevels = 1;

					depthTextureDesc.SampleDesc.Count = msaaSample;
					depthTextureDesc.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

					if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
						depthTextureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
					else
						depthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;

					depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

					ID3D11Texture2D* depthBuffer;
					pDevice->CreateTexture2D(&depthTextureDesc, nullptr, &depthBuffer);

					///////////////////////////////
					// Create Depth Stencil View //
					///////////////////////////////

					D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
					ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

					if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
						depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
					else
						depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;

					if (msaaSample > 1)
						depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
					else
						depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

					if (depthBuffer)
						hr = pDevice->CreateDepthStencilView(depthBuffer, &depthStencilViewDesc, &pDepthStencilView);

					depthBuffer->Release();
				}

				/////////////////////////
				// Initialize Viewport //
				/////////////////////////

				D3D11_VIEWPORT viewport;
				viewport.TopLeftX = 0;
				viewport.TopLeftY = 0;
				viewport.Width = static_cast<float>(width);
				viewport.Height = static_cast<float>(height);
				viewport.MinDepth = 0.0f;
				viewport.MaxDepth = 1.0f;

				pImmediateContext->RSSetViewports(1, &viewport);
				// Call back event handler for DX11
				GReturn result = responder.Create([&](const GEvent& event)
					{
						GW::SYSTEM::GWindow::Events windowEvent;
						GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
						if (+event.Read(windowEvent, windowEventData))
						{
							switch (windowEvent)
							{
							case GW::SYSTEM::GWindow::Events::MINIMIZE: {} break;
							case GW::SYSTEM::GWindow::Events::DESTROY: {} break;

							case GW::SYSTEM::GWindow::Events::MAXIMIZE:
							case GW::SYSTEM::GWindow::Events::RESIZE:
							{
								gwindow.GetClientWidth(width);
								gwindow.GetClientHeight(height);

								aspectRatio = static_cast<float>(width) / static_cast<float>(height);

								if (pSwapChain)
								{
									pBackBufferView.Reset();

									HRESULT hr = pSwapChain->ResizeBuffers(0, width, height, DXGI_FORMAT_UNKNOWN, 0);
									if (hr != S_OK)
										return;

									ID3D11Texture2D* newRTVBuffer;
									hr = pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&newRTVBuffer));

									if (hr != S_OK)
										return;

									hr = pDevice->CreateRenderTargetView(newRTVBuffer, nullptr, &pBackBufferView);
									newRTVBuffer->Release();

									if (hr != S_OK)
										return;

									D3D11_TEXTURE2D_DESC depthTextureDesc = { 0 };
									depthTextureDesc.Width = width;
									depthTextureDesc.Height = height;
									depthTextureDesc.ArraySize = 1;
									depthTextureDesc.MipLevels = 1;
									depthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
									depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

									depthTextureDesc.SampleDesc.Count = msaaSample;
									depthTextureDesc.SampleDesc.Quality = msaaSample > 1 ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

									if (pDepthStencilView)
									{
										pDepthStencilView.Reset();
										ID3D11Texture2D* depthBuffer;
										pDevice->CreateTexture2D(&depthTextureDesc, nullptr, &depthBuffer);

										D3D11_DEPTH_STENCIL_VIEW_DESC newDSVdesc;
										ZeroMemory(&newDSVdesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
										newDSVdesc.Format = DXGI_FORMAT_D32_FLOAT;
										newDSVdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

										if (depthBuffer)
											pDevice->CreateDepthStencilView(depthBuffer, &newDSVdesc, &pDepthStencilView);

										depthBuffer->Release();
									}

									D3D11_VIEWPORT viewport;
									viewport.TopLeftX = 0;
									viewport.TopLeftY = 0;
									viewport.Width = static_cast<float>(width);
									viewport.Height = static_cast<float>(height);
									viewport.MinDepth = 0.0f;
									viewport.MaxDepth = 1.0f;

									pImmediateContext->RSSetViewports(1, &viewport);
								}
							}
							break;

							case GW::SYSTEM::GWindow::Events::MOVE:
							{
								gwindow.GetClientWidth(width);
								gwindow.GetClientHeight(height);

								D3D11_VIEWPORT viewport;
								viewport.TopLeftX = 0;
								viewport.TopLeftY = 0;
								viewport.Width = static_cast<float>(width);
								viewport.Height = static_cast<float>(height);
								viewport.MinDepth = 0.0f;
								viewport.MaxDepth = 1.0f;

								pImmediateContext->RSSetViewports(1, &viewport);
							}
							break;
							}
						}
					});
				if (G_PASS(result))
				{
					gwindow.Register(responder);
				}
				return result;
			}

			GReturn GetAspectRatio(float& _outRatio) const override
			{
				if (!gwindow)
					return GReturn::FAILURE;

				_outRatio = aspectRatio;
				return GReturn::SUCCESS;
			}

			GReturn GetDevice(void** _outDevice) const override
			{
				if (!pDevice)
					return GReturn::FAILURE;

				*_outDevice = pDevice.Get();
				pDevice->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetImmediateContext(void** _outContext) const override
			{
				if (!pImmediateContext)
					return GReturn::FAILURE;

				*_outContext = pImmediateContext.Get();
				pImmediateContext->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchain(void** _outSwapchain) const override
			{
				if (!pSwapChain)
					return GReturn::FAILURE;

				*_outSwapchain = pSwapChain.Get();
				pSwapChain->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetRenderTargetView(void** _outRenderTarget) const override
			{
				if (!pBackBufferView)
					return GReturn::FAILURE;

				*_outRenderTarget = pBackBufferView.Get();
				pBackBufferView->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetDepthStencilView(void** _outDepthStencilView) const override
			{
				if (!pDepthStencilView)
					return GReturn::FAILURE;

				*_outDepthStencilView = pDepthStencilView.Get();
				pDepthStencilView->AddRef();
				return GReturn::SUCCESS;
			}
			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return responder.Assign(_newHandler);
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return responder.Assign(_newEventHandler);
			}
			GReturn Invoke() const override {
				return responder.Invoke();
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return responder.Invoke(_incomingEvent);
			}
		};
	}
}
