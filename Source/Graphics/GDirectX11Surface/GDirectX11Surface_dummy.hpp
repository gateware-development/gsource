namespace GW
{
	namespace I
	{
		class GDirectX11SurfaceImplementation : public virtual GDirectX11SurfaceInterface
		{
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask) {
				return GReturn::FEATURE_UNSUPPORTED; 
			}
			GReturn GetAspectRatio(float& _outRatio) const override {
				return GReturn::FAILURE; 
			}
			GReturn GetDevice(void** _outDevice) const override {
				return GReturn::FAILURE; 
			}
			GReturn GetImmediateContext(void** _outContext) const override {
				return GReturn::FAILURE; 
			}
			GReturn GetSwapchain(void** _outSwapchain) const override {
				return GReturn::FAILURE; 
			}
			GReturn GetRenderTargetView(void** _outRenderTarget) const override {
				return GReturn::FAILURE; 
			}
			GReturn GetDepthStencilView(void** _outDepthStencilView) const override {
				return GReturn::FAILURE; 
			}
			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return GReturn::FAILURE;
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return GReturn::FAILURE;
			}
		};
	}
}