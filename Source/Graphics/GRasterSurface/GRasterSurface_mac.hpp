#ifdef __OBJC__
@import Foundation;
@import Cocoa;
#endif

namespace GW
{
	namespace I
	{
		class GRasterSurfaceImplementation;
	}
}


#include "../../../Interface/Graphics/GGraphicsDefines.h"
#include "../../../Interface/Math/GMathDefines.h"
#include "../../../Interface/Core/GEventResponder.h"
#include "../../../Interface/Core/GThreadShared.h"
#include "../../../Interface/System/GConcurrent.h"
#include "../../../Interface/System/GWindow.h"
#include "../../Shared/macutils.h"

#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <assert.h>


//#define GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY		// uncomment this line to disable multithreading in SmartUpdateSurface for debugging
//#define GRASTERSURFACE_DEBUG_MAC_DEBUG_ASSERTS					// uncomment this line to enable debug asserts
//#define GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINTS						// uncomment this line to enable debug messages to be printed to the console
//#define GRASTERSURFACE_DEBUG_MAC_PRINT_PRIORITY_LEVEL 0			// use this to set the priority level of debug messages to print (lower number = higher priority, any priority levels greater than this will be ignored)


namespace internal_gw
{
	// GMacBLITView Interface

	// Data members of GMacBLITView
	G_OBJC_DATA_MEMBERS_STRUCT(GMacBLITView)
	{
		unsigned int* 	m_data;
		CGContextRef 	m_context;
		CGImageRef 		m_image;
		CGColorSpaceRef	m_colorspace;

		unsigned int 	m_width;
		unsigned int 	m_height;
		unsigned int 	m_bitsPerComponent;
		unsigned int 	m_bytesPerRow;
	};

	// Forward declarations of GMacMusic methods
	G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(GMacBLITView);

	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, id, initUsingFrame, NSRect _frameRect);
	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, void, drawRect, CGRect _rect); // Override of inherited function.
	G_OBJC_HEADER_INSTANCE_METHOD(GMacBLITView, BOOL, isFlipped); // Override of inherited function.
	G_OBJC_HEADER_INSTANCE_METHOD(GMacBLITView, void, cleanup);
	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, void, update, unsigned int* _pixels, unsigned int _pixelCount);

	// Creates the GMacBLITView class at runtime when G_OBJC_GET_CLASS(GMacBLITView) is called.
	G_OBJC_CLASS_BEGIN(GMacBLITView, NSView)
	{
		G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(GMacBLITView);

		G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacBLITView, initUsingFrame, "@@:{CGRect={CGPoint=dd}{CGSize=dd}}", :);
		G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacBLITView, drawRect, "v@:{CGRect={CGPoint=dd}{CGSize=dd}}", :);
		G_OBJC_CLASS_METHOD(GMacBLITView, isFlipped, "c@:");
		G_OBJC_CLASS_METHOD(GMacBLITView, cleanup, "v@:");
		G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacBLITView, update, "v@:^II", ::);
	}
	G_OBJC_CLASS_END(GMacBLITView)

	// GMacBLITView Interface End
}


namespace GW
{
	namespace I
	{
		class GRasterSurfaceImplementation : public virtual GRasterSurfaceInterface,
			protected GEventResponderImplementation
		{
		private:

#pragma region DEFINES

			typedef unsigned int		Color; // XRGB quadruple
			typedef unsigned char		ColorChannel; // single value from a Color (X, R, G, or B)

#pragma endregion DEFINES
#pragma region ENUMS_STRUCTS_AND_CLASSES

			// GRasterUpdateFlags bit positions for left/right shifting
			enum UpdateFlagBitPosition
			{
				BIT_ALIGN_X_LEFT = 0,
				BIT_ALIGN_X_CENTER,
				BIT_ALIGN_X_RIGHT,
				BIT_ALIGN_Y_TOP,
				BIT_ALIGN_Y_CENTER,
				BIT_ALIGN_Y_BOTTOM,
				BIT_UPSCALE_2X,
				BIT_UPSCALE_3X,
				BIT_UPSCALE_4X,
				BIT_UPSCALE_8X,
				BIT_UPSCALE_16X,
				BIT_STRETCH_TO_FIT,
				BIT_INTERPOLATE_NEAREST,
				BIT_INTERPOLATE_BILINEAR,
			};

			// flags indicating the active state of the surface
			enum class SurfaceState : int
			{
				INVALID		= -1, // GWindow has been destroyed and surface cannot work properly
				INACTIVE	=  0, // surface is not active and will not draw
				ACTIVE		=  1, // surface is active and can draw
			};

			// class containing data unique to each buffer
			class Buffer : protected GThreadSharedImplementation
			{
			private:

				// prints a debug message to the console if GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINTS is defined (only prints if priority level is set above 0)
				inline void const DebugPrint(const char* _msg) const
				{
#if defined(GRASTERSURFACE_DEBUG_MAC_PRINTS) && defined(GRASTERSURFACE_DEBUG_MAC_PRINT_PRIORITY_LEVEL)
				if (GRASTERSURFACE_DEBUG_MAC_PRINT_PRIORITY_LEVEL > 0)
					std::printf("%s buffer - %s", debugName, _msg);
#endif
				}

			public:
				Color*						data = nullptr;
				unsigned int				numLocks = 0;
				unsigned long long			frameNum = 0;
				char						debugName[16];

				GReturn Create(const char* _debugName)
				{
					snprintf(debugName, 16, "%s", _debugName);
					DebugPrint("Create\n");
					return GThreadSharedImplementation::Create();
				}

				GReturn LockSyncWrite() override
				{
					DebugPrint("LockSyncWrite\n");
					return GThreadSharedImplementation::LockSyncWrite();
				}

				GReturn LockAsyncRead() const override
				{
					DebugPrint("LockAsyncRead\n");
					return GThreadSharedImplementation::LockAsyncRead();
				}

				GReturn UnlockSyncWrite() override
				{
					DebugPrint("UnlockSyncWrite\n");
					return GThreadSharedImplementation::UnlockSyncWrite();
				}

				GReturn UnlockAsyncRead() const override
				{
					DebugPrint("UnlockAsyncRead\n");
					return GThreadSharedImplementation::UnlockAsyncRead();
				}
			};

			// struct containing variables needed by SmartUpdateSurface
			struct SmartUpdateData
			{
				const Color*			inputColors = nullptr;			// pointer to input colors to use for update
				unsigned int			bitmapWidth = 0;				// local copy of bitmap width since it could be altered during execution
				unsigned int			bitmapHeight = 0;				// local copy of bitmap height, since it could be altered during execution
				unsigned int			rawDataWidth = 0;				// width of raw data passed to function
				unsigned int			rawDataHeight = 0;				// height of raw data passed to function
				unsigned int			upscaledDataWidth = 0;			// width of data after upscaling
				unsigned int			upscaledDataHeight = 0;			// height of data after upscaling
				int						offsetX = 0;					// horizontal offset of processed data
				int						offsetY = 0;					// vertical offset of processed data
				float					coordScaleRatioX = 1.0f;		// horizontal pixel coordinate scaling ratio
				float					coordScaleRatioY = 1.0f;		// vertical pixel coordinate scaling ratio
			};

#pragma endregion ENUMS_STRUCTS_AND_CLASSES
#pragma region VARIABLES

			// Gateware types are named as:		gVariableName
			// Cocoa/Mac types are named as:	cVariableName

			GW::SYSTEM::GWindow						m_gWindow;											// parent GWindow
			GW::CORE::GEventResponder				m_gEventListener;									// listener used to subscribe to GWindow events
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE		m_gUniversalWindowHandle = { nullptr, nullptr };	// universal window handle extracted from GWindow
			GW::SYSTEM::GConcurrent					m_gDrawThread;										// GConcurrent used for swapping buffers and drawing
			GW::SYSTEM::GConcurrent					m_gSmartUpdateRowProcessThread;						// GConcurrent used for copying data in SmartUpdateSurface

			unsigned int*							m_cImagePixelData = nullptr;						// bitmap needed by Cocoa image object
			id										m_cBlitView;										// instance of Cocoa view subclass

			SurfaceState							m_surfaceState = SurfaceState::INACTIVE;
			unsigned long long						m_frameCount = 0;
			Color*									m_bitmapData = nullptr;								// buffer color data (size is 2 * (width * height) since it contains both buffers' data)
			unsigned short							m_bitmapWidth = 0;									// width of surface data, as well as front and back buffers
			unsigned short							m_bitmapHeight = 0;									// height of surface data, as well as front and back buffers;
			Buffer									m_frontBuffer;										// buffer used to transfer the current frame to the screen
			Buffer									m_backBuffer; 										// buffer used to prepare the next frame

#pragma endregion VARIABLES
#pragma region PRIVATE_FUNCTIONS

			// Prints a debug message to the console if GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINTS is defined.
			/*
			*	Used to assist with debugging, since multithreading makes debugging tools harder to use.
			*	If int arguments are passed, they will be inserted into the output if _msg is a format string.
			*/
			static inline void DebugPrint(int _priorityLevel, const char* _msg,
				long long _intArg0 = 0, long long _intArg1 = 0, long long _intArg2 = 0, long long _intArg3 = 0,
				long long _intArg4 = 0, long long _intArg5 = 0, long long _intArg6 = 0, long long _intArg7 = 0)
			{
#if defined(GRASTERSURFACE_DEBUG_MAC_PRINTS) && defined(GRASTERSURFACE_DEBUG_MAC_PRINT_PRIORITY_LEVEL)
				if (_priorityLevel <= GRASTERSURFACE_DEBUG_MAC_PRINT_PRIORITY_LEVEL)
					std::printf(_msg, _intArg0, _intArg1, _intArg2, _intArg3, _intArg4, _intArg5, _intArg6, _intArg7);
#endif
			}

			// Calls assert on the passed-in statement if GRASTERSURFACE_MAC_DEBUG_ASSERTS is defined.
			static inline void DebugAssert(bool _statement)
			{
#if defined(GRASTERSURFACE_DEBUG_MAC_DEBUG_ASSERTS)
				assert(_statement);
#endif
			}

			// Inverts the byte order of a color to match the format of an NSImage.
			inline Color ColorXRGBtoBGRX(Color _xrgbColor)
			{
				return (_xrgbColor & 0xff000000) >> 24
					|  (_xrgbColor & 0x00ff0000) >> 8
					|  (_xrgbColor & 0x0000ff00) << 8
					|  (_xrgbColor & 0x000000ff) << 24;
			}

			// Destroys and recreates bitmap data with new dimensions.
			/*
			*	Makes the surface active.
			*
			*	retval GReturn::SUCCESS					Completed successfully.
			*	retval GReturn::PREMATURE_DEALLOCATION	The associated GWindow was deallocated and the surface is now invalid.
			*	retval GReturn::UNEXPECTED_RESULT		The function entered an area it was not supposed to be able to.
			*/
			GReturn ResizeBitmap(unsigned int _width, unsigned int _height)
			{
				DebugPrint(1, "\nResizeBitmap\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "ResizeBitmap - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// lock to prevent read/write conflicts
				DebugPrint(2, "ResizeBitmap - attempt to lock self, back buffer, front buffer for write\n");
				if (+LockSyncWrite()
					&& +m_backBuffer.LockSyncWrite()
					&& +m_frontBuffer.LockSyncWrite())
				{
					DebugPrint(2, "ResizeBitmap - locked self, back buffer, front buffer for write\n");
					// store new dimensions
					DebugPrint(2, "ResizeBitmap - store args\n");
					m_bitmapWidth = _width;
					m_bitmapHeight = _height;
					// destroy old Cocoa objects
					DebugPrint(2, "ResizeBitmap - destroy Cocoa objects\n");
					if (m_cImagePixelData)
					{
						free(m_cImagePixelData);
						m_cImagePixelData = nullptr;
					}
					// destroy old bitmap
					DebugPrint(2, "ResizeBitmap - destroy bitmap\n");
					if (m_bitmapData)
					{
						delete[] m_bitmapData;
						m_bitmapData = nullptr;
					}
					// allocate and clear new bitmap
					DebugPrint(2, "ResizeBitmap - create and clear bitmap\n");
					m_bitmapData = new Color[m_bitmapWidth * m_bitmapHeight * 2];
					memset(m_bitmapData, 0x00, (m_bitmapWidth * m_bitmapHeight * 2) * sizeof(Color));
					// reset buffer data pointers
					DebugPrint(2, "ResizeBitmap - reset buffer data pointers\n");
					m_backBuffer.data = &m_bitmapData[0];
					m_frontBuffer.data = &m_bitmapData[m_bitmapWidth * m_bitmapHeight];
					// recreate Cocoa objects
					DebugPrint(2, "ResizeBitmap - recreate Cocoa objects\n");
					m_cImagePixelData = static_cast<unsigned int*>(calloc(m_bitmapWidth * m_bitmapHeight, sizeof(unsigned int)));
					if (m_cBlitView)
					{
						[m_cBlitView removeFromSuperview]; // remove the view from the window
						internal_gw::G_OBJC_CALL_METHOD(GMacBLITView, m_cBlitView, cleanup);
						[m_cBlitView release];
					}
					NSRect windowRect = NSMakeRect(0, 0, _width, _height);
					m_cBlitView = [internal_gw::G_OBJC_GET_CLASS(GMacBLITView) alloc];
					m_cBlitView = internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacBLITView, m_cBlitView, initUsingFrame, windowRect);
					NSWindow* nsWindow = static_cast<NSWindow*>(m_gUniversalWindowHandle.window);
					[[nsWindow contentView] addSubview:m_cBlitView];
					[nsWindow makeFirstResponder:m_cBlitView];
					// mark surface as active
					DebugPrint(2, "ResizeBitmap - make surface active\n");
					m_surfaceState = SurfaceState::ACTIVE;
					// increment and store frame number in front buffer
					DebugPrint(2, "ResizeBitmap - increment and store frame number in front buffer\n");
					m_frontBuffer.frameNum = ++m_frameCount;
					// unlock and return
					DebugPrint(2, "ResizeBitmap - attempt to unlock self, back buffer, front buffer for write\n");
					if (+m_frontBuffer.UnlockSyncWrite()
						&& +m_backBuffer.UnlockSyncWrite()
						&& +UnlockSyncWrite())
					{
						DebugPrint(2, "ResizeBitmap - unlocked self, back buffer, front buffer for write; draw\n");
						DrawFrontBufferToScreen();
						DebugPrint(2, "ResizeBitmap - successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "ResizeBitmap - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			// Swaps back buffer to front for drawing and front buffer to back for new data.
			/*
			*	retval GReturn::SUCCESS					Completed successfully.
			*	retval GReturn::IGNORED					Back buffer data is older than front buffer data.
			*	retval GReturn::PREMATURE_DEALLOCATION	The associated GWindow was deallocated and the surface is now invalid.
			*	retval GReturn::UNEXPECTED_RESULT		The function entered an area it was not supposed to be able to.
			*/
			GReturn SwapBackAndFrontBuffers()
			{
				DebugPrint(1, "\nSwapBackAndFrontBuffers\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "SwapBackAndFrontBuffers - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				DebugPrint(2, "SwapBackAndFrontBuffers - attempt to lock back buffer, front buffer for write\n");
				if (+m_backBuffer.LockSyncWrite()
					&& +m_frontBuffer.LockSyncWrite())
				{
					DebugPrint(2, "SwapBackAndFrontBuffers - locked back buffer, front buffer for write\n");
					GReturn gr;
					DebugPrint(2, "SwapBackAndFrontBuffers - compare frame numbers\n");
					if (m_backBuffer.frameNum > m_frontBuffer.frameNum)
					{
						DebugPrint(2, "SwapBackAndFrontBuffers - back buffer has new data; swap buffers\n");
						Color* temp = m_backBuffer.data;
						m_backBuffer.data = m_frontBuffer.data;
						m_frontBuffer.data = temp;
						gr = GReturn::SUCCESS;
					}
					else
					{
						DebugPrint(2, "SwapBackAndFrontBuffers - back buffer outdated\n");
						gr = GReturn::IGNORED;
					}
					DebugPrint(2, "SwapBackAndFrontBuffers - unlock and return\n");
					if (+m_frontBuffer.UnlockSyncWrite()
						&& +m_backBuffer.UnlockSyncWrite())
					{
						DebugPrint(2, "SwapBackAndFrontBuffers - unlocked back buffer, front buffer for write; successful; return\n\n");
						return gr;
					}
				}
				DebugPrint(2, "SwapBackAndFrontBuffers - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			// Draws data in front buffer to screen.
			/*
			*	retval GReturn::SUCCESS					Completed successfully.
			*	retval GReturn::FAILURE					A needed variable was invalid.
			*	retval GReturn::PREMATURE_DEALLOCATION	The associated GWindow was deallocated and the surface is now invalid.
			*	retval GReturn::UNEXPECTED_RESULT		The function entered an area it was not supposed to be able to.
			*/
			GReturn DrawFrontBufferToScreen()
			{
				DebugPrint(1, "\nDrawFrontBufferToScreen\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "DrawFrontBufferToScreen - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				DebugPrint(2, "DrawFrontBufferToScreen - attempt to lock front buffer for read\n");
				if (+m_frontBuffer.LockAsyncRead())
				{
					DebugPrint(2, "DrawFrontBufferToScreen - locked front buffer for read\n");
					GReturn gr;
					// validate needed variables
					DebugPrint(2, "DrawFrontBufferToScreen - validate vars\n");
					if (m_gWindow == nullptr
						|| m_frontBuffer.data == nullptr
						|| m_bitmapData == nullptr)
					{
						DebugPrint(2, "DrawFrontBufferToScreen - m_gWindow, m_frontBuffer.data, or m_bitmapData was nullptr\n");
						gr = GReturn::FAILURE;
					}
					else // if variables are valid, continue
					{
						DebugPrint(2, "DrawFrontBufferToScreen - vars valid\n");
						// prepare to draw
						// transfer data from front buffer to Cocoa image object bitmap
						DebugPrint(2, "DrawFrontBufferToScreen - copy front buffer to Cocoa image object\n");
						memcpy(m_cImagePixelData, m_frontBuffer.data, m_bitmapWidth * m_bitmapHeight * sizeof(Color));
						// ensure proper pixel format before draw
						//   note: this operation could potentially be sped up with multithreading
						DebugPrint(2, "DrawFrontBufferToScreen - invert color channel order in Cocoa image object\n");
						for (unsigned int i = 0; i < m_bitmapWidth * m_bitmapHeight; ++i)
							m_cImagePixelData[i] = ColorXRGBtoBGRX(m_cImagePixelData[i]);
						// draw front buffer to screen
						DebugPrint(2, "DrawFrontBufferToScreen - draw to screen\n");
						internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacBLITView, m_cBlitView, update, m_cImagePixelData, m_bitmapWidth * m_bitmapHeight);
						gr = GReturn::SUCCESS;
					}
					DebugPrint(2, "DrawFrontBufferToScreen - unlock and return\n");
					if (+m_frontBuffer.UnlockAsyncRead())
					{
						DebugPrint(2, "DrawFrontBufferToScreen - unlocked front buffer for read; return\n\n");
						return gr;
					}
				}
				DebugPrint(2, "DrawFrontBufferToScreen - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			// Swaps back and front buffers, then draws front buffer to screen.
			/*
			*	Executes on another thread.
			*
			*	retval GReturn::SUCCESS					Completed successfully.
			*	retval GReturn::FAILURE					Could not converge draw thread.
			*	retval GReturn::PREMATURE_DEALLOCATION	The associated GWindow was deallocated and the surface is now invalid.
			*/
			GReturn SwapBuffersAndDraw()
			{
				DebugPrint(1, "\nSwapBuffersAndDraw\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "SwapBuffersAndDraw - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// Converge draw thread to force completion of outstanding tasks
				DebugPrint(2, "SwapBuffersAndDraw - attempt to converge draw thread\n");
				if (-m_gDrawThread.Converge(0))
				{
					DebugPrint(2, "SwapBuffersAndDraw - converge failed; return\n\n");
					return GReturn::FAILURE;
				}
				DebugPrint(2, "SwapBuffersAndDraw - converge succeeded\n");
				// swap buffers and draw to screen once no tasks are waiting
				DebugPrint(2, "SwapBuffersAndDraw - draw thread branch singular\n");
				m_gDrawThread.BranchSingular([&]() mutable
				{
					if (+SwapBackAndFrontBuffers()) // only draw front buffer if buffers were swapped
					{
						DebugPrint(2, "SwapBuffersAndDraw lambda - buffers were swapped; draw front buffer to screen\n");
						DrawFrontBufferToScreen();
					}
				});
				DebugPrint(2, "SwapBuffersAndDraw - return\n\n");
				return GReturn::SUCCESS;
			}

			// Returns the state of a single bit in a bitfield
			static inline bool IsolateBit(unsigned int _flags, unsigned short _bit)
			{
				return (_flags >> _bit) & 1;
			}

			// Interpolates linearly between two pixel color values.
			static inline Color LerpColor(Color _a, Color _b, float _r)
			{
				// split pixels into channels
				ColorChannel a_channels[4] =
				{
					static_cast<ColorChannel>( (_a & 0x000000FF)),
					static_cast<ColorChannel>(((_a & 0x0000FF00) >>  8)),
					static_cast<ColorChannel>(((_a & 0x00FF0000) >> 16)),
					static_cast<ColorChannel>(((_a & 0xFF000000) >> 24)),
				};
				ColorChannel b_channels[4] =
				{
					static_cast<ColorChannel>( (_b & 0x000000FF)),
					static_cast<ColorChannel>(((_b & 0x0000FF00) >>  8)),
					static_cast<ColorChannel>(((_b & 0x00FF0000) >> 16)),
					static_cast<ColorChannel>(((_b & 0xFF000000) >> 24)),
				};
				// interpolate results
				ColorChannel result[4] =
				{
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[0], b_channels[0], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[1], b_channels[1], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[2], b_channels[2], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[3], b_channels[3], _r))),
				};
				return *(reinterpret_cast<Color*>(result));
			}

			// Returns color at truncated integer coordinate; Faster.
			/*
				_xrgbColors				Array of colors to interpolate within
				_width					Width of color array
				_height					Height of color array
				_u						Horizontal "texel" coordinate
				_v						Vertical "texel" coordinate
			*/
			static inline Color InterpolateColorNearest(const Color* _xrgbColors,
				unsigned int _width, unsigned int _height, float _u, float _v)
			{
				return _xrgbColors[static_cast<unsigned int>(_u) + static_cast<unsigned int>(_v) * _width];
			}

			// Returns color blended between nearest four pixels; Slower.
			/*
				_xrgbColors				Array of colors to interpolate within
				_width					Width of color array
				_height					Height of color array
				_u						Horizontal "texel" coordinate
				_v						Vertical "texel" coordinate
			*/
			static inline Color InterpolateColorBilinear(const Color* _xrgbColors,
				unsigned int _width, unsigned int _height, float _u, float _v)
			{
				// offset coordinates to use pixel corners instead of centers
				_u -= 0.5f;
				_u = (_u < 0.0f) ? 0.0f : _u;
				_v -= 0.5f;
				_v = (_v < 0.0f) ? 0.0f : _v;
				// get top-left coordinates
				unsigned int u0 = static_cast<unsigned int>(_u);
				unsigned int v0 = static_cast<unsigned int>(_v);
				// get bottom-right coordinates
				unsigned int u1 = (u0 < _width - 1)  ? u0 + 1 : u0;
				unsigned int v1 = (v0 < _height - 1) ? v0 + 1 : v0;
				// calculate interpolation ratios
				float rx = _u - u0;
				float ry = _v - v0;
				// interpolate results
				return LerpColor(
					LerpColor(_xrgbColors[u0 + (v0 * _width)], _xrgbColors[u1 + (v0 * _width)], rx),
					LerpColor(_xrgbColors[u0 + (v1 * _width)], _xrgbColors[u1 + (v1 * _width)], rx),
					ry);
			}

			// Iterates over a row of pixels and samples colors with nearest interpolation
			static void ProcessPixelRowNearest(const void* _unused, Color* _output, unsigned int _y, const void* _data)
			{
				DebugPrint(3, "\nProcessPixelRowNearest, row %d\n", _y);
				DebugAssert(_output != nullptr);
				// convert user data to correct type
				DebugAssert(_data != nullptr);
				const SmartUpdateData data = *(reinterpret_cast<const SmartUpdateData*>(_data));
				DebugAssert(data.inputColors != nullptr);
				DebugPrint(4, "ProcessPixelRowNearest, row %d - bitmap width = %d\n", _y, data.bitmapWidth);
				DebugPrint(4, "ProcessPixelRowNearest, row %d - bitmap height = %d\n", _y, data.bitmapHeight);
				DebugAssert(data.bitmapWidth > 0);
				DebugAssert(data.bitmapHeight > 0);
				DebugAssert(_y >= 0);
				DebugAssert(_y < data.bitmapHeight);
				// get y position within rectangular area to draw (should only draw between 0 and upscaled data height)
				int relativeY = _y - data.offsetY;
				// skip this row if not in range
				if (relativeY < 0 || relativeY >= static_cast<int>(data.upscaledDataHeight))
				{
					DebugPrint(5, "ProcessPixelRowNearest, row %d - row out of range\n", _y);
					return;
				}
				// init nearest algorithm vars
				float v = relativeY * data.coordScaleRatioY; // vertical "texel" to sample from in input data
				DebugPrint(4, "ProcessPixelRowNearest, row %d - v = %d\n", _y, static_cast<unsigned int>(v));
				DebugAssert(v >= 0);
				DebugAssert(v < data.bitmapHeight);
				// get x position within rectangular area to draw (should only draw between 0 and upscaled data width)
				int relativeX = G_CLAMP_MIN(-data.offsetX, 0);
				// iterate through pixel row and process pixels
				for (int x = G_CLAMP_MIN(data.offsetX, 0); x < G_CLAMP_MAX(data.offsetX + static_cast<int>(data.upscaledDataWidth), static_cast<int>(data.bitmapWidth)); ++x, ++relativeX)
				{
					float u = relativeX * data.coordScaleRatioX; // horizontal "texel" to sample from in input data
					DebugAssert(u >= 0);
					DebugAssert(u < data.bitmapWidth);
					_output[x] = InterpolateColorNearest(data.inputColors, data.rawDataWidth, data.rawDataHeight, u, v);
				}
				DebugPrint(3, "\n");
			}

			// Iterates over a row of pixels and samples colors with bilinear interpolation
			static void ProcessPixelRowBilinear(const void* _unused, Color* _output, unsigned int _y, const void* _data)
			{
				DebugPrint(3, "\nProcessPixelRowBilinear, row %d\n", _y);
				DebugAssert(_output != nullptr);
				// convert user data to correct type
				DebugAssert(_data != nullptr);
				SmartUpdateData data = *(const_cast<SmartUpdateData*>(reinterpret_cast<const SmartUpdateData*>(_data)));
				DebugAssert(data.inputColors != nullptr);
				DebugPrint(4, "ProcessPixelRowBilinear, row %d - bitmap width = %d\n", _y, data.bitmapWidth);
				DebugPrint(4, "ProcessPixelRowBilinear, row %d - bitmap height = %d\n", _y, data.bitmapHeight);
				DebugAssert(data.bitmapWidth > 0);
				DebugAssert(data.bitmapHeight > 0);
				DebugAssert(_y >= 0);
				DebugAssert(_y < data.bitmapHeight);
				// get y position within rectangular area to draw (should only draw between 0 and upscaled data height)
				int relativeY = _y - data.offsetY;
				// skip this row if not in range
				if (relativeY < 0 || relativeY >= static_cast<int>(data.upscaledDataHeight))
				{
					DebugPrint(5, "ProcessPixelRowBilinear, row %d - row out of range\n", _y);
					return;
				}
				// get x position within rectangular area to draw (should only draw between 0 and upscaled data width)
				int relativeX = G_CLAMP_MIN(-data.offsetX, 0);
				// init bilinear algorithm vars
				float v = relativeY * data.coordScaleRatioY; // vertical "texel" to sample from in input data
				DebugPrint(4, "ProcessPixelRowNearest, row %d - v = %d\n", _y, static_cast<unsigned int>(v));
				DebugAssert(v >= 0);
				DebugAssert(v < data.bitmapHeight);
				// iterate through row and process pixels
				for (int x = G_CLAMP_MIN(data.offsetX, 0); x < G_CLAMP_MAX(data.offsetX + static_cast<int>(data.upscaledDataWidth), static_cast<int>(data.bitmapWidth)); ++x, ++relativeX)
				{
					float u = relativeX * data.coordScaleRatioX; // horizontal "texel" to sample from in input data
					DebugAssert(u >= 0);
					DebugAssert(u < data.bitmapWidth);
					_output[x] = InterpolateColorBilinear(data.inputColors, data.rawDataWidth, data.rawDataHeight, u, v);
				}
				DebugPrint(3, "\n");
			}

			void Cleanup()
			{
				DebugPrint(1, "\nCleanup\n");
				DebugPrint(2, "Cleanup - ensure all locks are released\n");
				while (m_backBuffer.numLocks > 0)
				{
					m_backBuffer.UnlockSyncWrite();
					--m_backBuffer.numLocks;
				}
				while (m_frontBuffer.numLocks > 0)
				{
					m_frontBuffer.UnlockAsyncRead();
					--m_frontBuffer.numLocks;
				}

				DebugPrint(2, "Cleanup - deallocate draw thread object\n");
				m_gDrawThread = nullptr; // draw thread must be stopped before freeing the variables it uses

				DebugPrint(2, "Cleanup - attempt to lock self, back buffer, front buffer for write\n");
				if (+LockSyncWrite()
					&& +m_backBuffer.LockSyncWrite()
					&& +m_frontBuffer.LockSyncWrite())
				{
					DebugPrint(2, "Cleanup - locked self, back buffer, front buffer for write\n");

					DebugPrint(2, "Cleanup - deallocate Cocoa image object's bitmap data\n");
					if (m_cImagePixelData)
					{
						free(m_cImagePixelData);
						m_cImagePixelData = nullptr;
					}
					DebugPrint(2, "Cleanup - clear buffers' data pointers\n");
					if (m_backBuffer.data)
						m_backBuffer.data = nullptr;
					if (m_frontBuffer.data)
						m_frontBuffer.data = nullptr;
					DebugPrint(2, "Cleanup - clear bitmap data\n");
					if (m_bitmapData)
					{
						delete[] m_bitmapData;
						m_bitmapData = nullptr;
					}
					DebugPrint(2, "Cleanup - deallocate Cocoa view subclass object\n");
					if (m_cBlitView)
					{
						[m_cBlitView removeFromSuperview];
						internal_gw::G_OBJC_CALL_METHOD(GMacBLITView, m_cBlitView, cleanup);
						[m_cBlitView release];
						m_cBlitView = nullptr;
					}

					DebugPrint(2, "Cleanup - attempt to unlock self, back buffer, front buffer for write\n");
					if (+m_frontBuffer.UnlockSyncWrite()
						&& +m_backBuffer.UnlockSyncWrite()
						&& +UnlockSyncWrite())
						DebugPrint(2, "Cleanup - unlocked self, back buffer, front buffer for write\n");
					else
						DebugPrint(2, "Cleanup - unexpected result\n"); // should never reach here
				}
				else
				{
					DebugPrint(2, "Cleanup - unexpected result\n"); // should never reach here
				}
			}

#pragma endregion PRIVATE_FUNCTIONS

		public:

#pragma region CREATE_AND_DESTROY_FUNCTIONS

			~GRasterSurfaceImplementation()
			{
				DebugPrint(0, "\nGRasterSurface destructor\n");
				Cleanup();
			}

			GReturn Create(GW::SYSTEM::GWindow _gWindow)
			{
				DebugPrint(0, "\nCreate\n");
				
				// validate arguments
				DebugPrint(2, "Create - validate args\n");
				// ensure gwindow exists
				if (_gWindow == nullptr)
				{
					DebugPrint(2, "Create - m_gWindow was nullptr; return\n\n");
					return GW::GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "Create - args valid\n");

				// store arguments
				m_gWindow = _gWindow;
				DebugPrint(2, "Create - store args\n");

				// initialize buffers
				DebugPrint(2, "Create - initialize buffers\n");
				m_backBuffer.Create("back");
				m_frontBuffer.Create("front");

				// get universal window handle from GWindow
				DebugPrint(2, "Create - get universal window handle from gwindow\n");
				if (-m_gWindow.GetWindowHandle(m_gUniversalWindowHandle))
				{
					Cleanup();
					return GReturn::INVALID_ARGUMENT;
				}
				// get inner dimensions from window and resize bitmap to fit
				DebugPrint(2, "Create - get client dimensions of window\n");
				unsigned int newWidth, newHeight;
				if (-m_gWindow.GetClientWidth(newWidth) || -m_gWindow.GetClientHeight(newHeight))
				{
					Cleanup();
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "Create - resize bitmap to client dimensions of window\n");
				ResizeBitmap(newWidth, newHeight);

				// create GConcurrent objects (false = no callbacks)
				DebugPrint(2, "Create - create GConcurrent objects\n");
				m_gDrawThread.Create(false);
				m_gSmartUpdateRowProcessThread.Create(false);
				// create event listener with callback function for GWindow to call
				DebugPrint(2, "Create - create event listener\n");
				GReturn gr = m_gEventListener.Create([&](GW::GEvent _event)
				{
					GW::SYSTEM::GWindow::Events windowEvent;
					GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
					if (+_event.Read(windowEvent, windowEventData))
						switch (windowEvent)
						{
							case GW::SYSTEM::GWindow::Events::MINIMIZE:
							{
								DebugPrint(2, "Create callback lambda - MINIMIZE event received\n");
								// deactivate raster when minimized to prevent updating while window is not visible
								m_surfaceState = SurfaceState::INACTIVE;
							} break;
							case GW::SYSTEM::GWindow::Events::MAXIMIZE:
							{
								DebugPrint(2, "Create callback lambda - MAXIMIZE event received\n");
								// get client dimensions from window and resize bitmap to fit
								DebugPrint(2, "Create callback lambda - MAXIMIZE - resize bitmap\n");
								ResizeBitmap(windowEventData.clientWidth, windowEventData.clientHeight); // makes surface active
							} break;
							case GW::SYSTEM::GWindow::Events::RESIZE:
							{
								DebugPrint(2, "Create callback lambda - RESIZE event received\n");
								// get client dimensions from window and resize bitmap to fit
								DebugPrint(2, "Create callback lambda - RESIZE - resize bitmap\n");
								ResizeBitmap(windowEventData.clientWidth, windowEventData.clientHeight); // makes surface active
							} break;
							case GW::SYSTEM::GWindow::Events::DESTROY:
							{
								DebugPrint(2, "Create callback lambda - DESTROY event received\n");
								m_surfaceState = SurfaceState::INVALID;
								Cleanup();
							} break;
							default:
							{
								DebugPrint(2, "Create callback lambda - unknown event received\n");
							} break;
						} // end switch (windowEvent)
				});
				// register event listener with GWindow's event generator
				DebugPrint(2, "Create - register event listener\n");
				if (-m_gWindow.Register(m_gEventListener))
				{
					DebugPrint(2, "Create - registering event listener failed; return\n\n");
					Cleanup();
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "Create - successful; return\n\n");
				return gr;
			}

#pragma endregion CREATE_AND_DESTROY_FUNCTIONS
#pragma region UPDATE_FUNCTIONS

			GReturn Clear(Color _xrgbColor) override
			{
				DebugPrint(0, "\nClear\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "Clear - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				DebugPrint(2, "Clear - attempt to lock back buffer for write\n");
				if (+m_backBuffer.LockSyncWrite())
				{
					DebugPrint(2, "Clear - locked back buffer for write\n");
					// store bitmap dimensions locally, since they could change
					unsigned int bitmapWidth = m_bitmapWidth;
					unsigned int bitmapHeight = m_bitmapHeight;
					// fill back buffer with passed color
					DebugPrint(2, "Clear - fill back buffer with color\n");
					for (unsigned int x = 0; x < bitmapWidth; ++x) // manually clear first row
						m_backBuffer.data[x] = _xrgbColor;
					for (unsigned int y = 1; y < bitmapHeight; ++y) // copy cleared row to each other row (much faster than manually clearing every pixel of every row)
						memcpy(&m_backBuffer.data[y * bitmapWidth], &m_backBuffer.data[0], bitmapWidth * sizeof(Color));
					// unlock and return
					DebugPrint(2, "Clear - attempt to unlock back buffer for write\n");
					if (+m_backBuffer.UnlockSyncWrite())
					{
						DebugPrint(2, "Clear - unlocked back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
					DebugPrint(2, "Clear - unexpected result; return\n\n");
					return GReturn::UNEXPECTED_RESULT; // should never reach here
				}
				DebugPrint(2, "Clear - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn UpdateSurface(const Color* _xrgbPixels, unsigned int _numPixels) override
			{
				DebugPrint(0, "\nUpdateSurface\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UpdateSurface - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				DebugPrint(2, "UpdateSurface - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "UpdateSurface - _xrgbPixels was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count is nonzero
				if (_numPixels < 1)
				{
					DebugPrint(2, "UpdateSurface - _numPixels was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count does not exceed surface pixel count
				if (_numPixels > static_cast<unsigned int>(m_bitmapWidth * m_bitmapHeight))
				{
					DebugPrint(2, "UpdateSurface - _numPixels was > surface's pixel count; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "UpdateSurface - args valid\n");

				// lock self and back buffer to prevent read/write conflicts
				DebugPrint(2, "UpdateSurface - attempt to lock self, back buffer for write\n");
				if (+LockSyncWrite()
					&& +m_backBuffer.LockSyncWrite())
				{
					DebugPrint(2, "UpdateSurface - locked self, back buffer for write\n");
					// BLIT pixel data to back buffer
					DebugPrint(2, "UpdateSurface - BLIT input pixels to back buffer\n");
					memcpy(m_backBuffer.data, _xrgbPixels, _numPixels * sizeof(Color));
					// increment and store frame number in back buffer
					DebugPrint(2, "UpdateSurface - increment and store frame number in back buffer\n");
					m_backBuffer.frameNum = ++m_frameCount;
					// unlock and return
					DebugPrint(2, "UpdateSurface - unlock and return\n");
					if (+m_backBuffer.UnlockSyncWrite()
						&& +UnlockSyncWrite())
					{
						DebugPrint(2, "UpdateSurface - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "UpdateSurface - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn UpdateSurfaceSubset(const Color* _xrgbPixels, unsigned short _numRows, unsigned short _rowWidth, unsigned short _rowStride, int _destX, int _destY) override
			{
				DebugPrint(0, "\nUpdateSurfaceSubset\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UpdateSurfaceSubset - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// store dimensions locally, since they could potentially change during execution
				DebugPrint(2, "UpdateSurfaceSubset - store bitmap dimensions\n");
				unsigned int bitmapWidth = m_bitmapWidth;
				unsigned int bitmapHeight = m_bitmapHeight;
				// validate arguments
				DebugPrint(2, "UpdateSurfaceSubset - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _xrgbPixels was nullpt; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row count is nonzero
				if (_numRows < 1)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _numRows was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row width is nonzero
				if (_rowWidth < 1)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _numRows was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row stride is >= row width, if provided
				if (_rowStride > 0 && _rowStride < _rowWidth)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _rowStride was < _rowWidth; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "UpdateSurfaceSubset - args valid\n");

				// return immediately if subset is completely outside surface
				if (   _destX >= static_cast<int>(bitmapWidth)
					|| _destX + _rowWidth < 0
					|| _destY >= static_cast<int>(bitmapHeight)
					|| _destY + _numRows < 0)
				{
					DebugPrint(2, "UpdateSurfaceSubset - subset was outside surface; return\n\n");
					return GReturn::REDUNDANT;
				}

				// get row stride
				DebugPrint(2, "UpdateSurfaceSubset - get row stride\n");
				if (_rowStride == 0) _rowStride = _rowWidth;
				// clip subset to edges of surface
				//   left side
				DebugPrint(2, "UpdateSurfaceSubset - clip left side\n");
				if (_destX < 0)
				{
					// move starting location in source data right and shorten rows
					_xrgbPixels += -_destX;
					_rowWidth += _destX; // add, because coord is negative
					// clamp coord to surface edge
					_destX = 0;
				}
				//   right side
				DebugPrint(2, "UpdateSurfaceSubset - clip right side\n");
				if (_destX + _rowWidth > bitmapWidth)
				{
					// shorten rows
					_rowWidth -= (_destX + _rowWidth - bitmapWidth);
				}
				//   top side
				DebugPrint(2, "UpdateSurfaceSubset - clip top side\n");
				if (_destY < 0)
				{
					// move starting location in source data down and reduce row count
					_xrgbPixels += -_destY * _rowStride;
					_numRows += _destY; // add, because coord is negative
					// clamp coord to surface edge
					_destY = 0;
				}
				//   bottom side
				DebugPrint(2, "UpdateSurfaceSubset - clip bottom side\n");
				if (_destY + _numRows > bitmapHeight)
				{
					// reduce row count
					_numRows -= (_destY + _numRows - bitmapHeight);
				}
				// calculate starting index from destination x/y coords
				DebugPrint(2, "UpdateSurfaceSubset - calculate starting index\n");
				unsigned int startIndex = _destX + (_destY * bitmapWidth);

				// lock back buffer to prevent read/write conflicts
				DebugPrint(2, "UpdateSurfaceSubset - attempt to lock self, back buffer for write\n");
				if (+LockSyncWrite()
					&& +m_backBuffer.LockSyncWrite())
				{
					DebugPrint(2, "UpdateSurfaceSubset - locked self, back buffer for write\n");
					// calculate end index
					DebugPrint(2, "UpdateSurfaceSubset - calculate end index\n");
					unsigned int endIndex = startIndex + (bitmapWidth * (_numRows - 1)) + (_rowWidth - 1);
					// BLIT rows of subset block to back buffer
					DebugPrint(2, "UpdateSurfaceSubset - BLIT subset to back buffer\n");
					for (; startIndex < endIndex; startIndex += bitmapWidth, _xrgbPixels += _rowStride)
						memcpy(&m_backBuffer.data[startIndex], _xrgbPixels, _rowWidth * sizeof(Color));
					// increment and store frame count in back buffer
					DebugPrint(2, "UpdateSurfaceSubset - increment and store frame number in back buffer\n");
					m_backBuffer.frameNum = ++m_frameCount;
					// unlock and return
					DebugPrint(2, "UpdateSurfaceSubset - unlock and return\n");
					if (+m_backBuffer.UnlockSyncWrite()
						&& +UnlockSyncWrite())
					{
						DebugPrint(2, "UpdateSurfaceSubset - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "UpdateSurfaceSubset - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn SmartUpdateSurface(const Color* _xrgbPixels, unsigned int _numPixels, unsigned short _rowWidth, unsigned int _drawOptionFlags) override
			{
				DebugPrint(0, "\nSmartUpdateSurface\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "SmartUpdateSurface - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				DebugPrint(2, "SmartUpdateSurface - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "SmartUpdateSurface - _xrgbPixels was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count is nonzero
				if (_numPixels < 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _numPixels was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row width is nonzero
				if (_rowWidth < 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _rowWidth was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// separate flags into sections and test each section
				const unsigned int bitmaskAlignX =
					  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_LEFT
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_RIGHT;
				const unsigned int bitmaskAlignY =
					  GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_TOP
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_BOTTOM;
				const unsigned int bitmaskUpscale =
					  GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_2X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_3X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_4X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_8X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_16X
					| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT;
				const unsigned int bitmaskInterpolate =
					  GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST
					| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_BILINEAR;
				unsigned int testFlags = 0;
				// validate x alignment flags
				testFlags = _drawOptionFlags & bitmaskAlignX;
				if (( IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_LEFT)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_CENTER)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_RIGHT)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting x alignment flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate y alignment flags
				testFlags = _drawOptionFlags & bitmaskAlignY;
				if (( IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_TOP)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_CENTER)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_BOTTOM)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting y alignment flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate upscaling flags
				testFlags = _drawOptionFlags & bitmaskUpscale;
				if ((  IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_2X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_3X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_4X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_8X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_16X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_STRETCH_TO_FIT)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting upscaling flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate interpolation flags
				testFlags = _drawOptionFlags & bitmaskInterpolate;
				if (( IsolateBit(testFlags, UpdateFlagBitPosition::BIT_INTERPOLATE_NEAREST)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_INTERPOLATE_BILINEAR)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting interpolation flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "SmartUpdateSurface - args valid\n");
				// if arguments are valid, continue

				// store surface dimensions and border color locally to avoid needing to read data more than once
				unsigned int bitmapWidth = m_bitmapWidth;
				unsigned int bitmapHeight = m_bitmapHeight;

				// create struct to store data to use during processing
				SmartUpdateData data = {};
				// store color array and metadata
				data.inputColors = _xrgbPixels;
				data.bitmapWidth = bitmapWidth;
				data.bitmapHeight = bitmapHeight;
				data.rawDataWidth = _rowWidth;
				data.rawDataHeight = _numPixels / _rowWidth;
				// determine data dimensions after processing
				DebugPrint(2, "SmartUpdateSurface - calculate upscaled dimensions\n");
				testFlags = _drawOptionFlags & bitmaskUpscale;
				switch (testFlags)
				{
					case GW::GRAPHICS::UPSCALE_2X:
						data.upscaledDataWidth = data.rawDataWidth << 1;
						data.upscaledDataHeight = data.rawDataHeight << 1;
						break;
					case GW::GRAPHICS::UPSCALE_3X:
						data.upscaledDataWidth = data.rawDataWidth * 3;
						data.upscaledDataHeight = data.rawDataHeight * 3;
						break;
					case GW::GRAPHICS::UPSCALE_4X:
						data.upscaledDataWidth = data.rawDataWidth << 2;
						data.upscaledDataHeight = data.rawDataHeight << 2;
						break;
					case GW::GRAPHICS::UPSCALE_8X:
						data.upscaledDataWidth = data.rawDataWidth << 3;
						data.upscaledDataHeight = data.rawDataHeight << 3;
						break;
					case GW::GRAPHICS::UPSCALE_16X:
						data.upscaledDataWidth = data.rawDataWidth << 4;
						data.upscaledDataHeight = data.rawDataHeight << 4;
						break;
					case GW::GRAPHICS::STRETCH_TO_FIT:
						data.upscaledDataWidth = bitmapWidth;
						data.upscaledDataHeight = bitmapHeight;
						break;
					default:
						data.upscaledDataWidth = data.rawDataWidth;
						data.upscaledDataHeight = data.rawDataHeight;
						break;
				}

				// calculate pixel coordinate scaling ratios
				DebugPrint(2, "SmartUpdateSurface - calculate coordinate scaling ratios\n");
				data.coordScaleRatioX = data.rawDataWidth / static_cast<float>(data.upscaledDataWidth);
				data.coordScaleRatioY = data.rawDataHeight / static_cast<float>(data.upscaledDataHeight);

				// determine X alignment
				DebugPrint(2, "SmartUpdateSurface - determine x alignment\n");
				testFlags = _drawOptionFlags & bitmaskAlignX;
				switch (testFlags)
				{
					case GW::GRAPHICS::ALIGN_X_LEFT:
						data.offsetX = 0;
						break;
					case GW::GRAPHICS::ALIGN_X_RIGHT:
						data.offsetX = static_cast<int>(bitmapWidth) - static_cast<int>(data.upscaledDataWidth);
						break;
					case GW::GRAPHICS::ALIGN_X_CENTER:
					default:
						data.offsetX = (static_cast<int>(bitmapWidth) - static_cast<int>(data.upscaledDataWidth)) >> 1;
						break;
				}

				// determine Y alignment
				DebugPrint(2, "SmartUpdateSurface - determine y alignment\n");
				testFlags = _drawOptionFlags & bitmaskAlignY;
				switch (testFlags)
				{
					case GW::GRAPHICS::ALIGN_Y_TOP:
						data.offsetY = 0;
						break;
					case GW::GRAPHICS::ALIGN_Y_BOTTOM:
						data.offsetY = static_cast<int>(bitmapHeight) - static_cast<int>(data.upscaledDataHeight);
						break;
					case GW::GRAPHICS::ALIGN_Y_CENTER:
					default:
						data.offsetY = (static_cast<int>(bitmapHeight) - static_cast<int>(data.upscaledDataHeight)) >> 1;
						break;
				}

				// lock self and back buffer to prevent read/write conflicts
				DebugPrint(2, "SmartUpdateSurface - attempt to lock self, back buffer for write\n");
				if (+LockSyncWrite()
					&& +m_backBuffer.LockSyncWrite())
				{
					DebugPrint(2, "SmartUpdateSurface - locked back buffer for write\n");
#if !defined(GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY)
					// calculate how many rows of the surface fit into 256 KB
					DebugPrint(2, "SmartUpdateSurface - calculate rows per thread\n");
					unsigned int rows = 262144 / (bitmapWidth * sizeof(Color));
					if (rows == 0) rows = 1; // min of 1
#endif

					// iterate through surface and process pixels
					if ((_drawOptionFlags & bitmaskInterpolate) == GW::GRAPHICS::INTERPOLATE_BILINEAR)
					{
#if defined(GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY)
						DebugPrint(2, "SmartUpdateSurface - process rows w/ bilinear interp (SERIAL)\n");
						for (int y = 0; y < bitmapHeight; ++y)
							ProcessPixelRowBilinear(static_cast<const void*>(nullptr), &m_backBuffer.data[y * bitmapWidth], y, reinterpret_cast<const void*>(&data));
#else
						DebugPrint(2, "SmartUpdateSurface - process rows w/ bilinear interp (PARALLEL)\n");
						m_gSmartUpdateRowProcessThread.BranchParallel(ProcessPixelRowBilinear, rows, bitmapHeight, reinterpret_cast<const void*>(&data), 0,
							static_cast<const void*>(nullptr), static_cast<int>(bitmapWidth * sizeof(Color)), m_backBuffer.data);
#endif
					}
					else
					{
#if defined(GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY)
						DebugPrint(2, "SmartUpdateSurface - process rows w/ nearest interp (SERIAL)\n");
						for (int y = 0; y < bitmapHeight; ++y)
							ProcessPixelRowNearest(static_cast<const void*>(nullptr), &m_backBuffer.data[y * bitmapWidth], y, reinterpret_cast<const void*>(&data));
#else
						DebugPrint(2, "SmartUpdateSurface - process rows w/ nearest interp (PARALLEL)\n");
						m_gSmartUpdateRowProcessThread.BranchParallel(ProcessPixelRowNearest, rows, bitmapHeight, reinterpret_cast<const void*>(&data), 0,
							static_cast<const void*>(nullptr), static_cast<int>(bitmapWidth * sizeof(Color)), m_backBuffer.data);
#endif
					}

#if !defined(GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY)
					// wait for data to finish processing
					DebugPrint(2, "SmartUpdateSurface - converge row processing threads\n");
					GReturn convergeResult = m_gSmartUpdateRowProcessThread.Converge(0);
					if (G_FAIL(convergeResult))
					{
						DebugPrint(2, "SmartUpdateSurface - converge failed, attempt to unlock back buffer for write\n");
						if (+m_backBuffer.UnlockSyncWrite())
						{
							DebugPrint(2, "SmartUpdateSurface - unlocked back buffer for write; return\n\n");
							return convergeResult;
						}
					}
#endif

					//	// increment and store frame count in buffer
					DebugPrint(2, "SmartUpdateSurface - increment and store frame number in back buffer\n");
					m_backBuffer.frameNum = ++m_frameCount;
					// unlock and return
					DebugPrint(2, "SmartUpdateSurface - unlock and return\n");
					if (+m_backBuffer.UnlockSyncWrite()
						&& +UnlockSyncWrite())
					{
						DebugPrint(2, "SmartUpdateSurface - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "SmartUpdateSurface - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

#pragma endregion UPDATE_FUNCTIONS
#pragma region LOCK_AND_UNLOCK_FUNCTIONS

			GReturn LockUpdateBufferWrite(Color** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override
			{
				DebugPrint(0, "\nLockUpdateBufferWrite\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "LockUpdateBufferWrite - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				if (_outMemoryBuffer == nullptr)
				{
					DebugPrint(2, "LockUpdateBufferWrite - _outMemoryBuffer was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// if args are valid, continue

				// reject lock if already locked for writing
				DebugPrint(2, "LockUpdateBufferWrite - check number of active write locks\n");
				if (m_backBuffer.numLocks > 0)
				{
					DebugPrint(2, "LockUpdateBufferWrite - write lock already active; return\n\n");
					return GReturn::FAILURE;
				}
				// otherwise, try to lock for writing
				DebugPrint(2, "LockUpdateBufferWrite - attempt to lock back buffer for write\n");
				if (+m_backBuffer.LockSyncWrite())
				{
					DebugPrint(2, "LockUpdateBufferWrite - locked back buffer for write\n");
					++m_backBuffer.numLocks;
					DebugPrint(2, "LockUpdateBufferWrite - %d write locks\n", m_backBuffer.numLocks);
					// give buffer data pointer and dimensions to caller and return
					DebugPrint(2, "LockUpdateBufferWrite - set return values\n");
					*_outMemoryBuffer = m_backBuffer.data;
					_outWidth = m_bitmapWidth;
					_outHeight = m_bitmapHeight;
					DebugPrint(2, "LockUpdateBufferWrite - successful; return\n\n");
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "LockUpdateBufferWrite - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn LockUpdateBufferRead(const Color** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override
			{
				DebugPrint(0, "\nLockUpdateBufferRead\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "LockUpdateBufferRead - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				if (_outMemoryBuffer == nullptr)
				{
					DebugPrint(2, "LockUpdateBufferWrite - _outMemoryBuffer was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// if args are valid, continue

				// reject lock if already locked for reading
				DebugPrint(2, "LockUpdateBufferRead - check number of active read locks\n");
				if (m_frontBuffer.numLocks > 0)
				{
					DebugPrint(2, "LockUpdateBufferRead - read lock already active; return\n\n");
					return GReturn::FAILURE;
				}
				// otherwise, try to lock for reading
				DebugPrint(2, "LockUpdateBufferRead - attempt to lock front buffer for read\n");
				if (+m_frontBuffer.LockAsyncRead())
				{
					DebugPrint(2, "LockUpdateBufferRead - locked front buffer for read\n");
					++m_frontBuffer.numLocks;
					DebugPrint(2, "LockUpdateBufferRead - %d read locks\n", m_frontBuffer.numLocks);
					// give buffer data pointer and dimensions to caller and return
					DebugPrint(2, "LockUpdateBufferRead - set return values\n");
					*_outMemoryBuffer = reinterpret_cast<const Color*>(m_frontBuffer.data);
					_outWidth = m_bitmapWidth;
					_outHeight = m_bitmapHeight;
					DebugPrint(2, "LockUpdateBufferRead - successful; return\n\n");
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "LockUpdateBufferRead - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			GReturn UnlockUpdateBufferWrite() override
			{
				DebugPrint(0, "\nUnlockUpdateBufferWrite\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// ignore unlock if not locked
				if (m_backBuffer.numLocks == 0)
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - redundant; return\n\n");
					return GReturn::REDUNDANT;
				}

				--m_backBuffer.numLocks;
				DebugPrint(2, "UnlockUpdateBufferWrite - %d write locks\n", m_backBuffer.numLocks);

				// otherwise, update frame number in buffer and unlock
				DebugPrint(2, "UnlockUpdateBufferWrite - attempt to lock self for write\n");
				if (+LockSyncWrite())
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - locked self for write\n");
					// increment and store frame count in buffer
					DebugPrint(2, "UnlockUpdateBufferWrite - increment and store frame number in back buffer\n");
					m_backBuffer.frameNum = ++m_frameCount;
					// unlock and return
					DebugPrint(2, "UnlockUpdateBufferWrite - attempt to unlock self, back buffer for write\n");
					if (+UnlockSyncWrite()
						&& +m_backBuffer.UnlockSyncWrite())
					{
						DebugPrint(2, "UnlockUpdateBufferWrite - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
					DebugPrint(2, "UnlockUpdateBufferWrite - unexpected result; return\n\n");
					return GReturn::UNEXPECTED_RESULT; // should never reach here
				}
				DebugPrint(2, "UnlockUpdateBufferWrite - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn UnlockUpdateBufferRead() override
			{
				DebugPrint(0, "\nUnlockUpdateBufferRead\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UnlockUpdateBufferRead - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// ignore unlock if not locked
				if (m_frontBuffer.numLocks < 1)
				{
					DebugPrint(2, "UnlockUpdateBufferRead - redundant; return\n\n");
					return GReturn::REDUNDANT;
				}
				// otherwise, unlock
				--m_frontBuffer.numLocks;
				DebugPrint(2, "UnlockUpdateBufferRead - %d read locks\n", m_frontBuffer.numLocks);
				DebugPrint(2, "UnlockUpdateBufferRead - attempt to unlock front buffer for read\n");
				if (+m_frontBuffer.UnlockAsyncRead())
				{
					DebugPrint(2, "UnlockUpdateBufferRead - unlocked front buffer for read\n");
					DebugPrint(2, "UnlockUpdateBufferRead - successful; return\n\n", m_frontBuffer.numLocks);
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "UnlockUpdateBufferRead - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

#pragma endregion LOCK_AND_UNLOCK_FUNCTIONS

			GReturn Present() override
			{
				DebugPrint(0, "\nPresent\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "Present - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				DebugPrint(2, "Present - draw\n");
				if (-SwapBuffersAndDraw())
				{
					DebugPrint(2, "Present - draw failed; return\n");
					return GReturn::FAILURE;
				}
				DebugPrint(2, "Present - successful; return\n");
				return GReturn::SUCCESS;
			}

		}; // end class GRasterSurfaceImplementation
	} // end namespace I
} // end namespace GW


namespace internal_gw
{
	// GMacBLITView Implementation

	G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(GMacBLITView);

	// Constructor
	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, id, initUsingFrame, NSRect _frameRect)
	{
		//NSLog(@"initUsingFrame: %@", self);
		self = [self initWithFrame:_frameRect];

		if (self)
		{

			// Retrieve the data members of this instance.
			G_OBJC_DATA_MEMBERS_TYPE(GMacBLITView)& selfDM = G_OBJC_GET_DATA_MEMBERS(GMacBLITView, self);

			selfDM.m_colorspace = CGColorSpaceCreateDeviceRGB();
			selfDM.m_bitsPerComponent = 8;
			selfDM.m_width = _frameRect.size.width;
			selfDM.m_height = _frameRect.size.height;
			selfDM.m_bytesPerRow = selfDM.m_width * sizeof(unsigned int);

			selfDM.m_data = static_cast<unsigned int*>(calloc(selfDM.m_width * selfDM.m_height, sizeof(unsigned int)));

			// kCGImageAlphaNoneSkipFirst is ARGB format but it skips the alpha channel.
			CGContextRef bitmapContext = CGBitmapContextCreate(selfDM.m_data,
															   static_cast<CGFloat>(selfDM.m_width),
															   static_cast<CGFloat>(selfDM.m_height),
															   selfDM.m_bitsPerComponent,
															   selfDM.m_bytesPerRow,
															   selfDM.m_colorspace,
															   kCGImageAlphaNoneSkipFirst);

			selfDM.m_image = CGBitmapContextCreateImage(bitmapContext);
			CGContextRelease(bitmapContext);
		}

		return self;
	}

	// Updates the view. Not to be called directly. Must use [self setNeedsDisplay:YES] to trigger call.
	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, void, drawRect, CGRect _rect)
	{
		G_OBJC_DATA_MEMBERS_TYPE(GMacBLITView)& selfDM = G_OBJC_GET_DATA_MEMBERS(GMacBLITView, self);

		// CGContext isn't available prior to calling drawRect
        // This code was previously attempting to cache the CGContext as a member
        // this was incorrect, the correct/valid context is always placed
        // on the stack during this call and should be used directly!
		selfDM.m_context = [[NSGraphicsContext currentContext] CGContext]; // force update

		if (selfDM.m_context && selfDM.m_image)
		{
			CGContextDrawImage(selfDM.m_context, _rect, selfDM.m_image);
			CGImageRelease(selfDM.m_image);
			selfDM.m_image = nil;
		}
	}

	// This override ensures the coordinate system origin is always in the upper-left corner.
	G_OBJC_HEADER_INSTANCE_METHOD(GMacBLITView, BOOL, isFlipped)
	{
		return NO;
	}

	// Frees dynamic memory used by the object.
	G_OBJC_HEADER_INSTANCE_METHOD(GMacBLITView, void, cleanup)
	{
		G_OBJC_DATA_MEMBERS_TYPE(GMacBLITView)& selfDM = G_OBJC_GET_DATA_MEMBERS(GMacBLITView, self);

		free(selfDM.m_data);
		selfDM.m_data = nullptr;

		if (selfDM.m_image)
		{
			CGImageRelease(selfDM.m_image);
			selfDM.m_image = nil;
		}
	}

	// Creates image to be displayed from given pixels.
	G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacBLITView, void, update, unsigned int* _pixels, unsigned int _pixelCount)
	{
		G_OBJC_DATA_MEMBERS_TYPE(GMacBLITView)& selfDM = G_OBJC_GET_DATA_MEMBERS(GMacBLITView, self);

		if (selfDM.m_image) // If this is true, then there is a frame that hasn't been drawn yet.
			return; // Rather than keep overriding frames, we drop them until the last one has been drawn.

		// Copy pixels into the raster data
		memcpy(selfDM.m_data, _pixels, _pixelCount * sizeof(unsigned int));

		// bitmapContext and image are immutable, so they need to be recreated every time.
		CGContextRef bitmapContext = CGBitmapContextCreate(selfDM.m_data,
														   static_cast<CGFloat>(selfDM.m_width),
														   static_cast<CGFloat>(selfDM.m_height),
														   selfDM.m_bitsPerComponent,
														   selfDM.m_bytesPerRow,
														   selfDM.m_colorspace,
														   kCGImageAlphaNoneSkipFirst);

		selfDM.m_image = CGBitmapContextCreateImage(bitmapContext);
		CGContextRelease(bitmapContext);

		// Notify system to redraw view and call drawRect. This must be done on the main thread.
		dispatch_async(dispatch_get_main_queue(), ^ {
			[self setNeedsDisplay:YES];
		});
	}

	// GMacBLITView Implementation End
}


#if defined(GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY)
#undef GRASTERSURFACE_DEBUG_MAC_SERIALIZE_SMARTUPDATE_COPY
#endif
#if defined(GRASTERSURFACE_DEBUG_MAC_DEBUG_ASSERTS)
#undef GRASTERSURFACE_DEBUG_MAC_DEBUG_ASSERTS
#endif
#if defined(GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINTS)
#undef GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINTS
#endif
#if defined(GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINT_PRIORITY_LEVEL)
#undef GRASTERSURFACE_DEBUG_MAC_DEBUG_PRINT_PRIORITY_LEVEL
#endif
