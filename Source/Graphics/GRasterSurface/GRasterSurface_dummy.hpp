namespace GW
{
	namespace I
	{
		class GRasterSurfaceImplementation : public virtual GRasterSurfaceInterface,
			protected GEventResponderImplementation
		{
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow) { return GReturn::INTERFACE_UNSUPPORTED; }
			GReturn Clear(unsigned int _xrgbColor) override { return GReturn::FAILURE; }
			GReturn UpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels) override { return GReturn::FAILURE; }
			GReturn UpdateSurfaceSubset(const unsigned int* _xrgbPixels, unsigned short _numRows, unsigned short _rowWidth, unsigned short _rowStride, int _destX, int _destY) override { return GReturn::FAILURE; }
			GReturn SmartUpdateSurface(const unsigned int* _xrgbPixels, unsigned int _numPixels, unsigned short _rowWidth, unsigned int _drawOptionFlags) override { return GReturn::FAILURE; }
			GReturn LockUpdateBufferWrite(unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override { return GReturn::FAILURE; }
			GReturn LockUpdateBufferRead(const unsigned int** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override { return GReturn::FAILURE; }
			GReturn UnlockUpdateBufferWrite() override { return GReturn::FAILURE; }
			GReturn UnlockUpdateBufferRead() override { return GReturn::FAILURE; }
			GReturn Present() override { return GReturn::FAILURE; }
		}; // end class GRasterSurfaceImplementation
	} // end namespace I
} // end namespace GW
