namespace GW {
	namespace I {

		class GBlitterImplementation : public virtual GBlitterInterface
		{
		public:
			GReturn Create(unsigned short _width, unsigned short _height) { return GReturn::INTERFACE_UNSUPPORTED; }
			GReturn LoadSource(const char* _colorFilepath, const char* _layersFilepath, const char* _stencilsFilepath, unsigned short& _outIndex) override { return GReturn::FAILURE; }
			GReturn ImportSource(
				const unsigned int* _colors, const unsigned char* _layers, const unsigned char* _stencils,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				unsigned short& _outIndex) override
			{
				return GReturn::FAILURE;
			}
			GReturn ImportSourceComplex(
				const unsigned int* _colors, unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				const unsigned char* _layers, unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				const unsigned char* _stencils, unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height, unsigned short& _outIndex) override
			{
				return GReturn::FAILURE;
			}
			GReturn DefineTiles(const TileDefinition* _tileDefinitions, const unsigned int _numTiles, unsigned int* _outIndices) override { return GReturn::FAILURE; }
			GReturn SetTileMaskValues(unsigned int* _tileIndices, unsigned int _numTiles, unsigned int _maskColor, unsigned char _maskLayer, unsigned char _maskStencil) override { return GReturn::FAILURE; }
			GReturn DiscardSources(unsigned short* _sourceIndices, unsigned short _numSources) override { return GReturn::FAILURE; }
			GReturn DiscardTiles(unsigned int* _tileIndices, unsigned int _numTiles) override { return GReturn::FAILURE; }
			GReturn Clear(unsigned int _color, unsigned char _layer, unsigned char _stencil) override { return GReturn::FAILURE; }
			GReturn ClearColor(unsigned int _color) override { return GReturn::FAILURE; }
			GReturn ClearLayer(unsigned char _layer) override { return GReturn::FAILURE; }
			GReturn ClearStencil(unsigned char _stencil) override { return GReturn::FAILURE; }
			GReturn DrawDeferred(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) override { return GReturn::FAILURE; }
			GReturn DrawImmediate(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) override { return GReturn::FAILURE; }
			GReturn Flush() override { return GReturn::FAILURE; }
			GReturn ExportResult(
				bool _flush,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) override
			{
				return GReturn::FAILURE;
			}
			GReturn ExportResultComplex(
				bool _flush,
				unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height,
				unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) override
			{
				return GReturn::FAILURE;
			}
		};

	} // end I namespace
} // end GW namespace