#include "../../Shared/gtl/ld_vector.h"
#include "../../Shared/gtl/factory.h"
#include "../../../Interface/System/GConcurrent.h"
#include "../../../Interface/System/GFile.h"
#include "../../../Interface/Math/GMathDefines.h"
#include "../../../Interface/Math/GMatrix.h"

#include <cctype> // included for tolower function
#include <vector>
#include <limits>
#include <cmath>


//#define GBLITTER_DEBUG_VERBOSE_NEW							// uncomment this line to override the "new" keyword with a macro that stores additional data to track down memory leaks more easily (WIN32 Debug only)
//#define GBLITTER_DEBUG_SERIAL_FLUSH							// uncomment this line to turn off multithreaded flushing to make debugging easier
//#define GBLITTER_DEBUG_OUTLINE_TRANFORMED_BOUNDS				// uncomment this line to make transformed instructions outline their bounds with a solid color wherever a pixel is not drawn
//#define GBLITTER_DEBUG_FILL_TRANFORMED_BOUNDS					// uncomment this line to make transformed instructions fill their bounds with a solid color wherever a pixel is not drawn (overrides outlining bounds)
//#define GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES				// uncomment this line to make transformed instructions fill the triangles they are divided into with a solid color (color lookup drawing will be done over top of solid color)
//#define GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP		// uncomment this line to make transformed instructions replace color lookup with filling the triangles they are divided into with a solid color (overrides filling triangles)
//#define GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS		// uncomment this line to make transformed instructions draw the corners of the triangles they are divided into with a solid color



#if defined(_WIN32) && !defined(NDEBUG) && defined(GBLITTER_DEBUG_VERBOSE_NEW)
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif


namespace GW
{
	namespace I
	{
		class GBlitterImplementation : public virtual GBlitterInterface
		{

#pragma region DEFINES

			typedef unsigned char				Byte;
			typedef unsigned int				Color; // RGBA quadruple
			typedef unsigned char				ColorChannel; // single value from a Color (R, G, B, or A)
			typedef unsigned char				Layer;
			typedef unsigned char				Stencil;

			// Masks for bits within a byte (MSB to LSB from left to right)
			static constexpr Byte				BIT_0									= 0b00000001u;
			static constexpr Byte				BIT_1									= 0b00000010u;
			static constexpr Byte				BIT_2									= 0b00000100u;
			static constexpr Byte				BIT_3									= 0b00001000u;
			static constexpr Byte				BIT_4									= 0b00010000u;
			static constexpr Byte				BIT_5									= 0b00100000u;
			static constexpr Byte				BIT_6									= 0b01000000u;
			static constexpr Byte				BIT_7									= 0b10000000u;

			// Masks for bytes within a multi-byte value (MSB to LSB from left to right)
			static constexpr unsigned short		BYTE_0									= 0x00ffu;
			static constexpr unsigned short		BYTE_1									= 0xff00u;
			static constexpr unsigned int		BYTE_2									= 0x00ff0000u;
			static constexpr unsigned int		BYTE_3									= 0xff000000u;
			static constexpr unsigned long long	BYTE_4									= 0x000000ff00000000ul;
			static constexpr unsigned long long	BYTE_5									= 0x0000ff0000000000ul;
			static constexpr unsigned long long	BYTE_6									= 0x00ff000000000000ul;
			static constexpr unsigned long long	BYTE_7									= 0xff00000000000000ul;

			// Minimum and maximum possible values in a color channel
			static constexpr ColorChannel		COLOR_CHANNEL_MIN						= (std::numeric_limits<ColorChannel>::min)();
			static constexpr ColorChannel		COLOR_CHANNEL_MAX						= (std::numeric_limits<ColorChannel>::max)();

			// Shorthands for relevant color channels in various formats (first channel to last channel from left to right)
			static constexpr Color				CHANNEL_0_MASK							= BYTE_3;
			static constexpr Color				CHANNEL_1_MASK							= BYTE_2;
			static constexpr Color				CHANNEL_2_MASK							= BYTE_1;
			static constexpr Color				CHANNEL_3_MASK							= BYTE_0;

			static constexpr Color				CHANNEL_MASK_LUMINANCE_L				= CHANNEL_0_MASK;

			static constexpr Color				CHANNEL_MASK_LUMINANCE_LA				= CHANNEL_0_MASK;
			static constexpr Color				CHANNEL_MASK_ALPHA_LA					= CHANNEL_1_MASK;

			static constexpr Color				CHANNEL_MASK_LUMINANCE_AL				= CHANNEL_1_MASK;
			static constexpr Color				CHANNEL_MASK_ALPHA_AL					= CHANNEL_0_MASK;

			static constexpr Color				CHANNEL_MASK_RED_RGBA					= CHANNEL_0_MASK;
			static constexpr Color				CHANNEL_MASK_GREEN_RGBA					= CHANNEL_1_MASK;
			static constexpr Color				CHANNEL_MASK_BLUE_RGBA					= CHANNEL_2_MASK;
			static constexpr Color				CHANNEL_MASK_ALPHA_RGBA					= CHANNEL_3_MASK;

			static constexpr Color				CHANNEL_MASK_RED_ARGB					= CHANNEL_1_MASK;
			static constexpr Color				CHANNEL_MASK_GREEN_ARGB					= CHANNEL_2_MASK;
			static constexpr Color				CHANNEL_MASK_BLUE_ARGB					= CHANNEL_3_MASK;
			static constexpr Color				CHANNEL_MASK_ALPHA_ARGB					= CHANNEL_0_MASK;

			// Byte offsets of color channels within a pixel in memory (values are stored in little endian order)
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_L	= 0;

			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_LA	= 1;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_LA		= 0;

			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL	= 0;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_AL		= 1;

			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB		= 2;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_GREEN_RGB		= 1;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_BLUE_RGB		= 0;

			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_RED_RGBA		= 3;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_GREEN_RGBA	= 2;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_BLUE_RGBA		= 1;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_RGBA	= 0;

			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_RED_ARGB		= 2;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_GREEN_ARGB	= 1;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_BLUE_ARGB		= 0;
			static constexpr unsigned short		LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_ARGB	= 3;

			// Relevant values for TGA files
			static constexpr Byte				TGA_VERTICAL_ORIGIN_TOP					= BIT_5;
			static constexpr Byte				TGA_HORIZONTAL_ORIGIN_RIGHT				= BIT_4;

			// Size of a pixel from a source file in bytes, by format
			static constexpr unsigned short		SOURCE_FILE_PIXEL_SIZE_BYTES_L_8		= 1u;
			static constexpr unsigned short		SOURCE_FILE_PIXEL_SIZE_BYTES_AL_16		= 2u;
			static constexpr unsigned short		SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24		= 3u;
			static constexpr unsigned short		SOURCE_FILE_PIXEL_SIZE_BYTES_ARGB_32	= 4u;
			static constexpr unsigned short		SOURCE_FILE_PIXEL_SIZE_BYTES_LUT_8		= 1u;

			// Size of a blitter internal pixel in bytes, by format
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_C			= sizeof(Color);
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_L			= sizeof(Layer);
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_S			= sizeof(Stencil);
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_CL		= INTERNAL_PIXEL_SIZE_BYTES_C + INTERNAL_PIXEL_SIZE_BYTES_L;
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_CS		= INTERNAL_PIXEL_SIZE_BYTES_C + INTERNAL_PIXEL_SIZE_BYTES_S;
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_LS		= INTERNAL_PIXEL_SIZE_BYTES_L + INTERNAL_PIXEL_SIZE_BYTES_S;
			static constexpr unsigned short		INTERNAL_PIXEL_SIZE_BYTES_CLS		= INTERNAL_PIXEL_SIZE_BYTES_C + INTERNAL_PIXEL_SIZE_BYTES_L + INTERNAL_PIXEL_SIZE_BYTES_S;

			// Maximum number of sources that can be imported
			static constexpr unsigned short		MAX_SOURCES							= (std::numeric_limits<unsigned short>::max)();

			// Maximum number of tiles that can be defined
			static constexpr unsigned int		MAX_TILES							= (std::numeric_limits<unsigned int>::max)();

			// Maximum dimensions of a source, in pixels
			static constexpr unsigned short		MAX_SOURCE_WIDTH					= (std::numeric_limits<unsigned short>::max)();
			static constexpr unsigned short		MAX_SOURCE_HEIGHT					= (std::numeric_limits<unsigned short>::max)();

			// Size of a pixel in the result canvas, in bytes
			static constexpr unsigned short		RESULT_PIXEL_SIZE_BYTES				= INTERNAL_PIXEL_SIZE_BYTES_CLS;

			// Size of a source or result block, in bytes (256 KB, 1024 B per KB)
			static constexpr unsigned int		DATA_BLOCK_SIZE_BYTES				= 256 * 1024;

			// Dimensions of source blocks
			static constexpr unsigned short		SOURCE_BLOCK_WIDTH					= 256;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_C				= 256;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_L				= 1024;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_S				= 1024;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_CL				= 204;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_CS				= 204;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_LS				= 512;
			static constexpr unsigned short		SOURCE_BLOCK_HEIGHT_CLS				= 170;

			// Dimensions of result blocks
			static constexpr unsigned short		RESULT_BLOCK_WIDTH					= 128;
			static constexpr unsigned short		RESULT_BLOCK_HEIGHT					= RESULT_BLOCK_WIDTH;

			// Size of a result block's canvas, in pixels
			static constexpr unsigned short		RESULT_BLOCK_CANVAS_SIZE_PIXELS		= RESULT_BLOCK_WIDTH * RESULT_BLOCK_HEIGHT;

			// Size of result block's canvas, in bytes
			static constexpr unsigned int		RESULT_BLOCK_CANVAS_SIZE_BYTES		= RESULT_BLOCK_CANVAS_SIZE_PIXELS * RESULT_PIXEL_SIZE_BYTES;

			// Space in a result block not occupied by the canvas, in bytes
			static constexpr unsigned int		RESULT_BLOCK_NON_CANVAS_SIZE_BYTES	= DATA_BLOCK_SIZE_BYTES - RESULT_BLOCK_CANVAS_SIZE_BYTES;

			// Maximum number of instructions in a result block
			//   Defined in STRUCTS region right after the result instruction to allow it to be calculated with the size of the result instruction

			// Space not taken up by canvas or instruction bin in a result block
			//   Defined in STRUCTS region because it depends on the above value, which is also defined there

			// Minimum amount of space that can be left in a result block's bin before it is considered full and the result must flush instructions
			static constexpr unsigned short		RESULT_FLUSH_THRESHOLD				= 4;

			// Maximum number of instructions that can be sent to a thread at once (this is ideally replaced by a heuristic in the future)
			static constexpr unsigned int		MAX_INSTRUCTIONS_PER_THREAD			= 256;

#pragma endregion DEFINES
#pragma region ENUMS

			// Detected file type of a source data file
			enum class SOURCE_FILE_TYPE : int
			{
				UNSUPPORTED				= -1,	// File type is not natively supported by GBlitter
				UNUSED					=  0,	// File is not used in source data (This value is used during data normalization while importing sources)
				TGA						= +1,	// TGA (Targa)
				BMP						= +2,	// BMP (Bitmap)
			};

			// Color formats for TGA files
			/*
				Officially supported image types in TGA file format guidelines
				Values are discrete formats and have no relation to each other
			*/
			struct TGA_IMAGE_TYPE
			{
				enum
				{
					NONE							= 0,	//						no image data is included in the file
					UNCOMPRESSED_COLORMAPPED		= 1,	// [RGB_LUT]			image data is not compressed and is in either pseudo-color or direct-color
					UNCOMPRESSED_TRUECOLOR			= 2,	// [RGB / ARGB]			image data is not compressed and is in true-color
					UNCOMPRESSED_GREYSCALE			= 3,	// [L / AL]				image data is not compressed and is in greyscale true-color
					COMPRESSED_COLORMAPPED			= 9,	// [RLE_RGB_LUT]		image data is run-length encoded and is in either pseudo-color or direct-color
					COMPRESSED_TRUECOLOR			= 10,	// [RLE_RGB / RLE_ARGB]	image data is run-length encoded and is in true-color
					COMPRESSED_GREYSCALE			= 11,	// [RLE_L / RLE_AL]		image data is run-length encoded and is in greyscale true-color
				};
			};

			// Supported pixel attributes for source file data
			struct SOURCE_FILE_PIXEL_ATTRIBUTES
			{
				// Data types/attributes supported for loaded source data pixels (only some combinations of elements are supported)
				enum ELEMENT
				{
					LUMINANCE		= 0x1,
					COLOR			= 0x2,
					ALPHA			= 0x4,
					LUT				= 0x8, // Look-Up Table
					RLE				= 0x10, // Run-Length Encoding
				};
				// Supported combinations of data types for loaded source data
				enum class FORMAT : int
				{
					INVALID				= 0,

					// Greyscale | Luminance | 8bpp | 1 channel, 8b per channel
					L_8					= ELEMENT::LUMINANCE,

					// Greyscale | Alpha-Luminance | 16bpp | 2 channels, 8b per channel
					AL_16				= ELEMENT::ALPHA | ELEMENT::LUMINANCE,

					// Color | Red-Green-Blue | 24bpp | 3 channels, 8b per channel
					RGB_24				= ELEMENT::COLOR,

					// Color | Alpha-Red-Green-Blue | 32bpp | 4 channels, 8b per channel
					ARGB_32				= ELEMENT::ALPHA | ELEMENT::COLOR,

					// Color | Red-Green-Blue Look-Up Table | 8bpp | 3 channels, 8b per channel; pixels contain an 8-bit index into a table of color values
					RGB_LUT_8			= ELEMENT::COLOR | ELEMENT::LUT,

					// Greyscale | Luminance, Run-Length Encoded | 8bpp | 1 channel, 8b per channel
					L_8_RLE				= ELEMENT::LUMINANCE | ELEMENT::RLE,

					// Greyscale | Alpha-Luminance, Run-Length Encoded | 16bpp | 2 channels, 8b per channel
					AL_16_RLE			= ELEMENT::ALPHA | ELEMENT::LUMINANCE | ELEMENT::RLE,

					// Color | Red-Green-Blue, Run-Length Encoded | 24bpp | 3 channels, 8b per channel
					RGB_24_RLE			= ELEMENT::COLOR | ELEMENT::RLE,

					// Color | Alpha-Red-Green-Blue, Run-Length Encoded | 32bpp | 4 channels, 8b per channel
					ARGB_32_RLE			= ELEMENT::ALPHA | ELEMENT::COLOR | ELEMENT::RLE,

					// Color | Red-Green-Blue Look-Up Table, Run-Length Encoded | 8bpp | 3 channels, 8b per channel; pixels contain an 8-bit index into a table of color values
					RGB_LUT_8_RLE		= ELEMENT::COLOR | ELEMENT::LUT | ELEMENT::RLE,
				};
			};

			// Pixel attributes for blitter internal pixels
			struct INTERNAL_PIXEL_ATTRIBUTES
			{
				// Data types that blitter internal pixels can contain
				enum ELEMENT
				{
					COLOR				= 0x1,
					LAYER				= 0x2,
					STENCIL				= 0x4,
				};
				// Possible combinations of data types in a blitter internal pixel (sources can have any valid format; the result uses only CLS format)
				enum class FORMAT : int
				{
					INVALID					= 0,
					C						= 1,
					L						= 2,
					S						= 3,
					CL						= 4,
					CS						= 5,
					LS						= 6,
					CLS						= 7,
				};
			};
			
			// Shorthands for DrawOptions bit-flag combinations indicating basic transformations
			/*
			*	Labels also indicate the angle produced by the flag combination and whether it is inverted (read: mirrored about its local y-axis)
			*
			*	R		ROTATE flag is on
			*	V		MIRROR_VERTICAL flag is on
			*	H		MIRROR_HORIZONTAL is on
			*	X		Flag is off
			*/
			struct BASIC_ORIENTATION
			{
				enum
				{
					XXX_0				= 0,
					RXX_90				= DrawOptions::ROTATE,
					XVX_180_INV			= DrawOptions::MIRROR_VERTICAL,
					RVX_90_INV			= DrawOptions::MIRROR_VERTICAL			| DrawOptions::ROTATE,
					XXH_0_INV			= DrawOptions::MIRROR_HORIZONTAL,
					RXH_270_INV			= DrawOptions::MIRROR_HORIZONTAL		| DrawOptions::ROTATE,
					XVH_180				= DrawOptions::MIRROR_HORIZONTAL		| DrawOptions::MIRROR_VERTICAL,
					RVH_270				= DrawOptions::MIRROR_HORIZONTAL		| DrawOptions::MIRROR_VERTICAL			| DrawOptions::ROTATE,
				};
			};
			
			// Shorthands for various DrawOptions bit-flag combinations indicating different methods of drawing tiles
			/*
			*	MSK		USE_MASKING
			*	TSF		USE_TRANSFORMATIONS
			*	TSP		USE_TRANSPARENCY
			*	ITP		USE_BILINEAR_INTERP
			*/
			struct RESULT_INSTRUCTION_FORMAT
			{
				enum
				{
					DEFAULT				= 0,
					MSK					= DrawOptions::USE_MASKING,
					TSF					= DrawOptions::USE_TRANSFORMATIONS,
					TSF_MSK				= DrawOptions::USE_TRANSFORMATIONS		| DrawOptions::USE_MASKING,
					TSP					= DrawOptions::USE_TRANSPARENCY,
					TSP_MSK				= DrawOptions::USE_TRANSPARENCY			| DrawOptions::USE_MASKING,
					TSP_TSF				= DrawOptions::USE_TRANSPARENCY			| DrawOptions::USE_TRANSFORMATIONS,
					TSP_TSF_MSK			= DrawOptions::USE_TRANSPARENCY			| DrawOptions::USE_TRANSFORMATIONS		| DrawOptions::USE_MASKING,
				};
			};

#pragma endregion ENUMS
#pragma region STRUCTS

#pragma pack(push,1) // single-byte alignment for loading
			// TGA file header
			struct TgaHeader
			{
				Byte				id_length;
				Byte				color_map_type;
				Byte				data_type_code;
				unsigned short		color_map_origin;
				unsigned short		color_map_length;
				Byte				color_map_depth;
				unsigned short		x_origin;
				unsigned short		y_origin;
				unsigned short		width;
				unsigned short		height;
				Byte				bits_per_pixel;
				Byte				image_descriptor;
				char				image_id[255];
				Byte				color_map[3 * 256];
			};
			// TGA run-length encoding pixel data packet
			struct TgaRLEpacket
			{
				enum class RLE_PACKET_TYPE : int
				{
					RAW = 0,
					RUN_LENGTH = 1,
				};

				RLE_PACKET_TYPE		packet_type = RLE_PACKET_TYPE::RAW; // bit 7 indicates whether this packet is a run-length packet (1) or a raw packet (0)
				Byte				pixel_count = 0; // bits 6-0 indicate the pixel count of the packet (number is one less than pixel count since pixel count cannot be zero, ie 0 = 1 pixel, 127 = 128 pixels)
				Byte				pixel_data[128 * 4]; // holds up to 128 pixels (maximum possible in a TGA RLE packet) with up to 4 bytes per pixel
			};
#pragma pack(pop)

			// NOTE Byte Labeled info is not necissarly needed after first load. 
			// pre converted information is used in the conversion process
#pragma pack(push, 2)
			struct BmpHeader {									//					  0 
				Byte signature[2];								// + 2				= 2
				unsigned int fileSize;							// + 4				= 6
				Byte reserved1[2];								// + 2				= 10
				Byte reserved2[2];								// + 2				= 12
				unsigned int dataOffset;						// + 4				= 14
			};													// TOTAL:				14 
			struct DIBV5Header {								//						= 0
				unsigned int headerSize;						// + 4					= 4
				int imageWidth;									// + 4					= 8
				int imageHeight;								// + 4					= 12
				short int planes; short int bitsPerPixel;		// + 2 , 2				= 16
				unsigned int compression;						// + 4					= 20
				unsigned int imageSize;							// + 4					= 24
				int  xPixelsPerMeter;							// + 4					= 28
				int  yPixelsPerMeter;							// + 4					= 32
				unsigned int colorsInColorTable;				// + 4					= 36
				unsigned int importantColorCount;				// + 4					= 40
				unsigned int redChannelMask;					// + 4 (big-endian)		= 44
				unsigned int greenChannelMask;					// + 4 (big-endian)		= 48
				unsigned int blueChannelMask;					// + 4 (big-endian)		= 52
				unsigned int alphaChannelMask;					// + 4 (big-endian)		= 56
				unsigned int colorSpaceType;					// + 4					= 60
				Byte colorSpaceEndpoints[36];					// + 36					= 96
				unsigned int gammaForRedChannel;				// + 4					= 100
				unsigned int gammaForGreenChannel;				// + 4					= 104
				unsigned int gammaForBlueChannel;				// + 4					= 108
				unsigned int intent;							// + 4					= 112
				unsigned int iccProfileData;					// + 4					= 116
				unsigned int iccProfileSize;					// + 4					= 120
				unsigned int reserved;							// + 4					= 124
			};													// TOTAL:				= 124
#pragma pack(pop)



			struct Vector2
			{
				union
				{
					struct
					{
						float x;
						float y;
					};
					float data[2];
				};
			};
			struct Vector3
			{
				union
				{
					struct
					{
						float x;
						float y;
						float z;
					};
					float data[3];
				};
			};

			struct Matrix2x2
			{
				union
				{
					struct
					{
						float a;
						float b;

						float c;
						float d;
					};
					float matrix[2][2];
					Vector2 vectors[2];
					float data[4];
				};
			};
			struct Matrix3x3
			{
				union
				{
					struct
					{
						float a;
						float b;
						float c;

						float d;
						float e;
						float f;

						float g;
						float h;
						float i;
					};
					float matrix[3][3];
					Vector3 vectors[3];
					float data[9];
				};
			};

			struct Point
			{
				int x;
				int y;

				Point() { x = 0; y = 0; }
				Point(int _x, int _y) { x = _x; y = _y; }
				Point(const Point& _lhs) { if (&_lhs != this) { x = _lhs.x; y = _lhs.y; } }
				Point(Vector2 _lhs) { x = static_cast<int>(_lhs.x); y = static_cast<int>(_lhs.y); }
			};
			struct Triangle
			{
				Point a;
				Point b;
				Point c;

				Triangle() {}
				Triangle(Point _a, Point _b, Point _c) { a = _a; b = _b; c = _c; }
				Triangle(Vector2 _a, Vector2 _b, Vector2 _c) { a = _a; b = _b; c = _c; }
			};
			struct Quad
			{
				Point a;
				Point b;
				Point c;
				Point d;

				Quad() {}
				Quad(Point _a, Point _b, Point _c, Point _d) { a = _a; b = _b; c = _c; d = _d; }
				Quad(Vector2 _a, Vector2 _b, Vector2 _c, Vector2 _d) { a = _a; b = _b; c = _c; d = _d; }
			};

			// struct containing data in a normalized format to be stored in a source
			// 10 B + data
			struct SourceDataBuffer
			{
				unsigned short			dataTypes = 0;		// bit-flags indicating the types of data present
				unsigned short			w = 0;				// width of source, in pixels
				unsigned short			h = 0;				// height of source, in pixels
				unsigned int			size = 0;			// size of source, in pixels
				Color*					colors = nullptr;	// pixel color data, if present
				Layer*					layers = nullptr;	// pixel layer data, if present
				Stencil*				stencils = nullptr;	// pixel stencil data, if present
			};

			// struct containing one block of data from a source
			// 256 KB (262,144 B)
			struct SourceBlock
			{
				// could contain color, layer, and/or stencil data
				struct COLOR_DATA
				{
					Color				colors[65536];
					// color = 4 B; 4 B per pixel; 256 x 256 pixels
					// 256 * 256 * 4 = 262,144 B (256 KB); 0 B (0 KB) unused
				};
				struct LAYER_DATA
				{
					Layer				layers[262144];
					// layer = 1 B; 1 B per pixel; 256 x 1024 pixels
					// 256 * 1024 * 1 = 262,144 B (256 KB); 0 B (0 KB) unused
				};
				struct STENCIL_DATA
				{
					Stencil				stencils[262144];
					// stencil = 1 B; 1 B per pixel; 256 x 1024 pixels
					// 256 * 1024 * 1 = 262,144 B (256 KB); 0 B (0 KB) unused
				};
				struct COLOR_LAYER_DATA
				{
					Color				colors[52224];
					Layer				layers[52224];
					Byte				pad[1024];
					// color = 4 B, layer = 1 B; 5 B per pixel; 256 x 204 pixels
					// 256 * 204 * 5 = 261,120 B (255 KB); 1024 B (1 KB) unused
				};
				struct COLOR_STENCIL_DATA
				{
					Color				colors[52224];
					Stencil				stencils[52224];
					Byte				pad[1024];
					// color = 4 B, stencil = 1 B; 5 B per pixel; 256 x 204 pixels
					// 256 * 204 * 5 = 261,120 B (255 KB); 1024 B (1 KB) unused
				};
				struct LAYER_STENCIL_DATA
				{
					Layer				layers[131072];
					Stencil				stencils[131072];
					// layer = 1 B, stencil = 1 B; 2 B per pixel; 256 x 512 pixels
					// 256 * 512 * 2 = 262,144 B (256 KB); 0 B (0 KB) unused
				};
				struct COLOR_LAYER_STENCIL_DATA
				{
					Color				colors[43520];
					Layer				layers[43520];
					Stencil				stencils[43520];
					// color = 4 B, layer = 1 B, stencil = 1 B; 6 B per pixel; 256 x 170 pixels
					// 256 * 170 * 6 = 261,120 B (255 KB); 1024 B (1 KB) unused
				};
				
				// all possibilities in the same memory space
				union
				{
					COLOR_DATA					c;
					LAYER_DATA					l;
					STENCIL_DATA				s;
					COLOR_LAYER_DATA			cl;
					COLOR_STENCIL_DATA			cs;
					LAYER_STENCIL_DATA			ls;
					COLOR_LAYER_STENCIL_DATA	cls;
					Byte						raw[262144];
				};
			};

			// struct containing source blocks and metadata about them
			// 18 B + data
			struct Source
			{
				INTERNAL_PIXEL_ATTRIBUTES::FORMAT	format;				// indicates the types of data contained in the source
				unsigned short						w_image;			// width of image, in pixels
				unsigned short						h_image;			// height of image, in pixels
				unsigned short						w_data;				// width of data, in source blocks
				unsigned short						h_data;				// height of data, in source blocks (dependent on pixel format)
				std::vector<SourceBlock>			data;				// source blocks containing source's pixel data
			};

			// struct containing either:
			//   (basic draw) a partial draw instruction that fits entirely within one source block and one result block
			//   (transformed draw) a tile and the inverse of the transformation applied to it
			// 88 B (86 B + padding)
			struct ResultInstruction
			{
				unsigned short			flags;				//  2 B - bit-flags indicating data to use and operations to perform
				unsigned short			source_id;			//  2 B - index of source that instruction refers to
				unsigned short			source_block_id;	//  2 B - index of source block that instruction refers to
				unsigned short			x_source;			//  2 B - x coordinate of data origin in source block, in pixels
				unsigned short			y_source;			//  2 B - y coordinate of data origin in source block, in pixels
				unsigned short			w_source;			//  2 B - width of data in source, in pixels
				unsigned short			h_source;			//  2 B - height of data in source, in pixels
				unsigned short			x_result;			//  2 B - x coordinate in result block to draw to, in pixels
				unsigned short			y_result;			//  2 B - y coordinate in result block to draw to, in pixels
				unsigned short			w_result;			//  2 B - width of drawn image in result, in pixels
				unsigned short			h_result;			//  2 B - height of drawn image in result, in pixels
				Layer					layer_override;		//  1 B - layer to draw to, if not using source layer values (still used for sorting even if using source layer values[, unless SORT_PER_PIXEL flag is on (planned feature)])
				Stencil					stencil_override;	//  1 B - stencil value to apply, if not using source stencil values
				Color					mask_color;			//  4 B - color value to remove with bitmasking
				Layer					mask_layer;			//  1 B - layer value to remove with bitmasking
				Stencil					mask_stencil;		//  1 B - stencil value to remove with bitmasking
				// 30 B

				Byte					pad[2];				//  2 B - needed for 8-byte alignment

				// only used by transformed instructions
				Matrix2x2				m_inverse;			// 16 B - inverse of transform matrix
				Vector2					t_inverse;			//  8 B - inverse of translation vector
				Quad					footprint;			// 32 B - quadrilateral formed by tile after transformation
				//56 B
			};

			// Maximum number of instructions in a result block
			//   Defined here instead of in DEFINES region to allow the size of the result instruction struct to be used to automatically determine how many can fit into 256 KB
			static constexpr unsigned short INSTRUCTIONS_PER_RESULT_BLOCK = RESULT_BLOCK_NON_CANVAS_SIZE_BYTES / sizeof(ResultInstruction);

			// Space not taken up by canvas or instruction bin in a result block
			//   Defined here instead of in DEFINES region because it depends on INSTRUCTIONS_PER_RESULT_BLOCK, which is also defined here
			static constexpr unsigned short RESULT_BLOCK_REMAINING_SIZE_BYTES = RESULT_BLOCK_NON_CANVAS_SIZE_BYTES - (INSTRUCTIONS_PER_RESULT_BLOCK * sizeof(ResultInstruction));

			// struct containing one block of data from the result
			// 256 KB (262,144 B)
			struct ResultBlock
			{
				// canvas; 128 x 128 pixels
				Color							color[RESULT_BLOCK_CANVAS_SIZE_PIXELS];
				Layer							layer[RESULT_BLOCK_CANVAS_SIZE_PIXELS];
				Stencil							stencil[RESULT_BLOCK_CANVAS_SIZE_PIXELS];
				// container for result instructions
				ResultInstruction				instruction_bin[INSTRUCTIONS_PER_RESULT_BLOCK];
				// other needed variables
				unsigned short					instruction_count = 0;
				unsigned short					x_result; // x coordinate of origin in result canvas
				unsigned short					y_result; // y coordinate of origin in result canvas

				ResultBlock() {};
				ResultBlock(const ResultBlock& _lhs)
				{
					memcpy(color, _lhs.color, RESULT_BLOCK_CANVAS_SIZE_PIXELS * sizeof(Color));
					memcpy(layer, _lhs.layer, RESULT_BLOCK_CANVAS_SIZE_PIXELS * sizeof(Layer));
					memcpy(stencil, _lhs.stencil, RESULT_BLOCK_CANVAS_SIZE_PIXELS * sizeof(Stencil));
					memcpy(instruction_bin, _lhs.instruction_bin, INSTRUCTIONS_PER_RESULT_BLOCK * sizeof(ResultInstruction));
					instruction_count = _lhs.instruction_count;
					x_result = _lhs.x_result;
					y_result = _lhs.y_result;
				}
			};

			// struct containing result blocks and metadata about them
			// 14 B + data
			struct Result
			{
				unsigned short					w_image;			// width of image, in pixels
				unsigned short					h_image;			// height of image, in pixels
				unsigned int					size_image;			// size of image, in pixels
				unsigned short					w_data;				// width of data, in result blocks
				unsigned short					h_data;				// height of data, in result blocks
				unsigned short					size_data;			// size of data, in result blocks
				std::vector<ResultBlock>		data;				// result blocks containing result's data
				std::vector<GReturn>			flush_results;		// results of individual block flush operations
			};




#pragma endregion STRUCTS
#pragma region VARIABLES

			// Gateware types are named as:	gVariableName

			GW::SYSTEM::GConcurrent				m_gFlushThread;
			gtl::ld_vector<Source>				m_sources;
			gtl::factory<TileDefinition>		m_tiles;
			Result								m_result;

#pragma endregion VARIABLES
#pragma region PRIVATE_GENERAL_FUNCTIONS

			// Converts pixel data type flags into a pixel format
			static inline INTERNAL_PIXEL_ATTRIBUTES::FORMAT pixelFlagsToFormat(unsigned short _dataTypes)
			{
				switch (_dataTypes)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS;
						break;
					default:
						return INTERNAL_PIXEL_ATTRIBUTES::FORMAT::INVALID;
						break;
				}
			}
			// Converts a pixel format into pixel data type flags
			static inline unsigned short pixelFormatToFlags(INTERNAL_PIXEL_ATTRIBUTES::FORMAT _format)
			{
				unsigned short dataTypeFlags = 0;
				if (   _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS)
					dataTypeFlags |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR;
				if (   _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS)
					dataTypeFlags |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER;
				if (   _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS
					|| _format == INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS)
					dataTypeFlags |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL;
				return dataTypeFlags;
			}

			// Returns the height of a source block containing the specified data types
			static inline unsigned short getSourceBlockHeight(unsigned short _dataTypes)
			{
				switch (_dataTypes)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR:
						return SOURCE_BLOCK_HEIGHT_C;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						return SOURCE_BLOCK_HEIGHT_L;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return SOURCE_BLOCK_HEIGHT_S;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						return SOURCE_BLOCK_HEIGHT_CL;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return SOURCE_BLOCK_HEIGHT_CS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return SOURCE_BLOCK_HEIGHT_LS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						return SOURCE_BLOCK_HEIGHT_CLS;
						break;
					default:
						return 0;
						break;
				}
			}
			static inline unsigned short getSourceBlockHeight(INTERNAL_PIXEL_ATTRIBUTES::FORMAT _format)
			{
				switch (_format)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C:
						return SOURCE_BLOCK_HEIGHT_C;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L:
						return SOURCE_BLOCK_HEIGHT_L;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S:
						return SOURCE_BLOCK_HEIGHT_S;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL:
						return SOURCE_BLOCK_HEIGHT_CL;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS:
						return SOURCE_BLOCK_HEIGHT_CS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS:
						return SOURCE_BLOCK_HEIGHT_LS;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
						return SOURCE_BLOCK_HEIGHT_CLS;
						break;
					default:
						return 0;
						break;
				}
			}

			// Returns the pixel format of a source file
			static inline SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT getSourceFilePixelFormat(SOURCE_FILE_TYPE _fileType, unsigned int _formatCode, unsigned int _bitsPerPixel)
			{
				SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::INVALID;
				switch (_fileType)
				{
					case (SOURCE_FILE_TYPE::TGA):
					{
						switch (_formatCode)
						{
							case TGA_IMAGE_TYPE::UNCOMPRESSED_COLORMAPPED:
							{
								switch (_bitsPerPixel)
								{
									case 8:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_LUT_8;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::UNCOMPRESSED_COLORMAPPED

							case TGA_IMAGE_TYPE::UNCOMPRESSED_TRUECOLOR:
							{
								switch (_bitsPerPixel)
								{
									case 24:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_24;
										break;
									case 32:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::ARGB_32;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::UNCOMPRESSED_TRUECOLOR

							case TGA_IMAGE_TYPE::UNCOMPRESSED_GREYSCALE:
							{
								switch (_bitsPerPixel)
								{
									case 8:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::L_8;
										break;
									case 16:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::AL_16;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::UNCOMPRESSED_GREYSCALE

							case TGA_IMAGE_TYPE::COMPRESSED_COLORMAPPED:
							{
								switch (_bitsPerPixel)
								{
									case 8:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_LUT_8_RLE;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::COMPRESSED_COLORMAPPED

							case TGA_IMAGE_TYPE::COMPRESSED_TRUECOLOR:
							{
								switch (_bitsPerPixel)
								{
									case 24:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_24_RLE;
										break;
									case 32:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::ARGB_32_RLE;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::COMPRESSED_TRUECOLOR

							case TGA_IMAGE_TYPE::COMPRESSED_GREYSCALE:
							{
								switch (_bitsPerPixel)
								{
									case 8:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::L_8_RLE;
										break;
									case 16:
										format = SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::AL_16_RLE;
										break;
									default: break;
								} // end switch (_bitsPerPixel)
							} break; // end case TGA_IMAGE_TYPE::COMPRESSED_GREYSCALE

							default: break;
						} // end switch (_formatCode)
						break;
					} break; // end case TGA

					default: break;
				} // end switch (_fileType)
				return format;
			}

#pragma endregion PRIVATE_GENERAL_FUNCTIONS
#pragma region PRIVATE_INIT_FUNCTIONS

			// Initalizes a result buffer and allocates result blocks for pixel data
			inline void initResult(unsigned short _w, unsigned short _h)
			{
				// store image dimensions
				m_result.w_image = _w;
				m_result.h_image = _h;
				m_result.size_image = _w * _h;
				// calculate data dimensions
				m_result.w_data = (m_result.w_image / RESULT_BLOCK_WIDTH) + ((m_result.w_image % RESULT_BLOCK_WIDTH) ? 1 : 0);
				m_result.h_data = (m_result.h_image / RESULT_BLOCK_HEIGHT) + ((m_result.h_image % RESULT_BLOCK_HEIGHT) ? 1 : 0);
				m_result.size_data = m_result.w_data * m_result.h_data;
				// allocate result blocks and flush results
				for (unsigned int i = 0; i < static_cast<unsigned int>(m_result.size_data); ++i)
				{
					ResultBlock block = {};
					block.x_result = (i % m_result.w_data) * RESULT_BLOCK_WIDTH;
					block.y_result = (i / m_result.w_data) * RESULT_BLOCK_HEIGHT;
					m_result.data.push_back(block);
					m_result.flush_results.push_back(GReturn::REDUNDANT);
				}
			}

			// Initializes a passed in source data buffer and allocates memory for data
			static inline void initSourceDataBuffer(SourceDataBuffer& _outSourceDataBuffer, unsigned short _w, unsigned short _h)
			{
				_outSourceDataBuffer.w = _w;
				_outSourceDataBuffer.h = _h;
				_outSourceDataBuffer.size = _w * _h;
				// initialize arrays to nullptr in case they aren't needed
				_outSourceDataBuffer.colors = nullptr;
				_outSourceDataBuffer.layers = nullptr;
				_outSourceDataBuffer.stencils = nullptr;
				// allocate arrays to store data in as needed
				if (_outSourceDataBuffer.dataTypes & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
					_outSourceDataBuffer.colors = new Color[_outSourceDataBuffer.size];
				if (_outSourceDataBuffer.dataTypes & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)
					_outSourceDataBuffer.layers = new Layer[_outSourceDataBuffer.size];
				if (_outSourceDataBuffer.dataTypes & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL)
					_outSourceDataBuffer.stencils = new Stencil[_outSourceDataBuffer.size];
			}
			// Resets a source data buffer's values and deallocates memory
			static inline void discardSourceDataBuffer(SourceDataBuffer& _sourceDataBuffer)
			{
				_sourceDataBuffer.dataTypes = 0;
				_sourceDataBuffer.w = 0;
				_sourceDataBuffer.h = 0;
				_sourceDataBuffer.size = 0;
				if (_sourceDataBuffer.colors)
				{
					delete[] _sourceDataBuffer.colors;
					_sourceDataBuffer.colors = nullptr;
				}
				if (_sourceDataBuffer.layers)
				{
					delete[] _sourceDataBuffer.layers;
					_sourceDataBuffer.layers = nullptr;
				}
				if (_sourceDataBuffer.stencils)
				{
					delete[] _sourceDataBuffer.stencils;
					_sourceDataBuffer.stencils = nullptr;
				}
			}

			// Extracts the extension from a filepath and returns the detected file type
			/*
			*	retval GReturn::SUCCESS				File type was determined.
			*	retval GReturn::FAILURE				_filepath did not have an extension.
			*/
			static GReturn filterFileExtension(const char* _filepath, SOURCE_FILE_TYPE& _outFiletype)
			{
				// find last '.' in filepath that comes after the last slash to locate extension
				short lastPeriodIndex = -1;
				for (int c = static_cast<int>(strlen(_filepath)) - 1; c >= 0; --c)
				{
					if (_filepath[c] == '\\' || _filepath[c] == '/')
						break;
					if (_filepath[c] == '.')
					{
						lastPeriodIndex = c;
						break;
					}
				}
				// if no '.' is found or a '.' is the first character, filepath is invalid
				if (lastPeriodIndex <= 0)
					return GReturn::FAILURE;
				// otherwise, extract extension from filepath
				size_t extensionStartIndex = _filepath + lastPeriodIndex - _filepath + 1; // get index of first character in extension
				size_t extensionLength = strlen(_filepath) - extensionStartIndex + 1; // get length of extension
				char extension[260]; // allocate and clear array to store extension
				memset(extension, 0, extensionLength);
				for (size_t i = 0; i < extensionLength; ++i) // copy extension into array and convert to lowercase
					extension[i] = tolower(_filepath[i + extensionStartIndex]);
				// filter extension and store detected type (0 = contents of both strings are equal)
				if (strcmp(extension, "tga") == 0)
					_outFiletype = SOURCE_FILE_TYPE::TGA;
				else if (strcmp(extension, "bmp") == 0)
					_outFiletype = SOURCE_FILE_TYPE::BMP;
				else
					_outFiletype = SOURCE_FILE_TYPE::UNSUPPORTED;
				// return success
				return GReturn::SUCCESS;
			}

			// Reads pixel data out of a TGA file and stores it in a source data buffer
			/*
			*	retval GReturn::SUCCESS				Data was loaded and converted.
			*	retval GReturn::FAILURE				GFile could not be created, or file data could not be read.
			*	retval GReturn::FILE_NOT_FOUND		File could not be opened.
			*	retval GReturn::FORMAT_UNSUPPORTED	File was not in a supported TGA format.
			*	retval GReturn::INVALID_ARGUMENT	File dimensions did not match previously loaded data type(s) file dimensions.
			*/
			GReturn readSourceDataFromTGA(const char* _filepath, unsigned int _dataType, SourceDataBuffer& _outSourceDataBuffer)
			{
				GW::SYSTEM::GFile gFile;
				if (-gFile.Create())
					return GReturn::FAILURE;

				TgaHeader header = {};
				if (-gFile.OpenBinaryRead(_filepath))
					return GReturn::FILE_NOT_FOUND;
				// read data into header
				gFile.Read(reinterpret_cast<char*>(&header), 18);

				// ensure width and height are nonzero
				if (header.width < 1 || header.height < 1)
					return GReturn::FORMAT_UNSUPPORTED;
				// ensure width and height are not greater than maximum
				if (header.width > MAX_SOURCE_WIDTH || header.height > MAX_SOURCE_HEIGHT)
					return GReturn::FORMAT_UNSUPPORTED;
				// ensure pixel format is supported
				SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT pixelFormat = getSourceFilePixelFormat(SOURCE_FILE_TYPE::TGA, header.data_type_code, header.bits_per_pixel);
				if (pixelFormat == SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::INVALID)
					return GReturn::FORMAT_UNSUPPORTED;
				// if multiple data types are used, ensure dimensions of all images are the same
				if (   (_dataType == INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER && (_outSourceDataBuffer.dataTypes & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)) // if reading layer data and color data was already read
					|| (_dataType == INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL && (_outSourceDataBuffer.dataTypes & (INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR | INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)))) // or if reading stencil data and color and/or layer data was already read
					if (header.width != _outSourceDataBuffer.w || header.height != _outSourceDataBuffer.h) // if width and/or height does not match already stored dimensions, images cannot be combined into one source
						return GReturn::INVALID_ARGUMENT;

				// read trailing ID if there is one
				if (header.id_length)
					gFile.Read(reinterpret_cast<char*>(&header.image_id), header.id_length);
				// read colormap if there is one
				if (header.color_map_length)
					gFile.Read(reinterpret_cast<char*>(&header.color_map), header.color_map_length * 3); // number of colors * 1 byte per channel per color * 3 channels
				// initialize source data buffer if it hasn't been already
				if (_outSourceDataBuffer.w == 0 && _outSourceDataBuffer.h == 0)
					initSourceDataBuffer(_outSourceDataBuffer, header.width, header.height);

				// read data into intermediate data buffer (data can have different bpp values and can represent differently sized pixel data types, so it has to be processed before storage)
				unsigned int bytesPerPixel = header.bits_per_pixel / 8;
				Byte* readBuffer = new Byte[_outSourceDataBuffer.size * bytesPerPixel];
				GReturn result = gFile.Read(reinterpret_cast<char*>(readBuffer), _outSourceDataBuffer.size * bytesPerPixel);
				if (G_FAIL(result) && result != GReturn::END_OF_FILE)
				{
					delete[] readBuffer;
					gFile.CloseFile();
					return GReturn::FAILURE;
				}
				// file is no longer needed
				gFile.CloseFile();

				// copy and normalize data from read buffer into source data buffer
				// necessary because processing can change the data size:
				//   greyscale color data must be expanded to ARGB, RGB layer/stencil data must be compacted to only the red channel, indexed color data must be filled in, compressed data must be decompressed, etc
				Byte* normalizeBuffer = nullptr;
				switch (_dataType)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR:
						normalizeBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.colors);
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						normalizeBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.layers);
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						normalizeBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.stencils);
						break;
					default: break;
				}
				normalizeSourceData(readBuffer, normalizeBuffer, header, pixelFormat, _dataType);
				// read buffer is no longer needed
				delete[] readBuffer;

				return GReturn::SUCCESS;
			}

			GReturn readSourceDataFromBMP(const char* _filepath, unsigned int dataType, SourceDataBuffer& _outSourceDataBuffer) {

				GW::SYSTEM::GFile gFile;
				// create file.
				if (-gFile.Create())
					return GReturn::FAILURE;
				// set file propertys as binary
				if (-gFile.OpenBinaryRead(_filepath))
					return GReturn::FILE_NOT_FOUND;
				
				// read header
				BmpHeader header = {};
				if (-gFile.Read(reinterpret_cast<char*>(&header), sizeof(BmpHeader))) {
					gFile.CloseFile();
					return GReturn::FAILURE;
				}

				// read dib
				DIBV5Header dib = {};
				if (-gFile.Read(reinterpret_cast<char*>(&dib), sizeof(DIBV5Header)))
				{
					gFile.CloseFile();
					return GReturn::FAILURE;
				}


				// init out buffer with information required.
				if (_outSourceDataBuffer.w == 0 && _outSourceDataBuffer.h == 0)
					initSourceDataBuffer(_outSourceDataBuffer, dib.imageWidth, dib.imageHeight);

				// with dib info and header read we are in place to read the image
				// read the image size (w*h*c) to a temp buffer for processing
				std::vector<Byte> readBuffer;
				readBuffer.resize(dib.imageSize);
				if (-gFile.Read(reinterpret_cast<char*>(readBuffer.data()), _outSourceDataBuffer.size * (dib.bitsPerPixel / 8))) {
					gFile.CloseFile();
					return GReturn::FAILURE;
				}


				if (-gFile.CloseFile()) {
					//delete[] readBuffer;
					return GReturn::FAILURE;
				}


				// destination buffer expects rgba unsigned int 4 bytes each
				// srcBuffer is in an unknown pixel type ..
				
				Byte* dstBuffer = nullptr;

				switch (dataType)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR:
						dstBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.colors);
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL:
						dstBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.stencils);
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER:
						dstBuffer = reinterpret_cast<Byte*>(_outSourceDataBuffer.layers);
						break;
				
				}

				if (dstBuffer == nullptr) {
					return GReturn::FAILURE;
				}

				// read into the destination buffer byte for byte
				if (dib.compression == 0) // NO COMPRESSION
				{
					int numPixels = dib.imageWidth * dib.imageHeight;
					switch (dataType)
					{
					case INTERNAL_PIXEL_ATTRIBUTES::COLOR:
						// Copy pixel data, flipping vertically
						for (int y = dib.imageHeight - 1; y >= 0; --y)
						{
							for (int x = 0; x < dib.imageWidth; ++x)
							{
								// Calculate the index of the current pixel
								int i = (y * dib.imageWidth + x) * 3; // Assuming RGB 24-bit image

								*dstBuffer++ = readBuffer[i];			// blue
								*dstBuffer++ = readBuffer[i + 1];		// green
								*dstBuffer++ = readBuffer[i + 2];		// red
								*dstBuffer++ = COLOR_CHANNEL_MAX;       // alpha
							}
						}
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::LAYER:
					case INTERNAL_PIXEL_ATTRIBUTES::STENCIL:
						for (int y = dib.imageHeight - 1; y >= 0; --y)
						{
							for (int x = 0; x < dib.imageWidth; ++x)
							{
								int i = (y * dib.imageWidth + x) * 3; // Assuming RGB 24-bit image
								*dstBuffer++ = readBuffer[i];
							}
						}
						break;
					default:
						break;
					}

				}
				else if(dib.compression == 3) // BIT_FIELDS
				{
					int numPixels = dib.imageWidth * dib.imageHeight / 4;
					switch (dataType)
					{
					case INTERNAL_PIXEL_ATTRIBUTES::COLOR:
						for (int y = dib.imageHeight - 1; y >= 0; --y)
						{
							for (int x = 0; x < dib.imageWidth; ++x)
							{
								// Calculate the index of the current pixel
								int i = (y * dib.imageWidth + x) * 4; // Assuming RGB 32-bit image

								*dstBuffer++ = readBuffer[i];			// blue
								*dstBuffer++ = readBuffer[i + 1];		// green
								*dstBuffer++ = readBuffer[i + 2];		// red
								*dstBuffer++ = readBuffer[i + 3];       // alpha
							}
						}
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::LAYER:
					case INTERNAL_PIXEL_ATTRIBUTES::STENCIL:
						for (int y = dib.imageHeight - 1; y >= 0; --y)
						{
							for (int x = 0; x < dib.imageWidth; ++x)
							{
								int i = (y * dib.imageWidth + x) * 4; // Assuming RGB 32-bit image
								*dstBuffer++ = readBuffer[i];
							}
						}
						break;
					default:
						break;
					}
				}
				else 
				{
					return GReturn::FORMAT_UNSUPPORTED;
				}

				return GW::GReturn::SUCCESS;
			}

			// Creates a source from a source data buffer
			void createAndStoreSource(const SourceDataBuffer& _sourceDataBuffer, unsigned short& _outIndex)
			{
				Source source;
				// store pixel format
				source.format = pixelFlagsToFormat(_sourceDataBuffer.dataTypes);
				// store image dimensions
				source.w_image = _sourceDataBuffer.w;
				source.h_image = _sourceDataBuffer.h;
				// get height of blocks for source
				unsigned short src_blockheight = getSourceBlockHeight(source.format);
				// calculate data dimensions
				source.w_data = source.w_image / SOURCE_BLOCK_WIDTH + ((source.w_image % SOURCE_BLOCK_WIDTH) ? 1 : 0);
				source.h_data = source.h_image / src_blockheight + ((source.h_image % src_blockheight) ? 1 : 0);
				// allocate source blocks for source's data
				for (unsigned int b = 0; b < static_cast<unsigned int>(source.w_data * source.h_data); ++b)
				{
					SourceBlock block;
					// calculate block's coordinates on block grid
					unsigned int y_sourceBlock = b / source.w_data;
					unsigned int x_sourceBlock = b - y_sourceBlock * source.w_data;
					// calculate starting index of block's data in source buffer
					unsigned int blockStartIndex =
						(y_sourceBlock * (source.w_image * src_blockheight)) // vertical offset of block from origin
						+ ((b - (y_sourceBlock * source.w_data)) * SOURCE_BLOCK_WIDTH); // horizontal offset of block from origin
					// calculate number of rows in block
					unsigned int rows = ((y_sourceBlock + 1) * src_blockheight) > source.h_image ? source.h_image % src_blockheight : src_blockheight;
					// calculate width of rows in block
					unsigned int rowwidth = ((x_sourceBlock + 1) * SOURCE_BLOCK_WIDTH) > source.w_image ? source.w_image % SOURCE_BLOCK_WIDTH : SOURCE_BLOCK_WIDTH;
					// iterate through block's rows
					for (unsigned int y = 0; y < rows; ++y)
					{
						// calculate starting index of row's data in source buffer
						unsigned int bufferIndex = blockStartIndex + (y * source.w_image);
						// store index that row's data starts at in block
						unsigned int sourceIndex = y * SOURCE_BLOCK_WIDTH;
						// copy data into source block
						switch (source.format)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C:
								memcpy(&block.c.colors[sourceIndex], &_sourceDataBuffer.colors[bufferIndex], rowwidth << 2);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L:
								memcpy(&block.l.layers[sourceIndex], &_sourceDataBuffer.layers[bufferIndex], rowwidth);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S:
								memcpy(&block.s.stencils[sourceIndex], &_sourceDataBuffer.stencils[bufferIndex], rowwidth);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL:
								memcpy(&block.cl.colors[sourceIndex], &_sourceDataBuffer.colors[bufferIndex], rowwidth << 2);
								memcpy(&block.cl.layers[sourceIndex], &_sourceDataBuffer.layers[bufferIndex], rowwidth);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS:
								memcpy(&block.cs.colors[sourceIndex], &_sourceDataBuffer.colors[bufferIndex], rowwidth << 2);
								memcpy(&block.cs.stencils[sourceIndex], &_sourceDataBuffer.stencils[bufferIndex], rowwidth);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS:
								memcpy(&block.ls.layers[sourceIndex], &_sourceDataBuffer.layers[bufferIndex], rowwidth);
								memcpy(&block.ls.stencils[sourceIndex], &_sourceDataBuffer.stencils[bufferIndex], rowwidth);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
								memcpy(&block.cls.colors[sourceIndex], &_sourceDataBuffer.colors[bufferIndex], rowwidth << 2);
								memcpy(&block.cls.layers[sourceIndex], &_sourceDataBuffer.layers[bufferIndex], rowwidth);
								memcpy(&block.cls.stencils[sourceIndex], &_sourceDataBuffer.stencils[bufferIndex], rowwidth);
							default: break;
						}
					}
					source.data.push_back(block);
				}
				// store source in pool and return index to caller
				_outIndex = m_sources.push_back(source);
			}

#pragma endregion PRIVATE_INIT_FUNCTIONS
#pragma region PRIVATE_DATA_MANIPULATION_FUNCTIONS

			// Inverts the order of bytes in a value (e.g. 3210 -> 0123)
			static inline void invertByteOrder(unsigned short& _outValue)
			{
				_outValue =
					  ((_outValue & BYTE_1) >> 8)
					| ((_outValue & BYTE_0) << 8);
			}
			static inline void invertByteOrder(unsigned int& _outValue)
			{
				_outValue =
					  ((_outValue & BYTE_3) >> 24)
					| ((_outValue & BYTE_2) >> 8)
					| ((_outValue & BYTE_1) << 8)
					| ((_outValue & BYTE_0) << 24);
			}
			static inline void invertByteOrder(unsigned long long& _outValue)
			{
				_outValue =
					  ((_outValue & BYTE_7) >> 56)
					| ((_outValue & BYTE_6) >> 40)
					| ((_outValue & BYTE_5) >> 24)
					| ((_outValue & BYTE_4) >> 8)
					| ((_outValue & BYTE_3) << 8)
					| ((_outValue & BYTE_2) << 24)
					| ((_outValue & BYTE_1) << 40)
					| ((_outValue & BYTE_0) << 56);
			}
			// Changes the order of bytes in a value to the specified order (e.g. 3210 -> 2031)
			static inline void changeByteOrder(unsigned int& _outValue, unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// validate args
				// ensure all orders are <= 3
				if (   _byte3Order > 3
					|| _byte2Order > 3
					|| _byte1Order > 3
					|| _byte0Order > 3)
					return;
				// ensure all orders are unique
				if (   _byte3Order == _byte0Order || _byte3Order == _byte1Order || _byte3Order == _byte2Order
					|| _byte2Order == _byte0Order || _byte2Order == _byte1Order
					|| _byte1Order == _byte0Order)
					return;
				// if args are valid, proceed
				// isolate each channel, shift them all the way to the right, then shift them left by (8 * channel order) bits
				_outValue =
					  (((_outValue & BYTE_3) >> 24)	<< (_byte3Order << 3))
					| (((_outValue & BYTE_2) >> 16)	<< (_byte2Order << 3))
					| (((_outValue & BYTE_1) >> 8)	<< (_byte1Order << 3))
					| (((_outValue & BYTE_0))		<< (_byte0Order << 3));
			}
			static inline void changeByteOrder(
				unsigned long long& _outValue,
				unsigned short _byte7Order, unsigned short _byte6Order, unsigned short _byte5Order, unsigned short _byte4Order,
				unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// validate args
				// ensure all orders are <= 7
				if (   _byte7Order > 7
					|| _byte6Order > 7
					|| _byte5Order > 7
					|| _byte4Order > 7
					|| _byte3Order > 7
					|| _byte2Order > 7
					|| _byte1Order > 7
					|| _byte0Order > 7)
					return;
				// ensure all orders are unique
				if (   _byte7Order == _byte0Order || _byte7Order == _byte1Order || _byte7Order == _byte2Order || _byte7Order == _byte3Order || _byte7Order == _byte4Order || _byte7Order == _byte5Order || _byte7Order == _byte6Order
					|| _byte6Order == _byte0Order || _byte6Order == _byte1Order || _byte6Order == _byte2Order || _byte6Order == _byte3Order || _byte6Order == _byte4Order || _byte6Order == _byte5Order
					|| _byte5Order == _byte0Order || _byte5Order == _byte1Order || _byte5Order == _byte2Order || _byte5Order == _byte3Order || _byte5Order == _byte4Order
					|| _byte4Order == _byte0Order || _byte4Order == _byte1Order || _byte4Order == _byte2Order || _byte4Order == _byte3Order
					|| _byte3Order == _byte0Order || _byte3Order == _byte1Order || _byte3Order == _byte2Order
					|| _byte2Order == _byte0Order || _byte2Order == _byte1Order
					|| _byte1Order == _byte0Order)
					return;
				// if args are valid, proceed
				// isolate each channel, shift them all the way to the right, then shift them left by (8 * channel order) bits
				_outValue =
					  (((_outValue & BYTE_7) >> 56)	<< (_byte7Order << 3))
					| (((_outValue & BYTE_6) >> 48)	<< (_byte6Order << 3))
					| (((_outValue & BYTE_5) >> 40)	<< (_byte5Order << 3))
					| (((_outValue & BYTE_4) >> 32)	<< (_byte4Order << 3))
					| (((_outValue & BYTE_3) >> 24)	<< (_byte3Order << 3))
					| (((_outValue & BYTE_2) >> 16)	<< (_byte2Order << 3))
					| (((_outValue & BYTE_1) >> 8)	<< (_byte1Order << 3))
					| (((_outValue & BYTE_0))		<< (_byte0Order << 3));
			}
			// only use this version of the function if byte orders are pre-validated
			static inline void changeByteOrder_NoValidation(unsigned int& _outValue, unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// isolate each channel, shift them all the way to the right, then shift them left by (8 * channel order) bits
				_outValue =
					  (((_outValue & BYTE_3) >> 24)	<< (_byte3Order << 3))
					| (((_outValue & BYTE_2) >> 16)	<< (_byte2Order << 3))
					| (((_outValue & BYTE_1) >> 8)	<< (_byte1Order << 3))
					| (((_outValue & BYTE_0))		<< (_byte0Order << 3));
			}
			static inline void changeByteOrder_NoValidation(
				unsigned long long& _outValue,
				unsigned short _byte7Order, unsigned short _byte6Order, unsigned short _byte5Order, unsigned short _byte4Order,
				unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// isolate each channel, shift them all the way to the right, then shift them left by (8 * channel order) bits
				_outValue =
					  (((_outValue & BYTE_7) >> 56)	<< (_byte7Order << 3))
					| (((_outValue & BYTE_6) >> 48)	<< (_byte6Order << 3))
					| (((_outValue & BYTE_5) >> 40)	<< (_byte5Order << 3))
					| (((_outValue & BYTE_4) >> 32)	<< (_byte4Order << 3))
					| (((_outValue & BYTE_3) >> 24)	<< (_byte3Order << 3))
					| (((_outValue & BYTE_2) >> 16)	<< (_byte2Order << 3))
					| (((_outValue & BYTE_1) >> 8)	<< (_byte1Order << 3))
					| (((_outValue & BYTE_0))		<< (_byte0Order << 3));
			}

			// Inverts the order of bytes in every value in an array
			static inline void invertArrayByteOrder(unsigned short* _values, unsigned int _numValues)
			{
				for (unsigned int i = 0; i < _numValues; ++i)
					invertByteOrder(_values[i]);
			}
			static inline void invertArrayByteOrder(unsigned int* _values, unsigned int _numValues)
			{
				for (unsigned int i = 0; i < _numValues; ++i)
					invertByteOrder(_values[i]);
			}
			static inline void invertArrayByteOrder(unsigned long long* _values, unsigned int _numValues)
			{
				for (unsigned int i = 0; i < _numValues; ++i)
					invertByteOrder(_values[i]);
			}
			// Changes the order of bytes in every value in an array to the specified order
			static inline void changeArrayByteOrder(unsigned int* _values, unsigned int _numValues, unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// validate args
				// ensure all orders are <= 3
				if (   _byte3Order > 3
					|| _byte2Order > 3
					|| _byte1Order > 3
					|| _byte0Order > 3)
					return;
				// ensure all orders are unique
				if (   _byte3Order == _byte0Order || _byte3Order == _byte1Order || _byte3Order == _byte2Order
					|| _byte2Order == _byte0Order || _byte2Order == _byte1Order
					|| _byte1Order == _byte0Order)
					return;
				// if args are valid, proceed
				// in the edge case that the order is 0123, the basic invert order function is faster
				if (_byte3Order == 0 && _byte2Order == 1 && _byte1Order == 2 && _byte0Order == 3)
					for (unsigned int i = 0; i < _numValues; ++i)
						invertByteOrder(_values[i]);
				// otherwise, default to brute-force change order function
				else
					for (unsigned int i = 0; i < _numValues; ++i)
						changeByteOrder_NoValidation(_values[i], _byte3Order, _byte2Order, _byte1Order, _byte0Order);
			}
			static inline void changeArrayByteOrder(
				unsigned long long* _values, unsigned int _numValues,
				unsigned short _byte7Order, unsigned short _byte6Order, unsigned short _byte5Order, unsigned short _byte4Order,
				unsigned short _byte3Order, unsigned short _byte2Order, unsigned short _byte1Order, unsigned short _byte0Order)
			{
				// validate args
				// ensure all orders are <= 7
				if (   _byte7Order > 7
					|| _byte6Order > 7
					|| _byte5Order > 7
					|| _byte4Order > 7
					|| _byte3Order > 7
					|| _byte2Order > 7
					|| _byte1Order > 7
					|| _byte0Order > 7)
					return;
				// ensure all orders are unique
				if (   _byte7Order == _byte0Order || _byte7Order == _byte1Order || _byte7Order == _byte2Order || _byte7Order == _byte3Order || _byte7Order == _byte4Order || _byte7Order == _byte5Order || _byte7Order == _byte6Order
					|| _byte6Order == _byte0Order || _byte6Order == _byte1Order || _byte6Order == _byte2Order || _byte6Order == _byte3Order || _byte6Order == _byte4Order || _byte6Order == _byte5Order
					|| _byte5Order == _byte0Order || _byte5Order == _byte1Order || _byte5Order == _byte2Order || _byte5Order == _byte3Order || _byte5Order == _byte4Order
					|| _byte4Order == _byte0Order || _byte4Order == _byte1Order || _byte4Order == _byte2Order || _byte4Order == _byte3Order
					|| _byte3Order == _byte0Order || _byte3Order == _byte1Order || _byte3Order == _byte2Order
					|| _byte2Order == _byte0Order || _byte2Order == _byte1Order
					|| _byte1Order == _byte0Order)
					return;
				// if args are valid, proceed
				// in the edge case that the order is 01234567, the basic invert order function is faster
				if (_byte7Order == 0 && _byte6Order == 1 && _byte5Order == 2 && _byte4Order == 3 && _byte3Order == 4 && _byte2Order == 5 && _byte1Order == 6 && _byte0Order == 7)
					for (unsigned int i = 0; i < _numValues; ++i)
						invertByteOrder(_values[i]);
				// otherwise, default to brute-force change order function
				else
					for (unsigned int i = 0; i < _numValues; ++i)
						changeByteOrder_NoValidation(_values[i], _byte7Order, _byte6Order, _byte5Order, _byte4Order, _byte3Order, _byte2Order, _byte1Order, _byte0Order);
			}

			// Linearly interpolates two colors by alpha channel using 0.8 fixed point arithmetic
			static inline Color fixedPointColorLerp(Color _start, Color _end)
			{
				// split colors into channels (ARGB channel order is stored in memory as BGRA due to little-endian byte order)
				ColorChannel start_bgra[4] =
				{
					ColorChannel((_start & CHANNEL_MASK_BLUE_ARGB)		),
					ColorChannel((_start & CHANNEL_MASK_GREEN_ARGB)		>> 8),
					ColorChannel((_start & CHANNEL_MASK_RED_ARGB)		>> 16),
					ColorChannel((_start & CHANNEL_MASK_ALPHA_ARGB)		>> 24)
				};
				ColorChannel end_bgra[4] =
				{
					ColorChannel((_end & CHANNEL_MASK_BLUE_ARGB)		),
					ColorChannel((_end & CHANNEL_MASK_GREEN_ARGB)		>> 8),
					ColorChannel((_end & CHANNEL_MASK_RED_ARGB)			>> 16),
					ColorChannel((_end & CHANNEL_MASK_ALPHA_ARGB)		>> 24)
				};
				// store alpha value of end color (used as ratio for blend)
				Color end_alpha = end_bgra[LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_ARGB];
				// interpolate the resulting color
				ColorChannel result_bgra[4] =
				{
					ColorChannel((((end_bgra[0] - start_bgra[0]) * end_alpha) >> 8) + start_bgra[0]),
					ColorChannel((((end_bgra[1] - start_bgra[1]) * end_alpha) >> 8) + start_bgra[1]),
					ColorChannel((((end_bgra[2] - start_bgra[2]) * end_alpha) >> 8) + start_bgra[2]),
					ColorChannel((((end_bgra[3] - start_bgra[3]) * end_alpha) >> 8) + start_bgra[3])
				};
				return *(reinterpret_cast<Color*>(result_bgra));
			}

			// Applies bitmasking, then linearly interpolates between result and another color using 0.8 fixed point arithmetic
			static inline Color fixedPointColorLerp_Masked(Color _start, Color _end, Color _maskColor)
			{
				// apply bitmasking before blending (see maskedColorCopy for details on how this works)
				Color pseudoEnd = _end ^ _maskColor;
				Color mask = (-1 + (((pseudoEnd | (~pseudoEnd + 1)) >> 31) & 1));
				_end = (_start & mask) | ((pseudoEnd ^ _maskColor) & (~mask));

				return fixedPointColorLerp(_start, _end);
			}

			// Extracts data packets from an encoded RLE data buffer
			static inline std::vector<TgaRLEpacket> extractRLEpackets(const Byte* _src, unsigned int _numPixels, unsigned short _pixelSize)
			{
				std::vector<TgaRLEpacket> packetList;
				TgaRLEpacket packet;
				Byte packet_type_and_pixel_count;

				// read packets
				for (; _numPixels;)
				{
					packet_type_and_pixel_count = *_src++; // first byte indicates both type of packet and number of pixels in packet
					packet.packet_type = packet_type_and_pixel_count & 0b10000000 ? TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH : TgaRLEpacket::RLE_PACKET_TYPE::RAW; // bit 7 indicates type; if bit is on, packet is a run-length packet; otherwise, packet is a raw packet
					packet.pixel_count = (packet_type_and_pixel_count & 0b01111111) + 1; // the rest of the bits in the byte are the pixel count; +1 because packet pixel count is one less than number of pixels in order to maximize the range of the bits (ie, 0 = 1 pixel, 127 = 128 pixels)

					// read color values
					// run-length packet has only one color value
					if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
					{
						memcpy(&packet.pixel_data[0], _src, _pixelSize);
						_src += _pixelSize;
					}
					// raw packet has a list of color values
					else
					{
						memcpy(&packet.pixel_data[0], _src, packet.pixel_count * _pixelSize);
						_src += packet.pixel_count * _pixelSize;
					}
					packetList.push_back(packet);
					_numPixels -= packet.pixel_count;
				}

				return packetList;
			}

			// Converts raw data from a source file into normalized data ready to be converted into a source
			static inline void normalizeSourceData(const Byte* _src, Byte* _dst, TgaHeader _header, SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT _pixelFormat, unsigned int _dataType)
			{
				Byte* processBuffer = _dst;
				unsigned int numPixels = _header.width * _header.height;

				// color values are stored in little endian byte order, ie ARGB is stored as BGRA, RGBA as ABGR, AL as LA, etc
				//   (this means that channel orders here look backward, but aren't)
				switch (_pixelFormat)
				{
					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::L_8:
					{
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 1 byte in -> 4 bytes out
								// duplicate luminance into color channels and set alpha to max
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src;				// luminance -> blue
									*_dst++ = *_src;				// luminance -> green
									*_dst++ = *_src++;				// luminance -> red
									*_dst++ = COLOR_CHANNEL_MAX;	// alpha
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 1 byte in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// copy data directly, since it is already in the correct format
								memcpy(_dst, _src, numPixels);
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::L_8

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::AL_16:
					{
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 2 bytes in -> 4 bytes out
								// duplicate luminance into color channels and copy alpha
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src;	// luminance -> blue
									*_dst++ = *_src;	// luminance -> green
									*_dst++ = *_src++;	// luminance -> red
									*_dst++ = *_src++;	// alpha -> alpha
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 2 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// take only luminance channel
								_src += LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL; // start at first pixel's luminance channel
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src;
									_src += SOURCE_FILE_PIXEL_SIZE_BYTES_AL_16; // advance to the next pixel's luminance channel
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::AL_16

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_24:
					{
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 3 bytes in -> 4 bytes out
								// set alpha to maximum and copy color channels
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src++;				// blue -> blue
									*_dst++ = *_src++;				// green -> green
									*_dst++ = *_src++;				// red -> red
									*_dst++ = COLOR_CHANNEL_MAX;	// alpha
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 3 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// take only red channel
								_src += LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB; // start at first pixel's red channel
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src;
									_src += SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24; // advance to the next pixel's red channel
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::RGB_24

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::ARGB_32:
					{
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 4 bytes in -> 4 bytes out
								// copy data directly, since it is already in the correct format
								memcpy(_dst, _src, numPixels << 2);
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 4 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// take only red channel
								_src += LITTLE_ENDIAN_BYTE_OFFSET_RED_ARGB; // skip to first pixel's red channel
								for (; numPixels; --numPixels)
								{
									*_dst++ = *_src;
									_src += SOURCE_FILE_PIXEL_SIZE_BYTES_ARGB_32; // advance to the next pixel's red channel
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::ARGB_32

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_LUT_8:
					{
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 1 byte in -> 4 bytes out
								for (; numPixels; --numPixels)
								{
									// get color value from LUT and set alpha to max
									// index in color map = color map origin + pixel value
									// start of color in color map = index * size of color values in color map
									// start of color channel in color map = start of color + byte offset of channel
									*_dst++ = _header.color_map[((_header.color_map_origin + *_src)		* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_BLUE_RGB];	// blue -> blue
									*_dst++ = _header.color_map[((_header.color_map_origin + *_src)		* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_GREEN_RGB];	// green -> green
									*_dst++ = _header.color_map[((_header.color_map_origin + *_src++)	* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB];	// red -> red
									*_dst++ = COLOR_CHANNEL_MAX;																														// alpha
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 1 byte in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// take only red channel
								for (; numPixels; --numPixels)
									// get color value from LUT
									*_dst++ = _header.color_map[((_header.color_map_origin + *_src++) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB];
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::RGB_LUT_8

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::L_8_RLE:
					{
						// extract packets from source buffer
						std::vector<TgaRLEpacket> packetList = extractRLEpackets(_src, numPixels, SOURCE_FILE_PIXEL_SIZE_BYTES_L_8);

						// decode packets into destination buffer
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 1 byte in -> 4 bytes out
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_L];		// luminance -> blue
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_L];		// luminance -> green
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_L];		// luminance -> red
											*_dst++ = COLOR_CHANNEL_MAX;											// alpha
										}
									}
									// for raw packets, copy list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData;			// luminance -> blue
											*_dst++ = *pixelData;			// luminance -> green
											*_dst++ = *pixelData++;			// luminance -> red
											*_dst++ = COLOR_CHANNEL_MAX;	// alpha
										}
									}
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 1 byte in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy luminance channel of single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_L];
									}
									// for raw packets, copy luminance channels of list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
											*_dst++ = *pixelData++;
									}
								}
								break;
							default: break;
						}
					} break; // end case COLOR_FORMAT::L_8_RLE

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::AL_16_RLE:
					{
						// extract packets from source buffer
						std::vector<TgaRLEpacket> packetList = extractRLEpackets(_src, numPixels, SOURCE_FILE_PIXEL_SIZE_BYTES_AL_16);

						// decode packets into destination buffer
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 2 bytes in -> 4 bytes out
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL];	// luminance -> blue
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL];	// luminance -> green
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL];	// luminance -> red
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_AL];		// alpha -> alpha
										}
									}
									// for raw packets, copy list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData;		// luminance -> blue
											*_dst++ = *pixelData;		// luminance -> green
											*_dst++ = *pixelData++;		// luminance -> red
											*_dst++ = *pixelData++;		// alpha -> alpha
										}
									}
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 2 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy luminance channel of single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL];
									}
									// for raw packets, copy luminance channels of list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data + LITTLE_ENDIAN_BYTE_OFFSET_LUMINANCE_AL; // start at first pixel's luminance channel
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData;
											pixelData += SOURCE_FILE_PIXEL_SIZE_BYTES_AL_16; // advance to the next pixel's luminance channel
										}
									}
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::AL_16_RLE

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_24_RLE:
					{
						// extract packets from source buffer
						std::vector<TgaRLEpacket> packetList = extractRLEpackets(_src, numPixels, SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24);

						// decode packets into destination buffer
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 3 bytes in -> 4 bytes out
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = packet.pixel_data[0];		// blue -> blue
											*_dst++ = packet.pixel_data[1];		// green -> green
											*_dst++ = packet.pixel_data[2];		// red -> red
											*_dst++ = COLOR_CHANNEL_MAX;		// alpha
										}
									}
									// for raw packets, copy list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData++;			// blue -> blue
											*_dst++ = *pixelData++;			// green -> green
											*_dst++ = *pixelData++;			// red -> red
											*_dst++ = COLOR_CHANNEL_MAX;	// alpha
										}
									}
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 3 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// keep only red channel
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy red channel of single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB]; // offset to get red channel
									}
									// for raw packets, copy red channels of list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB; // offset to get red channel
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData;
											pixelData += SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24; // advance to the next pixel's red channel
										}
									}
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::RGB_24_RLE

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::ARGB_32_RLE:
					{
						// extract packets from source buffer
						std::vector<TgaRLEpacket> packetList = extractRLEpackets(_src, numPixels, SOURCE_FILE_PIXEL_SIZE_BYTES_ARGB_32);

						// decode packets into destination buffer
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 4 bytes in -> 4 bytes out
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_BLUE_ARGB];	// blue -> blue
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_GREEN_ARGB];	// green -> green
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_RED_ARGB];	// red -> red
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_ALPHA_ARGB];	// alpha -> alpha
										}
									}
									// for raw packets, copy list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData++; // blue -> blue
											*_dst++ = *pixelData++; // green -> green
											*_dst++ = *pixelData++; // red -> red
											*_dst++ = *pixelData++; // alpha -> alpha
										}
									}
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 4 bytes in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// keep only red channel
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy red channel of single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
											*_dst++ = packet.pixel_data[LITTLE_ENDIAN_BYTE_OFFSET_RED_ARGB]; // offset to get red channel
									}
									// for raw packets, copy red channels of list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data + LITTLE_ENDIAN_BYTE_OFFSET_RED_ARGB; // start at first pixel's red channel
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = *pixelData;
											pixelData += SOURCE_FILE_PIXEL_SIZE_BYTES_ARGB_32; // advance to the next pixel's red channel
										}
									}
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::ARGB_32_RLE

					case SOURCE_FILE_PIXEL_ATTRIBUTES::FORMAT::RGB_LUT_8_RLE:
					{
						// extract packets from source buffer
						std::vector<TgaRLEpacket> packetList = extractRLEpackets(_src, numPixels, SOURCE_FILE_PIXEL_SIZE_BYTES_LUT_8);

						// decode packets into destination buffer
						switch (_dataType)
						{
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR: // 1 byte in -> 4 bytes out
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
										{
											// get color value from LUT
											// index in color map = color map origin + pixel value
											// start of color in color map = index * size of color values in color map
											// start of color channel in color map = start of color + byte offset of channel
											*_dst++ = _header.color_map[((_header.color_map_origin + packet.pixel_data[0]) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_BLUE_RGB];	// blue -> blue
											*_dst++ = _header.color_map[((_header.color_map_origin + packet.pixel_data[0]) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_GREEN_RGB];	// green -> green
											*_dst++ = _header.color_map[((_header.color_map_origin + packet.pixel_data[0]) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB];		// red -> red
											*_dst++ = COLOR_CHANNEL_MAX;																																	// alpha
										}
									}
									// for raw packets, copy list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
										{
											*_dst++ = _header.color_map[((_header.color_map_origin + *pixelData)	* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_BLUE_RGB];		// blue -> blue
											*_dst++ = _header.color_map[((_header.color_map_origin + *pixelData)	* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_GREEN_RGB];		// green -> green
											*_dst++ = _header.color_map[((_header.color_map_origin + *pixelData++)	* SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB];		// red -> red
											*_dst++ = COLOR_CHANNEL_MAX;																																// alpha
										}
									}
								}
								break;
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER: // 1 byte in -> 1 byte out
							case INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL: // fallthrough is intentional, because the same processing needs to happen in both cases
								// keep only red channel
								for (unsigned int i = 0; i < packetList.size(); ++i)
								{
									TgaRLEpacket packet = packetList[i];
									// for run-length packets, copy red channel of single color value from packet into destination buffer
									if (packet.packet_type == TgaRLEpacket::RLE_PACKET_TYPE::RUN_LENGTH)
									{
										for (; packet.pixel_count; --packet.pixel_count)
											// get red channel of color value from LUT
											*_dst++ = _header.color_map[((_header.color_map_origin + packet.pixel_data[0]) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB]; // offset to get red channel
									}
									// for raw packets, copy red channels of list of color values from packet into destination buffer
									else
									{
										Byte* pixelData = packet.pixel_data;
										for (; packet.pixel_count; --packet.pixel_count)
											// get red channel of color value from LUT
											*_dst++ = _header.color_map[((_header.color_map_origin + *pixelData++) * SOURCE_FILE_PIXEL_SIZE_BYTES_RGB_24) + LITTLE_ENDIAN_BYTE_OFFSET_RED_RGB]; // offset to get red channel
									}
								}
								break;
							default: break;
						} // end switch (_dataType)
					} break; // end case COLOR_FORMAT::RGB_LUT_8_RLE

					default: break;
				} // end switch (_pixelFormat)
				
				// flip horizontally and/or vertically if needed
				/*
					Image descriptor is a single-byte value representing alpha channel depth in bits 3-0 and image orientation in bits 5-4.
					Bits are numbered like so: (most significant bit) 7 6 5 4 3 2 1 0 (least significant bit)
					If bit 5 of image descriptor is 1, rows are arranged top to bottom; Otherwise, rows are arranged bottom to top. (i.e. if bit 5 is 0, image must be flipped vertically)
					If bit 4 of image descriptor is 1, colors within rows are arranged right to left; Otherwise, colors are arranged left to right. (i.e. if bit 4 is 1, image must be flipped horizontally)
				*/
				if (!(_header.image_descriptor & TGA_VERTICAL_ORIGIN_TOP) && (_header.image_descriptor & TGA_HORIZONTAL_ORIGIN_RIGHT))
					flipDataArrayDiagonal(processBuffer, _dataType, _header.width, _header.height);
				else if (!(_header.image_descriptor & TGA_VERTICAL_ORIGIN_TOP))
					flipDataArrayVertical(processBuffer, _dataType, _header.width, _header.height);
				else if (_header.image_descriptor & TGA_HORIZONTAL_ORIGIN_RIGHT)
					flipDataArrayHorizontal(processBuffer, _dataType, _header.width, _header.height);
			}

			// Flips an array of pixel data horizontally and/or vertically
			static inline void flipDataArrayHorizontal(Byte* _data, unsigned int _dataType, unsigned int _w, unsigned int _h)
			{
				// color data
				if (_dataType == INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					Color temp_color;
					Color* data = reinterpret_cast<Color*>(_data);
					for (unsigned int y = 0; y < _h; ++y)
						for (unsigned int x = 0; x < _w >> 1; ++x)
						{
							// swap values across center of row
							temp_color = data[x + (y * _w) * 4];
							data[x + (y * _w) * 4] = data[_w - 1 - x + (y * _w) * 4];
							data[_w - 1 - x + (y * _w) * 4] = temp_color;
						}
				}
				// layer/stencil data
				else
				{
					Byte temp_value;
					for (unsigned int y = 0; y < _h; ++y)
						for (unsigned int x = 0; x < _w >> 1; ++x)
						{
							// swap values across center of row
							temp_value = _data[x + (y * _w)];
							_data[x + (y * _w)] = _data[_w - 1 - x + (y * _w)];
							_data[_w - 1 - x + (y * _w)] = temp_value;
						}
				}
			}
			static inline void flipDataArrayVertical(Byte* _data, unsigned int _dataType, unsigned int _w, unsigned int _h)
			{
				// color data
				if (_dataType == INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					Color temp_row[MAX_SOURCE_WIDTH];
					Color* data = reinterpret_cast<Color*>(_data);
					for (unsigned int y = 0; y < _h >> 1; ++y)
					{
						// swap rows across center of array
						memcpy(&temp_row[0], &data[y * _w], _w << 2);
						memcpy(&data[y * _w], &data[(_h - 1 - y) * _w], _w << 2);
						memcpy(&data[(_h - 1 - y) * _w], &temp_row[0], _w << 2);
					}
				}
				// layer/stencil data
				else
				{
					Byte temp_row[MAX_SOURCE_WIDTH];
					for (unsigned int y = 0; y < _h >> 1; ++y)
					{
						// swap rows across center of array
						memcpy(&temp_row[0], &_data[y * _w], _w);
						memcpy(&_data[y * _w], &_data[(_h - 1 - y) * _w], _w);
						memcpy(&_data[(_h - 1 - y) * _w], &temp_row[0], _w);
					}
				}
			}
			static inline void flipDataArrayDiagonal(Byte* _data, unsigned int _dataType, unsigned int _w, unsigned int _h)
			{
				// color data
				if (_dataType == INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					Color temp_color;
					Color temp_row[MAX_SOURCE_WIDTH];
					Color* data = reinterpret_cast<Color*>(_data);
					for (unsigned int y = 0; y < _h >> 1; ++y)
					{
						for (unsigned int x = 0; x < _w >> 1; ++x)
						{
							// swap values across center of rows
							// row in top half
							temp_color = data[x + (y * _w)];
							data[x + (y * _w)] = data[_w - 1 - x + (y * _w)];
							data[_w - 1 - x + (y * _w)] = temp_color;
							// counterpart row in bottom half
							temp_color = data[x + ((_h - y) * _w)];
							data[x + ((_h - y) * _w)] = data[_w - 1 - x + ((_h - 1 - y) * _w)];
							data[_w - 1 - x + ((_h - 1 - y) * _w)] = temp_color;
						}
						// swap rows across center of array
						memcpy(&temp_row[0], &_data[y * _w * 4], _w << 2);
						memcpy(&_data[y * _w * 4], &_data[(_h - 1 - y) * _w * 4], _w << 2);
						memcpy(&_data[(_h - 1 - y) * _w * 4], &temp_row[0], _w << 2);
					}
				}
				// layer/stencil data
				else
				{
					Byte temp_value;
					Byte temp_row[MAX_SOURCE_WIDTH];
					for (unsigned int y = 0; y < _h >> 1; ++y)
					{
						for (unsigned int x = 0; x < _w >> 1; ++x)
						{
							// swap values across center of rows
							// row in top half
							temp_value = _data[x + (y * _w)];
							_data[x + (y * _w)] = _data[_w - 1 - x + (y * _w)];
							_data[_w - 1 - x + (y * _w)] = temp_value;
							// counterpart row in bottom half
							temp_value = _data[x + ((_h - y) * _w)];
							_data[x + ((_h - y) * _w)] = _data[_w - 1 - x + ((_h - 1 - y) * _w)];
							_data[_w - 1 - x + ((_h - 1 - y) * _w)] = temp_value;
						}
						// swap rows across center of array
						memcpy(&temp_row[0], &_data[y * _w], _w);
						memcpy(&_data[y * _w], &_data[(_h - 1 - y) * _w], _w);
						memcpy(&_data[(_h - 1 - y) * _w], &temp_row[0], _w);
					}
				}
			}

			// Copies data directly, in reverse order
			static inline void directColorCopyReverse(Color* _dst, const Color* _src, unsigned int _numPixels)
			{
				_src = _src + _numPixels - 1;
				for (; _numPixels; --_numPixels)
					*_dst++ = *_src--;
			}
			static inline void directMetadataCopyReverse(Byte* _dst, const Byte* _src, unsigned int _numPixels)
			{
				_src = _src + _numPixels - 1;
				for (; _numPixels; --_numPixels)
					*_dst++ = *_src--;
			}
			// Copies data directly, rotated 90*
			static inline void directColorCopyRotated(Color* _dst, const Color* _src, unsigned int _numPixels)
			{
				_src = _src + (_numPixels - 1) * SOURCE_BLOCK_WIDTH;
				for (; _numPixels; --_numPixels)
				{
					*_dst++ = *_src;
					_src -= SOURCE_BLOCK_WIDTH;
				}
			}
			static inline void directMetadataCopyRotated(Byte* _dst, const Byte* _src, unsigned int _numPixels)
			{
				_src = _src + (_numPixels - 1) * SOURCE_BLOCK_WIDTH;
				for (; _numPixels; --_numPixels)
				{
					*_dst++ = *_src;
					_src -= SOURCE_BLOCK_WIDTH;
				}
			}
			// Copies data directly, in reverse order and rotated 90*
			static inline void directColorCopyReverseRotated(Color* _dst, const Color* _src, unsigned int _numPixels)
			{
				for (; _numPixels; --_numPixels)
				{
					*_dst++ = *_src;
					_src += SOURCE_BLOCK_WIDTH;
				}
			}
			static inline void directMetadataCopyReverseRotated(Byte* _dst, const Byte* _src, unsigned int _numPixels)
			{
				for (; _numPixels; --_numPixels)
				{
					*_dst++ = *_src;
					_src += SOURCE_BLOCK_WIDTH;
				}
			}

			// Copies data using XOR transfer
			//   All of these functions are based on a function written by Lari Norri; See comments in maskedColorCopy for details on how they work
			static inline void maskedColorCopy(Color* _dst, const Color* _src, unsigned int _numPixels, Color _maskColor)
			{
				// store source and destination as uint64 pointers in order to operate on two pixels in one operation
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				const unsigned long long* read = reinterpret_cast<const unsigned long long*>(_src);

				// struct contaning two pixels' masks in one value
				union Mask
				{
					unsigned int partial[2];
					unsigned long long full;
				};
				Mask maskColors = {{ _maskColor, _maskColor }};

				// copy data, two pixels at a time
				for (; (_numPixels >> 1); _numPixels -= 2) // run until there are less than two pixels left to process
				{
					// store colors from source as a pseudo-color standin (any color matching _maskColor produces 0x00000000, anything else produces a nonzero value that can be turned back into the original color by XOR'ing it with the mask color)
					Mask pseudoSrc; pseudoSrc.full = *read++ ^ maskColors.full;
					// use pseudo-color to create a mask value for each color (any color matching _maskColor produces 0xFFFFFFFF, anything else produces 0x00000000)
					Mask mask =
					{{
						(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 31) & 1)),
						(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 31) & 1))
					}};
					// {A}	(*write & mask.full)				--> combine existing color with mask
					//												(preserves existing color for transparent pixels, becomes 0x00000000 for opaque pixels)
					// {B}	pseudoSrc.full ^ maskColors.full	--> XOR pseudo-color and color mask to retrieve the original color
					// {C}	({B} & ~mask.full)					--> combine color with inverse mask
					//												(becomes 0x00000000 for transparent pixels, preserves new color for opaque pixels)
					// {D}	{A} | {C}							--> combine the two colors to get the final color
					//												((existing color OR 0x00000000) for transparent pixels, (0x00000000 OR new color) for opaque pixels)
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskColors.full) & (~mask.full));
					++write;
				}
				if (_numPixels) // process the last pixel in case of an odd pixel count
				{
					_dst = reinterpret_cast<Color*>(write);
					_src = reinterpret_cast<const Color*>(read);
					Color pseudoSrc = *_src++ ^ _maskColor;
					Color mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 31) & 1));
					*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskColor) & (~mask));
					++_dst;
				}
/*
				processing steps example using 4 bit values for human readability

				_maskColor	0110

				maskColors	0110											0110

				*read		0111											0110
							(opaque)										(transparent)

				source		0111 ^ 0110										0110 ^ 0110
							0001											0000

				mask		1111 + (((0001 | (~0001 + 1)) >> 3) & 0001)		1111 + (((0000 | (~0000 + 1)) >> 3) & 0001)
							1111 + (((0001 | (1110 + 1)) >> 3) & 0001)		1111 + (((0000 | (1111 + 1)) >> 3) & 0001)
							1111 + (((0001 | 1111) >> 3) & 0001)			1111 + (((0000 | 0000) >> 3) & 0001)
							1111 + ((1111 >> 3) & 0001)						1111 + ((0000 >> 3) & 0001)
							1111 + (0001 & 0001)							1111 + (0000 & 0001)
							1111 + 0001										1111 + 0000
							0000											1111

				*write		1011											1101
							(1011 & 0000) | ((0001 ^ 0110) & ~0000)			(1101 & 1111) | ((0000 ^ 0110) & ~1111)
							0000 | (0111 & 1111)							1101 | (0110 & 0000)
							0000 | 0111										1101 | 0000
							0111											1101
*/
			}
			static inline void maskedMetadataCopy(Byte* _dst, const Byte* _src, unsigned int _numPixels, Byte _maskValue)
			{
				// store source and destination as uint64 pointers in order to operate on eight pixels in one operation
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				const unsigned long long* read = reinterpret_cast<const unsigned long long*>(_src);

				// struct contaning eight pixels' masks in one value
				union Mask
				{
					Byte partial[8];
					unsigned long long full;
				};
				Mask maskValues = {{ _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue }};

				//copy data, eight pixels at a time
				for (; (_numPixels >> 3); _numPixels -= 8) // run until there are less than eight pixels left to process
				{
					Mask pseudoSrc; pseudoSrc.full = *read++ ^ maskValues.full;
					Mask mask =
					{{
						Byte(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[2] | (~pseudoSrc.partial[2] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[3] | (~pseudoSrc.partial[3] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[4] | (~pseudoSrc.partial[4] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[5] | (~pseudoSrc.partial[5] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[6] | (~pseudoSrc.partial[6] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[7] | (~pseudoSrc.partial[7] + 1)) >> 7) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskValues.full) & (~mask.full));
					++write;
				}
				if (_numPixels) // process the last pixels for non-integer-mutliple-of-eight number of pixels
				{
					_dst = reinterpret_cast<Byte*>(write);
					_src = reinterpret_cast<const Byte*>(read);
					for (; _numPixels; --_numPixels)
					{
						Byte pseudoSrc = *_src++ ^ _maskValue;
						Byte mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 7) & 1));
						*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskValue) & (~mask));
						++_dst;
					}
				}
			}
			// Copies data in reversed order using XOR transfer
			static inline void maskedColorCopyReverse(Color* _dst, const Color* _src, unsigned int _numPixels, Color _maskColor)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				_src = _src + _numPixels - 1; // start at the end and work backward

				union Mask
				{
					unsigned int partial[2];
					unsigned long long full;
				};
				Mask maskColors = {{ _maskColor, _maskColor }};

				for (; (_numPixels >> 1); _numPixels -= 2)
				{
					Mask pseudoSrc; // data must be read from source one pixel at a time in reverse order
					pseudoSrc.partial[0] = *_src-- ^ maskColors.partial[0];
					pseudoSrc.partial[1] = *_src-- ^ maskColors.partial[1];
					Mask mask =
					{{
						(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 31) & 1)),
						(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 31) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskColors.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Color*>(write);
					Color pseudoSrc = *_src-- ^ _maskColor;
					Color mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 31) & 1));
					*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskColor) & (~mask));
					++_dst;
				}
			}
			static inline void maskedMetadataCopyReverse(Byte* _dst, const Byte* _src, unsigned int _numPixels, Byte _maskValue)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				_src = _src + _numPixels - 1; // start at the end and work backward

				union Mask
				{
					Byte partial[8];
					unsigned long long full;
				};
				Mask maskValues = {{ _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue }};

				for (; (_numPixels >> 3); _numPixels -= 8)
				{
					Mask pseudoSrc; // data must be read from source one pixel at a time in reverse order
					pseudoSrc.partial[0] = *_src-- ^ maskValues.partial[0];
					pseudoSrc.partial[1] = *_src-- ^ maskValues.partial[1];
					pseudoSrc.partial[2] = *_src-- ^ maskValues.partial[2];
					pseudoSrc.partial[3] = *_src-- ^ maskValues.partial[3];
					pseudoSrc.partial[4] = *_src-- ^ maskValues.partial[4];
					pseudoSrc.partial[5] = *_src-- ^ maskValues.partial[5];
					pseudoSrc.partial[6] = *_src-- ^ maskValues.partial[6];
					pseudoSrc.partial[7] = *_src-- ^ maskValues.partial[7];
					Mask mask =
					{{
						Byte(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[2] | (~pseudoSrc.partial[2] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[3] | (~pseudoSrc.partial[3] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[4] | (~pseudoSrc.partial[4] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[5] | (~pseudoSrc.partial[5] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[6] | (~pseudoSrc.partial[6] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[7] | (~pseudoSrc.partial[7] + 1)) >> 7) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskValues.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Byte*>(write);;
					for (; _numPixels; --_numPixels)
					{
						Byte pseudoSrc = *_src-- ^ _maskValue;
						Byte mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 7) & 1));
						*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskValue) & (~mask));
						++_dst;
					}
				}
			}
			// Copies data rotated 90* using XOR transfer
			static inline void maskedColorCopyRotated(Color* _dst, const Color* _src, unsigned int _numPixels, Color _maskColor)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				_src = _src + (_numPixels - 1) * SOURCE_BLOCK_WIDTH; // start at the end and work backward

				union Mask
				{
					unsigned int partial[2];
					unsigned long long full;
				};
				Mask maskColors = {{ _maskColor, _maskColor }};

				for (; (_numPixels >> 1); _numPixels -= 2)
				{
					Mask pseudoSrc; // source colors are in a column, ie a full row apart, and must be read in reverse order
					pseudoSrc.partial[0] = *_src ^ maskColors.partial[0]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[1] = *_src ^ maskColors.partial[1]; _src -= SOURCE_BLOCK_WIDTH;
					Mask mask =
					{{
						(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 31) & 1)),
						(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 31) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskColors.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Color*>(write);
					Color pseudoSrc = *_src ^ _maskColor; _src -= SOURCE_BLOCK_WIDTH;
					Color mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 31) & 1));
					*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskColor) & (~mask));
					++_dst;
				}
			}
			static inline void maskedMetadataCopyRotated(Byte* _dst, const Byte* _src, unsigned int _numPixels, Byte _maskValue)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);
				_src = _src + (_numPixels - 1) * SOURCE_BLOCK_WIDTH; // start at the end and work backward

				union Mask
				{
					Byte partial[8];
					unsigned long long full;
				};
				Mask maskValues = {{ _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue }};

				//copy data, eight pixels at a time
				for (; (_numPixels >> 3); _numPixels -= 8)
				{
					Mask pseudoSrc; // source colors are in a column, ie a full row apart from each other
					pseudoSrc.partial[0] = *_src ^ maskValues.partial[0]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[1] = *_src ^ maskValues.partial[1]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[2] = *_src ^ maskValues.partial[2]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[3] = *_src ^ maskValues.partial[3]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[4] = *_src ^ maskValues.partial[4]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[5] = *_src ^ maskValues.partial[5]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[6] = *_src ^ maskValues.partial[6]; _src -= SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[7] = *_src ^ maskValues.partial[7]; _src -= SOURCE_BLOCK_WIDTH;
					Mask mask =
					{{
						Byte(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[2] | (~pseudoSrc.partial[2] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[3] | (~pseudoSrc.partial[3] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[4] | (~pseudoSrc.partial[4] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[5] | (~pseudoSrc.partial[5] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[6] | (~pseudoSrc.partial[6] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[7] | (~pseudoSrc.partial[7] + 1)) >> 7) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskValues.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Byte*>(write);
					for (; _numPixels; --_numPixels)
					{
						Byte pseudoSrc = *_src ^ _maskValue; _src -= SOURCE_BLOCK_WIDTH;
						Byte mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 7) & 1));
						*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskValue) & (~mask));
						++_dst;
					}
				}
			}
			// Copies data in reverse order and rotated 90* using XOR transfer
			static inline void maskedColorCopyReverseRotated(Color* _dst, const Color* _src, unsigned int _numPixels, Color _maskColor)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);

				union Mask
				{
					unsigned int partial[2];
					unsigned long long full;
				};
				Mask maskColors = {{ _maskColor, _maskColor }};

				for (; (_numPixels >> 1); _numPixels -= 2)
				{
					Mask pseudoSrc; // source colors are in a column, ie a full row apart from each other
					pseudoSrc.partial[0] = *_src ^ maskColors.partial[0]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[1] = *_src ^ maskColors.partial[1]; _src += SOURCE_BLOCK_WIDTH;
					Mask mask =
					{{
						(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 31) & 1)),
						(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 31) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskColors.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Color*>(write);
					Color pseudoSrc = *_src ^ _maskColor; _src += SOURCE_BLOCK_WIDTH;
					Color mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 31) & 1));
					*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskColor) & (~mask));
					++_dst;
				}
			}
			static inline void maskedMetadataCopyReverseRotated(Byte* _dst, const Byte* _src, unsigned int _numPixels, Byte _maskValue)
			{
				unsigned long long* write = reinterpret_cast<unsigned long long*>(_dst);

				union Mask
				{
					Byte partial[8];
					unsigned long long full;
				};
				Mask maskValues = {{ _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue, _maskValue }};

				//copy data, eight pixels at a time
				for (; (_numPixels >> 3); _numPixels -= 8)
				{
					Mask pseudoSrc; // source colors are in a column, ie a full row apart from each other
					pseudoSrc.partial[0] = *_src ^ maskValues.partial[0]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[1] = *_src ^ maskValues.partial[1]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[2] = *_src ^ maskValues.partial[2]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[3] = *_src ^ maskValues.partial[3]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[4] = *_src ^ maskValues.partial[4]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[5] = *_src ^ maskValues.partial[5]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[6] = *_src ^ maskValues.partial[6]; _src += SOURCE_BLOCK_WIDTH;
					pseudoSrc.partial[7] = *_src ^ maskValues.partial[7]; _src += SOURCE_BLOCK_WIDTH;
					Mask mask =
					{{
						Byte(-1 + (((pseudoSrc.partial[0] | (~pseudoSrc.partial[0] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[1] | (~pseudoSrc.partial[1] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[2] | (~pseudoSrc.partial[2] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[3] | (~pseudoSrc.partial[3] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[4] | (~pseudoSrc.partial[4] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[5] | (~pseudoSrc.partial[5] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[6] | (~pseudoSrc.partial[6] + 1)) >> 7) & 1)),
						Byte(-1 + (((pseudoSrc.partial[7] | (~pseudoSrc.partial[7] + 1)) >> 7) & 1))
					}};
					*write = ((*write) & mask.full) | ((pseudoSrc.full ^ maskValues.full) & (~mask.full));
					++write;
				}
				if (_numPixels)
				{
					_dst = reinterpret_cast<Byte*>(write);
					for (; _numPixels; --_numPixels)
					{
						Byte pseudoSrc = *_src ^ _maskValue; _src += SOURCE_BLOCK_WIDTH;
						Byte mask = (-1 + (((pseudoSrc | (~pseudoSrc + 1)) >> 7) & 1));
						*_dst = ((*_dst) & mask) | ((pseudoSrc ^ _maskValue) & (~mask));
						++_dst;
					}
				}
			}

#pragma endregion PRIVATE_DATA_MANIPULATION_FUNCTIONS
#pragma region PRIVATE_MATRIX_FUNCTIONS

			// transposes a matrix (ie, row-major -> column-major or vice versa)
			static inline Matrix2x2 matrixTranspose(Matrix2x2 _m)
			{
				return
				{{{
					_m.matrix[0][0], _m.matrix[1][0],
					_m.matrix[0][1], _m.matrix[1][1],
				}}};
			}
			static inline Matrix3x3 matrixTranspose(Matrix3x3 _m)
			{
				return
				{{{
					_m.matrix[0][0], _m.matrix[1][0], _m.matrix[2][0],
					_m.matrix[0][1], _m.matrix[1][1], _m.matrix[2][1],
					_m.matrix[0][2], _m.matrix[1][2], _m.matrix[2][2],
				}}};
			}

			// multiplies two matrices together
			static inline Matrix2x2 matrixXMatrix_RM(Matrix2x2 _a, Matrix2x2 _b)
			{
				return
				{{{
					(_a.matrix[0][0] * _b.matrix[0][0]) + (_a.matrix[0][1] * _b.matrix[1][0]), //[0][0]
					(_a.matrix[0][0] * _b.matrix[0][1]) + (_a.matrix[0][1] * _b.matrix[1][1]), //[0][1]

					(_a.matrix[1][0] * _b.matrix[0][0]) + (_a.matrix[1][1] * _b.matrix[1][0]), //[1][0]
					(_a.matrix[1][0] * _b.matrix[0][1]) + (_a.matrix[1][1] * _b.matrix[1][1]), //[1][1]
				}}};
			}
			static inline Matrix3x3 matrixXMatrix_RM(Matrix3x3 _a, Matrix3x3 _b)
			{
				return
				{{{
					(_a.matrix[0][0] * _b.matrix[0][0]) + (_a.matrix[0][1] * _b.matrix[1][0]) + (_a.matrix[0][2] * _b.matrix[2][0]), //[0][0]
					(_a.matrix[0][0] * _b.matrix[0][1]) + (_a.matrix[0][1] * _b.matrix[1][1]) + (_a.matrix[0][2] * _b.matrix[2][1]), //[0][1]
					(_a.matrix[0][0] * _b.matrix[0][2]) + (_a.matrix[0][1] * _b.matrix[1][2]) + (_a.matrix[0][2] * _b.matrix[2][2]), //[0][2]

					(_a.matrix[1][0] * _b.matrix[0][0]) + (_a.matrix[1][1] * _b.matrix[1][0]) + (_a.matrix[1][2] * _b.matrix[2][0]), //[1][0]
					(_a.matrix[1][0] * _b.matrix[0][1]) + (_a.matrix[1][1] * _b.matrix[1][1]) + (_a.matrix[1][2] * _b.matrix[2][1]), //[1][1]
					(_a.matrix[1][0] * _b.matrix[0][2]) + (_a.matrix[1][1] * _b.matrix[1][2]) + (_a.matrix[1][2] * _b.matrix[2][2]), //[1][2]

					(_a.matrix[2][0] * _b.matrix[0][0]) + (_a.matrix[2][1] * _b.matrix[1][0]) + (_a.matrix[2][2] * _b.matrix[2][0]), //[2][0]
					(_a.matrix[2][0] * _b.matrix[0][1]) + (_a.matrix[2][1] * _b.matrix[1][1]) + (_a.matrix[2][2] * _b.matrix[2][1]), //[2][1]
					(_a.matrix[2][0] * _b.matrix[0][2]) + (_a.matrix[2][1] * _b.matrix[1][2]) + (_a.matrix[2][2] * _b.matrix[2][2]), //[2][2]
				}}};
			}
			static inline Matrix2x2 matrixXMatrix_CM(Matrix2x2 _a, Matrix2x2 _b)
			{
				return
				{{{
					(_a.matrix[0][0] * _b.matrix[0][0]) + (_a.matrix[1][0] * _b.matrix[0][1]), //[0][0]
					(_a.matrix[0][0] * _b.matrix[1][0]) + (_a.matrix[1][0] * _b.matrix[1][1]), //[0][1]

					(_a.matrix[0][1] * _b.matrix[0][0]) + (_a.matrix[1][1] * _b.matrix[0][1]), //[1][0]
					(_a.matrix[0][1] * _b.matrix[1][0]) + (_a.matrix[1][1] * _b.matrix[1][1]), //[1][1]
				}}};
			}
			static inline Matrix3x3 matrixXMatrix_CM(Matrix3x3 _a, Matrix3x3 _b)
			{
				return
				{{{
					(_a.matrix[0][0] * _b.matrix[0][0]) + (_a.matrix[1][0] * _b.matrix[0][1]) + (_a.matrix[2][0] * _b.matrix[0][2]), //[0][0]
					(_a.matrix[0][0] * _b.matrix[1][0]) + (_a.matrix[1][0] * _b.matrix[1][1]) + (_a.matrix[2][0] * _b.matrix[1][2]), //[0][1]
					(_a.matrix[0][0] * _b.matrix[2][0]) + (_a.matrix[1][0] * _b.matrix[2][1]) + (_a.matrix[2][0] * _b.matrix[2][2]), //[0][2]

					(_a.matrix[0][1] * _b.matrix[0][0]) + (_a.matrix[1][1] * _b.matrix[0][1]) + (_a.matrix[2][1] * _b.matrix[0][2]), //[1][0]
					(_a.matrix[0][1] * _b.matrix[1][0]) + (_a.matrix[1][1] * _b.matrix[1][1]) + (_a.matrix[2][1] * _b.matrix[1][2]), //[1][1]
					(_a.matrix[0][1] * _b.matrix[2][0]) + (_a.matrix[1][1] * _b.matrix[2][1]) + (_a.matrix[2][1] * _b.matrix[2][2]), //[1][2]

					(_a.matrix[0][2] * _b.matrix[0][0]) + (_a.matrix[1][2] * _b.matrix[0][1]) + (_a.matrix[2][2] * _b.matrix[0][2]), //[2][0]
					(_a.matrix[0][2] * _b.matrix[1][0]) + (_a.matrix[1][2] * _b.matrix[1][1]) + (_a.matrix[2][2] * _b.matrix[1][2]), //[2][1]
					(_a.matrix[0][2] * _b.matrix[2][0]) + (_a.matrix[1][2] * _b.matrix[2][1]) + (_a.matrix[2][2] * _b.matrix[2][2]), //[2][2]
				}}};
			}

			// multiplies a matrix and a vector together
			static inline Vector2 matrixXVector_RM(Matrix2x2 _m, Vector2 _v)
			{
				return
				{{{
					(_m.matrix[0][0] * _v.x) + (_m.matrix[0][1] * _v.y),
					(_m.matrix[1][0] * _v.x) + (_m.matrix[1][1] * _v.y),
				}}};
			}
			static inline Vector3 matrixXVector_RM(Matrix3x3 _m, Vector3 _v)
			{
				return
				{{{
					(_m.matrix[0][0] * _v.x) + (_m.matrix[0][1] * _v.y) + (_m.matrix[0][2] * _v.z),
					(_m.matrix[1][0] * _v.x) + (_m.matrix[1][1] * _v.y) + (_m.matrix[1][2] * _v.z),
					(_m.matrix[2][0] * _v.x) + (_m.matrix[2][1] * _v.y) + (_m.matrix[2][2] * _v.z),
				}}};
			}
			static inline Vector2 matrixXVector_CM(Matrix2x2 _m, Vector2 _v)
			{
				return
				{{{
					(_m.matrix[0][0] * _v.x) + (_m.matrix[1][0] * _v.y),
					(_m.matrix[0][1] * _v.x) + (_m.matrix[1][1] * _v.y),
				}}};
			}
			static inline Vector3 matrixXVector_CM(Matrix3x3 _m, Vector3 _v)
			{
				return
				{{{
					(_m.matrix[0][0] * _v.x) + (_m.matrix[1][0] * _v.y) + (_m.matrix[2][0] * _v.z),
					(_m.matrix[0][1] * _v.x) + (_m.matrix[1][1] * _v.y) + (_m.matrix[2][1] * _v.z),
					(_m.matrix[0][2] * _v.x) + (_m.matrix[1][2] * _v.y) + (_m.matrix[2][2] * _v.z),
				}}};
			}

			// calculates the determinant of a matrix
			static inline float matrixDeterminant(Matrix2x2 _m)
			{
				return (_m.a * _m.d) - (_m.b * _m.c);
			}
			static inline float matrixDeterminant(Matrix3x3 _m)
			{
				return _m.matrix[0][0] * matrixDeterminant(Matrix2x2{{{ _m.matrix[1][1], _m.matrix[1][2], _m.matrix[2][1], _m.matrix[2][2] }}})
						- _m.matrix[0][1] * matrixDeterminant(Matrix2x2{{{ _m.matrix[1][0], _m.matrix[1][2], _m.matrix[2][0], _m.matrix[2][2] }}})
						+ _m.matrix[0][2] * matrixDeterminant(Matrix2x2{{{ _m.matrix[1][0], _m.matrix[1][1], _m.matrix[2][0], _m.matrix[2][1] }}});
			}

			// calculates the cofactor matrix of a 3x3 matrix (ie, matrix of determinants of all minor 2x2 matrices)
			static inline Matrix3x3 cofactorMatrix(Matrix3x3 _m)
			{
				return
				{{{
					 matrixDeterminant(Matrix2x2{{{ _m.matrix[1][1], _m.matrix[1][2], _m.matrix[2][1], _m.matrix[2][2] }}}), //[0][0]
					-matrixDeterminant(Matrix2x2{{{ _m.matrix[1][0], _m.matrix[1][2], _m.matrix[2][0], _m.matrix[2][2] }}}), //[0][1]
					 matrixDeterminant(Matrix2x2{{{ _m.matrix[1][0], _m.matrix[1][1], _m.matrix[2][0], _m.matrix[2][1] }}}), //[0][2]

					-matrixDeterminant(Matrix2x2{{{ _m.matrix[0][1], _m.matrix[0][2], _m.matrix[2][1], _m.matrix[2][2] }}}), //[1][0]
					 matrixDeterminant(Matrix2x2{{{ _m.matrix[0][0], _m.matrix[0][2], _m.matrix[2][0], _m.matrix[2][2] }}}), //[1][1]
					-matrixDeterminant(Matrix2x2{{{ _m.matrix[0][0], _m.matrix[0][1], _m.matrix[2][0], _m.matrix[2][1] }}}), //[1][2]

					 matrixDeterminant(Matrix2x2{{{ _m.matrix[0][1], _m.matrix[0][2], _m.matrix[1][1], _m.matrix[1][2] }}}), //[2][0]
					-matrixDeterminant(Matrix2x2{{{ _m.matrix[0][0], _m.matrix[0][2], _m.matrix[1][0], _m.matrix[1][2] }}}), //[2][1]
					 matrixDeterminant(Matrix2x2{{{ _m.matrix[0][0], _m.matrix[0][1], _m.matrix[1][0], _m.matrix[1][1] }}}), //[2][2]
				}}};
			}

			// calculates the inverse of a matrix (if matrix has no inverse, its first value is incremented slightly to make it invertible)
			static inline Matrix2x2 matrixInverse(Matrix2x2 _m)
			{
				// get determinant of matrix
				float det = matrixDeterminant(_m);
				if (det == 0.0f)
				{
					_m.a += 0.000001f;
					det = matrixDeterminant(_m);
				}
				// pre-divide determinant to save processing time
				det = 1.0f / det;
				// use pre-divided determinant to calculate inverse
				return
				{{{
					( _m.d * det),	(-_m.b * det),
					(-_m.c * det),	( _m.a * det),
				}}};
			}
			static inline Matrix3x3 matrixInverse(Matrix3x3 _m)
			{
				// get determinant of matrix
				float det = matrixDeterminant(_m);
				if (det == 0.0f)
				{
					_m.a += 0.000001f;
					det = matrixDeterminant(_m);
				}
				// get cofactor matrix
				_m = cofactorMatrix(_m);
				// pre-divide determinant to save processing time
				det = 1.0f / det;
				// use pre-divided determinant to calculate inverse
				//   manually transposed so as to use adjugate matrix (ie, transposed cofactor matrix) instead of cofactor matrix
				//   (this could also be done by transposing the cofactor matrix, but that would take more processing time)
				return
				{{{
					(_m.a * det), (_m.d * det), (_m.g * det),
					(_m.b * det), (_m.e * det), (_m.h * det),
					(_m.c * det), (_m.f * det), (_m.i * det),
				}}};
			}

#pragma endregion PRIVATE_MATRIX_FUNCTIONS
#pragma region PRIVATE_DRAW_FUNCTIONS

			// Converts draw instructions to their corresponding result instructions, then sorts them into result blocks for later flushing to the result canvas
			void processDrawInstructions_Deferred(const DrawInstruction* _drawInstructions, const unsigned short _numInstructions)
			{
				// iterate through draw instructions
				for (unsigned short i = 0; i < _numInstructions; ++i)
				{
					// store draw instruction for neatness
					const DrawInstruction& drawInstr = static_cast<DrawInstruction>(_drawInstructions[i]);
					// process instruction depending on bit flags
					if (drawInstr.flags & RESULT_INSTRUCTION_FORMAT::TSF)
						processDrawInstruction_Deferred_Transformed(drawInstr);
					else
						processDrawInstruction_Deferred_Basic(drawInstr);
				} // end iterate through draw instructions
			}
			inline void processDrawInstruction_Deferred_Basic(const DrawInstruction& _drawInstr)
			{
				// store instruction's tile for neatness
				TileDefinition& tile = m_tiles[_drawInstr.tile_id];

				// ignore instructions that are completely outside result
				if (   _drawInstr.t[0] + tile.w - 1 < 0			// instruction is beyond left edge
					|| _drawInstr.t[0] >= m_result.w_image		// instruction is beyond right edge
					|| _drawInstr.t[1] + tile.h - 1 < 0			// instruction is beyond top edge
					|| _drawInstr.t[1] >= m_result.h_image)		// instruction is beyond bottom edge
					return;

				// convert draw instruction origin to integer form (using signed ints preserves the full range of an unsigned short, but allows the value to be negative)
				int draw_x_result = static_cast<int>(floorf(_drawInstr.t[0]));
				int draw_y_result = static_cast<int>(floorf(_drawInstr.t[1]));

				// store tile's source for neatness
				Source& src = m_sources[tile.source_id];
				// get height of source (depends on source's data format)
				unsigned short src_blockheight = getSourceBlockHeight(src.format);
				// calculate source blocks in source that instruction overlaps
				unsigned short src_blockMinX = tile.x / SOURCE_BLOCK_WIDTH;
				unsigned short src_blockMinY = tile.y / src_blockheight;
				unsigned short src_blockMaxX = (tile.x + tile.w - 1) / SOURCE_BLOCK_WIDTH;
				unsigned short src_blockMaxY = (tile.y + tile.h - 1) / src_blockheight;
				// iterate through overlapped source blocks
				for (unsigned short src_blockY = src_blockMinY; src_blockY <= src_blockMaxY; ++src_blockY)
					for (unsigned short src_blockX = src_blockMinX; src_blockX <= src_blockMaxX; ++src_blockX)
					{
						// calculate origin of source block in pixels
						unsigned short srcBlock_x_source = src_blockX * SOURCE_BLOCK_WIDTH;
						unsigned short srcBlock_y_source = src_blockY * src_blockheight;
						// calculate origin of subtile in source
						unsigned short subtile_x_source = tile.x < srcBlock_x_source ? srcBlock_x_source : tile.x; // if tile's x origin is not within block, store block's x origin as x; otherwise, store tile's x origin
						unsigned short subtile_y_source = tile.y < srcBlock_y_source ? srcBlock_y_source : tile.y; // same on y
						// calculate maximum x/y of subtile in source
						unsigned short subtile_xMax_source = (tile.x + tile.w) < (srcBlock_x_source + SOURCE_BLOCK_WIDTH)	? tile.x + tile.w	: srcBlock_x_source + SOURCE_BLOCK_WIDTH; // if tile's right edge is within block, store tile's edge; otherwise, store block's edge
						unsigned short subtile_yMax_source = (tile.y + tile.h) < (srcBlock_y_source + src_blockheight)		? tile.y + tile.h	: srcBlock_y_source + src_blockheight; // same on y
						// calculate width and height of subtile
						unsigned short subtile_w_source = subtile_xMax_source - subtile_x_source;
						unsigned short subtile_h_source = subtile_yMax_source - subtile_y_source;
						// calculate origin in result (instruction's result origin + difference between result instruction's source origin and data region's source origin)
						/*
							{tx}	-> tile x origin in result										(draw_x_result)
							{ty}	-> tile y origin in result										(draw_y_result)
							{dxs}	-> difference between subtile and tile x origin in source		(subtile_x_source - tile.x)
							{dys}	-> difference between subtile and tile y origin in source		(subtile_y_source - tile.y)
							{tw}	-> tile width													(tile.w)
							{th}	-> tile height													(tile.h)
							{sw}	-> subtile width												(subtile_w_source)
							{sh}	-> subtile height												(subtile_h_source)

							subtile x result =
								ROTATE ?
									MIRROR_HORIZONTAL ?
										{tx} + {dys}
									:
										{tx} + {th} - {dys} - {sh}
								:
									MIRROR_HORIZONTAL ?
										{tx} + {tw} - {dxs} - {sw}
									:
										{tx} + {dxs}

							subtile y result =
								ROTATE ?
									MIRROR_VERTICAL ?
										{ty} + {tw} - {dxs} - {sw}
									:
										{ty} + {dxs}
								:
									MIRROR_VERTICAL ?
										{ty} + {th} - {dys} - {sh}
									:
										{ty} + {dys}
						*/
						int subtile_x_result = draw_x_result;
						int subtile_y_result = draw_y_result;
						if (_drawInstr.flags & DrawOptions::ROTATE)
						{
							subtile_x_result += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? (subtile_y_source - tile.y)								: tile.h - (subtile_y_source - tile.y) - subtile_h_source;
							subtile_y_result += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)	? tile.w - (subtile_x_source - tile.x) - subtile_w_source	: (subtile_x_source - tile.x);
						}
						else
						{
							subtile_x_result += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? tile.w - (subtile_x_source - tile.x) - subtile_w_source	: (subtile_x_source - tile.x);
							subtile_y_result += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)	? tile.h - (subtile_y_source - tile.y) - subtile_h_source	: (subtile_y_source - tile.y);
						}

						// store subtile width and height after basic transformations (swapped if tile is rotated)
						unsigned short subtile_w_result = (_drawInstr.flags & DrawOptions::ROTATE) ? subtile_h_source : subtile_w_source;
						unsigned short subtile_h_result = (_drawInstr.flags & DrawOptions::ROTATE) ? subtile_w_source : subtile_h_source;
						// if subtile is completely outside result, skip it
						if (   subtile_x_result + subtile_w_result - 1 < 0	// subtile is beyond left edge
							|| subtile_x_result > m_result.w_image			// subtile is beyond right edge
							|| subtile_y_result + subtile_h_result - 1 < 0	// subtile is beyond top edge
							|| subtile_y_result > m_result.h_image)			// subtile is beyond bottom edge
							continue;

						// calculate result blocks overlapped by subtile (dimensions swapped if tile is rotated)
						unsigned short res_blockMinX = G_CLAMP_MIN(static_cast<short>(subtile_x_result / RESULT_BLOCK_WIDTH), 0);
						unsigned short res_blockMaxX = G_CLAMP_MAX(static_cast<short>((subtile_x_result + subtile_w_result - 1) / RESULT_BLOCK_HEIGHT), m_result.w_data - 1);
						unsigned short res_blockMinY = G_CLAMP_MIN(static_cast<short>(subtile_y_result / RESULT_BLOCK_HEIGHT), 0);
						unsigned short res_blockMaxY = G_CLAMP_MAX(static_cast<short>((subtile_y_result + subtile_h_result - 1) / RESULT_BLOCK_WIDTH), m_result.h_data - 1);

						// iterate through overlapped result blocks
						for (unsigned short res_blockY = res_blockMinY; res_blockY <= res_blockMaxY; ++res_blockY)
							for (unsigned short res_blockX = res_blockMinX; res_blockX <= res_blockMaxX; ++res_blockX)
							{
								// store result block by reference for neatness
								ResultBlock& resBlock = m_result.data[res_blockX + res_blockY * m_result.w_data];

								// reject new instructions if this block's bin is full
								if (resBlock.instruction_count >= INSTRUCTIONS_PER_RESULT_BLOCK)
									continue;

								// calculate result block origin in pixels
								unsigned short resBlock_x_result = res_blockX * RESULT_BLOCK_WIDTH;
								unsigned short resBlock_y_result = res_blockY * RESULT_BLOCK_HEIGHT;

								// create result instruction to sort into result block bins
								ResultInstruction resInstr = {};

								// store flags, source, and source block
								resInstr.flags = _drawInstr.flags;
								resInstr.source_id = tile.source_id;
								resInstr.source_block_id = src_blockX + (src_blockY * src.w_data);
								// calculate origin of data region inside block
								resInstr.x_result = subtile_x_result < resBlock_x_result ? resBlock_x_result : subtile_x_result < 0 ? 0 : subtile_x_result; // if instruction's x origin is not within block, store block's x origin as x; otherwise, store instruction's x origin (unless it is negative, in which case store 0)
								resInstr.y_result = subtile_y_result < resBlock_y_result ? resBlock_y_result : subtile_y_result < 0 ? 0 : subtile_y_result; // same on y
								// calculate maximum x/y of data region
								unsigned short resInstr_xMax_result = (subtile_x_result + subtile_w_result) < (resBlock_x_result + RESULT_BLOCK_WIDTH)	? subtile_x_result + subtile_w_result : resBlock_x_result + RESULT_BLOCK_WIDTH; // if instruction's right edge is within block, store instruction's edge; otherwise, store block's edge
								unsigned short resInstr_yMax_result = (subtile_y_result + subtile_h_result) < (resBlock_y_result + RESULT_BLOCK_HEIGHT)	? subtile_y_result + subtile_h_result : resBlock_y_result + RESULT_BLOCK_HEIGHT; // same on y
								// calculate width and height of instruction data region
								resInstr.w_result = resInstr_xMax_result - resInstr.x_result;
								resInstr.h_result = resInstr_yMax_result - resInstr.y_result;
								// for basic instructions, source w/h = result w/h (swapped if transformed)
								resInstr.w_source = (resInstr.flags & DrawOptions::ROTATE) ? resInstr.h_result : resInstr.w_result;
								resInstr.h_source = (resInstr.flags & DrawOptions::ROTATE) ? resInstr.w_result : resInstr.h_result;
								// calculate origin of result instruction in source
								/*
									{sx}	-> subtile x origin in source												(subtile_x_source)
									{sy}	-> subtile y origin in source												(subtile_y_source)
									{dxr}	-> difference between result instruction and subtile x origin in result		(resInstr.x_result - subtile_x_result)
									{dyr}	-> difference between result instruction and subtile y origin in result		(resInstr.y_result - subtile_y_result)
									{sw}	-> subtile width															(subtile_w_source)
									{sh}	-> subtile height															(subtile_h_source)
									{rw}	-> result instruction width													(resInstr.w_source)
									{rh}	-> result instruction height												(resInstr.h_source)

									res instr x source =
										ROTATE ?
											MIRROR_VERTICAL ?
												{sx} + {sw} - {dyr} - {rh}
											:
												{sx} + {dyr}
										:
											MIRROR_HORIZONTAL ?
												{sx} + {sw} - {dxr} - {rw}
											:
												{sx} + {dxr}

									res instr y source =
										ROTATE ?
											MIRROR_HORIZONTAL ?
												{sy} + {dxr}
											:
												{sy} + {sh} - {dxr} - {rw}
										:
											MIRROR_VERTICAL ?
												{sy} + {sh} - {dyr} - {rh}
											:
												{sy} + {dyr}
								*/
								resInstr.x_source = subtile_x_source;
								resInstr.y_source = subtile_y_source;
								if (_drawInstr.flags & DrawOptions::ROTATE)
								{
									resInstr.x_source += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)		? subtile_w_source - (resInstr.y_result - subtile_y_result) - resInstr.h_result	: (resInstr.y_result - subtile_y_result);
									resInstr.y_source += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? (resInstr.x_result - subtile_x_result)										: subtile_h_source - (resInstr.x_result - subtile_x_result) - resInstr.w_result;
								}
								else
								{
									resInstr.x_source += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? subtile_w_source - (resInstr.x_result - subtile_x_result) - resInstr.w_result	: (resInstr.x_result - subtile_x_result);
									resInstr.y_source += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)		? subtile_h_source - (resInstr.y_result - subtile_y_result) - resInstr.h_result	: (resInstr.y_result - subtile_y_result);
								}

								// adjust origins to be relative to block origins
								resInstr.x_source -= srcBlock_x_source;
								resInstr.y_source -= srcBlock_y_source;
								resInstr.x_result -= resBlock_x_result;
								resInstr.y_result -= resBlock_y_result;

								// calculate layer override value (draw instruction layer value + z value)
								resInstr.layer_override = Layer(_drawInstr.layer + _drawInstr.t[2]);
								// store stencil override value
								resInstr.stencil_override = _drawInstr.stencil;
								// store mask values
								resInstr.mask_color = tile.mask_color;
								resInstr.mask_layer = tile.mask_layer;
								resInstr.mask_stencil = tile.mask_stencil;

								// add new instruction into bin
								resBlock.instruction_bin[resBlock.instruction_count++] = resInstr;
								// insertion-sort new instruction into place
								for (unsigned short i = resBlock.instruction_count - 1; i > 0; --i)
								{
									// swap current item and next item if the next item's layer is lower
									if (resBlock.instruction_bin[i - 1].layer_override < resBlock.instruction_bin[i].layer_override)
									{
										// repurpose local instruction for swapping since new data is now in the bin
										resInstr = resBlock.instruction_bin[i - 1];
										resBlock.instruction_bin[i - 1] = resBlock.instruction_bin[i];
										resBlock.instruction_bin[i] = resInstr;
									}
									// otherwise, sorting is finished
									else
									{
										break;
									}
								} // end sort new instruction into place
							} // end iterate through overlapped result blocks
				} // end iterate through overlapped source blocks
			}
			inline void processDrawInstruction_Deferred_Transformed(const DrawInstruction& _drawInstr)
			{
				// store instruction's tile for neatness
				TileDefinition& tile = m_tiles[_drawInstr.tile_id];

				// store position in result, transform matrix (transposed from row-major to col-major), and pivot
				Vector2 draw_pos_result = {{{ _drawInstr.t[0], _drawInstr.t[1] }}};
				Matrix2x2 transformMatrix =
				{{{
					_drawInstr.m[0][0], _drawInstr.m[1][0],
					_drawInstr.m[0][1], _drawInstr.m[1][1],
				}}};
				Vector2 pivot = {{{ _drawInstr.p[0], _drawInstr.p[1] }}};

				// calculate size of tile after transformation
				// apply inverse transform matrix to tile corners
				Quad footprint =
				{
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f,								0.0f }}}),								// top left
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f + static_cast<float>(tile.w),	0.0f }}}),								// top right
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f + static_cast<float>(tile.w), 0.0f + static_cast<float>(tile.h) }}}), // bottom right
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f,								0.0f + static_cast<float>(tile.h) }}}),	// bottom left
				};

				// calculate min/max positions of corners after transformation
				//   this is essentially an AABB enclosing the draw with the draw's top-left corner fixed at the origin
				Vector2 draw_bounds_min =
				{{{
					static_cast<float>(G_SMALLER(footprint.a.x, G_SMALLER(footprint.b.x, G_SMALLER(footprint.c.x, footprint.d.x)))),
					static_cast<float>(G_SMALLER(footprint.a.y, G_SMALLER(footprint.b.y, G_SMALLER(footprint.c.y, footprint.d.y)))),
				}}};
				Vector2 draw_bounds_max =
				{{{
					static_cast<float>(G_LARGER(footprint.a.x, G_LARGER(footprint.b.x, G_LARGER(footprint.c.x, footprint.d.x)))),
					static_cast<float>(G_LARGER(footprint.a.y, G_LARGER(footprint.b.y, G_LARGER(footprint.c.y, footprint.d.y)))),
				}}};

				// use bounds to calculate size
				Vector2 draw_size_result =
				{{{
					floorf(draw_bounds_max.x - draw_bounds_min.x),
					floorf(draw_bounds_max.y - draw_bounds_min.y),
				}}};

				// calculate transformed pivot
				Vector2 pivot_transformed = matrixXVector_CM(transformMatrix, pivot);
				// calculate vector to move transformed pivot back to original position
				Vector2 pivot_reset =
				{{{
					pivot.x - pivot_transformed.x,
					pivot.y - pivot_transformed.y,
				}}};

				// apply pivot to draw position; the pivot is a fixed point that the transformation happens about, which means that the tile's position needs to be adjusted by the
				//	vector from the transformed pivot to the untransformed pivot in order to move the new position of the pivot back to its original position
				Vector2 draw_pos_result_adjusted =
				{{{
					floorf(draw_pos_result.x + pivot_reset.x),
					floorf(draw_pos_result.y + pivot_reset.y),
				}}};
				// add adjusted draw position to footprint quad
				footprint.a.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.b.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.c.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.d.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.a.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.b.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.c.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.d.y += static_cast<int>(draw_pos_result_adjusted.y);

				// calculate inverse of transform matrix and translation vector (adjusted by the vector from the transformed pivot to the original pivot, to keep the pivot fixed in place after transformation)
				Matrix3x3 mInverse = matrixInverse({{{
					_drawInstr.m[0][0],				_drawInstr.m[1][0],				0.0f,
					_drawInstr.m[0][1],				_drawInstr.m[1][1],				0.0f,
					draw_pos_result_adjusted.x,		draw_pos_result_adjusted.y,		1.0f,
				}}});

				// the draw's position needs to be adjusted further by the minimum x/y bounds of the AABB to account for the other corners' positions changing relative to the top-left corner
				draw_pos_result_adjusted.x += draw_bounds_min.x;
				draw_pos_result_adjusted.y += draw_bounds_min.y;

				// calculate result blocks overlapped by draw
				short res_blockMinX = G_CLAMP_MIN(static_cast<short>( draw_pos_result_adjusted.x / RESULT_BLOCK_WIDTH), 0);
				short res_blockMaxX = G_CLAMP_MAX(static_cast<short>((draw_pos_result_adjusted.x + draw_size_result.x - 1) / RESULT_BLOCK_WIDTH), m_result.w_data - 1); // -1 because this is a pixel-space calculation
				short res_blockMinY = G_CLAMP_MIN(static_cast<short>( draw_pos_result_adjusted.y / RESULT_BLOCK_HEIGHT), 0);
				short res_blockMaxY = G_CLAMP_MAX(static_cast<short>((draw_pos_result_adjusted.y + draw_size_result.y - 1) / RESULT_BLOCK_HEIGHT), m_result.h_data - 1); // -1 because this is a pixel-space calculation

				// check overlapped blocks to determine if draw is within result; if not, skip it
				if (   res_blockMaxX < 0					// beyond left edge
					|| res_blockMinX >= m_result.w_data		// beyond right edge
					|| res_blockMaxY < 0					// beyond top edge
					|| res_blockMinY >= m_result.h_data)	// beyond bottom edge
					return;

				// create instruction to store in result blocks
				ResultInstruction resInstr =
				{
					_drawInstr.flags,												// flags
					tile.source_id,													// source id
					(std::numeric_limits<unsigned short>::max)(),					// source block id (not used by transformed instructions)
					tile.x,															// x source
					tile.y,															// y source
					tile.w,															// w source
					tile.h,															// h source
					static_cast<unsigned short>(draw_pos_result.x),					// x result
					static_cast<unsigned short>(draw_pos_result.y),					// y result
					static_cast<unsigned short>(draw_size_result.x),				// w result
					static_cast<unsigned short>(draw_size_result.y),				// h result
					Layer(G_CLAMP(_drawInstr.layer + _drawInstr.t[2], 0, 255)),		// layer override
					_drawInstr.stencil,												// stencil override
					tile.mask_color,												// mask color
					tile.mask_layer,												// mask layer
					tile.mask_stencil,												// mask stencil
					{},																// pad
					Matrix2x2{{{ mInverse.vectors[0].x, mInverse.vectors[0].y,
						mInverse.vectors[1].x, mInverse.vectors[1].y }}},			// m inverse (top-left corner of inverse matrix)
					Vector2{{{ mInverse.vectors[2].x, mInverse.vectors[2].y }}},	// t inverse (third vector of inverse matrix)
					footprint,														// footprint
				};

				// iterate through overlapped result blocks
				for (unsigned short res_blockY = res_blockMinY; res_blockY <= res_blockMaxY; ++res_blockY)
					for (unsigned short res_blockX = res_blockMinX; res_blockX <= res_blockMaxX; ++res_blockX)
					{
						// store result block by reference for neatness
						ResultBlock& resBlock = m_result.data[res_blockX + res_blockY * m_result.w_data];
						// reject new instructions if this block's bin is full
						if (resBlock.instruction_count >= INSTRUCTIONS_PER_RESULT_BLOCK)
							continue;
						// otherwise, insertion-sort new instruction into place
						ResultInstruction sort;
						resBlock.instruction_bin[resBlock.instruction_count++] = resInstr;
						for (unsigned short i = resBlock.instruction_count - 1; i > 0; --i)
						{
							// swap current item and next item if the next item's layer is lower
							if (resBlock.instruction_bin[i - 1].layer_override < resBlock.instruction_bin[i].layer_override)
							{
								sort = resBlock.instruction_bin[i - 1];
								resBlock.instruction_bin[i - 1] = resBlock.instruction_bin[i];
								resBlock.instruction_bin[i] = sort;
							}
							// otherwise, sorting is finished
							else
							{
								break;
							}
						} // end sort new instruction into bin
					} // end iterate through overlapped result blocks
			}

			// Pre-sorts draw instructions, converts them to their corresponding result instructions, then flushes them to the result canvas immediately
			void processDrawInstructions_Immediate(DrawInstruction* _drawInstructions, const unsigned short _numInstructions)
			{
				// reset instruction counts (discards any outstanding instructions)
				for (unsigned short i = 0; i < m_result.size_data; ++i)
					m_result.data[i].instruction_count = 0;

				// pre-sort instructions to ensure they get drawn correctly
				std::vector<unsigned short> sortedIndices;
				for (unsigned short i = 0; i < _numInstructions; ++i)
				{
					// add new index to end of list
					sortedIndices.push_back(i);
					// determine final layer value of instruction to sort
					Layer sort_layer = Layer(_drawInstructions[i].layer + _drawInstructions[i].t[2]);
					// insertion sort instruction's index into place
					for (unsigned short k = static_cast<unsigned short>(sortedIndices.size()) - 1; k > 0; --k)
					{
						// determine final layer value of instruction to compare to
						Layer compare_layer = Layer(_drawInstructions[sortedIndices[k - 1]].layer + _drawInstructions[sortedIndices[k - 1]].t[2]);
						// if previous instruction's layer is lower or equal to this one's, sorting is finished
						if (sort_layer <= compare_layer)
							break;
						// otherwise, swap this instruction's with the previous one's index and repeat
						unsigned short temp = sortedIndices[k - 1];
						sortedIndices[k - 1] = sortedIndices[k];
						sortedIndices[k] = temp;
					}
				}

				// process & draw instructions
				processDrawInstructions_Immediate_recurse(_drawInstructions, &sortedIndices[0], _numInstructions);
			}
			void processDrawInstructions_Immediate_recurse(const DrawInstruction* _drawInstructions, const unsigned short* _sortedIndices, const unsigned short _numInstructions)
			{
				// whether the result has filled up and needs to flush instructions and recursively process the rest
				bool flushAndRepeat = false;

				// iterate through draw instructions and process them
				for (unsigned short i = 0; i < _numInstructions; ++i)
				{
					// store draw instruction for neatness
					const DrawInstruction& drawInstr = static_cast<DrawInstruction>(_drawInstructions[_sortedIndices[i]]);
					// process instruction depending on bit flags
					if (drawInstr.flags & RESULT_INSTRUCTION_FORMAT::TSF)
						flushAndRepeat = processDrawInstruction_Immediate_Transformed(drawInstr);
					else
						flushAndRepeat = processDrawInstruction_Immediate_Basic(drawInstr);
					
					// if a bin is full, flush current instructions, then process any remaining draw instructions
					//   this happens between draw instructions to avoid problems with trying to pause mid-processing of a draw instruction
					//   this is possible because each draw instruction can add at most 4 instructions into any given result block,
					//     so by making the flush threshold at least 4, bins can be filled to between 3 and 0 instructions of their capacity without overflowing
					if (flushAndRepeat)
					{
						Flush();
						// if there are instructions remaining, call this function again, but starting from the next instruction
						if (i < _numInstructions)
							processDrawInstructions_Immediate_recurse(_drawInstructions, &_sortedIndices[i + 1], _numInstructions - i - 1);
						// all instructions have now been processed recursively, so there is no reason to continue in the loop
						break;
					}
				} // end iterate through draw instructions
				// if this is the last iteration and instructions are outstanding, flush the last batch of instructions
				if (!flushAndRepeat)
					Flush();
			}
			// returns true if draw instruction caused a result block to fill up
			inline bool processDrawInstruction_Immediate_Basic(const DrawInstruction& _drawInstr)
			{
				bool resultBlockFull = false;

				// store instruction's tile for neatness
				TileDefinition& tile = m_tiles[_drawInstr.tile_id];

				// ignore instructions that are completely outside result
				if (   _drawInstr.t[0] + tile.w - 1 < 0		// instruction is beyond left edge
					|| _drawInstr.t[0] >= m_result.w_image	// instruction is beyond right edge
					|| _drawInstr.t[1] + tile.h - 1 < 0		// instruction is beyond top edge
					|| _drawInstr.t[1] >= m_result.h_image)	// instruction is beyond bottom edge
					return false;

				// convert draw instruction origin to integer form (using signed ints preserves the full range of an unsigned short, but allows the value to be negative)
				int draw_x_result = static_cast<int>(floorf(_drawInstr.t[0]));
				int draw_y_result = static_cast<int>(floorf(_drawInstr.t[1]));

				// store tile's source for neatness
				Source& src = m_sources[tile.source_id];
				// get height of source (depends on source's data format)
				unsigned short src_blockheight = getSourceBlockHeight(src.format);
				// calculate source blocks in source that instruction overlaps
				unsigned short src_blockMinX = tile.x / SOURCE_BLOCK_WIDTH;
				unsigned short src_blockMinY = tile.y / src_blockheight;
				unsigned short src_blockMaxX = (tile.x + tile.w - 1) / SOURCE_BLOCK_WIDTH;
				unsigned short src_blockMaxY = (tile.y + tile.h - 1) / src_blockheight;
				// iterate through overlapped source blocks
				for (unsigned short src_blockY = src_blockMinY; src_blockY <= src_blockMaxY; ++src_blockY)
					for (unsigned short src_blockX = src_blockMinX; src_blockX <= src_blockMaxX; ++src_blockX)
					{
						// calculate origin of source block in pixels
						unsigned short srcBlock_x_source = src_blockX * SOURCE_BLOCK_WIDTH;
						unsigned short srcBlock_y_source = src_blockY * src_blockheight;
						// calculate origin of subtile in source
						unsigned short subtile_x_source = tile.x < srcBlock_x_source ? srcBlock_x_source : tile.x; // if tile's x origin is not within block, store block's x origin as x; otherwise, store tile's x origin
						unsigned short subtile_y_source = tile.y < srcBlock_y_source ? srcBlock_y_source : tile.y; // same on y
						// calculate maximum x/y of subtile in source
						unsigned short subtile_xMax_source = (tile.x + tile.w) < (srcBlock_x_source + SOURCE_BLOCK_WIDTH)	? tile.x + tile.w	: srcBlock_x_source + SOURCE_BLOCK_WIDTH; // if tile's right edge is within block, store tile's edge; otherwise, store block's edge
						unsigned short subtile_yMax_source = (tile.y + tile.h) < (srcBlock_y_source + src_blockheight)		? tile.y + tile.h	: srcBlock_y_source + src_blockheight; // same on y
						// calculate width and height of subtile
						unsigned short subtile_w_source = subtile_xMax_source - subtile_x_source;
						unsigned short subtile_h_source = subtile_yMax_source - subtile_y_source;
						// calculate origin in result (instruction's result origin + difference between result instruction's source origin and data region's source origin)
						// (see processDrawInstructions_Deferred for summary of how this operation happens)
						int subtile_x_result = draw_x_result;
						int subtile_y_result = draw_y_result;
						if (_drawInstr.flags & DrawOptions::ROTATE)
						{
							subtile_x_result += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? (subtile_y_source - tile.y)								: tile.h - (subtile_y_source - tile.y) - subtile_h_source;
							subtile_y_result += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)	? tile.w - (subtile_x_source - tile.x) - subtile_w_source	: (subtile_x_source - tile.x);
						}
						else
						{
							subtile_x_result += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? tile.w - (subtile_x_source - tile.x) - subtile_w_source	: (subtile_x_source - tile.x);
							subtile_y_result += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)	? tile.h - (subtile_y_source - tile.y) - subtile_h_source	: (subtile_y_source - tile.y);
						}

						// store subtile width and height (swapped if tile is rotated)
						unsigned short subtile_w_result = (_drawInstr.flags & DrawOptions::ROTATE) ? subtile_h_source : subtile_w_source;
						unsigned short subtile_h_result = (_drawInstr.flags & DrawOptions::ROTATE) ? subtile_w_source : subtile_h_source;
						// if subtile is completely outside result, skip it
						if (   subtile_x_result + subtile_w_result - 1 < 0	// subtile is beyond left edge
							|| subtile_x_result > m_result.w_image			// subtile is beyond right edge
							|| subtile_y_result + subtile_h_result - 1 < 0	// subtile is beyond top edge
							|| subtile_y_result > m_result.h_image)			// subtile is beyond bottom edge
							continue;

						// calculate result blocks overlapped by subtile (dimensions swapped if tile is rotated)
						unsigned short res_blockMinX = G_CLAMP_MIN(static_cast<short>(subtile_x_result / RESULT_BLOCK_WIDTH), 0);
						unsigned short res_blockMaxX = G_CLAMP_MAX(static_cast<short>((subtile_x_result + subtile_w_result - 1) / RESULT_BLOCK_HEIGHT), m_result.w_data - 1);
						unsigned short res_blockMinY = G_CLAMP_MIN(static_cast<short>(subtile_y_result / RESULT_BLOCK_HEIGHT), 0);
						unsigned short res_blockMaxY = G_CLAMP_MAX(static_cast<short>((subtile_y_result + subtile_h_result - 1) / RESULT_BLOCK_WIDTH), m_result.h_data - 1);

						// iterate through overlapped result blocks
						for (unsigned short res_blockY = res_blockMinY; res_blockY <= res_blockMaxY; ++res_blockY)
							for (unsigned short res_blockX = res_blockMinX; res_blockX <= res_blockMaxX; ++res_blockX)
							{
								// store result block by reference for neatness
								ResultBlock& resBlock = m_result.data[res_blockX + res_blockY * m_result.w_data];
								// calculate result block origin in pixels
								unsigned short resBlock_x_result = res_blockX * RESULT_BLOCK_WIDTH;
								unsigned short resBlock_y_result = res_blockY * RESULT_BLOCK_HEIGHT;

								// create result instruction to sort into result block bins
								ResultInstruction resInstr = {};

								// store flags, source, and source block
								resInstr.flags = _drawInstr.flags;
								resInstr.source_id = tile.source_id;
								resInstr.source_block_id = src_blockX + (src_blockY * src.w_data);
								// calculate origin of data region inside block
								resInstr.x_result = subtile_x_result < resBlock_x_result ? resBlock_x_result : subtile_x_result < 0 ? 0 : subtile_x_result; // if instruction's x origin is not within block, store block's x origin as x; otherwise, store instruction's x origin (unless it is negative, in which case store 0)
								resInstr.y_result = subtile_y_result < resBlock_y_result ? resBlock_y_result : subtile_y_result < 0 ? 0 : subtile_y_result; // same on y
								// calculate maximum x/y of data region
								unsigned short resInstr_xMax_result = (subtile_x_result + subtile_w_result) < (resBlock_x_result + RESULT_BLOCK_WIDTH)	? subtile_x_result + subtile_w_result : resBlock_x_result + RESULT_BLOCK_WIDTH; // if instruction's right edge is within block, store instruction's edge; otherwise, store block's edge
								unsigned short resInstr_yMax_result = (subtile_y_result + subtile_h_result) < (resBlock_y_result + RESULT_BLOCK_HEIGHT)	? subtile_y_result + subtile_h_result : resBlock_y_result + RESULT_BLOCK_HEIGHT; // same on y
								// calculate width and height of instruction data region
								resInstr.w_result = resInstr_xMax_result - resInstr.x_result;
								resInstr.h_result = resInstr_yMax_result - resInstr.y_result;
								// for basic instructions, source w/h = result w/h (swapped if transformed)
								resInstr.w_source = (resInstr.flags & DrawOptions::ROTATE) ? resInstr.h_result : resInstr.w_result;
								resInstr.h_source = (resInstr.flags & DrawOptions::ROTATE) ? resInstr.w_result : resInstr.h_result;
								// (see processDrawInstructions_Deferred for summary of how this operation happens)
								resInstr.x_source = subtile_x_source;
								resInstr.y_source = subtile_y_source;
								if (_drawInstr.flags & DrawOptions::ROTATE)
								{
									resInstr.x_source += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)		? subtile_w_source - (resInstr.y_result - subtile_y_result) - resInstr.h_result	: (resInstr.y_result - subtile_y_result);
									resInstr.y_source += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? (resInstr.x_result - subtile_x_result)										: subtile_h_source - (resInstr.x_result - subtile_x_result) - resInstr.w_result;
								}
								else
								{
									resInstr.x_source += (_drawInstr.flags & DrawOptions::MIRROR_HORIZONTAL)	? subtile_w_source - (resInstr.x_result - subtile_x_result) - resInstr.w_result	: (resInstr.x_result - subtile_x_result);
									resInstr.y_source += (_drawInstr.flags & DrawOptions::MIRROR_VERTICAL)		? subtile_h_source - (resInstr.y_result - subtile_y_result) - resInstr.h_result	: (resInstr.y_result - subtile_y_result);
								}
								// adjust origins to be relative to block origins
								resInstr.x_source -= srcBlock_x_source;
								resInstr.y_source -= srcBlock_y_source;
								resInstr.x_result -= resBlock_x_result;
								resInstr.y_result -= resBlock_y_result;

								// calculate layer override value (draw instruction layer value + z value)
								resInstr.layer_override = Layer(_drawInstr.layer + _drawInstr.t[2]);
								// store stencil override value
								resInstr.stencil_override = _drawInstr.stencil;
								// store mask values
								resInstr.mask_color = tile.mask_color;
								resInstr.mask_layer = tile.mask_layer;
								resInstr.mask_stencil = tile.mask_stencil;

								// add new instruction into bin (no sorting needed due to pre-sort)
								resBlock.instruction_bin[resBlock.instruction_count++] = resInstr;
								// if result block's bin has less space left than the flush threshold, set flag on to flush instructions
								if (INSTRUCTIONS_PER_RESULT_BLOCK - resBlock.instruction_count < RESULT_FLUSH_THRESHOLD)
									resultBlockFull = true;
							} // end iterate through overlapped result blocks
					} // end iterate through overlapped source blocks
				return resultBlockFull;
			}
			// returns true if draw instruction caused a result block to fill up
			inline bool processDrawInstruction_Immediate_Transformed(const DrawInstruction& _drawInstr)
			{
				bool resultBlockFull = false;

				// store instruction's tile for neatness
				TileDefinition& tile = m_tiles[_drawInstr.tile_id];

				// store position in result, transform matrix (transposed from row-major to col-major), and pivot
				Vector2 draw_pos_result = {{{ _drawInstr.t[0], _drawInstr.t[1] }}};
				Matrix2x2 transformMatrix =
				{{{
					_drawInstr.m[0][0], _drawInstr.m[1][0],
					_drawInstr.m[0][1], _drawInstr.m[1][1],
				}}};
				Vector2 pivot = {{{ _drawInstr.p[0], _drawInstr.p[1] }}};

				// calculate size of tile after transformation
				// apply inverse transform matrix to tile corners
				Quad footprint =
				{
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f,								0.0f }}}),								// top left
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f + static_cast<float>(tile.w),	0.0f }}}),								// top right
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f + static_cast<float>(tile.w),	0.0f + static_cast<float>(tile.h) }}}),	// bottom right
					matrixXVector_CM(transformMatrix, Vector2{{{ 0.0f,								0.0f + static_cast<float>(tile.h) }}}),	// bottom left
				};

				// calculate min/max positions of corners after transformation
				//   this is essentially an AABB enclosing the draw with the draw's top-left corner fixed at the origin
				Vector2 draw_bounds_min =
				{{{
					static_cast<float>(G_SMALLER(footprint.a.x, G_SMALLER(footprint.b.x, G_SMALLER(footprint.c.x, footprint.d.x)))),
					static_cast<float>(G_SMALLER(footprint.a.y, G_SMALLER(footprint.b.y, G_SMALLER(footprint.c.y, footprint.d.y)))),
				}}};
				Vector2 draw_bounds_max =
				{{{
					static_cast<float>(G_LARGER(footprint.a.x, G_LARGER(footprint.b.x, G_LARGER(footprint.c.x, footprint.d.x)))),
					static_cast<float>(G_LARGER(footprint.a.y, G_LARGER(footprint.b.y, G_LARGER(footprint.c.y, footprint.d.y)))),
				}}};

				// use bounds to calculate size
				Vector2 draw_size_result =
				{{{
					floorf(draw_bounds_max.x - draw_bounds_min.x),
					floorf(draw_bounds_max.y - draw_bounds_min.y),
				}}};

				// calculate transformed pivot
				Vector2 pivot_transformed = matrixXVector_CM(transformMatrix, pivot);
				// calculate vector to move transformed pivot back to original position
				Vector2 pivot_reset =
				{{{
					pivot.x - pivot_transformed.x,
					pivot.y - pivot_transformed.y,
				}}};

				// apply pivot to draw position; the pivot is a fixed point that the transformation happens about, which means that the tile's position needs to be adjusted by the
				//	vector from the transformed pivot to the untransformed pivot in order to move the new position of the pivot back to its original position
				Vector2 draw_pos_result_adjusted =
				{{{
					floorf(draw_pos_result.x + pivot_reset.x),
					floorf(draw_pos_result.y + pivot_reset.y),
				}}};
				// add adjusted draw position to footprint quad
				footprint.a.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.b.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.c.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.d.x += static_cast<int>(draw_pos_result_adjusted.x);
				footprint.a.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.b.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.c.y += static_cast<int>(draw_pos_result_adjusted.y);
				footprint.d.y += static_cast<int>(draw_pos_result_adjusted.y);

				// calculate inverse of transform matrix and translation vector (adjusted by the vector from the transformed pivot to the original pivot, to keep the pivot fixed in place after transformation)
				Matrix3x3 mInverse = matrixInverse({{{
					_drawInstr.m[0][0],				_drawInstr.m[1][0],				0.0f,
					_drawInstr.m[0][1],				_drawInstr.m[1][1],				0.0f,
					draw_pos_result_adjusted.x,		draw_pos_result_adjusted.y,		1.0f,
				}}});

				// the draw's position needs to be adjusted further by the minimum x/y bounds of the AABB to account for the other corners' positions changing relative to the top-left corner
				draw_pos_result_adjusted.x += draw_bounds_min.x;
				draw_pos_result_adjusted.y += draw_bounds_min.y;

				// calculate result blocks overlapped by draw
				short res_blockMinX = G_CLAMP_MIN(static_cast<short>( draw_pos_result_adjusted.x / RESULT_BLOCK_WIDTH), 0);
				short res_blockMaxX = G_CLAMP_MAX(static_cast<short>((draw_pos_result_adjusted.x + draw_size_result.x - 1) / RESULT_BLOCK_WIDTH), m_result.w_data - 1); // -1 because this is a pixel-space calculation
				short res_blockMinY = G_CLAMP_MIN(static_cast<short>( draw_pos_result_adjusted.y / RESULT_BLOCK_HEIGHT), 0);
				short res_blockMaxY = G_CLAMP_MAX(static_cast<short>((draw_pos_result_adjusted.y + draw_size_result.y - 1) / RESULT_BLOCK_HEIGHT), m_result.h_data - 1); // -1 because this is a pixel-space calculation

				// check overlapped blocks to determine if draw is within result; if not, skip it
				if (   res_blockMaxX < 0					// beyond left edge
					|| res_blockMinX >= m_result.w_data		// beyond right edge
					|| res_blockMaxY < 0					// beyond top edge
					|| res_blockMinY >= m_result.h_data)	// beyond bottom edge
					return resultBlockFull;

				// create instruction to store in result blocks
				ResultInstruction resInstr =
				{
					_drawInstr.flags,												// flags
					tile.source_id,													// source id
					(std::numeric_limits<unsigned short>::max)(),					// source block id (not used by transformed instructions)
					tile.x,															// x source
					tile.y,															// y source
					tile.w,															// w source
					tile.h,															// h source
					static_cast<unsigned short>(draw_pos_result.x),					// x result
					static_cast<unsigned short>(draw_pos_result.y),					// y result
					static_cast<unsigned short>(draw_size_result.x),				// w result
					static_cast<unsigned short>(draw_size_result.y),				// h result
					Layer(G_CLAMP(_drawInstr.layer + _drawInstr.t[2], 0, 255)),		// layer override
					_drawInstr.stencil,												// stencil override
					tile.mask_color,												// mask color
					tile.mask_layer,												// mask layer
					tile.mask_stencil,												// mask stencil
					{},																// pad
					Matrix2x2{{{ mInverse.vectors[0].x, mInverse.vectors[0].y,
						mInverse.vectors[1].x, mInverse.vectors[1].y }}},			// m inverse (top-left corner of inverse matrix)
					Vector2{{{ mInverse.vectors[2].x, mInverse.vectors[2].y }}},	// t inverse (third vector of inverse matrix)
					footprint,														// footprint
				};

				// iterate through overlapped result blocks
				for (unsigned short res_blockY = res_blockMinY; res_blockY <= res_blockMaxY; ++res_blockY)
					for (unsigned short res_blockX = res_blockMinX; res_blockX <= res_blockMaxX; ++res_blockX)
					{
						// store result block by reference for neatness
						ResultBlock& resBlock = m_result.data[res_blockX + res_blockY * m_result.w_data];
						// add new instruction into bin (no sorting needed due to pre-sort)
						resBlock.instruction_bin[resBlock.instruction_count++] = resInstr;
						// if result block's bin has less space left than the flush threshold, set flag on to flush instructions
						if (INSTRUCTIONS_PER_RESULT_BLOCK - resBlock.instruction_count < RESULT_FLUSH_THRESHOLD)
							resultBlockFull = true;
					} // end iterate through overlapped result blocks

				return resultBlockFull;
			}

#pragma endregion PRIVATE_DRAW_FUNCTIONS
#pragma region PRIVATE_FLUSH_FUNCTIONS

			// Processes result instructions for a single result block
			/*
			*	retval GReturn::SUCCESS				All outstanding instructions were flushed to the result.
			*	retval GReturn::REDUNDANT			No instructions were outstanding.
			*/
			inline GReturn flushResultBlock_Serial(ResultBlock& _blockToFlush)
			{
				// if there are no instructions to draw, return immediately
				if (_blockToFlush.instruction_count < 1)
					return GReturn::REDUNDANT;

				// iterate through basic instruction bin
				for (unsigned short i = 0; i < _blockToFlush.instruction_count; ++i)
				{
					// store instruction by reference for neatness
					ResultInstruction& resInstr = _blockToFlush.instruction_bin[i];
					// determine how to draw the instruction and draw it
					if (resInstr.flags & DrawOptions::USE_TRANSFORMATIONS)
						flushResultInstruction_Transformed(_blockToFlush, resInstr);
					else
						flushResultInstruction_Basic(_blockToFlush, resInstr);
				} // end iterate through basic instruction bin

				// reset instruction count
				_blockToFlush.instruction_count = 0;

				return GReturn::SUCCESS;
			}

			// GConcurrent-compatible function to flush a single block
			// After fixing GConcurrent, this original operation seems to be the ideal way to flush a block
			static void flushResultBlock_Parallel(const void* _unused, ResultBlock* _blockToFlush, unsigned int _blockIndex, const void* _blitter)
			{
				GBlitterImplementation* blitter = const_cast<GBlitterImplementation*>(reinterpret_cast<const GBlitterImplementation*>(_blitter));
				blitter->m_result.flush_results[_blockIndex] = blitter->flushResultBlock_Serial(*_blockToFlush);
			}

			// GConcurrent-compatible function to flush a single instruction
			// After fixing GConcurrent, this operation is no longer necessary
			// We keep it here for reference on how to flush individual instructions
			static void flushInstructions_Parallel(const void* _unused,
				std::pair<unsigned short, unsigned int>* _ins, unsigned int _insIndex, const void* _blitter)
			{
				GBlitterImplementation* blitter =
					const_cast<GBlitterImplementation*>(reinterpret_cast<const GBlitterImplementation*>(_blitter));

				// store instruction by reference for neatness
				ResultBlock& resBlock = blitter->m_result.data[_ins->first];
				ResultInstruction& resInstr = resBlock.instruction_bin[_ins->second];
				// determine how to draw the instruction and draw it
				if (resInstr.flags & DrawOptions::USE_TRANSFORMATIONS)
					blitter->flushResultInstruction_Transformed(resBlock, resInstr);
				else
					blitter->flushResultInstruction_Basic(resBlock, resInstr);
			}

			// Gets pointers to source block color/layer/stencil data (if present) depending on source's format
			/*
				NOTE: Pointers are NOT the start of the source block's data; They are the start of the data region to copy.
				Doing this pre-offset saves a bit of processing time by only doing the pointer arithmetic once, instead of once per data copy operation.
			*/
			inline void getSourceBlockPixelDataRegionPointers(const ResultInstruction& _resInstr, Color*& _outSrcBlockColors, Layer*& _outSrcBlockLayers, Stencil*& _outSrcBlockStencils)
			{
				// store source block for neatness
				SourceBlock& srcBlock = m_sources[_resInstr.source_id].data[_resInstr.source_block_id];
				// get location in source block
				unsigned int sourceOffset = _resInstr.x_source + (_resInstr.y_source * SOURCE_BLOCK_WIDTH);
				switch (m_sources[_resInstr.source_id].format)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C:
						if (!(_resInstr.flags & DrawOptions::IGNORE_SOURCE_COLORS))
							_outSrcBlockColors = srcBlock.c.colors + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L:
						if (_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS)
							_outSrcBlockLayers = srcBlock.l.layers + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S:
						if (_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS)
							_outSrcBlockStencils = srcBlock.s.stencils + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL:
						if (!(_resInstr.flags & DrawOptions::IGNORE_SOURCE_COLORS))
							_outSrcBlockColors = srcBlock.cl.colors + sourceOffset;
						if (_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS)
							_outSrcBlockLayers = srcBlock.cl.layers + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS:
						if (!(_resInstr.flags & DrawOptions::IGNORE_SOURCE_COLORS))
							_outSrcBlockColors = srcBlock.cs.colors + sourceOffset;
						if (_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS)
							_outSrcBlockStencils = srcBlock.cs.stencils + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS:
						if (_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS)
							_outSrcBlockLayers = srcBlock.ls.layers + sourceOffset;
						if (_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS)
							_outSrcBlockStencils = srcBlock.ls.stencils + sourceOffset;
						break;
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
						if (!(_resInstr.flags & DrawOptions::IGNORE_SOURCE_COLORS))
							_outSrcBlockColors = srcBlock.cls.colors + sourceOffset;
						if (_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS)
							_outSrcBlockLayers = srcBlock.cls.layers + sourceOffset;
						if (_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS)
							_outSrcBlockStencils = srcBlock.cls.stencils + sourceOffset;
						break;
					default:
						break;
				}
			}

			// Applies inverse transformation to the center of a pixel in the result canvas
			static inline Vector2 getResInstrReverseLookupCoord(const ResultInstruction& _resInstr, const unsigned short _x, const unsigned short _y)
			{
				Vector3 v = matrixXVector_CM(
					Matrix3x3{{{
						_resInstr.m_inverse.a,	_resInstr.m_inverse.b,	0.0f,
						_resInstr.m_inverse.c,	_resInstr.m_inverse.d,	0.0f,
						_resInstr.t_inverse.x,	_resInstr.t_inverse.y,	1.0f
					}}}, Vector3{{{ _x + 0.5f, _y + 0.5f, 1.0f }}});
				return Vector2{{{ v.x + _resInstr.x_source, v.y + _resInstr.y_source }}};
			}

			// Checks a reverse-lookup coordinate against the dimensions of the tile being drawn to determine if the pixel in the result that it came from should be drawn to
			static inline bool checkResInstrReverseLookupCoord(const ResultInstruction& _resInstr, const Vector2 _coord)
			{
				return _coord.x >= _resInstr.x_source
					&& _coord.x <  _resInstr.x_source + _resInstr.w_source
					&& _coord.y >= _resInstr.y_source
					&& _coord.y <  _resInstr.y_source + _resInstr.h_source;
			}

			// Converts an x/y coordinate within a result block into a linear index into that result block's canvas
			static inline unsigned int getResultBlockPixelIndex(unsigned short _x, unsigned short _y)
			{
				return static_cast<unsigned int>(_x + (_y * RESULT_BLOCK_WIDTH));
			}

			// Uses a reverse-lookup coordinate to sample a color value from the source of the tile being drawn
			inline Color reverseLookupColorFromSource(const unsigned short _sourceId, Vector2 _coord)
			{
				// store source by reference for neatness
				Source& src = m_sources[_sourceId];
				// get height of blocks in source
				unsigned short src_blockheight = getSourceBlockHeight(src.format);
				// get x/y index of source block
				unsigned short block_x = static_cast<unsigned int>(_coord.x) / SOURCE_BLOCK_WIDTH;
				unsigned short block_y = static_cast<unsigned int>(_coord.y) / src_blockheight;
				// store block by reference for neatness
				SourceBlock& block = src.data[block_x + block_y * src.w_data];
				// convert x/y coordinate into a linear index into the source block's canvas
				unsigned int pixelindex = static_cast<unsigned int>(_coord.x) - (block_x * SOURCE_BLOCK_WIDTH) + ((static_cast<unsigned int>(_coord.y) - (block_y * src_blockheight)) * SOURCE_BLOCK_WIDTH);
				// sample the value from the canvas
				Color result;
				switch (src.format)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::C:
						result = block.c.colors[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL:
						result = block.cl.colors[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS:
						result = block.cs.colors[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
						result = block.cls.colors[pixelindex];
						break;
					default:
						result = 0x00000000;
						break;
				};
				return result;
			}
			// Uses a reverse-lookup coordinate to sample a layer value from the source of the tile being drawn
			inline Layer reverseLookupLayerFromSource(const unsigned short _sourceId, const Vector2 _coord)
			{
				// store source by reference for neatness
				Source& src = m_sources[_sourceId];
				// get height of blocks in source
				unsigned short src_blockheight = getSourceBlockHeight(src.format);
				// get x/y index of source block
				unsigned short block_x = static_cast<unsigned int>(_coord.x) / SOURCE_BLOCK_WIDTH;
				unsigned short block_y = static_cast<unsigned int>(_coord.y) / src_blockheight;
				// store block by reference for neatness
				SourceBlock& block = src.data[block_x + block_y * src.w_data];
				// convert x/y coordinate into a linear index into the source block's canvas
				unsigned int pixelindex = static_cast<unsigned int>(_coord.x) - (block_x * SOURCE_BLOCK_WIDTH) + ((static_cast<unsigned int>(_coord.y) - (block_y * src_blockheight)) * SOURCE_BLOCK_WIDTH);
				// sample the value from the canvas
				Layer result;
				switch (src.format)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::L:
						result = block.l.layers[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CL:
						result = block.cl.layers[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS:
						result = block.ls.layers[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
						result = block.cls.layers[pixelindex];
						break;
					default:
						result = 0xff;
						break;
				};
				return result;
			}
			// Uses a reverse-lookup coordinate to sample a stencil value from the source of the tile being drawn
			inline Stencil reverseLookupStencilFromSource(const unsigned short _sourceId, const Vector2 _coord)
			{
				// store source by reference for neatness
				Source& src = m_sources[_sourceId];
				// get height of blocks in source
				unsigned short src_blockheight = getSourceBlockHeight(src.format);
				// get x/y index of source block
				unsigned short block_x = static_cast<unsigned int>(_coord.x) / SOURCE_BLOCK_WIDTH;
				unsigned short block_y = static_cast<unsigned int>(_coord.y) / src_blockheight;
				// store block by reference for neatness
				SourceBlock& block = src.data[block_x + block_y * src.w_data];
				// convert x/y coordinate into a linear index into the source block's canvas
				unsigned int pixelindex = static_cast<unsigned int>(_coord.x) - (block_x * SOURCE_BLOCK_WIDTH) + ((static_cast<unsigned int>(_coord.y) - (block_y * src_blockheight)) * SOURCE_BLOCK_WIDTH);
				// sample the value from the canvas
				Stencil result;
				switch (src.format)
				{
					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::S:
						result = block.s.stencils[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CS:
						result = block.cs.stencils[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::LS:
						result = block.ls.stencils[pixelindex];
						break;

					case INTERNAL_PIXEL_ATTRIBUTES::FORMAT::CLS:
						result = block.cls.stencils[pixelindex];
						break;
					default:
						result = 0x00;
						break;
				};
				return result;
			}

			// Process an untransformed result instruction
			inline void flushResultInstruction_Basic(ResultBlock& _resBlock, ResultInstruction& _resInstr)
			{
				// get locations to copy data from
				Color* sourceColors = nullptr;
				Layer* sourceLayers = nullptr;
				Stencil* sourceStencils = nullptr;
				getSourceBlockPixelDataRegionPointers(_resInstr, sourceColors, sourceLayers, sourceStencils);
				// if not using source's layers/stencils, temporary buffers will be used instead to transfer the instruction's layer/stencil values into the result
				Layer layerOverride[RESULT_BLOCK_WIDTH];
				Stencil stencilOverride[RESULT_BLOCK_WIDTH];
				if (!(_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS))
					memset(layerOverride, _resInstr.layer_override, _resInstr.w_result);
				if (!(_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS))
					memset(stencilOverride, _resInstr.stencil_override, _resInstr.w_result);
				// get locations to copy data to (start of result block data array + offset (instruction origin x/y))
				unsigned int resultOffset = _resInstr.x_result + (_resInstr.y_result * RESULT_BLOCK_WIDTH);
				Color* resultColors = _resBlock.color + resultOffset;
				Layer* resultLayers = _resBlock.layer + resultOffset;
				Stencil* resultStencils = _resBlock.stencil + resultOffset;
				// copy data (the only difference between these instruction formats is the way colors are drawn to the result)
				switch (_resInstr.flags & RESULT_INSTRUCTION_FORMAT::TSP_MSK)
				//switch (_resInstr.flags & RESULT_INSTRUCTION_FORMAT::ITP_TSP_MSK) // replace the above line with this one once interpolation is implemented
				{
				case RESULT_INSTRUCTION_FORMAT::DEFAULT: // direct transfer (opaque)
					flushResultInstruction_Basic_Default(resultColors, resultLayers, resultStencils, sourceColors, sourceLayers, sourceStencils, layerOverride, stencilOverride, _resInstr.w_result, _resInstr.h_result, _resInstr.flags);
					break;
				case RESULT_INSTRUCTION_FORMAT::MSK: // bitmasking (XOR transfer; opaque/transparent)
					flushResultInstruction_Basic_Masked(resultColors, resultLayers, resultStencils, sourceColors, sourceLayers, sourceStencils, layerOverride, stencilOverride, _resInstr.w_result, _resInstr.h_result, _resInstr.flags, _resInstr.mask_color, _resInstr.mask_layer, _resInstr.mask_stencil);
					break;
				case RESULT_INSTRUCTION_FORMAT::TSP: // alpha blending (semi-transparent)
					flushResultInstruction_Basic_Transparent(resultColors, resultLayers, resultStencils, sourceColors, sourceLayers, sourceStencils, layerOverride, stencilOverride, _resInstr.w_result, _resInstr.h_result, _resInstr.flags);
					break;
				case RESULT_INSTRUCTION_FORMAT::TSP_MSK: // bitmasking and alpha blending
					flushResultInstruction_Basic_Transparent_Masked(resultColors, resultLayers, resultStencils, sourceColors, sourceLayers, sourceStencils, layerOverride, stencilOverride, _resInstr.w_result, _resInstr.h_result, _resInstr.flags, _resInstr.mask_color, _resInstr.mask_layer, _resInstr.mask_stencil);
					break;
				}
			}
			static inline void flushResultInstruction_Basic_Default(
				Color* _dstColors, Layer* _dstLayers, Stencil* _dstStencils, const Color* _srcColors, const Layer* _srcLayers, const Stencil* _srcStencils,
				const Layer* _layerOverride, const Stencil* _stencilOverride, unsigned short _w, unsigned short _h, unsigned short _flags)
			{
				switch (_flags & BASIC_ORIENTATION::RVH_270)
				{
					case BASIC_ORIENTATION::XXX_0:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (y * SOURCE_BLOCK_WIDTH), _w << 2);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXX_0

					case BASIC_ORIENTATION::XXH_0_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyReverse(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (y * SOURCE_BLOCK_WIDTH), _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXH_0_INV

					case BASIC_ORIENTATION::XVX_180_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w << 2);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVX_180_INV

					case BASIC_ORIENTATION::XVH_180:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyReverse(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVH_180

					case BASIC_ORIENTATION::RXX_90:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + y, _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXX_90

					case BASIC_ORIENTATION::RXH_270_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyReverseRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + y, _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXH_270_INV

					case BASIC_ORIENTATION::RVX_90_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (_h - 1 - y), _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVX_90_INV

					case BASIC_ORIENTATION::RVH_270:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								directColorCopyReverseRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (_h - 1 - y), _w);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVH_270
				}
			}
			static inline void flushResultInstruction_Basic_Masked(
				Color* _dstColors, Layer* _dstLayers, Stencil* _dstStencils, const Color* _srcColors, const Layer* _srcLayers, const Stencil* _srcStencils,
				const Layer* _layerOverride, const Stencil* _stencilOverride, unsigned short _w, unsigned short _h, unsigned short _flags,
				Color _maskColor, Layer _maskLayer, Stencil _maskStencil)
			{
				switch (_flags & BASIC_ORIENTATION::RVH_270)
				{
					case BASIC_ORIENTATION::XXX_0:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopy(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (y * SOURCE_BLOCK_WIDTH), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXX_0

					case BASIC_ORIENTATION::XXH_0_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyReverse(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (y * SOURCE_BLOCK_WIDTH), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXH_0_INV

					case BASIC_ORIENTATION::XVX_180_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopy(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVX_180_INV

					case BASIC_ORIENTATION::XVH_180:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyReverse(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVH_180

					case BASIC_ORIENTATION::RXX_90:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + y, _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXX_90

					case BASIC_ORIENTATION::RXH_270_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyReverseRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + y, _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXH_270_INV

					case BASIC_ORIENTATION::RVX_90_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (_h - 1 - y), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVX_90_INV

					case BASIC_ORIENTATION::RVH_270:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								maskedColorCopyReverseRotated(_dstColors + (y * RESULT_BLOCK_WIDTH), _srcColors + (_h - 1 - y), _w, _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVH_270
				}
			}
			static inline void flushResultInstruction_Basic_Transparent(
				Color* _dstColors, Layer* _dstLayers, Stencil* _dstStencils, const Color* _srcColors, const Layer* _srcLayers, const Stencil* _srcStencils,
				const Layer* _layerOverride, const Stencil* _stencilOverride, unsigned short _w, unsigned short _h, unsigned short _flags)
			{
				switch (_flags & BASIC_ORIENTATION::RVH_270)
				{
					case BASIC_ORIENTATION::XXX_0:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned int y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[x + (y * SOURCE_BLOCK_WIDTH)]);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXX_0

					case BASIC_ORIENTATION::XXH_0_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_w - 1 - x) + (y * SOURCE_BLOCK_WIDTH)]);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXH_0_INV

					case BASIC_ORIENTATION::XVX_180_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[x + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH)]);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVX_180_INV

					case BASIC_ORIENTATION::XVH_180:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_w - 1 - x) + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH)]);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVH_180

					case BASIC_ORIENTATION::RXX_90:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[y + ((_w - 1 - x) * SOURCE_BLOCK_WIDTH)]); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXX_90

					case BASIC_ORIENTATION::RXH_270_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[y + (x * SOURCE_BLOCK_WIDTH)]); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXH_270_INV

					case BASIC_ORIENTATION::RVX_90_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_h - 1 - y) + ((_w - 1 -x) * SOURCE_BLOCK_WIDTH)]); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVX_90_INV

					case BASIC_ORIENTATION::RVH_270:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_h - 1 - y) + (x * SOURCE_BLOCK_WIDTH)]); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								directMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVH_270
				}
			}
			static inline void flushResultInstruction_Basic_Transparent_Masked(
				Color* _dstColors, Layer* _dstLayers, Stencil* _dstStencils, const Color* _srcColors, const Layer* _srcLayers, const Stencil* _srcStencils,
				const Layer* _layerOverride, const Stencil* _stencilOverride, unsigned short _w, unsigned short _h, unsigned short _flags,
				Color _maskColor, Layer _maskLayer, Stencil _maskStencil)
			{
				switch (_flags & BASIC_ORIENTATION::RVH_270)
				{
					case BASIC_ORIENTATION::XXX_0:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[x + (y * SOURCE_BLOCK_WIDTH)], _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXX_0

					case BASIC_ORIENTATION::XXH_0_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_w - 1 - x) + (y * SOURCE_BLOCK_WIDTH)], _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (y * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (y * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XXH_0_INV

					case BASIC_ORIENTATION::XVX_180_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[x + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH)], _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVX_180_INV

					case BASIC_ORIENTATION::XVH_180:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_w - 1 - x) + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH)], _maskColor);
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverse(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + ((_h - 1 - y) * SOURCE_BLOCK_WIDTH), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::XVH_180

					case BASIC_ORIENTATION::RXX_90:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[y + ((_w - 1 - x) * SOURCE_BLOCK_WIDTH)], _maskColor); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXX_90

					case BASIC_ORIENTATION::RXH_270_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[y + (x * SOURCE_BLOCK_WIDTH)], _maskColor); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + y, _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + y, _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RXH_270_INV

					case BASIC_ORIENTATION::RVX_90_INV:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_h - 1 - y) + ((_w - 1 - x) * SOURCE_BLOCK_WIDTH)], _maskColor); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVX_90_INV

					case BASIC_ORIENTATION::RVH_270:
					{
						// color
						if (!(_flags & DrawOptions::IGNORE_SOURCE_COLORS) && _srcColors)
							for (unsigned short y = 0; y < _h; ++y)
								for (unsigned int x = 0; x < _w; ++x)
									_dstColors[x + (y * RESULT_BLOCK_WIDTH)] = fixedPointColorLerp_Masked(_dstColors[x + (y * RESULT_BLOCK_WIDTH)], _srcColors[(_h - 1 - y) + (x * SOURCE_BLOCK_WIDTH)], _maskColor); // dst x = src y, dst w = src h and vice versa
						// layer
						if ((_flags & DrawOptions::USE_SOURCE_LAYERS) && _srcLayers)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstLayers + (y * RESULT_BLOCK_WIDTH), _srcLayers + (_h - 1 - y), _w, _maskLayer);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstLayers + (y * RESULT_BLOCK_WIDTH), _layerOverride, _w);
						// stencil
						if ((_flags & DrawOptions::USE_SOURCE_STENCILS) && _srcStencils)
							for (unsigned short y = 0; y < _h; ++y)
								maskedMetadataCopyReverseRotated(_dstStencils + (y * RESULT_BLOCK_WIDTH), _srcStencils + (_h - 1 - y), _w, _maskStencil);
						else
							for (unsigned short y = 0; y < _h; ++y)
								memcpy(_dstStencils + (y * RESULT_BLOCK_WIDTH), _stencilOverride, _w);
					} break; // end case BASIC_ORIENTATION::RVH_270
				}
			}

			// Processes a transformed result instruction
			inline void flushResultInstruction_Transformed(ResultBlock& _resBlock, ResultInstruction& _resInstr)
			{
#if defined(GBLITTER_DEBUG_OUTLINE_TRANFORMED_BOUNDS) || defined(GBLITTER_DEBUG_FILL_TRANFORMED_BOUNDS)
				int bounds_left = static_cast<int>(
					G_CLAMP_MIN(
						G_SMALLER(_resInstr.footprint.a.x, G_SMALLER(_resInstr.footprint.b.x, G_SMALLER(_resInstr.footprint.c.x, _resInstr.footprint.d.x))),
						_resBlock.x_result)
					- _resBlock.x_result
					);
				int bounds_top = static_cast<int>(
					G_CLAMP_MIN(
						G_SMALLER(_resInstr.footprint.a.y, G_SMALLER(_resInstr.footprint.b.y, G_SMALLER(_resInstr.footprint.c.y, _resInstr.footprint.d.y))),
						_resBlock.y_result)
					- _resBlock.y_result
					);
				int bounds_right = static_cast<int>(
					G_CLAMP_MAX(
						G_LARGER(_resInstr.footprint.a.x, G_LARGER(_resInstr.footprint.b.x, G_LARGER(_resInstr.footprint.c.x, _resInstr.footprint.d.x))),
						_resBlock.x_result + RESULT_BLOCK_WIDTH)
					- (_resBlock.x_result + 1)
					);
				int bounds_bottom = static_cast<int>(
					G_CLAMP_MAX(
						G_LARGER(_resInstr.footprint.a.y, G_LARGER(_resInstr.footprint.b.y, G_LARGER(_resInstr.footprint.c.y, _resInstr.footprint.d.y))),
						_resBlock.y_result + RESULT_BLOCK_HEIGHT)
					- (_resBlock.y_result + 1)
					);
#endif
#if defined(GBLITTER_DEBUG_FILL_TRANFORMED_BOUNDS)
				// fill instruction bounds with solid color
				for (int y = bounds_top; y <= bounds_bottom; ++y)
					for (int x = bounds_left; x <= bounds_right; ++x)
						_resBlock.color[getResultBlockPixelIndex(x, y)] = 0xffff0000;
#elif defined(GBLITTER_DEBUG_OUTLINE_TRANFORMED_BOUNDS)
				// outline instruction bounds with solid color
				// if row is the first or last row, fill entire row
				for (int x = bounds_left; x <= bounds_right; ++x)
				{
					_resBlock.color[getResultBlockPixelIndex(x, bounds_top)] = 0xffff0000;
					_resBlock.color[getResultBlockPixelIndex(x, bounds_bottom)] = 0xffff0000;
				}
				// otherwise, fill first and last pixel in row
				for (int y = bounds_top + 1; y <= bounds_bottom - 1; ++y)
				{
					_resBlock.color[getResultBlockPixelIndex(bounds_left, y)] = 0xffff0000;
					_resBlock.color[getResultBlockPixelIndex(bounds_right, y)] = 0xffff0000;
				}
#endif

				// store instruction's source by reference for neatness
				Source& src = m_sources[_resInstr.source_id];
				// get source's format as flags
				unsigned short srcFormatFlags = pixelFormatToFlags(src.format);

				// determine which data types to draw and store the result in a bit-flag set
				//   (this could be done much more simply with bools, but bools need 64 bits each and only use 1 of them, whereas this uses 8 bits total, even if another type of metadata is added later)
				Byte dataTypesToDrawFlags = 0;
				// color
				dataTypesToDrawFlags |= (
						// get flag for if color data is present in source
							((srcFormatFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR) // isolate flag value from source format flags
							>> 0) // shift flag value to first position
						& // combine flag values
						// get flag for if source color data should be ignored by instruction
							(~((_resInstr.flags & DrawOptions::IGNORE_SOURCE_COLORS) // isolate flag value from instruction draw option flags
							>> 3) & 1) // shift flag value to first position (isolated value is inverted and &'ed with 1, since color should be used if IGNORE_SOURCE_COLORS is off and ignored if it is on)
					) << 0; // shift resulting flag value into position
				// layer
				dataTypesToDrawFlags |= (
						// get flag for if layer data is present in source
							((srcFormatFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER) // isolate flag value from source format flags
							>> 1) // shift flag value to first position
						& // combine flag values
						// get flag for if source layer data should be used by instruction
							((_resInstr.flags & DrawOptions::USE_SOURCE_LAYERS) // isolate flag value from instruction draw option flags
							>> 4) // shift flag value to first position
					) << 1; // shift resulting flag value into position
				// stencil
				dataTypesToDrawFlags |= (
						// get flag for if stencil data is present in source
							((srcFormatFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL) // isolate flag value from source format flags
							>> 2) // shift flag value to first position
						& // combine flag values
						// get flag for if source stencil data should be used by instruction
							((_resInstr.flags & DrawOptions::USE_SOURCE_STENCILS) // isolate flag value from instruction draw option flags
							>> 5) // shift flag value to first position
					) << 2; // shift resulting flag value into position

				// DEBUG triangle fill/corner colors
				static constexpr Color triangleAcolor = 0xff00ff00;
				static constexpr Color triangleAcornerColor = 0xffff00ff;
				static constexpr Color triangleBcolor = 0xff0000ff;
				static constexpr Color triangleBcornerColor = 0xffffff00;

				// call the appropriate draw helper function for the format specified by flags
				switch (_resInstr.flags & RESULT_INSTRUCTION_FORMAT::TSP_MSK)
				{
					case RESULT_INSTRUCTION_FORMAT::DEFAULT:
						drawClippedTriangle_Default(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.b, _resInstr.footprint.c }, dataTypesToDrawFlags, triangleAcolor, triangleAcornerColor);
						drawClippedTriangle_Default(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.c, _resInstr.footprint.d }, dataTypesToDrawFlags, triangleBcolor, triangleBcornerColor);
						break;
					case RESULT_INSTRUCTION_FORMAT::MSK:
						drawClippedTriangle_Masked(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.b, _resInstr.footprint.c }, dataTypesToDrawFlags, triangleAcolor, triangleAcornerColor);
						drawClippedTriangle_Masked(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.c, _resInstr.footprint.d }, dataTypesToDrawFlags, triangleBcolor, triangleBcornerColor);
						break;
					case RESULT_INSTRUCTION_FORMAT::TSP:
						drawClippedTriangle_Transparent(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.b, _resInstr.footprint.c }, dataTypesToDrawFlags, triangleAcolor, triangleAcornerColor);
						drawClippedTriangle_Transparent(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.c, _resInstr.footprint.d }, dataTypesToDrawFlags, triangleBcolor, triangleBcornerColor);
						break;
					case RESULT_INSTRUCTION_FORMAT::TSP_MSK:
						drawClippedTriangle_Transparent_Masked(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.b, _resInstr.footprint.c }, dataTypesToDrawFlags, triangleAcolor, triangleAcornerColor);
						drawClippedTriangle_Transparent_Masked(_resBlock, _resInstr, Triangle{ _resInstr.footprint.a, _resInstr.footprint.c, _resInstr.footprint.d }, dataTypesToDrawFlags, triangleBcolor, triangleBcornerColor);
						break;
				}
			}

			// Draws a textured triangle parametrically, clipped to the bounds of a result block
			//   Based on a function written by Lari Norri
			inline void drawClippedTriangle_Default(ResultBlock& _resBlock, const ResultInstruction& _resInstr, Triangle _tri, const Byte _dataTypesToDrawFlags, Color _color = 0x00000000, Color _cornerColor = 0x00000000)
			{
				// get clipping bounds
				int clipLeft	= _resBlock.x_result;
				int clipRight	= _resBlock.x_result + RESULT_BLOCK_WIDTH;
				int clipTop		= _resBlock.y_result;
				int clipBottom	= _resBlock.y_result + RESULT_BLOCK_HEIGHT;

				// reject triangles not within horizontal bounds
				if (_tri.a.x < clipLeft && _tri.b.x < clipLeft && _tri.c.x < clipLeft)
					return;
				if (_tri.a.x > clipRight && _tri.b.x > clipRight && _tri.c.x > clipRight)
					return;

				// sort triangle points from top to bottom
				Point swap;
				if (_tri.a.y > _tri.b.y)
				{
					swap = _tri.a;
					_tri.a = _tri.b;
					_tri.b = swap;
				}
				if (_tri.a.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.a;
					_tri.a = swap;
				}
				if (_tri.b.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.b;
					_tri.b = swap;
				}

				// reject triangles not within vertical bounds
				if (_tri.c.y < clipTop || _tri.a.y > clipBottom)
					return;

				// calculate slopes between all points
				float slope_ac = (_tri.c.x - _tri.a.x) / static_cast<float>(_tri.c.y - _tri.a.y);
				float slope_ab = (_tri.b.x - _tri.a.x) / static_cast<float>(_tri.b.y - _tri.a.y);
				float slope_bc = (_tri.c.x - _tri.b.x) / static_cast<float>(_tri.c.y - _tri.b.y);

				// start at top point if it is within vertical bounds, otherwise start at top of block
				int start_y = (_tri.a.y > clipTop) ? _tri.a.y : clipTop;
				// end at middle point if it is within vertical bounds, otherwise end at bottom of block
				int end_y = (_tri.b.y < clipBottom) ? _tri.b.y : clipBottom;

				// compute start and end x
				float start_x = ((_tri.c.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.c.y - _tri.a.y))) + _tri.a.x;
				float end_x = static_cast<float>(_tri.b.x); // initializing to the middle point's x handles edge cases where a triangle has a flat top
				if (start_y < _tri.b.y)
					end_x = ((_tri.b.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.b.y - _tri.a.y))) + _tri.a.x;
				else if (start_y > _tri.b.y)
					end_x = ((_tri.c.x - _tri.b.x) * (1.0f - ((_tri.c.y - start_y) / static_cast<float>(_tri.c.y - _tri.b.y)))) + _tri.b.x;

				// flip start and end if needed to make the rest of the logic simpler
				float flip = std::numeric_limits<float>::quiet_NaN();
				if (start_x + slope_ac > end_x + slope_ab)
				{
					flip = start_x;
					start_x = end_x;
					end_x = flip;
					flip = slope_ac;
					slope_ac = slope_ab;
					slope_ab = flip;
				}

				// draw data types indicated by flags
				// color data
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					float color_start_x = start_x;
					int color_start_y = start_y;
					float color_end_x = end_x;
					int color_end_y = end_y;
					float color_slope_ab = slope_ab;
					float color_slope_ac = slope_ac;
					float color_slope_bc = slope_bc;
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; color_start_y < color_end_y; ++color_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(color_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(color_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, color_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
#if defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP) || defined(GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES)
								_resBlock.color[index] = _color;
#endif
#if !defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP)
								// draw reverse-lookup color with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, color_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.color[index] = reverseLookupColorFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup color with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.color[index] = reverseLookupColorFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, color_start_y));
#endif
							}
							// adjust start and end x by slope
							color_start_x += color_slope_ac;
							color_end_x += color_slope_ab;
						}
						color_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? color_slope_ab = color_slope_bc : color_slope_ac = color_slope_bc;
					}
				}
				// layer data
				float layer_start_x = start_x;
				int layer_start_y = start_y;
				float layer_end_x = end_x;
				int layer_end_y = end_y;
				float layer_slope_ab = slope_ab;
				float layer_slope_ac = slope_ac;
				float layer_slope_bc = slope_bc;
				// use layer data from source if set, otherwise use instruction's layer override value
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, layer_start_y));
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = _resInstr.layer_override;

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = _resInstr.layer_override;
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				// stencil data
				float stencil_start_x = start_x;
				int stencil_start_y = start_y;
				float stencil_end_x = end_x;
				int stencil_end_y = end_y;
				float stencil_slope_ab = slope_ab;
				float stencil_slope_ac = slope_ac;
				float stencil_slope_bc = slope_bc;
				// use stencil data from source if set, otherwise use instruction's stencil override value
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, start_y));
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = _resInstr.stencil_override;

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = _resInstr.stencil_override;
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}

				// DEBUG draw corners
#if defined(GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS)
				// max corners in each dimension are 1 pixel past the last pixels drawn to, since corner positions are geometric rather than pixel-space
				//   (pixel-space coordinates use integers only, which means a pixel-space shape spans [min, max), rather than a geometric shape which spans [min, max] )
				if (_tri.a.x >= clipLeft && _tri.a.x < clipRight && _tri.a.y >= clipTop && _tri.a.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.a.x - _resBlock.x_result, _tri.a.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.b.x >= clipLeft && _tri.b.x < clipRight && _tri.b.y >= clipTop && _tri.b.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.b.x - _resBlock.x_result, _tri.b.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.c.x >= clipLeft && _tri.c.x < clipRight && _tri.c.y >= clipTop && _tri.c.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.c.x - _resBlock.x_result, _tri.c.y - _resBlock.y_result)] = _cornerColor;
#endif
			}
			inline void drawClippedTriangle_Masked(ResultBlock& _resBlock, const ResultInstruction& _resInstr, Triangle _tri, const Byte _dataTypesToDrawFlags, Color _color = 0x00000000, Color _cornerColor = 0x00000000)
			{
				// get clipping bounds
				int clipLeft	= _resBlock.x_result;
				int clipRight	= _resBlock.x_result + RESULT_BLOCK_WIDTH;
				int clipTop		= _resBlock.y_result;
				int clipBottom	= _resBlock.y_result + RESULT_BLOCK_HEIGHT;

				// reject triangles not within horizontal bounds
				if (_tri.a.x < clipLeft && _tri.b.x < clipLeft && _tri.c.x < clipLeft)
					return;
				if (_tri.a.x > clipRight && _tri.b.x > clipRight && _tri.c.x > clipRight)
					return;

				// sort triangle points from top to bottom
				Point swap;
				if (_tri.a.y > _tri.b.y)
				{
					swap = _tri.a;
					_tri.a = _tri.b;
					_tri.b = swap;
				}
				if (_tri.a.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.a;
					_tri.a = swap;
				}
				if (_tri.b.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.b;
					_tri.b = swap;
				}

				// reject triangles not within vertical bounds
				if (_tri.c.y < clipTop || _tri.a.y > clipBottom)
					return;

				// calculate slopes between all points
				float slope_ac = (_tri.c.x - _tri.a.x) / static_cast<float>(_tri.c.y - _tri.a.y);
				float slope_ab = (_tri.b.x - _tri.a.x) / static_cast<float>(_tri.b.y - _tri.a.y);
				float slope_bc = (_tri.c.x - _tri.b.x) / static_cast<float>(_tri.c.y - _tri.b.y);

				// start at top point if it is within vertical bounds, otherwise start at top of block
				int start_y = (_tri.a.y > clipTop) ? _tri.a.y : clipTop;
				// end at middle point if it is within vertical bounds, otherwise end at bottom of block
				int end_y = (_tri.b.y < clipBottom) ? _tri.b.y : clipBottom;

				// compute start and end x
				float start_x = ((_tri.c.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.c.y - _tri.a.y))) + _tri.a.x;
				float end_x = static_cast<float>(_tri.b.x); // initializing to the middle point's x handles edge cases where a triangle has a flat top
				if (start_y < _tri.b.y)
					end_x = ((_tri.b.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.b.y - _tri.a.y))) + _tri.a.x;
				else if (start_y > _tri.b.y)
					end_x = ((_tri.c.x - _tri.b.x) * (1.0f - ((_tri.c.y - start_y) / static_cast<float>(_tri.c.y - _tri.b.y)))) + _tri.b.x;

				// flip start and end if needed to make the rest of the logic simpler
				float flip = std::numeric_limits<float>::quiet_NaN();
				if (start_x + slope_ac > end_x + slope_ab)
				{
					flip = start_x;
					start_x = end_x;
					end_x = flip;
					flip = slope_ac;
					slope_ac = slope_ab;
					slope_ab = flip;
				}

				// draw data types indicated by flags
				// color data
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					float color_start_x = start_x;
					int color_start_y = start_y;
					float color_end_x = end_x;
					int color_end_y = end_y;
					float color_slope_ab = slope_ab;
					float color_slope_ac = slope_ac;
					float color_slope_bc = slope_bc;
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; color_start_y < color_end_y; ++color_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(color_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(color_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, color_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
#if defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP) || defined(GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES)
								_resBlock.color[index] = _color;
#endif
#if !defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP)
								// draw reverse-lookup color with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, color_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
								{
									// sample color from source and XOR it with instruction's mask color
									Color color_pseudoSrc = reverseLookupColorFromSource(_resInstr.source_id, coord) ^ _resInstr.mask_color;
									// use pseudo-source color to generate blending mask value
									Color color_mask = (-1 + (((color_pseudoSrc | (~color_pseudoSrc + 1)) >> 31) & 1));
									// draw color to result using mask (color drawn will either be existing color in result or new color from source)
									_resBlock.color[index] = (_resBlock.color[index] & color_mask) | ((color_pseudoSrc ^ _resInstr.mask_color) & (~color_mask));
								}

								// draw reverse-lookup color with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//// sample color from source and XOR it with instruction's mask color
								//Color color_pseudoSrc = reverseLookupColorFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, color_start_y)) ^ _resInstr.mask_color;
								//// use pseudo-source color to generate blending mask value
								//Color color_mask = (-1 + (((color_pseudoSrc | (~color_pseudoSrc + 1)) >> 31) & 1));
								//// draw color to result using mask (color drawn will either be existing color in result or new color from source)
								//_resBlock.color[index] = (_resBlock.color[index] & color_mask) | ((color_pseudoSrc ^ _resInstr.mask_color & ~color_mask));
#endif
							}
							// adjust start and end x by slope
							color_start_x += color_slope_ac;
							color_end_x += color_slope_ab;
						}
						color_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? color_slope_ab = color_slope_bc : color_slope_ac = color_slope_bc;
					}
				}
				// layer data
				float layer_start_x = start_x;
				int layer_start_y = start_y;
				float layer_end_x = end_x;
				int layer_end_y = end_y;
				float layer_slope_ab = slope_ab;
				float layer_slope_ac = slope_ac;
				float layer_slope_bc = slope_bc;
				// use layer data from source if set, otherwise use instruction's layer override value
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, layer_start_y));
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = _resInstr.layer_override;

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = _resInstr.layer_override;
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				// stencil data
				float stencil_start_x = start_x;
				int stencil_start_y = start_y;
				float stencil_end_x = end_x;
				int stencil_end_y = end_y;
				float stencil_slope_ab = slope_ab;
				float stencil_slope_ac = slope_ac;
				float stencil_slope_bc = slope_bc;
				// use stencil data from source if set, otherwise use instruction's stencil override value
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, start_y));
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = _resInstr.stencil_override;

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = _resInstr.stencil_override;
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}

				// DEBUG draw corners
#if defined(GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS)
				// max corners in each dimension are 1 pixel past the last pixels drawn to, since corner positions are geometric rather than pixel-space
				//   (pixel-space coordinates use integers only, which means a pixel-space shape spans [min, max), rather than a geometric shape which spans [min, max] )
				if (_tri.a.x >= clipLeft && _tri.a.x < clipRight && _tri.a.y >= clipTop && _tri.a.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.a.x - _resBlock.x_result, _tri.a.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.b.x >= clipLeft && _tri.b.x < clipRight && _tri.b.y >= clipTop && _tri.b.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.b.x - _resBlock.x_result, _tri.b.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.c.x >= clipLeft && _tri.c.x < clipRight && _tri.c.y >= clipTop && _tri.c.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.c.x - _resBlock.x_result, _tri.c.y - _resBlock.y_result)] = _cornerColor;
#endif
			}
			inline void drawClippedTriangle_Transparent(ResultBlock& _resBlock, const ResultInstruction& _resInstr, Triangle _tri, const Byte _dataTypesToDrawFlags, Color _color = 0x00000000, Color _cornerColor = 0x00000000)
			{
				// get clipping bounds
				int clipLeft	= _resBlock.x_result;
				int clipRight	= _resBlock.x_result + RESULT_BLOCK_WIDTH;
				int clipTop		= _resBlock.y_result;
				int clipBottom	= _resBlock.y_result + RESULT_BLOCK_HEIGHT;

				// reject triangles not within horizontal bounds
				if (_tri.a.x < clipLeft && _tri.b.x < clipLeft && _tri.c.x < clipLeft)
					return;
				if (_tri.a.x > clipRight && _tri.b.x > clipRight && _tri.c.x > clipRight)
					return;

				// sort triangle points from top to bottom
				Point swap;
				if (_tri.a.y > _tri.b.y)
				{
					swap = _tri.a;
					_tri.a = _tri.b;
					_tri.b = swap;
				}
				if (_tri.a.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.a;
					_tri.a = swap;
				}
				if (_tri.b.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.b;
					_tri.b = swap;
				}

				// reject triangles not within vertical bounds
				if (_tri.c.y < clipTop || _tri.a.y > clipBottom)
					return;

				// calculate slopes between all points
				float slope_ac = (_tri.c.x - _tri.a.x) / static_cast<float>(_tri.c.y - _tri.a.y);
				float slope_ab = (_tri.b.x - _tri.a.x) / static_cast<float>(_tri.b.y - _tri.a.y);
				float slope_bc = (_tri.c.x - _tri.b.x) / static_cast<float>(_tri.c.y - _tri.b.y);

				// start at top point if it is within vertical bounds, otherwise start at top of block
				int start_y = (_tri.a.y > clipTop) ? _tri.a.y : clipTop;
				// end at middle point if it is within vertical bounds, otherwise end at bottom of block
				int end_y = (_tri.b.y < clipBottom) ? _tri.b.y : clipBottom;

				// compute start and end x
				float start_x = ((_tri.c.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.c.y - _tri.a.y))) + _tri.a.x;
				float end_x = static_cast<float>(_tri.b.x); // initializing to the middle point's x handles edge cases where a triangle has a flat top
				if (start_y < _tri.b.y)
					end_x = ((_tri.b.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.b.y - _tri.a.y))) + _tri.a.x;
				else if (start_y > _tri.b.y)
					end_x = ((_tri.c.x - _tri.b.x) * (1.0f - ((_tri.c.y - start_y) / static_cast<float>(_tri.c.y - _tri.b.y)))) + _tri.b.x;

				// flip start and end if needed to make the rest of the logic simpler
				float flip = std::numeric_limits<float>::quiet_NaN();
				if (start_x + slope_ac > end_x + slope_ab)
				{
					flip = start_x;
					start_x = end_x;
					end_x = flip;
					flip = slope_ac;
					slope_ac = slope_ab;
					slope_ab = flip;
				}

				// draw data types indicated by flags
				// color data
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					float color_start_x = start_x;
					int color_start_y = start_y;
					float color_end_x = end_x;
					int color_end_y = end_y;
					float color_slope_ab = slope_ab;
					float color_slope_ac = slope_ac;
					float color_slope_bc = slope_bc;
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; color_start_y < color_end_y; ++color_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(color_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(color_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, color_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
#if defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP) || defined(GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES)
								_resBlock.color[index] = _color;
#endif
#if !defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP)
								// draw reverse-lookup color with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, color_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.color[index] = fixedPointColorLerp(_resBlock.color[index], reverseLookupColorFromSource(_resInstr.source_id, coord));

								// draw reverse-lookup color with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.color[index] = fixedPointColorLerp(_resBlock.color[index], reverseLookupColorFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, color_start_y)));
#endif
							}
							// adjust start and end x by slope
							color_start_x += color_slope_ac;
							color_end_x += color_slope_ab;
						}
						color_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? color_slope_ab = color_slope_bc : color_slope_ac = color_slope_bc;
					}
				}
				// layer data
				float layer_start_x = start_x;
				int layer_start_y = start_y;
				float layer_end_x = end_x;
				int layer_end_y = end_y;
				float layer_slope_ab = slope_ab;
				float layer_slope_ac = slope_ac;
				float layer_slope_bc = slope_bc;
				// use source layers if flag is set, otherwise use override value from instruction
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, layer_start_y));
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = _resInstr.layer_override;

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = _resInstr.layer_override;
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				// stencil data
				float stencil_start_x = start_x;
				int stencil_start_y = start_y;
				float stencil_end_x = end_x;
				int stencil_end_y = end_y;
				float stencil_slope_ab = slope_ab;
				float stencil_slope_ac = slope_ac;
				float stencil_slope_bc = slope_bc;
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, start_y));
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = _resInstr.stencil_override;

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = _resInstr.stencil_override;
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}

				// DEBUG draw corners
#if defined(GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS)
				// max corners in each dimension are 1 pixel past the last pixels drawn to, since corner positions are geometric rather than pixel-space
				//   (pixel-space coordinates use integers only, which means a pixel-space shape spans [min, max), rather than a geometric shape which spans [min, max] )
				if (_tri.a.x >= clipLeft && _tri.a.x < clipRight && _tri.a.y >= clipTop && _tri.a.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.a.x - _resBlock.x_result, _tri.a.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.b.x >= clipLeft && _tri.b.x < clipRight && _tri.b.y >= clipTop && _tri.b.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.b.x - _resBlock.x_result, _tri.b.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.c.x >= clipLeft && _tri.c.x < clipRight && _tri.c.y >= clipTop && _tri.c.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.c.x - _resBlock.x_result, _tri.c.y - _resBlock.y_result)] = _cornerColor;
#endif
			}
			inline void drawClippedTriangle_Transparent_Masked(ResultBlock& _resBlock, const ResultInstruction& _resInstr, Triangle _tri, const Byte _dataTypesToDrawFlags, Color _color = 0x00000000, Color _cornerColor = 0x00000000)
			{
				// get clipping bounds
				int clipLeft	= _resBlock.x_result;
				int clipRight	= _resBlock.x_result + RESULT_BLOCK_WIDTH;
				int clipTop		= _resBlock.y_result;
				int clipBottom	= _resBlock.y_result + RESULT_BLOCK_HEIGHT;

				// reject triangles not within horizontal bounds
				if (_tri.a.x < clipLeft && _tri.b.x < clipLeft && _tri.c.x < clipLeft)
					return;
				if (_tri.a.x > clipRight && _tri.b.x > clipRight && _tri.c.x > clipRight)
					return;

				// sort triangle points from top to bottom
				Point swap;
				if (_tri.a.y > _tri.b.y)
				{
					swap = _tri.a;
					_tri.a = _tri.b;
					_tri.b = swap;
				}
				if (_tri.a.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.a;
					_tri.a = swap;
				}
				if (_tri.b.y > _tri.c.y)
				{
					swap = _tri.c;
					_tri.c = _tri.b;
					_tri.b = swap;
				}

				// reject triangles not within vertical bounds
				if (_tri.c.y < clipTop || _tri.a.y > clipBottom)
					return;

				// calculate slopes between all points
				float slope_ac = (_tri.c.x - _tri.a.x) / static_cast<float>(_tri.c.y - _tri.a.y);
				float slope_ab = (_tri.b.x - _tri.a.x) / static_cast<float>(_tri.b.y - _tri.a.y);
				float slope_bc = (_tri.c.x - _tri.b.x) / static_cast<float>(_tri.c.y - _tri.b.y);

				// start at top point if it is within vertical bounds, otherwise start at top of block
				int start_y = (_tri.a.y > clipTop) ? _tri.a.y : clipTop;
				// end at middle point if it is within vertical bounds, otherwise end at bottom of block
				int end_y = (_tri.b.y < clipBottom) ? _tri.b.y : clipBottom;

				// compute start and end x
				float start_x = ((_tri.c.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.c.y - _tri.a.y))) + _tri.a.x;
				float end_x = static_cast<float>(_tri.b.x); // initializing to the middle point's x handles edge cases where a triangle has a flat top
				if (start_y < _tri.b.y)
					end_x = ((_tri.b.x - _tri.a.x) * ((start_y - _tri.a.y) / static_cast<float>(_tri.b.y - _tri.a.y))) + _tri.a.x;
				else if (start_y > _tri.b.y)
					end_x = ((_tri.c.x - _tri.b.x) * (1.0f - ((_tri.c.y - start_y) / static_cast<float>(_tri.c.y - _tri.b.y)))) + _tri.b.x;

				// flip start and end if needed to make the rest of the logic simpler
				float flip = std::numeric_limits<float>::quiet_NaN();
				if (start_x + slope_ac > end_x + slope_ab)
				{
					flip = start_x;
					start_x = end_x;
					end_x = flip;
					flip = slope_ac;
					slope_ac = slope_ab;
					slope_ab = flip;
				}

				// draw data types indicated by flags
				// color data
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR)
				{
					float color_start_x = start_x;
					int color_start_y = start_y;
					float color_end_x = end_x;
					int color_end_y = end_y;
					float color_slope_ab = slope_ab;
					float color_slope_ac = slope_ac;
					float color_slope_bc = slope_bc;
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; color_start_y < color_end_y; ++color_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(color_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(color_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, color_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
#if defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP) || defined(GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES)
								_resBlock.color[index] = _color;
#endif
#if !defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP)
								// draw reverse-lookup color with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, color_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
								{
									// sample color from source and XOR it with instruction's mask color
									Color color_pseudoSrc = reverseLookupColorFromSource(_resInstr.source_id, coord) ^ _resInstr.mask_color;
									// use pseudo-source color to generate blending mask value
									Color color_mask = (-1 + (((color_pseudoSrc | (~color_pseudoSrc + 1)) >> 31) & 1));
									// draw color to result using alpha blending and mask (color blended will either be existing color in result or new color from source)
									_resBlock.color[index] = fixedPointColorLerp(_resBlock.color[index], (_resBlock.color[index] & color_mask) | ((color_pseudoSrc ^ _resInstr.mask_color) & (~color_mask)));
								}

								// draw reverse-lookup color with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//// sample color from source and XOR it with instruction's mask color
								//Color color_pseudoSrc = reverseLookupColorFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, color_start_y)) ^ _resInstr.mask_color;
								//// use pseudo-source color to generate blending mask value
								//Color color_mask = (-1 + (((color_pseudoSrc | (~color_pseudoSrc + 1)) >> 31) & 1));
								//// draw color to result using alpha blending and mask (color blended will either be existing color in result or new color from source)
								//_resBlock.color[index] = fixedPointColorLerp(_resBlock.color[index], (_resBlock.color[index] & color_mask) | ((color_pseudoSrc ^ _resInstr.mask_color & ~color_mask)));
#endif
							}
							// adjust start and end x by slope
							color_start_x += color_slope_ac;
							color_end_x += color_slope_ab;
						}
						color_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? color_slope_ab = color_slope_bc : color_slope_ac = color_slope_bc;
					}
				}
				// layer data
				float layer_start_x = start_x;
				int layer_start_y = start_y;
				float layer_end_x = end_x;
				int layer_end_y = end_y;
				float layer_slope_ab = slope_ab;
				float layer_slope_ac = slope_ac;
				float layer_slope_bc = slope_bc;
				// use source layers if flag is set, otherwise use override value from instruction
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = reverseLookupLayerFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, layer_start_y));
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; layer_start_y < layer_end_y; ++layer_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(layer_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(layer_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, layer_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup layer with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, layer_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.layer[index] = _resInstr.layer_override;

								// draw reverse-lookup layer with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.layer[index] = _resInstr.layer_override;
							}
							// adjust start and end x by slope
							layer_start_x += layer_slope_ac;
							layer_end_x += layer_slope_ab;
						}
						layer_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? layer_slope_ab = layer_slope_bc : layer_slope_ac = layer_slope_bc;
					}
				// stencil data
				float stencil_start_x = start_x;
				int stencil_start_y = start_y;
				float stencil_end_x = end_x;
				int stencil_end_y = end_y;
				float stencil_slope_ab = slope_ab;
				float stencil_slope_ac = slope_ac;
				float stencil_slope_bc = slope_bc;
				if (_dataTypesToDrawFlags & INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL)
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, coord);

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = reverseLookupStencilFromSource(_resInstr.source_id, getResInstrReverseLookupCoord(_resInstr, left, start_y));
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}
				else
					// execute the following logic twice
					for (int i = 0; i < 2; ++i)
					{
						// loop while start_y < end_y (might be skipped)
						for (; stencil_start_y < stencil_end_y; ++stencil_start_y)
						{
							// clip start and end x to boundary (temporary)
							int left = static_cast<int>(stencil_start_x + 0.5f);
							if (left < clipLeft)
								left = clipLeft;
							int right = static_cast<int>(stencil_end_x + 0.5f);
							if (right > clipRight)
								right = clipRight;
							unsigned int index = getResultBlockPixelIndex(left - _resBlock.x_result, stencil_start_y - _resBlock.y_result); // offset to be relative to this result block
							// traverse and fill
							for (; left < right; ++left, ++index)
							{
								// draw reverse-lookup stencil with false-positive checking
								//   (theoretically, this error checking doesn't need to be here; however, at the moment the triangle drawing algorithm produces false
								//     positives around the edges, which can cause an out-of-bounds exception when trying to reverse-lookup colors for those pixels)
								Vector2 coord = getResInstrReverseLookupCoord(_resInstr, left, stencil_start_y);
								if (checkResInstrReverseLookupCoord(_resInstr, coord))
									_resBlock.stencil[index] = _resInstr.stencil_override;

								// draw reverse-lookup stencil with no error checking
								//   (this can't be used until the false positives mentioned above have been dealt with)
								//_resBlock.stencil[index] = _resInstr.stencil_override;
							}
							// adjust start and end x by slope
							stencil_start_x += stencil_slope_ac;
							stencil_end_x += stencil_slope_ab;
						}
						stencil_end_y = (_tri.c.y < clipBottom) ? static_cast<int>(_tri.c.y) : clipBottom;
						// if direction was flipped previously, adjust slope traversal
						std::isnan(flip) ? stencil_slope_ab = stencil_slope_bc : stencil_slope_ac = stencil_slope_bc;
					}

				// DEBUG draw corners
#if defined(GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS)
				// max corners in each dimension are 1 pixel past the last pixels drawn to, since corner positions are geometric rather than pixel-space
				//   (pixel-space coordinates use integers only, which means a pixel-space shape spans [min, max), rather than a geometric shape which spans [min, max] )
				if (_tri.a.x >= clipLeft && _tri.a.x < clipRight && _tri.a.y >= clipTop && _tri.a.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.a.x - _resBlock.x_result, _tri.a.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.b.x >= clipLeft && _tri.b.x < clipRight && _tri.b.y >= clipTop && _tri.b.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.b.x - _resBlock.x_result, _tri.b.y - _resBlock.y_result)] = _cornerColor;
				if (_tri.c.x >= clipLeft && _tri.c.x < clipRight && _tri.c.y >= clipTop && _tri.c.y < clipBottom)
					_resBlock.color[getResultBlockPixelIndex(_tri.c.x - _resBlock.x_result, _tri.c.y - _resBlock.y_result)] = _cornerColor;
#endif
			}

#pragma endregion PRIVATE_FLUSH_FUNCTIONS

		public:
#pragma region CREATE_AND_DESTROY_FUNCTIONS

			~GBlitterImplementation()
			{
				m_sources.clear();
				m_tiles.clear();
				m_result.data.clear();
			}

			GReturn Create(unsigned short _width, unsigned short _height)
			{
				// validate arguments
				if (_width < 1 || _height < 1)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// initialize result
				initResult(_width, _height);
				// create GConcurrent for flushing instructions to result
				m_gFlushThread.Create(false);
				// clear result
				Clear(0xff000000, 255, 0);
				// return success
				return GReturn::SUCCESS;
			}

#pragma endregion CREATE_AND_DESTROY_FUNCTIONS
#pragma region IMPORT_FUNCTIONS

			GReturn LoadSource(
				const char* _colorsFilepath, const char* _layersFilepath, const char* _stencilsFilepath,
				unsigned short &_outIndex) override
			{
				// validate arguments
				// ensure max sources would not be exceeded
				if (m_sources.size() > MAX_SOURCES)
					return GReturn::IGNORED;
				// ensure at least one filepath was passed
				if (_colorsFilepath == nullptr
					&& _layersFilepath == nullptr
					&& _stencilsFilepath == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// determine which file paths are used
				SOURCE_FILE_TYPE colorFiletype = _colorsFilepath ? SOURCE_FILE_TYPE::UNSUPPORTED : SOURCE_FILE_TYPE::UNUSED;
				SOURCE_FILE_TYPE layerFiletype = _layersFilepath ? SOURCE_FILE_TYPE::UNSUPPORTED : SOURCE_FILE_TYPE::UNUSED;
				SOURCE_FILE_TYPE stencilFiletype = _stencilsFilepath ? SOURCE_FILE_TYPE::UNSUPPORTED : SOURCE_FILE_TYPE::UNUSED;
				// ensure used file paths have extensions (if filtering file extension fails, filepath does not have a valid extension)
				if (colorFiletype != SOURCE_FILE_TYPE::UNUSED)
					if (-filterFileExtension(_colorsFilepath, colorFiletype))
						return GReturn::INVALID_ARGUMENT;
				if (layerFiletype != SOURCE_FILE_TYPE::UNUSED)
					if (-filterFileExtension(_layersFilepath, layerFiletype))
						return GReturn::INVALID_ARGUMENT;
				if (stencilFiletype != SOURCE_FILE_TYPE::UNUSED)
					if (-filterFileExtension(_stencilsFilepath, stencilFiletype))
						return GReturn::INVALID_ARGUMENT;
				// ensure file types are supported (a valid extension does not guarantee a supported extension)
				if (   colorFiletype == SOURCE_FILE_TYPE::UNSUPPORTED
					|| layerFiletype == SOURCE_FILE_TYPE::UNSUPPORTED
					|| stencilFiletype == SOURCE_FILE_TYPE::UNSUPPORTED)
					return GReturn::FORMAT_UNSUPPORTED;
				// if arguments are valid, continue

				// store pixel data types in source data buffer (the buffer cannot be initialized until the first time data is read from a file, since image dimensions are unknown before that; this initialization happens in the data read helper functions)
				SourceDataBuffer sourceDataBuffer = {};
				if (colorFiletype != SOURCE_FILE_TYPE::UNUSED)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR;
				if (layerFiletype != SOURCE_FILE_TYPE::UNUSED)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER;
				if (stencilFiletype != SOURCE_FILE_TYPE::UNUSED)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL;

				// attempt to read data from files
				GReturn gr;
				// color
				if (colorFiletype != SOURCE_FILE_TYPE::UNUSED)
				{
					switch (colorFiletype)
					{
						case SOURCE_FILE_TYPE::TGA:
							gr = readSourceDataFromTGA(_colorsFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR, sourceDataBuffer);
							break;
						case SOURCE_FILE_TYPE::BMP:
							gr = readSourceDataFromBMP(_colorsFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR, sourceDataBuffer);
							break;
						default:
							gr = GReturn::FORMAT_UNSUPPORTED;
							break;
					}
					// ensure data was read; if not, discard any source data and return the failure code
					if (G_FAIL(gr))
					{
						discardSourceDataBuffer(sourceDataBuffer);
						return gr;
					}
				}
				// layer
				if (layerFiletype != SOURCE_FILE_TYPE::UNUSED)
				{
					switch (layerFiletype)
					{
						case SOURCE_FILE_TYPE::TGA:
							gr = readSourceDataFromTGA(_layersFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER, sourceDataBuffer);
							break;
						case SOURCE_FILE_TYPE::BMP:
							gr = readSourceDataFromBMP(_layersFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER, sourceDataBuffer);
							break;
						default:
							gr = GReturn::FORMAT_UNSUPPORTED;
							break;
					}
					// ensure data was read; if not, discard any source data and return the failure code
					if (G_FAIL(gr))
					{
						discardSourceDataBuffer(sourceDataBuffer);
						return gr;
					}
				}
				// stencil
				if (stencilFiletype != SOURCE_FILE_TYPE::UNUSED)
				{
					switch (stencilFiletype)
					{
						case SOURCE_FILE_TYPE::TGA:
							gr = readSourceDataFromTGA(_stencilsFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL, sourceDataBuffer);
							break;
						case SOURCE_FILE_TYPE::BMP:
							gr = readSourceDataFromBMP(_stencilsFilepath, INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL, sourceDataBuffer);
							break;
						default:
							gr = GReturn::FORMAT_UNSUPPORTED;
							break;
					}
					// ensure data was read; if not, discard any source data and return the failure code
					if (G_FAIL(gr))
					{
						discardSourceDataBuffer(sourceDataBuffer);
						return gr;
					}
				}

				// create and store source, and return index to user
				createAndStoreSource(sourceDataBuffer, _outIndex);
				// discard loaded data since it is no longer needed
				discardSourceDataBuffer(sourceDataBuffer);

				return GReturn::SUCCESS;
			}
			GReturn ImportSource(
				const Color* _colors, const Layer* _layers, const Stencil* _stencils,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				unsigned short& _outIndex) override
			{
				// validate arguments
				// ensure max sources would not be exceeded
				if (m_sources.size() > MAX_SOURCES)
					return GReturn::IGNORED;
				// ensure at least one data type was passed
				if (_colors == nullptr
					&& _layers == nullptr
					&& _stencils == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure width and height are nonzero
				if (_width < 1 || _height < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure stride is not less than width, if nonzero
				if (_rowPixelStride > 0 && _rowPixelStride < _width)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// if stride is zero, set it to width
				if (_rowPixelStride == 0)
					_rowPixelStride = _width;
				// create source data buffer to copy data into for transfer into source
				SourceDataBuffer sourceDataBuffer = {};
				if (_colors)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR;
				if (_layers)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER;
				if (_stencils)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL;
				initSourceDataBuffer(sourceDataBuffer, _width, _height);
				// copy data into source data buffer
				for (unsigned short y = 0; y < _height; ++y)
				{
					unsigned int inputIndex = _pixelOffset + (y * _rowPixelStride);
					unsigned int bufferIndex = y * _width;
					// copy row of pixel data
					if (_colors)
						memcpy(&sourceDataBuffer.colors[bufferIndex], &_colors[inputIndex], _width << 2);
					// copy row of layer data, if passed
					if (_layers)
						memcpy(&sourceDataBuffer.layers[bufferIndex], &_layers[inputIndex], _width);
					// copy row of stencil data, if passed
					if (_stencils)
						memcpy(&sourceDataBuffer.stencils[bufferIndex], &_stencils[inputIndex], _width);
				}
				// create and store source, and return index to user
				createAndStoreSource(sourceDataBuffer, _outIndex);
				// discard source buffer since it is no longer needed
				discardSourceDataBuffer(sourceDataBuffer);
				// return success
				return GReturn::SUCCESS;
			}
			GReturn ImportSourceComplex(
				const Color* _colors, unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				const Layer* _layers, unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				const Stencil* _stencils, unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height, unsigned short& _outIndex) override
			{
				// validate arguments
				// ensure max sources would not be exceeded
				if (m_sources.size() > MAX_SOURCES)
					return GReturn::IGNORED;
				// ensure at least one data type was passed
				if (_colors == nullptr
					&& _layers == nullptr
					&& _stencils == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure width and height are nonzero
				if (_width < 1 || _height < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure data byte and row strides are nonzero, if used
				if (_colors && (_colorByteStride < 1 || _colorRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				if (_layers && (_layerByteStride < 1 || _layerRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				if (_stencils && (_stencilByteStride < 1 || _stencilRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				// ensure row strides are >= width * byte strides, if used
				if (_colors && _colorRowByteStride < _colorByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				if (_layers && _layerRowByteStride < _layerByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				if (_stencils && _stencilRowByteStride < _stencilByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				// ensure data byte strides are <= row strides, if used
				if (_colors && _colorByteStride > _colorRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				if (_layers && _layerByteStride > _layerRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				if (_stencils && _stencilByteStride > _stencilRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// create source data buffer to copy data into for transfer into source
				SourceDataBuffer sourceDataBuffer = {};
				if (_colors)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::COLOR;
				if (_layers)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::LAYER;
				if (_stencils)
					sourceDataBuffer.dataTypes |= INTERNAL_PIXEL_ATTRIBUTES::ELEMENT::STENCIL;
				initSourceDataBuffer(sourceDataBuffer, _width, _height);
				// copy data into source data buffer
				if (_colors)
				{
					if (_colorByteStride == sizeof(Color)) // if colors are contiguous, memcpy can be used
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readColor = reinterpret_cast<const Byte*>(_colors) + (y * _colorRowByteStride);
							memcpy(&sourceDataBuffer.colors[y * _width], readColor, _width << 2);
						} // end iterate through color rows
					else // otherwise, use pointer arithmetic instead
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readColor = reinterpret_cast<const Byte*>(_colors) + (y * _colorRowByteStride);
							for (unsigned short x = 0; x < _width; ++x)
							{
								sourceDataBuffer.colors[x + y * _width] = *(reinterpret_cast<const unsigned int*>(readColor));
								readColor += _colorByteStride;
							} // end iterate through colors in row
						} // end iterate through color rows
				} // end copy colors
				if (_layers)
				{
					if (_layerByteStride == sizeof(Layer)) // if layers are contiguous, memcpy can be used
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readLayer = reinterpret_cast<const Byte*>(_layers) + (y * _layerRowByteStride);
							memcpy(&sourceDataBuffer.layers[y * _width], readLayer, _width);
						} // end iterate through layer rows
					else
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readLayer = reinterpret_cast<const Byte*>(_layers) + (y * _layerRowByteStride);
							for (unsigned short x = 0; x < _width; ++x)
							{
								sourceDataBuffer.layers[x + (y * _width)] = *readLayer;
								readLayer += _layerByteStride;
							} // end iterate through layers in row
						} // end iterate through layer rows
				} // end copy layers
				if (_stencils)
				{
					if (_stencilByteStride == sizeof(Stencil)) // if stencils are contiguous, memcpy can be used
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readStencil = reinterpret_cast<const Byte*>(_stencils) + (y *  _stencilRowByteStride);
							memcpy(&sourceDataBuffer.stencils[y * _width], readStencil, _width);
						} // end iterate through stencil rows
					else
						for (unsigned short y = 0; y < _height; ++y)
						{
							const Byte* readStencil = reinterpret_cast<const Byte*>(_stencils) + (y *  _stencilRowByteStride);
							for (unsigned short x = 0; x < _width; ++x)
							{
								sourceDataBuffer.stencils[x + (y * _width)] = *readStencil;
								readStencil += _stencilByteStride;
							} // end iterate through stencils in row
						} // end iterate through stencil rows
				} // end copy stencils
				// create and store source, and return index to user
				createAndStoreSource(sourceDataBuffer, _outIndex);
				// discard source buffer since it is no longer needed
				discardSourceDataBuffer(sourceDataBuffer);
				// return success
				return GReturn::SUCCESS;
			}
			GReturn DefineTiles(
				const TileDefinition* _tileDefinitions,
				const unsigned int _numTiles, unsigned int *_outIndices) override
			{
				// validate arguments
				// ensure valid tile definition array was passed
				if (_tileDefinitions == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure valid index result array was passed
				if (_outIndices == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure tile count is nonzero
				if (_numTiles < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure max number of tiles would not be exceeded
				if (static_cast<unsigned long long>(m_tiles.size() + _numTiles) > MAX_TILES)
					return GReturn::IGNORED;
				// ensure tile definitions are valid
				bool definitionsValid = true;
				for (unsigned int i = 0; i < _numTiles; ++i)
				{
					TileDefinition def = _tileDefinitions[i];
					// ensure source index is set
					if (!(~def.source_id))
					{
						definitionsValid = false;
						break;
					}
					// ensure source index is in factory
					if (!m_sources.is_valid(def.source_id))
					{
						definitionsValid = false;
						break;
					}
					Source& source = m_sources[def.source_id];
					// ensure tile dimensions are nonzero
					if (def.w < 1 || def.h < 1)
					{
						definitionsValid = false;
						break;
					}
					// ensure tile origin is within source
					if (def.x >= source.w_image || def.y >= source.h_image)
					{
						definitionsValid = false;
						break;
					}
					// ensure tile bounds are within source bounds
					if (def.x + def.w > source.w_image
						|| def.y + def.h > source.h_image)
					{
						definitionsValid = false;
						break;
					}
				}
				if (!definitionsValid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// add tile definitions to pool
				for (unsigned int i = 0; i < _numTiles; ++i)
					_outIndices[i] = m_tiles.push_back(_tileDefinitions[i]);
				// return success
				return GReturn::SUCCESS;
			}
			GReturn SetTileMaskValues(
				unsigned int* _tileIndices, unsigned int _numTiles,
				Color _maskColor, Layer _maskLayer, Stencil _maskStencil) override
			{
				// validate arguments
				// ensure valid index array was passed
				if (_tileIndices == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices is nonzero
				if (_numTiles < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices does not exceed number of tiles
				if (_numTiles > m_tiles.size())
					return GReturn::INVALID_ARGUMENT;
				// ensure indices are valid
				bool indicesValid = true;
				for (unsigned short i = 0; i < _numTiles; ++i)
				{
					// ensure index is in the tile pool
					if (!m_tiles.is_valid(_tileIndices[i]))
					{
						indicesValid = false;
						break;
					}
				}
				if (!indicesValid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// iterate through list of indices and set values for each one
				for (unsigned int i = 0; i < _numTiles; ++i)
				{
					TileDefinition& tile = m_tiles[_tileIndices[i]];
					tile.mask_color = _maskColor;
					tile.mask_layer = _maskLayer;
					tile.mask_stencil = _maskStencil;
				}
				// return success
				return GReturn::SUCCESS;
			}

#pragma endregion IMPORT_FUNCTIONS
#pragma region DISCARD_FUNCTIONS

			GReturn DiscardSources(unsigned short* _sourceIndices, unsigned short _numSources) override
			{
				// validate arguments
				// ensure valid index array was passed
				if (_sourceIndices == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices is nonzero
				if (_numSources < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices does not exceed number of sources
				if (_numSources > m_sources.size())
					return GReturn::INVALID_ARGUMENT;
				// ensure indices are valid
				bool indicesValid = true;
				for (unsigned short i = 0; i < _numSources; ++i)
				{
					// ensure index is in the source pool
					if (!m_sources.is_valid(_sourceIndices[i]))
					{
						indicesValid = false;
						break;
					}
				}
				if (!indicesValid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// remove sources from pool
				for (unsigned int i = 0; i < _numSources; ++i)
					m_sources.set_valid(_sourceIndices[i], false);
				// return success
				return GReturn::SUCCESS;
			}
			GReturn DiscardTiles(unsigned int* _tileIndices, unsigned int _numTiles) override
			{
				// validate arguments
				// ensure valid index array was passed
				if (_tileIndices == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices is nonzero
				if (_numTiles < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of indices does not exceed number of tiles
				if (_numTiles > m_tiles.size())
					return GReturn::INVALID_ARGUMENT;
				// ensure indices are valid
				bool indicesValid = true;
				for (unsigned short i = 0; i < _numTiles; ++i)
				{
					// ensure index is in the tile pool
					if (!m_tiles.is_valid(_tileIndices[i]))
					{
						indicesValid = false;
						break;
					}
				}
				if (!indicesValid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// remove tiles from pool
				for (unsigned int i = 0; i < _numTiles; ++i)
					m_tiles.remove(_tileIndices[i]);
				// return success
				return GReturn::SUCCESS;
			}

#pragma endregion DISCARD_FUNCTIONS
#pragma region DRAW_FUNCTIONS

			GReturn Clear(Color _color, Layer _layer, Stencil _stencil) override
			{
				ClearColor(_color);
				ClearLayer(_layer);
				ClearStencil(_stencil);
				// return success (this operation cannot reasonably fail)
				return GReturn::SUCCESS;
			}
			GReturn ClearColor(Color _color) override
			{
				// clear first row of first block
				for (unsigned short x = 0; x < RESULT_BLOCK_WIDTH; ++x)
					m_result.data[0].color[x]	= _color;
				// copy cleared row into other rows of first block
				for (unsigned short y = 1; y < RESULT_BLOCK_HEIGHT; ++y)
					memcpy(&m_result.data[0].color[y * RESULT_BLOCK_WIDTH], &m_result.data[0].color[0], RESULT_BLOCK_WIDTH << 2);
				// copy cleared block into other blocks
				for (unsigned short b = 1; b < m_result.data.size(); ++b)
					memcpy(&m_result.data[b].color[0], &m_result.data[0].color[0], (RESULT_BLOCK_WIDTH * RESULT_BLOCK_HEIGHT) << 2);
				// return success (this operation cannot reasonably fail)
				return GReturn::SUCCESS;
			}
			GReturn ClearLayer(Layer _layer) override
			{
				// clear first row of first block
				for (unsigned short x = 0; x < RESULT_BLOCK_WIDTH; ++x)
					m_result.data[0].layer[x]	= _layer;
				// copy cleared row into other rows of first block
				for (unsigned short y = 1; y < RESULT_BLOCK_HEIGHT; ++y)
					memcpy(&m_result.data[0].layer[y * RESULT_BLOCK_WIDTH], &m_result.data[0].layer[0], RESULT_BLOCK_WIDTH);
				// copy cleared block into other blocks
				for (unsigned short b = 1; b < m_result.data.size(); ++b)
					memcpy(&m_result.data[b].layer[0], &m_result.data[0].layer[0], (RESULT_BLOCK_WIDTH * RESULT_BLOCK_HEIGHT));
				// return success (this operation cannot reasonably fail)
				return GReturn::SUCCESS;
			}
			GReturn ClearStencil(Stencil _stencil) override
			{
				// clear first row of first block
				for (unsigned short x = 0; x < RESULT_BLOCK_WIDTH; ++x)
					m_result.data[0].stencil[x]	= _stencil;
				// copy cleared row into other rows of first block
				for (unsigned short y = 1; y < RESULT_BLOCK_HEIGHT; ++y)
					memcpy(&m_result.data[0].stencil[y * RESULT_BLOCK_WIDTH], &m_result.data[0].stencil[0], RESULT_BLOCK_WIDTH);
				// copy cleared block into other blocks
				for (unsigned short b = 1; b < m_result.data.size(); ++b)
					memcpy(&m_result.data[b].stencil[0], &m_result.data[0].stencil[0], (RESULT_BLOCK_WIDTH * RESULT_BLOCK_HEIGHT));
				// return success (this operation cannot reasonably fail)
				return GReturn::SUCCESS;
			}

			GReturn DrawDeferred(const DrawInstruction* _drawInstructions, const unsigned short _numInstructions) override
			{
				// validate arguments
				// ensure valid instruction array was passed
				if (_drawInstructions == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of instructions is nonzero
				if (_numInstructions < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure instructions are valid
				bool instructionsvalid = true;
				for (unsigned short i = 0; i < _numInstructions; ++i)
				{
					const DrawInstruction& instr = static_cast<DrawInstruction>(_drawInstructions[i]);
					// ensure tile index is set
					if (!(~instr.tile_id))
					{
						instructionsvalid = false;
						break;
					}
					// ensure tile index is in factory
					if (!m_tiles.is_valid(instr.tile_id))
					{
						instructionsvalid = false;
						break;
					}
				}
				if (!instructionsvalid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// convert draw instructions to result instructions
				processDrawInstructions_Deferred(_drawInstructions, _numInstructions);
				// return success
				return GReturn::SUCCESS;
			}
			GReturn DrawImmediate(const DrawInstruction* _drawInstructions, const unsigned short _numInstructions) override
			{
				// validate arguments
				// ensure valid instruction array was passed
				if (_drawInstructions == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure number of instructions is nonzero
				if (_numInstructions < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure instructions are valid
				bool instructionsvalid = true;
				for (unsigned short i = 0; i < _numInstructions; ++i)
				{
					const DrawInstruction& instr = static_cast<DrawInstruction>(_drawInstructions[i]);
					// ensure tile index is set
					if (!(~instr.tile_id))
					{
						instructionsvalid = false;
						break;
					}
					// ensure tile index is in factory
					if (!m_tiles.is_valid(instr.tile_id))
					{
						instructionsvalid = false;
						break;
					}
				}
				if (!instructionsvalid)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// convert draw instructions to result instructions and draw them to the result
				processDrawInstructions_Immediate(const_cast<DrawInstruction*>(_drawInstructions), _numInstructions);
				// return success
				return GReturn::SUCCESS;
			}

#pragma endregion DRAW_FUNCTIONS
#pragma region FLUSH_FUNCTIONS

			GReturn Flush() override
			{
				GReturn result = GReturn::REDUNDANT;

#ifdef GBLITTER_DEBUG_SERIAL_FLUSH
				// iterate through result blocks
				for (unsigned short b = 0; b < m_result.size_data; ++b) {
					// flush the current block and store the result of the operation
					m_result.flush_results[b] = flushResultBlock_Serial(m_result.data[b]);
				}
#else // process blocks in parallel with GConcurrent; default
				
				// flush all blocks in parallel, each block in the result gets one job in the thread pool
				// since blocks are independent, they can be processed in parallel
				// each block has a varying amount of work to do, so the thread pool will balance the load
				m_gFlushThread.BranchParallel(flushResultBlock_Parallel, 1,
					m_result.size_data, reinterpret_cast<const void*>(this), 0,
					static_cast<const void*>(nullptr), 0, m_result.data.data());
				
				// wait for all instructions to be processed
				m_gFlushThread.Converge(1000000); // 1ms spin wait
#endif
				// determine the result of the flush (if at least one block was flushed, operation was successful; otherwise, operation was redundant)
				for (unsigned short b = 0; b < m_result.size_data; ++b) {
					if (m_result.flush_results[b] == GReturn::SUCCESS) {
						result = GReturn::SUCCESS;
						break;
					}
				}
				return result;
			}

#pragma endregion FLUSH_FUNCTIONS
#pragma region EXPORT_FUNCTIONS

			GReturn ExportResult(
				bool _flush,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				Color* _outColors, Layer* _outLayers, Stencil* _outStencils) override
			{
				// validate arguments
				// ensure at least one output buffer was passed
				if (_outColors == nullptr
					&& _outLayers == nullptr
					&& _outStencils == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure width and height are nonzero
				if (_width < 1 || _height < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure stride is not lower than width if stride is nonzero
				if (_rowPixelStride > 0 && _rowPixelStride < _width)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// flush any outstanding instructions to the result, if requested
				if (_flush)
					Flush();
				// if stride is zero, set to width
				if (_rowPixelStride == 0)
					_rowPixelStride = _width;
				// set values to use while transferring data
				unsigned short w = (_width < m_result.w_image) ? _width : m_result.w_image; // only copy as much data as will fit into output buffers/only copy as much data as is in result
				unsigned short h = (_height < m_result.h_image) ? _height : m_result.h_image; // same on y
				// determine how many blocks will fit in output buffers
				unsigned short blockSpanX = w / RESULT_BLOCK_WIDTH + ((w % RESULT_BLOCK_WIDTH) ? 1 : 0);
				unsigned short blockSpanY = h / RESULT_BLOCK_HEIGHT + ((h % RESULT_BLOCK_HEIGHT) ? 1 : 0);
				// iterate through blocks
				for (unsigned short blockY = 0; blockY < blockSpanY; ++blockY)
					for (unsigned short blockX = 0; blockX < blockSpanX; ++blockX)
					{
						// store block by reference for neatness
						ResultBlock& block = m_result.data[blockX + (blockY * m_result.w_data)];
						// calculate block x/y in pixels
						unsigned short x_resultBlock = blockX * RESULT_BLOCK_WIDTH;
						unsigned short y_resultBlock = blockY * RESULT_BLOCK_HEIGHT;
						// determine region to copy
						unsigned short regionWidth = ((x_resultBlock + RESULT_BLOCK_WIDTH) > w) ? w - x_resultBlock : RESULT_BLOCK_WIDTH; // if entire width won't fit, clamp to output buffer width
						unsigned short regionHeight = ((y_resultBlock + RESULT_BLOCK_HEIGHT) > h) ? h - y_resultBlock : RESULT_BLOCK_HEIGHT; // same on y
						// determine starting index of region in output buffers
						unsigned int outputStartIndex = _pixelOffset + x_resultBlock + (y_resultBlock * _rowPixelStride);

						// iterate through rows in block and copy data to output buffers
						if (_outColors)
							for (unsigned short y = 0; y < regionHeight; ++y)
								memcpy(&_outColors[outputStartIndex + (y * _rowPixelStride)], &block.color[y * RESULT_BLOCK_WIDTH], regionWidth << 2);
						if (_outLayers)
							for (unsigned short y = 0; y < regionHeight; ++y)
								memcpy(&_outLayers[outputStartIndex + (y * _rowPixelStride)], &block.layer[y * RESULT_BLOCK_WIDTH], regionWidth);
						if (_outStencils)
							for (unsigned short y = 0; y < regionHeight; ++y)
								memcpy(&_outStencils[outputStartIndex + (y * _rowPixelStride)], &block.stencil[y * RESULT_BLOCK_WIDTH], regionWidth);
					}
				// return success
				return GReturn::SUCCESS;
			}
			GReturn ExportResultComplex(
				bool _flush,
				unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height,
				Color* _outColors, Layer* _outLayers, Stencil* _outStencils) override
			{
				// validate arguments
				// ensure at least one output buffer was passed
				if (   _outColors == nullptr
					&& _outLayers == nullptr
					&& _outStencils == nullptr)
					return GReturn::INVALID_ARGUMENT;
				// ensure width and height are nonzero
				if (_width < 1 || _height < 1)
					return GReturn::INVALID_ARGUMENT;
				// ensure data byte and row strides are nonzero, if used
				if (_outColors && (_colorByteStride < 1 || _colorRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				if (_outLayers && (_layerByteStride < 1 || _layerRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				if (_outStencils && (_stencilByteStride < 1 || _stencilRowByteStride < 1))
					return GReturn::INVALID_ARGUMENT;
				// ensure row strides are >= width * byte strides, if used
				if (_outColors && _colorRowByteStride < _colorByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				if (_outLayers && _layerRowByteStride < _layerByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				if (_outStencils && _stencilRowByteStride < _stencilByteStride * _width)
					return GReturn::INVALID_ARGUMENT;
				// ensure data byte strides are <= row strides, if used
				if (_outColors && _colorByteStride > _colorRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				if (_outLayers && _layerByteStride > _layerRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				if (_outStencils && _stencilByteStride > _stencilRowByteStride)
					return GReturn::INVALID_ARGUMENT;
				// if arguments are valid, continue

				// flush any outstanding instructions to the result, if requested
				if (_flush)
					Flush();
				// set values to use while transferring data
				unsigned short w = (_width < m_result.w_image) ? _width : m_result.w_image; // only copy as much data as will fit into output buffers/only copy as much data as is in result
				unsigned short h = (_height < m_result.h_image) ? _height : m_result.h_image; // same on y
				// determine how many blocks will fit in output buffers
				unsigned short blockSpanX = w / RESULT_BLOCK_WIDTH + ((w % RESULT_BLOCK_WIDTH) ? 1 : 0);
				unsigned short blockSpanY = h / RESULT_BLOCK_HEIGHT + ((h % RESULT_BLOCK_HEIGHT) ? 1 : 0);
				// iterate through blocks
				for (unsigned short blockY = 0; blockY < blockSpanY; ++blockY)
					for (unsigned short blockX = 0; blockX < blockSpanX; ++blockX)
					{
						// store block by reference for neatness
						ResultBlock& block = m_result.data[blockX + (blockY * m_result.w_data)];
						// calculate block x/y in pixels
						unsigned short x_sourceBlock = blockX * RESULT_BLOCK_WIDTH;
						unsigned short y_sourceBlock = blockY * RESULT_BLOCK_HEIGHT;
						// determine region to copy
						unsigned short regionWidth = ((x_sourceBlock + RESULT_BLOCK_WIDTH) > w) ? w - x_sourceBlock : RESULT_BLOCK_WIDTH; // if entire width won't fit, clamp to output buffer width
						unsigned short regionHeight = ((y_sourceBlock + RESULT_BLOCK_HEIGHT) > h) ? h - y_sourceBlock : RESULT_BLOCK_HEIGHT; // same on y

						// copy data into output buffers
						if (_outColors)
						{
							if (_colorByteStride == sizeof(Color)) // if colors are contiguous, memcpy can be used
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeColor = reinterpret_cast<Byte*>(_outColors) + (y * _colorRowByteStride);
									memcpy(writeColor, &block.color[y * RESULT_BLOCK_WIDTH], regionWidth << 2);
								}
							else // otherwise, use pointer arithmetic instead
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeColor = reinterpret_cast<Byte*>(_outColors) + (y * _colorRowByteStride);
									for (unsigned short x = 0; x < regionWidth; ++x)
									{
										*(reinterpret_cast<unsigned int*>(writeColor)) = block.color[x + y * RESULT_BLOCK_WIDTH];
										writeColor += _colorByteStride;
									} // end iterate through colors in row
								} // end iterate through color rows
						} // end copy colors
						if (_outLayers)
						{
							if (_layerByteStride == sizeof(Layer)) // if layers are contiguous, memcpy can be used
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeLayer = reinterpret_cast<Byte*>(_outLayers) + (y * _layerRowByteStride);
									memcpy(writeLayer, &block.layer[y * RESULT_BLOCK_WIDTH], regionWidth);
								}
							else
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeLayer = reinterpret_cast<Byte*>(_outLayers) + (y * _layerRowByteStride);
									for (unsigned short x = 0; x < regionWidth; ++x)
									{
										*writeLayer = block.layer[x + y * RESULT_BLOCK_WIDTH];
										writeLayer += _layerByteStride;
									} // end iterate through layers in row
								} // end iterate through layer rows
						} // end copy layers
						if (_outStencils)
						{
							if (_stencilByteStride == sizeof(Stencil)) // if stencils are contiguous, memcpy can be used
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeStencil = reinterpret_cast<Byte*>(_outStencils) + (y * _stencilRowByteStride);
									memcpy(writeStencil, &block.stencil[y * RESULT_BLOCK_WIDTH], regionWidth);
								}
							else
								for (unsigned short y = 0; y < regionHeight; ++y)
								{
									Byte* writeStencil = reinterpret_cast<Byte*>(_outStencils) + (y * _stencilRowByteStride);
									for (unsigned short x = 0; x < regionWidth; ++x)
									{
										*writeStencil = block.stencil[x + y * RESULT_BLOCK_WIDTH];
										writeStencil += _stencilByteStride;
									} // end iterate through stencils in row
								} // end iterate through stencil rows
						} // end copy stencils
					} // end iterate through blocks
				// return success
				return GReturn::SUCCESS;
			}

#pragma endregion EXPORT_FUNCTIONS

		}; // end class GBlitterImplementation
	} // end I namespace
} // end GW namespace


#if defined(_WIN32) && !defined(NDEBUG) && defined(GBLITTER_DEBUG_VERBOSE_NEW)
#undef new
#endif

#if defined(GBLITTER_DEBUG_VERBOSE_NEW)
#undef GBLITTER_DEBUG_VERBOSE_NEW
#endif
#if defined(GBLITTER_DEBUG_SERIAL_FLUSH)
#undef GBLITTER_DEBUG_SERIAL_FLUSH
#endif
#if defined(GBLITTER_DEBUG_OUTLINE_TRANFORMED_BOUNDS)
#undef GBLITTER_DEBUG_OUTLINE_TRANFORMED_BOUNDS
#endif
#if defined(GBLITTER_DEBUG_FILL_TRANFORMED_BOUNDS)
#undef GBLITTER_DEBUG_FILL_TRANFORMED_BOUNDS
#endif
#if defined(GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES)
#undef GBLITTER_DEBUG_FILL_TRANSFORMED_TRIANGLES
#endif
#if defined(GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP)
#undef GBLITTER_DEBUG_DISABLE_TRANSFORMED_COLOR_LOOKUP
#endif
#if defined(GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS)
#undef GBLITTER_DEBUG_DRAW_TRANSFORMED_TRIANGLE_CORNERS
#endif
