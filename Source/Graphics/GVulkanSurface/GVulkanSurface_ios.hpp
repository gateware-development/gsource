// For access to UI objects.
@import UIKit;

// For access to MTKView for default Metal View layers.
@import MetalKit;

// For RUN_ON_UI_THREAD function.
#include "../../Shared/iosutils.h"

// For Vulkan objects and functions.
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_ios.h>

namespace GW
{
    namespace I
    {
		class GVulkanSurfacePlatformFucts {
		public:
            
			//Get the Surface Extension for Macos
			const char* GetPlatformSurfaceExtension() {
                return VK_MVK_IOS_SURFACE_EXTENSION_NAME;
			}

			//Get the platform name
			char* GetPlatformWindowName(const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh) {
				__block char* instance_app_name = nullptr;
				//Make sure this is on the main thread
				RUN_ON_UI_THREAD(^ {
                    
                    // On other platforms this would be the window title,
                    // iOS doesn't have those so we use the application title.
                    NSString* name = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
                    const char* const_name = [name UTF8String];
                    unsigned long long length = strlen(const_name);
                    
                    if (length)
                    {
                        instance_app_name = new char[length + 1];
                        strcpy(instance_app_name, const_name);
                    }
                    
                    // If the above failed, make a title
                    if (!instance_app_name)
                    {
                        //Default Window Title[22] (21 Characters, +1 for null terminator)
                        instance_app_name = new char[22];
                        strcpy(instance_app_name, "Gateware Application");
                    }
                    
                });
                

				//Return The Window Title
				return instance_app_name;
			}

			//Create the surface
			VkResult CreateVkSurfaceKHR(const VkInstance& vkInstance, const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh, VkSurfaceKHR& vkSurface) {
				
                //Create this as a block
				__block VkResult r = VK_ERROR_FEATURE_NOT_PRESENT;

				//Make sure this is on the main thread
				RUN_ON_UI_THREAD(^ {
                    
                    //Get the rootViewController and replace its view.
                    UIWindow * uiWindow = (__bridge UIWindow*)(uwh.window);
                    void* pView;
                    
                    /* TODO:
                        This code checks the main layer and its sublayers, but not the sublayers of the sublayers, this
                        should be considered to ensure that the user definitely doesn't have a metal compatible view.
                    */
                    
                    // Check if the main layer is Metal-Capable.
                    bool hasMetalLayerAttached = false;
                    if (![uiWindow.rootViewController.view.layer isMemberOfClass:[CAMetalLayer class]])
                    {
                        // Check if any of its sublayers are Metal-Capable.
                        for (int i = 0; i < uiWindow.rootViewController.view.layer.sublayers.count; i++) {
                            if ([uiWindow.rootViewController.view.layer.sublayers[i] isMemberOfClass:[CAMetalLayer class]])
                            {
                                hasMetalLayerAttached = true;
                                pView = (__bridge void*)(uiWindow.rootViewController.view.layer.sublayers[i]);
                                break;
                            }
                        }
                        // If not suitable Metal layer is found, replace the View with a MTKView.
                        if (!hasMetalLayerAttached)
                        {
                            // Grab the controller and the new View
                            UIViewController *newRootViewController = [[UIViewController alloc] init];                            
                            MTKView* uiv = [[MTKView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                            
                            // Replace the view and set a background color for validation
                            newRootViewController.view = uiv;
                            newRootViewController.view.backgroundColor = [UIColor redColor];
                            
                            // Replace the view-controller and make the view visible.
                            uiWindow.rootViewController = newRootViewController;
                            [uiWindow makeKeyAndVisible];
                            
                            pView = (__bridge void*)(uiWindow.rootViewController.view);
                        }
                    }
                    else
                    {
                        pView = (__bridge void*)(uiWindow.rootViewController.view.layer);
                    }
                    
                    //Setup Create Info
                    VkIOSSurfaceCreateInfoMVK create_info = {};
                    create_info.sType = VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK;
                    create_info.flags = 0;
                    create_info.pNext = VK_NULL_HANDLE;
                    create_info.pView = pView;

                    //Create the Surface
                    r = vkCreateIOSSurfaceMVK(vkInstance, &create_info, VK_NULL_HANDLE, &vkSurface);
                    
                });
				return r;
			}

			VkResult PlatformDestroyGVulkanSurface(const std::function<void()>& callback) {
				//Cleanup Vulkan Surface
				callback();

				//Return Success
				return VK_SUCCESS;
			}
		};
    }
}

#include "GVulkanSurface_core.hpp"
