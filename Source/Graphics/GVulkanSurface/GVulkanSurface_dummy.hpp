//The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//The namespace to which all Gateware internal implementation must belong.
	namespace I
	{
		class GVulkanSurfaceImplementation :	public virtual GVulkanSurfaceInterface,
												public GEventGeneratorImplementation
		{
		public:
            //Create
			GReturn Create(SYSTEM::GWindow _gwindow, unsigned long long _initMask) { return GReturn::FEATURE_UNSUPPORTED; }
			GReturn Create(SYSTEM::GWindow _gWindow, unsigned long long _initMask, unsigned int _layerCount, const char** _layers,
				unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount, const char** _deviceExtensions,
				bool allPhysicalDeviceFeatures) { return GReturn::FEATURE_UNSUPPORTED; }
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask, unsigned int _layerCount, const char** _layers, unsigned int _layerSettingsEXTCount, 
				const void* _layerSettingsEXT, unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount, 
				const char** _deviceExtensions, bool _allPhysicalDeviceFeatures) { return GReturn::FEATURE_UNSUPPORTED; }

			GReturn Create(SYSTEM::GWindow _gWindow, GVulkanSurfaceQueryInfo** _queryInfo) { return GReturn::FEATURE_UNSUPPORTED; }

            //GVulkanSurface
			GReturn GetAspectRatio(float& _outRatio) const override              		  { return GReturn::FAILURE; }
            GReturn GetSwapchainImageCount(unsigned int& _outImageCount) const override   { return GReturn::FAILURE; }
			GReturn GetSwapchainCurrentImage(unsigned int& _outImageIndex) const override { return GReturn::FAILURE; }
			GReturn GetGraphicsQueue(void** _outVkQueue) const override         		  { return GReturn::FAILURE; }
			GReturn GetPresentQueue(void** _outVkQueue) const override           		  { return GReturn::FAILURE; }
			GReturn GetQueueFamilyIndices(unsigned int& _outGraphicsIndex, unsigned int& _outPresentIndex) const override { return GReturn::FAILURE; }
			GReturn GetSwapchainImage(const int& _index, void** _outVkImage) const override								  { return GReturn::FAILURE; }
			GReturn GetSwapchainView(const int& _index, void** _outVkImageView) const override          				  { return GReturn::FAILURE; }
			GReturn GetSwapchainFramebuffer(const int& _index, void** _outVkFramebuffer) const override 		 		  { return GReturn::FAILURE; }
			GReturn GetSwapchainDepthBufferImage(const int _index, void** _outVkDepthImage) const override					  { return GReturn::FAILURE; }
			GReturn GetSwapchainDepthBufferView(const int _index, void** _outVkDepthView) const override					  { return GReturn::FAILURE; }

			GReturn GetInstance(void** _outVkInstance) const override               { return GReturn::FAILURE; }
			GReturn GetSurface(void** _outVkSurfaceKHR) const override              { return GReturn::FAILURE; }
			GReturn GetPhysicalDevice(void** _outVkPhysicalDevice) const override   { return GReturn::FAILURE; }
			GReturn GetDevice(void** _outVkDevice) const override                   { return GReturn::FAILURE; }
			GReturn GetCommandPool(void** _outCommandPool) const override           { return GReturn::FAILURE; }
			GReturn GetSwapchain(void** _outVkSwapchainKHR) const override          { return GReturn::FAILURE; }
			GReturn GetRenderPass(void** _outVkRenderPass) const override			{ return GReturn::FAILURE;}
			GReturn GetCommandBuffer(const int& _index, void** _outCommandBuffer) const override         { return GReturn::FAILURE; }
			GReturn GetImageAvailableSemaphore(const int& _index, void** _outVkSemaphore) const override { return GReturn::FAILURE; }
			GReturn GetRenderFinishedSemaphore(const int& _index, void** _outVkSemaphore) const override { return GReturn::FAILURE; }
			GReturn GetRenderFence(const int& _index, void** _outVkFence) const override                 { return GReturn::FAILURE; }

			GReturn StartFrame(const unsigned int& _clearCount, void* _vkClearValues) override { return GReturn::FAILURE; }
			GReturn EndFrame(const bool& _vSync) override { return GReturn::FAILURE; }

            //GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override 						{ return GReturn::FAILURE;}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override 	{ return GReturn::FAILURE;}
			GReturn Invoke() const override													{ return GReturn::FAILURE;}
			GReturn Invoke(const GEvent& _incomingEvent) const override 					{ return GReturn::FAILURE;}
		};
    } // end I namespace
} // end GW namespace
