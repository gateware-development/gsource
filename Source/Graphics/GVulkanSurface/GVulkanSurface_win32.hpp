#include <Windows.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_win32.h>
#pragma comment(lib, "vulkan-1.lib")

namespace GW
{
	namespace I
	{
		class GVulkanSurfacePlatformFucts {
		public:
			const char* GetPlatformSurfaceExtension() {
				return VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
			}
            const char* GetPlatformDeviceExtension() {
                return nullptr;
            }

			void PlatformOverrideInstanceCreate(VkInstanceCreateInfo& _create_info, uint32_t& _instanceExtensionCount,
				const char*** _instanceExtensions, uint32_t& _instanceLayerCount, const char*** _instanceLayer) {
				//Hey there! Nothing to see here!...yet
			}

			char* GetPlatformWindowName(const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh) {
				//Get the proper window handle
				HWND hWnd = static_cast<HWND>(uwh.window);

				//Get Window Title
				char* str = new char[255]; int len;
				len = GetWindowTextA(hWnd, str, 255) + 1;

				//Create a Window Title
				char* window_title = nullptr;
				if (len) {
					window_title = new char[len];
					strcpy_s(window_title, len, str);
				}

				//If Above Fails
				if (!window_title) {
					//Default Window Title[22] (21 Characters, +1 for null terminator)
					window_title = new char[22];
					strcpy_s(window_title, 22, "Gateware Application");
				}

				delete[] str;
				return window_title;
			}

			VkResult CreateVkSurfaceKHR(const VkInstance& vkInstance, const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh, VkSurfaceKHR& vkSurface) {
				
				HWND hWnd = static_cast<HWND>(uwh.window);
				HINSTANCE* hInst = reinterpret_cast<HINSTANCE*>(GetWindowLongPtr(static_cast<HWND>(hWnd), GWLP_HINSTANCE));

				VkWin32SurfaceCreateInfoKHR create_info;
				create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
				create_info.hinstance = (*hInst) ? *hInst : nullptr;
				create_info.hwnd = hWnd;
				create_info.flags = 0;
				create_info.pNext = 0;

				VkResult r = vkCreateWin32SurfaceKHR(vkInstance, &create_info, VK_NULL_HANDLE, &vkSurface);
				return r;
			}

			VkResult PlatformDestroyGVulkanSurface(const std::function<void()>& callback) {
				//Cleanup Vulkan Surface
				callback();

				//Return Success
				return VK_SUCCESS;
			}
		};
	}
}

