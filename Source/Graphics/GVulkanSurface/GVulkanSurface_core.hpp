#include "../../../Source/Shared/GVersion.hpp"
#include "../../../Interface/Math/GMathDefines.h"
#include "../../Shared/GVulkanHelper.hpp" // included so it will be part of the concatenated header
#include <vector>

#define GATEWARE_VK_FAIL(vkresult, greturn) if (vkresult) return greturn;

//The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//The namespace to which all Gateware internal implementation must belong.
	namespace I {
    
		class GVulkanSurfaceImplementation :	public virtual GVulkanSurfaceInterface,
												public GEventGeneratorImplementation
			{
		public:
			//Destructor
			~GVulkanSurfaceImplementation() { CleanupVulkanSurface(); DestroyQueryVariable(); }

            //Create
			GReturn Create(SYSTEM::GWindow _gWindow, unsigned long long _initMask) {
				//Redirect Create. Mainly for convenience if using DX Surface Also.
				return Create(_gWindow, _initMask, 0, nullptr, 0, nullptr, 0, nullptr, true);
			}
		
			GReturn Create(SYSTEM::GWindow _gWindow, unsigned long long _initMask, unsigned int _instanceLayerCount, const char** _instanceLayers, 
				unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount, const char** _deviceExtensions,
				bool _allPhysicalDeviceFeatures) {
					//Redirect Create to use most complex version with no layer settings.
					return Create(_gWindow, _initMask, _instanceLayerCount, _instanceLayers, 0, nullptr, 
						_instanceExtensionCount, _instanceExtensions, _deviceExtensionCount, _deviceExtensions, _allPhysicalDeviceFeatures);
				}

			GReturn Create(GW::SYSTEM::GWindow _gWindow, unsigned long long _initMask, unsigned int _instanceLayerCount, const char** _instanceLayers, unsigned int _layerSettingsEXTCount,
				const void* _layerSettingsEXT, unsigned int _instanceExtensionCount, const char** _instanceExtensions, unsigned int _deviceExtensionCount,
				const char** _deviceExtensions, bool _allPhysicalDeviceFeatures) {

				//Error Check #1: GWindow or surface is nullptr
				if (!_gWindow)
					return GReturn::INVALID_ARGUMENT;

				//Error Check #2: Instance Layer Count and Content Mismatch (Count vs Address)
				if (_instanceLayerCount == 0 && _instanceLayers)
					return GReturn::INVALID_ARGUMENT;
				else if (_instanceLayerCount > 0 && !_instanceLayers)
					return GReturn::INVALID_ARGUMENT;

				//Error Check #3: Layer Setting Count and Content Mismatch (Count vs Address)
				if (_layerSettingsEXTCount == 0 && _layerSettingsEXT)
					return GReturn::INVALID_ARGUMENT;
				else if (_layerSettingsEXTCount > 0 && !_layerSettingsEXT)
					return GReturn::INVALID_ARGUMENT;

				//Error Check #4: Instance Extension Count and Content Mismatch. (Count vs Address)
				if (_instanceExtensionCount == 0 && _instanceExtensions)
					return GReturn::INVALID_ARGUMENT;
				else if (_instanceExtensionCount > 0 && !_instanceExtensions)
					return GReturn::INVALID_ARGUMENT;

				//Error Check #5: Device Extension Count and Content Mismatch (Count vs Address)
				if (_deviceExtensionCount == 0 && _deviceExtensions)
					return GReturn::INVALID_ARGUMENT;
				else if (_deviceExtensionCount > 0 && !_deviceExtensions)
					return GReturn::INVALID_ARGUMENT;

				//Error Check #6: Supported Initialization Masks
				unsigned long long allowed =
					GRAPHICS::DEPTH_BUFFER_SUPPORT | GRAPHICS::DEPTH_STENCIL_SUPPORT |
					(GRAPHICS::MSAA_64X_SUPPORT - GRAPHICS::MSAA_2X_SUPPORT) | GRAPHICS::MSAA_64X_SUPPORT
					| GRAPHICS::TRIPLE_BUFFER | GRAPHICS::BINDLESS_SUPPORT;
				if (~allowed & _initMask)
					return GReturn::INVALID_ARGUMENT;
				m_InitMask = _initMask;

				//Set GWindow & All Device Features
				m_GWindow = _gWindow;
				m_AllPhysicalDeviceFeatures = _allPhysicalDeviceFeatures;
				m_VkQueryInfo = nullptr;

				//Initialize all Vulkan Variables
				InitVariables();

				//Set Instance Layers
				m_InstanceLayerCount = _instanceLayerCount;
				m_InstanceLayers = new const char* [m_InstanceLayerCount];
				for (uint32_t i = 0; i < _instanceLayerCount; ++i)
					m_InstanceLayers[i] = _instanceLayers[i];

// older versions of Vulkan do not have layer settings
#if defined(VK_EXT_layer_settings)
				//Set Layer Settings
				m_LayerSettingEXTCount = _layerSettingsEXTCount;
				m_LayerSettingsEXT = new VkLayerSettingEXT[m_LayerSettingEXTCount];
				for (uint32_t i = 0; i < _layerSettingsEXTCount; ++i)
					m_LayerSettingsEXT[i] = *(reinterpret_cast<const VkLayerSettingEXT*>(_layerSettingsEXT)+i);
#endif
				// copy device extensions, leaving room for bindless support
				std::unique_ptr<const char* []> device_extensions(new const char* [(_deviceExtensionCount + 1)]);
				device_extensions[_deviceExtensionCount] = nullptr; // don't allow to be garbage
				for (uint32_t i = 0; i < _deviceExtensionCount; ++i)
					device_extensions[i] = _deviceExtensions[i];
				// add bindless support if requested
				if (m_InitMask & GRAPHICS::BINDLESS_SUPPORT)
					device_extensions[_deviceExtensionCount++] = VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME;

				//Error Check #6: No Swapchain or Surface Support
				m_InstanceExtensionCount = _instanceExtensionCount;
				m_DeviceExtensionCount = _deviceExtensionCount;
				if (CheckCompatibility(_instanceExtensions, device_extensions.get())) {
					CleanupVulkanSurface();
					return GReturn::HARDWARE_UNAVAILABLE;
				}
				// temporarily enable VSync so we get enough Swapchain images for all situations.
				m_VSync = true; // on some cards we Get 5 swap images when VSync is ON.
				//Initialize Vulkan Surface
				GReturn toCreate = InitVulkanSurface();
				if (G_FAIL(toCreate)) {
					CleanupVulkanSurface();
					return toCreate;
				}
				// older versions of Vulkan do not support layer settings
#if !defined(VK_EXT_layer_settings)
				if (_layerSettingsEXTCount > 0) {
					CleanupVulkanSurface();
					return GReturn::SOFTWARE_UNAVAILABLE;
				}
#endif
				// disable it by default.(once initialization has completed)
				m_VSync = false;
				return GReturn::SUCCESS;
			}

			GReturn Create(SYSTEM::GWindow _gWindow, GVulkanSurfaceQueryInfo** _queryInfo) {
				//Error Check: Bad Query Info Handle
				if (!_queryInfo)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: Bad GWindow Handle
				if (!_gWindow)
					return GReturn::INVALID_ARGUMENT;
					
				//Setup Creation of QueryInfo
				CreateQueryVariable();

				//Setup Variables
				m_GWindow = _gWindow;
				m_VkQueryInfo->initializationMask = 0;
				VkLayerProperties* all_layer_properties = nullptr;
				VkExtensionProperties* all_instance_extensions = nullptr;
				VkExtensionProperties* all_device_extensions = nullptr;

				//Setup Vulkan Environment for Query
				InitVariables();
				CheckInstanceVectors();
				CheckCompatibility(nullptr, nullptr);
				CreateVkInstance();
				m_VulkanHelperOS.CreateVkSurfaceKHR(m_VkInstance, m_WindowHandle, m_VkSurfaceKHR);
				FindVkPhysicalDevice();
				CheckDeviceVector();
				CreateVkDevice();

				//Instance Layer Enumeration:
				vkEnumerateInstanceLayerProperties(&(m_VkQueryInfo->instanceLayerCount), VK_NULL_HANDLE);
				if (m_VkQueryInfo->instanceLayerCount) {
					all_layer_properties = new VkLayerProperties[m_VkQueryInfo->instanceLayerCount];
					vkEnumerateInstanceLayerProperties(&(m_VkQueryInfo->instanceLayerCount), all_layer_properties);

					//Instance Layers Copy
					m_VkQueryInfo->instanceLayers = new const char* [m_VkQueryInfo->instanceLayerCount];
					for (unsigned int i = 0; i < m_VkQueryInfo->instanceLayerCount; ++i)
						m_VkQueryInfo->instanceLayers[i] = all_layer_properties[i].layerName;
				}

				//Instance Extension Enumeration:
				vkEnumerateInstanceExtensionProperties(VK_NULL_HANDLE, &m_VkQueryInfo->instanceExtensionCount, VK_NULL_HANDLE);
				if (m_VkQueryInfo->instanceExtensionCount) {
					all_instance_extensions = new VkExtensionProperties[m_VkQueryInfo->instanceExtensionCount];
					vkEnumerateInstanceExtensionProperties(VK_NULL_HANDLE, &m_VkQueryInfo->instanceExtensionCount, all_instance_extensions);

					//Instance Extension Copy
					m_VkQueryInfo->instanceExtensions = new const char* [m_VkQueryInfo->instanceExtensionCount];
					for (unsigned int i = 0; i < m_VkQueryInfo->instanceExtensionCount; ++i)
						m_VkQueryInfo->instanceExtensions[i] = all_instance_extensions[i].extensionName;
				}

				//Instance Extension Enumeration:
				vkEnumerateDeviceExtensionProperties(m_VkPhysicalDevice, VK_NULL_HANDLE, &m_VkQueryInfo->deviceExtensionCount, VK_NULL_HANDLE);
				if (m_VkQueryInfo->deviceExtensionCount) {
					all_device_extensions = new VkExtensionProperties[m_VkQueryInfo->deviceExtensionCount];
					vkEnumerateDeviceExtensionProperties(m_VkPhysicalDevice, VK_NULL_HANDLE, &m_VkQueryInfo->deviceExtensionCount, all_device_extensions);

					//Instance Extension Copy
					m_VkQueryInfo->deviceExtensions = new const char* [m_VkQueryInfo->deviceExtensionCount];
					for (unsigned int i = 0; i < m_VkQueryInfo->deviceExtensionCount; ++i)
						m_VkQueryInfo->deviceExtensions[i] = all_device_extensions[i].extensionName;
				}

				//Physical Device Features Query
				VkPhysicalDeviceFeatures* all_device_features = new VkPhysicalDeviceFeatures;
				vkGetPhysicalDeviceFeatures(m_VkPhysicalDevice, all_device_features);
				m_VkQueryInfo->physicalDeviceFeatures = all_device_features;

				//Feature Support
				VkPhysicalDeviceProperties physical_device_properties;
				vkGetPhysicalDeviceProperties(m_VkPhysicalDevice, &physical_device_properties);
				VkSampleCountFlagBits flags;
				if (physical_device_properties.limits.framebufferColorSampleCounts < physical_device_properties.limits.framebufferDepthSampleCounts)
					flags = static_cast<VkSampleCountFlagBits>(physical_device_properties.limits.framebufferColorSampleCounts);
				else {
					flags = static_cast<VkSampleCountFlagBits>(G_LARGER(
					static_cast<int32_t>(physical_device_properties.limits.framebufferColorSampleCounts),
					static_cast<int32_t>(physical_device_properties.limits.framebufferDepthSampleCounts)
				));
				}

				m_VkQueryInfo->initializationMask |= static_cast<unsigned long long>(static_cast<unsigned long long>(flags) << static_cast<int>(__log2(static_cast<float>(GRAPHICS::MSAA_2X_SUPPORT)) - 1));
				m_VkQueryInfo->initializationMask |= GRAPHICS::DEPTH_BUFFER_SUPPORT | GRAPHICS::DEPTH_STENCIL_SUPPORT | GRAPHICS::TRIPLE_BUFFER;

				//Setting up the properties [When deleted, the const char* stuff gets erased, why not send the properties too?]
				m_VkQueryInfo->instanceLayerProperties		= all_layer_properties;
				m_VkQueryInfo->instanceExtensionProperties = all_instance_extensions;
				m_VkQueryInfo->deviceExtensionProperties	= all_device_extensions;
				CleanupVulkanSurface();

				*_queryInfo = m_VkQueryInfo;
				
				//Return Success
				return GReturn::SUCCESS;
			}

            //GVulkanSurface
			GReturn GetAspectRatio(float& _outRatio) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: GWindow fails to exist
				if (!m_GWindow)
					return GReturn::FAILURE;
				
				//Give out the Aspect Ratio
				_outRatio = m_AspectRatio;

				//Return Success
				return GReturn::SUCCESS;
			}
		
            GReturn GetSwapchainImageCount(unsigned int& _outImageCount) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Give the Maximum Image Count
				_outImageCount = m_MaxFrameCount;

				//Return Success
				return GReturn::SUCCESS;
			}
		
			GReturn GetSwapchainCurrentImage(unsigned int& _outImageIndex) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Give the Current Image Index
				_outImageIndex = m_CurrentFrame;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetGraphicsQueue(void** _outVkQueue) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkQueue)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkQueue = m_VkQueueGraphics;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetPresentQueue(void** _outVkQueue) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkQueue)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkQueue = m_VkQueuePresent;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetQueueFamilyIndices(unsigned int& _outGraphicsIndex, unsigned int& _outPresentIndex) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Give out the Queue Indices
				_outGraphicsIndex = static_cast<uint32_t>(m_QueueFamilyIndices[0]);
				_outPresentIndex = static_cast<uint32_t>(m_QueueFamilyIndices[1]);

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchainImage(const int& _index, void** _outVkImage) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkImage)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: Swapchain or Swapchain Image doesn't exist
				if (!m_VkSwapchainKHR || !m_VkImageSwapchain[0])
					return GReturn::FAILURE;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkImage = m_VkImageSwapchain[m_CurrentFrame];
				else
					*_outVkImage = m_VkImageSwapchain[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchainView(const int& _index, void** _outVkImageView) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkImageView)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: Swapchain and Swapchain's View Exist
				if (!m_VkSwapchainKHR || !m_VkImageViewSwapchain)
					return GReturn::FAILURE;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkImageView = m_VkImageViewSwapchain[m_CurrentFrame];
				else
					*_outVkImageView = m_VkImageViewSwapchain[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchainFramebuffer(const int& _index, void** _outVkFramebuffer) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkFramebuffer)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: No Swapchain or Framebuffer
				if (!m_VkSwapchainKHR || !m_VkFramebuffer[0])
					return GReturn::FAILURE;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkFramebuffer = m_VkFramebuffer[m_CurrentFrame];
				else
					*_outVkFramebuffer = m_VkFramebuffer[_index];

				//Return Success
				return GReturn::SUCCESS;
			}
			
			GReturn GetSwapchainDepthBufferImage(const int _index, void** _outVkDepthImage) const override{
				//add doxygen before you finish this!
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;

				//Error Check: Parameter sent is Nullptr
				if (!_outVkDepthImage)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: No Swapchain or Depth image
				if (!m_VkSwapchainKHR || !m_GVkImageDepth.image)
					return GReturn::FAILURE;

				*_outVkDepthImage = m_GVkImageDepth.image;

				return GReturn::SUCCESS;
			}

			GReturn GetSwapchainDepthBufferView(const int _index, void** _outVkDepthView) const override {
				//add doxygen before you finish this!
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;

				//Error Check: Parameter sent is Nullptr
				if (!_outVkDepthView)
					return GReturn::INVALID_ARGUMENT;

				//Error Check: No Swapchain or Depth View
				if (!m_VkSwapchainKHR || !m_GVkImageDepth.view)
					return GReturn::FAILURE;

				*_outVkDepthView = m_GVkImageDepth.view;

				return GReturn::SUCCESS;
			}

			GReturn GetInstance(void** _outVkInstance) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkInstance)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkInstance = m_VkInstance;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetSurface(void** _outVkSurfaceKHR) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkSurfaceKHR)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkSurfaceKHR = m_VkSurfaceKHR;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetPhysicalDevice(void** _outVkPhysicalDevice) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkPhysicalDevice)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkPhysicalDevice = m_VkPhysicalDevice;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetDevice(void** _outVkDevice) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkDevice)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkDevice = m_VkDevice;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetCommandPool(void** _outCommandPool) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outCommandPool)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outCommandPool = m_VkCommandPool;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchain(void** _outVkSwapchainKHR) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkSwapchainKHR)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkSwapchainKHR = m_VkSwapchainKHR;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetRenderPass(void** _outVkRenderPass) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Parameter is Nullptr
				if (!_outVkRenderPass)
					return GReturn::INVALID_ARGUMENT;

				//Give the object to the parameter
				*_outVkRenderPass = m_VkRenderPass;

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetCommandBuffer(const int& _index, void** _outCommandBuffer) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outCommandBuffer)
					return GReturn::INVALID_ARGUMENT;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outCommandBuffer = m_VkCommandBuffer[m_CurrentFrame];
				else
					*_outCommandBuffer = m_VkCommandBuffer[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetImageAvailableSemaphore(const int& _index, void** _outVkSemaphore) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkSemaphore)
					return GReturn::INVALID_ARGUMENT;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkSemaphore = m_VkSemaphoreImageAvailable[m_CurrentFrame];
				else
					*_outVkSemaphore = m_VkSemaphoreImageAvailable[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetRenderFinishedSemaphore(const int& _index, void** _outVkSemaphore) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkSemaphore)
					return GReturn::INVALID_ARGUMENT;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkSemaphore = m_VkSemaphoreRenderFinished[m_CurrentFrame];
				else
					*_outVkSemaphore = m_VkSemaphoreRenderFinished[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn GetRenderFence(const int& _index, void** _outVkFence) const override {
				//Error Check: Deallocated
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				//Error Check: Index is out of bounds
				if (_index < -1 || _index >= static_cast<int32_t>(m_MaxFrameCount))
					return GReturn::INVALID_ARGUMENT;
				
				//Error Check: Parameter sent is Nullptr
				if (!_outVkFence)
					return GReturn::INVALID_ARGUMENT;

				//If index is -1, give the current object. otherwise give based on index
				if (_index == -1)
					*_outVkFence = m_VkFenceRendering[m_CurrentFrame];
				else
					*_outVkFence = m_VkFenceRendering[_index];

				//Return Success
				return GReturn::SUCCESS;
			}

			GReturn StartFrame(const unsigned int& _clearCount, void* _vkClearValues) override {
					//Error Check: Deallocated
					if (m_Deallocated)
						return GReturn::PREMATURE_DEALLOCATION;

					//Error Check: Screen is minimized
					if (m_NoDrawing)
						return GReturn::FAILURE;

					//Error Check: Clear Count Bounds (0 - 2)
					if (_clearCount > 2)
						return GReturn::INVALID_ARGUMENT;
					
					//Error Check: Have Clear Count but no Clear Color
					if (_clearCount && !_vkClearValues)
						return GReturn::INVALID_ARGUMENT;

					//Error Check: Clear Count is 0 but have a Clear Value
					if (!_clearCount && _vkClearValues)
						return GReturn::INVALID_ARGUMENT;

					//Check if Frame is locked
					if (m_FrameLocked)
						UnlockSyncWrite();

					//Lock the Thread
					LockSyncWrite();

					//Frame is now Locked
					m_FrameLocked = true;

					//Wait for Queue to be ready
					//if (m_PrevFrame != m_CurrentFrame)
					vkWaitForFences(m_VkDevice, 1, &m_VkFenceRendering[m_CurrentFrame], VK_TRUE, ~(static_cast<uint64_t>(0)));

					//Get the Frame Result
					//m_PrevFrame = m_CurrentFrame;
					//Go to the next frame
					//if (++m_CurrentFrame >= m_MaxFrameCount)
					//	m_CurrentFrame = 0; 

					VkResult frame_result = vkAcquireNextImageKHR(m_VkDevice, m_VkSwapchainKHR, 
						~(0ull), m_VkSemaphoreImageAvailable[m_CurrentFrame], VK_NULL_HANDLE, &m_TargetImage);
					
					//Error Check: VkResult is NOT Successful (1: Swapchain Bad, 2: Unknown)
					if (frame_result == VK_ERROR_OUT_OF_DATE_KHR) {
						//Fix the Swapchain
						GReturn g = ResetSwapchain();

						//Unlock Thread
						UnlockSyncWrite();

						//Set Frame Unlocked
						m_FrameLocked = false;

						//Notify Observers a new swapchain was created
						EVENT_DATA edata = { frame_result, {m_VkExtent2DSurface.width, m_VkExtent2DSurface.height} };
						GEvent e;
						e.Write(Events::REBUILD_PIPELINE, edata);
						Push(e);

						//Return Failure [Pipelines must be reset right after this!]
						return G_PASS(g) ? GReturn::FAILURE : GReturn::UNEXPECTED_RESULT;
					}
					else if (frame_result && frame_result != VK_SUBOPTIMAL_KHR) {
						//Unlock Thread
						UnlockSyncWrite();

						//Set Frame Unlocked
						m_FrameLocked = false;

						//Please refer to your Validation Layer for more info.
						return GReturn::FAILURE;
					}
					//Create the Command Buffer's Begin Info
					VkCommandBufferBeginInfo command_buffer_begin_info = {};
					command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
					command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
					command_buffer_begin_info.pInheritanceInfo = nullptr;
					vkBeginCommandBuffer(m_VkCommandBuffer[m_CurrentFrame], &command_buffer_begin_info);

					//Setup Clear Color
					VkClearValue clear_value[2];
					uint32_t clear_value_count = _clearCount; 
					if (!clear_value_count) {
						clear_value[0].color = {{ 0.0f, 0.0f, 0.0f, 1.0f }}; ++clear_value_count;
						if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) {
							clear_value[1].depthStencil = { 1.0f, 16777216 }; ++clear_value_count;
						}
					}
					else {
						VkClearValue* paramClearValue = reinterpret_cast<VkClearValue*>(_vkClearValues);
						clear_value[0] = paramClearValue[0];
						if (clear_value_count == 2 && m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT)
							clear_value[1] = paramClearValue[1];
					}

					//Setup the Render Pass
					VkRenderPassBeginInfo render_pass_begin_info = {};
					render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
					render_pass_begin_info.renderPass = m_VkRenderPass;
					render_pass_begin_info.framebuffer = m_VkFramebuffer[m_CurrentFrame];
					render_pass_begin_info.renderArea.extent = {m_VkExtent2DSurface.width, m_VkExtent2DSurface.height};
					render_pass_begin_info.clearValueCount = clear_value_count;
					render_pass_begin_info.pClearValues = clear_value;

					//Begin the Render Pass
					vkCmdBeginRenderPass(m_VkCommandBuffer[m_CurrentFrame], &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

					//Return Success
					return GReturn::SUCCESS;
				}

			GReturn EndFrame(const bool& _vSync) override {
					//Error Check: GWindow Deallocated
					if (m_Deallocated) {
						//Check if Frame is Locked. If so Unlock it.
						if (m_FrameLocked) {
							UnlockSyncWrite();
							m_FrameLocked = false;
						}

						//Return Premature Deallocation
						return GReturn::PREMATURE_DEALLOCATION;
					}

					//Error Check: Window is not drawable
					if (m_NoDrawing) {
						//Check if Frame is Locked. If so Unlock it.
						if (m_FrameLocked) {
							UnlockSyncWrite();
							m_FrameLocked = false;
						}

						//Return Failure
						return GReturn::FAILURE;
					}

					//Error Check: Start Frame has not started! (It is supposed to be locked to prevent Swapchain from being rebuilt in between draws)
					if (!m_FrameLocked)
						return GReturn::FAILURE;

					//Stop the Render Pass
					vkCmdEndRenderPass(m_VkCommandBuffer[m_CurrentFrame]);
					vkEndCommandBuffer(m_VkCommandBuffer[m_CurrentFrame]);

					//Setup the Semaphores and Command Buffer to be sent into Queue Submit
					VkSemaphore wait_semaphores[] = { m_VkSemaphoreImageAvailable[m_CurrentFrame] };
					VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
					VkSemaphore signal_semaphore[] = { m_VkSemaphoreRenderFinished[m_CurrentFrame] };
					VkCommandBuffer pCommandBuffer[] = { m_VkCommandBuffer[m_CurrentFrame] };
					
					//Setup the Queue Submit Info
					VkSubmitInfo submit_info = {};
					submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
					submit_info.waitSemaphoreCount = 1;
					submit_info.pWaitSemaphores = wait_semaphores;
					submit_info.pWaitDstStageMask = wait_stages;
					submit_info.commandBufferCount = 1;
					submit_info.pCommandBuffers = pCommandBuffer;
					submit_info.signalSemaphoreCount = 1;
					submit_info.pSignalSemaphores = signal_semaphore;

					//Reset the Fence
					vkResetFences(m_VkDevice, 1, &m_VkFenceRendering[m_CurrentFrame]);

					//Submit Queue <--Something to come back to.
					VkResult r;
					r = vkQueueSubmit(m_VkQueueGraphics, 1, &submit_info, m_VkFenceRendering[m_CurrentFrame]);
					if (r) {
						//Unlock the Thread
						UnlockSyncWrite();

						//Set Frame to Unlock
						m_FrameLocked = false;

						//Please refer to your Validation Layer for more info before reporting.
						return GReturn::FAILURE;
					}

					//Setup the Present Info
					VkSwapchainKHR swapchains[] = { m_VkSwapchainKHR };
					VkPresentInfoKHR present_info = {};
					present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
					present_info.waitSemaphoreCount = 1;
					present_info.pWaitSemaphores = signal_semaphore;
					present_info.swapchainCount = 1;
					present_info.pSwapchains = swapchains;
					present_info.pImageIndices = &m_TargetImage;//&m_CurrentFrame;
					present_info.pResults = nullptr;

					//Present onto the surface
					VkResult frame_result = vkQueuePresentKHR(m_VkQueuePresent, &present_info);

					//Error Check for Swapchain and VSync Changes
					if (frame_result == VK_ERROR_OUT_OF_DATE_KHR || frame_result == VK_SUBOPTIMAL_KHR || _vSync != m_VSync) {
						//Set VSync and its property.
						m_VSync = _vSync;
						
						//Clear the Swapchain
                        GReturn g = ResetSwapchain();

						//Unlock Thread
						UnlockSyncWrite();

						//Set Frame to Unlock
						m_FrameLocked = false;

						//Notify Observers a new swapchain was created
						EVENT_DATA edata = { frame_result, {m_VkExtent2DSurface.width, m_VkExtent2DSurface.height} };
						GEvent e;
						e.Write(Events::REBUILD_PIPELINE, edata);
						Push(e);

						//Return Failure [Pipelines must be reset right after this!]
						return G_PASS(g) ? GReturn::FAILURE : GReturn::UNEXPECTED_RESULT;
					}
					else if (frame_result) {
						//Unlock Thread
						UnlockSyncWrite();

						//Set Frame to Unlock
						m_FrameLocked = false;

						//Please refer to your Validation Layer for more info.
						return GReturn::FAILURE;
					}

					//Go to the next frame
					if (++m_CurrentFrame >= m_MaxFrameCount)
						m_CurrentFrame = 0; 

					//Set Start Frame to false
					m_FrameLocked = false;

					//Unlock Thread
					UnlockSyncWrite();

					//Return Success
					return GReturn::SUCCESS;
			}

            //GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override { 
				return m_Deallocated ? GReturn::PREMATURE_DEALLOCATION : m_EventResponder.Assign(_newHandler); 
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override { 
				return m_Deallocated ? GReturn::PREMATURE_DEALLOCATION : m_EventResponder.Assign(_newEventHandler); 
			}
			GReturn Invoke() const override { 
				return m_Deallocated ? GReturn::PREMATURE_DEALLOCATION : m_EventResponder.Invoke(); 
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override { 
				return m_Deallocated ? GReturn::PREMATURE_DEALLOCATION : m_EventResponder.Invoke(_incomingEvent); 
			}
		private:
			//Init & Create Methods
			GReturn InitVulkanSurface() {
				//Error Check: Instance Layers & Extensions [Invalid Argument]
				GATEWARE_VK_FAIL(CheckInstanceVectors(), GReturn::INVALID_ARGUMENT);

				//Setup #1: VkInstance [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(CreateVkInstance(), GReturn::HARDWARE_UNAVAILABLE);

				//Setup #2: VkSurfaceKHR [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(m_VulkanHelperOS.CreateVkSurfaceKHR(m_VkInstance, m_WindowHandle, m_VkSurfaceKHR), GReturn::HARDWARE_UNAVAILABLE);

				//Setup #3: VkPhysicalDevice [ Feature Unsupported: MSAA Unsupported, Failure: {Unknown: Need to Investigate} ]
				VkResult r = FindVkPhysicalDevice(); 
				if (r == VK_ERROR_FORMAT_NOT_SUPPORTED) return GReturn::FEATURE_UNSUPPORTED;
				else if (r) return GReturn::FAILURE;

				//Error Check: Device Extension [Invalid Argument]
				GATEWARE_VK_FAIL(CheckDeviceVector(), GReturn::INVALID_ARGUMENT);

				//Setup #4: VkDevice [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(CreateVkDevice(), GReturn::HARDWARE_UNAVAILABLE);

				//Setup #5: VkCommandPool [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(CreateVkCommandPool(), GReturn::FAILURE);

				//Setup #6: Swapchain Properties [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(GetSurfaceData(), GReturn::FAILURE);

				//Setup #7: Swapchain [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(-CreateSwapchain(), GReturn::FAILURE);

				//Setup #8: Semaphores and Fences [ Failure: {Unknown: Need to Investigate} ]
				GATEWARE_VK_FAIL(CreateSyncObjects(), GReturn::FAILURE);

				//All Done!
				return GReturn::SUCCESS;
			}

			GReturn InitVariables() {
				//Basic GWindow Setup
				m_GWindow.GetWindowHandle(m_WindowHandle);
				m_GWindow.GetX(m_WindowTopLeft.width);
				m_GWindow.GetY(m_WindowTopLeft.height);
				m_GWindow.GetClientWidth(m_WindowExtent.width);
				m_GWindow.GetClientHeight(m_WindowExtent.height);
				m_WindowName = m_VulkanHelperOS.GetPlatformWindowName(m_WindowHandle);
				m_AspectRatio = m_WindowExtent.width / static_cast<float>(m_WindowExtent.height);
				m_Deallocated = false;
				m_NoDrawing = false;

				//Main Vulkan Objects Setup
				m_VkInstance = {};
				m_VkSurfaceKHR = {};
				m_VkPhysicalDevice = {};
				m_VkDevice = {};
				m_VkCommandPool = {};
				m_VkSwapchainKHR = {};

				//Feature-Based Images
				m_GVkImageMSAA = {};
				m_GVkImageDepth = {};
				m_VSync = false;
				m_MSAAOn = false; //MSAA is now off

				//Frame-Based Vulkan Objects & Properties
				m_MaxFrameCount = 0;
				m_CurrentFrame = 0;
				memset(m_VkImageSwapchain, 0, sizeof(VkImage) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkImageViewSwapchain, 0, sizeof(VkImageView) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkFramebuffer, 0, sizeof(VkFramebuffer) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkCommandBuffer, 0, sizeof(VkCommandBuffer) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkSemaphoreImageAvailable, 0, sizeof(VkSemaphore) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkSemaphoreRenderFinished, 0, sizeof(VkSemaphore) * GVK_SWAP_BUFFER_LIMIT); 
				memset(m_VkFenceRendering, 0, sizeof(VkFence) * GVK_SWAP_BUFFER_LIMIT); 

				//Vulkan Properties Setup
				m_QueueFamilyIndices[0] = -1;
				m_QueueFamilyIndices[1] = -1;
				m_CanCompute = VK_FALSE;
				m_MSAA = VK_SAMPLE_COUNT_1_BIT;
				m_VkQueueGraphics = {};
				m_VkQueuePresent = {};
				m_VkSurfaceCapabilitiesKHR = {};
				m_VkPresentModeKHRSurface = {};
				m_VkExtent2DSurface = {};
				m_VkFormatSurface = {};
				m_VkFormatDepth = {};
				m_VkRenderPass = {};
				m_FrameLocked = false;

				//Helper Method Setup  
				m_AllInstanceLayerCount = 0;
				m_AllInstanceExtensionCount = 0;
				m_AllDeviceExtensionCount = 0;
				m_AllSurfaceFormatCount = 0;
				m_AllPresentModeCount = 0;
				m_AllPhysicalDevices = nullptr;			m_AllPhysicalDeviceCount = 0;
				m_AllQueueFamilyProperties = nullptr;	m_AllQueueFamilyPropertyCount = 0;

				//Event Setup
				m_EventResponder.Create([&](const GW::GEvent& event) {
					GW::SYSTEM::GWindow::Events windowEvent;
					GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
					if(+event.Read(windowEvent, windowEventData))
                    {
                        switch (windowEvent) {
                            case GW::SYSTEM::GWindow::Events::MINIMIZE: {
                                m_NoDrawing = true;
                            } break;
                            case GW::SYSTEM::GWindow::Events::DESTROY: {
                                //Notify Observers its time to release any Vulkan resources
                                GEvent shutdown;
                                EVENT_DATA sdata = { // wasn't actually lost but its about to be
                                    VK_ERROR_SURFACE_LOST_KHR, { m_VkExtent2DSurface.width, m_VkExtent2DSurface.height }
                                };
                                shutdown.Write(Events::RELEASE_RESOURCES, sdata);
                                Push(shutdown);
                                //Cleanup the whole surface
                                m_VulkanHelperOS.PlatformDestroyGVulkanSurface([&](){CleanupVulkanSurface();});
                            } break;
                            case GW::SYSTEM::GWindow::Events::DISPLAY_CLOSED: {
                                //Cleanup Instance
                                if (m_VkInstance) {
                                    vkDestroyInstance(m_VkInstance, VK_NULL_HANDLE);
                                    m_VkInstance = VK_NULL_HANDLE;
                                }
                            } break;
                            case GW::SYSTEM::GWindow::Events::MAXIMIZE:
                            case GW::SYSTEM::GWindow::Events::RESIZE: {
                                if (windowEventData.clientWidth < 1 || windowEventData.clientHeight < 1)
                                    m_NoDrawing = true;
                                else {
                                    //Lock the Thread
                                    LockSyncWrite();

                                    //Reset the Swapchain
                                    GReturn g = ResetSwapchain();

                                    //Unlock The Thread
                                    UnlockSyncWrite();

                                    if (G_PASS(g))
                                    {
                                        //Notify Observers a new swapchain was created
                                        EVENT_DATA edata = { VK_ERROR_OUT_OF_DATE_KHR, {m_VkExtent2DSurface.width, m_VkExtent2DSurface.height} };
                                        GEvent e;
                                        e.Write(Events::REBUILD_PIPELINE, edata);
                                        g = Push(e);

                                        //Reset Minimization
                                        m_NoDrawing = false;
                                    }
                                }
                            } break;
                            case GW::SYSTEM::GWindow::Events::MOVE: {
                                //Set Window TopLeft
                                m_GWindow.GetClientTopLeft(m_WindowTopLeft.width, m_WindowTopLeft.height);
                            } break;
                            default:
                                break;
                        }
                    }
				});
				return m_GWindow.Register(m_EventResponder);
			}

			VkResult CreateVkInstance() {
				//Application Information (Will come back to this for Gateware Version. Maybe used for RenderDoc)
				VkApplicationInfo app_info = {};
				app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
				app_info.apiVersion = VK_API_VERSION_1_1;
				app_info.pApplicationName = m_WindowName;
				app_info.applicationVersion = 1;
				app_info.pEngineName = "Gateware";
				app_info.engineVersion = VK_MAKE_VERSION(GATEWARE_MAJOR, GATEWARE_MINOR, GATEWARE_PATCH);
				app_info.pNext = nullptr;

				//Application Create Info [Basics]
				VkInstanceCreateInfo create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
				create_info.pApplicationInfo = &app_info;

				//Extensions
				create_info.enabledExtensionCount = m_InstanceExtensionCount;
				create_info.ppEnabledExtensionNames = m_InstanceExtensions;
				create_info.enabledLayerCount = m_InstanceLayerCount;
				create_info.ppEnabledLayerNames = m_InstanceLayers;

// older versions of Vulkan do not have layer settings
#if defined(VK_EXT_layer_settings)
				//Layer Settings (if provided)
				const VkLayerSettingsCreateInfoEXT layer_settings_create_info = {
					VK_STRUCTURE_TYPE_LAYER_SETTINGS_CREATE_INFO_EXT, nullptr,
					m_LayerSettingEXTCount, m_LayerSettingsEXT 
				};
				create_info.pNext = (m_LayerSettingEXTCount > 0) ? &layer_settings_create_info : nullptr;
#endif
                m_VulkanHelperOS.PlatformOverrideInstanceCreate(create_info, m_InstanceExtensionCount,
					&m_InstanceExtensions, m_InstanceLayerCount, &m_InstanceLayers);

				VkResult r = vkCreateInstance(&create_info, nullptr, &m_VkInstance);
				return r;
			}

			VkResult FindVkPhysicalDevice() {
				//Get all GPUs
				VkResult r = GetAllPhysicalDevices();
				if (r) return VK_ERROR_INITIALIZATION_FAILED;

				//Pick Best GPU
				r = GetBestGPU();

				//Get Family Queue
				r = GetQueueFamilyIndices();
				if (r) return VK_ERROR_FEATURE_NOT_PRESENT;

				//Get MSAA
				unsigned long long msaa_init = m_InitMask & ((GRAPHICS::MSAA_64X_SUPPORT - GRAPHICS::MSAA_2X_SUPPORT) | GRAPHICS::MSAA_64X_SUPPORT);
				if (msaa_init) {
					//Turn on MSAA support
					m_MSAAOn = true;

					//Set the flag to the proper one (-1 since i skip MSAA_1X_SUPPORT, since thats default)
					m_MSAA = static_cast<VkSampleCountFlagBits>( msaa_init >> static_cast<int>(__log2(static_cast<float>(GRAPHICS::MSAA_2X_SUPPORT)) - 1) );

					//Find Support
					VkPhysicalDeviceProperties physical_device_properties;
					vkGetPhysicalDeviceProperties(m_VkPhysicalDevice, &physical_device_properties);

					//Set a flag based on the minimum
					VkSampleCountFlagBits flags;
					if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) {
						flags = static_cast<VkSampleCountFlagBits>(G_SMALLER(
						static_cast<int32_t>(physical_device_properties.limits.framebufferColorSampleCounts),
						static_cast<int32_t>(physical_device_properties.limits.framebufferDepthSampleCounts)
						));
					}
					else
						flags = static_cast<VkSampleCountFlagBits>(physical_device_properties.limits.framebufferColorSampleCounts);

					//Return if bad
					if (m_MSAA & ~(flags)) return VK_ERROR_FORMAT_NOT_SUPPORTED;
				}
				
				//Cleanup
				return r;
			}

			VkResult CreateVkDevice() {
				//Setup Unique Queue Family
				uint32_t qf_createsize = 0;					
				if (m_QueueFamilyIndices[0] ^ m_QueueFamilyIndices[1])
					qf_createsize = 2;						
				else										
					qf_createsize = 1;						
				VkDeviceQueueCreateInfo* queue_create_info_array = new VkDeviceQueueCreateInfo[qf_createsize];

				//Set up Create Info for all unique queue families
				float priority = 1.0f;
				for (uint32_t i = 0; i < qf_createsize; ++i) {
					VkDeviceQueueCreateInfo create_info = {};

					create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
					create_info.queueFamilyIndex = m_QueueFamilyIndices[i];
					create_info.queueCount = 1;
					create_info.pQueuePriorities = &priority;
					queue_create_info_array[i] = create_info;
				}

				//Get all available device features
				VkPhysicalDeviceFeatures all_device_features;
				vkGetPhysicalDeviceFeatures(m_VkPhysicalDevice, &all_device_features);

				//Set the needed (or all) device features
				VkPhysicalDeviceFeatures device_features = {};
				if (m_AllPhysicalDeviceFeatures)
					device_features = all_device_features;
				else {
					if (all_device_features.tessellationShader)	device_features.tessellationShader = VK_TRUE;
					if (all_device_features.geometryShader)		device_features.geometryShader = VK_TRUE;
					if (all_device_features.fillModeNonSolid)	device_features.fillModeNonSolid = VK_TRUE;
					if (all_device_features.samplerAnisotropy)	device_features.samplerAnisotropy = VK_TRUE; //MSAA
					if (m_MSAAOn)						
						if (all_device_features.sampleRateShading)	device_features.sampleRateShading = VK_TRUE; //MSAA
				}
				
				//Setup Logical device create info [*: Two different Queue Indices Check Needed]
				VkDeviceCreateInfo create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
				create_info.pQueueCreateInfos = queue_create_info_array;
				create_info.queueCreateInfoCount = qf_createsize;
				create_info.pEnabledFeatures = &device_features;

				create_info.enabledExtensionCount = m_DeviceExtensionCount;
				create_info.ppEnabledExtensionNames = m_DeviceExtensions;

				// add bindless support if requested
				VkPhysicalDeviceDescriptorIndexingFeaturesEXT physicalDeviceDescriptorIndexingFeatures{};
				if (m_InitMask & GRAPHICS::BINDLESS_SUPPORT) {
					physicalDeviceDescriptorIndexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;
					physicalDeviceDescriptorIndexingFeatures.shaderSampledImageArrayNonUniformIndexing = VK_TRUE;
					physicalDeviceDescriptorIndexingFeatures.runtimeDescriptorArray = VK_TRUE;
					physicalDeviceDescriptorIndexingFeatures.descriptorBindingVariableDescriptorCount = VK_TRUE;
					physicalDeviceDescriptorIndexingFeatures.descriptorBindingPartiallyBound = VK_TRUE;
					create_info.pNext = &physicalDeviceDescriptorIndexingFeatures;
				}
				//Create the Surface (With Results) [VK_SUCCESS = 0]
				VkResult r = vkCreateDevice(m_VkPhysicalDevice, &create_info, nullptr, &m_VkDevice);

				//If Device has been created, Setup the Device Queue for graphics and present family
				vkGetDeviceQueue(m_VkDevice, m_QueueFamilyIndices[0], 0, &m_VkQueueGraphics);
				vkGetDeviceQueue(m_VkDevice, m_QueueFamilyIndices[1], 0, &m_VkQueuePresent);

				//Device has been created successfully!
				delete[] queue_create_info_array;
				return r;
			}

			VkResult CreateVkCommandPool() {
				//Command Pool's Create Info
				VkCommandPoolCreateInfo create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
				create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
				create_info.queueFamilyIndex = m_QueueFamilyIndices[0];

				VkResult r = vkCreateCommandPool(m_VkDevice, &create_info, nullptr, &m_VkCommandPool);
				return r;
			}

			VkResult GetSurfaceData() {
				//Gather The Surface Capabilities
				VkResult r = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &m_VkSurfaceCapabilitiesKHR);
				if (r) return r;

				//Get the best Surface Format
				if (m_VkFormatSurface.format == VK_FORMAT_UNDEFINED) {
					r = GetSurfaceFormat();
					if (r) return r;
				}

				//Get the best present mode
				r = GetSurfacePresentMode();
				if (r) return r;

				//Get the surface extent
				r = GetSurfaceExtent();
				if (r) return r;

				if ( (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) && m_VkFormatDepth == VK_FORMAT_UNDEFINED)
					r = GetDepthFormat();

				return r;
			}

			GReturn CreateSwapchain() {
				//Create The Swapchain
				GATEWARE_VK_FAIL(CreateVkSwapchainKHR(), GReturn::FAILURE);

				//Create the RenderPass
				GATEWARE_VK_FAIL(CreateVkRenderPass(), GReturn::FAILURE);

				//Create the MSAA Buffer (If Needed)
				if (m_MSAAOn) GATEWARE_VK_FAIL(CreateMSAABuffer(), GReturn::FAILURE);

				//Create the Depth Buffer (If Needed)
				if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) GATEWARE_VK_FAIL(CreateDepthBuffer(), GReturn::FAILURE);

				//Create the Framebuffer
				GATEWARE_VK_FAIL(CreateVkFramebuffer(), GReturn::FAILURE);

				//Create the Command Buffer
				GATEWARE_VK_FAIL(CreateVkCommandBuffer(), GReturn::FAILURE);

				return GReturn::SUCCESS;
			}

			VkResult CreateSyncObjects() {
				//Semaphore Info Create
				VkSemaphoreCreateInfo semaphore_create_info = {};
				semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

				//Fence Info Create
				VkFenceCreateInfo fence_create_info = {};
				fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
				fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

				//Create the Semaphores and Fences
				VkResult r{};
				for (unsigned int i = 0; i < m_MaxFrameCount; ++i) {
					r = vkCreateSemaphore(m_VkDevice, &semaphore_create_info, nullptr, &m_VkSemaphoreImageAvailable[i]);
					if (r) {
						return r;
					}
					r = vkCreateSemaphore(m_VkDevice, &semaphore_create_info, nullptr, &m_VkSemaphoreRenderFinished[i]);
					if (r) {
						return r;
					}
					r = vkCreateFence(m_VkDevice, &fence_create_info, nullptr, &m_VkFenceRendering[i]);
					if (r) {
						return r;
					}
				}

				//Semaphores and Fences has been created successfully!
				return r;
			}

			//Create Methods for Swapchain
			VkResult CreateVkSwapchainKHR() {
				//Gather Swapchain Count
				unsigned int newFrameCount = (m_InitMask & GRAPHICS::TRIPLE_BUFFER) ? 3u : 2u;
				if (m_VkSurfaceCapabilitiesKHR.minImageCount > 0 && newFrameCount < m_VkSurfaceCapabilitiesKHR.minImageCount)
					newFrameCount = m_VkSurfaceCapabilitiesKHR.minImageCount;
				if (m_VkSurfaceCapabilitiesKHR.maxImageCount > 0 && newFrameCount > m_VkSurfaceCapabilitiesKHR.maxImageCount)
					newFrameCount = m_VkSurfaceCapabilitiesKHR.maxImageCount;
					
				//Create Info for SwapchainKHR [Part 1]
				VkSwapchainCreateInfoKHR create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
				create_info.surface = m_VkSurfaceKHR;
				create_info.minImageCount = newFrameCount;
				create_info.imageFormat = m_VkFormatSurface.format;
				create_info.imageColorSpace = m_VkFormatSurface.colorSpace;
				create_info.imageExtent = m_VkExtent2DSurface;
				create_info.imageArrayLayers = 1;
				create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
				create_info.preTransform = m_VkSurfaceCapabilitiesKHR.currentTransform;
				create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
				create_info.presentMode = m_VkPresentModeKHRSurface;
				create_info.clipped = VK_TRUE;
				create_info.oldSwapchain = m_VkSwapchainKHR; // supposed to help/optimize transitions

				//Setup Correct Queue Family Indices
				if (m_QueueFamilyIndices[0] ^ m_QueueFamilyIndices[1]) {
					create_info.queueFamilyIndexCount = 2;
					create_info.pQueueFamilyIndices = reinterpret_cast<uint32_t*>(&m_QueueFamilyIndices[0]);
					create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
				}
				else {
					create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
				}

				//Create Swapchain
				VkSwapchainKHR newSwapchain = nullptr;
				VkResult r = vkCreateSwapchainKHR(m_VkDevice, &create_info, nullptr, &newSwapchain);
				if (r >= 0) // on success free the old swapchain and replace with the new one
				{
					//Destroy old Swapchain & sync
					CleanupSwapchain();
					CleanupSyncObjects();
					m_VkSwapchainKHR = newSwapchain;
				}

				//Swapchain Image Setup
				unsigned required_images = 0; 
				r = vkGetSwapchainImagesKHR(m_VkDevice, m_VkSwapchainKHR, &required_images, VK_NULL_HANDLE);
				// possible we may get more Images than requested and we must handle it
				if (required_images > GVK_SWAP_BUFFER_LIMIT)
					return VkResult::VK_ERROR_INITIALIZATION_FAILED;
				if (required_images != m_MaxFrameCount)
					m_MaxFrameCount = required_images; // we must cycle all buffers required
				// **IMPORTANT** Currently the amount of "in-flight" frames is linked to the requested/provided number of images 
				// provided by the swap chain based on our selected modes. However, it is possible to limit this below that number.
				r = vkGetSwapchainImagesKHR(m_VkDevice, m_VkSwapchainKHR, &m_MaxFrameCount, m_VkImageSwapchain);

				//Obtain the Image and Image Views
				for (uint32_t i = 0; i < m_MaxFrameCount; ++i)
					r = CreateImageView(m_VkImageSwapchain[i], m_VkFormatSurface.format, VK_IMAGE_ASPECT_COLOR_BIT, &m_VkImageViewSwapchain[i]);

				//Set Current Frame to 0
				m_CurrentFrame = 0;
				m_TargetImage = 0;
		
				return r;
			}

			VkResult CreateVkRenderPass() {
				//Preliminary Setup
				uint32_t count = 0;
				VkAttachmentDescription* attachments = new VkAttachmentDescription[3];

				//Primary Swapchain Description and Swapchain
				VkAttachmentDescription color_attachment_description = {};
				color_attachment_description.format = m_VkFormatSurface.format;
				color_attachment_description.samples = m_MSAA;
				color_attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
				color_attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
				color_attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				color_attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
				color_attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				color_attachment_description.finalLayout = (m_MSAAOn) ? VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

				VkAttachmentReference color_attachment_reference = {};
				color_attachment_reference.attachment = count;
				color_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

				attachments[count++] = color_attachment_description;

				//Depth Swapchain Attachment & Reference
				VkAttachmentDescription depth_attachment_description = {};
				VkAttachmentReference depth_attachment_reference = {};
				if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) {
					depth_attachment_description.format = m_VkFormatDepth;
					depth_attachment_description.samples = m_MSAA;
					depth_attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
					depth_attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
					depth_attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
					depth_attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
					depth_attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
					depth_attachment_description.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

					depth_attachment_reference.attachment = count;
					depth_attachment_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

					attachments[count++] = depth_attachment_description;
				}

				//Backup Swapchain Attachment & Reference (Need it for MSAA)
				VkAttachmentDescription color_attachment_resolve = {};
				VkAttachmentReference color_attachment_resolve_reference = {};
				if (m_MSAAOn) {
					color_attachment_resolve.format = m_VkFormatSurface.format;
					color_attachment_resolve.samples = VK_SAMPLE_COUNT_1_BIT;
					color_attachment_resolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
					color_attachment_resolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
					color_attachment_resolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
					color_attachment_resolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
					color_attachment_resolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
					color_attachment_resolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

					color_attachment_resolve_reference.attachment = count;
					color_attachment_resolve_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

					attachments[count++] = color_attachment_resolve;
				}

				//Setup the Subpass and Dependency
				VkSubpassDescription subpass_description = {};
				subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
				subpass_description.colorAttachmentCount = 1;
				subpass_description.pColorAttachments = &color_attachment_reference;
				subpass_description.pDepthStencilAttachment = (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT) ? &depth_attachment_reference : nullptr;
				subpass_description.pResolveAttachments = (m_MSAAOn) ? &color_attachment_resolve_reference : nullptr;

				VkSubpassDependency subpass_dependency = {};
				subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
				subpass_dependency.dstSubpass = 0;
				subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
				subpass_dependency.srcAccessMask = 0;
				subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
				subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

				//Setup and Create the RenderPass
				VkRenderPassCreateInfo render_pass_create_info = {};
				render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
				render_pass_create_info.attachmentCount = count;
				render_pass_create_info.pAttachments = attachments;
				render_pass_create_info.subpassCount = 1;
				render_pass_create_info.pSubpasses = &subpass_description;
				render_pass_create_info.dependencyCount = 1;
				render_pass_create_info.pDependencies = &subpass_dependency;

				VkResult r = vkCreateRenderPass(m_VkDevice, &render_pass_create_info, nullptr, &m_VkRenderPass);

				delete[] attachments;
				return r;
			}

			VkResult CreateMSAABuffer() {
				//Create the image and image view for MSAA
				VkResult r = CreateImage(m_VkFormatSurface.format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &m_GVkImageMSAA.image, &m_GVkImageMSAA.memory);
				if (r) {
					if (m_GVkImageMSAA.memory)
						vkFreeMemory(m_VkDevice, m_GVkImageMSAA.memory, VK_NULL_HANDLE);
					if (m_GVkImageMSAA.image)
						vkDestroyImage(m_VkDevice, m_GVkImageMSAA.image, VK_NULL_HANDLE);
					return r;
				}

				r = CreateImageView(m_GVkImageMSAA.image, m_VkFormatSurface.format, VK_IMAGE_ASPECT_COLOR_BIT, &m_GVkImageMSAA.view);
				if (r) {
					if (m_GVkImageMSAA.memory)
						vkFreeMemory(m_VkDevice, m_GVkImageMSAA.memory, VK_NULL_HANDLE);
					if (m_GVkImageMSAA.view)
						vkDestroyImageView(m_VkDevice, m_GVkImageMSAA.view, VK_NULL_HANDLE);
					if (m_GVkImageMSAA.image)
						vkDestroyImage(m_VkDevice, m_GVkImageMSAA.image, VK_NULL_HANDLE);
					return r;
				}

				//Transition the image layout from Undefined to Color Attachment (Optimal)
				r = TransitionImageLayout(m_GVkImageMSAA.image, m_VkFormatSurface.format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
				return r;
			}

			VkResult CreateDepthBuffer() {
				//Create the image and image view for Depth Buffer
				VkResult r = CreateImage(m_VkFormatDepth, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &m_GVkImageDepth.image, &m_GVkImageDepth.memory);

				if (r) {
					if (m_GVkImageDepth.image)
						vkDestroyImage(m_VkDevice, m_GVkImageDepth.image, VK_NULL_HANDLE);
					if (m_GVkImageDepth.memory)
						vkFreeMemory(m_VkDevice, m_GVkImageDepth.memory, VK_NULL_HANDLE);

					m_GVkImageDepth.image = VK_NULL_HANDLE;
					m_GVkImageDepth.memory = VK_NULL_HANDLE;
					return r;
				}

				r = CreateImageView(m_GVkImageDepth.image, m_VkFormatDepth, VK_IMAGE_ASPECT_DEPTH_BIT, &m_GVkImageDepth.view);
				if (r) {
					if (m_GVkImageDepth.view)
						vkDestroyImageView(m_VkDevice, m_GVkImageDepth.view, VK_NULL_HANDLE);
					if (m_GVkImageDepth.image)
						vkDestroyImage(m_VkDevice, m_GVkImageDepth.image, VK_NULL_HANDLE);
					if (m_GVkImageDepth.memory)
						vkFreeMemory(m_VkDevice, m_GVkImageDepth.memory, VK_NULL_HANDLE);

					m_GVkImageDepth.view = VK_NULL_HANDLE;
					m_GVkImageDepth.image = VK_NULL_HANDLE;
					return r;
				}

				//Transition the image layout from Undefined to Color Attachment (Optimal)
				r = TransitionImageLayout(m_GVkImageDepth.image, m_VkFormatDepth, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

				return r;
			}
		
			VkResult CreateVkFramebuffer() {
				//Setup Variables
				VkResult r{};
				uint32_t count = 0;
				VkImageView image_attachments[GVK_SWAP_BUFFER_LIMIT];

				//Loop through the Swapchain Frame Buffers and set their create info
				for (unsigned int i = 0; i < m_MaxFrameCount; ++i) {
					// Create an array of image attachments for create info (NOTE: There is only 1 Color Image View and Depth Buffer!)
					count = 0;
					if (m_MSAAOn) {
						image_attachments[count++] = m_GVkImageMSAA.view;
						if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT)
							image_attachments[count++] = m_GVkImageDepth.view;
						image_attachments[count++] = m_VkImageViewSwapchain[i];
					}
					else {
						image_attachments[count++] = m_VkImageViewSwapchain[i];
						if (m_InitMask & GRAPHICS::DEPTH_BUFFER_SUPPORT)
							image_attachments[count++] = m_GVkImageDepth.view;
					}

					//Frame Buffer's Create Info
					VkFramebufferCreateInfo frame_buffer_create_info = {};
					frame_buffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
					frame_buffer_create_info.renderPass = m_VkRenderPass;
					frame_buffer_create_info.attachmentCount = count;
					frame_buffer_create_info.pAttachments = image_attachments;
					frame_buffer_create_info.width = m_VkExtent2DSurface.width;
					frame_buffer_create_info.height = m_VkExtent2DSurface.height;
					frame_buffer_create_info.layers = 1;

					//Create the Surface (With Results) [VK_SUCCESS = 0]
					r = vkCreateFramebuffer(m_VkDevice, &frame_buffer_create_info, nullptr, &m_VkFramebuffer[i]);
				}

				//delete[] image_attachments;
				return r;
			}

			VkResult CreateVkCommandBuffer() {
				//Allocate Command buffer Information
				VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
				command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
				command_buffer_allocate_info.commandPool = m_VkCommandPool;
				command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
				command_buffer_allocate_info.commandBufferCount = m_MaxFrameCount;

				//Create Command Buffer
				VkResult r = vkAllocateCommandBuffers(m_VkDevice, &command_buffer_allocate_info, &m_VkCommandBuffer[0]);

				//Return Result
				return r;
			}

			//Cleanup & Error Check Methods
			GReturn ResetSwapchain() {
				if (m_VkDevice == nullptr)
                    return GReturn::FAILURE;
                
                //Wait for Device to finish
				vkDeviceWaitIdle(m_VkDevice);
                
                // free existing objects
                CleanupSyncObjects();
                CleanupSwapchain();

				//Update Swapchain Surface Data
				m_GWindow.GetClientWidth(m_WindowExtent.width);
				m_GWindow.GetClientHeight(m_WindowExtent.height);
				m_AspectRatio = m_WindowExtent.width / static_cast<float>(m_WindowExtent.height);
				GetSurfaceData();

				//Recreate Swapchain
				return (+CreateSwapchain() && CreateSyncObjects() == VK_SUCCESS)
					? GReturn::SUCCESS : GReturn::UNEXPECTED_RESULT; //How would this fail if it was created before......
			}

			void CleanupSwapchain() {
				//Cleanup Command Buffers
				if (m_VkCommandBuffer[0]) {
					vkFreeCommandBuffers(m_VkDevice, m_VkCommandPool, m_MaxFrameCount, &m_VkCommandBuffer[0]);
					for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i)
						m_VkCommandBuffer[i] = VK_NULL_HANDLE;
				}

				//Cleanup Frame Buffers
				for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i) {
					if (m_VkFramebuffer[i]) {
						vkDestroyFramebuffer(m_VkDevice, m_VkFramebuffer[i], VK_NULL_HANDLE);
					}
					m_VkFramebuffer[i] = VK_NULL_HANDLE;
				}

				//Cleanup Depth Buffer
				if (m_GVkImageDepth.image) {
					vkDestroyImageView(m_VkDevice, m_GVkImageDepth.view, VK_NULL_HANDLE);
					vkDestroyImage(m_VkDevice, m_GVkImageDepth.image, VK_NULL_HANDLE);
					vkFreeMemory(m_VkDevice, m_GVkImageDepth.memory, VK_NULL_HANDLE);
					m_GVkImageDepth = {};
				}

				//Cleanup MSAA Image
				if (m_GVkImageMSAA.image) {
					vkDestroyImageView(m_VkDevice, m_GVkImageMSAA.view, VK_NULL_HANDLE);
					vkDestroyImage(m_VkDevice, m_GVkImageMSAA.image, VK_NULL_HANDLE);
					vkFreeMemory(m_VkDevice, m_GVkImageMSAA.memory, VK_NULL_HANDLE);
					m_GVkImageMSAA = {};
				}

				//Cleanup Render Pass
				if (m_VkRenderPass) {
					vkDestroyRenderPass(m_VkDevice, m_VkRenderPass, VK_NULL_HANDLE);
					m_VkRenderPass = VK_NULL_HANDLE;
				}

				//Cleanup Swapchain's Image View
				for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i) {
					if (m_VkImageViewSwapchain[i]) {
						vkDestroyImageView(m_VkDevice, m_VkImageViewSwapchain[i], VK_NULL_HANDLE);
					}
					m_VkImageViewSwapchain[i] = VK_NULL_HANDLE;
				}

				//Cleanup Swapchain
				if (m_VkSwapchainKHR) {
					vkDestroySwapchainKHR(m_VkDevice, m_VkSwapchainKHR, VK_NULL_HANDLE);
					m_VkSwapchainKHR = VK_NULL_HANDLE;
				}
			}
			
			void CleanupSyncObjects() {
				//Cleanup Fence (Rendering)
				for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i) {
					if (m_VkFenceRendering[i]) {
						vkWaitForFences(m_VkDevice, 1, &m_VkFenceRendering[i], VK_TRUE, ~(static_cast<uint64_t>(0)));
						vkDestroyFence(m_VkDevice, m_VkFenceRendering[i], VK_NULL_HANDLE);
					}
					m_VkFenceRendering[i] = VK_NULL_HANDLE;
				}

				//Cleanup Semaphore (Render Finished)
				for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i) {
					if (m_VkSemaphoreRenderFinished[i]) {
						vkDestroySemaphore(m_VkDevice, m_VkSemaphoreRenderFinished[i], VK_NULL_HANDLE);
					}
					m_VkSemaphoreRenderFinished[i] = VK_NULL_HANDLE;
				}

				//Cleanup Semaphore (Image Available)
				for (uint32_t i = 0; i < GVK_SWAP_BUFFER_LIMIT; ++i) {
					if (m_VkSemaphoreImageAvailable[i]) {
						vkDestroySemaphore(m_VkDevice, m_VkSemaphoreImageAvailable[i], VK_NULL_HANDLE);
					}
					m_VkSemaphoreImageAvailable[i] = VK_NULL_HANDLE;
				}
			}

			void CleanupVulkanSurface() {
				//Wait for Device to finish
				if (m_VkDevice)
					vkDeviceWaitIdle(m_VkDevice);

				//Cleanup Sync primitives
				CleanupSyncObjects();

				//Cleanup Swapchain
				CleanupSwapchain();

				//Cleanup Command Pool
				if (m_VkCommandPool) {
					vkDestroyCommandPool(m_VkDevice, m_VkCommandPool, VK_NULL_HANDLE);
					m_VkCommandPool = VK_NULL_HANDLE;
				}

				//Cleanup Device
				if (m_VkDevice) {
					vkDestroyDevice(m_VkDevice, VK_NULL_HANDLE);
					m_VkDevice = VK_NULL_HANDLE;
				}

				//Cleanup Surface
				if (m_VkSurfaceKHR) {
					vkDestroySurfaceKHR(m_VkInstance, m_VkSurfaceKHR, VK_NULL_HANDLE);
					m_VkSurfaceKHR = VK_NULL_HANDLE;
				}

				//Other Allocated Memory 
				if (m_WindowName) {
					delete[] m_WindowName;
					m_WindowName = nullptr;
				}

				if (m_AllPhysicalDevices)		{ delete[] m_AllPhysicalDevices; m_AllPhysicalDevices = nullptr;}
				if (m_AllQueueFamilyProperties) { delete[] m_AllQueueFamilyProperties; m_AllQueueFamilyProperties = nullptr; }
				if (m_DeviceExtensions)			{ delete[] m_DeviceExtensions; m_DeviceExtensions = nullptr; }
				if (m_InstanceExtensions)		{ delete[] m_InstanceExtensions; m_InstanceExtensions = nullptr; }
				if (m_InstanceLayers)			{ delete[] m_InstanceLayers; m_InstanceLayers = nullptr; }
// older versions of Vulkan do not have layer settings
#if defined(VK_EXT_layer_settings)
				if (m_LayerSettingsEXT)			{ delete[] m_LayerSettingsEXT; m_LayerSettingsEXT = nullptr; }
#endif
				m_Deallocated = true;
			}

			VkResult CheckCompatibility(const char** _instanceExtensions, const char** _deviceExtensions) {
				//Setup Variables for checks
				const char* use_surface = VK_KHR_SURFACE_EXTENSION_NAME;
				const char* use_swapchain = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
				const char* platform_surface = m_VulkanHelperOS.GetPlatformSurfaceExtension();
                const char* platform_device = m_VulkanHelperOS.GetPlatformDeviceExtension();

				//Check for Compatibility
				if (CheckInstanceExtensionName(use_surface))
					return VK_ERROR_EXTENSION_NOT_PRESENT;
				if (CheckInstanceExtensionName(platform_surface))
					return VK_ERROR_EXTENSION_NOT_PRESENT;
                // device level compatibility is not checked here since the
                // vkPhysical device has not been selected yet.
                // this code may be rearranged and reworked as it seems brittle.

				//Check if the extensions are already in the extensions
				uint32_t gotExtFlag = 15; //Flag Magic Numbers: 1 = Vulkan surface, 2 = platform surface, 4 = swapchain, 8 = platform device
				for (uint32_t i = 0; i < m_InstanceExtensionCount; ++i) {
					if (!strcmp(_instanceExtensions[i], use_surface))
						gotExtFlag &= ~(1u);
					if (!strcmp(_instanceExtensions[i], platform_surface))
						gotExtFlag &= ~(2u);
				}

				//And in Device Extension
                for (uint32_t i = 0; i < m_DeviceExtensionCount; ++i) {
					if (!strcmp(_deviceExtensions[i], use_swapchain))
						gotExtFlag &= ~(4u);
					if (platform_device && // providing a platform device extension is optional
                        !strcmp(_deviceExtensions[i], platform_device))
                        gotExtFlag &= ~(8u);
                }

				//Find the true number you need:
				uint32_t instExtAdded = 0;
				if (gotExtFlag & 1) ++instExtAdded;
				if (gotExtFlag & 2) ++instExtAdded;
                uint32_t devExtAdded = 0;
                if (gotExtFlag & 4) ++devExtAdded;
                if (gotExtFlag & 8) ++devExtAdded;

				//Create the new arrays for the extensions

                m_InstanceExtensions = new const char* [static_cast<size_t>(m_InstanceExtensionCount) + instExtAdded];
                m_DeviceExtensions = new const char* [static_cast<size_t>(m_DeviceExtensionCount) + devExtAdded];
                for (uint32_t i = 0; i < m_InstanceExtensionCount; ++i)
                    m_InstanceExtensions[i] = _instanceExtensions[i];
                for (uint32_t i = 0; i < m_DeviceExtensionCount; ++i)
                    m_DeviceExtensions[i] = _deviceExtensions[i];

				//Add in the adds if needed
				if (gotExtFlag & 2) {
					m_InstanceExtensions[m_InstanceExtensionCount + instExtAdded - 1] = platform_surface;
					--instExtAdded;
				}

				if (gotExtFlag & 1) {
					m_InstanceExtensions[m_InstanceExtensionCount + instExtAdded - 1] = use_surface;
					--instExtAdded;
				}

				if (gotExtFlag & 4) {
					m_DeviceExtensions[m_DeviceExtensionCount] = use_swapchain;
					++m_DeviceExtensionCount;
				}
				// providing a platform device extension is optional
                if (platform_device && gotExtFlag & 8) {
                    m_DeviceExtensions[m_DeviceExtensionCount] = platform_device;
                    ++m_DeviceExtensionCount;
                }
                
				//Add up the instance extension (since there was more than 1)
				if (gotExtFlag & 1) ++m_InstanceExtensionCount;
				if (gotExtFlag & 2) ++m_InstanceExtensionCount;

				//Return Successful
				return VK_SUCCESS;
			}

			VkResult CheckInstanceVectors() {
				for (uint32_t i = 0; i < m_InstanceLayerCount; ++i)
					if (CheckInstanceLayerName(m_InstanceLayers[i]))
						return VK_ERROR_LAYER_NOT_PRESENT;
				for (uint32_t i = 0; i < m_InstanceExtensionCount; ++i)
					if (CheckInstanceExtensionName(m_InstanceExtensions[i]))
						return VK_ERROR_EXTENSION_NOT_PRESENT;

				return VK_SUCCESS;
			}

			VkResult CheckDeviceVector() {
				for (uint32_t i = 0; i < m_DeviceExtensionCount; ++i)
					if (CheckDeviceExtensionName(m_DeviceExtensions[i]))
						return VK_ERROR_EXTENSION_NOT_PRESENT;

				return VK_SUCCESS;
			}

			//Error Checking Helper Methods & Variables
			std::vector<VkLayerProperties> m_AllInstanceLayers;			uint32_t m_AllInstanceLayerCount = 0;
			std::vector<VkExtensionProperties> m_AllInstanceExtensions;	uint32_t m_AllInstanceExtensionCount = 0;
			std::vector<VkExtensionProperties>	m_AllDeviceExtensions;	uint32_t m_AllDeviceExtensionCount = 0;
			VkResult CheckInstanceLayerName(const char* _layer) {
				//Check to see if this was already created
				if (m_AllInstanceLayers.empty()) {
					//Get the Size
					VkResult r = vkEnumerateInstanceLayerProperties(&m_AllInstanceLayerCount, VK_NULL_HANDLE);
					if (m_AllInstanceLayerCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }
					if (r) return r;

					//Resize Vector and Put the contents in it.
					m_AllInstanceLayers.resize(m_AllInstanceLayerCount);
					r = vkEnumerateInstanceLayerProperties(&m_AllInstanceLayerCount, m_AllInstanceLayers.data());
					if (r) return r;
				}

				//Compare all instance layers with the parameter
				for (int i = 0; i < m_AllInstanceLayers.size(); i++)
					if (!strcmp(m_AllInstanceLayers[i].layerName, _layer))
						return VK_SUCCESS;

				return VK_ERROR_LAYER_NOT_PRESENT;
			}

			VkResult CheckInstanceExtensionName(const char* _extensions) {
				//Check to see if this was already created
				if (m_AllInstanceExtensions.empty()) {
					//Get the Size
					VkResult r = vkEnumerateInstanceExtensionProperties(nullptr, &m_AllInstanceExtensionCount, VK_NULL_HANDLE);
					if (m_AllInstanceExtensionCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }
					if (r) return r;

					//Resize Vector and Put the contents in it.
					m_AllInstanceExtensions.resize(m_AllInstanceExtensionCount);
					r = vkEnumerateInstanceExtensionProperties(nullptr, &m_AllInstanceExtensionCount, m_AllInstanceExtensions.data());
					if (r) return r;
				}

				//Compare all instance extensions with the parameter
				for (int i = 0; i < m_AllInstanceExtensions.size(); ++i)
					if (!strcmp(m_AllInstanceExtensions[i].extensionName, _extensions))
						return VK_SUCCESS;

				return VK_ERROR_EXTENSION_NOT_PRESENT;
			}

			VkResult CheckDeviceExtensionName(const char* _extensions) {
				//Check to see if this was already created
				if (m_AllDeviceExtensions.empty()) {
					//Get the Size
					VkResult r = vkEnumerateDeviceExtensionProperties(m_VkPhysicalDevice, nullptr, &m_AllDeviceExtensionCount, VK_NULL_HANDLE);
					if (m_AllDeviceExtensionCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }
					if (r) return r;

					//Resize Vector and Put the contents in it.
					m_AllDeviceExtensions.resize(m_AllDeviceExtensionCount);
					r = vkEnumerateDeviceExtensionProperties(m_VkPhysicalDevice, nullptr, &m_AllDeviceExtensionCount, m_AllDeviceExtensions.data());
					if (r) return r;
				}

				//Compare all device extensions with the parameter
				for (int i = 0; i < m_AllDeviceExtensions.size(); ++i)
					if (!strcmp(m_AllDeviceExtensions[i].extensionName, _extensions))
						return VK_SUCCESS;

				return VK_ERROR_EXTENSION_NOT_PRESENT;
			}

			//Gathering Helper Methods
			VkPhysicalDevice* m_AllPhysicalDevices = nullptr;				uint32_t m_AllPhysicalDeviceCount = 0;
			VkQueueFamilyProperties* m_AllQueueFamilyProperties = nullptr;	uint32_t m_AllQueueFamilyPropertyCount = 0;
			VkResult GetAllPhysicalDevices() {
				//Check to see if this was already created
				if (!m_AllPhysicalDevices) {
					//Get the Size
					VkResult r = vkEnumeratePhysicalDevices(m_VkInstance, &m_AllPhysicalDeviceCount, VK_NULL_HANDLE);
					if (m_AllPhysicalDeviceCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }
					if (r) return r;

					//Resize Vector and Put the contents in it.
					m_AllPhysicalDevices = new VkPhysicalDevice[m_AllPhysicalDeviceCount];
					r = vkEnumeratePhysicalDevices(m_VkInstance, &m_AllPhysicalDeviceCount, m_AllPhysicalDevices);
					if (r) return r;
				}

				//Return Success
				return VK_SUCCESS;
			}
		
			VkResult GetBestGPU() {
				/* What will determine the best GPU:
				* 1.) Compatibility [DQ] {Swapchain, Queue Family, Device Extensions}
				* 2.) Device Type [^N] (Dedicated, Virtual, Integrated, Other, CPU)
				* 3.) Added Memory [x(N/1024)]
				* 4.) Features [+N * size]
				*/

				//Of course, don't do this if there is only 1 device......
				if (m_AllPhysicalDeviceCount == 1) {
					m_VkPhysicalDevice = m_AllPhysicalDevices[0];
					return VK_SUCCESS;
				}

				//Setup
				uint32_t gpuPow = 0;
				uint64_t gpuMult = 1;
				uint32_t gpuAdd = 0;

				uint64_t BestIndex = 0;
				uint64_t BestScore = 0;

				//Loop through each of the Physical Devices
				for (size_t i = 0; i < m_AllPhysicalDeviceCount; ++i) {
					//Setup: Set Physical Device as if this is the chosen device
					m_VkPhysicalDevice = m_AllPhysicalDevices[i];

					//Compatibility Setup: Device Extension Check: (PS: Swapchain support should be here at this point)
					bool _reset = false;
					for (uint32_t j = 0; j < m_DeviceExtensionCount; ++j)
						if (CheckDeviceExtensionName(m_DeviceExtensions[j])) {
							_reset = true;
							break;
						}
					if (_reset) continue;

					//Compatibility Setup: Queue Family Indices
					m_QueueFamilyIndices[0] = m_QueueFamilyIndices[1] = -1;
					if (m_AllQueueFamilyProperties) {
						delete[] m_AllQueueFamilyProperties;
						m_AllQueueFamilyProperties = nullptr;
						m_AllQueueFamilyPropertyCount = 0;
					}
					VkResult r = GetQueueFamilyIndices();
					if (r) continue;

					//Device Type: Best Type [Discrete, Virtual, Integrated, CPU/OTHER] in order
					VkPhysicalDeviceProperties pDevProps;
					vkGetPhysicalDeviceProperties(m_VkPhysicalDevice, &pDevProps);
					switch (pDevProps.deviceType) {
						case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
							gpuPow = 3;
							break;
						case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
							gpuPow = 2;
							break;
						default:
							gpuPow = 1;
							break;
					}

					//Physical Device Memory
					VkPhysicalDeviceMemoryProperties mem_prop = {};
					vkGetPhysicalDeviceMemoryProperties(m_VkPhysicalDevice, &mem_prop);
					for (uint32_t j = 0; j < mem_prop.memoryHeapCount; ++j)
						if (mem_prop.memoryHeaps[j].flags & VkMemoryHeapFlagBits::VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
						{
							gpuMult = mem_prop.memoryHeaps[j].size / 0x40000000;
							break;
						}

					//Lastly: Features
					VkPhysicalDeviceFeatures device_feature;
					vkGetPhysicalDeviceFeatures(m_VkPhysicalDevice, &device_feature);
					int32_t pDevFeatSize = sizeof(VkPhysicalDeviceFeatures);
					const VkBool32 IMTRUE = VK_TRUE;
					while (pDevFeatSize > -1) {
						pDevFeatSize -= sizeof(VkBool32);
						if (!memcmp(reinterpret_cast<char*>(&device_feature) + pDevFeatSize, &IMTRUE, sizeof(VkBool32)))
							gpuAdd += 100;
					}

					//Tally up Score:
					uint64_t score = 0;
					score += gpuAdd;
					score *= gpuMult;
					uint64_t cScore = score;
					for (uint32_t j = 0; j < gpuPow; ++j) score *= cScore;

					//Check Best Score
					if (score > BestScore) {
						BestIndex = i;
						BestScore = score;
					}
				}
				m_VkPhysicalDevice = m_AllPhysicalDevices[BestIndex];


				return VK_SUCCESS;
			}

			VkResult GetQueueFamilyIndices() {
				//Check to see if this was already created
				if (!m_AllQueueFamilyProperties) {
					vkGetPhysicalDeviceQueueFamilyProperties(m_VkPhysicalDevice, &m_AllQueueFamilyPropertyCount, VK_NULL_HANDLE);
					if (m_AllQueueFamilyPropertyCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }

					//Resize Vector and Put the contents in it.
					m_AllQueueFamilyProperties = new VkQueueFamilyProperties[m_AllQueueFamilyPropertyCount];
					vkGetPhysicalDeviceQueueFamilyProperties(m_VkPhysicalDevice, &m_AllQueueFamilyPropertyCount, m_AllQueueFamilyProperties);
				}

				//Setup Variable Usage
				VkBool32 presentSupport = VK_FALSE;
				VkResult r = VK_SUCCESS;

				//Loop through each Queue Properties and find a queue that works together. [WITH COMPUTER SHADER]
				for (uint32_t i = 0; i < m_AllQueueFamilyPropertyCount; ++i) {
					//Set current Queue property
					VkQueueFamilyProperties* cur = &m_AllQueueFamilyProperties[i];

					//Check to see if flags meet
					if (cur->queueCount && (cur->queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT))) {
						//Figure out if surface can present with queue index
						r = vkGetPhysicalDeviceSurfaceSupportKHR(m_VkPhysicalDevice, i, m_VkSurfaceKHR, &presentSupport);
						if (r) continue;

						//If found, set the indices
						if (presentSupport) {
							m_QueueFamilyIndices[0] = m_QueueFamilyIndices[1] = i;
							m_CanCompute = VK_TRUE;
							return r;
						}
					}
				}

				//Loop through each Queue Properties and find a queue that works together. [WITHOUT COMPUTER SHADER]
				for (uint32_t i = 0; i < m_AllQueueFamilyPropertyCount; ++i) {
					//Set current Queue property
					VkQueueFamilyProperties* cur = &m_AllQueueFamilyProperties[i];

					//Check to see if flags meet
					if (cur->queueCount && (cur->queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
						//Figure out if surface can present with queue index
						r = vkGetPhysicalDeviceSurfaceSupportKHR(m_VkPhysicalDevice, i, m_VkSurfaceKHR, &presentSupport);
						if (r) continue;

						//If found, set the indices
						if (presentSupport) {
							m_QueueFamilyIndices[0] = m_QueueFamilyIndices[1] = i;
							m_CanCompute = VK_FALSE;
							return r;
						}
					}
				}

				//Couldn't find anything.
				m_QueueFamilyIndices[0] = m_QueueFamilyIndices[1] = -1;
				return VK_ERROR_FEATURE_NOT_PRESENT;
			}

			//Gathering Surface Data
			std::vector<VkSurfaceFormatKHR> m_AllSurfaceFormats;	uint32_t m_AllSurfaceFormatCount = 0;
			std::vector<VkPresentModeKHR> m_AllPresentModes;		uint32_t m_AllPresentModeCount = 0;
			VkResult GetSurfaceFormat() {
				if (m_AllSurfaceFormats.empty()) {
					//Gather all the surface formats
					VkResult r = vkGetPhysicalDeviceSurfaceFormatsKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &m_AllSurfaceFormatCount, VK_NULL_HANDLE);
					if (m_AllSurfaceFormatCount < 1) return VK_ERROR_FEATURE_NOT_PRESENT;
					if (r) return r;

					//Resize and fill in surface formats
					m_AllSurfaceFormats.resize(m_AllSurfaceFormatCount);
					r = vkGetPhysicalDeviceSurfaceFormatsKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &m_AllSurfaceFormatCount, m_AllSurfaceFormats.data());
					if (r) return r;
				}

				//If Format is undefined, set it to best automatically
				if (m_AllSurfaceFormatCount == 1 && m_AllSurfaceFormats[0].format == VK_FORMAT_UNDEFINED) {
					m_VkFormatSurface = { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
					return VK_SUCCESS;
				}

				//Find the best
				for (int i = 0; i < m_AllSurfaceFormats.size(); ++i)
					if (m_AllSurfaceFormats[i].format == VK_FORMAT_B8G8R8A8_UNORM && m_AllSurfaceFormats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
						m_VkFormatSurface = { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
						return VK_SUCCESS;
					}

				//Best wasn't found, take [0]
				m_VkFormatSurface = m_AllSurfaceFormats[0];
				return VK_SUCCESS;
			}

			VkResult GetSurfacePresentMode() {
				//Check to see if this was already created
				if (m_AllPresentModes.empty()) {
					//Get the Size
					VkResult r = vkGetPhysicalDeviceSurfacePresentModesKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &m_AllPresentModeCount, VK_NULL_HANDLE);
					if (m_AllPresentModeCount < 1) { return VK_ERROR_FEATURE_NOT_PRESENT; }
					if (r) return r;

					//Resize Vector and Put the contents in it.
					m_AllPresentModes.resize(m_AllPresentModeCount);
					r = vkGetPhysicalDeviceSurfacePresentModesKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &m_AllPresentModeCount, m_AllPresentModes.data());
					if (r) return r;
				}

				//Find the best mode (best: Mailbox, runner-up: Immediate, Default: FIFO)
				VkPresentModeKHR VSyncModes[2] = { VK_PRESENT_MODE_IMMEDIATE_KHR , VK_PRESENT_MODE_MAILBOX_KHR };
				VkPresentModeKHR best_mode = VK_PRESENT_MODE_FIFO_KHR;//	//This is best by default. It is because Vulkan requires this to be supported, and not any of others.
				for (uint32_t i = 0; i < m_AllPresentModeCount; ++i) {	//So if any of these fail (Mailbox, Immediate, Shared), FIFO is guaranteed to work!
					if (m_AllPresentModes[i] == VSyncModes[m_VSync]) {
						best_mode = VSyncModes[m_VSync];
						m_VkPresentModeKHRSurface = best_mode;
						break;
					}
					else if (m_AllPresentModes[i] == VSyncModes[!m_VSync])
						m_VkPresentModeKHRSurface = VSyncModes[!m_VSync];
				}

				return VK_SUCCESS;
			}

			VkResult GetSurfaceExtent() {
				//Gather all surface capabilities
				VkSurfaceCapabilitiesKHR surface_capabilities;
				VkResult r = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_VkPhysicalDevice, m_VkSurfaceKHR, &surface_capabilities);
				if (r) return r;

				//If `Capabilities`'s extent is not MAX, Set to those extents
				if (surface_capabilities.currentExtent.width != 0xFFFFFFFF)
					m_VkExtent2DSurface = { surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height };
				else //Otherwise set it to window's width and height.
					m_VkExtent2DSurface = { m_WindowExtent.width, m_WindowExtent.height };

				return r;
			}

			VkResult GetDepthFormat() {
				//Setup Depth Format
				VkFormat depth_formats[3];

				//Order Formats
				if (m_InitMask & GRAPHICS::DEPTH_STENCIL_SUPPORT) {
					depth_formats[0] = VK_FORMAT_D32_SFLOAT_S8_UINT;
					depth_formats[1] = VK_FORMAT_D24_UNORM_S8_UINT;
					depth_formats[2] = VK_FORMAT_D32_SFLOAT;
				}
				else {
					depth_formats[0] = VK_FORMAT_D32_SFLOAT;
					depth_formats[1] = VK_FORMAT_D32_SFLOAT_S8_UINT;
					depth_formats[2] = VK_FORMAT_D24_UNORM_S8_UINT;
				}

				//Find the best compatible format for Depth
				for (uint32_t i = 0; i < 3; ++i) {
					VkFormatProperties format_properties;
					vkGetPhysicalDeviceFormatProperties(m_VkPhysicalDevice, depth_formats[i], &format_properties);

					if ( (format_properties.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
						m_VkFormatDepth = depth_formats[i];
						return VK_SUCCESS;
					}
					else if ((format_properties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
						m_VkFormatDepth = depth_formats[i];
						return VK_SUCCESS;
					}
				}

				//Format not found, set to [0]
				m_VkFormatDepth = depth_formats[0];
				return VK_SUCCESS;
			}

			//Image Creation Helper Methods
			VkResult CreateImage(const VkFormat& _format, const VkImageTiling& _tiling, const VkImageUsageFlags& _usageFlags, const VkMemoryPropertyFlags& _memoryPropertyFlags, VkImage* _outImage, VkDeviceMemory* _outImageMemory) {
				//Create image info
				VkImageCreateInfo create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
				create_info.imageType = VK_IMAGE_TYPE_2D;
				create_info.extent = { m_VkExtent2DSurface.width, m_VkExtent2DSurface.height, 1 };
				create_info.mipLevels = 1;
				create_info.arrayLayers = 1;
				create_info.format = _format;
				create_info.tiling = _tiling;
				create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				create_info.usage = _usageFlags;
				create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				create_info.samples = m_MSAA;
				create_info.flags = 0;

				//Create the image
				VkResult r = vkCreateImage(m_VkDevice, &create_info, VK_NULL_HANDLE, _outImage);
				if (r) return r;

				//Gather Memory Information from image & Physical Device
				VkMemoryRequirements memory_requirements;
				vkGetImageMemoryRequirements(m_VkDevice, *_outImage, &memory_requirements);
				VkPhysicalDeviceMemoryProperties memory_properties;
				vkGetPhysicalDeviceMemoryProperties(m_VkPhysicalDevice, &memory_properties);

				//Loop through the memory type count and see if there is a match with both the filter and property flags
				int32_t memory_type_index = -1;
				for (uint32_t i = 0; i < memory_properties.memoryTypeCount; ++i) {
					if ((memory_requirements.memoryTypeBits & (1 << i)) &&
						(memory_properties.memoryTypes[i].propertyFlags & _memoryPropertyFlags) == _memoryPropertyFlags) {
						memory_type_index = i;
						break;
					}
				}
				if (memory_type_index == -1)
					return VK_ERROR_NOT_PERMITTED_EXT;


				//Memory Allocate Info
				VkMemoryAllocateInfo memory_allocate_info = {};
				memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				memory_allocate_info.allocationSize = memory_requirements.size;
				memory_allocate_info.memoryTypeIndex = memory_type_index;

				//Allocate the memory created
				r = vkAllocateMemory(m_VkDevice, &memory_allocate_info, VK_NULL_HANDLE, _outImageMemory);
				if (r) {
					vkDestroyImage(m_VkDevice, *_outImage, VK_NULL_HANDLE);
					return r;
				}

				//Bind the memory created
				r = vkBindImageMemory(m_VkDevice, *_outImage, *_outImageMemory, 0);
				if (r) {
					vkDestroyImage(m_VkDevice, *_outImage, VK_NULL_HANDLE);
					vkFreeMemory(m_VkDevice, *_outImageMemory, VK_NULL_HANDLE);
					return r;
				}

				//Image Creation has been successful!
				return r;
			}

			VkResult CreateImageView(const VkImage& _image, const VkFormat& _format, const VkImageAspectFlags& _imageAspectFlags, VkImageView* _outImageView) {
				//Image View Create Info
				VkImageViewCreateInfo create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				create_info.image = _image;
				create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
				create_info.format = _format;
				create_info.subresourceRange.aspectMask = _imageAspectFlags;
				create_info.subresourceRange.baseMipLevel = 0;
				create_info.subresourceRange.levelCount = 1;
				create_info.subresourceRange.baseArrayLayer = 0;
				create_info.subresourceRange.layerCount = 1;

				//Create the Surface (With Results) [VK_SUCCESS = 0]
				VkResult r = vkCreateImageView(m_VkDevice, &create_info, nullptr, _outImageView);

				//Image View has been created successfully, return it
				return r;
			}

			VkResult TransitionImageLayout(const VkImage& _image, const VkFormat& _format, const VkImageLayout& _previousLayout, const VkImageLayout& _currentLayout) {
				//Setup the Command Buffer's Allocation Information
				VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
				command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
				command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
				command_buffer_allocate_info.commandPool = m_VkCommandPool;
				command_buffer_allocate_info.commandBufferCount = 1;

				//Allocate the Command Buffer
				VkCommandBuffer command_buffer = VK_NULL_HANDLE;
				VkResult r = vkAllocateCommandBuffers(m_VkDevice, &command_buffer_allocate_info, &command_buffer);
				if (r) return r;

				//Start the command buffer's begin info
				VkCommandBufferBeginInfo command_buffer_begin_info = {};
				command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

				//Begin the Command Buffer's recording process
				r = vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
				if (r) return r;

				//Create the image memory barrier
				VkImageMemoryBarrier image_memory_barrier = {};
				image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
				image_memory_barrier.image = _image;
				image_memory_barrier.oldLayout = _previousLayout;
				image_memory_barrier.newLayout = _currentLayout;
				image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				image_memory_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				image_memory_barrier.subresourceRange.baseMipLevel = 0;
				image_memory_barrier.subresourceRange.levelCount = 1;
				image_memory_barrier.subresourceRange.layerCount = 1;
				image_memory_barrier.subresourceRange.baseArrayLayer = 0;

				//Setup the source and destination stage flags. Will be set based on the Old and New Layout set from outside
				VkPipelineStageFlags source_stage = 0;
				VkPipelineStageFlags destrination_stage = 0;

				if (_currentLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
				{
					image_memory_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
					if (_format == VK_FORMAT_D24_UNORM_S8_UINT || _format == VK_FORMAT_D32_SFLOAT_S8_UINT)
						image_memory_barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
				}

				if (_previousLayout == VK_IMAGE_LAYOUT_UNDEFINED)
				{
					if (_currentLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
					{
						image_memory_barrier.srcAccessMask = 0;
						image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

						source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
						destrination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
					}
					else if (_currentLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
					{
						image_memory_barrier.srcAccessMask = 0;
						image_memory_barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

						source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
						destrination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
					}
					else if (_currentLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
					{
						image_memory_barrier.srcAccessMask = 0;
						image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

						source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
						destrination_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
					}
				}
				else if (_previousLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && _currentLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
				{
					image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
					image_memory_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

					source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
					destrination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
				}

				vkCmdPipelineBarrier(command_buffer, source_stage, destrination_stage, 0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);

				//End the Command Buffer's recording Process
				r = vkEndCommandBuffer(command_buffer);
				if (r) return r;

				//Create the submit info
				VkSubmitInfo submit_info = {};
				submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
				submit_info.commandBufferCount = 1;
				submit_info.pCommandBuffers = &command_buffer;

				//Submit The Commands Recorded into the Queue. Then wait for the Graphics Queue to be idle
				r = vkQueueSubmit(m_VkQueueGraphics, 1, &submit_info, VK_NULL_HANDLE);
				if (r) return r;

				r = vkQueueWaitIdle(m_VkQueueGraphics);
				if (r) return r;

				//Free the command buffer from memory
				vkFreeCommandBuffers(m_VkDevice, m_VkCommandPool, 1, &command_buffer);

				//The Command Buffer has ended successfully!
				return VK_SUCCESS;
			}

			//Math Helper Function (Credit: https://graphics.stanford.edu/~seander/bithacks.html)
			//Title: Find integer log base 2 of a 32-bit IEEE float
			uint32_t __log2(const float& v) {
				int32_t c; // 32-bit int c gets the result;
				int32_t x;
				memcpy(&x, &v, sizeof(x)); //[OR, not for portability: int x = *(const int *) &v;]
				c = x >> 23;
				c -= 127;
				return c;
			}

			//Destroy Query Info
			void DestroyQueryVariable() {
				if (m_VkQueryInfo) {
				//Delete Const Chars
				if (m_VkQueryInfo->instanceLayers)			 	delete[] m_VkQueryInfo->instanceLayers;
				if (m_VkQueryInfo->instanceExtensions)			delete[] m_VkQueryInfo->instanceExtensions;	
				if (m_VkQueryInfo->deviceExtensions)			delete[] m_VkQueryInfo->deviceExtensions;

				//Delete Void*
				if (m_VkQueryInfo->instanceLayerProperties)		delete static_cast<VkLayerProperties*>(m_VkQueryInfo->instanceLayerProperties);
				if (m_VkQueryInfo->instanceExtensionProperties)	delete static_cast<VkExtensionProperties*>(m_VkQueryInfo->instanceExtensionProperties);
				if (m_VkQueryInfo->deviceExtensionProperties)	delete static_cast<VkExtensionProperties*>(m_VkQueryInfo->deviceExtensionProperties);
				if (m_VkQueryInfo->physicalDeviceFeatures)		delete static_cast<VkPhysicalDeviceFeatures*>(m_VkQueryInfo->physicalDeviceFeatures);

				//Delete Itself
				delete[] m_VkQueryInfo; m_VkQueryInfo = nullptr;
				}
			}

			//Construct Query Info
			void CreateQueryVariable() {
				m_VkQueryInfo = new GVulkanSurfaceQueryInfo();
				memset(m_VkQueryInfo, 0, sizeof(GVulkanSurfaceQueryInfo));
			}

		private:
			//GVulkan Structs
			GVulkanSurfaceQueryInfo* m_VkQueryInfo = nullptr;
			GVulkanSurfacePlatformFucts m_VulkanHelperOS;

			//GWindow Properties
			SYSTEM::GWindow m_GWindow;
			SYSTEM::UNIVERSAL_WINDOW_HANDLE m_WindowHandle = { nullptr, nullptr };
			VkExtent2D m_WindowTopLeft = {0, 0};
			VkExtent2D m_WindowExtent = {0, 0};
			GW::CORE::GEventResponder m_EventResponder;
			char* m_WindowName = nullptr;
			float m_AspectRatio = 0;
			bool m_Deallocated = false;
			bool m_NoDrawing = false;

			//Main Vulkan Objects
			VkInstance m_VkInstance = VK_NULL_HANDLE;
			VkSurfaceKHR m_VkSurfaceKHR = VK_NULL_HANDLE;
			VkPhysicalDevice m_VkPhysicalDevice = VK_NULL_HANDLE;
			VkDevice m_VkDevice = VK_NULL_HANDLE;
			VkCommandPool m_VkCommandPool = VK_NULL_HANDLE;
			VkSwapchainKHR m_VkSwapchainKHR = VK_NULL_HANDLE;

			//Feature-Based Objects & Properties
			struct GVkImage {
				VkImage image = VK_NULL_HANDLE;
				VkImageView view = VK_NULL_HANDLE;
				VkDeviceMemory memory = VK_NULL_HANDLE;
				VkDeviceSize size = 0;
			};
			GVkImage m_GVkImageMSAA;
			GVkImage m_GVkImageDepth;
			bool m_VSync;
			bool m_MSAAOn; //MSAA toggle

			//Frame-Based Vulkan Objects & Properties
			uint32_t m_MaxFrameCount = 0;
			uint32_t m_CurrentFrame = 0;
			uint32_t m_TargetImage = 0;
			static constexpr unsigned GVK_SWAP_BUFFER_LIMIT = 8; 
			VkImage m_VkImageSwapchain[GVK_SWAP_BUFFER_LIMIT]{}; 
			VkImageView m_VkImageViewSwapchain[GVK_SWAP_BUFFER_LIMIT]{};
			VkFramebuffer m_VkFramebuffer[GVK_SWAP_BUFFER_LIMIT]{}; 
			VkCommandBuffer m_VkCommandBuffer[GVK_SWAP_BUFFER_LIMIT]{};
			VkSemaphore m_VkSemaphoreImageAvailable[GVK_SWAP_BUFFER_LIMIT]{}; 
			VkSemaphore m_VkSemaphoreRenderFinished[GVK_SWAP_BUFFER_LIMIT]{}; 
			VkFence m_VkFenceRendering[GVK_SWAP_BUFFER_LIMIT]{};
			
			//Other Vulkan Objects/Properties
			const char** m_InstanceLayers = nullptr;			uint32_t m_InstanceLayerCount = 0;
// older versions of Vulkan do not have layer settings
#if defined(VK_EXT_layer_settings)
			VkLayerSettingEXT* m_LayerSettingsEXT = nullptr;	uint32_t m_LayerSettingEXTCount = 0;
#endif
			const char** m_InstanceExtensions = nullptr;		uint32_t m_InstanceExtensionCount = 0;
			const char** m_DeviceExtensions = nullptr;			uint32_t m_DeviceExtensionCount = 0;
			uint64_t m_InitMask = 0;
			bool m_AllPhysicalDeviceFeatures = false;
			int m_QueueFamilyIndices[2] = {-1, -1};
			VkBool32 m_CanCompute = VK_FALSE;
			VkSampleCountFlagBits m_MSAA = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
			VkQueue m_VkQueueGraphics = VK_NULL_HANDLE;
			VkQueue m_VkQueuePresent = VK_NULL_HANDLE;
			VkSurfaceCapabilitiesKHR m_VkSurfaceCapabilitiesKHR = { 0, 0, {0, 0}, {0, 0}, {0, 0}, 0, VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR, VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR, VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR, VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM };
			VkPresentModeKHR m_VkPresentModeKHRSurface = VK_PRESENT_MODE_MAX_ENUM_KHR;
			VkExtent2D m_VkExtent2DSurface = {0, 0};
			VkSurfaceFormatKHR m_VkFormatSurface = {VK_FORMAT_UNDEFINED, VK_COLOR_SPACE_MAX_ENUM_KHR};
			VkFormat m_VkFormatDepth = VK_FORMAT_UNDEFINED; //format for depth buffer
			VkRenderPass m_VkRenderPass = VK_NULL_HANDLE;
			bool m_FrameLocked = false;
        };
	} // end I namespace
} // end GW namespace
