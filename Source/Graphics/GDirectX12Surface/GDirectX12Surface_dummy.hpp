namespace GW
{
	namespace I
	{
		class GDirectX12SurfaceImplementation : public virtual GDirectX12SurfaceInterface
		{
		public:
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask) { return GReturn::FEATURE_UNSUPPORTED; }

			GReturn GetAspectRatio(float& outRatio) const override { return GReturn::FAILURE; }
			GReturn GetSwapchain4(void** ppOutSwapchain) const override { return GReturn::FAILURE; }
			GReturn GetSwapChainBufferIndex(unsigned int& outSwapChainBufferIndex) const override { return GReturn::FAILURE; }

			GReturn GetDevice(void** ppOutDevice) const override { return GReturn::FAILURE; }

			GReturn GetCommandList(void** ppOutDirectCommandList) const override { return GReturn::FAILURE; }
			GReturn GetCommandAllocator(void** ppOutDirectCommandAllocator) const override { return GReturn::FAILURE; }
			GReturn GetCommandQueue(void** ppOutDirectCommandQueue) const override { return GReturn::FAILURE; }
			GReturn GetFence(void** ppOutDirectFence) const override { return GReturn::FAILURE; }

			GReturn GetCBSRUADescriptorSize(unsigned int& outCBSRUADescriptorSize) const override { return GReturn::FAILURE; }
			GReturn GetSamplerDescriptorSize(unsigned int& outSamplerDescriptorSize) const override { return GReturn::FAILURE; }
			GReturn GetRenderTargetDescriptorSize(unsigned int& outRenderTargetDescriptorSize) const override { return GReturn::FAILURE; }
			GReturn GetDepthStencilDescriptorSize(unsigned int& outDepthStencilDescriptorSize) const override { return GReturn::FAILURE; }

			GReturn GetCurrentRenderTarget(void** ppOutRenderTarget) const override { return GReturn::FAILURE; }
			GReturn GetDepthStencil(void** ppOutDepthStencil) const override { return GReturn::FAILURE; }

			GReturn GetCurrentRenderTargetView(void* pOutRenderTargetView) const override { return GReturn::FAILURE; }
			GReturn GetDepthStencilView(void* pOutDepthStencilView) const override { return GReturn::FAILURE; }

			GReturn StartFrame() override { return GReturn::FAILURE; }
			GReturn EndFrame(bool VSync) override { return GReturn::FAILURE; }

			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override { return GReturn::FAILURE; }
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override { return GReturn::FAILURE; }
			GReturn Invoke() const override { return GReturn::FAILURE; }
			GReturn Invoke(const GEvent& _incomingEvent) const override { return GReturn::FAILURE; }
		};
	}
}