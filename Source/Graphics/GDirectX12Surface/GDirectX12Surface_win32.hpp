#pragma warning(disable : 26812) // prefer enum class over enum warning
#include <wrl/client.h>
#include <dxgi1_6.h>
#if defined(_DEBUG)
#include <dxgidebug.h>
#endif
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dxguid.lib")
#include <d3d12.h>
#pragma comment(lib, "d3d12.lib")
#include <vector>

namespace GW
{
	namespace I
	{
		class GDirectX12SurfaceImplementation : public virtual GDirectX12SurfaceInterface,
			private virtual GEventResponderImplementation, private virtual GThreadSharedImplementation
		{
		private:
			template<class T>
			void SafeRelease(T*& COMObject)
			{
				if (COMObject)
				{
					COMObject->Release();
					COMObject = nullptr;
				}
			}

			GW::SYSTEM::GWindow m_GWindow;
			GW::CORE::GEventResponder m_GEventResponder;
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE m_UWH;
			unsigned int m_Width = 0;
			unsigned int m_Height = 0;
			float m_AspectRatio = 0.0f;

			bool m_Deallocated;

			static constexpr UINT m_SwapChainBufferCount = 2u; // How many swapchain back buffers will be createcd

			DXGI_FORMAT m_SwapChainBufferFormat;
			DXGI_FORMAT m_DepthBufferFormat;
			bool m_DepthBufferSupport;

			IDXGIFactory6* m_pDXGIFactory = nullptr;
			IDXGISwapChain4* m_pSwapChain = nullptr;
			IDXGIAdapter4* m_pAdapter = nullptr;

			ID3D12Device* m_pDevice = nullptr;

			struct CommandList
			{
				ID3D12CommandAllocator* m_pCommandAllocator;
				ID3D12GraphicsCommandList* m_pCommandList;

				CommandList()
				{
					m_pCommandAllocator = nullptr;
					m_pCommandList = nullptr;
				}
			} m_CommandList;

			struct CommandQueue
			{
				ID3D12CommandQueue* m_pCommandQueue;
				ID3D12Fence* m_pFence;
				HANDLE m_EventHandle; // Handle used to synchronize
				UINT64 m_FenceValue; // The fence value CPU should wait for

				operator bool()
				{
					return m_pCommandQueue && m_pFence && m_EventHandle;
				}

				void Signal()
				{
					m_pCommandQueue->Signal(m_pFence, ++m_FenceValue);
				}

				void Flush()
				{
					if (!(m_pFence->GetCompletedValue() >= m_FenceValue))
					{
						HRESULT hr = m_pFence->SetEventOnCompletion(m_FenceValue, m_EventHandle);
						if (FAILED(hr))
							return;
						::WaitForSingleObjectEx(m_EventHandle, INFINITE, FALSE);
					}
				}

				CommandQueue()
				{
					m_pCommandQueue = nullptr;
					m_pFence = nullptr;
					m_EventHandle = nullptr;
					m_FenceValue = 0u;
				}
			} m_CommandQueue;

			struct DescriptorInfo
			{
				UINT CBSRUADescriptorSize;
				UINT SamplerDescriptorSize;
				UINT RenderTargetDescriptorSize;
				UINT DepthStencilDescriptorSize;

				DescriptorInfo()
				{
					CBSRUADescriptorSize = 0;
					SamplerDescriptorSize = 0;
					RenderTargetDescriptorSize = 0;
					DepthStencilDescriptorSize = 0;
				}
			} m_DescriptorInfo;

			ID3D12DescriptorHeap* m_pRenderTargetDescriptorHeap = nullptr;
			ID3D12DescriptorHeap* m_pDepthStencilDescriptorHeap = nullptr;

			ID3D12Resource* m_pRenderTargets[m_SwapChainBufferCount] = { nullptr, nullptr };
			ID3D12Resource* m_pDepthStencil = nullptr;

			D3D12_VIEWPORT m_ScreenViewport = {};
			D3D12_RECT m_ScissorRect = {};

			GReturn CreateFactory()
			{
				UINT factoryFlags = 0;
#if defined (_DEBUG)
				factoryFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif
				HRESULT hr = ::CreateDXGIFactory2(factoryFlags, IID_PPV_ARGS(&m_pDXGIFactory));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				return GW::GReturn::SUCCESS;
			}

			GReturn QueryAdapter()
			{
				std::vector<Microsoft::WRL::ComPtr<IDXGIAdapter4>> m_pAdapters;
				// Enumerate hardware
				{
					// Enumerating iGPU, dGPU, xGPU
					Microsoft::WRL::ComPtr<IDXGIAdapter1> pAdapter1 = nullptr;
					Microsoft::WRL::ComPtr<IDXGIAdapter4> pAdapter4 = nullptr;
					for (UINT index = 0; m_pDXGIFactory->EnumAdapterByGpuPreference(index, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(pAdapter1.GetAddressOf())) != DXGI_ERROR_NOT_FOUND; ++index)
					{
						pAdapter1.As(&pAdapter4);
						m_pAdapters.push_back(pAdapter4);
						pAdapter1.Reset();
					}
				}

				if (m_pAdapters.size() == 0)
					return GW::GReturn::HARDWARE_UNAVAILABLE;

				// Quering adapter
				{
					HRESULT hardwareResult = E_FAIL;
					DXGI_ADAPTER_DESC3 adapterDesc;

					for (unsigned int i = 0; i < static_cast<unsigned int>(m_pAdapters.size()); ++i)
					{
						HRESULT hr = m_pAdapters[i]->GetDesc3(&adapterDesc);
						if (FAILED(hr))
							return GW::GReturn::FAILURE;
						// Attempt to create a DX12 compatible device
						if ((adapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0)
						{
							hardwareResult = ::D3D12CreateDevice(
								m_pAdapters[i].Get(),
								D3D_FEATURE_LEVEL_11_0,
								__uuidof(ID3D12Device), nullptr);
							if (SUCCEEDED(hardwareResult))
							{
								m_pAdapter = m_pAdapters[i].Get();
								m_pAdapter->AddRef();
								break;
							}
						}
					}

					if (!m_pAdapter)
						return GW::GReturn::HARDWARE_UNAVAILABLE;
				}
				return GW::GReturn::SUCCESS;
			}

			GReturn CreateDevice()
			{
				HRESULT hr;
#if defined(_DEBUG)
				// NOTE: Enabling the debug layer after creating the ID3D12Device will cause the DX runtime to remove the device.
				ID3D12Debug* pDebug = nullptr;
				hr = ::D3D12GetDebugInterface(IID_PPV_ARGS(&pDebug));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				pDebug->EnableDebugLayer();
				pDebug->Release();
#endif

				// Create our virtual device used for interacting with the GPU so we can create resources
				{
					hr = ::D3D12CreateDevice(m_pAdapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_pDevice));
					if (FAILED(hr))
						return GW::GReturn::FAILURE;
				}

				// Query descriptor size (descriptor size vary based on GPU vendor)
				{
					m_DescriptorInfo.CBSRUADescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
					m_DescriptorInfo.SamplerDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);
					m_DescriptorInfo.RenderTargetDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
					m_DescriptorInfo.DepthStencilDescriptorSize = m_pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
				}
				return GW::GReturn::SUCCESS;
			}

			GReturn CreateCommandLists()
			{
				HRESULT hr = m_pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_CommandList.m_pCommandAllocator));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				hr = m_pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT,
					m_CommandList.m_pCommandAllocator,
					nullptr, IID_PPV_ARGS(&m_CommandList.m_pCommandList));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				hr = m_CommandList.m_pCommandList->Close();
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				return GW::GReturn::SUCCESS;
			}

			GReturn CreateCommandQueues()
			{
				D3D12_COMMAND_QUEUE_DESC commandQueueDesc = {};
				commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
				commandQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
				commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
				commandQueueDesc.NodeMask = 0;
				HRESULT hr = m_pDevice->CreateCommandQueue(&commandQueueDesc, IID_PPV_ARGS(&m_CommandQueue.m_pCommandQueue));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				hr = m_pDevice->CreateFence(m_CommandQueue.m_FenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_CommandQueue.m_pFence));
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				m_CommandQueue.m_EventHandle = ::CreateEventEx(nullptr, nullptr, false, EVENT_ALL_ACCESS);
				if (!m_CommandQueue.m_EventHandle)
					return GW::GReturn::FAILURE;
				return GW::GReturn::SUCCESS;
			}

			GReturn CreateSwapChain()
			{
				Microsoft::WRL::ComPtr<IDXGISwapChain4> pSwapChain;
				DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
				swapChainDesc.Width = m_Width;
				swapChainDesc.Height = m_Height;
				swapChainDesc.Format = m_SwapChainBufferFormat;
				swapChainDesc.Stereo = FALSE;
				swapChainDesc.SampleDesc.Count = 1u;
				swapChainDesc.SampleDesc.Quality = 0u;
				swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainDesc.BufferCount = m_SwapChainBufferCount;
				swapChainDesc.Scaling = DXGI_SCALING_NONE;
				swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
				swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
				swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
				Microsoft::WRL::ComPtr<IDXGISwapChain1> pSwapChain1;
				HRESULT hr = m_pDXGIFactory->CreateSwapChainForHwnd(m_CommandQueue.m_pCommandQueue,
					static_cast<HWND>(m_UWH.window), &swapChainDesc, nullptr, nullptr, pSwapChain1.GetAddressOf());
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				hr = m_pDXGIFactory->MakeWindowAssociation(static_cast<HWND>(m_UWH.window), DXGI_MWA_NO_ALT_ENTER);
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				hr = pSwapChain1.As(&pSwapChain);
				if (FAILED(hr))
					return GW::GReturn::FAILURE;
				m_pSwapChain = pSwapChain.Get();
				m_pSwapChain->AddRef();
				return GW::GReturn::SUCCESS;
			}

			GReturn CreateDescriptorHeap()
			{
				// RT Descriptor Heap
				{
					D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc;
					descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
					descriptorHeapDesc.NumDescriptors = m_SwapChainBufferCount;
					descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
					descriptorHeapDesc.NodeMask = 0;

					HRESULT hr = m_pDevice->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(&m_pRenderTargetDescriptorHeap));
					if (FAILED(hr))
						return GW::GReturn::FAILURE;
				}

				if (m_DepthBufferSupport)
				{
					// DS Descriptor Heap
					{
						D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc;
						descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
						descriptorHeapDesc.NumDescriptors = 1;
						descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
						descriptorHeapDesc.NodeMask = 0;

						HRESULT hr = m_pDevice->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(&m_pDepthStencilDescriptorHeap));
						if (FAILED(hr))
							return GW::GReturn::FAILURE;
					}
				}
				return GW::GReturn::SUCCESS;
			}

			void Resize()
			{
				m_GWindow.GetClientWidth(m_Width);
				m_GWindow.GetClientHeight(m_Height);
				m_AspectRatio = static_cast<float>(m_Width) / static_cast<float>(m_Height);

				// Wait for GPU to finish before changing any resources
				m_CommandQueue.Signal();
				m_CommandQueue.Flush();
				{
					// Release resources
					for (unsigned int i = 0; i < m_SwapChainBufferCount; ++i)
						if (m_pRenderTargets[i])
							m_pRenderTargets[i]->Release();

					// Resize backbuffer
					UINT nodes[m_SwapChainBufferCount] = { 0, 0 };
					IUnknown* commandQueues[m_SwapChainBufferCount] =
					{
						m_CommandQueue.m_pCommandQueue,
						m_CommandQueue.m_pCommandQueue
					};
					m_pSwapChain->ResizeBuffers1(m_SwapChainBufferCount, m_Width, m_Height, m_SwapChainBufferFormat,
						DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH, nodes, commandQueues);

					// Recreate RT resource and descriptors
					for (unsigned int i = 0; i < m_SwapChainBufferCount; ++i)
					{
						ID3D12Resource* pBackBuffer = nullptr;
						m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&pBackBuffer));
						m_pRenderTargets[i] = pBackBuffer;
						D3D12_RENDER_TARGET_VIEW_DESC renderTargetViewDesc = {};
						renderTargetViewDesc.Format = m_SwapChainBufferFormat;
						renderTargetViewDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
						renderTargetViewDesc.Texture2D.MipSlice = 0;
						renderTargetViewDesc.Texture2D.PlaneSlice = 0;
						D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle;
						cpuHandle.ptr = m_pRenderTargetDescriptorHeap->GetCPUDescriptorHandleForHeapStart().ptr + (static_cast<size_t>(i) * m_DescriptorInfo.RenderTargetDescriptorSize);
						m_pDevice->CreateRenderTargetView(m_pRenderTargets[i], &renderTargetViewDesc, cpuHandle);
					}

					if (m_DepthBufferSupport)
					{
						// Recreate DS resource and descriptors
						if (m_pDepthStencil)
						{
							m_pDepthStencil->Release();
							m_pDepthStencil = nullptr;
						}

						D3D12_RESOURCE_DESC resourceDesc = {};
						resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
						resourceDesc.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
						resourceDesc.Width = m_Width;
						resourceDesc.Height = m_Height;
						resourceDesc.DepthOrArraySize = resourceDesc.MipLevels = 1;
						resourceDesc.Format = m_DepthBufferFormat;
						resourceDesc.SampleDesc = { 1, 0 };
						resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
						resourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

						D3D12_CLEAR_VALUE clearValue = {};
						clearValue.Format = m_DepthBufferFormat;
						clearValue.DepthStencil.Depth = 1.0f;
						clearValue.DepthStencil.Stencil = 0;

						// Create GPU Memory
						{
							D3D12_HEAP_PROPERTIES defaultHeapProperties;
							defaultHeapProperties.Type = D3D12_HEAP_TYPE_DEFAULT;
							defaultHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
							defaultHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
							defaultHeapProperties.CreationNodeMask = 1;
							defaultHeapProperties.VisibleNodeMask = 1;
							const D3D12_HEAP_FLAGS heapFlags = D3D12_HEAP_FLAG_NONE;
							const D3D12_RESOURCE_STATES depthWriteState = D3D12_RESOURCE_STATE_DEPTH_WRITE;
							m_pDevice->CreateCommittedResource(&defaultHeapProperties, heapFlags,
								&resourceDesc, depthWriteState, &clearValue, IID_PPV_ARGS(&m_pDepthStencil));
						}

						D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc = {};
						depthStencilViewDesc.Format = m_DepthBufferFormat;
						depthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
						depthStencilViewDesc.Flags = D3D12_DSV_FLAG_NONE;
						depthStencilViewDesc.Texture2D.MipSlice = 0;
						m_pDevice->CreateDepthStencilView(m_pDepthStencil, &depthStencilViewDesc, m_pDepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
					}

					// Update the viewport transform to cover the client area.
					m_ScreenViewport.TopLeftX = m_ScreenViewport.TopLeftY = 0.0f;
					m_ScreenViewport.Width = static_cast<FLOAT>(m_Width);
					m_ScreenViewport.Height = static_cast<FLOAT>(m_Height);
					m_ScreenViewport.MinDepth = 0.0f;
					m_ScreenViewport.MaxDepth = 1.0f;
					m_ScissorRect.left = m_ScissorRect.top = 0;
					m_ScissorRect.right = m_Width;
					m_ScissorRect.bottom = m_Height;
				}
				// Wait until resize is complete.
				m_CommandQueue.Signal();
				m_CommandQueue.Flush();
			}

			void Release()
			{
				if (!m_Deallocated)
				{
					if (m_CommandQueue)
					{
						m_CommandQueue.Signal();
						m_CommandQueue.Flush();
					}
					SafeRelease(m_pDepthStencil);
					for (unsigned int i = 0; i < m_SwapChainBufferCount; ++i)
					{
						SafeRelease(m_pRenderTargets[i]);
					}
					SafeRelease(m_pDepthStencilDescriptorHeap);
					SafeRelease(m_pRenderTargetDescriptorHeap);
					::CloseHandle(m_CommandQueue.m_EventHandle);
					if (m_CommandQueue)
					{
						m_CommandQueue.m_EventHandle = nullptr;
					}
					SafeRelease(m_CommandQueue.m_pFence);
					SafeRelease(m_CommandQueue.m_pCommandQueue);
					SafeRelease(m_CommandList.m_pCommandList);
					SafeRelease(m_CommandList.m_pCommandAllocator);
					SafeRelease(m_pDevice);
					SafeRelease(m_pAdapter);
					SafeRelease(m_pSwapChain);
					SafeRelease(m_pDXGIFactory);
					m_Deallocated = true;
				}
			}
		public:
			~GDirectX12SurfaceImplementation()
			{
				Release();
			}

			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask)
			{

				if (!_gwindow)
					return GReturn::INVALID_ARGUMENT;

				m_GWindow = _gwindow;
				//Check if valid _initMask was passed in
				unsigned long long allowed = ~(GW::GRAPHICS::COLOR_10_BIT | GW::GRAPHICS::DEPTH_BUFFER_SUPPORT | GW::GRAPHICS::DEPTH_STENCIL_SUPPORT);
				if (allowed & _initMask)
				{
					return GReturn::INVALID_ARGUMENT;
				}

				// Set swap chain buffer format
				if (_initMask & GW::GRAPHICS::COLOR_10_BIT)
					m_SwapChainBufferFormat = DXGI_FORMAT_R10G10B10A2_UNORM;
				else if (_initMask & GW::GRAPHICS::DIRECT2D_SUPPORT)
					m_SwapChainBufferFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
				else
					m_SwapChainBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;

				// If depth buffer or depth stencil support is requested, set m_DepthBufferSupport to true, else false
				if (_initMask & GW::GRAPHICS::DEPTH_BUFFER_SUPPORT || _initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
				{
					m_DepthBufferSupport = true;
					// If depth stencil support is requested set format to DXGI_FORMAT_D24_UNORM_S8_UINT, else DXGI_FORMAT_D32_FLOAT
					if (_initMask & GW::GRAPHICS::DEPTH_STENCIL_SUPPORT)
						m_DepthBufferFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
					else
						m_DepthBufferFormat = DXGI_FORMAT_D32_FLOAT;
				}
				else
					m_DepthBufferSupport = false;

				m_GWindow.GetWindowHandle(m_UWH);
				m_GWindow.GetClientWidth(m_Width);
				m_GWindow.GetClientHeight(m_Height);
				m_AspectRatio = static_cast<float>(m_Width) / static_cast<float>(m_Height);

				m_Deallocated = false;

				GReturn gr = CreateFactory();
				if (G_FAIL(gr))
					return gr;

				gr = QueryAdapter();
				if (G_FAIL(gr))
					return gr;

				gr = CreateDevice();
				if (G_FAIL(gr))
					return gr;

				gr = CreateCommandLists();
				if (G_FAIL(gr))
					return gr;

				gr = CreateCommandQueues();
				if (G_FAIL(gr))
					return gr;

				gr = CreateSwapChain();
				if (G_FAIL(gr))
					return gr;

				gr = CreateDescriptorHeap();
				if (G_FAIL(gr))
					return gr;

				Resize();

				m_GEventResponder.Create([&](const GW::GEvent& event)
				{
					GW::SYSTEM::GWindow::Events windowEvent;
					GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
					if (+event.Read(windowEvent, windowEventData))
					{
						switch (windowEvent)
						{
						case GW::SYSTEM::GWindow::Events::MINIMIZE: {} break;
						case GW::SYSTEM::GWindow::Events::DESTROY:
						{
							Release();
						}
						break;

						case GW::SYSTEM::GWindow::Events::MOVE:
						case GW::SYSTEM::GWindow::Events::MAXIMIZE:
						case GW::SYSTEM::GWindow::Events::RESIZE:
						{
							Resize();
						}
						break;
						}
					}
				});
				return m_GWindow.Register(m_GEventResponder);
			}

			GReturn GetAspectRatio(float& outRatio) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_GWindow)
					return GReturn::FAILURE;

				outRatio = m_AspectRatio;
				return GReturn::SUCCESS;
			}

			GReturn GetSwapchain4(void** ppOutSwapchain) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pSwapChain)
					return GReturn::FAILURE;

				if (!ppOutSwapchain)
					return GReturn::INVALID_ARGUMENT;

				*ppOutSwapchain = m_pSwapChain;
				m_pSwapChain->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetSwapChainBufferIndex(unsigned int& outSwapChainBufferIndex) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pSwapChain)
					return GReturn::FAILURE;

				outSwapChainBufferIndex = m_pSwapChain->GetCurrentBackBufferIndex();
				return GReturn::SUCCESS;
			}

			GReturn GetDevice(void** ppOutDevice) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDevice)
					return GReturn::FAILURE;

				if (!ppOutDevice)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDevice = m_pDevice;
				m_pDevice->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetCommandList(void** ppOutDirectCommandList) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandList.m_pCommandList)
					return GReturn::FAILURE;

				if (!ppOutDirectCommandList)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDirectCommandList = m_CommandList.m_pCommandList;
				m_CommandList.m_pCommandList->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetCommandAllocator(void** ppOutDirectCommandAllocator) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandList.m_pCommandAllocator)
					return GReturn::FAILURE;

				if (!ppOutDirectCommandAllocator)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDirectCommandAllocator = m_CommandList.m_pCommandAllocator;
				m_CommandList.m_pCommandAllocator->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetCommandQueue(void** ppOutDirectCommandQueue) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandQueue.m_pCommandQueue)
					return GReturn::FAILURE;

				if (!ppOutDirectCommandQueue)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDirectCommandQueue = m_CommandQueue.m_pCommandQueue;
				m_CommandQueue.m_pCommandQueue->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetFence(void** ppOutDirectFence) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandQueue.m_pFence)
					return GReturn::FAILURE;

				if (!ppOutDirectFence)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDirectFence = m_CommandQueue.m_pFence;
				m_CommandQueue.m_pFence->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetCBSRUADescriptorSize(unsigned int& outCBSRUADescriptorSize) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDevice)
					return GReturn::FAILURE;

				outCBSRUADescriptorSize = m_DescriptorInfo.CBSRUADescriptorSize;
				return GReturn::SUCCESS;
			}
			GReturn GetSamplerDescriptorSize(unsigned int& outSamplerDescriptorSize) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDevice)
					return GReturn::FAILURE;

				outSamplerDescriptorSize = m_DescriptorInfo.SamplerDescriptorSize;
				return GReturn::SUCCESS;
			}
			GReturn GetRenderTargetDescriptorSize(unsigned int& outRenderTargetDescriptorSize) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDevice)
					return GReturn::FAILURE;

				outRenderTargetDescriptorSize = m_DescriptorInfo.RenderTargetDescriptorSize;
				return GReturn::SUCCESS;
			}
			GReturn GetDepthStencilDescriptorSize(unsigned int& outDepthStencilDescriptorSize) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDevice)
					return GReturn::FAILURE;

				outDepthStencilDescriptorSize = m_DescriptorInfo.DepthStencilDescriptorSize;
				return GReturn::SUCCESS;
			}

			GReturn GetCurrentRenderTarget(void** ppOutRenderTarget) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pRenderTargets[m_pSwapChain->GetCurrentBackBufferIndex()])
					return GReturn::FAILURE;

				if (!ppOutRenderTarget)
					return GReturn::INVALID_ARGUMENT;

				*ppOutRenderTarget = m_pRenderTargets[m_pSwapChain->GetCurrentBackBufferIndex()];
				m_pRenderTargets[m_pSwapChain->GetCurrentBackBufferIndex()]->AddRef();
				return GReturn::SUCCESS;
			}
			GReturn GetDepthStencil(void** ppOutDepthStencil) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDepthStencil)
					return GReturn::FAILURE;

				if (!ppOutDepthStencil)
					return GReturn::INVALID_ARGUMENT;

				*ppOutDepthStencil = m_pDepthStencil;
				m_pDepthStencil->AddRef();
				return GReturn::SUCCESS;
			}

			GReturn GetCurrentRenderTargetView(void* pOutRenderTargetView) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pRenderTargetDescriptorHeap)
					return GReturn::FAILURE;

				if (!pOutRenderTargetView)
					return GReturn::INVALID_ARGUMENT;

				reinterpret_cast<D3D12_CPU_DESCRIPTOR_HANDLE*>(pOutRenderTargetView)->ptr =
					m_pRenderTargetDescriptorHeap->GetCPUDescriptorHandleForHeapStart().ptr + (SIZE_T(m_pSwapChain->GetCurrentBackBufferIndex()) * SIZE_T(m_DescriptorInfo.RenderTargetDescriptorSize));
				return GReturn::SUCCESS;
			}
			GReturn GetDepthStencilView(void* pOutDepthStencilView) const override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_pDepthStencilDescriptorHeap)
					return GReturn::FAILURE;

				if (!pOutDepthStencilView)
					return GReturn::INVALID_ARGUMENT;

				reinterpret_cast<D3D12_CPU_DESCRIPTOR_HANDLE*>(pOutDepthStencilView)->ptr = m_pDepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart().ptr;
				return GReturn::SUCCESS;
			}

			GReturn StartFrame() override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandQueue.m_pCommandQueue ||
					!m_CommandList.m_pCommandAllocator ||
					!m_CommandList.m_pCommandList)
					return GReturn::FAILURE;

				m_CommandQueue.Signal();
				m_CommandQueue.Flush();
				m_CommandList.m_pCommandAllocator->Reset();
				m_CommandList.m_pCommandList->Reset(m_CommandList.m_pCommandAllocator, nullptr);

				D3D12_RESOURCE_BARRIER rtTransitionBarrier = {};
				rtTransitionBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				rtTransitionBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
				rtTransitionBarrier.Transition.pResource = m_pRenderTargets[m_pSwapChain->GetCurrentBackBufferIndex()];
				rtTransitionBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
				rtTransitionBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
				rtTransitionBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
				m_CommandList.m_pCommandList->ResourceBarrier(1, &rtTransitionBarrier);

				m_CommandList.m_pCommandList->RSSetViewports(1, &m_ScreenViewport);
				m_CommandList.m_pCommandList->RSSetScissorRects(1, &m_ScissorRect);

				return GReturn::SUCCESS;
			}
			GReturn EndFrame(bool VSync) override
			{
				if (m_Deallocated)
					return GReturn::PREMATURE_DEALLOCATION;

				if (!m_CommandQueue.m_pCommandQueue ||
					!m_CommandList.m_pCommandAllocator ||
					!m_CommandList.m_pCommandList ||
					!m_pSwapChain)
					return GReturn::FAILURE;

				D3D12_RESOURCE_BARRIER rtTransitionBarrier = {};
				rtTransitionBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				rtTransitionBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
				rtTransitionBarrier.Transition.pResource = m_pRenderTargets[m_pSwapChain->GetCurrentBackBufferIndex()];
				rtTransitionBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
				rtTransitionBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
				rtTransitionBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
				m_CommandList.m_pCommandList->ResourceBarrier(1, &rtTransitionBarrier);

				ID3D12CommandList* DirectCommandList[] = { m_CommandList.m_pCommandList };
				m_CommandList.m_pCommandList->Close();
				m_CommandQueue.m_pCommandQueue->ExecuteCommandLists(1, DirectCommandList);

				UINT SyncInterval = VSync ? 1u : 0u;
				UINT PresentFlags = 0u;
				DXGI_PRESENT_PARAMETERS PresentParameters = { 0u, NULL, NULL, NULL };
				HRESULT hr = m_pSwapChain->Present1(SyncInterval, PresentFlags, &PresentParameters);
				if (FAILED(hr))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}
			//GEventResponderInterface
			GReturn Assign(std::function<void()> _newHandler) override {
				return m_GEventResponder.Assign(_newHandler);
			}
			GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) override {
				return m_GEventResponder.Assign(_newEventHandler);
			}
			GReturn Invoke() const override {
				return m_GEventResponder.Invoke();
			}
			GReturn Invoke(const GEvent& _incomingEvent) const override {
				return m_GEventResponder.Invoke(_incomingEvent);
			}
		};
	}
}