// dummy implementation of GInput

namespace GW
{
	namespace I
	{
		class GInputImplementation : public virtual GInputInterface
		{
		public:
			GReturn GetState(int _keyCode, float& _outState) override
			{
				return GReturn::FAILURE;
			}
			GReturn GetMouseDelta(float& _x, float& _y) override
			{
				return GReturn::FAILURE;
			}
			GReturn GetMousePosition(float& _x, float& _y) const override
			{
				return GReturn::FAILURE;
			}
			GReturn GetKeyMask(unsigned int& _outKeyMask) const override
			{
				return GReturn::FAILURE;
			}
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _dummyHandle)
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Create(const GW::SYSTEM::GWindow _dummyWindow)
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}
		};
	}
}
