// Need include guard for this because GInput and GBufferedInput uses this file
#ifndef GINPUTTABLEROUTING_HPP
#define GINPUTTABLEROUTING_HPP

// Defines platform specific key codes that is shared between
// GInput and GBufferedInput
namespace GW
{
	namespace I
	{
#if defined(_WIN32)
#include <winapifamily.h>
#if (WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP)

#define G_MAX_KEYCODES 128

		constexpr unsigned int Keycodes[G_MAX_KEYCODES] =
		{
			G_KEY_UNKNOWN,				// 0
			G_KEY_ESCAPE,				// 1
			G_KEY_1,					// 2
			G_KEY_2,					// 3
			G_KEY_3,					// 4
			G_KEY_4,					// 5
			G_KEY_5,					// 6
			G_KEY_6,					// 7
			G_KEY_7,					// 8
			G_KEY_8,					// 9
			G_KEY_9,					// 10
			G_KEY_0,					// 11
			G_KEY_MINUS,				// 12
			G_KEY_EQUALS,				// 13
			G_KEY_BACKSPACE,			// 14
			G_KEY_TAB,					// 15
			G_KEY_Q,					// 16
			G_KEY_W,					// 17
			G_KEY_E,					// 18
			G_KEY_R,					// 19
			G_KEY_T,					// 20
			G_KEY_Y,					// 21
			G_KEY_U,					// 22
			G_KEY_I,					// 23
			G_KEY_O,					// 24
			G_KEY_P,					// 25
			G_KEY_BRACKET_OPEN,			// 26
			G_KEY_BRACKET_CLOSE,		// 27
			G_KEY_ENTER,				// 28
			G_KEY_LEFTCONTROL,			// 29
			G_KEY_A,					// 30
			G_KEY_S,					// 31
			G_KEY_D,					// 32
			G_KEY_F,					// 33
			G_KEY_G,					// 34
			G_KEY_H,					// 35
			G_KEY_J,					// 36
			G_KEY_K,					// 37
			G_KEY_L,					// 38
			G_KEY_SEMICOLON,			// 39
			G_KEY_QUOTE,				// 40
			G_KEY_TILDE,				// 41
			G_KEY_LEFTSHIFT,			// 42
			G_KEY_BACKSLASH,			// 43
			G_KEY_Z,					// 44
			G_KEY_X,					// 45
			G_KEY_C,					// 46
			G_KEY_V,					// 47
			G_KEY_B,					// 48
			G_KEY_N,					// 49
			G_KEY_M,					// 50
			G_KEY_COMMA,				// 51
			G_KEY_PERIOD,				// 52
			G_KEY_FORWARDSLASH,			// 53
			G_KEY_RIGHTSHIFT,			// 54
			G_KEY_NUMPAD_MULTIPLY,		// 55
			G_KEY_LEFTALT,				// 56
			G_KEY_SPACE,				// 57
			G_KEY_CAPSLOCK,				// 58
			G_KEY_F1,					// 59
			G_KEY_F2,					// 60
			G_KEY_F3,					// 61
			G_KEY_F4,					// 62
			G_KEY_F5,					// 63
			G_KEY_F6,					// 64
			G_KEY_F7,					// 65
			G_KEY_F8,					// 66
			G_KEY_F9,					// 67
			G_KEY_F10,					// 68
			G_KEY_NUMLOCK,				// 69
			G_KEY_SCROLL_LOCK,			// 70
			G_KEY_NUMPAD_7,				// 71
			G_KEY_NUMPAD_8,				// 72
			G_KEY_NUMPAD_9,				// 73
			G_KEY_NUMPAD_SUBTRACT,		// 74
			G_KEY_NUMPAD_4,				// 75
			G_KEY_NUMPAD_5,				// 76
			G_KEY_NUMPAD_6,				// 77
			G_KEY_NUMPAD_ADD,			// 78
			G_KEY_NUMPAD_1,				// 79
			G_KEY_NUMPAD_2,				// 80
			G_KEY_NUMPAD_3,				// 81
			G_KEY_NUMPAD_0,				// 82
			G_KEY_NUMPAD_PERIOD,		// 83
			G_KEY_UNKNOWN,				// 84
			G_KEY_UNKNOWN,				// 85
			G_KEY_UNKNOWN,				// 86
			G_KEY_F11,					// 87
			G_KEY_F12,					// 88
			G_KEY_UNKNOWN,				// 89
			G_KEY_UNKNOWN,				// 90
			G_KEY_UNKNOWN,				// 91
			G_KEY_UNKNOWN,				// 92
			G_KEY_UNKNOWN,				// 93
			G_KEY_UNKNOWN,				// 94
			G_KEY_UNKNOWN,				// 95
			G_KEY_UNKNOWN,				// 96
			G_KEY_UNKNOWN,				// 97
			G_KEY_UNKNOWN,				// 98
			G_KEY_UNKNOWN,				// 99
			G_KEY_UNKNOWN,				// 100
			G_KEY_UNKNOWN,				// 101
			G_KEY_UNKNOWN,				// 102
			G_KEY_UNKNOWN,				// 103
			G_KEY_UNKNOWN,				// 104
			G_KEY_UNKNOWN,				// 105
			G_KEY_UNKNOWN,				// 106
			G_KEY_UNKNOWN,				// 107
			G_KEY_UNKNOWN,				// 108
			G_KEY_UNKNOWN,				// 109
			G_KEY_UNKNOWN,				// 110
			G_KEY_UNKNOWN,				// 111
			G_KEY_UNKNOWN,				// 112
			G_KEY_UNKNOWN,				// 113
			G_KEY_UNKNOWN,				// 114
			G_KEY_UNKNOWN,				// 115
			G_KEY_UNKNOWN,				// 116
			G_KEY_UNKNOWN,				// 117
			G_KEY_UNKNOWN,				// 118
			G_KEY_UNKNOWN,				// 119
			G_KEY_UNKNOWN,				// 120
			G_KEY_UNKNOWN,				// 121
			G_KEY_UNKNOWN,				// 122
			G_KEY_UNKNOWN,				// 123
			G_KEY_UNKNOWN,				// 124
			G_KEY_UNKNOWN,				// 125
			G_KEY_UNKNOWN,				// 126
			G_KEY_UNKNOWN				// 127
		};
#elif (WINAPI_FAMILY == WINAPI_FAMILY_APP)

#define G_MAX_KEYCODES 256

		constexpr unsigned int Keycodes[G_MAX_KEYCODES] =
		{
			G_KEY_UNKNOWN,				// 0
			G_BUTTON_LEFT,				// 1
			G_BUTTON_RIGHT,				// 2
			G_KEY_UNKNOWN,				// 3 Cancel
			G_BUTTON_MIDDLE,			// 4
			G_KEY_UNKNOWN,				// 5 XButton1
			G_KEY_UNKNOWN,				// 6 XButton2
			G_KEY_UNKNOWN,				// 7
			G_KEY_BACKSPACE,			// 8
			G_KEY_TAB,					// 9
			G_KEY_UNKNOWN,				// 10
			G_KEY_UNKNOWN,				// 11
			G_KEY_UNKNOWN,				// 12 Clear
			G_KEY_ENTER,				// 13
			G_KEY_UNKNOWN,				// 14
			G_KEY_UNKNOWN,				// 15
			G_KEY_LEFTSHIFT,			// 16 Shift
			G_KEY_LEFTCONTROL,			// 17 Control
			G_KEY_UNKNOWN,				// 18 Menu
			G_KEY_PAUSE,				// 19
			G_KEY_CAPSLOCK,				// 20
			G_KEY_UNKNOWN,				// 21 Kana/Hangul
			G_KEY_UNKNOWN,				// 22 ImeOn
			G_KEY_UNKNOWN,				// 23 Junja
			G_KEY_UNKNOWN,				// 24 Final
			G_KEY_UNKNOWN,				// 25 Kanji
			G_KEY_UNKNOWN,				// 26 ImeOff
			G_KEY_ESCAPE,				// 27 
			G_KEY_UNKNOWN,				// 28 Convert
			G_KEY_UNKNOWN,				// 29 NonConvert
			G_KEY_UNKNOWN,				// 30 Accept
			G_KEY_UNKNOWN,				// 31 ModeChange
			G_KEY_SPACE,				// 32
			G_KEY_PAGEUP,				// 33
			G_KEY_PAGEDOWN,				// 34
			G_KEY_END,					// 35
			G_KEY_HOME,					// 36
			G_KEY_LEFT,					// 37
			G_KEY_UP,					// 38
			G_KEY_RIGHT,				// 39
			G_KEY_DOWN,					// 40
			G_KEY_UNKNOWN,				// 41 Select
			G_KEY_PRINTSCREEN,			// 42
			G_KEY_UNKNOWN,				// 43 Execute
			G_KEY_PRINTSCREEN,				// 44 Snapshot
			G_KEY_INSERT,				// 45
			G_KEY_DELETE,				// 46
			G_KEY_UNKNOWN,				// 47 Help
			G_KEY_0,					// 48
			G_KEY_1,					// 49
			G_KEY_2,					// 50
			G_KEY_3,					// 51
			G_KEY_4,					// 52
			G_KEY_5,					// 53
			G_KEY_6,					// 54
			G_KEY_7,					// 55
			G_KEY_8,					// 56
			G_KEY_9,					// 57
			G_KEY_UNKNOWN,				// 58
			G_KEY_UNKNOWN,				// 59
			G_KEY_UNKNOWN,				// 60
			G_KEY_UNKNOWN,				// 61
			G_KEY_UNKNOWN,				// 62
			G_KEY_UNKNOWN,				// 63
			G_KEY_UNKNOWN,				// 64
			G_KEY_A,					// 65
			G_KEY_B,					// 66
			G_KEY_C,					// 67
			G_KEY_D,					// 68
			G_KEY_E,					// 69
			G_KEY_F,					// 70
			G_KEY_G,					// 71
			G_KEY_H,					// 72
			G_KEY_I,					// 73
			G_KEY_J,					// 74
			G_KEY_K,					// 75
			G_KEY_L,					// 76
			G_KEY_M,					// 77
			G_KEY_N,					// 78
			G_KEY_O,					// 79
			G_KEY_P,					// 80
			G_KEY_Q,					// 81
			G_KEY_R,					// 82
			G_KEY_S,					// 83
			G_KEY_T,					// 84
			G_KEY_U,					// 85
			G_KEY_V,					// 86
			G_KEY_W,					// 87
			G_KEY_X,					// 88
			G_KEY_Y,					// 89
			G_KEY_Z,					// 90
			G_KEY_UNKNOWN,				// 91 LeftWindows
			G_KEY_UNKNOWN,				// 92 RightWindow
			G_KEY_UNKNOWN,				// 93 Application
			G_KEY_UNKNOWN,				// 94
			G_KEY_UNKNOWN,				// 95 Sleep
			G_KEY_NUMPAD_0,				// 96
			G_KEY_NUMPAD_1,				// 97
			G_KEY_NUMPAD_2,				// 98
			G_KEY_NUMPAD_3,				// 99
			G_KEY_NUMPAD_4,				// 100
			G_KEY_NUMPAD_5,				// 101
			G_KEY_NUMPAD_6,				// 102
			G_KEY_NUMPAD_7,				// 103
			G_KEY_NUMPAD_8,				// 104
			G_KEY_NUMPAD_9,				// 105
			G_KEY_NUMPAD_MULTIPLY,		// 106
			G_KEY_NUMPAD_ADD,			// 107
			G_KEY_UNKNOWN,				// 108 Separator
			G_KEY_NUMPAD_SUBTRACT,		// 109
			G_KEY_NUMPAD_PERIOD,		// 110
			G_KEY_NUMPAD_DIVIDE,		// 111
			G_KEY_F1,					// 112
			G_KEY_F2,					// 113
			G_KEY_F3,					// 114
			G_KEY_F4,					// 115
			G_KEY_F5,					// 116
			G_KEY_F6,					// 117
			G_KEY_F7,					// 118
			G_KEY_F8,					// 119
			G_KEY_F9,					// 120
			G_KEY_F10,					// 121
			G_KEY_F11,					// 122
			G_KEY_F12,					// 123
			G_KEY_UNKNOWN,				// 124 F13
			G_KEY_UNKNOWN,				// 125 F14
			G_KEY_UNKNOWN,				// 126 F15
			G_KEY_UNKNOWN,				// 127 F16
			G_KEY_UNKNOWN,				// 128 F17
			G_KEY_UNKNOWN,				// 129 F18
			G_KEY_UNKNOWN,				// 130 F19
			G_KEY_UNKNOWN,				// 131 F20
			G_KEY_UNKNOWN,				// 132 F21
			G_KEY_UNKNOWN,				// 133 F22
			G_KEY_UNKNOWN,				// 134 F23
			G_KEY_UNKNOWN,				// 135 F24
			G_KEY_UNKNOWN,				// 136 NavigationView
			G_KEY_UNKNOWN,				// 137 NavigationMenu
			G_KEY_UNKNOWN,				// 138 NavigationUp
			G_KEY_UNKNOWN,				// 139 NavigationDown
			G_KEY_UNKNOWN,				// 140 NavigationLeft
			G_KEY_UNKNOWN,				// 141 NavigationRight 
			G_KEY_UNKNOWN,				// 142 NavigationAccept
			G_KEY_UNKNOWN,				// 143 NavigationCancel
			G_KEY_NUMLOCK,				// 144
			G_KEY_SCROLL_LOCK,			// 145 Scroll
			G_KEY_UNKNOWN,				// 146
			G_KEY_UNKNOWN,				// 147
			G_KEY_UNKNOWN,				// 148
			G_KEY_UNKNOWN,				// 149
			G_KEY_UNKNOWN,				// 150
			G_KEY_UNKNOWN,				// 151
			G_KEY_UNKNOWN,				// 152
			G_KEY_UNKNOWN,				// 153
			G_KEY_UNKNOWN,				// 154
			G_KEY_UNKNOWN,				// 155
			G_KEY_UNKNOWN,				// 156
			G_KEY_UNKNOWN,				// 157
			G_KEY_UNKNOWN,				// 158
			G_KEY_UNKNOWN,				// 159
			G_KEY_LEFTSHIFT,			// 160
			G_KEY_RIGHTSHIFT,			// 161
			G_KEY_LEFTCONTROL,			// 162
			G_KEY_RIGHTCONTROL,			// 163
			G_KEY_UNKNOWN,				// 164
			G_KEY_UNKNOWN,				// 165
			G_KEY_UNKNOWN,				// 166
			G_KEY_UNKNOWN,				// 167
			G_KEY_UNKNOWN,				// 168
			G_KEY_UNKNOWN,				// 169
			G_KEY_UNKNOWN,				// 170
			G_KEY_UNKNOWN,				// 171
			G_KEY_UNKNOWN,				// 172
			G_KEY_UNKNOWN,				// 173
			G_KEY_UNKNOWN,				// 174
			G_KEY_UNKNOWN,				// 175
			G_KEY_UNKNOWN,				// 176
			G_KEY_UNKNOWN,				// 177
			G_KEY_UNKNOWN,				// 178
			G_KEY_UNKNOWN,				// 179
			G_KEY_UNKNOWN,				// 180
			G_KEY_UNKNOWN,				// 181
			G_KEY_UNKNOWN,				// 182
			G_KEY_UNKNOWN,				// 183
			G_KEY_UNKNOWN,				// 184
			G_KEY_UNKNOWN,				// 185
			G_KEY_SEMICOLON,			// 186
			G_KEY_EQUALS,				// 187
			G_KEY_COMMA,				// 188
			G_KEY_MINUS,				// 189
			G_KEY_PERIOD,				// 190
			G_KEY_FORWARDSLASH,			// 191
			G_KEY_TILDE,				// 192
			G_KEY_UNKNOWN,				// 193
			G_KEY_UNKNOWN,				// 194
			G_KEY_UNKNOWN,				// 195
			G_KEY_UNKNOWN,				// 196
			G_KEY_UNKNOWN,				// 197
			G_KEY_UNKNOWN,				// 198
			G_KEY_UNKNOWN,				// 199
			G_KEY_UNKNOWN,				// 200
			G_KEY_UNKNOWN,				// 201
			G_KEY_UNKNOWN,				// 202
			G_KEY_UNKNOWN,				// 203
			G_KEY_UNKNOWN,				// 204
			G_KEY_UNKNOWN,				// 205
			G_KEY_UNKNOWN,				// 206
			G_KEY_UNKNOWN,				// 207
			G_KEY_UNKNOWN,				// 208
			G_KEY_UNKNOWN,				// 209
			G_KEY_UNKNOWN,				// 210
			G_KEY_UNKNOWN,				// 211
			G_KEY_UNKNOWN,				// 212
			G_KEY_UNKNOWN,				// 213
			G_KEY_UNKNOWN,				// 214
			G_KEY_UNKNOWN,				// 215
			G_KEY_UNKNOWN,				// 216
			G_KEY_UNKNOWN,				// 217
			G_KEY_UNKNOWN,				// 218
			G_KEY_BRACKET_OPEN,			// 219
			G_KEY_BACKSLASH,			// 220
			G_KEY_BRACKET_CLOSE,		// 221
			G_KEY_QUOTE,				// 222
			G_KEY_UNKNOWN,				// 223
			G_KEY_UNKNOWN,				// 224
			G_KEY_UNKNOWN,				// 225
			G_KEY_BACKSLASH,			// 226


		};
#endif
#elif defined(__linux__)

#define G_MAX_KEYCODES 128

		constexpr unsigned int Keycodes[G_MAX_KEYCODES] =
		{
			G_KEY_UNKNOWN,			// 0
			G_KEY_UNKNOWN,			// 1
			G_KEY_UNKNOWN,			// 2
			G_KEY_UNKNOWN,			// 3
			G_KEY_UNKNOWN,			// 4
			G_KEY_UNKNOWN,			// 5
			G_KEY_UNKNOWN,			// 6
			G_KEY_UNKNOWN,			// 7
			G_KEY_UNKNOWN,			// 8
			G_KEY_ESCAPE,			// 9
			G_KEY_1,				// 10
			G_KEY_2,				// 11
			G_KEY_3,				// 12
			G_KEY_4,				// 13
			G_KEY_5,				// 14
			G_KEY_6,				// 15
			G_KEY_7,				// 16
			G_KEY_8,				// 17
			G_KEY_9,				// 18
			G_KEY_0,				// 19
			G_KEY_MINUS,			// 20
			G_KEY_EQUALS,			// 21
			G_KEY_BACKSPACE,		// 22
			G_KEY_TAB,				// 23
			G_KEY_Q,				// 24
			G_KEY_W,				// 25
			G_KEY_E,				// 26
			G_KEY_R,				// 27
			G_KEY_T,				// 28
			G_KEY_Y,				// 29
			G_KEY_U,				// 30
			G_KEY_I,				// 31
			G_KEY_O,				// 32
			G_KEY_P,				// 33
			G_KEY_BRACKET_OPEN,		// 34
			G_KEY_BRACKET_CLOSE,	// 35
			G_KEY_ENTER,			// 36
			G_KEY_LEFTCONTROL,		// 37
			G_KEY_A,				// 38
			G_KEY_S,				// 39
			G_KEY_D,				// 40
			G_KEY_F,				// 41
			G_KEY_G,				// 42
			G_KEY_H,				// 43
			G_KEY_J,				// 44
			G_KEY_K,				// 45
			G_KEY_L,				// 46
			G_KEY_SEMICOLON,		// 47
			G_KEY_QUOTE,			// 48
			G_KEY_TILDE,			// 49
			G_KEY_LEFTSHIFT,		// 50
			G_KEY_BACKSLASH,		// 51
			G_KEY_Z,				// 52
			G_KEY_X,				// 53
			G_KEY_C,				// 54
			G_KEY_V,				// 55
			G_KEY_B,				// 56
			G_KEY_N,				// 57
			G_KEY_M,				// 58
			G_KEY_COMMA,			// 59
			G_KEY_PERIOD,			// 60
			G_KEY_FORWARDSLASH,		// 61
			G_KEY_RIGHTSHIFT,		// 62
			G_KEY_NUMPAD_MULTIPLY,	// 63
			G_KEY_LEFTALT,			// 64
			G_KEY_SPACE,			// 65
			G_KEY_CAPSLOCK,			// 66
			G_KEY_F1,				// 67
			G_KEY_F2,				// 68
			G_KEY_F3,				// 69
			G_KEY_F4,				// 70
			G_KEY_F5,				// 71
			G_KEY_F6,				// 72
			G_KEY_F7,				// 73
			G_KEY_F8,				// 74
			G_KEY_F9,				// 75
			G_KEY_F10,				// 76
			G_KEY_NUMLOCK,			// 77
			G_KEY_SCROLL_LOCK,		// 78
			G_KEY_NUMPAD_7,			// 79
			G_KEY_NUMPAD_8,			// 80
			G_KEY_NUMPAD_9,			// 81
			G_KEY_NUMPAD_SUBTRACT,	// 82
			G_KEY_NUMPAD_4,			// 83
			G_KEY_NUMPAD_5,			// 84
			G_KEY_NUMPAD_6,			// 85
			G_KEY_NUMPAD_ADD,		// 86
			G_KEY_NUMPAD_1,			// 87
			G_KEY_NUMPAD_2,			// 88
			G_KEY_NUMPAD_3,			// 89
			G_KEY_NUMPAD_0,			// 90
			G_KEY_NUMPAD_PERIOD,	// 91
			G_KEY_UNKNOWN,			// 92
			G_KEY_UNKNOWN,			// 93
			G_KEY_UNKNOWN,			// 94
			G_KEY_F11,				// 95
			G_KEY_F12,				// 96
			G_KEY_UNKNOWN,			// 97
			G_KEY_UNKNOWN,			// 98
			G_KEY_UNKNOWN,			// 99
			G_KEY_UNKNOWN,			// 100
			G_KEY_UNKNOWN,			// 101
			G_KEY_UNKNOWN,			// 102
			G_KEY_UNKNOWN,			// 103
			G_KEY_NUMPAD_ENTER,		// 104
			G_KEY_UNKNOWN,			// 105
			G_KEY_NUMPAD_DIVIDE,	// 106
			G_KEY_PRINTSCREEN,		// 107
			G_KEY_RIGHTALT,			// 108
			G_KEY_UNKNOWN,			// 109
			G_KEY_HOME,				// 110
			G_KEY_UP,				// 111
			G_KEY_PAGEUP,			// 112
			G_KEY_LEFT,				// 113
			G_KEY_RIGHT,			// 114
			G_KEY_END,				// 115
			G_KEY_DOWN,				// 116
			G_KEY_PAGEDOWN,			// 117
			G_KEY_INSERT,			// 118
			G_KEY_DELETE,			// 119
			G_KEY_UNKNOWN,			// 120
			G_KEY_UNKNOWN,			// 121
			G_KEY_UNKNOWN,			// 122
			G_KEY_UNKNOWN,			// 123
			G_KEY_UNKNOWN,			// 124
			G_KEY_UNKNOWN,			// 125
			G_KEY_UNKNOWN,			// 126
			G_KEY_PAUSE				// 127
			
		};
#elif defined(__APPLE__)

#define G_MAX_KEYCODES 128

		constexpr unsigned int Keycodes[G_MAX_KEYCODES] =
		{
			G_KEY_A,				// 0
			G_KEY_S,				// 1
			G_KEY_D,				// 2
			G_KEY_F,				// 3
			G_KEY_H,				// 4
			G_KEY_G,				// 5
			G_KEY_Z,				// 6
			G_KEY_X,				// 7
			G_KEY_C,				// 8
			G_KEY_V,				// 9
			G_KEY_UNKNOWN,			// 10
			G_KEY_B,				// 11
			G_KEY_Q,				// 12
			G_KEY_W,				// 13
			G_KEY_E,				// 14
			G_KEY_R,				// 15
			G_KEY_Y,				// 16
			G_KEY_T,				// 17
			G_KEY_1,				// 18
			G_KEY_2,				// 19
			G_KEY_3,				// 20
			G_KEY_4,				// 21
			G_KEY_6,				// 22
			G_KEY_5,				// 23
			G_KEY_EQUALS,			// 24
			G_KEY_9,				// 25
			G_KEY_7,				// 26
			G_KEY_MINUS,			// 27
			G_KEY_8,				// 28
			G_KEY_0,				// 29
			G_KEY_BRACKET_CLOSE,	// 30
			G_KEY_O,				// 31
			G_KEY_U,				// 32
			G_KEY_BRACKET_OPEN,		// 33
			G_KEY_I,				// 34
			G_KEY_P,				// 35
			G_KEY_ENTER,			// 36
			G_KEY_L,				// 37
			G_KEY_J,				// 38
			G_KEY_QUOTE,			// 39
			G_KEY_K,				// 40
			G_KEY_SEMICOLON,		// 41
			G_KEY_BACKSLASH,		// 42
			G_KEY_COMMA,			// 43
			G_KEY_FORWARDSLASH,		// 44
			G_KEY_N,				// 45
			G_KEY_M,				// 46
			G_KEY_PERIOD,			// 47
			G_KEY_TAB,				// 48
			G_KEY_SPACE,			// 49
			G_KEY_TILDE,			// 50
			G_KEY_DELETE,			// 51
			G_KEY_UNKNOWN,			// 52
			G_KEY_ESCAPE,			// 53
			G_KEY_UNKNOWN,			// 54
			G_KEY_COMMAND,		    // 55
			G_KEY_LEFTSHIFT,		// 56
			G_KEY_CAPSLOCK,			// 57
			G_KEY_LEFTALT,			// 58
			G_KEY_LEFTCONTROL,		// 59
			G_KEY_RIGHTSHIFT,		// 60
			G_KEY_RIGHTALT,			// 61
			G_KEY_RIGHTCONTROL,		// 62
			G_KEY_UNKNOWN,			// 63
			G_KEY_UNKNOWN,			// 64
			G_KEY_NUMPAD_PERIOD,	// 65
			G_KEY_UNKNOWN,			// 66
			G_KEY_NUMPAD_MULTIPLY,	// 67
			G_KEY_UNKNOWN,			// 68
			G_KEY_NUMPAD_ADD,		// 69
			G_KEY_UNKNOWN,			// 70
			G_KEY_NUMLOCK,			// 71
			G_KEY_UNKNOWN,			// 72
			G_KEY_UNKNOWN,			// 73
			G_KEY_UNKNOWN,			// 74
			G_KEY_NUMPAD_DIVIDE,	// 75
			G_KEY_NUMPAD_ENTER,		// 76
			G_KEY_UNKNOWN,			// 77
			G_KEY_NUMPAD_SUBTRACT,	// 78
			G_KEY_UNKNOWN,			// 79
			G_KEY_UNKNOWN,			// 80
			G_KEY_EQUALS,			// 81
			G_KEY_NUMPAD_0,			// 82
			G_KEY_NUMPAD_1,			// 83
			G_KEY_NUMPAD_2,			// 84
			G_KEY_NUMPAD_3,			// 85
			G_KEY_NUMPAD_4,			// 86
			G_KEY_NUMPAD_5,			// 87
			G_KEY_NUMPAD_6,			// 88
			G_KEY_NUMPAD_7,			// 89
			G_KEY_UNKNOWN,			// 90
			G_KEY_NUMPAD_8,			// 91
			G_KEY_NUMPAD_9,			// 92
			G_KEY_UNKNOWN,			// 93
			G_KEY_UNKNOWN,			// 94
			G_KEY_UNKNOWN,			// 95
			G_KEY_F5,				// 96
			G_KEY_F6,				// 97
			G_KEY_F7,				// 98
			G_KEY_F3,				// 99
			G_KEY_F8,				// 100
			G_KEY_F9,				// 101
			G_KEY_UNKNOWN,			// 102
			G_KEY_F11,				// 103
			G_KEY_UNKNOWN,			// 104
			G_KEY_UNKNOWN,			// 105
			G_KEY_UNKNOWN,			// 106
			G_KEY_UNKNOWN,			// 107
			G_KEY_UNKNOWN,			// 108
			G_KEY_F10,				// 109
			G_KEY_UNKNOWN,			// 110
			G_KEY_F12,				// 111
			G_KEY_UNKNOWN,			// 112
			G_KEY_UNKNOWN,			// 113
			G_KEY_INSERT,			// 114
			G_KEY_HOME,				// 115
			G_KEY_PAGEUP,			// 116
			G_KEY_DELETE,			// 117
			G_KEY_F4,				// 118
			G_KEY_END,				// 119
			G_KEY_F2,				// 120
			G_KEY_PAGEDOWN,			// 121
			G_KEY_F1,				// 122
			G_KEY_LEFT,				// 123
			G_KEY_RIGHT,			// 124
			G_KEY_DOWN,				// 125
			G_KEY_UP,				// 126
            G_KEY_UNKNOWN           // 127
		};
#endif
	}
}
#endif
