#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <X11/keysym.h>
#include "../../../Interface/System/GDaemon.h"
#include "../../../Interface/Core/GEventReceiver.h"

namespace GW
{
	namespace I
	{
		class GInputImplementation : public virtual GInputInterface,
			protected GInterfaceImplementation
		{
		private:
			int _mousePrevX = 0;
			int _mousePrevY = 0;
			int _mousePositionX = 0;
			int _mousePositionY = 0;
			int _mouseDeltaX = 0;
			int _mouseDeltaY = 0;
			unsigned int keyMask = 0;
			unsigned int mouseReadCount = 0;
			unsigned int mouseWriteCount = 0;

			// Daemon data
			int _code = -1;
			Window root, child;
			int _mouseScreenPositionX = 0, _mouseScreenPositionY = 0;
			char keys_return[32];

			// Window data
			Display* _display = nullptr;
			Window _window;

			unsigned int keyStates[256] = { 0 };
			GW::SYSTEM::GDaemon inputDaemon;
			GW::CORE::GEventReceiver watcher;
		public:
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle)
			{
				if (_windowHandle.window == nullptr || _windowHandle.display == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Copy data from UNIVERSAL_WINDOW_HANDLE(void * display, void * window) structure.
				_window = *((Window*)(_windowHandle.window));
				_display = ((Display*)(_windowHandle.display));

				//Select the type of Input events we wish to receive.
				///XSelectInput(_display, _window, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyReleaseMask | KeyPressMask | LockMask | ControlMask | ShiftMask);

				XInitThreads();// daemon run on another thread
				// Creates a daemon that parses all the input on the other thread
				inputDaemon.Create(G_INPUT_OPERATION_INTERVAL, [&]()
					{
						XQueryKeymap(_display, keys_return);

						static int stateOfKeyOld = 0;

						for (unsigned int i = 0; i < 128; ++i)
						{
							_code = Keycodes[i];

							int index = i >> 3;
							int bitshift = 1 << (i & 7);

							// to debug, add a conditional break point where b != 0, then press any key
							// whatever I is equal to, is the bit offset of the key you pressed.
							// ex: num pad 0 is at i = 90
							auto b = keys_return[index] & (bitshift) ? 1 : 0;

							//Set keyboard input
							keyStates[_code] = b;
						}

						// not currently supported. would be returned by XQueryKeymap if it was.
						// auto keycode = XKeysymToKeycode( _display, XK_Control_R );
						// keyStates[G_KEY_RIGHTCONTROL] = keys_return[keycode >> 3] & (1 << (keycode & 7)) ? 1 : 0;


						XQueryPointer(_display, _window, &root, &child, &_mouseScreenPositionX, &_mouseScreenPositionY, &_mousePositionX, &_mousePositionY, &keyMask);

						//Set the mouse input
						keyStates[G_BUTTON_LEFT] = (keyMask & Button1Mask) ? 1 : 0;
						keyStates[G_BUTTON_RIGHT] = (keyMask & Button3Mask) ? 1 : 0;
						keyStates[G_BUTTON_MIDDLE] = (keyMask & Button2Mask) ? 1 : 0;

						//Set the change in mouse position.
						_mouseDeltaX = _mousePositionX - _mousePrevX;
						_mouseDeltaY = _mousePositionY - _mousePrevY;

						//Set the previous mouse position as the current.
						_mousePrevX = _mousePositionX;
						_mousePrevY = _mousePositionY;

						//Insure updates are tracked correctly
						if (_mouseDeltaX || _mouseDeltaY)
							++mouseWriteCount;
					});

				return GReturn::SUCCESS;
			}

			GReturn Create(const GW::SYSTEM::GWindow _gWindow)
			{
				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwHndl;

				if (-_gWindow.GetWindowHandle(uwHndl))
					return GW::GReturn::FAILURE;

				// stop if window is killed
				watcher.Create(_gWindow, [&]() {
					if (+watcher.Find(GW::SYSTEM::GWindow::Events::DESTROY, true))
						inputDaemon = nullptr;
					});

				return Create(uwHndl);
			}

			GReturn GetState(int _keyCode, float& _outState) override
			{
				if (_keyCode == G_MOUSE_SCROLL_DOWN || _keyCode == G_MOUSE_SCROLL_UP)
					return GW::GReturn::FEATURE_UNSUPPORTED;
				_outState = (float)keyStates[_keyCode];
				return GW::GReturn::SUCCESS;
			}

			GReturn GetMouseDelta(float& _x, float& _y) override
			{
				_x = static_cast<float>(_mouseDeltaX);
				_y = static_cast<float>(_mouseDeltaY);
				if (mouseReadCount != mouseWriteCount)
				{
					mouseReadCount = mouseWriteCount;
					return GW::GReturn::SUCCESS;
				}
				return GW::GReturn::REDUNDANT;
			}

			GReturn GetMousePosition(float& _x, float& _y) const override
			{
				_x = static_cast<float>(_mousePositionX);
				_y = static_cast<float>(_mousePositionY);

				return GW::GReturn::SUCCESS;
			}

			GReturn GetKeyMask(unsigned int& _outKeyMask) const override
			{
				_outKeyMask = keyMask;
				return GW::GReturn::SUCCESS;
			}
		};
	}
}
