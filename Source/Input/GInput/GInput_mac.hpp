#ifdef __OBJC__
@import Foundation;
@import Cocoa;
#endif

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define TURNON_BIT(var,pos) ((var) |=  (1<<(pos)))
#define TURNOFF_BIT(var,pos) ((var) &= ~(1<<(pos)))
#define TOGGLE_BIT(var, pos) (CHECK_BIT(var, pos) ? (TURNOFF_BIT(var,pos)) : (TURNON_BIT(var,pos)))

#include <chrono>
#include "../../../Interface/System/GDaemon.h"
#include "../../Shared/macutils.h"

namespace internal_gw
{
    struct GINPUT_GLOBAL
    {
        GW::SYSTEM::GDaemon myDaemon;
        int mousePositionX;
        int mousePositionY;
        int mouseDeltaX;
        int mouseDeltaY;
        int upPings;
        int downPings;
        unsigned int keyMask;
        unsigned int mouseReadCount;
        unsigned int mouseWriteCount;

        unsigned int n_Keys[256];
    };

    static GINPUT_GLOBAL& GInputGlobal()
    {
        static GINPUT_GLOBAL keyData;
        return keyData;
    }
    // GIResponder Interface
    // The GIResponder is our interpretation of the NSReponder to recieve events.

    // Data members of GResponder
    G_OBJC_DATA_MEMBERS_STRUCT(GIResponder)
    {
        NSWindow* targetWindow; // so we can get the height of the client area
    };
    G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(GIResponder);
    // Forward declarations of GResponder methods
    // Overide NSResponder functions
    G_OBJC_HEADER_INSTANCE_METHOD(GIResponder, bool, acceptFirstResponder);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, bool, acceptsFirstMouse, NSEvent* event);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, flagsChanged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, keyDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, keyUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, scrollWheel, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseMoved, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, GetKeyMask, NSEvent* theEvent);

    // Creates the GIResponder class at runtime when G_OBJC_GET_CLASS(GIResponder) is called
    G_OBJC_CLASS_BEGIN(GIResponder, NSResponder)
    {
        G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(GIResponder);
        G_OBJC_CLASS_METHOD(GIResponder, acceptFirstResponder, "B@:");
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, acceptsFirstMouse, "B@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, flagsChanged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, keyDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, keyUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, mouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, mouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, scrollWheel, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, rightMouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, rightMouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, otherMouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, otherMouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, mouseMoved, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, mouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, rightMouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, otherMouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GIResponder, GetKeyMask, "v@:@", :);
    }
    G_OBJC_CLASS_END(GIResponder)
    
    // GIResponder Interface End
}

namespace GW
{
    namespace I
    {
        class GInputImplementation : public virtual GInputInterface,
            private GThreadSharedImplementation
        {

        private:
            NSWindow* currentResponder;
            id responder;

            static void RUN_ON_UI_THREAD(dispatch_block_t block)
            {
                if ([NSThread isMainThread])
                    block();
                else
                    dispatch_sync(dispatch_get_main_queue(), block);
            }

        public:
            ~GInputImplementation()
            {
                [currentResponder setNextResponder : nil] ;
                [responder release] ;
            }

            GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle)
            {
                responder = [internal_gw::G_OBJC_GET_CLASS(GIResponder) alloc];

                if (_windowHandle.window == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                //Need to convert data back into an NSWindow*.
                //NSWindow * currentResponder = ((__bridge NSWindow*)_data);
                currentResponder = ((__bridge NSWindow*)_windowHandle.window);
                // provide handle to current window
                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GIResponder)& responderDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GIResponder, responder);
                responderDataMembers.targetWindow = currentResponder;

                //We only want to process the message and pass it on. So if there is already
                //a responder we set our responders next responder to be the current next responder.
                [responder setNextResponder : currentResponder.nextResponder];

                //We then set out responder to the next responder of the window.
                [currentResponder setNextResponder : responder] ;

                //We also need to make our responder the first responder of the window.
                [currentResponder makeFirstResponder : responder] ;

                RUN_ON_UI_THREAD(^ {
                    //In order to get mouse button presses we need to set our responder to be
                    //the next responder in the contentView as well.
                    [currentResponder.contentView setNextResponder : responder] ;
                    });
                //Enable responder to accept mouse move events. By default it doesn't.
                [currentResponder setAcceptsMouseMovedEvents:YES];

                return GReturn::SUCCESS;
            }

            GReturn Create(GW::SYSTEM::GWindow _gWindow)
            {
                GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwHndl;

                if (-_gWindow.GetWindowHandle(uwHndl))
                    return GW::GReturn::FAILURE;

                return Create(uwHndl);
            }

            GReturn GetState(int _keyCode, float& _outState) override
            {
                _outState = static_cast<float>(internal_gw::GInputGlobal().n_Keys[_keyCode]);
                return GW::GReturn::SUCCESS;
            }

            GReturn GetMouseDelta(float& _x, float& _y) override
            {
                _x = static_cast<float>(internal_gw::GInputGlobal().mouseDeltaX);
                _y = static_cast<float>(internal_gw::GInputGlobal().mouseDeltaY);
                if (internal_gw::GInputGlobal().mouseReadCount != internal_gw::GInputGlobal().mouseWriteCount)
                {
                    internal_gw::GInputGlobal().mouseReadCount = internal_gw::GInputGlobal().mouseWriteCount;
                    return GW::GReturn::SUCCESS;
                }
                return GW::GReturn::REDUNDANT;
            }

            GReturn GetMousePosition(float& _x, float& _y) const override
            {
                _x = static_cast<float>(internal_gw::GInputGlobal().mousePositionX);
                _y = static_cast<float>(internal_gw::GInputGlobal().mousePositionY);

                return GW::GReturn::SUCCESS;
            }

            GReturn GetKeyMask(unsigned int& _outKeyMask) const override
            {
                _outKeyMask = internal_gw::GInputGlobal().keyMask;
                return GW::GReturn::SUCCESS;
            }
        };

    }
}

namespace internal_gw
{
    // GIResponder Implementation
    G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(GIResponder);

    G_OBJC_HEADER_INSTANCE_METHOD(GIResponder, bool, acceptFirstResponder)
    {
        return YES;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, bool, acceptsFirstMouse, NSEvent* event)
    {
        return YES;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, flagsChanged, NSEvent* theEvent)
    {
        // these keys are treated a bit differently and need a custom handler
        NSUInteger code = [theEvent keyCode];
        NSUInteger gwcode = GW::I::Keycodes[code];
        NSUInteger xflags = [theEvent modifierFlags];
       
        if (gwcode == G_KEY_LEFTSHIFT || gwcode == G_KEY_RIGHTSHIFT)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagShift) ? 1 : 0;
        if (gwcode == G_KEY_LEFTALT || gwcode == G_KEY_RIGHTALT)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagOption) ? 1 : 0;
        if (gwcode == G_KEY_LEFTCONTROL || gwcode == G_KEY_RIGHTCONTROL)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagControl) ? 1 : 0;
        if (gwcode == G_KEY_NUMLOCK)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagNumericPad) ? 1 : 0;
        if (gwcode == G_KEY_COMMAND)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagCommand) ? 1 : 0;
        if (gwcode == G_KEY_FUNCTION)
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagFunction) ? 1 : 0;
        if (gwcode == G_KEY_CAPSLOCK) // will continue to trigger until pressed again
            GInputGlobal().n_Keys[gwcode] =
                (xflags & NSEventModifierFlagCapsLock) ? 1 : 0;
        
        //Get the keymask.
        // NOTE: if above method becomes problematic, this could also be done
        // using a GDaemon that tracks change in the NSModifierFlag
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);

        if ([self nextResponder] != nil)
            [[self nextResponder]keyDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, keyDown, NSEvent* theEvent)
    {
        //Set the key to pressed after getting it from the static table of keys.
        NSUInteger code = [theEvent keyCode];
        GInputGlobal().n_Keys[GW::I::Keycodes [code] ] = 1;
        //Get the keymask.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);

        if ([self nextResponder] != nil)
            [[self nextResponder]keyDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, keyUp, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[GW::I::Keycodes [[theEvent keyCode]] ] = 0;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);

        if ([self nextResponder] != nil)
            [[self nextResponder]keyUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseDown, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_LEFT] = 1;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]mouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseUp, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_LEFT] = 0;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]mouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, scrollWheel, NSEvent* theEvent)
    {
        ////Scrolling Up
        if (theEvent.deltaY > 0)
        {
            GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] = 1;
            GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
        }
        ////Scrolling down
        if (theEvent.deltaY < 0)
        {
            GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] = 0;
            GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] = 1;
        }

        std::chrono::time_point<std::chrono::steady_clock> start = std::chrono::steady_clock::now();

        if (GInputGlobal().myDaemon == nullptr)
        {
            GInputGlobal().myDaemon.Create(6, [&]()
            {
                auto end = std::chrono::steady_clock::now();

                if (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() >= 50000)
                {
                    GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] = 0;
                    GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
                    return;
                }
            });
        }

        if ([self nextResponder] != nil)
            [[self nextResponder]scrollWheel:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseDown, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_RIGHT] = 1;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]rightMouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseUp, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_RIGHT] = 0;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]rightMouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseDown, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_MIDDLE] = 1;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]otherMouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseUp, NSEvent* theEvent)
    {
        GInputGlobal().n_Keys[G_BUTTON_MIDDLE] = 0;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, GetKeyMask, theEvent);
        if ([self nextResponder] != nil)
            [[self nextResponder]otherMouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseMoved, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GIResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GIResponder, self);
        NSRect rect = selfDataMembers.targetWindow.frame;
        NSRect contentRect = [selfDataMembers.targetWindow contentRectForFrameRect : rect];
        NSPoint mousePosition = [theEvent locationInWindow];
        mousePosition.y = contentRect.size.height - mousePosition.y;
        
        ++GInputGlobal().mouseWriteCount;
        GInputGlobal().mouseDeltaX = mousePosition.x - GInputGlobal().mousePositionX;
        GInputGlobal().mouseDeltaY = mousePosition.y - GInputGlobal().mousePositionY;
        GInputGlobal().mousePositionX = mousePosition.x;
        GInputGlobal().mousePositionY = mousePosition.y;
    
        if ([self nextResponder] != nil)
            [[self nextResponder]mouseMoved:theEvent];
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, mouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, mouseMoved, theEvent);
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, rightMouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, mouseMoved, theEvent);
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, otherMouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GIResponder, self, mouseMoved, theEvent);
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GIResponder, void, GetKeyMask, NSEvent* theEvent)
    {
        GInputGlobal().keyMask = 0;
        //Get the unsigned int of all the modifier flags.
        NSUInteger xflags = [theEvent modifierFlags];
        //Check individual modifier flags and turn them on respectivly to our keymask(unsigned int).
        if (xflags & NSEventModifierFlagShift)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_SHIFT);
        }
        if (xflags & NSEventModifierFlagOption)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_ALT);
        }
        if (xflags & NSEventModifierFlagCommand)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_COMMAND);
        }
        if (xflags & NSEventModifierFlagControl)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_CONTROL);
        }
        if (xflags & NSEventModifierFlagCapsLock)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_CAPS_LOCK);
        }
        if (xflags & NSEventModifierFlagFunction)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_FUNCTION);
        }
        if (xflags & NSEventModifierFlagNumericPad)
        {
            TURNON_BIT(GInputGlobal().keyMask, G_MASK_NUM_LOCK);
        }
    }

    // GIResponder Implementation End
}

#undef CHECK_BIT
#undef TURNON_BIT
#undef TURNOFF_BIT
#undef TOGGLE_BIT
