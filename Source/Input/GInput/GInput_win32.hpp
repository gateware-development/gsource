#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <vector>
#include <map>

namespace internal_gw {
	struct GINPUT_VALUES {
		int mousePrevX;
		int mousePrevY;
		int mousePositionX;
		int mousePositionY;
		int mouseDeltaX;
		int mouseDeltaY;
		unsigned int keyMask;

		unsigned int mouseReadCount;
		unsigned int mouseWriteCount;
		unsigned int scrollUpWriteCount;
		unsigned int scrollUpReadCount;
		unsigned int scrollDownWriteCount;
		unsigned int scrollDownReadCount;
		LONG_PTR _userWinProc;

		unsigned int n_Keys[256];
		unsigned int count = 0;
	};

	inline std::map<HWND, GINPUT_VALUES>& GetWindowInputValues() {
		static std::map<HWND, GINPUT_VALUES> hWndMap;
		return hWndMap;
	}

}

namespace GW {
	namespace I {
		class GInputImplementation : public virtual GInputInterface,
			private GInterfaceImplementation {
		private:
			HWND hWnd = nullptr;
			internal_gw::GINPUT_VALUES* inputStates;

			static bool SetGKeyIfSpecialCase(unsigned int& gKey, const USHORT& makeCode, const USHORT& flags, const LPARAM& lp) {
				if (flags & 2) {
					switch (makeCode) {
						default:
							return false;
						case 29:
							gKey = G_KEY_RIGHTCONTROL;
							return true;
						case 56:
							gKey = G_KEY_RIGHTALT;
							return true;
						case 28:
							gKey = G_KEY_NUMPAD_ENTER;
							return true;
						case 53:
							gKey = G_KEY_NUMPAD_DIVIDE;
							return true;
						case 82:
							gKey = G_KEY_INSERT;
							return true;
						case 83:
							gKey = G_KEY_DELETE;
							return true;
						case 71:
							gKey = G_KEY_HOME;
							return true;
						case 79:
							gKey = G_KEY_END;
							return true;
						case 73:
							gKey = G_KEY_PAGEUP;
							return true;
						case 81:
							gKey = G_KEY_PAGEDOWN;
							return true;
						case 75:
							gKey = G_KEY_LEFT;
							return true;
						case 77:
							gKey = G_KEY_RIGHT;
							return true;
						case 72:
							gKey = G_KEY_UP;
							return true;
						case 80:
							gKey = G_KEY_DOWN;
							return true;
						case 42:
							gKey = G_KEY_PRINTSCREEN;
							return true;
					}
				}
				return false;
			}

			// This functions checks to see if the key was "pressed" by windows and not the user
			// as is the case when num lock is off and the user presses any of the keys in the area 
			// to the left of the num pad, for example.
			static bool ShouldSkipBecauseBogusKeyPress(const USHORT& makeCode, const USHORT& flags, HWND window) {
				// makecode must be shift, numlock must be off, and the E0 bit must be set
				bool bogusShift = makeCode == 42 && 
					!G_CHECK_BIT(internal_gw::GetWindowInputValues()[window].keyMask, 
						G_MASK_NUM_LOCK) && (flags & 2);

				// makecode must be numpad multiply, and the E0 bit must be set
				bool bogusMultiply = makeCode == 55 && (flags & 2);

				// makecode must be control, and E1 bit must be set
				bool bogusControl = makeCode == 29 && (flags & 4);


				if (bogusShift || bogusMultiply || bogusControl) {
					return true;
				}
				return false;
			}
			
			static LRESULT CALLBACK GWinProc(HWND window, unsigned int msg, WPARAM wp, LPARAM lp) {
				switch (msg) {
					case WM_INPUT:
					{
						UINT dwSize = 0;

						//Get the size of RawInput
						GetRawInputData((HRAWINPUT)lp, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));

						LPBYTE lpb = new BYTE[dwSize];

						if (GetRawInputData((HRAWINPUT)lp, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize) {
							delete[] lpb;
							return LRESULT(false);
						}

						RAWINPUT* raw = (RAWINPUT*)lpb;

						unsigned int gKey = 0;

						if (raw->header.dwType == RIM_TYPEKEYBOARD) {
							auto& makeCode = raw->data.keyboard.MakeCode;
							gKey = Keycodes[makeCode];
							auto& message = raw->data.keyboard.Message;
							auto flags = raw->data.keyboard.Flags;
							

							//Set state released or pressed. SYSKEY is for keys like alt
							if (message == WM_KEYDOWN || message == WM_SYSKEYDOWN) {
								SetGKeyIfSpecialCase(gKey, makeCode, flags, lp);

								if (!(ShouldSkipBecauseBogusKeyPress(makeCode, flags, window))) {
									internal_gw::GetWindowInputValues()[window].n_Keys[gKey] = 1;
								}

								switch (gKey) {
									case G_KEY_RIGHTSHIFT:
									case G_KEY_LEFTSHIFT:
										G_TURNON_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_SHIFT);
										break;
									case G_KEY_RIGHTCONTROL:
									case G_KEY_LEFTCONTROL:
										G_TURNON_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_CONTROL);
										break;
									case G_KEY_CAPSLOCK:
										G_TOGGLE_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_CAPS_LOCK);
										break;
									case G_KEY_NUMLOCK:
										G_TOGGLE_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_NUM_LOCK);
										break;
									case G_KEY_SCROLL_LOCK:
										G_TOGGLE_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_SCROLL_LOCK);
										break;
								}
							}
							else if (message == WM_KEYUP || message == WM_SYSKEYUP) {
								SetGKeyIfSpecialCase(gKey, makeCode, flags, lp);
								internal_gw::GetWindowInputValues()[window].n_Keys[gKey] = 0;
								
								switch (gKey) {
									case G_KEY_RIGHTSHIFT:
									case G_KEY_LEFTSHIFT:
										G_TURNOFF_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_SHIFT);
										break;
									case G_KEY_RIGHTCONTROL:
									case G_KEY_LEFTCONTROL:
										G_TURNOFF_BIT(internal_gw::GetWindowInputValues()[window].keyMask, G_MASK_CONTROL);
										break;
								}
							}
							//printf("scanCode: %d\t flags: %d\t keymask: %d\n", makeCode, flags, internal_gw::GInputGlobal().keyMask);
						}
						else if (raw->header.dwType == RIM_TYPEMOUSE) {
							++internal_gw::GetWindowInputValues()[window].mouseWriteCount;

							switch (raw->data.mouse.ulButtons) {
								case 1:
								case 2:
									gKey = G_BUTTON_LEFT;
									break;
								case 4:
								case 8:
									gKey = G_BUTTON_RIGHT;
									break;
								case 16:
								case 32:
									gKey = G_BUTTON_MIDDLE;
									break;
							}

							switch (raw->data.mouse.usButtonData) {
								case 120:
								{
									gKey = G_MOUSE_SCROLL_UP;
									++internal_gw::GetWindowInputValues()[window].scrollUpWriteCount;
									break;
								}
								case 65416:
								{
									gKey = G_MOUSE_SCROLL_DOWN;
									++internal_gw::GetWindowInputValues()[window].scrollDownWriteCount;
									break;
								}
							}

							switch (raw->data.mouse.usButtonFlags) {
								//pressed
								case 1:
								case 4:
								case 16:
									internal_gw::GetWindowInputValues()[window].n_Keys[gKey] = 1;
									break;
									//Released
								case 2:
								case 8:
								case 32:
									internal_gw::GetWindowInputValues()[window].n_Keys[gKey] = 0;
									break;
								case 1024:
									internal_gw::GetWindowInputValues()[window].n_Keys[gKey] = 1;
									break;
							}

							//update delta mouse position
							if ((internal_gw::GetWindowInputValues()[window].mouseWriteCount - internal_gw::GetWindowInputValues()[window].mouseReadCount) <= 1) {
								internal_gw::GetWindowInputValues()[window].mouseDeltaX = raw->data.mouse.lLastX;
								internal_gw::GetWindowInputValues()[window].mouseDeltaY = raw->data.mouse.lLastY;
							}
							else {
								internal_gw::GetWindowInputValues()[window].mouseDeltaX += raw->data.mouse.lLastX;
								internal_gw::GetWindowInputValues()[window].mouseDeltaY += raw->data.mouse.lLastY;
							}

						}

						if (gKey != G_MOUSE_SCROLL_UP) {
							internal_gw::GetWindowInputValues()[window].n_Keys[G_MOUSE_SCROLL_UP] = 0;
							internal_gw::GetWindowInputValues()[window].scrollUpReadCount = internal_gw::GetWindowInputValues()[window].scrollUpWriteCount;
						}
						if (gKey != G_MOUSE_SCROLL_DOWN) {
							internal_gw::GetWindowInputValues()[window].n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
							internal_gw::GetWindowInputValues()[window].scrollDownReadCount = internal_gw::GetWindowInputValues()[window].scrollDownWriteCount;
						}

						POINT p;
						if (GetCursorPos(&p) && ScreenToClient(window, &p)) {
							internal_gw::GetWindowInputValues()[window].mousePositionX = static_cast<int>(p.x);
							internal_gw::GetWindowInputValues()[window].mousePositionY = static_cast<int>(p.y);

							internal_gw::GetWindowInputValues()[window].mousePrevX = internal_gw::GetWindowInputValues()[window].mousePositionX;
							internal_gw::GetWindowInputValues()[window].mousePrevY = internal_gw::GetWindowInputValues()[window].mousePositionY;
						}

						delete[] lpb;
						break;
					}
					default:
						break;
				}
				return CallWindowProcW((WNDPROC)internal_gw::GetWindowInputValues()[window]._userWinProc, window, msg, wp, lp);
			}

		public:
			~GInputImplementation() {
				if (hWnd) {
					--inputStates->count;
					//Sets the WinProc back. (Fixes the StackOverFlow bug)
					if (inputStates->count == 0) {
						SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)inputStates->_userWinProc);
						internal_gw::GetWindowInputValues().erase(hWnd);
					}
				}
			}

			// GWindow - one version
			// UNIVERSAL_WINDOW_HANDLE by value - another version
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle) {
				if (_windowHandle.window == nullptr)
					return GW::GReturn::INVALID_ARGUMENT;

				hWnd = static_cast<HWND>(_windowHandle.window);

				auto iter = internal_gw::GetWindowInputValues().find(hWnd);

				if (iter != internal_gw::GetWindowInputValues().end()) {
					inputStates = &internal_gw::GetWindowInputValues()[hWnd];
					++inputStates->count;

					return GW::GReturn::SUCCESS;
				}

				internal_gw::GetWindowInputValues()[hWnd] = internal_gw::GINPUT_VALUES();
				inputStates = &internal_gw::GetWindowInputValues()[hWnd];

				inputStates->count = 1;

				inputStates->_userWinProc = SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)GWinProc);

				if (inputStates->_userWinProc == NULL)
					return GW::GReturn::FAILURE;

				//Getting Raw Input Devices.
				UINT numDevices = 0;
				std::vector<RAWINPUTDEVICELIST> rawInputDeviceList;

				//Get Number of Devices.
				if (GetRawInputDeviceList(NULL, &numDevices, sizeof(RAWINPUTDEVICELIST)) != 0)
					return GW::GReturn::FAILURE;

				//Allocate the list of devices.
				rawInputDeviceList.resize(sizeof(RAWINPUTDEVICELIST) * numDevices);
				if (rawInputDeviceList.size() < 1)
					return GW::GReturn::FAILURE;

				int nNoOfDevices = 0;
				//Using the new List and number of devices.
				//Populate the raw input device list.
				if ((nNoOfDevices = GetRawInputDeviceList(rawInputDeviceList.data(), &numDevices, sizeof(RAWINPUTDEVICELIST))) == ((UINT)-1))
					return GW::GReturn::FAILURE;

				RID_DEVICE_INFO rdi;
				rdi.cbSize = sizeof(RID_DEVICE_INFO);

				//For all of the devices, display their correspondent information.
				for (int i = 0; i < nNoOfDevices; i++) {
					UINT size = 256;
					TCHAR tBuffer[256] = { 0 };
					tBuffer[0] = '\0';

					//Find the device name.
					if (!rawInputDeviceList.empty()) {
						if (GetRawInputDeviceInfo(rawInputDeviceList[i].hDevice, RIDI_DEVICENAME, tBuffer, &size) < 0)
							return GW::GReturn::FAILURE;

						UINT cbSize = rdi.cbSize;
						//Get the device information.
						if (GetRawInputDeviceInfo(rawInputDeviceList[i].hDevice, RIDI_DEVICEINFO, &rdi, &cbSize) < 0)
							return GW::GReturn::FAILURE;
					}
				}

				//Register the raw input devices.
				RAWINPUTDEVICE rID[2];

				//KeyBoard
				rID[0].usUsagePage = 0x01;
				rID[0].usUsage = 0x06;
				rID[0].dwFlags = RIDEV_EXINPUTSINK;
				rID[0].hwndTarget = hWnd;

				//Mouse
				rID[1].usUsagePage = 0x01;
				rID[1].usUsage = 0x02;
				rID[1].dwFlags = RIDEV_EXINPUTSINK;
				rID[1].hwndTarget = hWnd;

				if (RegisterRawInputDevices(rID, 2, sizeof(rID[0])) == false)
					return GW::GReturn::FAILURE;

				//Capslock
				if ((GetKeyState(VK_CAPITAL) & 0x0001) != 0)
					G_TURNON_BIT(inputStates->keyMask, G_MASK_CAPS_LOCK);

				//Numlock
				if ((GetKeyState(VK_NUMLOCK) & 0x0001) != 0)
					G_TURNON_BIT(inputStates->keyMask, G_MASK_NUM_LOCK);

				//ScrollLock
				if ((GetKeyState(VK_SCROLL) & 0x0001) != 0)
					G_TURNON_BIT(inputStates->keyMask, G_MASK_SCROLL_LOCK);


				return GW::GReturn::SUCCESS;
			}

			GReturn Create(const GW::SYSTEM::GWindow _gWindow) {
				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwHndl;

				if (-_gWindow.GetWindowHandle(uwHndl))
					return GW::GReturn::FAILURE;

				return Create(uwHndl);
			}

			GReturn GetState(int _keyCode, float& _outState) override {
				if (_keyCode == G_MOUSE_SCROLL_UP && inputStates->n_Keys[G_MOUSE_SCROLL_UP] == 1) {
					// Compare writes to the SCROLL_UP state by GWinProc to reads by the user.
					if (inputStates->scrollUpWriteCount != inputStates->scrollUpReadCount)
						++inputStates->scrollUpReadCount;
					else
						inputStates->n_Keys[G_MOUSE_SCROLL_UP] = 0; // Prevents over-reporting scroll up.
				}
				else if (_keyCode == G_MOUSE_SCROLL_DOWN && inputStates->n_Keys[G_MOUSE_SCROLL_DOWN] == 1) {
					if (inputStates->scrollDownWriteCount != inputStates->scrollDownReadCount)
						++inputStates->scrollDownReadCount;
					else
						inputStates->n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
				}

				_outState = (float)inputStates->n_Keys[_keyCode];
				return GW::GReturn::SUCCESS;
			}

			GReturn GetMouseDelta(float& _x, float& _y) override {
				_x = static_cast<float>(inputStates->mouseDeltaX);
				_y = static_cast<float>(inputStates->mouseDeltaY);
				if (inputStates->mouseReadCount != inputStates->mouseWriteCount) {
					inputStates->mouseReadCount = inputStates->mouseWriteCount;
					return GW::GReturn::SUCCESS;
				}
				return GW::GReturn::REDUNDANT;
			}

			GReturn GetMousePosition(float& _x, float& _y) const override {
				_x = static_cast<float>(inputStates->mousePositionX);
				_y = static_cast<float>(inputStates->mousePositionY);

				return GW::GReturn::SUCCESS;
			}

			GReturn GetKeyMask(unsigned int& _outKeyMask) const override {
				_outKeyMask = inputStates->keyMask;
				return GW::GReturn::SUCCESS;
			}
		};
	}
}

#undef G_MACRO_PAGEUP
#undef G_MACRO_PAGEDOWN
