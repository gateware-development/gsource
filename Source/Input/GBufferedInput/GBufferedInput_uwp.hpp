#include <Windows.h>
#include <vector>

#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.UI.Input.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.UI.ViewManagement.h>
#include "../GInput/GInputTableRouting.hpp"
#include "../../../Interface/Core/GEventReceiver.h"
#include <iostream>

namespace internal_gw {
	struct GBUFFEREDINPUT_GLOBAL {

		unsigned int keyMask;
		LONG_PTR _userWinProc;
		winrt::Windows::Foundation::Point lastMousePosition = { 0, 0 };
		winrt::Windows::Foundation::Point lastMousePositionRaw = { 0, 0 };
		UCHAR lastPressedState[256] = { 0, };

		GW::CORE::GEventGenerator keyEvents;
		GBUFFEREDINPUT_GLOBAL() {
			keyEvents.Create();
		}
	};
	inline GBUFFEREDINPUT_GLOBAL& GBufferedInputGlobal() {
		static GBUFFEREDINPUT_GLOBAL keyData;
		return keyData;
	}
}

namespace GW {
	namespace I {
		class GBufferedInputImplementation : public virtual GBufferedInputInterface {
		private:
			CORE::GThreadShared threadShare;
			winrt::event_token m_keyDown;
			winrt::event_token m_keyUp;
			winrt::event_token m_pointerMoved;
			winrt::event_token m_pointerPressed;
			winrt::event_token m_pointerReleased;
			winrt::event_token m_pointerWheelChanged;

			// This should only be used if there is only one view
			winrt::Windows::Foundation::IAsyncAction CallOnMainViewUiThreadAsync(winrt::Windows::UI::Core::DispatchedHandler handler) const
			{
				co_await winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher().RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal, handler);
			}

		private:
			void OnKeyDown(winrt::Windows::UI::Core::KeyEventArgs const& _args) {
				GEvent m_GEvent;
				Events m_Event = Events::Invalid;
				EVENT_DATA m_EventData;

				m_EventData.screenX = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.screenY = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.x = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);
				m_EventData.y = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);

				unsigned int keyMask = internal_gw::GBufferedInputGlobal().keyMask;
				m_EventData.data = Keycodes[(USHORT)_args.VirtualKey()];
				m_Event = Events::KEYPRESSED;

				if (internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] == 1)
					m_Event = Events::Invalid;
				else if (internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] == 0)
					internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] = 1;

				internal_gw::GBufferedInputGlobal().keyMask = keyMask;
				m_EventData.keyMask = keyMask;

				m_GEvent.Write(m_Event, m_EventData);
				if (m_Event != Events::Invalid)
					internal_gw::GBufferedInputGlobal().keyEvents.Push(m_GEvent);
			}

			void OnKeyUp(winrt::Windows::UI::Core::KeyEventArgs const& _args) {
				GEvent m_GEvent;
				Events m_Event = Events::Invalid;
				EVENT_DATA m_EventData;

				m_EventData.screenX = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.screenY = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.x = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);
				m_EventData.y = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);

				unsigned int keyMask = internal_gw::GBufferedInputGlobal().keyMask;
				m_EventData.data = Keycodes[(USHORT)_args.VirtualKey()];
				m_Event = Events::KEYRELEASED;

				internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] = 0;

				internal_gw::GBufferedInputGlobal().keyMask = keyMask;
				m_EventData.keyMask = keyMask;

				m_GEvent.Write(m_Event, m_EventData);
				if (m_Event != Events::Invalid)
					internal_gw::GBufferedInputGlobal().keyEvents.Push(m_GEvent);
			}

			void OnMouseMoved(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				if (internal_gw::GBufferedInputGlobal().lastMousePosition.X - _args.CurrentPoint().Position().X != 0 ||
					internal_gw::GBufferedInputGlobal().lastMousePosition.Y - _args.CurrentPoint().Position().Y != 0) {
					internal_gw::GBufferedInputGlobal().lastMousePosition = _args.CurrentPoint().Position();
					internal_gw::GBufferedInputGlobal().lastMousePositionRaw = _args.CurrentPoint().RawPosition();
				}
			}

			void OnMousePressed(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				GEvent m_GEvent;
				Events m_Event = Events::Invalid;
				EVENT_DATA m_EventData;
				m_EventData.data = G_KEY_UNKNOWN;

				m_EventData.screenX = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.screenY = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.x = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);
				m_EventData.y = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);

				unsigned int keyMask = internal_gw::GBufferedInputGlobal().keyMask;

				if (_args.CurrentPoint().Properties().IsLeftButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_LEFT] == 0)
					m_EventData.data = G_BUTTON_LEFT;
				if (_args.CurrentPoint().Properties().IsRightButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_LEFT] == 0)
					m_EventData.data = G_BUTTON_RIGHT;
				if (_args.CurrentPoint().Properties().IsMiddleButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_LEFT] == 0)
					m_EventData.data = G_BUTTON_MIDDLE;

				m_Event = Events::KEYPRESSED;

				if (internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] == 1)
					m_Event = Events::Invalid;
				else if (internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] == 0)
					internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] = 1;

				internal_gw::GBufferedInputGlobal().keyMask = keyMask;
				m_EventData.keyMask = keyMask;

				m_GEvent.Write(m_Event, m_EventData);
				if (m_Event != Events::Invalid && m_EventData.data != G_KEY_UNKNOWN)
					internal_gw::GBufferedInputGlobal().keyEvents.Push(m_GEvent);
			}

			void OnMouseReleased(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				GEvent m_GEvent;
				Events m_Event = Events::Invalid;
				EVENT_DATA m_EventData;
				m_EventData.data = G_KEY_UNKNOWN;

				m_EventData.screenX = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.screenY = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.x = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);
				m_EventData.y = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);

				unsigned int keyMask = internal_gw::GBufferedInputGlobal().keyMask;

				if (!_args.CurrentPoint().Properties().IsLeftButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_LEFT] == 1)
					m_EventData.data = G_BUTTON_LEFT;
				if (!_args.CurrentPoint().Properties().IsRightButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_RIGHT] == 1)
					m_EventData.data = G_BUTTON_RIGHT;
				if (!_args.CurrentPoint().Properties().IsMiddleButtonPressed() && internal_gw::GBufferedInputGlobal().lastPressedState[G_BUTTON_MIDDLE] == 1)
					m_EventData.data = G_BUTTON_MIDDLE;

				m_Event = Events::KEYRELEASED;

				internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] = 0;

				internal_gw::GBufferedInputGlobal().keyMask = keyMask;
				m_EventData.keyMask = keyMask;

				m_GEvent.Write(m_Event, m_EventData);
				if (m_Event != Events::Invalid && m_EventData.data != G_KEY_UNKNOWN)
					internal_gw::GBufferedInputGlobal().keyEvents.Push(m_GEvent);
			}

			void OnMouseScroll(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				unsigned int gKey;
				int scrollDelta = _args.CurrentPoint().Properties().MouseWheelDelta();
				if (scrollDelta > 0)
					gKey = G_MOUSE_SCROLL_UP;
				else if (scrollDelta < 0)
					gKey = G_MOUSE_SCROLL_DOWN;

				GEvent m_GEvent;
				Events m_Event = Events::Invalid;
				EVENT_DATA m_EventData;

				m_EventData.screenX = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.screenY = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePositionRaw.X);
				m_EventData.x = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);
				m_EventData.y = static_cast<int>(internal_gw::GBufferedInputGlobal().lastMousePosition.X);

				unsigned int keyMask = internal_gw::GBufferedInputGlobal().keyMask;
				m_EventData.data = gKey;
				m_Event = Events::MOUSESCROLL;

				internal_gw::GBufferedInputGlobal().lastPressedState[m_EventData.data] = 0;

				internal_gw::GBufferedInputGlobal().keyMask = keyMask;
				m_EventData.keyMask = keyMask;

				m_GEvent.Write(m_Event, m_EventData);
				if (m_Event != Events::Invalid)
					internal_gw::GBufferedInputGlobal().keyEvents.Push(m_GEvent);
			}


		public:
			~GBufferedInputImplementation() {
				// revoke events
				auto process = CallOnMainViewUiThreadAsync([this]()
					{
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						window.KeyDown(m_keyDown);
						window.KeyUp(m_keyUp);
						window.PointerMoved(m_pointerMoved);
						window.PointerPressed(m_pointerPressed);
						window.PointerReleased(m_pointerReleased);
						window.PointerWheelChanged(m_pointerWheelChanged);
					}); process.get();
			}

			// GWindow - one version
			// UNIVERSAL_WINDOW_HANDLE by value - another version
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle) {
				if (!_windowHandle.window)
					return GW::GReturn::INVALID_ARGUMENT;

				// subscribe events to delegates
				// call on main thread
				auto process = CallOnMainViewUiThreadAsync([this]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						// Event::KEYDOWN
						this->m_keyDown = window.KeyDown([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::KeyEventArgs const& args)
							{
								this->OnKeyDown(args);
							});


						// Event::KEYUP
						this->m_keyUp = window.KeyUp([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::KeyEventArgs const& args)
							{
								this->OnKeyUp(args);
							});

						// Event::MOUSEMOVE
						this->m_pointerMoved = window.PointerMoved([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseMoved(args);
							});
						// Event::MOUSEPRESSED
						this->m_pointerPressed = window.PointerPressed([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMousePressed(args);
							});
						// Event::MOUSERELEASED
						this->m_pointerReleased = window.PointerReleased([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseReleased(args);
							});
						// Event::MOUSESCROLLED
						this->m_pointerWheelChanged = window.PointerWheelChanged([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseScroll(args);
							});
					}); process.get();

					return GReturn::SUCCESS;
			}

			GReturn Create(const GW::SYSTEM::GWindow _gWindow) {
				if (!_gWindow)
					return GReturn::INVALID_ARGUMENT;

				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwh;
				GReturn code = _gWindow.GetWindowHandle(uwh);
				if (G_FAIL(code))
					return code;

				return Create(uwh);
			}

			// transfer any receivers to internal global broadcaster
			GReturn Register(CORE::GEventCache _observer) override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Register(_observer);
			}
			GReturn Register(CORE::GEventResponder _observer) override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Register(_observer);
			}
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Register(_observer, _callback);
			}
			GReturn Deregister(CORE::GInterface _observer) override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Deregister(_observer);
			}
			GReturn Observers(unsigned int& _outCount) const override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Observers(_outCount);
			}
			GReturn Push(const GEvent& _newEvent) override {
				return internal_gw::GBufferedInputGlobal().keyEvents.Push(_newEvent);
			}
		};
	}
}