#include <map>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "../GInput/GInputTableRouting.hpp"
#include <vector>

namespace internal_gw
{
	struct GBUFFEREDINPUT_VALUES
	{
		unsigned int keyMask = 0;
		LONG_PTR _userWinProc = 0;
		POINT lastMousePosition = { 0, 0 };
		UCHAR lastPressedState[256] = { 0, };
		GW::CORE::GEventGenerator keyEvents;

		unsigned int count = 0;
		GBUFFEREDINPUT_VALUES() {
			keyEvents.Create();
		}
	};

	inline std::map<HWND, GBUFFEREDINPUT_VALUES>& GetWindowBufferedInputValues() {
		static std::map<HWND, GBUFFEREDINPUT_VALUES> hWndMapBuffered;
		return hWndMapBuffered;
	}
}

namespace GW
{
	namespace I
	{
		class GBufferedInputImplementation : public virtual GBufferedInputInterface
		{
		private:
			HWND hWnd = nullptr;
			internal_gw::GBUFFEREDINPUT_VALUES* inputStates;
			
			static LRESULT CALLBACK GWindowProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
			{
				switch (uMsg)
				{
				case WM_INPUT:
				{
					GEvent m_GEvent;
					Events m_Event = Events::Invalid;
					EVENT_DATA m_EventData;

					POINT p;
					if (GetCursorPos(&p))
					{
						m_EventData.screenX = p.x;
						m_EventData.screenY = p.y;
					}
					if (ScreenToClient(hWnd, &p))
					{
						m_EventData.x = p.x;
						m_EventData.y = p.y;
					}

					UINT dwSize = 0;
					unsigned int keyMask = internal_gw::GetWindowBufferedInputValues()[hWnd].keyMask;
					//Get the size of RawInput
					GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
					static std::vector<BYTE> rawbuff;
					if (rawbuff.capacity() < dwSize)
						rawbuff.reserve(dwSize);
					if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, rawbuff.data(), &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
					{
						break;
					}
					RAWINPUT* raw = (RAWINPUT*)rawbuff.data();
					m_Event = Events::Invalid;
					m_EventData.data = -1;

					if (raw->header.dwType == RIM_TYPEKEYBOARD)
					{
						//Get G_KEY
						m_EventData.data = Keycodes[raw->data.keyboard.MakeCode];

						//Set state released or pressed.
						switch (raw->data.keyboard.Message) {
						case 256:

							m_Event = Events::KEYPRESSED;
							switch (m_EventData.data) {
							case G_KEY_RIGHTSHIFT:
							case G_KEY_LEFTSHIFT:
								G_TURNON_BIT(keyMask, G_MASK_SHIFT);
								break;
							case G_KEY_RIGHTCONTROL:
							case G_KEY_LEFTCONTROL:
								G_TURNON_BIT(keyMask, G_MASK_CONTROL);
								break;
							case G_KEY_CAPSLOCK:
								G_TOGGLE_BIT(keyMask, G_MASK_CAPS_LOCK);
								break;
							case G_KEY_NUMLOCK:
								G_TOGGLE_BIT(keyMask, G_MASK_NUM_LOCK);
								break;
							case G_KEY_SCROLL_LOCK:
								G_TOGGLE_BIT(keyMask, G_MASK_SCROLL_LOCK);
								break;
							}
							break;
						case 257:
							m_Event = Events::KEYRELEASED;
							switch (m_EventData.data) {
							case G_KEY_RIGHTSHIFT:
							case G_KEY_LEFTSHIFT:
								G_TURNOFF_BIT(keyMask, G_MASK_SHIFT);
								break;
							case G_KEY_RIGHTCONTROL:
							case G_KEY_LEFTCONTROL:
								G_TURNOFF_BIT(keyMask, G_MASK_CONTROL);
								break;
							}
							break;
						}
					}
					else if (raw->header.dwType == RIM_TYPEMOUSE)
					{
						//Mouse button events and movement events can occur at the same time.
						switch (raw->data.mouse.usFlags) {
							//Mouse Move
						case 0:
						case 1:
							//Check if the mouse position changed by at least a pixel amount, we don't want to send messages for sub-pixel change.
							if ((m_EventData.x - internal_gw::GetWindowBufferedInputValues()[hWnd].lastMousePosition.x) != 0 ||
								(m_EventData.y - internal_gw::GetWindowBufferedInputValues()[hWnd].lastMousePosition.y) != 0)
							{
								m_EventData.data = G_KEY_UNKNOWN;
								m_EventData.keyMask = keyMask;

								m_Event = Events::MOUSEMOVE;
								m_GEvent.Write(m_Event, m_EventData);
								internal_gw::GetWindowBufferedInputValues()[hWnd].keyEvents.Push(m_GEvent);

								internal_gw::GetWindowBufferedInputValues()[hWnd].lastMousePosition = { m_EventData.x, m_EventData.y };
							}
							break;
						}

						//Set Code
						switch (raw->data.mouse.ulButtons) {
						case 1:
						case 2:
							m_EventData.data = G_BUTTON_LEFT;
							break;
						case 4:
						case 8:
							m_EventData.data = G_BUTTON_RIGHT;
							break;
						case 16:
						case 32:
							m_EventData.data = G_BUTTON_MIDDLE;
							break;
						}

						switch (raw->data.mouse.usButtonData) {
						case 120:
							m_EventData.data = G_MOUSE_SCROLL_UP;
							break;
						case 65416:
							m_EventData.data = G_MOUSE_SCROLL_DOWN;
							break;
						}

						switch (raw->data.mouse.usButtonFlags) {
							//Mouse Pressed
						case 1:
						case 4:
						case 16:
							m_Event = Events::BUTTONPRESSED;
							break;
							//Mouse Released
						case 2:
						case 8:
						case 32:
							m_Event = Events::BUTTONRELEASED;
							break;
							//Scroll
						case 1024:
							m_Event = Events::MOUSESCROLL;
							break;
						}
					}
					// don't send key repeats when holding down the button
					if (internal_gw::GetWindowBufferedInputValues()[hWnd].
					lastPressedState[m_EventData.data] == 1 &&
					m_Event == Events::KEYPRESSED)
						m_Event = Events::Invalid;
					else if (internal_gw::GetWindowBufferedInputValues()[hWnd].
					lastPressedState[m_EventData.data] == 0 &&
					m_Event == Events::KEYPRESSED)
						internal_gw::GetWindowBufferedInputValues()[hWnd].lastPressedState[m_EventData.data] = 1;
					else if (internal_gw::GetWindowBufferedInputValues()[hWnd].
					lastPressedState[m_EventData.data] == 1 &&
					m_Event == Events::KEYRELEASED)
						internal_gw::GetWindowBufferedInputValues()[hWnd].lastPressedState[m_EventData.data] = 0;

					//If the mouse move event was the only event, don't send it again.
					if (m_Event != Events::MOUSEMOVE)
					{
						internal_gw::GetWindowBufferedInputValues()[hWnd].keyMask = keyMask;
						m_EventData.keyMask = keyMask;

						m_GEvent.Write(m_Event, m_EventData);
						if (m_Event != Events::Invalid)
							internal_gw::GetWindowBufferedInputValues()[hWnd].keyEvents.Push(m_GEvent);
					}
				}
				break;

				default:
					break;
				}
				return CallWindowProcW((WNDPROC)internal_gw::GetWindowBufferedInputValues()[hWnd]._userWinProc, hWnd, uMsg, wParam, lParam);
			}
		public:
			~GBufferedInputImplementation()
			{
				if (hWnd) {
					--inputStates->count;
					//Sets the WinProc back. (Fixes the StackOverFlow bug)
					if (inputStates->count == 0) {
						SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)inputStates->_userWinProc);
						internal_gw::GetWindowBufferedInputValues().erase(hWnd);
					}
				}
			}
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle)
			{
				if (!_windowHandle.window)
					return GReturn::INVALID_ARGUMENT;

				hWnd = static_cast<HWND>(_windowHandle.window);
				auto iter = internal_gw::GetWindowBufferedInputValues().find(hWnd);

				if (iter != internal_gw::GetWindowBufferedInputValues().end()) {
					inputStates = &internal_gw::GetWindowBufferedInputValues()[hWnd];
					++inputStates->count;

					return GW::GReturn::SUCCESS;
				}

				internal_gw::GetWindowBufferedInputValues()[hWnd] = internal_gw::GBUFFEREDINPUT_VALUES();
				inputStates = &internal_gw::GetWindowBufferedInputValues()[hWnd];

				inputStates->count = 1;

				if ((inputStates->_userWinProc = SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)GWindowProcedure)) == NULL)
				{
					//The user has not setup a windows proc prior to this point.
					printf("SetWindowLongPtr Error : %d \n", GetLastError());
					return GReturn::FAILURE;
				}

				//Getting Raw Input Devices.
				UINT numDevices = 0;
				std::vector<RAWINPUTDEVICELIST> rawInputDeviceList;

				//Get Number of Devices.
				if (GetRawInputDeviceList(NULL, &numDevices, sizeof(RAWINPUTDEVICELIST)) == ((UINT)-1))
				{
					printf("GetRawInputDeviceList Error : %d \n", GetLastError());
					//If GetRawInputDeviceList returned 0 devices, following calls will fail.
					if (numDevices == 0)
					{
						printf("Zero devices returned from GetRawInputDeviceList.");
					}
					return GReturn::FAILURE;
				}

				//Allocate the list of devices.
				rawInputDeviceList.resize(sizeof(RAWINPUTDEVICELIST) * numDevices);
				if (rawInputDeviceList.size() < 1) {
					printf("Failed to allocate memory for RawInputDeviceList.");
					return GReturn::FAILURE;
				}

				int nNoOfDevices = 0;
				//Using the new List and number of devices,
				//populate the raw input device list.
				if ((nNoOfDevices = GetRawInputDeviceList(rawInputDeviceList.data(), &numDevices, sizeof(RAWINPUTDEVICELIST))) == ((UINT)-1))
				{
					printf("GetRawInputDeviceList Error : %d \n", GetLastError());
					return GReturn::FAILURE;
				}

				RID_DEVICE_INFO rdi;
				rdi.cbSize = sizeof(RID_DEVICE_INFO);

				//For all of the devices, display their correspondent information.
				for (int i = 0; i < nNoOfDevices; i++)
				{
					UINT size = 256;
					TCHAR tBuffer[256] = { 0 };
					tBuffer[0] = '\0';

					//Find the device name.
					if (!rawInputDeviceList.empty())
					{
						if (GetRawInputDeviceInfo(rawInputDeviceList[i].hDevice, RIDI_DEVICENAME, tBuffer, &size) < 0)
							return GW::GReturn::FAILURE;

						UINT cbSize = rdi.cbSize;
						//Get the device information.
						if (GetRawInputDeviceInfo(rawInputDeviceList[i].hDevice, RIDI_DEVICEINFO, &rdi, &cbSize) < 0)
							return GW::GReturn::FAILURE;
					}
				}

				//Register the raw input devices.
				RAWINPUTDEVICE rID[2];

				//KeyBoard
				rID[0].usUsagePage = 0x01;
				rID[0].usUsage = 0x06;
				rID[0].dwFlags = RIDEV_EXINPUTSINK;
				rID[0].hwndTarget = hWnd;

				//Mouse
				rID[1].usUsagePage = 0x01;
				rID[1].usUsage = 0x02;
				rID[1].dwFlags = RIDEV_EXINPUTSINK;
				rID[1].hwndTarget = hWnd;

				if (RegisterRawInputDevices(rID, 2, sizeof(rID[0])) == false)
				{
					printf("RegisterRawInputDevices Error: %d \n", GetLastError());
					return GReturn::FAILURE;
				}

				//Capslock
				if ((GetKeyState(VK_CAPITAL) & 0x0001) != 0)
				{
					G_TURNON_BIT(inputStates->keyMask, G_MASK_CAPS_LOCK);
				}

				//Numlock
				if ((GetKeyState(VK_NUMLOCK) & 0x0001) != 0)
				{
					G_TURNON_BIT(inputStates->keyMask, G_MASK_NUM_LOCK);
				}

				//ScrollLock
				if ((GetKeyState(VK_SCROLL) & 0x0001) != 0)
				{
					G_TURNON_BIT(inputStates->keyMask, G_MASK_SCROLL_LOCK);
				}

				return GReturn::SUCCESS;
			}

			GReturn Create(GW::SYSTEM::GWindow _window)
			{
				if (!_window)
					return GReturn::INVALID_ARGUMENT;

				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwh;
				GReturn code = _window.GetWindowHandle(uwh);
				if (G_FAIL(code))
					return code;

				return Create(uwh);
			}
			// transfer any receivers to internal global broadcaster
			GReturn Register(CORE::GEventCache _observer) override {
				return inputStates->keyEvents.Register(_observer);
			}
			GReturn Register(CORE::GEventResponder _observer) override {
				return inputStates->keyEvents.Register(_observer);
			}
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
				return inputStates->keyEvents.Register(_observer, _callback);
			}
			GReturn Deregister(CORE::GInterface _observer) override {
				return inputStates->keyEvents.Deregister(_observer);
			}
			GReturn Observers(unsigned int& _outCount) const override {
				return inputStates->keyEvents.Observers(_outCount);
			}
			GReturn Push(const GEvent& _newEvent) override {
				return inputStates->keyEvents.Push(_newEvent);
			}
		};
	}
}