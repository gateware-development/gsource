#include <cmath>
#include <chrono>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "GControllerTableRouting.hpp"
#include "../../../Interface/System/GDaemon.h"

#include "winrt/Windows.Foundation.Collections.h"
#include "winrt/Windows.Gaming.Input.h"
#include <roapi.h>

namespace GW
{
	namespace I
	{
		class GGeneralController; // Interface which all controller will inherit from
		class GXboxController; // xbox
		// class GPS4Controller; // Not supported yet! future devs, this is for you :)

		class GControllerImplementation : public virtual GControllerInterface,
			protected GEventGeneratorImplementation
		{
		private:
			GGeneralController* pController; // POLYMORPHISM lol
		public:
			~GControllerImplementation();

			GReturn Create();

			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override;
			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override;
			GReturn GetMaxIndex(int& _outMax) override;
			GReturn GetNumConnected(int& _outConnectedCount) override;
			GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override;
			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override;
			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override;
			GReturn StopVibration(unsigned int _controllerIndex) override;
			GReturn StopAllVibrations() override;

			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override;
			GReturn Observers(unsigned int& _outCount) const override;
			GReturn Push(const GEvent& _newEvent) override;
		};

		class GGeneralController : public GControllerInterface,
			protected GEventGeneratorImplementation
		{
		protected:
			struct CONTROLLER_STATE
			{
				int isConnected;
				int isVibrating;
				float vibrationDuration;
				std::chrono::high_resolution_clock::time_point* vibrationStartTime;
				int maxInputs; // Hold the size of controllerInputs array
				float* controllerInputs; // controllerInputs is used to hold an array for the input values of the controller
				GW::INPUT::GControllerType controllerID;
			} *pControllers;

			GW::INPUT::GControllerType controllerType;
			GW::I::GControllerInterface::DeadZoneTypes deadZoneType;
			//winrt::Windows::Gaming::Input::RawGameController:: lastReading[4];
			float deadZonePercentage;
			int numConnected;

			// This function does not lock before using _controllers
			unsigned int FindEmptyControllerIndex(unsigned int _maxIndex, const CONTROLLER_STATE* _controllers)
			{
				for (unsigned int i = 0; i < _maxIndex; ++i)
				{
					if (_controllers[i].isConnected == 0)
						return i;
				}
				return -1;
			}

			// perhaps make return value GRETURN
			CONTROLLER_STATE* CopyControllerState(const CONTROLLER_STATE* _stateToCopy, CONTROLLER_STATE* _outCopy)
			{
				if (_stateToCopy->maxInputs == _outCopy->maxInputs)
					for (int i = 0; i < _outCopy->maxInputs; ++i)
					{
						_outCopy->controllerInputs[i] = _stateToCopy->controllerInputs[i];
					}
				else
					_outCopy = nullptr;

				return _outCopy;
			}

			void DeadZoneCalculation(float _x, float _y, float _axisMax, float _axisMin, float& _outX, float& _outY, GW::I::GControllerInterface::DeadZoneTypes _deadzoneType, float _deadzonePercentage)
			{
				float range = _axisMax - _axisMin;
				_outX = (((_x - _axisMin) * 2) / range) - 1;
				_outY = (((_y - _axisMin) * 2) / range) - 1;
				float liveRange = 1.0f - _deadzonePercentage;
				if (_deadzoneType == GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE)
				{
					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;

					if (_outX > 0.0f)
						_outX = (_outX - _deadzonePercentage) / liveRange;
					else if (_outX < 0.0f)
						_outX = (_outX + _deadzonePercentage) / liveRange;
					if (_outY > 0.0f)
						_outY = (_outY - _deadzonePercentage) / liveRange;
					else if (_outY < 0.0f)
						_outY = (_outY + _deadzonePercentage) / liveRange;
				}
				else
				{
					float mag = std::sqrt(_outX * _outX + _outY * _outY);
					mag = (mag - _deadzonePercentage) / liveRange;
					_outX *= mag;
					_outY *= mag;

					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;
				}
			}
		public:
			GGeneralController()
			{
				
			}

			virtual void Initialize()
			{
				pControllers = new CONTROLLER_STATE[G_MAX_CONTROLLER_COUNT];
				deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
				deadZonePercentage = 0.2f;

				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					pControllers[i].isConnected = 0;
					pControllers[i].isVibrating = 0;
					pControllers[i].vibrationDuration = 0;
					pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
					pControllers[i].maxInputs = G_MAX_GENERAL_INPUTS;
					pControllers[i].controllerInputs = new float[G_MAX_GENERAL_INPUTS];
					for (unsigned int j = 0; j < G_MAX_GENERAL_INPUTS; ++j)
					{
						pControllers[i].controllerInputs[j] = 0.0f;
					}
				}
			}

			virtual void Release()
			{
				for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					delete[] pControllers[i].controllerInputs;
					delete pControllers[i].vibrationStartTime;
				}
				delete[] pControllers;
			}

			virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override
			{
				if (_controllerIndex < 0 || _controllerIndex > G_MAX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode >= G_MAX_GENERAL_INPUTS)
					return GReturn::INVALID_ARGUMENT;
				if (pControllers[_controllerIndex].isConnected == 0)
					return GReturn::FAILURE;
				LockAsyncRead();
				_outState = pControllers[_controllerIndex].controllerInputs[(_inputCode)];
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override
			{
				if (_controllerIndex < 0 || _controllerIndex > G_MAX_CONTROLLER_INDEX)
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
				UnlockAsyncRead();

				return GReturn::SUCCESS;
			}

			virtual GReturn GetMaxIndex(int& _outMax) override
			{
				_outMax = G_MAX_CONTROLLER_INDEX;
				return GReturn::SUCCESS;
			}

			virtual GReturn GetNumConnected(int& _outConnectedCount) override
			{
				_outConnectedCount = 0;
				LockAsyncRead();
				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isConnected)
						++_outConnectedCount;
				}
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override
			{
				if (_deadzonePercentage > 1.0f || _deadzonePercentage < 0.0f)
					return GReturn::INVALID_ARGUMENT;
				LockSyncWrite();
				deadZoneType = _type;
				deadZonePercentage = _deadzonePercentage;
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn StopVibration(unsigned int _controllerIndex) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn StopAllVibrations() override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override final { return GEventGeneratorImplementation::Register(_observer, _callback); }
			GReturn Observers(unsigned int& _outCount) const override final { return GEventGeneratorImplementation::Observers(_outCount); }
			GReturn Push(const GEvent& _newEvent) override final { return GEventGeneratorImplementation::Push(_newEvent); }
		};

		class GXboxController : public GGeneralController
		{
		private:
			GW::SYSTEM::GDaemon xinputDaemon;
			int XControllerSlotIndices[4];
			winrt::Windows::Gaming::Input::GamepadReading lastReading[4];
			struct DaemonData
			{
				winrt::Windows::Gaming::Input::GamepadReading controllerState;
				EVENT_DATA eventData;
				CONTROLLER_STATE oldState;

				DaemonData()
				{
					ZeroMemory(&controllerState, sizeof(winrt::Windows::Gaming::Input::GamepadReading));
					ZeroMemory(&eventData, sizeof(EVENT_DATA));
					oldState.maxInputs = G_MAX_XBOX_INPUTS;
					oldState.controllerInputs = new float[G_MAX_XBOX_INPUTS];
				}

				~DaemonData()
				{
					delete[] oldState.controllerInputs;
				}
			} daemonData;

			float XboxDeadZoneCalc(float _value, bool _isTrigger)
			{
				if (_isTrigger)
				{
					if (_value < 0.11764706f) // calculated from 30/255, what xinput uses for it's threshold
						_value = 0;
				}
				else
				{
					if (_value < 0.26516723f) // this number is from XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE
						_value = 0;
				}
				return _value;
			}
		public:
			GXboxController()
				: GGeneralController()
			{
				for (int i = 0; i < 4; ++i)
					XControllerSlotIndices[i] = -1;
			}

			virtual void Initialize()
			{
				pControllers = new CONTROLLER_STATE[G_MAX_XBOX_CONTROLLER_COUNT];
				deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
				deadZonePercentage = 0.2f;
				numConnected = 0;
				for (unsigned int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					pControllers[i].isConnected = 0;
					pControllers[i].isVibrating = 0;
					pControllers[i].vibrationDuration = 0;
					pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
					pControllers[i].maxInputs = G_MAX_XBOX_INPUTS;
					pControllers[i].controllerInputs = new float[G_MAX_XBOX_INPUTS];

					for (int j = 0; j < G_MAX_XBOX_INPUTS; ++j)
					{
						pControllers[i].controllerInputs[j] = 0;
					}
				}
				// Event callback
				xinputDaemon.Create(G_CONTROLLER_DAEMON_OPERATION_INTERVAL, [&]()
					{
						GW::GEvent m_GEvent;
						auto connectedPads = winrt::Windows::Gaming::Input::Gamepad::Gamepads();
						
						for (int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
						{
							if(numConnected < static_cast<int>(connectedPads.Size()))
							{
								LockSyncWrite();
								XControllerSlotIndices[i] = FindEmptyControllerIndex(G_MAX_XBOX_CONTROLLER_COUNT, pControllers);
								pControllers[XControllerSlotIndices[i]].isConnected = 1;
								++numConnected;
								UnlockSyncWrite();

								daemonData.eventData.controllerIndex = XControllerSlotIndices[i];
								daemonData.eventData.inputCode = 0;
								daemonData.eventData.inputValue = 0;
								daemonData.eventData.isConnected = 1;
								daemonData.eventData.controllerID = INPUT::GControllerType::XBOXONE;

								m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, daemonData.eventData);
								Push(m_GEvent);
							}

							if (numConnected > static_cast<int>(connectedPads.Size()))
							{
								if (XControllerSlotIndices[i] >= 0)
								{
									GW::GEvent m_GEvent;
									//call event
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].isConnected = 0;
									pControllers[XControllerSlotIndices[i]].isVibrating = 0;
									pControllers[XControllerSlotIndices[i]].vibrationDuration = 0.0f;
									--numConnected;
									UnlockSyncWrite();
									daemonData.eventData.controllerIndex = XControllerSlotIndices[i];
									daemonData.eventData.inputCode = 0;
									daemonData.eventData.inputValue = 0;
									daemonData.eventData.isConnected = 0;
									daemonData.eventData.controllerID = INPUT::GControllerType::XBOXONE;

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERDISCONNECTED, daemonData.eventData);
									Push(m_GEvent);
									XControllerSlotIndices[i] = -1;
								}
							}
								
							if (XControllerSlotIndices[i] >= 0)
							{
								daemonData.controllerState = connectedPads.GetAt(i).GetCurrentReading();
							}

							if (XControllerSlotIndices[i] >= 0 && daemonData.controllerState != lastReading[i])
							{
								daemonData.controllerState = connectedPads.GetAt(i).GetCurrentReading();
								CopyControllerState(&pControllers[XControllerSlotIndices[i]], &daemonData.oldState);
								daemonData.eventData.isConnected = 1;
								daemonData.eventData.controllerIndex = XControllerSlotIndices[i];

								lastReading[i] = daemonData.controllerState;

								//Buttons
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::A) == winrt::Windows::Gaming::Input::GamepadButtons::A ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_SOUTH_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_SOUTH_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::A) == winrt::Windows::Gaming::Input::GamepadButtons::A ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_SOUTH_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_SOUTH_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::B) == winrt::Windows::Gaming::Input::GamepadButtons::B ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_EAST_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_EAST_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::B) == winrt::Windows::Gaming::Input::GamepadButtons::B ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_EAST_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_EAST_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::Y) == winrt::Windows::Gaming::Input::GamepadButtons::Y ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_NORTH_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_NORTH_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::Y) == winrt::Windows::Gaming::Input::GamepadButtons::Y ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_NORTH_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_NORTH_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::X) == winrt::Windows::Gaming::Input::GamepadButtons::X ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_WEST_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_WEST_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::X) == winrt::Windows::Gaming::Input::GamepadButtons::X ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_WEST_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_WEST_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::LeftShoulder) == winrt::Windows::Gaming::Input::GamepadButtons::LeftShoulder ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_LEFT_SHOULDER_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_SHOULDER_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::LeftShoulder) == winrt::Windows::Gaming::Input::GamepadButtons::LeftShoulder ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_LEFT_SHOULDER_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_SHOULDER_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::RightShoulder) == winrt::Windows::Gaming::Input::GamepadButtons::RightShoulder ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_RIGHT_SHOULDER_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_SHOULDER_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::RightShoulder) == winrt::Windows::Gaming::Input::GamepadButtons::RightShoulder ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_RIGHT_SHOULDER_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_SHOULDER_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadLeft) == winrt::Windows::Gaming::Input::GamepadButtons::DPadLeft ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_DPAD_LEFT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_LEFT_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadLeft) == winrt::Windows::Gaming::Input::GamepadButtons::DPadLeft ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_LEFT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_LEFT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadRight) == winrt::Windows::Gaming::Input::GamepadButtons::DPadRight ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_DPAD_RIGHT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_RIGHT_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadRight) == winrt::Windows::Gaming::Input::GamepadButtons::DPadRight ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_RIGHT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_RIGHT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadUp) == winrt::Windows::Gaming::Input::GamepadButtons::DPadUp ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_DPAD_UP_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_UP_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadUp) == winrt::Windows::Gaming::Input::GamepadButtons::DPadUp ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_UP_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_UP_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadDown) == winrt::Windows::Gaming::Input::GamepadButtons::DPadDown ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_DPAD_DOWN_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_DOWN_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::DPadDown) == winrt::Windows::Gaming::Input::GamepadButtons::DPadDown ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_DOWN_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_DOWN_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::LeftThumbstick) == winrt::Windows::Gaming::Input::GamepadButtons::LeftThumbstick ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_LEFT_THUMB_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_THUMB_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::LeftThumbstick) == winrt::Windows::Gaming::Input::GamepadButtons::LeftThumbstick ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_LEFT_THUMB_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_THUMB_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::RightThumbstick) == winrt::Windows::Gaming::Input::GamepadButtons::RightThumbstick ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_RIGHT_THUMB_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_THUMB_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::RightThumbstick) == winrt::Windows::Gaming::Input::GamepadButtons::RightThumbstick ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_RIGHT_THUMB_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_THUMB_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::Menu) == winrt::Windows::Gaming::Input::GamepadButtons::Menu ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_START_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_START_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::Menu) == winrt::Windows::Gaming::Input::GamepadButtons::Menu ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_START_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_START_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::View) == winrt::Windows::Gaming::Input::GamepadButtons::View ? 1.0f : 0.0f)
									!= daemonData.oldState.controllerInputs[G_SELECT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_SELECT_BTN] = (daemonData.controllerState.Buttons & winrt::Windows::Gaming::Input::GamepadButtons::View) == winrt::Windows::Gaming::Input::GamepadButtons::View ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_SELECT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_SELECT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								// AXES
								if (XboxDeadZoneCalc(static_cast<float>(daemonData.controllerState.LeftTrigger), true) != daemonData.oldState.controllerInputs[G_LEFT_TRIGGER_AXIS])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_TRIGGER_AXIS] = XboxDeadZoneCalc(static_cast<float>(daemonData.controllerState.LeftTrigger), true);
									daemonData.eventData.inputCode = G_LEFT_TRIGGER_AXIS;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_TRIGGER_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (XboxDeadZoneCalc(static_cast<float>(daemonData.controllerState.RightTrigger), true) != daemonData.oldState.controllerInputs[G_RIGHT_TRIGGER_AXIS])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_TRIGGER_AXIS] = XboxDeadZoneCalc(static_cast<float>(daemonData.controllerState.RightTrigger), true);
									daemonData.eventData.inputCode = G_RIGHT_TRIGGER_AXIS;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_TRIGGER_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								LockSyncWrite();

								DeadZoneCalculation(static_cast<float>(daemonData.controllerState.LeftThumbstickX),
									static_cast<float>(daemonData.controllerState.LeftThumbstickY),
									1.0F,
									-1.0F,
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS],
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS],
									deadZoneType,
									deadZonePercentage);

								DeadZoneCalculation(static_cast<float>(daemonData.controllerState.RightThumbstickX),
									static_cast<float>(daemonData.controllerState.RightThumbstickY),
									1.0F,
									-1.0F,
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS],
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS],
									deadZoneType,
									deadZonePercentage);

								float newLX = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS];
								float newLY = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS];
								float newRX = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS];
								float newRY = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS];

								UnlockSyncWrite();

								if (newLX != daemonData.oldState.controllerInputs[G_LX_AXIS])
								{
									daemonData.eventData.inputCode = G_LX_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (newLY != daemonData.oldState.controllerInputs[G_LY_AXIS])
								{
									daemonData.eventData.inputCode = G_LY_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								if (newRX != daemonData.oldState.controllerInputs[G_RX_AXIS])
								{
									daemonData.eventData.inputCode = G_RX_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								if (newRY != daemonData.oldState.controllerInputs[G_RY_AXIS])
								{
									daemonData.eventData.inputCode = G_RY_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
							}

							if (XControllerSlotIndices[i] >= 0 && pControllers[XControllerSlotIndices[i]].isVibrating)
							{
								if (pControllers[XControllerSlotIndices[i]].vibrationDuration <=
									(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() -
										*pControllers[XControllerSlotIndices[i]].vibrationStartTime).count() * .001f))
								{
									winrt::Windows::Gaming::Input::GamepadVibration vibrationState;
									vibrationState.LeftMotor = 0;
									vibrationState.RightMotor = 0;
									vibrationState.LeftTrigger = 0;
									vibrationState.RightTrigger = 0;
									pControllers[XControllerSlotIndices[i]].isVibrating = 0;
									pControllers[XControllerSlotIndices[i]].vibrationDuration = 0.0f;
									//myGamepads.at(i).Vibration(vibrationState);
									connectedPads.GetAt(i).Vibration(vibrationState);
								}
							}
						}
					});
			}

			virtual void Release()
			{
				for (int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					delete[] pControllers[i].controllerInputs;
					delete pControllers[i].vibrationStartTime;
				}
				delete[] pControllers;
			}

			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override final
			{
				if (_controllerIndex < 0 || _controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode  > 19)
					return GReturn::INVALID_ARGUMENT;
				if (pControllers[_controllerIndex].isConnected == 0)
					return GReturn::FAILURE;
				LockAsyncRead();
				_outState = pControllers[_controllerIndex].controllerInputs[_inputCode];
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override final
			{
				if (_controllerIndex < 0 || _controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX)
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn GetMaxIndex(int& _outMax) override final
			{
				_outMax = G_MAX_XBOX_CONTROLLER_INDEX;
				return GReturn::SUCCESS;
			}

			GReturn GetNumConnected(int& _outConnectedCount) override final
			{
				_outConnectedCount = 0;
				LockAsyncRead();
				for (unsigned int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isConnected)
						++_outConnectedCount;
				}
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX)
					|| (_pan < -1.0f || _pan > 1.0f)
					|| _duration < 0.0f
					|| (_strength < -1.0f || _strength > 1.0f))
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isVibrating)
				{
					UnlockAsyncRead();
					return GReturn::REDUNDANT;
				}
				UnlockAsyncRead();

				winrt::Windows::Gaming::Input::GamepadVibration vibrationState;
				vibrationState.LeftMotor = _strength * (.5f + (.5f * (-1 * _pan)));
				vibrationState.RightMotor = _strength * (.5f + (.5f * _pan));

				LockSyncWrite();
				for (int i = 0; i < 4; ++i)
				{
					if (_controllerIndex == XControllerSlotIndices[i])
					{
						pControllers[i].isVibrating = 1;
						pControllers[i].vibrationDuration = _duration;
						*pControllers[i].vibrationStartTime = std::chrono::high_resolution_clock::now();
						
						// quick check to make sure gamepad is valid in list
						if (XControllerSlotIndices[i] >= 0)
						{
							winrt::Windows::Gaming::Input::Gamepad::Gamepads().GetAt(i).Vibration(vibrationState);
							break;
						}
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX))
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsVibrating = pControllers[_controllerIndex].isVibrating == 0 ? false : true;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn StopVibration(unsigned int _controllerIndex) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX))
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isVibrating == false)
				{
					UnlockAsyncRead();
					return GReturn::REDUNDANT;
				}
				UnlockAsyncRead();

				winrt::Windows::Gaming::Input::GamepadVibration vibrationState;
				vibrationState.LeftMotor = 0;
				vibrationState.RightMotor = 0;
				vibrationState.LeftTrigger = 0;
				vibrationState.RightTrigger = 0;

				LockSyncWrite();
				for (int i = 0; i < 4; ++i)
				{
					if (_controllerIndex == XControllerSlotIndices[i])
					{
						pControllers[i].isVibrating = 0;
						pControllers[i].vibrationDuration = 0.0f;
						
						// quick check to make sure gamepad is valid in list
						if (XControllerSlotIndices[i] >= 0)
						{
							winrt::Windows::Gaming::Input::Gamepad::Gamepads().GetAt(i).Vibration(vibrationState);
							break;
						}
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			GReturn StopAllVibrations() override final
			{
				winrt::Windows::Gaming::Input::GamepadVibration vibrationState;
				vibrationState.LeftMotor = 0;
				vibrationState.RightMotor = 0;
				vibrationState.LeftTrigger = 0;
				vibrationState.RightTrigger = 0;

				LockSyncWrite();
				for (int i = 0; i < 4; ++i)
				{
					if (pControllers[i].isVibrating)
					{
						pControllers[i].isVibrating = 0;
						pControllers[i].vibrationDuration = 0.0f;

						// quick check to make sure gamepad is valid in list
						if (XControllerSlotIndices[i] >= 0)
						{
							winrt::Windows::Gaming::Input::Gamepad::Gamepads().GetAt(i).Vibration(vibrationState);
						}
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}
		};

		// Need to put definitions here instead of class body because it needs to know function definitions
		inline GControllerImplementation::~GControllerImplementation()
		{
			if (pController)
			{
				pController->Release();
				delete pController;
				pController = nullptr;
			}
		}

		inline GReturn GControllerImplementation::Create()
		{
			pController = new GXboxController();
			pController->Initialize();

			return GReturn::SUCCESS;
		}
		inline GReturn GControllerImplementation::GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) { return pController ? pController->GetState(_controllerIndex, _inputCode, _outState) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) { return pController ? pController->IsConnected(_controllerIndex, _outIsConnected) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetMaxIndex(int& _outMax) { return pController ? pController->GetMaxIndex(_outMax) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetNumConnected(int& _outConnectedCount) { return pController ? pController->GetNumConnected(_outConnectedCount) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) { return pController ? pController->SetDeadZone(_type, _deadzonePercentage) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) { return pController ? pController->StartVibration(_controllerIndex, _pan, _duration, _strength) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) { return pController ? pController->IsVibrating(_controllerIndex, _outIsVibrating) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopVibration(unsigned int _controllerIndex) { return pController ? pController->StopVibration(_controllerIndex) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopAllVibrations() { return pController ? pController->StopAllVibrations() : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) { return pController ? pController->Register(_observer, _callback) : GEventGeneratorImplementation::Register(_observer, _callback); }
		inline GReturn GControllerImplementation::Observers(unsigned int& _outCount) const { return pController ? pController->Observers(_outCount) : GEventGeneratorImplementation::Observers(_outCount); }
		inline GReturn GControllerImplementation::Push(const GEvent& _newEvent) { return pController ? pController->Push(_newEvent) : GEventGeneratorImplementation::Push(_newEvent); }
	} // end I namespace
} // end GW namespace