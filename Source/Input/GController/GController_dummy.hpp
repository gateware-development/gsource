// dummy implementation for GController
namespace GW {
	namespace I {
		class GControllerImplementation :	public virtual GControllerInterface,
											public GEventGeneratorImplementation
		{
		public:
			GReturn Create() {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override {
				return GW::GReturn::FAILURE;
			}
			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override {
				return GW::GReturn::FAILURE;
			}
			GReturn GetMaxIndex(int& _outMax) override {
				return GW::GReturn::FAILURE;
			}
			GReturn GetNumConnected(int& _outConnectedCount) override {
				return GW::GReturn::FAILURE;
			}
			GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override {
				return GW::GReturn::FAILURE;
			}
			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override {
				return GW::GReturn::FAILURE;
			}
			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override {
				return GW::GReturn::FAILURE;
			}
			GReturn StopVibration(unsigned int _controllerIndex) override {
				return GW::GReturn::FAILURE;
			}
			GReturn StopAllVibrations() override {
				return GW::GReturn::FAILURE;
			}
		};
	} // end I
} // end GW