#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/input-event-codes.h>
#include <chrono>
#include <cmath>
#include <dirent.h>
#include <string>
#include <cstring>
#include <atomic>
#include "../../../Interface/System/GConcurrent.h"

namespace GW
{
	namespace I
	{
		class GGeneralController; // Interface which all controller will inherit from

		class GControllerImplementation : public virtual GControllerInterface,
			protected GEventGeneratorImplementation
		{
		private:
			GGeneralController* pController; // POLYMORPHISM
		public:
			~GControllerImplementation();

			GReturn Create();

			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override;
			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override;
			GReturn GetMaxIndex(int& _outMax) override;
			GReturn GetNumConnected(int& _outConnectedCount) override;
			GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override;
			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override;
			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override;
			GReturn StopVibration(unsigned int _controllerIndex) override;
			GReturn StopAllVibrations() override;

			GReturn Register(CORE::GEventCache _observer) override;
			GReturn Register(CORE::GEventResponder _observer) override;
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override;
			GReturn Deregister(CORE::GInterface _observer) override;
			GReturn Observers(unsigned int& _outCount) const override;
			GReturn Push(const GEvent& _newEvent) override;
		};

		class GGeneralController : public virtual GControllerInterface,
			protected GEventGeneratorImplementation
		{
		protected:
			struct CONTROLLER_STATE
			{
				int isConnected;
				int isVibrating;
				float vibrationDuration;
				std::chrono::high_resolution_clock::time_point* vibrationStartTime;
				int maxInputs; // Hold the size of controllerInputs array
				float* controllerInputs; // controllerInputs is used to hold an array for the input values of the controller
				GW::INPUT::GControllerType controllerID;
				char controllerFilePath[8];
				int codeMapping;
				int axisMapping;
			} *pControllers;

			struct G_inotify_event
			{
				int wd;        /* Watch descriptor.  */
				uint32_t mask;    /* Watch mask.  */
				uint32_t cookie;    /* Cookie to synchronize two events.  */
				uint32_t len;        /* Length (including NULs) of name.  */
				char name[16];    /* Name.  */
			};

			GW::I::GControllerInterface::DeadZoneTypes deadZoneType;
			float deadZonePercentage;

			std::atomic_bool isRunning;
			GW::SYSTEM::GConcurrent controllerThreads[G_MAX_CONTROLLER_COUNT];
			std::atomic_bool isControllerThreadActive[G_MAX_CONTROLLER_COUNT];
			GW::SYSTEM::GConcurrent notifyThread;

			// This function does not lock before using _controllers
			int FindEmptyControllerIndex(unsigned int _maxIndex, const CONTROLLER_STATE* _controllers)
			{
				for (unsigned int i = 0; i < _maxIndex; ++i)
				{
					if (_controllers[i].isConnected == 0)
						return i;
				}
				return -1;
			}

			CONTROLLER_STATE* CopyControllerState(const CONTROLLER_STATE* _stateToCopy, CONTROLLER_STATE* _outCopy)
			{
				if (_stateToCopy->maxInputs == _outCopy->maxInputs)
					for (int i = 0; i < _outCopy->maxInputs; ++i)
					{
						_outCopy->controllerInputs[i] = _stateToCopy->controllerInputs[i];
					}
				else
					_outCopy = nullptr;

				return _outCopy;
			}

			void DeadZoneCalculation(float _x, float _y, float _axisMax, float _axisMin, float& _outX, float& _outY, GW::I::GControllerInterface::DeadZoneTypes _deadzoneType, float _deadzonePercentage)
			{
				float range = _axisMax - _axisMin;
				_outX = (((_x - _axisMin) * 2) / range) - 1;
				_outY = (((_y - _axisMin) * 2) / range) - 1;
				float liveRange = 1.0f - _deadzonePercentage;
				if (_deadzoneType == GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE)
				{
					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;

					if (_outX > 0.0f)
						_outX = (_outX - _deadzonePercentage) / liveRange;
					else if (_outX < 0.0f)
						_outX = (_outX + _deadzonePercentage) / liveRange;
					if (_outY > 0.0f)
						_outY = (_outY - _deadzonePercentage) / liveRange;
					else if (_outY < 0.0f)
						_outY = (_outY + _deadzonePercentage) / liveRange;
				}
				else
				{
					float mag = std::sqrt(_outX * _outX + _outY * _outY);
					mag = (mag - _deadzonePercentage) / liveRange;
					_outX *= mag;
					_outY *= mag;

					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;
				}
			}

			// This functions gets an array of bits representing the keys supported for the device and check if BTN_GAMEPAD/BTN_SOUTH is set
			bool isGamepadBitSet(int _event_fd)
			{
				// keys' size is based on how many sets of 64 bits it would take to cover all of the different types of keys where each key is a bit
				unsigned long keys[(KEY_CNT + 64 - 1) / 64];
				// EXVIOCGBIT gets the keys supported by the device returned as an array of 64 bits per index
				// BTN_GAMEPAD represents the bit we want to check so we find where the bit would be located in the array
				// and in the current 64 bits
				ioctl(_event_fd, EVIOCGBIT(EV_KEY, sizeof(keys)), keys);
				return (keys[BTN_GAMEPAD / 64] & (1LL << (BTN_GAMEPAD % 64))) != 0 ? true : false;
			}

			void GetControllerIDAndMappings(int _event_fd, GW::INPUT::GControllerType& _outControllerID, int& _outCodeMapping, int& _outAxisMapping)
			{
				char name[256];
				memset(name, ' ', 256);
				ioctl(_event_fd, EVIOCGNAME(256), name);

				if (strstr(name, "Sony") != nullptr)
				{
					_outControllerID = GW::INPUT::GControllerType::PS4;
					_outCodeMapping = G_CODE_MAPPING_PS4_WIRED;
					_outAxisMapping = G_LINUX_AXIS_MAPPING_PS4_WIRED;
				}
				else if (strstr(name, "Xbox") != nullptr || strstr(name, "Microsoft") != nullptr)
				{
					if (strstr(name, "Wireless") != nullptr)
					{
						_outControllerID = GW::INPUT::GControllerType::XBOXONE;
						_outCodeMapping = G_CODE_MAPPING_XBOXONE_WIRELESS;
						_outAxisMapping = G_LINUX_AXIS_MAPPING_XBOXONE_WIRELESS;
					}
					else if (strstr(name, "One") != nullptr)
					{
						_outControllerID = GW::INPUT::GControllerType::XBOXONE;
						_outCodeMapping = G_CODE_MAPPING_XBOXONE_WIRED;
						_outAxisMapping = G_LINUX_AXIS_MAPPING_XBOXONE_WIRED;
					}
					else
					{
						_outControllerID = GW::INPUT::GControllerType::XBOX360;
						_outCodeMapping = G_CODE_MAPPING_XBOX360;
						_outAxisMapping = G_LINUX_AXIS_MAPPING_XBOX360;
					}
				}
				else if (strstr(name, "Wireless Controller") != nullptr)
				{
					_outControllerID = GW::INPUT::GControllerType::PS4;
					_outCodeMapping = G_CODE_MAPPING_PS4_WIRELESS;
					_outAxisMapping = G_LINUX_AXIS_MAPPING_PS4_WIRELESS;
				}
				else
				{
					_outControllerID = GW::INPUT::GControllerType::GENERAL;
					_outCodeMapping = G_CODE_MAPPING_GENERAL;
					_outAxisMapping = G_LINUX_AXIS_MAPPING_GENERAL;
				}
			}

			void ControllerInputThreadEntryPoint(unsigned int _controllerIndex, int fd)
			{
				GEvent l_GEvent;
				input_event ev; // time value type code
				input_event base;
				base.value = 0;
				base.type = 0;
				base.code = 0;
				EVENT_DATA eventData;
				int lastLX = 0, lastLY = 0, lastRX = 0, lastRY = 0, lastLT = 0, lastRT = 0;

				while (isControllerThreadActive[_controllerIndex])
				{
					ev = base;
					int result = read(fd, &ev, sizeof(struct input_event));
					if (result != -1)
					{
						switch (ev.type)
						{
						case EV_KEY:
						{
							if (ev.code < KEY_BACK)
								break;

							int inputCode = Linux_ControllerCodes[ev.code - KEY_BACK][pControllers[_controllerIndex].codeMapping];

							if (inputCode == G_UNKNOWN_INPUT)
								break;

							LockSyncWrite();
							pControllers[_controllerIndex].controllerInputs[inputCode] = ev.value;
							eventData.inputCode = inputCode;
							eventData.inputValue = pControllers[_controllerIndex].controllerInputs[inputCode];
							eventData.controllerIndex = _controllerIndex;
							eventData.controllerID = pControllers[_controllerIndex].controllerID;
							UnlockSyncWrite();

							l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
							Push(l_GEvent);
						}
						break;

						case EV_ABS:
						{
							if (ev.code <= ABS_BRAKE)
							{
								int inputCode = GW::I::Linux_ControllerAxisCodes[ev.code][pControllers[_controllerIndex].codeMapping];
								int axisOffset = GW::I::ControllerAxisOffsets[inputCode];

								switch (inputCode)
								{
								case G_LX_AXIS:
								{
									// leftX
									if (ev.value != lastLX)
									{
										LockSyncWrite();
										float oldY = pControllers[_controllerIndex].controllerInputs[G_LY_AXIS];
										lastLX = ev.value;
										DeadZoneCalculation(lastLX,
											lastLY,
											GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset],
											GW::I::Linux_ControllerAxisRangesMin[pControllers[_controllerIndex].axisMapping][axisOffset],
											pControllers[_controllerIndex].controllerInputs[G_LX_AXIS],
											pControllers[_controllerIndex].controllerInputs[G_LY_AXIS],
											deadZoneType,
											deadZonePercentage);

										eventData.inputCode = G_LX_AXIS;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_LX_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
										Push(l_GEvent);

										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_LY_AXIS] *= -1.0f; // to fix flipped value
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_LY_AXIS];
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										eventData.controllerIndex = _controllerIndex;
										UnlockSyncWrite();

										if (oldY != eventData.inputValue)
										{
											// Send LY event
											eventData.inputCode = G_LY_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;

								case G_LY_AXIS:
								{
									//leftY
									if (ev.value != lastLY)
									{
										LockSyncWrite();
										lastLY = ev.value; // evdev values for Y are flipped
										float oldX = pControllers[_controllerIndex].controllerInputs[G_LX_AXIS];
										int axisMapping = pControllers[_controllerIndex].axisMapping;
										DeadZoneCalculation(lastLX,
											lastLY,
											GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset],
											GW::I::Linux_ControllerAxisRangesMin[pControllers[_controllerIndex].axisMapping][axisOffset],
											pControllers[_controllerIndex].controllerInputs[G_LX_AXIS],
											pControllers[_controllerIndex].controllerInputs[G_LY_AXIS],
											deadZoneType,
											deadZonePercentage);

										// Send LY event
										pControllers[_controllerIndex].controllerInputs[G_LY_AXIS] *= -1.0f; // to fix flipped value
										eventData.inputCode = G_LY_AXIS;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_LY_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
										Push(l_GEvent);

										LockSyncWrite();
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_LX_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										if (oldX != eventData.inputValue)
										{
											// Send LX event
											eventData.inputCode = G_LX_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;

								case G_RX_AXIS:
								{
									// RightX
									if (ev.value != lastRX)
									{
										LockSyncWrite();
										float oldY = pControllers[_controllerIndex].controllerInputs[G_RY_AXIS];
										lastRX = ev.value;
										DeadZoneCalculation(lastRX,
											lastRY,
											GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset],
											GW::I::Linux_ControllerAxisRangesMin[pControllers[_controllerIndex].axisMapping][axisOffset],
											pControllers[_controllerIndex].controllerInputs[G_RX_AXIS],
											pControllers[_controllerIndex].controllerInputs[G_RY_AXIS],
											deadZoneType,
											deadZonePercentage);

										eventData.inputCode = G_RX_AXIS;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_RX_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
										Push(l_GEvent);

										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_RY_AXIS] *= -1.0f; // to fix flipped value
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_RY_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										if (oldY != eventData.inputValue)
										{
											// Send LY event
											eventData.inputCode = G_RY_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;

								case G_RY_AXIS:
								{
									//leftY
									if (ev.value != lastRY)
									{
										LockSyncWrite();
										lastRY = ev.value; // evdev values for y are flipped.
										float oldX = pControllers[_controllerIndex].controllerInputs[G_RX_AXIS];
										DeadZoneCalculation(lastRX,
											lastRY,
											GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset],
											GW::I::Linux_ControllerAxisRangesMin[pControllers[_controllerIndex].axisMapping][axisOffset],
											pControllers[_controllerIndex].controllerInputs[G_RX_AXIS],
											pControllers[_controllerIndex].controllerInputs[G_RY_AXIS],
											deadZoneType,
											deadZonePercentage);

										// Send LY event
										pControllers[_controllerIndex].controllerInputs[G_RY_AXIS] *= -1.0f; // to fix flipped value
										eventData.inputCode = G_RY_AXIS;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_RY_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
										Push(l_GEvent);

										LockSyncWrite();
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_RX_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										if (oldX != eventData.inputValue)
										{
											// Send LX event
											eventData.inputCode = G_RX_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;

								case G_LEFT_TRIGGER_AXIS:
								{
									// left trigger
									if (ev.value != lastLT)
									{
										LockSyncWrite();
										lastLT = ev.value;
										float oldAxis = pControllers[_controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS];
										if (ev.value > G_GENERAL_TRIGGER_THRESHOLD)
											pControllers[_controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS] = (float)ev.value / GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset];
										else
											pControllers[_controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS] = 0;

										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();
										if (oldAxis != eventData.inputValue)
										{
											eventData.inputCode = G_LEFT_TRIGGER_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;

								case G_RIGHT_TRIGGER_AXIS:
								{
									if (ev.value != lastRT)
									{
										LockSyncWrite();
										lastRT = ev.value;
										float oldAxis = pControllers[_controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS];
										if (ev.value > G_GENERAL_TRIGGER_THRESHOLD)
											pControllers[_controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS] = (float)ev.value / GW::I::Linux_ControllerAxisRangesMax[pControllers[_controllerIndex].axisMapping][axisOffset];
										else
											pControllers[_controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS] = 0;

										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();
										if (oldAxis != pControllers[_controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS])
										{
											eventData.inputCode = G_RIGHT_TRIGGER_AXIS;
											l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
											Push(l_GEvent);
										}
									}
								}
								break;
								}
							}
							else
							{
								switch (ev.code)
								{
								case ABS_HAT0X:
								case ABS_HAT3X:
								{
									// DPAD HORZONTAL
									if (ev.value == 1)
									{
										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 1;
										eventData.inputCode = G_DPAD_RIGHT_BTN;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
									else if (ev.value == -1)
									{
										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 1;
										eventData.inputCode = G_DPAD_LEFT_BTN;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
									else if (ev.value == 0)
									{
										LockSyncWrite();
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										eventData.controllerIndex = _controllerIndex;
										if (pControllers[_controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] != 0)
										{
											pControllers[_controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
											eventData.inputCode = G_DPAD_LEFT_BTN;
											eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
										}
										if (pControllers[_controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] != 0)
										{
											pControllers[_controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
											eventData.inputCode = G_DPAD_RIGHT_BTN;
											eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
										}
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
								}
								break;

								case ABS_HAT0Y:
								case ABS_HAT3Y:
								{
									// DPAD VERT
									if (ev.value == 1)
									{
										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 1;
										eventData.inputCode = G_DPAD_DOWN_BTN;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
									else if (ev.value == -1)
									{
										LockSyncWrite();
										pControllers[_controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 1;
										eventData.inputCode = G_DPAD_UP_BTN;
										eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_UP_BTN];
										eventData.controllerIndex = _controllerIndex;
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
									else if (ev.value == 0)
									{
										LockSyncWrite();
										eventData.controllerID = pControllers[_controllerIndex].controllerID;
										eventData.controllerIndex = _controllerIndex;
										if (pControllers[_controllerIndex].controllerInputs[G_DPAD_UP_BTN] != 0)
										{
											pControllers[_controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
											eventData.inputCode = G_DPAD_UP_BTN;
											eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_UP_BTN];
										}
										if (pControllers[_controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] != 0)
										{
											pControllers[_controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
											eventData.inputCode = G_DPAD_DOWN_BTN;
											eventData.inputValue = pControllers[_controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
										}
										UnlockSyncWrite();

										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
										Push(l_GEvent);
									}
								}
								break;
								}
							}
						}
						break;
						}
					}
					else
					{
						if (errno == EAGAIN)
							sleep(.001);
						else
							break;
					}
				}

				if (close(fd) == -1)
				{
					if (errno == EBADF)
						printf("fd isn't a valid open file descriptor\n");
					else if (errno == EINTR)
						printf("close call is interrupted by signal\n");
					else if (errno == EIO)
						printf("i/o error\n");
				}

				LockSyncWrite();
				pControllers[_controllerIndex].isConnected = 0;
				UnlockSyncWrite();

				eventData.controllerIndex = _controllerIndex;
				eventData.inputCode = 0;
				eventData.inputValue = 0;
				eventData.isConnected = 0;

				l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERDISCONNECTED, eventData);
				Push(l_GEvent);
			}

			void NotifyThreadEntryPoint()
			{
				EVENT_DATA eventData;
				int fd = 0;
				int wd = 0;
				struct G_inotify_event iev, base;
				int length = sizeof(struct G_inotify_event);
				base.len = 0;
				base.mask = 0;

				fd = inotify_init1(IN_NONBLOCK);
				wd = inotify_add_watch(fd, "/dev/input", IN_ATTRIB | IN_CREATE | IN_DELETE);

				while (isRunning)
				{
					iev = base;
					if (read(fd, &iev, length) != -1)
					{
						if (iev.len)
						{
							if (iev.mask & IN_ATTRIB || iev.mask & IN_CREATE)
							{
								if (!(iev.mask & IN_ISDIR))
								{
									char newFile[30];
									strcpy(newFile, "/dev/input/");
									strcat(newFile, iev.name);
									// check the type of file
									int evdevCheck = strncmp(iev.name, "event", 5);
									// check if file has a vaild number of inputs
									if (evdevCheck == 0)
									{
										int event_fd = open(newFile, O_RDONLY | O_NONBLOCK);
										if (event_fd > -1)
										{
											if (isGamepadBitSet(event_fd))
											{
												LockSyncWrite();
												int controllerIndex = FindEmptyControllerIndex(G_MAX_CONTROLLER_COUNT, pControllers);
												UnlockSyncWrite();
												if (controllerIndex != -1)
												{
													GW::INPUT::GControllerType controllerID;
													int codeMapping, axisMapping;
													GetControllerIDAndMappings(event_fd, controllerID, codeMapping, axisMapping);

													LockSyncWrite();
													pControllers[controllerIndex].controllerID = controllerID;
													pControllers[controllerIndex].codeMapping = codeMapping;
													pControllers[controllerIndex].axisMapping = axisMapping;
													isControllerThreadActive[controllerIndex] = true;
													for (int i = 0; i < 8; ++i)
														pControllers[controllerIndex].controllerFilePath[i] = iev.name[i];
													pControllers[controllerIndex].isConnected = 1;
													eventData.controllerID = pControllers[controllerIndex].controllerID;
													UnlockSyncWrite();

													controllerThreads[controllerIndex].Create(false);
													controllerThreads[controllerIndex].BranchSingular(std::bind(&GGeneralController::ControllerInputThreadEntryPoint, this, controllerIndex, event_fd));

													eventData.controllerIndex = controllerIndex;
													eventData.inputCode = 0;
													eventData.inputValue = 0;
													eventData.isConnected = 1;

													GW::GEvent l_GEvent;
													l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, eventData);
													Push(l_GEvent);
												}
											}
										}
									}
								}
							}
							else if (iev.mask & IN_DELETE)
							{
								if (!(iev.mask & IN_ISDIR))
								{
									for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
									{
										if (strncmp(pControllers[i].controllerFilePath, iev.name, 8) == 0)
										{
											isControllerThreadActive[i] = false;
											controllerThreads[i].Converge(0);
											controllerThreads[i] = nullptr;
										}
									}
								}
							}
						}
					}
				}

				close(fd);
				for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					if (isControllerThreadActive[i])
					{
						isControllerThreadActive[i] = false;
						controllerThreads[i].Converge(0);
						controllerThreads[i] = nullptr;
					}
				}
			}
		public:
			GGeneralController() {}

			virtual void Initialize()
			{
				isRunning = true;

				pControllers = new CONTROLLER_STATE[G_MAX_CONTROLLER_COUNT];
				deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
				deadZonePercentage = 0.2f;

				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					// Init the bools
					isControllerThreadActive[i] = false;

					pControllers[i].isConnected = 0;
					pControllers[i].isVibrating = 0;
					pControllers[i].vibrationDuration = 0;
					pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
					pControllers[i].maxInputs = G_MAX_GENERAL_INPUTS;
					pControllers[i].controllerInputs = new float[G_MAX_GENERAL_INPUTS];
					for (unsigned int j = 0; j < G_MAX_GENERAL_INPUTS; ++j)
					{
						pControllers[i].controllerInputs[j] = 0.0f;
					}
				}

				DIR* dir;
				dirent* fileData;
				EVENT_DATA eventData;

				if ((dir = opendir("/dev/input")) != NULL)
				{
					/* print all the files and directories within directory */
					while ((fileData = readdir(dir)) != NULL)
					{
						char newFile[30];
						strcpy(newFile, "/dev/input/");
						strcat(newFile, fileData->d_name);
						// check the type of file
						int evdevCheck = strncmp(fileData->d_name, "event", 5);
						// check if file has a vaild number of inputs
						if (evdevCheck == 0)
						{
							int event_fd = open(newFile, O_RDONLY | O_NONBLOCK);
							if (event_fd > -1)
							{
								if (isGamepadBitSet(event_fd))
								{
									LockAsyncRead();
									int controllerIndex = FindEmptyControllerIndex(G_MAX_CONTROLLER_COUNT, pControllers);
									UnlockAsyncRead();
									if (controllerIndex != -1)
									{
										GW::INPUT::GControllerType controllerID;
										int codeMapping, axisMapping;
										GetControllerIDAndMappings(event_fd, controllerID, codeMapping, axisMapping);

										LockSyncWrite();
										isControllerThreadActive[controllerIndex] = true;
										for (int i = 0; i < 8; ++i)
											pControllers[controllerIndex].controllerFilePath[i] = fileData->d_name[i];
										pControllers[controllerIndex].controllerID = controllerID;
										pControllers[controllerIndex].codeMapping = codeMapping;
										pControllers[controllerIndex].axisMapping = axisMapping;
										pControllers[controllerIndex].isConnected = 1;
										eventData.controllerID = pControllers[controllerIndex].controllerID;
										UnlockSyncWrite();

										controllerThreads[controllerIndex].Create(false);
										controllerThreads[controllerIndex].BranchSingular(std::bind(&GGeneralController::ControllerInputThreadEntryPoint, this, controllerIndex, event_fd));

										eventData.controllerIndex = controllerIndex;
										eventData.inputCode = 0;
										eventData.inputValue = 0;
										eventData.isConnected = 1;

										GW::GEvent l_GEvent;
										l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, eventData);
										Push(l_GEvent);
									}
								}
							}
						}
					}
					closedir(dir);
				}

				notifyThread.Create(false);
				notifyThread.BranchSingular(std::bind(&GGeneralController::NotifyThreadEntryPoint, this));
			}

			virtual void Release()
			{
				isRunning = false;

				for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
					if (isControllerThreadActive[i])
					{
						isControllerThreadActive[i] = false;
						controllerThreads[i].Converge(0);
						controllerThreads[i] = nullptr;
					}

				notifyThread.Converge(0);

				for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					delete[] pControllers[i].controllerInputs;
					delete pControllers[i].vibrationStartTime;
				}
				delete[] pControllers;
			}

			virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override
			{
				if (_controllerIndex > G_MAX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode >= G_MAX_GENERAL_INPUTS)
					return GReturn::INVALID_ARGUMENT;
				if (pControllers[_controllerIndex].isConnected == 0)
					return GReturn::FAILURE;
				LockAsyncRead();
				_outState = pControllers[_controllerIndex].controllerInputs[(_inputCode)];
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override
			{
				if (_controllerIndex > G_MAX_CONTROLLER_INDEX)
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
				UnlockAsyncRead();

				return GReturn::SUCCESS;
			}

			virtual GReturn GetMaxIndex(int& _outMax) override
			{
				_outMax = G_MAX_CONTROLLER_INDEX;
				return GReturn::SUCCESS;
			}

			virtual GReturn GetNumConnected(int& _outConnectedCount) override
			{
				_outConnectedCount = 0;
				LockAsyncRead();
				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isConnected)
						++_outConnectedCount;
				}
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override
			{
				if (_deadzonePercentage > 1.0f || _deadzonePercentage < 0.0f)
					return GReturn::INVALID_ARGUMENT;
				LockSyncWrite();
				deadZoneType = _type;
				deadZonePercentage = _deadzonePercentage;
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			// General controllers do not support these features because it is used cross-platform as a "general" controller
			virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override { return GReturn::FEATURE_UNSUPPORTED; }
			virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override { return GReturn::FEATURE_UNSUPPORTED; }
			virtual GReturn StopVibration(unsigned int _controllerIndex) override { return GReturn::FEATURE_UNSUPPORTED; }
			virtual GReturn StopAllVibrations() override { return GReturn::FEATURE_UNSUPPORTED; }

			GReturn Register(CORE::GEventCache _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
			GReturn Register(CORE::GEventResponder _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override final { return GEventGeneratorImplementation::Register(_observer, _callback); }
			GReturn Deregister(CORE::GInterface _observer) override final { return GEventGeneratorImplementation::Deregister(_observer); }
			GReturn Observers(unsigned int& _outCount) const override final { return GEventGeneratorImplementation::Observers(_outCount); }
			GReturn Push(const GEvent& _newEvent) override final { return GEventGeneratorImplementation::Push(_newEvent); }
		};

		inline GControllerImplementation::~GControllerImplementation()
		{
			if (pController)
			{
				pController->Release();
				delete pController;
				pController = nullptr;
			}
		}

		inline GReturn GControllerImplementation::Create()
		{
			pController = new GGeneralController();
			pController->Initialize();

			return GReturn::SUCCESS;
		}
		inline GReturn GControllerImplementation::GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) { return pController ? pController->GetState(_controllerIndex, _inputCode, _outState) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) { return pController ? pController->IsConnected(_controllerIndex, _outIsConnected) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetMaxIndex(int& _outMax) { return pController ? pController->GetMaxIndex(_outMax) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetNumConnected(int& _outConnectedCount) { return pController ? pController->GetNumConnected(_outConnectedCount) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) { return pController ? pController->SetDeadZone(_type, _deadzonePercentage) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) { return pController ? pController->StartVibration(_controllerIndex, _pan, _duration, _strength) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) { return pController ? pController->IsVibrating(_controllerIndex, _outIsVibrating) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopVibration(unsigned int _controllerIndex) { return pController ? pController->StopVibration(_controllerIndex) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopAllVibrations() { return pController ? pController->StopAllVibrations() : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::Register(CORE::GEventCache _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
		inline GReturn GControllerImplementation::Register(CORE::GEventResponder _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
		inline GReturn GControllerImplementation::Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) { return pController ? pController->Register(_observer, _callback) : GEventGeneratorImplementation::Register(_observer, _callback); }
		inline GReturn GControllerImplementation::Deregister(CORE::GInterface _observer) { return pController ? pController->Deregister(_observer) : GEventGeneratorImplementation::Deregister(_observer); }
		inline GReturn GControllerImplementation::Observers(unsigned int& _outCount) const { return pController ? pController->Observers(_outCount) : GEventGeneratorImplementation::Observers(_outCount); }
		inline GReturn GControllerImplementation::Push(const GEvent& _newEvent) { return pController ? pController->Push(_newEvent) : GEventGeneratorImplementation::Push(_newEvent); }
	} // end I namespace
} // end GW namespace
