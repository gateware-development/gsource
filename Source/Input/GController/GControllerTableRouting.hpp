#ifndef GCONTROLLERTABLEROUTING_HPP
#define GCONTROLLERTABLEROUTING_HPP

/*!
	Purpose: Provides macros to GController
	Author: Devin Wright
	Contributors: Kai Huang, Lari Norri, Ozzie Mercado, Alexander Cusaac
	Last Modified: 10/02/2020
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#define G_MAX_CONTROLLER_COUNT 16
#define G_MAX_XBOX_CONTROLLER_COUNT 4
#define G_MAX_XBOX_CONTROLLER_COUNT_XBOXONE 8
#define G_MAX_CONTROLLER_INDEX (G_MAX_CONTROLLER_COUNT - 1)
#define G_MAX_XBOX_CONTROLLER_INDEX (G_MAX_XBOX_CONTROLLER_COUNT - 1)
#define G_MAX_XBOX_CONTROLLER_INDEX_XBOXONE (G_MAX_XBOX_CONTROLLER_COUNT_XBOXONE - 1)
#define G_MAX_GENERAL_INPUTS 20
#define G_MAX_XBOX_INPUTS 20
#define G_MAX_PS4_INPUTS 20
#define G_MAX_XBOX_THUMB_AXIS 32768
#define G_MIN_XBOX_THUMB_AXIS -32768
#define G_MAX_XBOX_TRIGGER_AXIS 255
#define G_XINPUT_MAX_VIBRATION 65535
#define G_MAX_LINUX_THUMB_AXIS 32768
#define G_MAX_GENERAL_TRIGGER_AXIS 255
#define G_GENERAL_TRIGGER_THRESHOLD 51

#define G_MAX_AXIS 0
#define G_MIN_AXIS 1

#define G_SONY_VENDOR_ID 0x054C
#define G_MICROSOFT_VENDOR_ID 0x045E

#define G_CODE_MAPPING_GENERAL 0
#define G_CODE_MAPPING_PS4_WIRED 1
#define G_CODE_MAPPING_PS4_WIRELESS 1
#define G_CODE_MAPPING_XBOX360 0
#define G_CODE_MAPPING_XBOXONE_WIRED 0
#define G_CODE_MAPPING_XBOXONE_WIRELESS 2

#define G_MAC_AXIS_MAPPING_GENERAL 0
#define G_MAC_AXIS_MAPPING_PS4_WIRED 2
#define G_MAC_AXIS_MAPPING_PS4_WIRELESS 0
#define G_MAC_AXIS_MAPPING_XBOX360 1
#define G_MAC_AXIS_MAPPING_XBOXONE_WIRED 1
#define G_MAC_AXIS_MAPPING_XBOXONE_WIRELESS 3

#define G_LINUX_AXIS_MAPPING_GENERAL 0
#define G_LINUX_AXIS_MAPPING_PS4_WIRED 0
#define G_LINUX_AXIS_MAPPING_PS4_WIRELESS 0
#define G_LINUX_AXIS_MAPPING_XBOX360 1
#define G_LINUX_AXIS_MAPPING_XBOXONE_WIRED 3
#define G_LINUX_AXIS_MAPPING_XBOXONE_WIRELESS 2

namespace GW
{
	namespace I
	{
		// Routes GController inputs (defined in GInputDefines.h) to the corresponding column in ControllerAxisRangesMin/Max tables.
		constexpr int ControllerAxisOffsets[] =
		{
			-1, // 0
			-1, // 1
			-1, // 2
			-1, // 3
			-1, // 4
			-1, // 5
			 0, // 6 G_LEFT_TRIGGER_AXIS
			 1, // 7 G_RIGHT_TRIGGER_AXIS
			-1, // 8
			-1, // 9
			-1, // 10
			-1, // 11
			-1, // 12
			-1, // 13
			-1, // 14
			-1, // 15
			 2, // 16 G_LX_AXIS
			 3, // 17 G_LY_AXIS
			 4, // 18 G_RX_AXIS
			 5  // 19 G_RY_AXIS
		};


		// Mac tables

		constexpr float Mac_ControllerAxisRangesMax[][6] =
		{
			//G_LEFT_TRIGGER_AXIS   G_RIGHT_TRIGGER_AXIS   G_LX_AXIS   G_LY_AXIS   G_RX_AXIS   G_RY_AXIS
			{ 255.0f,               255.0f,                255.0f,     255.0f,     255.0f,     255.0f    }, // General & PS4 Wireless
			{ 255.0f,               255.0f,                32767.0f,   32767.0f,   32767.0f,   32767.0f  }, // Xbox 360 & Xbox One Wired
			{ 315.0f,               315.0f,                255.0f,     255.0f,     255.0f,     255.0f    }, // PS4 Wired
			{ 1023.0,               1023.0,                65535.0f,   65535.0f,   65535.0f,   65535.0f  }, // Xbox One Wireless
		};

		constexpr float Mac_ControllerAxisRangesMin[][6] =
		{
			//G_LEFT_TRIGGER_AXIS   G_RIGHT_TRIGGER_AXIS   G_LX_AXIS   G_LY_AXIS   G_RX_AXIS   G_RY_AXIS
			{ 0.0f,                 0.0f,                  0.0f,       0.0f,       0.0f,       0.0f      }, // General & PS4 Wireless
			{ 255.0f,               255.0f,                -32768.0f,  -32768.0f,  -32768.0f,  -32768.0f }, // Xbox 360 & Xbox One Wired
			{ 0.0f,                 0.0f,                  0.0f,       0.0f,       0.0f,       0.0f      }, // PS4 Wired
			{ 0.0f,                 0.0f,                  0.0f,       0.0f,       0.0f,       0.0f      }, // Xbox One Wireless
		};

		// Routes usage codes (defined in IOHIDUsageTables.h on Mac) to GController input.
		constexpr unsigned int Mac_ControllerCodes[][3] =
		{
			//General, Xbox 360 & Xbox One Wired      PS4                    Xbox One Wireless
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 0
			{ G_SOUTH_BTN,                            G_WEST_BTN,            G_SOUTH_BTN            }, // 1
			{ G_EAST_BTN,                             G_SOUTH_BTN,           G_EAST_BTN             }, // 2
			{ G_WEST_BTN,                             G_EAST_BTN,            G_UNKNOWN_INPUT        }, // 3
			{ G_NORTH_BTN,                            G_NORTH_BTN,           G_WEST_BTN             }, // 4
			{ G_LEFT_SHOULDER_BTN,                    G_LEFT_SHOULDER_BTN,   G_NORTH_BTN            }, // 5
			{ G_RIGHT_SHOULDER_BTN,                   G_RIGHT_SHOULDER_BTN,  G_UNKNOWN_INPUT        }, // 6
			{ G_LEFT_THUMB_BTN,                       G_UNKNOWN_INPUT,       G_LEFT_SHOULDER_BTN    }, // 7
			{ G_RIGHT_THUMB_BTN,                      G_UNKNOWN_INPUT,       G_RIGHT_SHOULDER_BTN   }, // 8
			{ G_START_BTN,                            G_SELECT_BTN,          G_UNKNOWN_INPUT        }, // 9
			{ G_SELECT_BTN,                           G_START_BTN,           G_UNKNOWN_INPUT        }, // 10
			{ G_UNKNOWN_INPUT,                        G_LEFT_THUMB_BTN,      G_UNKNOWN_INPUT        }, // 11
			{ G_DPAD_UP_BTN,                          G_RIGHT_THUMB_BTN,     G_START_BTN            }, // 12
			{ G_DPAD_DOWN_BTN,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 13
			{ G_DPAD_LEFT_BTN,                        G_UNKNOWN_INPUT,       G_LEFT_THUMB_BTN       }, // 14
			{ G_DPAD_RIGHT_BTN,                       G_UNKNOWN_INPUT,       G_RIGHT_THUMB_BTN      }, // 15
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 16
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 17
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 18
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 19
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 20
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 21
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 22
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 23
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 24
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 25
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 26
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 27
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 28
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 29
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 30
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 31
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 32
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 33
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 34
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 35
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 36
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 37
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 38
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 39
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 40
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 41
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 42
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 43
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 44
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 45
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 46
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 47
			{ G_LX_AXIS,                              G_LX_AXIS,             G_LX_AXIS              }, // 48
			{ G_LY_AXIS,                              G_LY_AXIS,             G_LY_AXIS              }, // 49
			{ G_LEFT_TRIGGER_AXIS,                    G_RX_AXIS,             G_RX_AXIS              }, // 50
			{ G_RX_AXIS,                              G_LEFT_TRIGGER_AXIS,   G_UNKNOWN_INPUT        }, // 51
			{ G_RY_AXIS,                              G_RIGHT_TRIGGER_AXIS,  G_UNKNOWN_INPUT        }, // 52
			{ G_RIGHT_TRIGGER_AXIS,                   G_RY_AXIS,             G_RY_AXIS              }, // 53
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 54
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 55
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 56
			{ G_UNKNOWN_INPUT,                        G_DPAD_LEFT_BTN,       G_DPAD_LEFT_BTN        }, // 57 /* Dpad value reported as 0-360, 0-8, or 0-315 & -45 */
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 58
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 59
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 60
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 61
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 62
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 63
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 64
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 65
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 66
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 67
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 68
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 69
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 70
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 71
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 72
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 73
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 74
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 75
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 76
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 77
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 78
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 79
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 80
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 81
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 82
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 83
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 84
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 85
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 86
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 87
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 88
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 89
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 90
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 91
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 92
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 93
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 94
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 95
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 96
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 97
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 98
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 99
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 100
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 101
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 102
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 103
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 104
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 105
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 106
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 107
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 108
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 109
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 110
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 111
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 112
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 113
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 114
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 115
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 116
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 117
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 118
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 119
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 120
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 121
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 122
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 123
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 124
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 125
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 126
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 127
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 128
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 129
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 130
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 131
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 132
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 133
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 134
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 135
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 136
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 137
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 138
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 139
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 140
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 141
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 142
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 143
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 144
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 145
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 146
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 147
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 148
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 149
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 150
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 151
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 152
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 153
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 154
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 155
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 156
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 157
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 158
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 159
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 160
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 161
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 162
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 163
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 164
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 165
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 166
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 167
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 168
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 169
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 170
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 171
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 172
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 173
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 174
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 175
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 176
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 177
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 178
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 179
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 180
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 181
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 182
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 183
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 184
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 185
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 186
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 187
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 188
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 189
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 190
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 191
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 192
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 193
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 194
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 195
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_RIGHT_TRIGGER_AXIS   }, // 196
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_LEFT_TRIGGER_AXIS    }, // 197
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 198
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 199
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 200
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 201
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 202
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 203
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 204
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 205
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 206
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 207
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 208
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 209
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 210
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 211
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 212
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 213
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 214
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 215
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 216
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 217
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 218
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 219
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 220
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 221
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 222
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 223
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 224
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 225
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 226
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 227
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 228
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 229
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 230
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 231
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 232
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 233
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 234
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 235
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 236
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 237
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 238
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 239
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 240
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 241
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 242
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 243
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 244
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 245
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 246
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 247
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 248
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 249
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 250
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 251
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 252
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 253
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 254
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 255
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 256
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 257
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 258
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 259
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 260
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 261
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 262
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 263
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 264
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 265
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 266
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 267
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 268
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 269
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 270
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 271
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 272
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 273
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 274
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 275
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 276
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 277
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 278
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 279
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 280
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 281
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 282
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 283
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 284
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 285
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 286
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 287
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 288
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 289
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 290
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 291
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 292
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 293
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 294
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 295
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 296
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 297
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 298
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 299
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 300
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 301
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 302
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 303
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 304
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 305
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 306
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 307
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 308
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 309
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 310
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 311
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 312
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 313
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 314
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 315
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 316
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 317
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 318
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 319
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 320
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 321
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 322
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 323
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 324
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 325
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 326
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 327
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 328
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 329
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 330
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 331
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 332
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 333
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 334
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 335
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 336
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 337
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 338
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 339
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 340
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 341
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 342
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 343
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 344
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 345
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 346
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 347
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 348
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 349
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 350
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 351
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 352
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 353
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 354
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 355
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 356
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 357
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 358
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 359
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 360
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 361
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 362
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 363
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 364
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 365
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 366
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 367
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 368
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 369
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 370
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 371
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 372
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 373
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 374
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 375
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 376
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 377
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 378
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 379
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 380
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 381
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 382
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 383
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 384
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 385
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 386
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 387
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 388
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 389
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 390
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 391
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 392
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 393
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 394
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 395
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 396
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 397
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 398
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 399
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 400
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 401
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 402
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 403
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 404
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 405
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 406
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 407
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 408
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 409
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 410
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 411
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 412
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 413
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 414
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 415
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 416
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 417
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 418
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 419
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 420
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 421
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 422
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 423
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 424
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 425
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 426
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 427
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 428
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 429
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 430
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 431
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 432
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 433
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 434
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 435
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 436
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 437
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 438
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 439
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 440
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 441
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 442
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 443
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 444
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 445
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 446
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 447
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 448
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 449
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 450
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 451
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 452
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 453
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 454
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 455
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 456
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 457
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 458
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 459
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 460
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 461
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 462
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 463
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 464
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 465
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 466
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 467
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 468
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 469
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 470
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 471
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 472
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 473
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 474
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 475
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 476
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 477
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 478
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 479
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 480
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 481
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 482
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 483
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 484
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 485
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 486
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 487
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 488
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 489
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 490
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 491
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 492
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 493
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 494
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 495
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 496
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 497
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 498
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 499
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 500
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 501
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 502
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 503
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 504
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 505
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 506
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 507
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 508
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 509
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 510
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 511
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 512
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 513
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 514
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 515
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 516
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 517
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 518
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 519
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 520
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 521
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 522
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 523
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 524
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 525
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 526
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 527
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 528
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 529
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 530
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 531
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 532
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 533
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 534
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 535
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 536
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 537
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 538
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 539
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 540
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 541
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 542
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 543
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 544
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 545
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 546
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT        }, // 547
			{ G_UNKNOWN_INPUT,                        G_UNKNOWN_INPUT,       G_SELECT_BTN           }, // 548
		};


		// Linux tables

		constexpr float Linux_ControllerAxisRangesMax[][6] =
		{
			//G_LEFT_TRIGGER_AXIS   G_RIGHT_TRIGGER_AXIS   G_LX_AXIS   G_LY_AXIS   G_RX_AXIS   G_RY_AXIS
			{ 255.0f,               255.0f,                255.0f,     255.0f,     255.0f,     255.0f    }, // General, PS4 Wired & PS4 Wireless
			{ 255.0f,               255.0f,                32767.0f,   32767.0f,   32767.0f,   32767.0f  }, // Xbox 360
			{ 1023.0,               1023.0,                65535.0f,   65535.0f,   65535.0f,   65535.0f  }, // Xbox One Wireless
			{ 1023.0,               1023.0,                32767.0f,   32767.0f,   32767.0f,   32767.0f  }, // Xbox One Wired
		};

		constexpr float Linux_ControllerAxisRangesMin[][6] =
		{
			//G_LEFT_TRIGGER_AXIS   G_RIGHT_TRIGGER_AXIS   G_LX_AXIS   G_LY_AXIS   G_RX_AXIS   G_RY_AXIS
			{ 0.0f,                 0.0f,                  0.0f,       0.0f,       0.0f,       0.0f      }, // General, PS4 Wired & PS4 Wireless
			{ 255.0f,               255.0f,                -32768.0f,  -32768.0f,  -32768.0f,  -32768.0f }, // Xbox 360 & Xbox One Wired
			{ 0.0f,                 0.0f,                  0.0f,       0.0f,       0.0f,       0.0f      }, // Xbox One Wireless
			{ 0.0f,                 0.0f,                  -32768.0f,  -32768.0f,  -32768.0f,  -32768.0f }, // Xbox One Wired
		};

		// Routes input_event codes (defined in input-event-codes.h on Linux) for different axes to GController axis input (defined in GInputDefines.h)
		constexpr unsigned int Linux_ControllerAxisCodes[][3] =
		{
			//General, Xbox 360 & Xbox One Wired    PS4                    Xbox One Wireless
			{ G_LX_AXIS,                            G_LX_AXIS,             G_LX_AXIS            }, // 0	ABS_X
			{ G_LY_AXIS,                            G_LY_AXIS,             G_LY_AXIS            }, // 1 ABS_Y
			{ G_LEFT_TRIGGER_AXIS,                  G_LEFT_TRIGGER_AXIS,   G_RX_AXIS            }, // 2 ABS_Z
			{ G_RX_AXIS,                            G_RX_AXIS,             G_UNKNOWN_INPUT      }, // 3 ABS_RX
			{ G_RY_AXIS,                            G_RY_AXIS,             G_UNKNOWN_INPUT      }, // 4 ABS_RY
			{ G_RIGHT_TRIGGER_AXIS,                 G_RIGHT_TRIGGER_AXIS,  G_RY_AXIS            }, // 5 ABS_RZ
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT      }, // 6 ABS_THROTTLE
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT      }, // 7 ABS_RUDDER
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT      }, // 8 ABS_WHEEL
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_RIGHT_TRIGGER_AXIS }, // 9 ABS_GAS
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_LEFT_TRIGGER_AXIS  }, // 10 ABS_BRAKE
		};

		// Routes input_event codes (from KEY_BACK to BTN_THUMBR) to GController button input.
		constexpr unsigned int Linux_ControllerCodes[][3] =
		{
			//General, Xbox 360 & Xbox One Wired    PS4                    Xbox One Wireless
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_SELECT_BTN             }, // 0
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 1
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 2
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 3
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 4
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 5
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 6
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 7
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 8
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 9
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 10
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 11
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 12
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 13
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 14
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 15
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 16
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 17
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 18
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 19
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 20
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 21
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 22
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 23
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 24
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 25
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 26
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 27
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 28
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 29
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 30
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 31
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 32
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 33
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 34
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 35
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 36
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 37
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 38
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 39
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 40
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 41
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 42
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 43
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 44
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 45
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 46
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 47
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 48
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 49
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 50
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 51
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 52
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 53
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 54
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 55
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 56
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 57
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 58
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 59
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 60
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 61
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 62
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 63
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 64
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 65
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 66
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 67
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 68
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 69
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 70
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 71
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 72
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 73
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 74
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 75
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 76
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 77
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 78
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 79
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 80
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 81
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 82
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 83
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 84
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 85
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 86
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 87
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 88
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 89
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 90
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 91
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 92
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 93
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 94
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 95
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 96
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 97
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 98
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 99
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 100
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 101
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 102
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 103
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 104
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 105
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 106
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 107
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 108
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 109
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 110
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 111
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 112
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 113
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 114
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 115
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 116
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 117
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 118
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 119
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 120
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 121
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 122
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 123
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 124
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 125
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 126
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 127
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 128
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 129
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 130
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 131
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 132
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 133
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 134
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 135
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 136
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 137
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 138
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 139
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 140
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 141
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 142
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 143
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 144
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 145
			{ G_SOUTH_BTN,                          G_SOUTH_BTN,           G_SOUTH_BTN              }, // 146
			{ G_EAST_BTN,                           G_EAST_BTN,            G_EAST_BTN               }, // 147
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 148
			{ G_WEST_BTN,                           G_NORTH_BTN,           G_WEST_BTN               }, // 149
			{ G_NORTH_BTN,                          G_WEST_BTN,            G_NORTH_BTN              }, // 150
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 151
			{ G_LEFT_SHOULDER_BTN,                  G_LEFT_SHOULDER_BTN,   G_LEFT_SHOULDER_BTN      }, // 152
			{ G_RIGHT_SHOULDER_BTN,                 G_RIGHT_SHOULDER_BTN,  G_RIGHT_SHOULDER_BTN     }, // 153
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 154
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 155
			{ G_SELECT_BTN,                         G_SELECT_BTN,          G_UNKNOWN_INPUT          }, // 156
			{ G_START_BTN,                          G_START_BTN,           G_START_BTN              }, // 157
			{ G_UNKNOWN_INPUT,                      G_UNKNOWN_INPUT,       G_UNKNOWN_INPUT          }, // 158
			{ G_LEFT_THUMB_BTN,                     G_LEFT_THUMB_BTN,      G_LEFT_THUMB_BTN         }, // 159
			{ G_RIGHT_THUMB_BTN,                    G_RIGHT_THUMB_BTN,     G_RIGHT_THUMB_BTN        }, // 160
		};
	}
}
#endif
