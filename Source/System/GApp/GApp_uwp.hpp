#include <iostream>
#include <vector>
#include <sstream>
#include <windows.h>
#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.ApplicationModel.Activation.h>
#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.UI.ViewManagement.h>

namespace internal_gw
{
	struct GAPP_GLOBAL
	{
		std::mutex sleeper;
		std::condition_variable started;
		bool g_previouslyTerminated;
	};
	static GAPP_GLOBAL& GAppGlobal()
	{
		static GAPP_GLOBAL appData;
		return appData;
	}
}

namespace GW
{
	namespace I
	{
		// Allows C++/WinRT UWP application to run normally
		struct WinRTApp : winrt::implements<WinRTApp,
			winrt::Windows::ApplicationModel::Core::IFrameworkViewSource,
			winrt::Windows::ApplicationModel::Core::IFrameworkView>
		{
			winrt::Windows::ApplicationModel::Core::IFrameworkView CreateView() {
				return *this;
			}
			void Initialize(winrt::Windows::ApplicationModel::Core::CoreApplicationView const& applicationView)
			{
				applicationView.Activated({ this, &WinRTApp::OnActivated });
			}
			void Load(winrt::hstring const&) {}
			void Uninitialize() {}
			void Run() // run & manage the UWP window until main has ended
			{
				// window events & details are detected and posted by the GWindow library
				winrt::Windows::UI::Core::CoreWindow window =
					winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
				// set min window size to lowest possible value to allow user the freedom to set it as small as they can
				winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView().SetPreferredMinSize(
					winrt::Windows::Foundation::Size(192, 48));
				//window.Activate();
				winrt::Windows::UI::Core::CoreDispatcher dispatcher = window.Dispatcher();
				// notifies conditional of app start to prevent desktop main from running before the app initializes.
				internal_gw::GAppGlobal().started.notify_all();
				dispatcher.ProcessEvents(
					winrt::Windows::UI::Core::CoreProcessEventsOption::ProcessUntilQuit);
			}
			void SetWindow(winrt::Windows::UI::Core::CoreWindow const& window)
			{
				winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView().
					PreferredLaunchWindowingMode(winrt::Windows::UI::ViewManagement::ApplicationViewWindowingMode::PreferredLaunchViewSize);
				
				auto navigation = winrt::Windows::UI::Core::SystemNavigationManager::GetForCurrentView();

				// UWP on Xbox One triggers a back request whenever the B button is pressed
				// which can result in the app being suspended if unhandled
				navigation.BackRequested([](winrt::Windows::Foundation::IInspectable const&,
					winrt::Windows::UI::Core::BackRequestedEventArgs const& args)
					{
						args.Handled(true);
					});
			}
			void OnActivated(winrt::Windows::ApplicationModel::Core::CoreApplicationView const& view,
				winrt::Windows::ApplicationModel::Activation::IActivatedEventArgs const& args)
			{
				auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
				window.Activate();

				// fixes weird issue where title bar is absent at app launch dispute what window style is chosen
				view.TitleBar().ExtendViewIntoTitleBar(false);

				if (args.PreviousExecutionState() == winrt::Windows::ApplicationModel::Activation::ApplicationExecutionState::Terminated)
				{
					internal_gw::GAppGlobal().g_previouslyTerminated = true;
				}
			}
		};
		// This class needs to be credited to the original post and made gateware compliant
		// https://web.archive.org/web/20140825203329/http://blog.tomaka17.com/2011/07/redirecting-cerr-and-clog-to-outputdebugstring/ link to class
		template<typename TChar, typename TTraits = std::char_traits<TChar>>
		class OutputDebugStringBuf : public std::basic_stringbuf<TChar, TTraits>
		{
		public:
			typedef std::basic_stringbuf<TChar, TTraits> BaseClass;

			explicit OutputDebugStringBuf() : _buffer(256)
			{
				setg(nullptr, nullptr, nullptr);
				setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());
			}

			static_assert(std::is_same<TChar, char>::value || std::is_same<TChar, wchar_t>::value,
				"OutputDebugStringBuf only supports char and wchar_t types");

			int sync() override try {
				MessageOutputer<TChar, TTraits>()(pbase(), pptr());
				setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());;
				return 0;
			}
			catch (...) {
				return -1;
			}
			typename BaseClass::int_type overflow(typename BaseClass::int_type c = TTraits::eof()) override
			{
				auto syncRet = sync();
				if (c != TTraits::eof())
				{
					_buffer[0] = c;
					setp(_buffer.data(), _buffer.data() + 1, _buffer.data() + _buffer.size());
				}
				return syncRet == -1 ? TTraits::eof() : 0;
			}

		private:
			std::vector<TChar> _buffer;
			template<typename TChar, typename TTraits>
			struct MessageOutputer;
			template<>
			struct MessageOutputer<char, std::char_traits<char>>
			{
				template<typename TIterator>
				void operator()(TIterator begin, TIterator end) const
				{
					std::string s(begin, end);
					OutputDebugStringA(s.c_str());
				}
			};
			template<>
			struct MessageOutputer<wchar_t, std::char_traits<wchar_t>> {
				template<typename TIterator>
				void operator()(TIterator begin, TIterator end) const {
					std::wstring s(begin, end);
					OutputDebugStringW(s.c_str());
				}
			};
		};
		// this function is unique to the GApp
		class GAppImplementation : public GAppInterface
		{
			GW::SYSTEM::GConcurrent run; // make global perhaps
		public:
			GReturn Create(std::function<int()> _desktopMainFunction)
			{
				GReturn result = GReturn::FAILURE;
				// ensure we have a valid main function and GConcurrent library
				if (_desktopMainFunction == nullptr || -run.Create(true))
					return result;
				// redirects cout to the output
				static OutputDebugStringBuf<char> outputDebugBufChar;
				std::cout.rdbuf(&outputDebugBufChar);
				 // pass below function by value
				result = run.BranchSingular([_desktopMainFunction] {
					// block execution of main function until UWP app has successfully started
					{
						std::unique_lock<std::mutex> locker(internal_gw::GAppGlobal().sleeper);
						internal_gw::GAppGlobal().started.wait(locker);
					}
					
					int ret = _desktopMainFunction(); // run the unified code base
					std::cout << "GApp: desktop main() routine returned " << ret << std::endl;
					// send a close message so our app window know to close down
					winrt::Windows::ApplicationModel::Core::CoreApplication::Exit();
					});
				return result; // did it work?
			}

			static GReturn PreviousExecutionStateWasTerminated(bool& _outWasTerminated)
			{
				_outWasTerminated = internal_gw::GAppGlobal().g_previouslyTerminated;

				return GReturn::SUCCESS;
			}

			// in case we need to debug program shutdown
			~GAppImplementation() {	run.Converge(0); }
		};
	}
}
// If you are providing your own wWinMain then be sure to:
// #define GATEWARE_DISABLE_GAPP // before including gateware.h
int wWinMain(HINSTANCE, HINSTANCE, PWSTR, int)
{
	winrt::Windows::ApplicationModel::Core::CoreApplication::Run(
		winrt::make<GW::I::WinRTApp>());
	return 0;
}
