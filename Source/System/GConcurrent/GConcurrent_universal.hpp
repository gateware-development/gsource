// Link to our internal thread pool
#include "gw_internal_threadpool.hpp"
#include <vector>

// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {
		// Just like GEventReceiver except it can store multiple events in a queue so you can delay processing
		class GConcurrentImplementation : public virtual GConcurrentInterface
		{
			bool suppressEvents = false;
			std::atomic_uint tasksProcessing;  
			std::atomic_uint64_t taskSubmissionIndex;
			// This is flag is only false when tasksProcessing == 0
			// It is used to efficiently spin-lock during a Converge operation
			std::atomic_flag working = ATOMIC_FLAG_INIT;
			// HAS A relationship allows for safe lifetime access in external threads
			CORE::GEventGenerator generator;
			// this class creates a message when it falls out of scope (uses shared_ptr)
			// solves the issue on how to notify when all parallel threads complete
			struct ScopedEvent {
				EVENT_DATA info = { 0, 0, { 0,1 } };
				CORE::GEventGenerator me; // safe even if proxied object is destructed
				std::chrono::time_point<std::chrono::steady_clock> start;
				~ScopedEvent() {
					GEvent send; // what people are listening for
					auto end = std::chrono::steady_clock::now();
					info.microsecondsElapsed = 
						std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
					if (+send.Write(Events::PARALLEL_TASK_COMPLETE, info))
						me.Push(send); // Notify anyone who is listening the Parallel Task has completed
				}
			};
			
		public:
			// required for HAS-A relationship
			GReturn Register(CORE::GEventCache _observer) override {
				return generator.Register(_observer);
			}
			GReturn Register(CORE::GEventResponder _observer) override {
				return generator.Register(_observer);
			}
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
				return generator.Register(_observer, _callback);
			}
			GReturn Deregister(CORE::GInterface _observer) override {
				return generator.Deregister(_observer);
			}
			GReturn Observers(unsigned int& _outCount) const override {
				return generator.Observers(_outCount);
			}
			GReturn Push(const GEvent& _newEvent) override {
				return generator.Push(_newEvent);
			}
			// actual implementation starts here
			GReturn Create(bool _supressEvents)
			{
				suppressEvents = _supressEvents;
				tasksProcessing = 0;
				taskSubmissionIndex = 0;
				return generator.Create();
			}
			GReturn BranchSingular(std::function<void()> _singleTask) override
			{
				if (_singleTask == nullptr)
					return GReturn::INVALID_ARGUMENT;
				EVENT_DATA einfo = { 0, 0, { 0,1 } };
				std::chrono::time_point<std::chrono::steady_clock> start;
				++tasksProcessing; // as soon as a task is submitted it is considered to be processing
				if (tasksProcessing == 1) // acquire "working" lock until no tasks are processing anymore
					std::atomic_flag_test_and_set_explicit(&working, std::memory_order_acquire);
				// send events to end users?
				if (suppressEvents == false)
				{
					start = std::chrono::steady_clock::now();
					einfo.taskSubmissionIndex = ++taskSubmissionIndex;
				}
				// Safe transfer of event suppression state.
				bool suppress = suppressEvents;
				// because "singleTask" below could theoretically invalidate "this" we use a safe handle 
				CORE::GEventGenerator safe = generator;
				// We identify each job from this GConcurrent with our memory address.
				// this helps us avoid deadlock scenarios when using the "Converge" routine.
				unsigned long long jobID = reinterpret_cast<unsigned long long>(this);
				// we need to send by reference due to "tasksProcessing" being a class member.
				// The task itself & a few other items must be copied as they will fall out of scope.
				internal_gw::GatewareThreadPool().AddJob([&, _singleTask, suppress, safe,
															einfo, start]() mutable {
					_singleTask(); // execute job while within the thread pool
					// send any event that still must be sent
					if (suppress == false)
					{
						GEvent send; // what people are listening for
						auto end = std::chrono::steady_clock::now();
						einfo.microsecondsElapsed =
							std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
						// Notify anyone who is listening
						send.Write(Events::SINGULAR_TASK_COMPLETE, einfo);
						safe.Push(send);
					}
					// increase count, notify listeners and unlock spin if still possible
					CORE::GEventGenerator::burst_w alive = *safe;
					if (alive)
					{
						--tasksProcessing; // tasks are also considered processing until all event responders have completed
						if (tasksProcessing == 0) // disable spin-lock
							std::atomic_flag_clear_explicit(&working, std::memory_order_release);
					}
				}, jobID); // so we know who owns this job
				return GReturn::SUCCESS;
			}
			// Due to std::function's aggressive constructor we had to give this function a unique name
			GReturn BranchDynamic(CORE::GLogic _dynamicTask) override 
			{
				if (_dynamicTask == nullptr) return GReturn::INVALID_ARGUMENT;
				return BranchSingular([_dynamicTask]() { _dynamicTask.Invoke(); });
			}
			template<typename Input, typename Output>
			GReturn BranchParallel(	void(*_parallelTask)(const Input*, Output*,unsigned int,const void*), 
									unsigned int _maxSection, unsigned int _arraySize, const void* _userData,
									int _inStride, const Input* _inputArray, int _outStride, Output* _outputArray)
			{
				// error check what is possible
				if (_parallelTask == nullptr) return GReturn::INVALID_ARGUMENT;
				if (_maxSection == 0) return GReturn::INVALID_ARGUMENT;
				if (_arraySize == 0) return GReturn::INVALID_ARGUMENT;
				// Input & Output may not be the same array. (if you need this just use Output alone!)
				if ((_inputArray || _outputArray) && 
					reinterpret_cast<std::uintptr_t>(_inputArray) == 
					reinterpret_cast<std::uintptr_t>(_outputArray))
					return GReturn::INVALID_ARGUMENT;
				// set input and output stride if not provided
				if (_inStride == 0) _inStride = sizeof(Input);
				if (_outStride == 0) _outStride = sizeof(Output);
				// check for overlapped memory if it could exist
				if (_inputArray != nullptr && _outputArray != nullptr)
				{
					std::uintptr_t input_start = reinterpret_cast<std::uintptr_t>(_inputArray);
					std::uintptr_t output_start = reinterpret_cast<std::uintptr_t>(_outputArray);
					std::uintptr_t input_end = input_start + static_cast<std::uintptr_t>(_arraySize) * _inStride;
					std::uintptr_t output_end = output_start + static_cast<std::uintptr_t>(_arraySize) * _outStride;
					if (input_start < output_start && input_end > output_start)
						return GReturn::MEMORY_CORRUPTION;
					if (output_start < input_start && output_end > input_start)
						return GReturn::MEMORY_CORRUPTION;
				}
				// Safe transfer of event suppression state.
				bool suppress = suppressEvents;
				// When the shared object falls completely out of scope we know all threads have completed.
				std::shared_ptr<ScopedEvent> waitForDeath;
				// because "parallelTask" below could theoretically invalidate "this" we use a safe handle 
				CORE::GEventGenerator safe = generator;
				// error checks done we compute required jobs and submit to thread pool.
				EVENT_DATA einfo = { 0, 0, { 0,_arraySize } };
				if (suppressEvents == false)
				{
					einfo.taskSubmissionIndex = ++taskSubmissionIndex;
					// allocate a ScopedEvent to handle the last message
					waitForDeath = std::make_shared<ScopedEvent>();
					waitForDeath->info = einfo;
					waitForDeath->start = std::chrono::steady_clock::now();
					waitForDeath->me = generator; // safe even if proxied object is destructed
				}
				// determine total amount of jobs to be sent
				int jobCount = ((_arraySize / _maxSection) + ((_arraySize % _maxSection) ? 1 : 0));
				// loop sending a job for each parallel task
				for (int i = 0; i < jobCount; ++i)
				{
					// as soon as a task is submitted it is considered to be processing
					++tasksProcessing;
					if (tasksProcessing == 1) // acquire "working" lock until no tasks are processing anymore
						std::atomic_flag_test_and_set_explicit(&working, std::memory_order_acquire);
					// include completion event info (fixed for C++11 syntax)
                    einfo.completionRange[0] = i * _maxSection;
                    einfo.completionRange[1] = (i+1) * _maxSection - 1;
					if (einfo.completionRange[1] >= _arraySize)
						einfo.completionRange[1] = _arraySize - 1u;
					// We identify each job from this GConcurrent with our memory address.
					// this helps us avoid deadlock scenarios when using the "Converge" routine.
					unsigned long long jobID = reinterpret_cast<unsigned long long>(this);
					// launch job, copy ScopedEvent so it does not fall out of scope until we are done.
					// The task itself & a few other items must be copied as they will fall out of scope.
					internal_gw::GatewareThreadPool().AddJob([&, _parallelTask, _inStride, _inputArray, 
																_outStride, _outputArray, _userData, 
																einfo, suppress, waitForDeath, safe]() mutable {
						// used for timing individual sections to help find performance bottlenecks.
						std::chrono::time_point<std::chrono::steady_clock> start;
						if (suppress == false)
							start = std::chrono::steady_clock::now();
						// traverse using byte pointers to account for unique data strides
						const unsigned char* in = reinterpret_cast<const unsigned char*>(_inputArray) + static_cast<uint64_t>(einfo.completionRange[0]) * _inStride;
						unsigned char* out = reinterpret_cast<unsigned char*>(_outputArray) + static_cast<uint64_t>(einfo.completionRange[0]) * _outStride;
						// do the actual work requested
						for (unsigned int j = einfo.completionRange[0]; j <= einfo.completionRange[1]; ++j, in += _inStride, out += _outStride)
						{
							_parallelTask(reinterpret_cast<const Input*>(in),
								reinterpret_cast<Output*>(out), j, _userData);
						}
						// if the user wants to be informed we inform them
						if (suppress == false)
						{
							GEvent send; // what people are listening for
							auto end = std::chrono::steady_clock::now();
							einfo.microsecondsElapsed = // *NEW* Include timing data always
								std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
							send.Write(Events::PARALLEL_SECTION_COMPLETE, einfo);
							safe.Push(send); // Notify anyone who is listening
						}
						// manually free "waitForDeath" here so like "BranchSingular" tasks processing is inclusive
						// to any event responders who may be reacting to completed events.
						waitForDeath.reset(); // drop this copy (will invoke destructor if this is the last one)
						// increase count, notify listeners and unlock spin if still possible
						CORE::GEventGenerator::burst_w alive = *safe;
						if (alive)
						{
							--tasksProcessing; // tasks are considered processing until all responders have reacted
							if (tasksProcessing == 0) // disable spin-lock
								std::atomic_flag_clear_explicit(&working, std::memory_order_release);
						}
					}, jobID); // so we know who owns this job
				}
				return GReturn::SUCCESS;
			}
			// specialization of void input type
			template<typename Output>
			GReturn BranchParallel(void(*_parallelTask)(const void*, Output*, unsigned int, const void*),
				unsigned int _maxSection, unsigned int _arraySize, const void* _userData,
				int _inStride, const void* _inputArray, int _outStride, Output* _outputArray)
			{
				void(*_job)(const size_t*, Output*, unsigned int, const void*) =
					reinterpret_cast<void(*)(const size_t*, Output*, unsigned int, const void*)>(_parallelTask);
				return BranchParallel<size_t, Output>(_job, _maxSection, _arraySize, _userData, _inStride,
					static_cast<const size_t*>(_inputArray), _outStride, _outputArray);
			}
			// specialization of void output type
			template<typename Input>
			GReturn BranchParallel(void(*_parallelTask)(const Input*, void*, unsigned int, const void*),
				unsigned int _maxSection, unsigned int _arraySize, const void* _userData,
				int _inStride, const Input* _inputArray, int _outStride, void* _outputArray)
			{
				void(*_job)(const Input*, size_t*, unsigned int, const void*) =
					reinterpret_cast<void(*)(const Input*, size_t*, unsigned int, const void*)>(_parallelTask);
				return BranchParallel<Input, size_t>(_parallelTask, _maxSection, _arraySize, _userData, _inStride,
					_inputArray, _outStride, static_cast<size_t*>(_outputArray));
			}
			// simplified version
			template<typename Data>
			GReturn BranchParallel(void(*_parallelTask)(Data&), unsigned int _arraySize, Data* _outDataArray) 
			{
				// error check what is possible
				if (_parallelTask == nullptr) return GReturn::INVALID_ARGUMENT;
				if (_arraySize == 0) return GReturn::INVALID_ARGUMENT;
				if (_outDataArray == nullptr) return GReturn::INVALID_ARGUMENT;
				// Safe transfer of event suppression state.
				bool suppress = suppressEvents;
				// When the shared object falls completely out of scope we know all threads have completed.
				std::shared_ptr<ScopedEvent> waitForDeath;
				// because "parallelTask" below could theoretically invalidate "this" we use a safe handle 
				CORE::GEventGenerator safe = generator;
				// error checks done we compute required jobs and submit to thread pool.
				EVENT_DATA einfo = { 0, 0, { 0,_arraySize } };
				if (suppressEvents == false)
				{
					einfo.taskSubmissionIndex = ++taskSubmissionIndex;
					// allocate a ScopedEvent to handle the last message
					waitForDeath = std::make_shared<ScopedEvent>();
					waitForDeath->info = einfo;
					waitForDeath->start = std::chrono::steady_clock::now();
					waitForDeath->me = generator; // safe even if proxied object is destructed
				}
				// determine total amount of jobs to be sent
				int autoSection = (sizeof(Data) * _arraySize) / G_CONCURRENT_AUTO_SECTION;// one thread per Nkb items 
				int jobCount = ((_arraySize / autoSection) + ((_arraySize % autoSection) ? 1 : 0));
				// loop sending a job for each parallel task
				for (int i = 0; i < jobCount; ++i)
				{
					// as soon as a task is submitted it is considered to be processing
					++tasksProcessing;
					if (tasksProcessing == 1) // acquire "working" lock until no tasks are processing anymore
						std::atomic_flag_test_and_set_explicit(&working, std::memory_order_acquire);
					// include completion event info (fixed for C++11 syntax)
					einfo.completionRange[0] = i * autoSection;
					einfo.completionRange[1] = (i + 1) * autoSection - 1;
					if (einfo.completionRange[1] >= _arraySize)
						einfo.completionRange[1] = _arraySize - 1u;
					// We identify each job from this GConcurrent with our memory address.
					// this helps us avoid deadlock scenarios when using the "Converge" routine.
					unsigned long long jobID = reinterpret_cast<unsigned long long>(this);
					// launch job, copy ScopedEvent so it does not fall out of scope until we are done.
					// The task itself & a few other items must be copied as they will fall out of scope.
					internal_gw::GatewareThreadPool().AddJob([&, _parallelTask, _outDataArray,
						einfo, suppress, waitForDeath, safe]() mutable {
							// used for timing individual sections to help find performance bottlenecks.
							std::chrono::time_point<std::chrono::steady_clock> start;
							if (suppress == false)
								start = std::chrono::steady_clock::now();
							// do the actual work requested
							for (unsigned int j = einfo.completionRange[0]; 
								j <= einfo.completionRange[1]; ++j)
							{
								_parallelTask(_outDataArray[j]);
							}
							// if the user wants to be informed we inform them
							if (suppress == false)
							{
								GEvent send; // what people are listening for
								auto end = std::chrono::steady_clock::now();
								einfo.microsecondsElapsed = // *NEW* Include timing data always
									std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
								send.Write(Events::PARALLEL_SECTION_COMPLETE, einfo);
								safe.Push(send); // Notify anyone who is listening
							}
							// manually free "waitForDeath" here so like "BranchSingular" tasks processing is inclusive
							// to any event responders who may be reacting to completed events.
							waitForDeath.reset(); // drop this copy (will invoke destructor if this is the last one)
							// increase count, notify listeners and unlock spin if still possible
							CORE::GEventGenerator::burst_w alive = *safe;
							if (alive)
							{
								--tasksProcessing; // tasks are considered processing until all responders have reacted
								if (tasksProcessing == 0) // disable spin-lock
									std::atomic_flag_clear_explicit(&working, std::memory_order_release);
							}
						}, jobID); // so we know who owns this job
				}
				return GReturn::SUCCESS;
			}
			GReturn Converge(unsigned int _spinUntil) override
			{
				// don't converge inside yourself, that bad m'kay?
				if (internal_gw::GatewareThreadPool().InsideJob(reinterpret_cast<unsigned long long>(this)))
					return GReturn::DEADLOCK;
				// waits until "working" is false, sleeping the thread for 1ms each time _spinUntil is reached.
				auto last = std::chrono::steady_clock::now();
				// This operation is lock free, so very optimized for short wait times
				while (std::atomic_flag_test_and_set_explicit(&working, std::memory_order_acquire))
				{	// measures how much time this thread is spin locked
					if (std::chrono::duration_cast<std::chrono::nanoseconds>(
						std::chrono::steady_clock::now() - last).count() >= _spinUntil)
					{
						// if things take longer than our maximum lock time we free the thread and try again later
						last = std::chrono::steady_clock::now();
					}
				}
				// Once we have converged we release our lock so we can lock again later.
				std::atomic_flag_clear_explicit(&working, std::memory_order_release);
				return GReturn::SUCCESS;
			}
			// Ensures "ScopedEvent" stays valid until all tasks complete
			~GConcurrentImplementation()
			{
				Converge(0);
			}
		};
	} // end CORE
} // end GW
