// dummy implementation of GConcurrent
namespace GW {
	namespace I {
		class GConcurrentImplementation :	public virtual GConcurrentInterface,
											public GEventGeneratorImplementation
		{
		public:
			// actual dummies for this class
			GReturn Create(bool _nope) { 
				return GReturn::INTERFACE_UNSUPPORTED; 
			}
			GReturn BranchSingular(std::function<void()> _singleTask) override	{ 
				return GReturn::FAILURE;
			}
			GReturn BranchDynamic(CORE::GLogic _dynamicTask) override {
				return GReturn::FAILURE;
			}
			template<typename Data>
			GReturn BranchParallel(void(*_parallelTask)(Data&),
				unsigned int _arraySize, Data* _outDataArray) {
				return GReturn::FAILURE;
			}
			template<typename Input, typename Output>
			GReturn BranchParallel(void(*_parallelTask)(const Input*, Output*, unsigned int, const void*),
				unsigned int _maxSection, unsigned int _arraySize, const void* _userData,
				int _inStride, const Input* _inputArray,
				int _outStride, Output* _outputArray) {
				return GReturn::FAILURE;
			}
			GReturn Converge(unsigned int _spinUntil) override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW