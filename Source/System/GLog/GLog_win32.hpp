#include "../../Shared/winutils.h"
#include "../../../Interface/System/GFile.h"
#include "../../../Interface/System/GConcurrent.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <queue>
#include <atomic>
#include <mutex>
#include <sstream>
#include <iostream>
#include <condition_variable>

#define THREAD_SLEEP_TIME 1
#define TIME_BUFFER 40

namespace GW
{
	namespace I
	{
		class GLogImplementation : public virtual GLogInterface,
			protected GThreadSharedImplementation
		{
		private:
			GW::SYSTEM::GFile logFile;
			GW::SYSTEM::GConcurrent thread;
			std::atomic<bool> threadRunning;

			std::condition_variable conditional;
			std::mutex queueLock;
			std::queue<std::string> logQueue;

			bool isVerbose = true;
			bool isConsoleLogged = false;

			unsigned long long GetThreadID()
			{
				std::stringstream ss;
				ss << std::this_thread::get_id();
				return std::stoull(ss.str());
			};

			GReturn LauchThread()
			{
				isVerbose = true;
				isConsoleLogged = false;
				threadRunning = true;
				thread.Create(false);
				return thread.BranchSingular([&]() {
					std::unique_lock<std::mutex> localQueueLock(queueLock);
					while (threadRunning || logQueue.size() != 0)
					{
						//Will lock the mutex when awaken and unlock it when put back to sleep.
						conditional.wait_for(localQueueLock, std::chrono::seconds(THREAD_SLEEP_TIME));
						//If there is anything to write.
						if (logQueue.size() != 0)
						{
							while (logQueue.size() != 0)
							{
								logFile.WriteLine(logQueue.front().c_str());
								logQueue.pop();

								logFile.FlushFile();
							}
						}
					}
				});
			}
		public:
			~GLogImplementation()
			{
				threadRunning = false;
				thread.Converge(0);
			}

			GReturn Create(const char* const _fileName)
			{
				GReturn rv = logFile.Create();
				if (G_FAIL(rv))
					return rv;

				rv = logFile.AppendTextWrite(_fileName);
				if (G_FAIL(rv))
					return rv;

				return LauchThread();
			}

			GReturn Create(SYSTEM::GFile _file)
			{
				if (!_file)
					return GReturn::INVALID_ARGUMENT;
				logFile = _file;

				return LauchThread();
			}

			GReturn Log(const char* const _log) override
			{
				if (_log == nullptr)
					return GReturn::INVALID_ARGUMENT;

				std::stringstream logStream;

				//Check verbose logging and add the verbose info if on.
				if (isVerbose)
				{
					time_t t = time(0);   //Get time now.
					char timeBuffer[TIME_BUFFER];

					//Parse the time out to readable time.
					struct tm buf;
					localtime_s(&buf, &t);
					asctime_s(timeBuffer, TIME_BUFFER, &buf);

					//Get rid of new line added by asctime.
					timeBuffer[strlen(timeBuffer) - 1] = '\0';

					//Create our log string.
					logStream << "[" << timeBuffer << "] ThreadID[";
					logStream << GetThreadID() << "]\t";
				}

				//Add the log and a newline.
				logStream << _log << "\r\n";

				//Lock the mutex to push the new message.
				queueLock.lock();

				//Push the message to the queue.
				logQueue.push(logStream.str());

				if (isConsoleLogged)
					std::cout << logStream.str();
				OutputDebugStringW(internal_gw::UTFStringConverter(logStream.str()).c_str());
				queueLock.unlock();
				return GReturn::SUCCESS;
			}

			GReturn LogCategorized(const char* const _category, const char* const _log) override
			{
				if (_category == nullptr || _log == nullptr)
					return GW::GReturn::INVALID_ARGUMENT;

				//The stream that will contain the full message.
				std::stringstream logStream;

				//Check verbose logging and add the verbose info if on.
				if (isVerbose)
				{
					time_t t = time(0);   //Get time now.
					char timeBuffer[TIME_BUFFER];

					//Parse time to readable time.
					struct tm buf;
					localtime_s(&buf, &t);
					asctime_s(timeBuffer, TIME_BUFFER, &buf);

					//Get rid of new line added by asctime.
					timeBuffer[strlen(timeBuffer) - 1] = '\0';

					//Build the string.
					logStream << "[" << timeBuffer << "] ThreadID[";
					logStream << GetThreadID() << "]\t";
				}

				//Add the category and message.
				logStream << "[" << _category << "]\t" << _log << "\r\n";

				//Lock the mutex to push the new msg.
				queueLock.lock();

				//Push the message to the queue.
				logQueue.push(logStream.str());

				if (isConsoleLogged)
					std::cout << logStream.str();
				OutputDebugStringW(internal_gw::UTFStringConverter(logStream.str()).c_str());
				queueLock.unlock();
				return GReturn::SUCCESS;
			}

			GReturn EnableVerboseLogging(bool _value) override
			{
				isVerbose = _value;
				return GReturn::SUCCESS;
			};

			GReturn EnableConsoleLogging(bool _value) override
			{
				isConsoleLogged = _value;
				return GReturn::SUCCESS;
			};

			GReturn Flush() override
			{
				conditional.notify_all();
				return GReturn::SUCCESS;
			};

			GReturn LockAsyncRead() const override { return GThreadSharedImplementation::LockAsyncRead(); }
			GReturn UnlockAsyncRead() const override { return GThreadSharedImplementation::UnlockAsyncRead(); }
			GReturn LockSyncWrite() override { return GThreadSharedImplementation::LockSyncWrite(); }
			GReturn UnlockSyncWrite() override { return GThreadSharedImplementation::UnlockSyncWrite(); }
		};
	} //end namespace I
} // end namespace GW

#undef THREAD_SLEEP_TIME
#undef TIME_BUFFER