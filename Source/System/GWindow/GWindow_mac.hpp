#ifdef __OBJC__
@import Foundation;
@import Cocoa;
@import AppKit;
#endif

#include <thread>
#include "../../Shared/macutils.h"
#include <iostream>

#define GWINDOW_EVENT_FLUSHES 10
#define GWINDOW_SLEEP_TIME_BETWEEN_FLUSHES std::chrono::milliseconds(100)
// Flushes events several times. Used during certain style reconfigurations to ensure windows
// complete their transition fully. Otherwise, windows may remain partly visible when deallocated
// before the transition is complete.
#define GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES()\
int gwinEventFlushs = 0;\
if (GWINDOW_EVENT_FLUSHES > 0)\
    do\
    {\
        std::this_thread::sleep_for(GWINDOW_SLEEP_TIME_BETWEEN_FLUSHES);\
        FlushMacEventLoop();\
    }\
    while (++gwinEventFlushs <= GWINDOW_EVENT_FLUSHES)

namespace GW
{
    namespace I
    {
        class GWindowImplementation;
    }
}

namespace internal_gw
{
    // GWAppDelegate Interface
    
    // Forward declarations of GWAppDelegate methods
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWAppDelegate, void, applicationDidFinishLaunching, NSNotification* notification);

    // Creates the GWAppDelegate class at runtime when G_OBJC_GET_CLASS(GWAppDelegate) is called.
    G_OBJC_CLASS_BEGIN(GWAppDelegate, NSObject<NSApplicationDelegate>)
    {
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWAppDelegate, applicationDidFinishLaunching, "v@:@", :);
    }
    G_OBJC_CLASS_END(GWAppDelegate)

    // GWAppDelegate Interface End



    // GWResponder Interface
    // The GWResponder is our interpretation of the NSResponder that will propagate window messages to other responders

    // Forward declarations of GWResponder methods
    G_OBJC_HEADER_INSTANCE_METHOD(GWResponder, bool, acceptFirstResponder);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWResponder, bool, acceptsFirstMouse, NSEvent* event);
    
    // Creates the GWResponder class at runtime when G_OBJC_GET_CLASS(GWResponder) is called.
    G_OBJC_CLASS_BEGIN(GWResponder, NSResponder)
    {
        G_OBJC_CLASS_METHOD(GWResponder, acceptFirstResponder, "B@:");
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWResponder, acceptsFirstMouse, "B@:@", :);
    }
    G_OBJC_CLASS_END(GWResponder)

    // GWResponder Interface End



    // GWDelegate Interface
    // The GWDelegate will be the delegate of the main window which will receive window events

    // Data members of GWDelegate
    G_OBJC_DATA_MEMBERS_STRUCT(GWDelegate)
    {
        GW::I::GWindowImplementation* pWindow;
        GW::I::GWindowInterface::EVENT_DATA eventData;
        GW::GEvent gevent;
        GW::SYSTEM::GWindowStyle* gWindowStyle;
        bool* windowWasDestroyed;
        bool* windowNeedsRedirecting;
        bool* windowStyleNeedsUpdating;
    };

    // Forward declarations of GWDelegate methods
    G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(GWDelegate);

    G_OBJC_HEADER_STATIC_METHOD_WITH_ARGUMENTS(GWDelegate, void, doNothing, id threadID);

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, NSSize, windowWillResize, NSWindow* sender, NSSize frameSize);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidResize, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidMove, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidMiniaturize, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidDeminiaturize, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidEnterFullScreen, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidExitFullScreen, NSNotification* notification);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowWillClose, NSNotification* notification);

    // Creates the GWDelegate class at runtime when G_OBJC_GET_CLASS(GWDelegate) is called
    G_OBJC_CLASS_BEGIN(GWDelegate, NSObject<NSWindowDelegate>)
    {
        G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(GWDelegate);

        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, doNothing, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowWillResize, "@@:@@", ::);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidResize, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidMove, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidMiniaturize, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidDeminiaturize, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidEnterFullScreen, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowDidExitFullScreen, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GWDelegate, windowWillClose, "v@:@", :);
    }
    G_OBJC_CLASS_END(GWDelegate)

    // GWDelegate Interface End
}

#include <atomic>
#include <mutex>
#include <string.h>
#include "../../Shared/GVersion.hpp"

namespace GW
{
    namespace I
    {
        class GWindowImplementation : public virtual GWindowInterface,
            public GEventGeneratorImplementation
        {
        private:
            NSWindow* window;
            NSAutoreleasePool* pool;

            std::atomic<int> xPos;
            std::atomic<int> yPos;
            std::atomic<int> width;
            std::atomic<int> height;

            SYSTEM::GWindowStyle gWindowStyle;
            GEvent gEvent;
            GW::I::GWindowInterface::EVENT_DATA* eventData;
            __block bool windowWasDestroyed;
            __block bool windowNeedsRedirecting;
            __block bool windowStyleNeedsUpdating;
            __block bool reconfigRequiresExtraStep;

            void SetInteralData(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
            {
                width = _width;
                height = _height;
                xPos = _x;
                yPos = _y;
                gWindowStyle = _style;
            }

            id responder;
            id delegate;
            id appDel;

            GReturn OpenWindow()
            {
                if (window)
                    return GReturn::REDUNDANT;

                responder = [internal_gw::G_OBJC_GET_CLASS(GWResponder) alloc];
                delegate = [internal_gw::G_OBJC_GET_CLASS(GWDelegate) alloc];
                appDel = [internal_gw::G_OBJC_GET_CLASS(GWAppDelegate) alloc];

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& delegateDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GWDelegate, delegate);
                delegateDataMembers.pWindow = this;
                delegateDataMembers.gWindowStyle = &gWindowStyle;
                delegateDataMembers.windowWasDestroyed = &windowWasDestroyed;
                delegateDataMembers.windowNeedsRedirecting = &windowNeedsRedirecting;
                delegateDataMembers.windowStyleNeedsUpdating = &windowStyleNeedsUpdating;

                windowWasDestroyed = false;
                windowNeedsRedirecting = false;
                windowStyleNeedsUpdating = false;
                reconfigRequiresExtraStep = false;

                eventData = &delegateDataMembers.eventData;

                pool = [[NSAutoreleasePool alloc]init];

                [NSApplication sharedApplication] ;

                [NSThread detachNewThreadSelector : @selector(doNothing :) toTarget: delegate withObject : nil] ;

                [NSApp setActivationPolicy : NSApplicationActivationPolicyRegular] ;
                [NSApp setDelegate : appDel] ;

                NSUInteger windowStyleMask = ConvertWindowsStyle(gWindowStyle);

                CGSize screenSize = [[NSScreen mainScreen]frame] .size;

                NSRect windowRect;
                if (gWindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                {
                    windowRect = NSMakeRect(xPos, screenSize.height - yPos - height, screenSize.width, screenSize.height);

                    [NSMenu setMenuBarVisible : NO] ;
                }
                else
                {
                    windowRect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);

                    // Adjust the window rect so the content area is the desired width and height.
                    ConvertContentRectToFrameRect(windowRect, windowStyleMask);

                    [NSMenu setMenuBarVisible : YES] ;
                }

                window = [[NSWindow alloc]initWithContentRect:windowRect
                    styleMask : windowStyleMask
                    backing : NSBackingStoreBuffered
                    defer : NO];

                if (window == nil)
                    return GReturn::FAILURE;
#ifdef _DEBUG
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING_LONG
#else
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING
#endif
                [window setTitle : @GATEWARE_WINDOW_NAME];
#undef GATEWARE_WINDOW_NAME

                [window setFrame : windowRect display : YES];

                if (gWindowStyle == SYSTEM::GWindowStyle::WINDOWEDLOCKED)
                    [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenNone];
                else
                    [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];

                [responder setNextResponder : window.nextResponder] ;
                [window setNextResponder : responder] ;
                [window makeFirstResponder : window.contentView] ;
                [window.contentView setNextResponder : responder] ;

                [window setDelegate : delegate] ;
                [window makeKeyAndOrderFront : nil] ;

                if (gWindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED || gWindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                {
                    [window toggleFullScreen : nil] ;
                    GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                }
                else if (gWindowStyle == SYSTEM::GWindowStyle::MINIMIZED)
                {
                    [window miniaturize : nil] ;
                    GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();

                    [pool drain] ;

                    if ([window isMiniaturized])
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }

                [pool drain] ;

                if ([window isVisible])
                    return GReturn::SUCCESS;
                else
                    return GReturn::FAILURE;
            }

            /*
                Converts a content area (aka client area) rectangle to a window frame rectangle, while taking into account
                the style mask of the window. The resultant rectangle is large enough to size a window so that contains the
                full size of the original rectangle passed to the function.
             */
            void ConvertContentRectToFrameRect(NSRect& _rect, const NSUInteger _mask) const
            {
                // Get the content area (client area) after creating a window frame with _rect.
                NSRect contentRect = [NSWindow contentRectForFrameRect : _rect styleMask : _mask];

                // Calculate the border size
                CGSize borderSize = NSMakeSize(
                    _rect.size.width - contentRect.size.width,
                    _rect.size.height - contentRect.size.height
                );

                _rect.origin.y -= borderSize.height; // Adjust the y position to account for the titlebar.
                // Add the border size to _rect to create a frame area that preserves the desired content area dimensions.
                _rect.size = NSMakeSize(
                    _rect.size.width + borderSize.width,
                    _rect.size.height + borderSize.height
                );
            }

            /*
                Takes a GWindowStyle and returns the corresponding NSWindowStyleMask.
             */
            NSUInteger ConvertWindowsStyle(SYSTEM::GWindowStyle _style) const
            {
                switch (_style)
                {
                case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
                case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
                case SYSTEM::GWindowStyle::MINIMIZED:
                    return NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskResizable | NSWindowStyleMaskMiniaturizable;

                case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
                    return NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable;

                case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
                case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
                    return NSWindowStyleMaskMiniaturizable;
                }
                return 0;
            }

        public:
            ~GWindowImplementation()
            {
                RUN_ON_UI_THREAD(^ {
                    if (window && [window acceptsMouseMovedEvents])
                    {
                        [window setAcceptsMouseMovedEvents:NO];
                        FlushMacEventLoop();
                    }
                    
                    [NSMenu setMenuBarVisible:YES];
                    FlushMacEventLoop(); //crashes as of 06/13/2022
                    
                    if (window)
                    {
                        NSWindowStyleMask winStyle = [window styleMask];
                        if ((winStyle & NSWindowStyleMaskFullScreen) == NSWindowStyleMaskFullScreen)
                        {
                            [window toggleFullScreen : nil] ;
                            FlushMacEventLoop();
                        }
                        else if ([window isMiniaturized])
                        {
                            [window deminiaturize : nil];
                            FlushMacEventLoop();
                        }
                        
                        [window close];
                        FlushMacEventLoop();
                    }

                    // A paranoia check to make sure there are no remaining
                    // events that may trigger after the window is cleaned up.
                    for (int i = 0; i < 10; ++i)
                        FlushMacEventLoop();
                    
                    window = nil;
                }); //end of RUN_ON_UI_THREAD
                
                if (window)
                {
                    // Remove subviews like the one added by GRasterSurface.
                    NSArray* subViewArray = [[window contentView] subviews];
                    for (id obj in subViewArray)
                        [obj removeFromSuperview];
                        
                    [window release] ;
                    window = nil;
                }
                
                if (responder)
                {
                    [responder release] ;
                    responder = nil;
                }
                
                if (delegate)
                {
                    [(delegate)release] ;
                    (delegate) = nil;
                }
                
                if (appDel)
                {
                    [appDel release] ;
                    appDel = nil;
                }
            }

            GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
            {
                this->SetInteralData(_x, _y, _width, _height, _style);
                return OpenWindow();
            }
               // TODO: make seperate var to kill the window
            GReturn ProcessWindowEvents() override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                
                RUN_ON_UI_THREAD(^ {
                    FlushMacEventLoop();
                    eventData->eventFlags = Events::EVENTS_PROCESSED;
                    gEvent.Write(Events::EVENTS_PROCESSED, *eventData);
                    Push(gEvent);
                });

                return GReturn::SUCCESS;
            }

            GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, GW::SYSTEM::GWindowStyle _style) override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                
                GW::SYSTEM::GWindowStyle prevStyle = gWindowStyle;
                SetInteralData(_x, _y, _width, _height, _style);
                
                switch (gWindowStyle)
                {
                case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);
                    
                    // If the window is minimized or fullscreen, it will need to be brought out of those states
                    // before modifying the window further.
                    if (fullscreen)
                    {
                        if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            // Must use RUN_ON_UI_THREAD when changing the window's look.
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true; // Marks that exiting fullscreen doesn not complete the style change.
                                [window toggleFullScreen : nil];

                                // Flush the events so they propagate.
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                reconfigRequiresExtraStep = true;
                                [window toggleFullScreen : nil];
                                
                                GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                            });

                            return GReturn::SUCCESS;
                        }
                    }
                    else if ([window isMiniaturized])
                    {
                        RUN_ON_UI_THREAD(^ {
                            windowNeedsRedirecting = true;
                            [window deminiaturize : nil];

                            FlushMacEventLoop();
                        });
                    }
                    else if (gWindowStyle == prevStyle && !reconfigRequiresExtraStep)
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskResizable | NSWindowStyleMaskMiniaturizable;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);
                            NSRect windowFrame = [window frame];
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            if ([window styleMask] != mask)
                                [window setStyleMask : mask] ;
                            else if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];

                            FlushMacEventLoop();
                        });
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskResizable | NSWindowStyleMaskMiniaturizable;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);

                            [NSMenu setMenuBarVisible : YES];
                            
                            reconfigRequiresExtraStep = false;
                            [window setHasShadow: YES];
                            if ([window styleMask] != mask)
                                windowStyleNeedsUpdating = true;
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            NSRect windowFrame = [window frame];
                            if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            else
                            {
                                windowStyleNeedsUpdating = false;
                                if ([window styleMask] != mask)
                                    [window setStyleMask : mask] ;
                            }
                            
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];

                            FlushMacEventLoop();
                        });
                    }

                    if (window)
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }
                break;

                case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);
                    
                    if (fullscreen)
                    {
                        if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                reconfigRequiresExtraStep = true;
                                [window toggleFullScreen : nil];
                                
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                reconfigRequiresExtraStep = true;
                                [window toggleFullScreen : nil];
                                
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                    }
                    else if ([window isMiniaturized])
                    {
                        RUN_ON_UI_THREAD(^ {
                            windowNeedsRedirecting = true;
                            [window deminiaturize : nil];

                            FlushMacEventLoop();
                        });
                    }
                    else if (gWindowStyle == prevStyle && !reconfigRequiresExtraStep)
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskBorderless;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);
                            NSRect windowFrame = [window frame];
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            if ([window styleMask] != mask)
                                [window setStyleMask : mask] ;
                            else if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];

                            FlushMacEventLoop();
                        });
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskBorderless;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);

                            [NSMenu setMenuBarVisible : YES];
                            
                            reconfigRequiresExtraStep = false;
                            [window setHasShadow: YES];
                            if ([window styleMask] != mask)
                                windowStyleNeedsUpdating = true;
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            NSRect windowFrame = [window frame];
                            if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            else
                            {
                                windowStyleNeedsUpdating = false;
                                if ([window styleMask] != mask)
                                    [window setStyleMask : mask] ;
                            }
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];

                            FlushMacEventLoop();
                        });
                    }

                    if (window)
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }
                break;
                        
                case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);
                    
                    if (fullscreen)
                    {
                        if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                [window toggleFullScreen : nil];
                                
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                reconfigRequiresExtraStep = true;
                                [window toggleFullScreen : nil];
                                
                                GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                            });

                            return GReturn::SUCCESS;
                        }
                    }
                    else if ([window isMiniaturized])
                    {
                        RUN_ON_UI_THREAD(^ {
                            windowNeedsRedirecting = true;
                            [window deminiaturize : nil];

                            FlushMacEventLoop();
                        });
                    }
                    else if (gWindowStyle == prevStyle && !reconfigRequiresExtraStep)
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);
                            NSRect windowFrame = [window frame];
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            if ([window styleMask] != mask)
                                [window setStyleMask : mask] ;
                            else if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenNone];

                            FlushMacEventLoop();
                        });
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger mask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(xPos, screenSize.height - yPos - height, width, height);

                            [NSMenu setMenuBarVisible : YES];
                            
                            reconfigRequiresExtraStep = false;
                            [window setHasShadow: YES];
                            if ([window styleMask] != mask)
                                windowStyleNeedsUpdating = true;
                            
                            ConvertContentRectToFrameRect(rect, mask);
                            
                            NSRect windowFrame = [window frame];
                            if (windowFrame.origin.x != rect.origin.x ||
                                windowFrame.origin.y != rect.origin.y ||
                                windowFrame.size.width != rect.size.width ||
                                windowFrame.size.height != rect.size.height)
                                [window setFrame : rect display : YES] ;
                            else
                            {
                                windowStyleNeedsUpdating = false;
                                if ([window styleMask] != mask)
                                    [window setStyleMask : mask] ;
                            }
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenNone];

                            FlushMacEventLoop();
                        });
                    }

                    if (window)
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }
                break;

                case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);

                    if (fullscreen)
                    {
                        if (gWindowStyle == prevStyle)
                            return GReturn::SUCCESS;
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                reconfigRequiresExtraStep = true;
                                [window toggleFullScreen : nil];

                                GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                            });

                            if (window)
                                return GReturn::SUCCESS;

                            return GReturn::FAILURE;
                        }
                    }
                    else if ([window isMiniaturized])
                    {
                        RUN_ON_UI_THREAD(^ {
                            windowNeedsRedirecting = true;
                            [window deminiaturize : nil];

                            FlushMacEventLoop();
                        });
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger styleMask = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskResizable | NSWindowStyleMaskMiniaturizable;

                            [NSMenu setMenuBarVisible : YES];

                            [window setStyleMask : styleMask] ;
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];
                            [window toggleFullScreen : nil];

                            if (reconfigRequiresExtraStep)
                            {
                                reconfigRequiresExtraStep = false;
                                GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                            }
                            else
                                FlushMacEventLoop();
                        });

                        if (window)
                            return GReturn::SUCCESS;

                        return GReturn::FAILURE;
                    }
                }
                break;

                case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);

                    if (fullscreen)
                    {
                        if (gWindowStyle == prevStyle)
                            return GReturn::SUCCESS;
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                [window toggleFullScreen : nil];

                                FlushMacEventLoop();
                            });
                            
                            if (window)
                                return GReturn::SUCCESS;

                            return GReturn::FAILURE;
                        }
                    }
                    else if ([window isMiniaturized])
                    {
                        RUN_ON_UI_THREAD(^ {
                            windowNeedsRedirecting = true;
                            [window deminiaturize : nil];

                            FlushMacEventLoop();
                        });
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            NSUInteger styleMask = NSWindowStyleMaskMiniaturizable;
                            CGSize screenSize = [[NSScreen mainScreen] frame].size;
                            NSRect rect = NSMakeRect(0, 0, screenSize.width, screenSize.height);

                            [NSMenu setMenuBarVisible : NO];

                            [window setStyleMask : styleMask] ;
                            [window setFrame : rect display : YES] ;
                            [window setCollectionBehavior : NSWindowCollectionBehaviorFullScreenPrimary];
                            [window toggleFullScreen : nil];

                            FlushMacEventLoop();
                        });
                    }
                    
                    if (window)
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }
                break;

                case SYSTEM::GWindowStyle::MINIMIZED:
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);
                    
                    if (fullscreen)
                    {
                        if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            RUN_ON_UI_THREAD(^ {
                                windowNeedsRedirecting = true;
                                [window toggleFullScreen : nil];
                                
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                        else if (prevStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            RUN_ON_UI_THREAD(^ {
                                [NSMenu setMenuBarVisible : YES];
                                
                                [window setHasShadow: YES];
                                
                                windowNeedsRedirecting = true;
                                [window toggleFullScreen : nil];
                                
                                FlushMacEventLoop();
                            });

                            return GReturn::SUCCESS;
                        }
                    }
                    else
                    {
                        RUN_ON_UI_THREAD(^ {
                            [NSMenu setMenuBarVisible : YES];
                            
                            [window miniaturize : nil] ;
                            
                            GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES();
                        });
                    }

                    if ([window isMiniaturized])
                        return GReturn::SUCCESS;

                    return GReturn::FAILURE;
                }
                break;
                }
                
                return GReturn::SUCCESS;
            }

            GReturn SetWindowName(const char* _newName) override
            {
                __block bool operationResult = false;

                if (_newName == nil)
                    return GReturn::INVALID_ARGUMENT;

                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                RUN_ON_UI_THREAD(^ {
                    NSString* macName = [NSString stringWithCString : _newName encoding : NSUTF8StringEncoding];
                    window.title = macName;

                    if (window.title == macName)
                        operationResult = true;
                });
                if (operationResult)
                    return GReturn::SUCCESS;

                return GReturn::FAILURE;
            }

			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
            {
                if (_argbPixels == nullptr)
                    return GReturn::INVALID_ARGUMENT;
                else if (_width <= 0 || _height <= 0)
                    return GReturn::INVALID_ARGUMENT;

                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                NSBitmapImageRep* bitmap = [[NSBitmapImageRep alloc]
                    initWithBitmapDataPlanes:NULL
                    pixelsWide : _width
                    pixelsHigh : _height
                    bitsPerSample : 8
                    samplesPerPixel : 4
                    hasAlpha : YES
                    isPlanar : NO
                    colorSpaceName : NSDeviceRGBColorSpace
                    bitmapFormat : NSBitmapFormatAlphaFirst
                    bytesPerRow : 0
                    bitsPerPixel : 0
                ];
                unsigned int* imagePixels = (unsigned int*)[bitmap bitmapData];
                NSImage* image = [[NSImage alloc] init];
                [image addRepresentation : bitmap] ;

                // Convert the pixels from ARGB ro BGRA
                for (size_t i = 0; i < _width * _height; ++i)
                    imagePixels[i] = (_argbPixels[i] & 0xFF000000) >> 24
                                   | (_argbPixels[i] & 0x00FF0000) >> 8
                                   | (_argbPixels[i] & 0x0000FF00) << 8
                                   | (_argbPixels[i] & 0x000000FF) << 24;
                
                [NSApplication sharedApplication].applicationIconImage = image;
                
                [image release];
                image = nil;
                
                [bitmap release];
                bitmap = nil;

                return GReturn::SUCCESS;
            }

            GReturn MoveWindow(int _x, int _y) override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                SetInteralData(_x, _y, width, height, gWindowStyle);

                RUN_ON_UI_THREAD(^ {
                    CGSize screenSize = [[NSScreen mainScreen] frame].size;
                    
                    CGPoint newPos;
                    newPos.y = screenSize.height - yPos;
                    newPos.x = xPos;

                    NSPoint pointPos;
                    pointPos.y = newPos.y;
                    pointPos.x = newPos.x;

                    [window setFrameTopLeftPoint : pointPos] ;
                    FlushMacEventLoop();
                });

                return GReturn::SUCCESS;
            }

            GReturn ResizeWindow(int _width, int _height) override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                CGSize screenSize = [[NSScreen mainScreen] frame].size;
                SetInteralData(xPos, screenSize.height - yPos - _height, _width, _height, gWindowStyle);
                
                NSUInteger windowStyleMask = ConvertWindowsStyle(gWindowStyle);

                RUN_ON_UI_THREAD(^ {
                    NSRect rect = NSMakeRect(xPos, yPos, _width, _height);
                    
                    ConvertContentRectToFrameRect(rect, windowStyleMask);

                    [window setFrame : rect display : YES] ;
                    FlushMacEventLoop();
                });
                return GReturn::SUCCESS;
            }

            GReturn Maximize() override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                if (gWindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERED || gWindowStyle == SYSTEM::GWindowStyle::MINIMIZED)
                {
                    return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERED);
                }
                else if (gWindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)
                {
                    return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
                }
                else
                {
                    bool fullscreen;
                    IsFullscreen(fullscreen);
                    
                    if (!fullscreen)
                    {
                        if (gWindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
                        {
                            return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERED);
                        }
                        else if (gWindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
                        {
                            return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
                        }
                    }
                }

                return GReturn::REDUNDANT;
            }

            GReturn Minimize() override
            {
                return ChangeWindowStyle(SYSTEM::GWindowStyle::MINIMIZED);
            }

            GReturn ChangeWindowStyle(GW::SYSTEM::GWindowStyle _style) override
            {
                return ReconfigureWindow(xPos, yPos, width, height, _style);
            }

            GReturn GetWidth(unsigned int& _outWidth) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                _outWidth = rect.size.width;
                return GReturn::SUCCESS;
            }

            GReturn GetHeight(unsigned int& _outHeight) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                _outHeight = rect.size.height;
                return GReturn::SUCCESS;
            }

            GReturn GetClientWidth(unsigned int& _outClientWidth) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                NSRect contentRect = [window contentRectForFrameRect : rect];
                _outClientWidth = contentRect.size.width;
                return GReturn::SUCCESS;
            }

            GReturn GetClientHeight(unsigned int& _outClientHeight) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                NSRect contentRect = [window contentRectForFrameRect : rect];
                _outClientHeight = contentRect.size.height;
                return GReturn::SUCCESS;
            }

            GReturn GetX(unsigned int& _outX) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                _outX = rect.origin.x;
                return GReturn::SUCCESS;
            }

            GReturn GetY(unsigned int& _outY) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                CGSize screenSize = [[NSScreen mainScreen] frame].size;
                _outY = screenSize.height - rect.origin.y - rect.size.height;
                return GReturn::SUCCESS;
            }

            GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                NSRect rect = window.frame;
                NSRect contentRect = [window contentRectForFrameRect : rect];
                NSRect screenSize = [[NSScreen mainScreen] frame];
                
                unsigned int x;
                unsigned int y;
                GetX(x);
                GetY(y);
                y = screenSize.size.height - y;
                
                _outX = x - contentRect.origin.x;
                _outY = y - (contentRect.origin.y + contentRect.size.height); // origin is expressed with +y axis going up
                return GReturn::SUCCESS;
            }

            GReturn GetWindowHandle(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                if (!(__bridge void*)window)
                {
                    return GReturn::FAILURE;
                }
                _outUniversalWindowHandle.window = window;
                return GReturn::SUCCESS;
            }

            GReturn IsFullscreen(bool& _outIsFullscreen) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;

                __block NSWindowStyleMask winStyle;
                RUN_ON_UI_THREAD(^ { winStyle = [window styleMask]; });
                if ((winStyle & NSWindowStyleMaskFullScreen) == NSWindowStyleMaskFullScreen)
                    _outIsFullscreen = TRUE;
                else
                    _outIsFullscreen = FALSE;

                return GReturn::SUCCESS;
            }

            GReturn IsFocus(bool& _outIsFocus) const override
            {
                if (windowWasDestroyed)
                    return GReturn::FAILURE;
                // mac auto filters input events based on active window
                // for conformity we may need a way to get backgroud keys
                // then this function will be needed for filtering.
                _outIsFocus = true;
                return GReturn::SUCCESS;
            }
        };
    }
}

namespace internal_gw
{
    // GWAppDelegate Implementation

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWAppDelegate, void, applicationDidFinishLaunching, NSNotification* notification)
    {
        [NSApp stop : nil] ;

        NSPoint p;
        p.x = 0;
        p.y = 0;

        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];

        NSEvent* event = [NSEvent otherEventWithType : NSEventTypeApplicationDefined
            location : p
            modifierFlags : 0
            timestamp : 0
            windowNumber : 0
            context : nil
            subtype : 0
            data1 : 0
            data2 : 0];

        [NSApp postEvent : event atStart : YES] ;
        [pool drain] ;
    }

    // GWAppDelegate Implementation End



    // GWResponder Implementation

    G_OBJC_HEADER_INSTANCE_METHOD(GWResponder, bool, acceptFirstResponder)
    {
        return YES;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWResponder, bool, acceptsFirstMouse, NSEvent* event)
    {
        return YES;
    }

    // GWResponder Implementation End



    // GWDelegate Implementation

    G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(GWDelegate);

    G_OBJC_HEADER_STATIC_METHOD_WITH_ARGUMENTS(GWDelegate, void, doNothing, id threadID) {}

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, NSSize, windowWillResize, NSWindow* sender, NSSize frameSize)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window will be resized (whether by the user or through one of the setFrame...methods
        //other than setFrame:display)
        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::RESIZE;
        eventData.height = frameSize.height;
        eventData.width = frameSize.width;
		NSSize contentSize = [sender contentRectForFrameRect : sender.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = sender.frame.origin.x;
        eventData.windowY = screenSize.height - sender.frame.origin.y - sender.frame.size.height;
        eventData.windowHandle = (__bridge void*)sender;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
        
        return frameSize;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidResize, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window been resized
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::RESIZE;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
        
        if (*selfDataMembers.windowStyleNeedsUpdating)
        {
            *selfDataMembers.windowStyleNeedsUpdating = false;
            selfDataMembers.pWindow->ChangeWindowStyle(*selfDataMembers.gWindowStyle);
        }
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidMove, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window has moved.
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::MOVE;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidMiniaturize, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window has been minimized.
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::MINIMIZE;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidDeminiaturize, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window has been deminimized.
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::RESIZE;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
        
        //A window might need to be redirected to another state after becoming windowed for the following reasons:
        //    - macOS does not support minimizing directly from FullScreen.
        //    - Switching between FULLSCREENBORDERED and FULLSCREENBORDERLESS leads to empty screen spaces.
        //    - Resizing a window and exiting fullscreen causes EXC_BAD_ACCESS errors.
        if (*selfDataMembers.windowNeedsRedirecting)
        {
            *selfDataMembers.windowNeedsRedirecting = false;
            selfDataMembers.pWindow->ChangeWindowStyle(*selfDataMembers.gWindowStyle);
        }
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidEnterFullScreen, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);

        //The window has entered full-screen mode.
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::MAXIMIZE;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowDidExitFullScreen, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //The window has left full-screen mode.
        
        if (*selfDataMembers.windowNeedsRedirecting)
        {
            *selfDataMembers.windowNeedsRedirecting = false;
            selfDataMembers.pWindow->ChangeWindowStyle(*selfDataMembers.gWindowStyle);
        }
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GWDelegate, void, windowWillClose, NSNotification* notification)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GWDelegate)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GWDelegate, self);
        
        //Tells the delegate that the window is about to close.
        NSWindow* window = notification.object;

        GW::I::GWindowInterface::EVENT_DATA& eventData = selfDataMembers.eventData;
        eventData.eventFlags = GW::I::GWindowInterface::Events::DESTROY;
        eventData.height = window.frame.size.height;
        eventData.width = window.frame.size.width;
		NSSize contentSize = [window contentRectForFrameRect : window.frame].size;
		eventData.clientHeight = contentSize.height;
		eventData.clientWidth = contentSize.width;
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        eventData.windowX = window.frame.origin.x;
        eventData.windowY = screenSize.height - window.frame.origin.y - window.frame.size.height;
        eventData.windowHandle = (__bridge void*)window;

        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(eventData.eventFlags, eventData);

        *selfDataMembers.windowWasDestroyed = true;
        GW::I::GWindowImplementation* pWindow = selfDataMembers.pWindow;
        pWindow->Push(gevent);
    }

    // GWDelegate Implementation End
}

#undef GWINDOW_EVENT_FLUSHES
#undef GWINDOW_SLEEP_TIME_BETWEEN_FLUSHES
#undef GWINDOW_FLUSH_MAC_EVENTS_SEVERAL_TIMES
