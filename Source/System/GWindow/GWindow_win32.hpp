#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include "../../Shared/winutils.h"
#include "../../Shared/GVersion.hpp"

namespace GW
{
	namespace I
	{
		class GWindowImplementation :	public virtual GWindowInterface,
										public GEventGeneratorImplementation
		{
		private:
			HWND wndHandle = nullptr;
			std::wstring className;
			bool destroyEventIsSentByWndProc = false;
			int m_WindowX = -1;
			int m_WindowY = -1;
			int m_WindowWidth = -1;
			int m_WindowHeight = -1;
			SYSTEM::GWindowStyle m_WindowStyle = SYSTEM::GWindowStyle::WINDOWEDBORDERED;

			GEvent m_GEvent;
			EVENT_DATA m_EventData;

			HICON m_IconSmall = NULL;
			HICON m_IconBig = NULL;

			void SetInteralData(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				m_WindowWidth = _width;
				m_WindowHeight = _height;
				m_WindowX = _x;
				m_WindowY = _y;
				m_WindowStyle = _style;
			}

			LRESULT DispatchEventAndData(UINT uMsg, WPARAM wParam, LPARAM lParam)
			{
				RECT windowRect;
				::GetWindowRect(wndHandle, &windowRect);

				switch (uMsg)
				{
				case WM_GETMINMAXINFO:
				{
                    // This will enforce a minimum width and height that the window will now have to obey. This is
                    // mainly here to prevent surfaces (particularly Vulkan) from crashing when the internal width or
                    // height of the window is 0. By default, Linux already does this same behavior. Windows, however,
                    // seems to enforce a minimum width only, ignoring the height.

                    // This is kind of complicated to get in windows through WinAPI calls, generally the height will be
                    // around 36px from what I've researched and seen. This may be *slightly* different on older OSes.
                    constexpr long WINDOW_BORDER_HEIGHT  = 36;
                    constexpr long WINDOW_MINIMUM_HEIGHT = 64;
                    LPMINMAXINFO   minMaxInfo            = reinterpret_cast<LPMINMAXINFO>(lParam);
                    minMaxInfo->ptMinTrackSize.y         = WINDOW_BORDER_HEIGHT + WINDOW_MINIMUM_HEIGHT;
                }
				break;
				case WM_SIZE:
				{
					if (wParam == SIZE_MAXHIDE || wParam == SIZE_MAXSHOW) 
					{
						break;
					}

					m_EventData.height = windowRect.bottom - windowRect.top;
					m_EventData.width = windowRect.right - windowRect.left;
					m_EventData.clientHeight = (unsigned int)HIWORD(lParam);
					m_EventData.clientWidth = (unsigned int)LOWORD(lParam);
					m_EventData.windowX = windowRect.left;
					m_EventData.windowY = windowRect.top;
					m_EventData.windowHandle = wndHandle;

					switch (wParam)
					{
					case SIZE_MAXIMIZED: { m_EventData.eventFlags = Events::MAXIMIZE; } break;
					case SIZE_MINIMIZED: { m_EventData.eventFlags = Events::MINIMIZE; } break;
					case SIZE_RESTORED: { m_EventData.eventFlags = Events::RESIZE; } break;
					}

					m_GEvent.Write(m_EventData.eventFlags, m_EventData);
					Push(m_GEvent);
				}
				break;

				case WM_MOVE:
				{
					RECT clientRect;
					::GetClientRect(wndHandle, &clientRect);

					m_EventData.eventFlags = Events::MOVE;
					m_EventData.height = windowRect.bottom - windowRect.top;
					m_EventData.width = windowRect.right - windowRect.left;
					m_EventData.clientHeight = clientRect.bottom - clientRect.top;
					m_EventData.clientWidth = clientRect.right - clientRect.left;
					m_EventData.windowX = windowRect.left;
					m_EventData.windowY = windowRect.top;
					m_EventData.windowHandle = wndHandle;

					m_GEvent.Write(m_EventData.eventFlags, m_EventData);
					Push(m_GEvent);
				}
				break;

				case WM_CLOSE:
				{
					RECT clientRect;
					::GetClientRect(wndHandle, &clientRect);

					m_EventData.eventFlags = Events::DESTROY;
					m_EventData.height = windowRect.bottom - windowRect.top;
					m_EventData.width = windowRect.right - windowRect.left;
					m_EventData.clientHeight = clientRect.bottom - clientRect.top;
					m_EventData.clientWidth = clientRect.right - clientRect.left;
					m_EventData.windowX = windowRect.left;
					m_EventData.windowY = windowRect.top;
					m_EventData.windowHandle = wndHandle;

					m_GEvent.Write(m_EventData.eventFlags, m_EventData);
					Push(m_GEvent);
					destroyEventIsSentByWndProc = true;
				}
				break;

				case WM_DESTROY: { ::PostQuitMessage(0); } break;
				default: { return ::DefWindowProc(wndHandle, uMsg, wParam, lParam); }
				}
				return ::DefWindowProc(wndHandle, uMsg, wParam, lParam);
			}

			static LRESULT CALLBACK GWindowProcedure(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
			{
				GWindowImplementation* self;
				if (uMsg == WM_NCCREATE)
				{
					LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
					self = reinterpret_cast<GWindowImplementation*>(lpcs->lpCreateParams);
					self->wndHandle = hwnd;
					::SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(self));
				}
				else
				{
					self = reinterpret_cast<GWindowImplementation*>(::GetWindowLongPtr(hwnd, GWLP_USERDATA));
				}
				if (self)
				{
					return self->DispatchEventAndData(uMsg, wParam, lParam);
				}
				else
				{
					return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
				}
			}

			GReturn OpenWindow()
			{
				if (wndHandle)
					return GReturn::REDUNDANT;

				auto hasGUI = GetDesktopWindow();
				if (!hasGUI)
					return GReturn::INTERFACE_UNSUPPORTED;

				destroyEventIsSentByWndProc = false;
				WNDCLASSEX winClass;
				ZeroMemory(&winClass, sizeof(WNDCLASSEX));

				// Generate random string based on a given length. This is because if the end-user want to create multiple GWindow instances,
				// class name have to be unique to the GWindow.
				auto randomString = [](size_t length) -> std::string
				{
					auto randchar = []() -> char
					{
						const char charset[] =
							"0123456789"
							"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
							"abcdefghijklmnopqrstuvwxyz";
						const size_t max_index = (sizeof(charset) - 1);
						return charset[rand() % max_index];
					};
					std::string str(length, 0);
					std::generate_n(str.begin(), length, randchar);
					return str;
				};
				std::string randName = randomString(9);
				std::wstring wcharName = L"GatewareWindow:";
				wcharName += std::wstring(randName.begin(), randName.end());
				className = std::move(wcharName);

				// Icon members are not set to the default icon (IDI_APPLICATION) because shared icons should not be destroyed.
				ZeroMemory(&m_IconSmall, sizeof(HICON));
				m_IconSmall = NULL;
				ZeroMemory(&m_IconBig, sizeof(HICON));
				m_IconBig = NULL;

				winClass.cbSize = sizeof(WNDCLASSEX);
				winClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
				winClass.hCursor = LoadCursorW(NULL, IDC_ARROW);
				winClass.hIcon = LoadIconW(0, IDI_APPLICATION);
				winClass.lpfnWndProc = GWindowProcedure;
				winClass.lpszClassName = className.data();
				winClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
				winClass.hInstance = GetModuleHandleW(0);

				if (!::RegisterClassExW(&winClass))
				{
					::printf("RegisterClassExW Error : %d \n", GetLastError());
					return GReturn::INTERFACE_UNSUPPORTED;
				}

				DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
				RECT adjustedRect = { 0, 0, m_WindowWidth, m_WindowHeight };
				if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0) // Creates a rect for a window frame that can fit the desired client size.
				{
					::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
					return GReturn::FAILURE;
				}

#ifndef NDEBUG
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING_LONG
#else
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING
#endif
				wndHandle = ::CreateWindowW(className.data(), L"" GATEWARE_WINDOW_NAME, windowsStyle, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, NULL, NULL, GetModuleHandleW(0), this);
#undef GATEWARE_WINDOW_NAME

				if (wndHandle && (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED || m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS))
				{
					if (::ShowWindow(wndHandle, SW_MAXIMIZE) != 0)
					{
						::printf("ShowWindow Error Message : %d \n", GetLastError());
						return GReturn::FAILURE;
					}
					else
					{
						return GReturn::SUCCESS;
					}
				}
				else if (wndHandle && m_WindowStyle != SYSTEM::GWindowStyle::MINIMIZED)
				{
					if (::ShowWindow(wndHandle, SW_SHOWDEFAULT) != 0)
					{
						::printf("ShowWindow Error Message : %d \n", GetLastError());
						return GReturn::FAILURE;
					}
					else
					{
						return GReturn::SUCCESS;
					}
				}
				else if (wndHandle && m_WindowStyle == SYSTEM::GWindowStyle::MINIMIZED)
				{
					::ShowWindow(wndHandle, SW_MINIMIZE);
					return GReturn::SUCCESS;
				}
				return GReturn::FAILURE;
			}

			DWORD GetWindowsStyle(GW::SYSTEM::GWindowStyle _style)
			{
				switch (m_WindowStyle)
				{
				case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED:
				case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERED:
					return WS_OVERLAPPEDWINDOW;

				case GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
				case GW::SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
					return WS_POPUP;

				case GW::SYSTEM::GWindowStyle::MINIMIZED:
					return WS_MINIMIZE;

				case GW::SYSTEM::GWindowStyle::WINDOWEDLOCKED:
					return  WS_OVERLAPPEDWINDOW ^ WS_SIZEBOX ^ WS_MAXIMIZEBOX;
				}
				return 0;
			}

		public:
			~GWindowImplementation()
			{
				if (!destroyEventIsSentByWndProc)
				{
					if (m_IconBig)
						DestroyIcon(m_IconBig);

					if (m_IconSmall)
						DestroyIcon(m_IconSmall);

					RECT windowRect = { 0,0,0,0 };
					RECT clientRect = { 0,0,0,0 };
					if (wndHandle)
					{
						::GetWindowRect(wndHandle, &windowRect);
						::GetClientRect(wndHandle, &clientRect);
					}

					m_EventData.eventFlags = Events::DESTROY;
					m_EventData.height = windowRect.bottom - windowRect.top;
					m_EventData.width = windowRect.right - windowRect.left;
					m_EventData.clientHeight = clientRect.bottom - clientRect.top;
					m_EventData.clientWidth = clientRect.right - clientRect.left;
					m_EventData.windowX = windowRect.left;
					m_EventData.windowY = windowRect.top;
					m_EventData.windowHandle = wndHandle;

					m_GEvent.Write(m_EventData.eventFlags, m_EventData);
					Push(m_GEvent);
				}
				if (!wndHandle)
					return;
				::SetWindowLongPtr(wndHandle, GWLP_USERDATA, reinterpret_cast<LPARAM>(nullptr));
				::DestroyWindow(wndHandle);
				wndHandle = nullptr;
				if (!::UnregisterClassW(className.data(), GetModuleHandleW(0)))
				{
					::printf("UnregisterClassW Error Message : %d \n", GetLastError());
				}
			}

			GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				this->SetInteralData(_x, _y, _width, _height, _style);
				return OpenWindow();
			}

			GReturn ProcessWindowEvents() override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				MSG msg;
				ZeroMemory(&msg, sizeof(MSG));
				// Processes all messages in the current queue 
				while (PeekMessage(&msg, wndHandle, 0, 0, PM_REMOVE))
				{
					//Translate messages
					TranslateMessage(&msg);
					//Send to WindowProc
					DispatchMessage(&msg);
				}

				m_EventData.eventFlags = Events::EVENTS_PROCESSED;
				m_GEvent.Write(m_EventData.eventFlags, m_EventData);
				Push(m_GEvent);

				return GReturn::SUCCESS;
			}

			GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style) override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				SYSTEM::GWindowStyle previousStyle = m_WindowStyle;
				SetInteralData(_x, _y, _width, _height, _style);

				switch (m_WindowStyle)
				{
				case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
				{
					bool reconfiguringFromMinimizedState = (previousStyle == SYSTEM::GWindowStyle::MINIMIZED);

					if (reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_RESTORE); // Restore the window prior to making changes to it. Otherwise, they may not take effect.

					DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
					::SetWindowLongPtr(wndHandle, GWL_STYLE, windowsStyle);

					RECT adjustedRect = { 0, 0, m_WindowWidth, m_WindowHeight };
					if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0)
					{
						::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
						return GReturn::FAILURE;
					}

					BOOL winRet = ::SetWindowPos(wndHandle, nullptr, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
					if (winRet == 0)
					{
						::printf("SetWindowPos Error : %d \n", GetLastError());
						return  GReturn::FAILURE;
					}

					if (!reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_SHOW);
					SetInteralData(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, SYSTEM::GWindowStyle::WINDOWEDBORDERED);
				}
				break;

				case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
				{
					bool reconfiguringFromMinimizedState = (previousStyle == SYSTEM::GWindowStyle::MINIMIZED);

					if (reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_RESTORE);

					DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
					::SetWindowLongPtr(wndHandle, GWL_STYLE, windowsStyle);

					RECT adjustedRect = { 0, 0, m_WindowWidth, m_WindowHeight };
					if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0)
					{
						::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
						return GReturn::FAILURE;
					}

					BOOL winRet = ::SetWindowPos(wndHandle, nullptr, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, SWP_SHOWWINDOW);
					if (winRet == 0)
					{
						::printf("SetWindowPos Error : %d \n", GetLastError());
						return  GReturn::FAILURE;
					}

					if (!reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_SHOW);
					SetInteralData(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, SYSTEM::GWindowStyle::WINDOWEDBORDERLESS);
				}
				break;

				case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
				{
					bool reconfiguringFromMinimizedState = (previousStyle == SYSTEM::GWindowStyle::MINIMIZED);

					if (reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_RESTORE);

					DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
					::SetWindowLongPtr(wndHandle, GWL_STYLE, windowsStyle);

					RECT adjustedRect = { 0, 0, m_WindowWidth, m_WindowHeight };
					if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0)
					{
						::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
						return GReturn::FAILURE;
					}

					BOOL winRet = ::SetWindowPos(wndHandle, nullptr, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
					if (winRet == 0)
					{
						::printf("SetWindowPos Error : %d \n", GetLastError());
						return  GReturn::FAILURE;
					}

					if (!reconfiguringFromMinimizedState)
						::ShowWindow(wndHandle, SW_SHOW);
					SetInteralData(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, SYSTEM::GWindowStyle::WINDOWEDLOCKED);
				}
				break;

				case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
				{
					RECT windowRect;
					::GetWindowRect(wndHandle, &windowRect);

					::SetWindowLongPtr(wndHandle, GWL_STYLE, WS_OVERLAPPEDWINDOW);
					BOOL winRet = ::SetWindowPos(wndHandle, nullptr, windowRect.left, windowRect.top, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, SWP_NOREDRAW);
					if (winRet == 0)
						return  GReturn::FAILURE;

					::ShowWindow(wndHandle, SW_MAXIMIZE);
					::GetWindowRect(wndHandle, &windowRect);
					SetInteralData(0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, SYSTEM::GWindowStyle::FULLSCREENBORDERED);
				}
				break;

				case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
				{
					RECT windowRect;
					::GetWindowRect(wndHandle, &windowRect);

					::SetWindowLongPtr(wndHandle, GWL_STYLE, WS_POPUP);
					BOOL winRet = ::SetWindowPos(wndHandle, nullptr, windowRect.left, windowRect.top, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, SWP_NOREDRAW);
					if (winRet == 0)
						return  GReturn::FAILURE;

					::ShowWindow(wndHandle, SW_MAXIMIZE);
					::GetWindowRect(wndHandle, &windowRect);
					SetInteralData(0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
				}
				break;

				case SYSTEM::GWindowStyle::MINIMIZED:
				{
					::ShowWindow(wndHandle, SW_MINIMIZE);
					SetInteralData(0, 0, m_WindowWidth, m_WindowHeight, SYSTEM::GWindowStyle::MINIMIZED);
				}
				break;
				}
				return  GReturn::SUCCESS;
			}

			GReturn SetWindowName(const char* _newName) override
			{
				if (_newName == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (wndHandle == nullptr || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				return ::SetWindowTextW(wndHandle, internal_gw::UTFStringConverter(_newName).c_str()) ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				if (_argbPixels == nullptr)
					return GReturn::INVALID_ARGUMENT;
				else if (_width <= 0 || _height <= 0)
					return GReturn::INVALID_ARGUMENT;

				const bool usingBigIcon = _width > 16 || _height > 16;

				if (usingBigIcon)
				{
					if (m_IconBig)
					{
						DestroyIcon(m_IconBig);
						ZeroMemory(&m_IconBig, sizeof(HICON));
					}
				}
				else
				{
					if (m_IconSmall)
					{
						DestroyIcon(m_IconSmall);
						ZeroMemory(&m_IconSmall, sizeof(HICON));
					}
				}

				ICONINFO iconInfo = { TRUE, NULL, NULL, NULL, NULL };

				iconInfo.hbmColor = CreateBitmap(_width, _height, 1, 32, _argbPixels);
				if (!iconInfo.hbmColor)
					return GReturn::FAILURE;

				HDC dc = GetDC(wndHandle);
				iconInfo.hbmMask = CreateCompatibleBitmap(dc, _width, _height);
				DeleteDC(dc);

				if (!iconInfo.hbmMask)
					return GReturn::FAILURE;

				if (usingBigIcon)
				{
					m_IconBig = CreateIconIndirect(&iconInfo);

					if (!m_IconBig)
					{
						ZeroMemory(&m_IconBig, sizeof(HICON));
						return GReturn::FAILURE;
					}

					SendMessage(wndHandle, WM_SETICON, ICON_BIG, (LPARAM)m_IconBig);

					// This fixes a few issues that occur if m_IconSmall icon is set.
					if (m_IconSmall)
					{						
						SendMessage(wndHandle, WM_SETICON, ICON_SMALL, NULL); // Needed so the taskbar icon will change to m_IconBig.
						SendMessage(wndHandle, WM_SETICON, ICON_SMALL, (LPARAM)m_IconSmall); // Needed to keep the window icon as m_IconSmall.
					}
				}
				else
				{
					m_IconSmall = CreateIconIndirect(&iconInfo);

					if (!m_IconSmall)
					{
						ZeroMemory(&m_IconSmall, sizeof(HICON));
						return GReturn::FAILURE;
					}

					SendMessage(wndHandle, WM_SETICON, ICON_SMALL, (LPARAM)m_IconSmall);
				}

				return GReturn::SUCCESS;
			}

			GReturn MoveWindow(int _x, int _y) override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return  GReturn::FAILURE;

				DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
				RECT adjustedRect = { 0, 0, m_WindowWidth, m_WindowHeight };
				if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0)
				{
					::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
					return GReturn::FAILURE;
				}

				SetInteralData(_x, _y, m_WindowWidth, m_WindowHeight, m_WindowStyle);
				return ::SetWindowPos(wndHandle, nullptr, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, SWP_SHOWWINDOW) ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn ResizeWindow(int _width, int _height) override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return  GReturn::FAILURE;

				DWORD windowsStyle = GetWindowsStyle(m_WindowStyle);
				RECT adjustedRect = { 0, 0, _width, _height };
				if (::AdjustWindowRect(&adjustedRect, windowsStyle, FALSE) == 0)
				{
					::printf("AdjustWindowRect Error Message : %d \n", GetLastError());
					return GReturn::FAILURE;
				}

				SetInteralData(m_WindowX, m_WindowY, _width, _height, m_WindowStyle);
				return ::SetWindowPos(wndHandle, nullptr, m_WindowX, m_WindowY, adjustedRect.right - adjustedRect.left, adjustedRect.bottom - adjustedRect.top, SWP_SHOWWINDOW) ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn Maximize() override
			{
				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERED || 
					m_WindowStyle == SYSTEM::GWindowStyle::MINIMIZED || 
					m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDLOCKED)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERED);
				else if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
				return GReturn::REDUNDANT;
			}

			GReturn Minimize() override
			{
				return ChangeWindowStyle(SYSTEM::GWindowStyle::MINIMIZED);
			}

			GReturn ChangeWindowStyle(SYSTEM::GWindowStyle _style) override
			{
				return ReconfigureWindow(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, _style);
			}

			GReturn GetWidth(unsigned int& _outWidth) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return  GReturn::FAILURE;

				RECT windowRect;
				::GetWindowRect(wndHandle, &windowRect);
				_outWidth = windowRect.right - windowRect.left;
				return  GReturn::SUCCESS;
			}

			GReturn GetHeight(unsigned int& _outHeight) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				RECT windowRect;
				::GetWindowRect(wndHandle, &windowRect);
				_outHeight = windowRect.bottom - windowRect.top;
				return GReturn::SUCCESS;
			}

			GReturn GetClientWidth(unsigned int& _outClientWidth) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				RECT clientRect;
				::GetClientRect(wndHandle, &clientRect);
				_outClientWidth = clientRect.right - clientRect.left;
				return GReturn::SUCCESS;
			}

			GReturn GetClientHeight(unsigned int& _outClientHeight) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				RECT clientRect;
				::GetClientRect(wndHandle, &clientRect);
				_outClientHeight = clientRect.bottom - clientRect.top;
				return GReturn::SUCCESS;
			}

			GReturn GetX(unsigned int& _outX) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				RECT windowRect;
				::GetWindowRect(wndHandle, &windowRect);
				_outX = windowRect.left;
				return GReturn::SUCCESS;
			}

			GReturn GetY(unsigned int& _outY) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				RECT windowRect;
				::GetWindowRect(wndHandle, &windowRect);
				_outY = windowRect.top;
				return GReturn::SUCCESS;
			}

			GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				POINT clientAreaTopLeft = { 0, 0 };
				::ClientToScreen(wndHandle, &clientAreaTopLeft);
				unsigned int x;
				unsigned int y;
				GetX(x);
				GetY(y);
				_outX = clientAreaTopLeft.x - x;
				_outY = clientAreaTopLeft.y - y;
				return GReturn::SUCCESS;
			}

			GReturn GetWindowHandle(SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				_outUniversalWindowHandle.window = wndHandle;
				_outUniversalWindowHandle.display = nullptr;
				return GReturn::SUCCESS;
			}

			GReturn IsFullscreen(bool& _outIsFullscreen) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;

				unsigned int height = 0;
				unsigned int width = 0;
				int mainWindowWidth = 0;
				int mainWindowHeight = 0;
				int borderHeight = 0;
				int resizeBarWidth = 0;
				int resizeBarHeight = 0;

				if (G_FAIL(GetWidth(width)) || G_FAIL(GetHeight(height)))
				{
					return GReturn::FAILURE;
				}
				// mainWindowWidth and mainWindowHeight does not refer to application window,
				// but the main desktop window
				mainWindowWidth = ::GetSystemMetrics(SM_CXFULLSCREEN);
				mainWindowHeight = ::GetSystemMetrics(SM_CYFULLSCREEN);
				borderHeight = ::GetSystemMetrics(SM_CYCAPTION);
				resizeBarWidth = ::GetSystemMetrics(SM_CXBORDER);
				resizeBarHeight = ::GetSystemMetrics(SM_CYBORDER);

				if (((int)width + resizeBarWidth) >= mainWindowWidth && ((int)height + borderHeight + resizeBarHeight) >= mainWindowHeight)
					_outIsFullscreen = true;
				else
					_outIsFullscreen = false;

				return GReturn::SUCCESS;
			}

			GReturn IsFocus(bool& _outIsFocus) const override
			{
				if (!wndHandle || destroyEventIsSentByWndProc)
					return GReturn::FAILURE;
				// determine if HWND is the foreground window
				_outIsFocus = (wndHandle == GetForegroundWindow());
				return GReturn::SUCCESS;
			}
		};
	}
}
