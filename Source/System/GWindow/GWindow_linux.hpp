#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>
#include <thread>
#include "../../Shared/GVersion.hpp"

// Depending on the hardware, the default color buffer size isn't always 32-bit.
// GLX is needed to create a window for OpenGL with a 32-bit color buffer.
#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GOPENGLSURFACE)
#include <GL/glx.h>
#endif

//#define GWINDOW_DEBUG_LINUX_OUTPUT_EVENTS // Enables debug output of GWindow events.
//#define GWINDOW_DEBUG_LINUX_OUTPUT_EVENT_DATA // Enables debug output of GWindow event data.
//#define GWINDOW_DEBUG_LINUX_OUTPUT_EVENTS_EVENTS_PROCESSED // Enable output of events processed.
//#define GWINDOW_DEBUG_LINUX_OUTPUT_X11_EVENTS // Enables debug output of X11 events.
//#define GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWPOSITIONANDSIZE // Enables debug output of calls to GetWindowPositionAndSize().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_ISWINDOWDIFFERENT // Enables debug output of calls to IsWindowDifferent().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_TRAVERSEWINDOWTREEFORRESERVEDSPACE // Enables debug output of calls to TraverseWindowTreeForReservedSpace().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_GETUNRESERVEDSPACE // Enables debug output of calls to GetUnreservedSpace().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_SETUPATOM // Enables debug output of calls to SetupAtom().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_XERROREVENTS // Enables debug output of calls to HandleError().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWBORDERSIZES // Enables debug output of calls to GetWindowBorderSizes().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_GETCLIENTTOPLEFT // Enables debug output of calls to GetClientTopLeft().
//#define GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES // Enables debug output of retries made by G_RETRY_IF_FAILED().

// Retries a function that returns a bool if the return value is false. This macro was specifically
// made for calling X11 functions that may fail at first but then return success when called later.
// This is one of the many frustrations of working with X server. It forced me to do this. I don't
// like it. Please, find a better way.
#define G_RETRY_IF_FAILED(function_call)\
	constexpr unsigned int SLEEP_TIME = 100;\
	constexpr unsigned int MAX_RETRIES = 5;\
	unsigned int g_retries = 0;\
	bool g_success = false;\
	do\
	{\
		g_success = function_call;\
		if (!g_success)\
		{\
			std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_TIME));\
			++g_retries;\
		}\
	}\
	while (!g_success && g_retries <= MAX_RETRIES)

#define _NET_WM_STATE_ADD 1l
#define _NET_WM_STATE_REMOVE 0l

namespace GW
{
	namespace I
	{
		class GWindowImplementation :	public virtual GWindowInterface,
										public GEventGeneratorImplementation
		{
		private:
			Display* display;
			Window window;

			bool destroyEventIsSentByXButton;
			bool windowIsRunning;
			struct
			{
				Atom _NET_WM_STATE;
				Atom _NET_WM_STATE_HIDDEN;
				Atom _NET_WM_STATE_FOCUSED;
				Atom _NET_WM_STATE_MAXIMIZED_VERT;
				Atom _NET_WM_STATE_MAXIMIZED_HORZ;
				Atom _NET_WM_STATE_FULLSCREEN;
				Atom _NET_WM_STATE_ABOVE;
				Atom _NET_WM_STATE_SKIP_TASKBAR;
				Atom _NET_WM_STATE_SKIP_PAGER;
				Atom _NET_WM_STATE_MODAL;
				Atom _NET_WM_MOVERESIZE;
				Atom _NET_WM_ALLOWED_ACTIONS;
				Atom _NET_WM_ACTION_FULLSCREEN;
				Atom _NET_WM_NAME;
				Atom _NET_WM_ICON_NAME;
				Atom _NET_WM_ICON;
				Atom _NET_WM_PING;
				Atom _NET_WM_WINDOW_OPACITY;
				Atom _NET_WM_USER_TIME;
				Atom _NET_ACTIVE_WINDOW;
				Atom _NET_FRAME_EXTENTS;
				Atom _MOTIF_WM_HINTS;
				Atom _NET_WM_STRUT;
				Atom WM_DELETE_WINDOW;
			} atoms;

			struct
			{
				unsigned long flags;
				unsigned long functions;
				unsigned long decorations;
				long inputMode;
				unsigned long status;
			} hints;

			int m_WindowX;
			int m_WindowY;
			int m_WindowWidth;
			int m_WindowHeight;
			SYSTEM::GWindowStyle m_WindowStyle;
			
			int prevX; int prevY;
			unsigned int prevWidth; unsigned int prevHeight;
			
			GEvent m_GEvent;
			Events m_prevEvent;

			unsigned long* m_iconPixels;
			int m_iconPixelsCount;

			// Syncs window operations with the X server. This is primarily used to ensure that the window is properly
			// resized or maximized before the next operation is performed. This is necessary because the X server may
			// not have finished processing the previous operation before the next operation is performed.
			// NOTE: Timeout is in milliseconds and 100 is normally fine, but maximized can take abnormally long to process.
			//		 So, use a value higher than 100 if you're having issues with maximized.
			void X11_SyncWindow(const int timeout = 100)
			{
				const unsigned int max_retries = timeout / 10;
				unsigned int retries = 0;
				while (true)
				{
					XSync(display, False);
					ProcessWindowEvents();
					if (XPending(display) == 0)
						break;

					std::this_thread::sleep_for(std::chrono::milliseconds(timeout / 10));
					++retries;
					if (retries >= max_retries)
						break;
				}
			}
			
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_EVENTS)
			void DebugOutputGWindowEvent(EVENT_DATA _eventData)
			{
				switch (_eventData.eventFlags)
				{
				case Events::MINIMIZE: 			printf("GWindowEvent: MINIMIZE(%d)\n", static_cast<int>(_eventData.eventFlags)); 			break;
				case Events::MAXIMIZE: 			printf("GWindowEvent: MAXIMIZE(%d)\n", static_cast<int>(_eventData.eventFlags));			break;
				case Events::RESIZE: 			printf("GWindowEvent: RESIZE(%d)\n", static_cast<int>(_eventData.eventFlags));				break;
				case Events::MOVE: 				printf("GWindowEvent: MOVE(%d)\n", static_cast<int>(_eventData.eventFlags));				break;
				case Events::DISPLAY_CLOSED: 	printf("GWindowEvent: DISPLAY_CLOSED(%d)\n", static_cast<int>(_eventData.eventFlags));		break;
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_EVENTS_EVENTS_PROCESSED)
				case Events::EVENTS_PROCESSED:	printf("GWindowEvent: EVENTS_PROCESSED(%d)\n", static_cast<int>(_eventData.eventFlags));	break;
#endif
				case Events::DESTROY: 			printf("GWindowEvent: DESTROY(%d)\n", static_cast<int>(_eventData.eventFlags));				break;
				default: 
					break;
				}
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_EVENT_DATA)
				if (_eventData.eventFlags != Events::EVENTS_PROCESSED)
				{
					printf("x:%-5d y:%-5d width:%-5u height:%-5u clientWidth:%-5u clientHeight:%-5u\n",
						   _eventData.windowX,
						   _eventData.windowY,
						   _eventData.width,
						   _eventData.height,
						   _eventData.clientWidth,
						   _eventData.clientHeight);
				}
#endif
			}
#else
			void DebugOutputGWindowEvent(EVENT_DATA _eventData) {}
#endif

#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_X11_EVENTS)
			void DebugOutputX11Event(XEvent& _event)
			{
				switch (_event.type)
				{
					case Expose: 			printf("X11 XEvent: Expose(%d)\n", _event.type);			break;
					case SelectionClear: 	printf("X11 XEvent: SelectionClear(%d)\n", _event.type);	break;
					case SelectionNotify: 	printf("X11 XEvent: SelectionNotify(%d)\n", _event.type);	break;
					case SelectionRequest: 	printf("X11 XEvent: SelectionRequest(%d)\n", _event.type);	break;
					case NoExpose: 			printf("X11 XEvent: NoExpose(%d)\n", _event.type);			break;
					case ButtonPress: 		printf("X11 XEvent: ButtonPress(%d)\n", _event.type);		break;
					case PropertyNotify:
					{
						Atom actual_type = 0;
						int actual_format = 0;
						unsigned long nitems = 0;
						unsigned long bytes_after = 0;
						unsigned char* propRet = nullptr;
						
						int status = XGetWindowProperty(_event.xproperty.display, _event.xproperty.window, prop_type, 0L, sizeof(Atom),
							false, AnyPropertyType, &actual_type, &actual_format, &nitems, &bytes_after, &propRet);
						
						if (status == Success && propRet && nitems > 0)
						{
							Atom prop = reinterpret_cast<Atom*>(propRet)[0];
							XFree(propRet);
							
							if (prop == prop_hidden)
								printf("X11 XEvent: PropertyNotify(%d), Atom: prop_hidden(%d)\n", _event.type, prop);
							else if (prop == prop_full)
								printf("X11 XEvent: PropertyNotify(%d), Atom: prop_full(%d)\n", _event.type, prop);
							else if (prop == prop_hMax)
								printf("X11 XEvent: PropertyNotify(%d), Atom: prop_hMax(%d)\n", _event.type, prop);
							else if (prop == prop_vMax)
								printf("X11 XEvent: PropertyNotify(%d), Atom: prop_vMax(%d)\n", _event.type, prop);
							else
								printf("X11 XEvent: PropertyNotify(%d), Atom: unknown(%d)\n", _event.type, prop);
						}
						else
						{
							printf("X11 XEvent: PropertyNotify(%d)\n", _event.type);
						}
						break;
					}
					case ConfigureNotify:
						printf("X11 XEvent: ConfigureNotify(%d)\n", _event.type);
						break;
					case ClientMessage:
					{
						if (_event.xclient.message_type == prop_hidden)
							printf("X11 XEvent: ClientMessage(%d), message_type: prop_hidden(%d)\n", _event.type, _event.xclient.message_type);
						else if (_event.xclient.message_type == prop_full)
							printf("X11 XEvent: ClientMessage(%d), message_type: prop_full(%d)\n", _event.type, _event.xclient.message_type);
						else if (_event.xclient.data.l[0] == wmDeleteMessage)
							printf("X11 XEvent: ClientMessage(%d), data.l[0]: wmDeleteMessage(%d)\n", _event.type, _event.xclient.data.l[0]);
						else
							printf("X11 XEvent: ClientMessage(%d), message_type: unknown(%d), data.l[0]: unknown(%d)\n", _event.type, _event.xclient.message_type, _event.xclient.data.l[0]);
						break;
					}
					default:
						break;
				}
			}
#else
			void DebugOutputX11Event(XEvent& _event) {}
#endif

			void SetInteralData(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				m_WindowX = _x;
				m_WindowY = _y;
				m_WindowWidth = _width;
				m_WindowHeight = _height;
				m_WindowStyle = _style;
			}

			GReturn OpenWindow()
			{
				if (display && window)
					return GReturn::REDUNDANT;

				// Initialize the threads in the event they aren't already.
				XInitThreads();
				
				display = XOpenDisplay(NULL);
				if (!display)
					return GReturn::FAILURE;

				int screen = DefaultScreen(display);
				int depth = DefaultDepth(display, screen);
				int valueMask = CWOverrideRedirect | CWBackPixel | CWBorderPixel | CWEventMask | CWBackingStore;

				XSetWindowAttributes attributes;
				attributes.override_redirect = False; // Unless we want to create a window that bypasses the window manager.
				attributes.backing_store     = NotUseful;
				attributes.background_pixel  = XWhitePixel(display, screen);
				attributes.border_pixel      = XBlackPixel(display, screen);
				attributes.event_mask        = SubstructureNotifyMask | PropertyChangeMask | ExposureMask | StructureNotifyMask;

				m_iconPixels = nullptr;
				m_iconPixelsCount = 0;

// If OpenGL is enabled then we need to make sure we create a window with a 32-bit color buffer.
#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GOPENGLSURFACE)

				constexpr int colorBufferSize = 32;

				XVisualInfo visualTemplate;
				visualTemplate.screen = screen;

				int visualListCount = 0;
				XVisualInfo *visualList = XGetVisualInfo(display, VisualScreenMask, &visualTemplate, &visualListCount);

				// Search through the list of visuals for a match with our target depth and color buffer.
				XVisualInfo* visualInfo = nullptr;
				for (int i = 0; i < visualListCount; ++i)
				{
					if (visualList[i].depth == depth)
					{
						int bufferSizeAtIndex;
						glXGetConfig(display, &visualList[i], GLX_BUFFER_SIZE, &bufferSizeAtIndex);

						if (bufferSizeAtIndex == colorBufferSize)
						{
							visualInfo = &visualList[i];
							break;
						}
					}
				}

				if (!visualInfo)
				{
					XFree(visualList);
					XCloseDisplay(display);
					return GReturn::FAILURE;
				}

				valueMask |= CWColormap;
				attributes.colormap = XCreateColormap(display, XDefaultRootWindow(display), visualInfo->visual, AllocNone);

				window = XCreateWindow(display, XRootWindow(display, screen), m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, 5,
					visualInfo->depth, InputOutput, visualInfo->visual, valueMask, &attributes);

				// No longer need visual info
				XFree(visualList);
				visualInfo = nullptr;

#else // Use the color buffer size from the parent if OpenGL is not being used.

				window = XCreateWindow(display,
										RootWindow(display, screen),
										m_WindowX,
										m_WindowY,
										m_WindowWidth,
										m_WindowHeight,
										5,
										depth,
										InputOutput,
										CopyFromParent,
										valueMask,
										&attributes);

#endif

				if (!window)
				{
					XCloseDisplay(display);
					return GReturn::FAILURE;
				}

				prevX = m_WindowX;
				prevY = m_WindowY;
				prevWidth = m_WindowWidth;
				prevHeight = m_WindowHeight;

#ifdef _DEBUG
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING_LONG
#else
#define GATEWARE_WINDOW_NAME GATEWARE_VERSION_STRING
#endif
				XStoreName(display, window, GATEWARE_WINDOW_NAME);
#undef GATEWARE_WINDOW_NAME

				// Initialize the window size, position, and hints.
				XSizeHints *size_hints = XAllocSizeHints();
				size_hints->flags      = PSize | PPosition;
				size_hints->x          = m_WindowX;
				size_hints->y          = m_WindowY;
				size_hints->width      = m_WindowWidth;
				size_hints->height     = m_WindowHeight;

				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDLOCKED)
				{
					size_hints->flags |= PMinSize | PMaxSize;
					size_hints->min_width = size_hints->max_width = m_WindowWidth;
					size_hints->min_height = size_hints->max_height = m_WindowHeight;
				}

				XSetWMNormalHints(display, window, size_hints);

				const Atom _NET_WM_WINDOW_TYPE = XInternAtom(display, "_NET_WM_WINDOW_TYPE", False);
				Atom wintype = XInternAtom(display, "_NET_WM_WINDOW_TYPE_NORMAL", False); // This can be changed in the future to support other window types.
				XChangeProperty(display, window, _NET_WM_WINDOW_TYPE, XA_ATOM, 32, PropModeReplace, reinterpret_cast<unsigned char *>(&wintype), 1);

				Atom protocols[3];
				int proto_count = 0;
				protocols[proto_count++] = XInternAtom(display, "WM_DELETE_WINDOW", False); // Allow window to be deleted by the WM
				protocols[proto_count++] = XInternAtom(display, "_NET_WM_PING", False); // This is used to check if the window is still alive.
				XSetWMProtocols(display, window, protocols, proto_count);
				XSetErrorHandler(HandleError);

				// Free the allocated hints.
				XFree(size_hints);

				if (XMapWindow(display, window))
				{
					m_prevEvent = Events::EVENTS_PROCESSED;
					SetupAtom();
					
					if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS || m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
					{
						hints.flags = 2;
						hints.decorations = 0;
						hints.functions = 0;
						XChangeProperty(display, window, atoms._MOTIF_WM_HINTS, atoms._MOTIF_WM_HINTS, 32, PropModeReplace, reinterpret_cast<unsigned char *>(&hints), 5);
					}
					else if (m_WindowStyle == SYSTEM::GWindowStyle::MINIMIZED)
					{
						if (!XIconifyWindow(display, window, DefaultScreen(display)))
						{
							XDestroyWindow(display, window);
							XCloseDisplay(display);
							return GReturn::FAILURE;
						}
					}

					if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
					{
						// Fullscreen window
						XEvent ev               = {};
						ev.xany.type            = ClientMessage;
						ev.xclient.message_type = atoms._NET_WM_STATE;
						ev.xclient.format       = 32;
						ev.xclient.window       = window;
						ev.xclient.data.l[0]    = _NET_WM_STATE_ADD;
						ev.xclient.data.l[1]    = atoms._NET_WM_STATE_FULLSCREEN;
						ev.xclient.data.l[3]    = 0;
						XSendEvent(display, RootWindow(display, screen), False, SubstructureNotifyMask | SubstructureRedirectMask, &ev);
					}
					else if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
					{
						// Maximize window
						XEvent ev               = {};
						ev.xany.type            = ClientMessage;
						ev.xclient.message_type = atoms._NET_WM_STATE;
						ev.xclient.format       = 32;
						ev.xclient.window       = window;
						ev.xclient.data.l[0]    = _NET_WM_STATE_ADD;
						ev.xclient.data.l[1]    = atoms._NET_WM_STATE_MAXIMIZED_VERT;
						ev.xclient.data.l[2]    = atoms._NET_WM_STATE_MAXIMIZED_HORZ;
						ev.xclient.data.l[3]    = 0;
						XSendEvent(display, RootWindow(display, screen), False, SubstructureNotifyMask | SubstructureRedirectMask, &ev);
					}

					X11_SyncWindow(); // This is a nice spot to put this

					windowIsRunning = true;
					destroyEventIsSentByXButton = false;

					return GReturn::SUCCESS;
				}

				XDestroyWindow(display, window);
				XCloseDisplay(display);
				return GReturn::FAILURE;
			}
			
			// Handles errors reported by X. This is needed to capture unsuccessful calls
			// to XQueryTree and prevent the error from terminating the program.
			static int HandleError(Display* _display, XErrorEvent* _errorEvent)
			{
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_XERROREVENTS)
				constexpr unsigned int MSG_LEN = 256;
				char msg[MSG_LEN];
				XGetErrorText(_display, _errorEvent->error_code, msg, MSG_LEN);
				printf("XErrorEvent(): %s\n", msg);
#endif
				return 0;
			}
			
			void SetupAtom()
			{
				atoms._NET_WM_STATE = XInternAtom(display, "_NET_WM_STATE", False);
				atoms._NET_WM_STATE_HIDDEN = XInternAtom(display, "_NET_WM_STATE_HIDDEN", False);
				atoms._NET_WM_STATE_FOCUSED = XInternAtom(display, "_NET_WM_STATE_FOCUSED", False);
				atoms._NET_WM_STATE_MAXIMIZED_VERT = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_VERT", False);
				atoms._NET_WM_STATE_MAXIMIZED_HORZ = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
				atoms._NET_WM_STATE_FULLSCREEN = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);
				atoms._NET_WM_STATE_ABOVE = XInternAtom(display, "_NET_WM_STATE_ABOVE", False);
				atoms._NET_WM_STATE_SKIP_TASKBAR = XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
				atoms._NET_WM_STATE_SKIP_PAGER = XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False);
				atoms._NET_WM_STATE_MODAL = XInternAtom(display, "_NET_WM_STATE_MODAL", False);
				atoms._NET_WM_MOVERESIZE = XInternAtom(display, "_NET_WM_MOVERESIZE", False);
				atoms._NET_WM_ALLOWED_ACTIONS = XInternAtom(display, "_NET_WM_ALLOWED_ACTIONS", False);
				atoms._NET_WM_ACTION_FULLSCREEN = XInternAtom(display, "_NET_WM_ACTION_FULLSCREEN", False);
				atoms._NET_WM_NAME = XInternAtom(display, "_NET_WM_NAME", False);
				atoms._NET_WM_ICON_NAME = XInternAtom(display, "_NET_WM_ICON_NAME", False);
				atoms._NET_WM_ICON = XInternAtom(display, "_NET_WM_ICON", False);
				atoms._NET_WM_PING = XInternAtom(display, "_NET_WM_PING", False);
				atoms._NET_WM_WINDOW_OPACITY = XInternAtom(display, "_NET_WM_WINDOW_OPACITY", False);
				atoms._NET_WM_USER_TIME = XInternAtom(display, "_NET_WM_USER_TIME", False);
				atoms._NET_ACTIVE_WINDOW = XInternAtom(display, "_NET_ACTIVE_WINDOW", False);
				atoms._NET_FRAME_EXTENTS = XInternAtom(display, "_NET_FRAME_EXTENTS", False);
				atoms._MOTIF_WM_HINTS = XInternAtom(display, "_MOTIF_WM_HINTS", False);
				atoms._NET_WM_STRUT = XInternAtom(display, "_NET_WM_STRUT", False);
				atoms.WM_DELETE_WINDOW = XInternAtom(display, "WM_DELETE_WINDOW", False);

#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_SETUPATOM)
				printf("Configured Atoms\n");
				printf("_NET_WM_STATE: %lu\n", atoms._NET_WM_STATE);
				printf("_NET_WM_STATE_HIDDEN: %lu\n", atoms._NET_WM_STATE_HIDDEN);
				printf("_NET_WM_STATE_FOCUSED: %lu\n", atoms._NET_WM_STATE_FOCUSED);
				printf("_NET_WM_STATE_MAXIMIZED_VERT: %lu\n", atoms._NET_WM_STATE_MAXIMIZED_VERT);
				printf("_NET_WM_STATE_MAXIMIZED_HORZ: %lu\n", atoms._NET_WM_STATE_MAXIMIZED_HORZ);
				printf("_NET_WM_STATE_FULLSCREEN: %lu\n", atoms._NET_WM_STATE_FULLSCREEN);
				printf("_NET_WM_STATE_ABOVE: %lu\n", atoms._NET_WM_STATE_ABOVE);
				printf("_NET_WM_STATE_SKIP_TASKBAR: %lu\n", atoms._NET_WM_STATE_SKIP_TASKBAR);
				printf("_NET_WM_STATE_SKIP_PAGER: %lu\n", atoms._NET_WM_STATE_SKIP_PAGER);
				printf("_NET_WM_STATE_MODAL: %lu\n", atoms._NET_WM_STATE_MODAL);
				printf("_NET_WM_MOVERESIZE: %lu\n", atoms._NET_WM_MOVERESIZE);
				printf("_NET_WM_ALLOWED_ACTIONS: %lu\n", atoms._NET_WM_ALLOWED_ACTIONS);
				printf("_NET_WM_ACTION_FULLSCREEN: %lu\n", atoms._NET_WM_ACTION_FULLSCREEN);
				printf("_NET_WM_NAME: %lu\n", atoms._NET_WM_NAME);
				printf("_NET_WM_ICON_NAME: %lu\n", atoms._NET_WM_ICON_NAME);
				printf("_NET_WM_ICON: %lu\n", atoms._NET_WM_ICON);
				printf("_NET_WM_PING: %lu\n", atoms._NET_WM_PING);
				printf("_NET_WM_WINDOW_OPACITY: %lu\n", atoms._NET_WM_WINDOW_OPACITY);
				printf("_NET_WM_USER_TIME: %lu\n", atoms._NET_WM_USER_TIME);
				printf("_NET_ACTIVE_WINDOW: %lu\n", atoms._NET_ACTIVE_WINDOW);
				printf("_NET_FRAME_EXTENTS: %lu\n", atoms._NET_FRAME_EXTENTS);
				printf("_MOTIF_WM_HINTS: %lu\n", atoms._MOTIF_WM_HINTS);
				printf("_NET_WM_STRUT: %lu\n", atoms._NET_WM_STRUT);
				printf("WM_DELETE_WINDOW: %lu\n", atoms.WM_DELETE_WINDOW);
#endif
			}
			// Brings the window out of the minimize (aka iconified) state to the restore state only
			// if necessary. Returns if successful.
			bool UnminimizeWindow()
			{
				XClientMessageEvent evUnMinEvent;
				memset(&evUnMinEvent, 0, sizeof evUnMinEvent);
				evUnMinEvent.type = ClientMessage;
				evUnMinEvent.window = window;
				evUnMinEvent.message_type = atoms._NET_ACTIVE_WINDOW;
				evUnMinEvent.format = 32;
				evUnMinEvent.data.l[0] = _NET_WM_STATE_ADD;
				evUnMinEvent.data.l[1] = CurrentTime;
				evUnMinEvent.data.l[2] = evUnMinEvent.data.l[3] = evUnMinEvent.data.l[4] = 0;

				return static_cast<bool>(XSendEvent(
					display, 
					RootWindow(display, XDefaultScreen(display)), 
					False,
					SubstructureRedirectMask | SubstructureNotifyMask, 
					(XEvent*)&evUnMinEvent
				));
			}
			
			// Brings the window out of the maximize state to the restore state if necessary. 
			// Returns if successful.
			bool UnmaximizeWindow()
			{
				XEvent unMaxEvent;
				unMaxEvent.type = ClientMessage;
				unMaxEvent.xclient.window = window;
				unMaxEvent.xclient.message_type = atoms._NET_WM_STATE;
				unMaxEvent.xclient.format = 32;
				unMaxEvent.xclient.data.l[0] = _NET_WM_STATE_REMOVE;
				unMaxEvent.xclient.data.l[1] = atoms._NET_WM_STATE_MAXIMIZED_HORZ;
				unMaxEvent.xclient.data.l[2] = atoms._NET_WM_STATE_MAXIMIZED_VERT;
				unMaxEvent.xclient.data.l[3] = unMaxEvent.xclient.data.l[4]  = 0;

				return static_cast<bool>(XSendEvent(
					display, 
					RootWindow(display, XDefaultScreen(display)), 
					False,
					SubstructureRedirectMask | SubstructureNotifyMask, 
					&unMaxEvent
				));
			}
			
			// Changes the decorations of the window depending on the given style. Returns if successful.
			bool StylizeWindow(SYSTEM::GWindowStyle _style)
			{
				// Minimized does not change the appearance of the window.
				if (_style == SYSTEM::GWindowStyle::MINIMIZED)
					return false;
				
				memset(&hints, 0, sizeof(hints));
				
				switch (_style)
				{
				case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
				case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
				case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
				{
					hints.flags = 2;
					hints.decorations = 5;
					break;
				}
				case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
				case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
				{
					hints.flags = 2;
					hints.decorations = 0;
					break;
				}
				}

				return static_cast<bool>(XChangeProperty(
					display,
					window,
					atoms._MOTIF_WM_HINTS,
					atoms._MOTIF_WM_HINTS,
					32,
					PropModeReplace,
					reinterpret_cast<unsigned char *>(&hints),
					5
				));
			}
			
			// Permits the window to be resized by mouse or programmatically.
			void EnableWindowResizing()
			{
				// This is the default behavior of a window.
				XSizeHints *rect = XAllocSizeHints();
				rect->flags = PMinSize | PMaxSize;
				rect->min_width = rect->min_height = 0;
				rect->max_width = rect->max_height = USHRT_MAX;
				XSetWMNormalHints(display, window, rect);
				XFree(rect);
			}
			
			// Restricts resizing of the window by mouse or programmatically.
			void DisableWindowResizing()
			{				
				XSizeHints *rect = XAllocSizeHints();
				rect->flags = PMinSize | PMaxSize;
				rect->min_width = rect->max_width = m_WindowWidth;
				rect->min_height = rect->max_height = m_WindowHeight;
				XSetWMNormalHints(display, window, rect);
				XFree(rect);
			}
			
			// Minimizes the window. Returns if successful.
			bool MinimizeWindow()
			{				
				XEvent ev;
				memset(&ev, 0, sizeof ev);
				ev.type = PropertyNotify;
				ev.xclient.window = window;
				ev.xclient.message_type = atoms._NET_WM_STATE_HIDDEN;
				ev.xclient.format = 32;
				ev.xclient.data.l[0] = _NET_WM_STATE_ADD;
				ev.xclient.data.l[1] = atoms._NET_WM_STATE_HIDDEN;
				
				return static_cast<bool>(XSendEvent(
					display, 
					window, 
					False, 
					PropertyChangeMask, 
					&ev
				));
			}
			
			// Maximizes the window. Returns if successful.
			bool MaximizeWindow()
			{
				XEvent ev;
				memset(&ev, 0, sizeof ev);
				ev.type = ClientMessage;
				ev.xclient.window = window;
				ev.xclient.message_type = atoms._NET_WM_STATE;
				ev.xclient.format = 32;
				ev.xclient.data.l[0] = _NET_WM_STATE_ADD;
				ev.xclient.data.l[1] = atoms._NET_WM_STATE_MAXIMIZED_HORZ;
				ev.xclient.data.l[2] = atoms._NET_WM_STATE_MAXIMIZED_VERT;
				
				return static_cast<bool>(XSendEvent(
					display, 
					DefaultRootWindow(display), 
					False, 
					SubstructureNotifyMask, 
					&ev
				));
			}
			
			// Moves and resize the window. Returns if successful.
			bool MoveAndResizeWindow(int _x, int _y, int _width, int _height)
			{
				if (XMoveResizeWindow(
					display,
					window,
					_x,
					_y,
					_width,
					_height
				))
				{
					// Launch an event to notify the window has been moved.
					XEvent ev;
					memset(&ev, 0, sizeof ev);
					ev.type = PropertyNotify;
					ev.xclient.window = window;
					ev.xclient.message_type = atoms._NET_WM_STATE;
					ev.xclient.format = 32; // 32-bit cardinal

					XSendEvent(display, window, False, PropertyChangeMask, &ev);

					return true;
				}

				return false;
			}
			
			// Gets the window border sizes. Returns if successful.
			bool GetWindowBorderSizes(long& _titleBarHeight, long& _bottomBorderHeight, 
										  long& _leftBorderWidth, long& _rightBorderWidth) const
			{
				if (!windowIsRunning || !display || !window)
					return false;
				
				// On Ubuntu, even if we remove the border of the window, GetWindowProperty() will
				// return a titlebar height greater than 0. To get around this we'll just set every
				// thing to 0 when we know we're using a borderless window style.
				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS ||
					m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
				{
					_leftBorderWidth = 0;
					_rightBorderWidth = 0;
					_titleBarHeight = 0;
					_bottomBorderHeight = 0;
				}
				else
				{
					// Get the window frame dimensions.
					Atom actualType;
					int actualFormat;
					unsigned long nItems;
					unsigned long bytesAfter;
					unsigned char* extents = nullptr;				
					int result = XGetWindowProperty(display, window, atoms._NET_FRAME_EXTENTS,
													0L, 4L, false, 
													AnyPropertyType, &actualType, &actualFormat,
													&nItems, &bytesAfter, &extents);
					
					if (result != Success || nItems != 4 || bytesAfter != 0 || !extents)
					{
	#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWBORDERSIZES)
						printf("GetWindowBorderSizes() Call to XGetWindowProperty() failed %d \n", result);
	#endif
						return false;
					}
					
					_leftBorderWidth = reinterpret_cast<long*>(extents)[0];
					_rightBorderWidth = reinterpret_cast<long*>(extents)[1];
					_titleBarHeight = reinterpret_cast<long*>(extents)[2];
					_bottomBorderHeight = reinterpret_cast<long*>(extents)[3];
					XFree(extents);
				}
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWBORDERSIZES)
				printf("GetWindowBorderSizes() titlebar:%d left:%d right:%d bottom:%d\n", 
						_titleBarHeight,
						_leftBorderWidth,
						_rightBorderWidth,
						_bottomBorderHeight);
#endif
				
				return true;
			}
			
			// Gets the window frame and client position and dimensions data. Returns if successful.
			bool GetWindowPositionAndSize(int& _frameX, int& _frameY, unsigned int& _frameWidth, unsigned int& _frameHeight, 
										  int& _clientX, int& _clientY, unsigned int& _clientWidth, unsigned int& _clientHeight) const
			{
				if (!windowIsRunning || !display || !window)
					return false;
				
				// Get the root.
				int x, y;
				unsigned int borderWidth, depth;
				Window child, root;
				if (!XGetGeometry(display, window, &root, &x, &y, &_clientWidth, &_clientHeight, &borderWidth, &depth))
				{
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWPOSITIONANDSIZE)
					printf("GetWindowPositionAndSize() call to XGetGeometry() failed%d\n", 0);
#endif
					return false;
				}
					
				// Bring the window coordinates into root space.
				if (!XTranslateCoordinates(display, window, root, 0, 0, &_clientX, &_clientY, &child))					
				{
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWPOSITIONANDSIZE)
					printf("XTranslateCoordinates() call to XGetGeometry() failed%d\n", 0);
#endif
					return false;
				}
				
				// Get the window frame dimensions.
				Atom actualType;
				int actualFormat;
				unsigned long nItems;
				unsigned long bytesAfter;
				unsigned char* extents = nullptr;				
				int result = XGetWindowProperty(display, window, atoms._NET_FRAME_EXTENTS,
												0L, 4L, false, 
												AnyPropertyType, &actualType, &actualFormat,
												&nItems, &bytesAfter, &extents);
				
				if (result != Success || nItems != 4 || bytesAfter != 0 || !extents)
				{
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWPOSITIONANDSIZE)
					printf("GetWindowPositionAndSize() call to XGetWindowProperty() failed%d\n", 0);
#endif
					return false;
				}
				
				long leftBorderWidth = reinterpret_cast<long*>(extents)[0];
				long rightBorderWidth = reinterpret_cast<long*>(extents)[1];
				long titleBarHeight = reinterpret_cast<long*>(extents)[2];
				long bottomBorderHeight = reinterpret_cast<long*>(extents)[3];
				XFree(extents);
				
				// X and y are at client area's top-left at the moment. We need to subtract the border to
				// get the top-left position of the frame.
				_frameX = _clientX - static_cast<int>(leftBorderWidth);
				_frameY = _clientY - static_cast<int>(titleBarHeight);
				_frameWidth =  static_cast<unsigned int>(leftBorderWidth) + _clientWidth + static_cast<unsigned int>(rightBorderWidth);
				_frameHeight = static_cast<unsigned int>(titleBarHeight) + _clientHeight + static_cast<unsigned int>(bottomBorderHeight);
				
				// On Ubuntu, even if we remove the border of the window, XGetGeometry() will
				// set _clientWidth/Height as if there were a border. To get around this we'll
				// adjust those values to what they should be. This should be okay if we know
				// we're using a borderless window style.
				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS ||
					m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
				{
					_frameWidth = _clientWidth;
					_frameHeight = _clientHeight;
				}
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETWINDOWPOSITIONANDSIZE)
				printf("GetWindowPositionAndSize() frameX:%d frameY:%d frameWidth:%u frameHeight:%u\n"
					   "                           clientX:%d clientY:%d clientWidth:%u clientHeight:%u\n", 
					   _frameX, _frameY, _frameWidth, _frameHeight,
					   _clientX, _clientY, _clientWidth, _clientHeight);
#endif

				return true;
			}
			
			// Gets the client position and dimensions data. Returns if successful.
			bool GetClientPositionAndSize(int& _clientX, int& _clientY, unsigned int& _clientWidth, unsigned int& _clientHeight) const
			{
				if (!windowIsRunning || !display || !window)
					return false;
				
				// Get the root.
				int x, y;
				unsigned int borderWidth, depth;
				Window child, root;
				if (!XGetGeometry(display, window, &root, &x, &y, &_clientWidth, &_clientHeight, &borderWidth, &depth))					
					return false;
					
				// Bring the window coordinates into root space.
				if (!XTranslateCoordinates(display, window, root, 0, 0, &_clientX, &_clientY, &child))					
					return false;
				
				return true;
			}
			
			// Compares previous window properties with those passed into the function and returns true if any are different.
			bool IsWindowDifferent(int& _frameX, int& _frameY, unsigned int& _clientWidth, unsigned int& _clientHeight) const
			{
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_ISWINDOWDIFFERENT)
				printf("IsWindowDifferent(): %d\n", 
					  (_frameX != prevX || 
				       _frameY != prevY || 
				       _clientWidth != prevWidth || 
				       _clientHeight != prevHeight));
#endif

				return _frameX != prevX || 
				       _frameY != prevY || 
				       _clientWidth != prevWidth || 
				       _clientHeight != prevHeight;
			}
			
			// Recursive preorder traversal of a window's children.
			bool TraverseWindowTreeForReservedSpace(
				Window _window, unsigned int _depthLimit, 
				unsigned int& _reservedLeft, unsigned int& _reservedRight, 
				unsigned int& _reservedTop, unsigned int& _reservedBottom,
				unsigned int _depth = 0
			) const
			{
				if (!display || !window || !_window)
					return false;
				
				Window root, parent;
				Window* children;
				unsigned int numChildren;				
				if (XQueryTree(display, _window, &root, &parent, &children, &numChildren) == 0)
					return false;
				
#if defined (GWINDOW_DEBUG_LINUX_OUTPUT_TRAVERSEWINDOWTREEFORRESERVEDSPACE)
				printf("TraverseWindowTreeForReservedSpace(): numChildren:%u\n", numChildren);
#endif
				
				for (int i = 0; i < numChildren; ++i)
				{
					Atom actualType;
					int actualFormat;
					unsigned long nItems, bytesAfter;
					unsigned char* extents = nullptr;
					int status = XGetWindowProperty(display, _window, atoms._NET_WM_STRUT,
													0, 12, False, XA_CARDINAL,
													&actualType, &actualFormat, &nItems, &bytesAfter, &extents);

#if defined (GWINDOW_DEBUG_LINUX_OUTPUT_TRAVERSEWINDOWTREEFORRESERVEDSPACE)
					if (actualType == XA_CARDINAL)
						printf("TraverseWindowTreeForReservedSpace(): i:%u, status:%d, actualType:%d, actualFormat:%d, nItems:%u\n", i, status, static_cast<int>(actualType), actualFormat, nItems);
#endif
					if (status == Success && actualType == XA_CARDINAL && actualFormat == 32 && nItems >= 4)
					{
#if defined (GWINDOW_DEBUG_LINUX_OUTPUT_TRAVERSEWINDOWTREEFORRESERVEDSPACE)
						printf("TraverseWindowTreeForReservedSpace(): calculating space %d\n", 0);
#endif

						unsigned long* struts = reinterpret_cast<unsigned long*>(extents);
						unsigned int spaceReservedLeft = static_cast<unsigned int>(struts[0]);
						unsigned int spaceReservedRight = static_cast<unsigned int>(struts[1]);
						unsigned int spaceReservedTop = static_cast<unsigned int>(struts[2]);
						unsigned int spaceReservedBottom = static_cast<unsigned int>(struts[3]);
						
						if (spaceReservedLeft > _reservedLeft) 		_reservedLeft = spaceReservedLeft;
						if (spaceReservedRight > _reservedRight) 	_reservedRight = spaceReservedRight;
						if (spaceReservedTop > _reservedTop) 		_reservedTop = spaceReservedTop;
						if (spaceReservedBottom > _reservedBottom) 	_reservedBottom = spaceReservedBottom;
					}
					
					if (extents)
						XFree(extents);
					
#if defined (GWINDOW_DEBUG_LINUX_OUTPUT_TRAVERSEWINDOWTREEFORRESERVEDSPACE)
					if (actualType == XA_CARDINAL)
						printf("TraverseWindowTreeForReservedSpace(): _depth:%u\n", _depth);
#endif

					if (_depth < _depthLimit)
					{
						TraverseWindowTreeForReservedSpace(
							children[i], _depthLimit, 
							_reservedLeft, _reservedRight, 
							_reservedTop, _reservedBottom,
							_depth + 1
						);
					}
				}
				
				return true;
			}
			
			// Reserved space is the area occupied by the things like the taskbar or dock. Therefore, unreserved
			// space is where our window is primarily within. This function provides the screen coordinates and
			// size of the unreserved space. It returns true, if the operation was successful.
			bool GetUnreservedSpace(int& _x, int& _y, unsigned int& _width, unsigned int& _height) const
			{
				if (!windowIsRunning || !display || !window)
					return false;
				
				// In tests, the strut information was only at a depth of 3. However, this may be different
				// depending on someone's Linux installation.
				constexpr unsigned int DEPTH_LIMIT = 5;
				
				unsigned int reservedLeft = 0, reservedRight = 0, reservedTop = 0, reservedBottom = 0;
				
				bool success = TraverseWindowTreeForReservedSpace(
					RootWindow(display, XDefaultScreen(display)),
					DEPTH_LIMIT,
					reservedLeft, reservedRight,
					reservedTop, reservedBottom
				);
				
				if (!success)
					return false;
				
				Screen* screen = DefaultScreenOfDisplay(display);
				
				_x = reservedLeft;
				_y = reservedTop;
				_width = screen->width - reservedLeft - reservedRight;
				_height = screen->height - reservedTop - reservedBottom;

#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETUNRESERVEDSPACE)
				printf("GetUnreservedSpace() x:%d y:%d width:%u height:%u\n", _x, _y, _width, _height);
#endif
				
				return true;
			}
			
		public:
			~GWindowImplementation()
			{
				if (!windowIsRunning)
					return;
				
				if (!destroyEventIsSentByXButton)
				{
					//Delete icon
					if (m_iconPixels)
					{
						delete m_iconPixels;
						m_iconPixels = nullptr;
						m_iconPixelsCount = 0;
					}

					//Send Event Data for DESTROY
					int windowFrameX = 0, windowFrameY = 0, windowClientX = 0, windowClientY = 0;
					unsigned int windowFrameWidth = 0, windowFrameHeight = 0, windowClientWidth = 0, windowClientHeight = 0;
						
					GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
											 windowClientX, windowClientY, windowClientWidth, windowClientHeight);
											 
					windowIsRunning = false;
					
					EVENT_DATA eventData;
					eventData.eventFlags = Events::DESTROY;
					eventData.width = windowFrameWidth;
					eventData.height = windowFrameHeight;
					eventData.clientWidth = windowClientWidth;
					eventData.clientHeight = windowClientHeight;
					eventData.windowX = windowFrameX;
					eventData.windowY = windowFrameY;
					eventData.windowHandle = display;
					DebugOutputGWindowEvent(eventData);
					SetInteralData(eventData.windowX, eventData.windowY, eventData.width, eventData.height, m_WindowStyle);
					m_GEvent.Write(eventData.eventFlags, eventData);
					Push(m_GEvent);

					//Close Window and Display
					XDestroyWindow(display, window);
					XCloseDisplay(display);
					window = 0;
					display = nullptr;

					//Send Event Data for DISPLAY_CLOSED
					eventData.eventFlags = Events::DISPLAY_CLOSED;
					eventData.width = 0;
					eventData.height = 0;
					eventData.clientWidth = 0;
					eventData.clientHeight = 0;
					eventData.windowX = 0;
					eventData.windowY = 0;
					eventData.windowHandle = 0;
					DebugOutputGWindowEvent(eventData);
					SetInteralData(eventData.windowX, eventData.windowY, eventData.width, eventData.height, m_WindowStyle);
					m_GEvent.Write(eventData.eventFlags, eventData);
					Push(m_GEvent);
				}
				else
					windowIsRunning = false;
			}

			GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				this->SetInteralData(_x, _y, _width, _height, _style);
				return OpenWindow();
			}

			GReturn ProcessWindowEvents() override
			{
				if (!windowIsRunning || !display)
					return GReturn::FAILURE;
				
				XEvent xEvent;
				EVENT_DATA eventData;
				
				while (XPending(display) > 0)
				{
					//ZeroMemory the structs
					memset(&xEvent, 0, sizeof(xEvent));
					memset(&eventData, 0, sizeof(eventData));
					
					eventData.eventFlags = Events::DESTROY; // ensure we don't spam
					
					// Flushes the request buffer if xlib's queue does not contain an event and waits for an event to arrive from server connection
					XNextEvent(display, &xEvent);
					
					DebugOutputX11Event(xEvent);
					
					switch (xEvent.type)
					{
					default:
						break;
					case Expose: //Expose, when a window becomes visible on the screen, after being obscured or unmapped.
					case SelectionClear: //Clients losing ownership of a selection.
					case SelectionNotify: //A response to a ConvertSelection request when there is no owner for the selection
					case SelectionRequest: //A client requests a selection conversion by calling XConvertSelection() for the owned selection.
					case NoExpose: //Generates this event whenever a destination region could not be computed due to an obscured or out-of-bounds source region.
					case ButtonPress: //Respond to button press
						break;

					case PropertyNotify:
					{
						// Ensure we have a window and display
						if (!display || !window)
							break;
						
						Atom actual_type = 0;
						int actual_format = 0;
						unsigned long nitems = 0;
						unsigned long bytes_after = 0;
						unsigned char* propRet = nullptr;
						
						// PropertyNotify, when a client wants info about property changes for a specified window.
						int status = XGetWindowProperty(xEvent.xproperty.display, xEvent.xproperty.window, atoms._NET_WM_STATE, 0L, sizeof(Atom),
							false, AnyPropertyType, &actual_type, &actual_format, &nitems, &bytes_after, &propRet);
						
						if (status == Success && propRet && nitems > 0)
						{
							/*
							 * This is here because a property return can contain multiple property event data types
							 * that can specify in more detail what kind of event is actually occurring. This is only
							 * used currently for maximize and minimize events. This should be considered as the event
							 * bursts that are sent when a window is maximized or minimized all contain multiple
							 * property return values. This is a way to filter out the maximize and minimize events
							 * from the rest of the property return values.
							 */
							bool verticalMaximizedFlag = false;
							bool horizontalMaximizedFlag = false;
							bool hiddenFlag = false;
							for (unsigned index = 0; index < nitems; ++index)
							{
								Atom prop = reinterpret_cast<Atom*>(propRet)[index];

								if (prop == atoms._NET_WM_STATE_MAXIMIZED_VERT)
									verticalMaximizedFlag = true;
								else if (prop == atoms._NET_WM_STATE_MAXIMIZED_HORZ)
									horizontalMaximizedFlag = true;
								else if (prop == atoms._NET_WM_STATE_HIDDEN)
								{
									hiddenFlag = true;
									verticalMaximizedFlag = false;
									horizontalMaximizedFlag = false;
								}
							}

							Atom prop = reinterpret_cast<Atom*>(propRet)[0];
							XFree(propRet);

							int windowFrameX, windowFrameY, windowClientX, windowClientY;
							unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;

							if (!GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight,
														  windowClientX, windowClientY, windowClientWidth, windowClientHeight))
								break;

							const bool isWindowTheSame = !IsWindowDifferent(windowFrameX, windowFrameY, windowClientWidth, windowClientHeight);
							const bool isWindowMinimizing = isWindowTheSame && hiddenFlag && m_prevEvent != Events::MINIMIZE;
							const bool isWindowMaximizing = isWindowTheSame && (horizontalMaximizedFlag && verticalMaximizedFlag) && m_prevEvent != Events::MAXIMIZE;

							if (isWindowMinimizing)
								eventData.eventFlags = Events::MINIMIZE;
							else if (isWindowMaximizing)
								eventData.eventFlags = Events::MAXIMIZE;
							else if (isWindowTheSame && prop != atoms._NET_WM_STATE_HIDDEN && xEvent.xclient.message_type != atoms._NET_WM_STATE_HIDDEN)
								break;
							else
							{
								if ((prop == atoms._NET_WM_STATE_HIDDEN && m_prevEvent != Events::MINIMIZE) || xEvent.xclient.message_type == atoms._NET_WM_STATE_HIDDEN) // Minimize button pressed.
									eventData.eventFlags = Events::MINIMIZE;
								else if (prop == 362 || // This seems to be received whenever the user clicking on any window frame button.
										prop == atoms._NET_WM_STATE_FULLSCREEN || // Received the the frame fills the unreserved screen space.
										prop == atoms._NET_WM_STATE_MAXIMIZED_HORZ || // Received the horizontal area of the frame fills the unreserved screen space.
										prop == atoms._NET_WM_STATE_MAXIMIZED_VERT) // Received the vertical area of the frame fills the unreserved screen space.
								{
									if (prop == 362)
									{
										if (prevHeight != windowClientHeight || prevWidth != windowClientWidth)
											eventData.eventFlags = Events::RESIZE;
										else if (prevX != windowFrameX || prevY != windowFrameY)
											eventData.eventFlags = Events::MOVE;
										else
											break;
									}
									else
										eventData.eventFlags = Events::MAXIMIZE;
								}
								else if (prevHeight != windowClientHeight || prevWidth != windowClientWidth)
									eventData.eventFlags = Events::RESIZE;
								else if (prevX != windowFrameX || prevY != windowFrameY)
									eventData.eventFlags = Events::MOVE;
								else
									break; // Should never reach this point based on the above checks.
							}

							eventData.width = windowFrameWidth;
							eventData.height = windowFrameHeight;
							eventData.clientWidth = windowClientWidth;
							eventData.clientHeight = windowClientHeight;
							eventData.windowX = windowFrameX;
							eventData.windowY = windowFrameY;
							eventData.windowHandle = display;
							
							prevX = windowFrameX; 
							prevY = windowFrameY; 
							prevHeight = windowClientHeight; 
							prevWidth = windowClientWidth;
						}
						else
							XFree(propRet);
					}
					break;

					case ConfigureNotify:
					{
						int windowFrameX, windowFrameY, windowClientX, windowClientY;
						unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;
							
						if (!GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
													  windowClientX, windowClientY, windowClientWidth, windowClientHeight))
							break;
								
						if (!IsWindowDifferent(windowFrameX, windowFrameY, windowClientWidth, windowClientHeight))
							break;
						
						int unreservedX, unreservedY;
						unsigned int unreservedWidth, unreservedHeight;
						GetUnreservedSpace(unreservedX, unreservedY, unreservedWidth, unreservedHeight);
						
						if (windowFrameX == unreservedX && windowFrameY == unreservedY && windowFrameWidth == unreservedWidth && windowFrameHeight == unreservedHeight)
							eventData.eventFlags = Events::MAXIMIZE;
						else if (prevHeight != windowClientHeight || prevWidth != windowClientWidth)
							eventData.eventFlags = Events::RESIZE;
						else if (prevX != windowFrameX || prevY != windowFrameY)
							eventData.eventFlags = Events::MOVE;
						else
							break; // Should never reach this point based on the above checks.
						
						eventData.width = windowFrameWidth;
						eventData.height = windowFrameHeight;
						eventData.clientWidth = windowClientWidth;
						eventData.clientHeight = windowClientHeight;
						eventData.windowX = windowFrameX;
						eventData.windowY = windowFrameY;
						eventData.windowHandle = display;
						
						prevX = windowFrameX; 
						prevY = windowFrameY; 
						prevHeight = windowClientHeight; 
						prevWidth = windowClientWidth;
					}
					break;

					case ClientMessage:
					{
						// Primarily used for transferring selection data,
						// also might be used in a private inter-client
						// protocol
						if (xEvent.xclient.message_type == atoms._NET_WM_STATE_HIDDEN)
						{
							int windowFrameX, windowFrameY, windowClientX, windowClientY;
							unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;
								
							if (!GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
														  windowClientX, windowClientY, windowClientWidth, windowClientHeight))
								break;
						
							eventData.eventFlags = Events::MINIMIZE;
							eventData.width = windowFrameWidth;
							eventData.height = windowFrameHeight;
							eventData.clientWidth = windowClientWidth;
							eventData.clientHeight = windowClientHeight;
							eventData.windowX = windowFrameX;
							eventData.windowY = windowFrameY;
							eventData.windowHandle = display;
							
							prevX = windowFrameX; 
							prevY = windowFrameY; 
							prevHeight = windowClientHeight; 
							prevWidth = windowClientWidth;
						}
						else if (xEvent.xclient.message_type == atoms._NET_WM_STATE_FULLSCREEN)
						{
							int windowFrameX, windowFrameY, windowClientX, windowClientY;
							unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;
								
							if (!GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
														  windowClientX, windowClientY, windowClientWidth, windowClientHeight))
								break;
								
							if (!IsWindowDifferent(windowFrameX, windowFrameY, windowClientWidth, windowClientHeight))
								break;
						
							int unreservedX, unreservedY;
							unsigned int unreservedWidth, unreservedHeight;
							GetUnreservedSpace(unreservedX, unreservedY, unreservedWidth, unreservedHeight);
							
							if (windowFrameX == unreservedX && windowFrameY == unreservedY && windowFrameWidth == unreservedWidth && windowFrameHeight == unreservedHeight)
								eventData.eventFlags = Events::MAXIMIZE;
							else
								break; // Should never reach this point based on the above checks.
							
							eventData.width = windowFrameWidth;
							eventData.height = windowFrameHeight;
							eventData.clientWidth = windowClientWidth;
							eventData.clientHeight = windowClientHeight;
							eventData.windowX = windowFrameX;
							eventData.windowY = windowFrameY;
							eventData.windowHandle = display;
							
							prevX = windowFrameX; 
							prevY = windowFrameY; 
							prevHeight = windowClientHeight; 
							prevWidth = windowClientWidth;							
						}
						else if (xEvent.xclient.data.l[0] == atoms.WM_DELETE_WINDOW)
						{							
							windowIsRunning = false;
							destroyEventIsSentByXButton = true;
							
							int windowFrameX, windowFrameY, windowClientX, windowClientY;
							unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;
								
							GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
													 windowClientX, windowClientY, windowClientWidth, windowClientHeight);
						
							prevX = windowFrameX; prevY = windowFrameY; prevHeight = windowClientHeight; prevWidth = windowClientWidth;
							
							eventData.eventFlags = Events::DESTROY;
							eventData.width = windowFrameWidth;
							eventData.height = windowFrameHeight;
							eventData.clientWidth = windowClientWidth;
							eventData.clientHeight = windowClientHeight;
							eventData.windowX = windowFrameX;
							eventData.windowY = windowFrameY;
							eventData.windowHandle = display;
							DebugOutputGWindowEvent(eventData);
							SetInteralData(eventData.windowX, eventData.windowY, eventData.width, eventData.height, m_WindowStyle);
							m_GEvent.Write(eventData.eventFlags, eventData);
							Push(m_GEvent);
							XDestroyWindow(display, window);
							XCloseDisplay(display);
							window = 0;
							display = nullptr;


							//Send Event Data for DISPLAY_CLOSED
							eventData.eventFlags = Events::DISPLAY_CLOSED;
							eventData.width = 0;
							eventData.height = 0;
							eventData.clientWidth = 0;
							eventData.clientHeight = 0;
							eventData.windowX = 0;
							eventData.windowY = 0;
							eventData.windowHandle = 0;
							DebugOutputGWindowEvent(eventData);
							SetInteralData(eventData.windowX, eventData.windowY, eventData.width, eventData.height, m_WindowStyle);
							m_GEvent.Write(eventData.eventFlags, eventData);
							Push(m_GEvent);
							
							return GReturn::FAILURE;
						}
						else if (eventData.eventFlags == Events::DESTROY)
						{
							int windowFrameX, windowFrameY, windowClientX, windowClientY;
							unsigned int windowFrameWidth, windowFrameHeight, windowClientWidth, windowClientHeight;
								
							if (!GetWindowPositionAndSize(windowFrameX, windowFrameY, windowFrameWidth, windowFrameHeight, 
														  windowClientX, windowClientY, windowClientWidth, windowClientHeight))
								break;
							
							Screen* screen = DefaultScreenOfDisplay(display);
							
							eventData.width = windowFrameWidth;
							eventData.height = windowFrameHeight;
							eventData.clientWidth = windowClientWidth;
							eventData.clientHeight = windowClientHeight;
							eventData.windowX = windowFrameX;
							eventData.windowY = windowFrameY;
							eventData.windowHandle = display;
						}
					}
					break;
					}

					if (eventData.eventFlags != Events::DESTROY)
					{
						m_prevEvent = eventData.eventFlags; // Previous event saved so Minimize isn't spammed.
						DebugOutputGWindowEvent(eventData);
						SetInteralData(eventData.windowX, eventData.windowY, eventData.width, eventData.height, m_WindowStyle);
						m_GEvent.Write(eventData.eventFlags, eventData);
						Push(m_GEvent);
					}
				}
				
				eventData.eventFlags = Events::EVENTS_PROCESSED;
				DebugOutputGWindowEvent(eventData);
				m_GEvent.Write(eventData.eventFlags, eventData);
				Push(m_GEvent);
				
				return GReturn::SUCCESS;
			}

			GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style) override
			{
				if (!windowIsRunning || !display || !window)
					return GReturn::FAILURE;

				SYSTEM::GWindowStyle previousStyle = m_WindowStyle;
				int previousX = m_WindowX;
				int previousY = m_WindowY;
				int previousWidth = m_WindowWidth;
				int previousHeight = m_WindowHeight;
				
				// If the style parameter is the same as the previous style, the all we need to do is move or resize.
				if (previousStyle == _style)
				{						
					if (previousX != _x || previousY != _y || previousWidth != _width || previousHeight != _height)
					{
						if (MoveAndResizeWindow(_x, _y, _width, _height))
						{
							SetInteralData(_x, _y, _width, _height, _style);
							X11_SyncWindow();
							return GReturn::SUCCESS;
						}
						else
							return GReturn::FAILURE;
					}
					
					// Nothing needs to change because all parameters match existing position, size, and style.
					return GReturn::REDUNDANT;
				}
				else // Otherwise we restyle.
				{
					SetInteralData(_x, _y, _width, _height, _style);
					
					switch (m_WindowStyle)
					{
					case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
					{
						if (!UnminimizeWindow()) 
							return GReturn::FAILURE;
						
						if (!UnmaximizeWindow()) 
							return GReturn::FAILURE;
						
						if (!StylizeWindow(SYSTEM::GWindowStyle::WINDOWEDBORDERED))
							return GReturn::FAILURE;

						EnableWindowResizing();
						
						if (!MoveAndResizeWindow(_x, _y, _width, _height))
							return GReturn::FAILURE;
					}
					break;

					case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
					{
						if (!UnminimizeWindow()) 
							return GReturn::FAILURE;
						
						if (!UnmaximizeWindow()) 
							return GReturn::FAILURE;
						
						if (!StylizeWindow(SYSTEM::GWindowStyle::WINDOWEDBORDERLESS))
							return GReturn::FAILURE;

						EnableWindowResizing();
						
						if (!MoveAndResizeWindow(_x, _y, _width, _height))
							return GReturn::FAILURE;
					}
					break;
					
					case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
					{
						if (!UnminimizeWindow()) 
							return GReturn::FAILURE;
						
						if (!UnmaximizeWindow()) 
							return GReturn::FAILURE;
						
						EnableWindowResizing();
						
						if (!MoveAndResizeWindow(_x, _y, _width, _height))
							return GReturn::FAILURE;

						if (!StylizeWindow(SYSTEM::GWindowStyle::WINDOWEDLOCKED)) 
							return GReturn::FAILURE;
						
						DisableWindowResizing();
					}
					break;

					case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
					{
						if (!UnminimizeWindow()) 
							return GReturn::FAILURE;
						
						EnableWindowResizing();
												
						if (!StylizeWindow(SYSTEM::GWindowStyle::FULLSCREENBORDERED)) 
							return GReturn::FAILURE;
						
						if (!MaximizeWindow()) 
							return GReturn::FAILURE;
					}
					break;

					case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
					{
						if (!UnminimizeWindow()) 
							return GReturn::FAILURE;
						
						EnableWindowResizing();
						
						if (!StylizeWindow(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)) 
							return GReturn::FAILURE;
						
						if (!MaximizeWindow()) 
							return GReturn::FAILURE;
					}
					break;

					case SYSTEM::GWindowStyle::MINIMIZED:
					{
						if (!XIconifyWindow(display, window, DefaultScreen(display)))
							return GReturn::FAILURE;
						
						if (!MinimizeWindow()) 
							return GReturn::FAILURE;
					}
					break;
					}
				}

				X11_SyncWindow();
				
				return GReturn::SUCCESS;
			}

			GReturn SetWindowName(const char* _newName) override
			{
				if (_newName == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (!windowIsRunning || !display || !window)
					return GReturn::FAILURE;

				return XStoreName(display, window, _newName) != 0 ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
			{
				if (!display || !window)
					return GReturn::FAILURE;
				
				if (_argbPixels == nullptr || _width <= 0 || _height <= 0)
					return GReturn::INVALID_ARGUMENT;
				
				Atom cardinal = XInternAtom(display, "CARDINAL", false);
				if (!cardinal)
					return GReturn::FAILURE;
				
				const int convertedLength = 2 + _width * _height;
				std::unique_ptr<unsigned long[]> convertedPixels(new unsigned long[convertedLength]);
				convertedPixels[0] = static_cast<unsigned long>(_width);
				convertedPixels[1] = static_cast<unsigned long>(_height);

				for (int i = 2; i < convertedLength; ++i)
					convertedPixels[i] = static_cast<unsigned long>(_argbPixels[i - 2]);
				
				if (!m_iconPixels)
				{
					m_iconPixelsCount = convertedLength;
					m_iconPixels = new unsigned long[m_iconPixelsCount];
					
					memcpy(m_iconPixels, convertedPixels.get(), sizeof(unsigned long) * m_iconPixelsCount);
				}
				else
				{
					int iconStartIndex = 0;
					int existingIconWidth, existingIconHeight;
					bool iconSizeExists = false;
					
					// Search for an existing icon of the same size.
					while (iconStartIndex < m_iconPixelsCount)
					{
						existingIconWidth = m_iconPixels[iconStartIndex];
						existingIconHeight = m_iconPixels[iconStartIndex + 1];
						
						if (existingIconWidth == _width && existingIconHeight == _height)
						{
							iconSizeExists = true;
							break;
						}
						else
							iconStartIndex += 2 + existingIconWidth * existingIconHeight;
					}
					
					// Overwrite the icon if the same size icon exists.
					if (iconSizeExists)
					{
						for (int i = 2; i < convertedLength; ++i)
							m_iconPixels[iconStartIndex + i] = convertedPixels[i];
					}
					else // Otherwise, append the new icon to the existing array.
					{
						const int tempPixelsCount = m_iconPixelsCount + convertedLength;
						unsigned long* tempPixels = new unsigned long[tempPixelsCount];
						
						memcpy(tempPixels, m_iconPixels, sizeof(unsigned long) * m_iconPixelsCount);
						
						for (int i = 0; i < convertedLength; ++i)
							tempPixels[m_iconPixelsCount + i] = convertedPixels[i];
						
						delete m_iconPixels;
						
						m_iconPixels = tempPixels;
						m_iconPixelsCount = tempPixelsCount;
					}
				}
				
				// The window system will choose the icon whose size is closest to the icon display area. 
				// Icons will automatically be stretched or squashed as needed.
				XChangeProperty(display, window, atoms._NET_WM_ICON, cardinal, 32, PropModeReplace, (const unsigned char*)m_iconPixels, m_iconPixelsCount);

				return GReturn::SUCCESS;
			}

			GReturn MoveWindow(int _x, int _y) override
			{
				if (!windowIsRunning || !display || !window)
					return GReturn::FAILURE;

				SetInteralData(_x, _y, m_WindowWidth, m_WindowHeight, m_WindowStyle);
				if (XMoveWindow(display, window, m_WindowX, m_WindowY))
				{
					if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
						m_WindowStyle = SYSTEM::GWindowStyle::WINDOWEDBORDERED;
					else if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
						m_WindowStyle = SYSTEM::GWindowStyle::WINDOWEDBORDERLESS;
					
					XEvent ev;
					memset(&ev, 0, sizeof ev);
					ev.type = PropertyNotify;
					ev.xclient.window = window;
					ev.xclient.message_type = atoms._NET_WM_STATE;
					ev.xclient.format = 32; // 32-bit cardinal
					
					XSendEvent(display, window, False, PropertyChangeMask, &ev);
					return GReturn::SUCCESS;
				}
				return  GReturn::FAILURE;
			}

			GReturn ResizeWindow(int _width, int _height) override
			{
				if (!windowIsRunning || !display || !window)
					return GReturn::FAILURE;

				SetInteralData(m_WindowX, m_WindowY, _width, _height, m_WindowStyle);
				if (XResizeWindow(display, window, m_WindowWidth, m_WindowHeight))
				{
					if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERED)
						m_WindowStyle = SYSTEM::GWindowStyle::WINDOWEDBORDERED;
					else if (m_WindowStyle == SYSTEM::GWindowStyle::FULLSCREENBORDERLESS)
						m_WindowStyle = SYSTEM::GWindowStyle::WINDOWEDBORDERLESS;

					X11_SyncWindow();

					return GReturn::SUCCESS;
				}
				return  GReturn::FAILURE;
			}

			GReturn Maximize() override
			{
				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERED || m_WindowStyle == SYSTEM::GWindowStyle::MINIMIZED)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERED);
				else if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
				return  GReturn::REDUNDANT;
			}

			GReturn Minimize() override
			{
				return ChangeWindowStyle(SYSTEM::GWindowStyle::MINIMIZED);
			}

			GReturn ChangeWindowStyle(SYSTEM::GWindowStyle _style) override
			{
				return ReconfigureWindow(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, _style);
			}

			GReturn GetWidth(unsigned int& _outWidth) const override
			{
				int frameX, frameY, clientX, clientY;
				unsigned int frameWidth, frameHeight, clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetWindowPositionAndSize(frameX, frameY, frameWidth, frameHeight, clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetWidth() GetWindowPositionAndSize() retry:%u\n", g_retries);
#endif

				if (g_success)
				{
					_outWidth = static_cast<unsigned int>(frameWidth);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetHeight(unsigned int& _outHeight) const override
			{
				int frameX, frameY, clientX, clientY;
				unsigned int frameWidth, frameHeight, clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetWindowPositionAndSize(frameX, frameY, frameWidth, frameHeight, clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetHeight() GetWindowPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outHeight = static_cast<unsigned int>(frameHeight);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetClientWidth(unsigned int& _outClientWidth) const override
			{
				int clientX, clientY;
				unsigned int clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetClientPositionAndSize(clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetClientWidth() GetClientPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outClientWidth = static_cast<unsigned int>(clientWidth);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetClientHeight(unsigned int& _outClientHeight) const override
			{
				int clientX, clientY;
				unsigned int clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetClientPositionAndSize(clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetClientHeight() GetClientPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outClientHeight = static_cast<unsigned int>(clientHeight);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetX(unsigned int& _outX) const override
			{
				int frameX, frameY, clientX, clientY;
				unsigned int frameWidth, frameHeight, clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetWindowPositionAndSize(frameX, frameY, frameWidth, frameHeight, clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetX() GetWindowPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outX = static_cast<unsigned int>(frameX);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetY(unsigned int& _outY) const override
			{
				int frameX, frameY, clientX, clientY;
				unsigned int frameWidth, frameHeight, clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetWindowPositionAndSize(frameX, frameY, frameWidth, frameHeight, clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetY() GetWindowPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outY = static_cast<unsigned int>(frameY);
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
			{
				long _titleBarHeight, _bottomBorderHeight, _leftBorderWidth, _rightBorderWidth;
				
				G_RETRY_IF_FAILED(GetWindowBorderSizes(_titleBarHeight, _bottomBorderHeight, _leftBorderWidth, _rightBorderWidth));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("GetClientTopLeft() GetWindowBorderSizes() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					_outX = static_cast<unsigned int>(_leftBorderWidth);
					_outY = static_cast<unsigned int>(_titleBarHeight);
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETCLIENTTOPLEFT)
					printf("GetClientTopLeft() x:%u y:%u\n", _outX, _outY);
#endif
					return GReturn::SUCCESS;
				}
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_GETCLIENTTOPLEFT)
				printf("GetClientTopLeft() Failed%u\n", 0);
#endif
				return GReturn::FAILURE;
			}

			GReturn GetWindowHandle(SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const override
			{
				if (!windowIsRunning || !display || !window)
					return GReturn::FAILURE;

				_outUniversalWindowHandle.window = (void*)(&window);
				_outUniversalWindowHandle.display = (void*)(display);
				return  GReturn::SUCCESS;
			}

			GReturn IsFullscreen(bool& _outIsFullscreen) const override
			{
				int frameX, frameY, clientX, clientY;
				unsigned int frameWidth, frameHeight, clientWidth, clientHeight;
				
				G_RETRY_IF_FAILED(GetWindowPositionAndSize(frameX, frameY, frameWidth, frameHeight, clientX, clientY, clientWidth, clientHeight));
				
#if defined(GWINDOW_DEBUG_LINUX_OUTPUT_RETRIES)
				if (g_retries > 0)
					printf("IsFullscreen() GetWindowPositionAndSize() retry:%u\n", g_retries);
#endif
				
				if (g_success)
				{
					Screen* screen = DefaultScreenOfDisplay(display);
					_outIsFullscreen = (frameWidth == screen->width && frameHeight == screen->height);
					
					return GReturn::SUCCESS;
				}
				
				return GReturn::FAILURE;
			}

			GReturn IsFocus(bool& _outIsFocus) const override
			{
				if (!windowIsRunning || !display || !window)
					return  GReturn::FAILURE;
				int revert;
				Window focus;
				XGetInputFocus(display, &focus, &revert);
				_outIsFocus = (window == focus);
				return GReturn::SUCCESS;
			}
		};
	}
}

#undef _NET_WM_STATE_ADD
#undef _NET_WM_STATE_REMOVE
