#define WIN32_LEAN_AND_MEAN

#include "../../Shared/uwputils.h"
#include "../../../Interface/Core/GEventReceiver.h"
#include "../../../Interface/Core/GThreadShared.h"

#include <gamingdeviceinformation.h>

#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.UI.ViewManagement.h>
#include <winrt/Windows.Graphics.Display.h>

namespace GW
{
	namespace I
	{
		class GWindowImplementation : public virtual GWindowInterface,
			private virtual GEventGeneratorImplementation
		{
		private:
			CORE::GThreadShared threadShare;
			int m_WindowX;
			int m_WindowY;
			int m_WindowWidth;
			int m_WindowHeight;
			SYSTEM::GWindowStyle m_WindowStyle;

			// event tokens
			winrt::event_token m_sizeChanged;
			winrt::event_token m_destroyed;
			winrt::event_token m_suspended;
			winrt::event_token m_resumed;
			winrt::event_token m_enteredBackground;
			winrt::event_token m_leavingBackground;
			winrt::event_token m_isFocus;

			bool m_isInFocus = true;

			// This should only be used if there is only one view
			winrt::Windows::Foundation::IAsyncAction CallOnMainViewUiThreadAsync(winrt::Windows::UI::Core::DispatchedHandler handler) const
			{
				co_await winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher().RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal, handler);
			}

			void SetInteralData(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				m_WindowWidth = _width;
				m_WindowHeight = _height;
				m_WindowX = _x;
				m_WindowY = _y;
				m_WindowStyle = _style;
			}

			GReturn OpenWindow()
			{
				return GReturn::REDUNDANT;
			}
		public:
			~GWindowImplementation()
			{
				// revoke events
				auto process = CallOnMainViewUiThreadAsync([this]()
					{
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						window.SizeChanged(m_sizeChanged);
						winrt::Windows::ApplicationModel::Core::CoreApplication::Resuming(m_resumed);
						winrt::Windows::ApplicationModel::Core::CoreApplication::Suspending(m_suspended);
						winrt::Windows::ApplicationModel::Core::CoreApplication::EnteredBackground(m_enteredBackground);
						winrt::Windows::ApplicationModel::Core::CoreApplication::LeavingBackground(m_leavingBackground);
						winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread().VisibilityChanged(m_isFocus);

						GEvent exitingEvent;
						EVENT_DATA eventData;

						eventData.eventFlags = Events::DESTROY;
						/*eventData.height = winRect.Height;
						eventData.width = winRect.Width;
						eventData.windowX = winRect.X;
						eventData.windowY = winRect.Y;
						eventData.windowHandle = nullptr;*/

						exitingEvent.Write(eventData.eventFlags, eventData);
						this->Push(exitingEvent);
					}); process.get();
			}

			GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				threadShare.Create();

				this->SetInteralData(_x, _y, _width, _height, _style);

				// subscribe events to delegates
				// call on main thread
				auto process = CallOnMainViewUiThreadAsync([this, &_style]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						winrt::Windows::Foundation::Size size = { 1920, 1080 };
						view.PreferredLaunchViewSize(size);

						// Event::RESIZE
						this->m_sizeChanged = window.SizeChanged([this, &_style]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::WindowSizeChangedEventArgs const& args)
							{
								auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
								winrt::Windows::Foundation::Rect winRect = win.Bounds();
								const int width = static_cast<int>(args.Size().Width);
								const int height = static_cast<int>(args.Size().Height);
								GEvent gEvent;
								EVENT_DATA eventData;
								
								eventData.eventFlags = Events::RESIZE;
								eventData.height = static_cast<unsigned int>(height);
								eventData.width = static_cast<unsigned int>(width);
								eventData.windowX = static_cast<int>(winRect.X);
								eventData.windowY = static_cast<int>(winRect.Y);
								eventData.windowHandle = nullptr;

								//update member variables
								// thread lock safety
								threadShare.LockSyncWrite();
								this->m_WindowWidth = width;
								this->m_WindowHeight = height;
								// unlock thread
								threadShare.UnlockSyncWrite();

								gEvent.Write(eventData.eventFlags, eventData);
								
								this->Push(gEvent);
								
								this->SetInteralData(0, 0, static_cast<int>(winRect.Width), static_cast<int>(winRect.Height), _style);
							});


						// Event::SUSPEND
						this->m_suspended = winrt::Windows::ApplicationModel::Core::CoreApplication::Suspending([this]
						(winrt::Windows::Foundation::IInspectable const&,
							winrt::Windows::ApplicationModel::SuspendingEventArgs const& args)
							{
								winrt::Windows::ApplicationModel::SuspendingDeferral deferral = args.SuspendingOperation().GetDeferral();

								auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
								winrt::Windows::Foundation::Rect winRect = win.Bounds();
								GEvent suspendEvent;
								EVENT_DATA eventData;

								eventData.eventFlags = Events::SUSPEND;
								eventData.height = static_cast<unsigned int>(winRect.Height);
								eventData.width = static_cast<unsigned int>(winRect.Width);
								eventData.windowX = static_cast<int>(winRect.X);
								eventData.windowY = static_cast<int>(winRect.Y);
								eventData.windowHandle = nullptr;

								suspendEvent.Write(eventData.eventFlags, eventData);
								
								this->Push(suspendEvent);
								
								deferral.Complete();
							});

						// Event::RESUME
						this->m_resumed = winrt::Windows::ApplicationModel::Core::CoreApplication::Resuming([this]
						(winrt::Windows::Foundation::IInspectable const&,
							winrt::Windows::Foundation::IInspectable const&)
							{
								auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
								
								winrt::Windows::Foundation::Rect winRect = win.Bounds();
								GEvent resumeEvent;
								EVENT_DATA eventData;

								eventData.eventFlags = Events::RESUME;
								eventData.height = static_cast<unsigned int>(winRect.Height);
								eventData.width = static_cast<unsigned int>(winRect.Width);
								eventData.windowX = static_cast<int>(winRect.X);
								eventData.windowY = static_cast<int>(winRect.Y);
								eventData.windowHandle = nullptr;

								resumeEvent.Write(eventData.eventFlags, eventData);
								this->Push(resumeEvent);
							});

						// Event::ENTERED_BACKGROUND
						this->m_enteredBackground = winrt::Windows::ApplicationModel::Core::CoreApplication::EnteredBackground([this]
						(winrt::Windows::Foundation::IInspectable const&,
							winrt::Windows::ApplicationModel::EnteredBackgroundEventArgs const&)
							{
								auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
								winrt::Windows::Foundation::Rect winRect = win.Bounds();
								GEvent enteredBackgroundEvent;
								EVENT_DATA eventData;

								eventData.eventFlags = Events::ENTERED_BACKGROUND;
								eventData.height = static_cast<unsigned int>(winRect.Height);
								eventData.width = static_cast<unsigned int>(winRect.Width);
								eventData.windowX = static_cast<int>(winRect.X);
								eventData.windowY = static_cast<int>(winRect.Y);
								eventData.windowHandle = nullptr;

								enteredBackgroundEvent.Write(eventData.eventFlags, eventData);
								this->Push(enteredBackgroundEvent);
							});

						// Event::LEAVING_BACKGROUND
						this->m_leavingBackground = winrt::Windows::ApplicationModel::Core::CoreApplication::LeavingBackground([this]
						(winrt::Windows::Foundation::IInspectable const&,
							winrt::Windows::ApplicationModel::LeavingBackgroundEventArgs const&)
							{
								auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
								winrt::Windows::Foundation::Rect winRect = win.Bounds();
								GEvent leavingBackgroundEvent;
								EVENT_DATA eventData;

								eventData.eventFlags = Events::LEAVING_BACKGROUND;
								eventData.height = static_cast<unsigned int>(winRect.Height);
								eventData.width = static_cast<unsigned int>(winRect.Width);
								eventData.windowX = static_cast<int>(winRect.X);
								eventData.windowY = static_cast<int>(winRect.Y);
								eventData.windowHandle = nullptr;

								leavingBackgroundEvent.Write(eventData.eventFlags, eventData);
								this->Push(leavingBackgroundEvent);
							});

						// isFocus event
						this->m_isFocus = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread().VisibilityChanged([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::VisibilityChangedEventArgs const& args)
							{
								if (args.Visible())
								{
									m_isInFocus = true;
								}
								else
								{
									m_isInFocus = false;
								}
							});
					}); process.get();

					


				GAMING_DEVICE_MODEL_INFORMATION info = {};
				GetGamingDeviceModelInformation(&info);
				if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
				{
					switch (info.deviceId)
					{
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:

					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:
						ResizeWindow(1920, 1080);
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:

					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
						ResizeWindow(3840, 2160);
						return this->ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
					default:
						break;
					}
				}
				else
				{
					if (-this->ResizeWindow(_width, _height))
					{
						return GReturn::FAILURE;
					}
					return this->ChangeWindowStyle(_style);
				}
				return GReturn::SUCCESS;
			}

			// Always returns success to keep parity with other platforms
			GReturn ProcessWindowEvents() override
			{
				GEvent processEvent;
				EVENT_DATA eventData;

				eventData.eventFlags = Events::EVENTS_PROCESSED;
				processEvent.Write(eventData.eventFlags, eventData);
				Push(processEvent);

				return GReturn::SUCCESS;
			}

			GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style) override
			{
				SYSTEM::GWindowStyle previousStyle = m_WindowStyle;
				SetInteralData(_x, _y, _width, _height, _style);

				switch (m_WindowStyle)
				{
				case SYSTEM::GWindowStyle::WINDOWEDBORDERED:
				{
					auto process = CallOnMainViewUiThreadAsync([]()
						{
							auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
							if (view.IsFullScreenMode())
							{
								view.GetForCurrentView().ExitFullScreenMode();
							}
							auto titleBar = winrt::Windows::ApplicationModel::Core::CoreApplication::GetCurrentView().TitleBar();
							titleBar.ExtendViewIntoTitleBar(false);
						}); process.get();
					ResizeWindow(_width, _height);

					SetInteralData(_x, _y, _width, _height, SYSTEM::GWindowStyle::WINDOWEDBORDERED);
					break;
				}

				case SYSTEM::GWindowStyle::WINDOWEDBORDERLESS:
				{
					// technically works, still has the main 3 navigation buttons up top though,
					// unsure of what that will do if you are displaying a full image on client area.
					auto process = CallOnMainViewUiThreadAsync([]()
						{
							auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
							if (view.IsFullScreenMode())
							{
								view.GetForCurrentView().ExitFullScreenMode();
							}
							auto titleBar = winrt::Windows::ApplicationModel::Core::CoreApplication::GetCurrentView().TitleBar();
							titleBar.ExtendViewIntoTitleBar(true);
						}); process.get();

					ResizeWindow(_width, _height);

					SetInteralData(_x, _y, _width, _height, SYSTEM::GWindowStyle::WINDOWEDBORDERLESS);
					break;
				}

				// might be possible, but needed to move on to other libraries before graduation
				case SYSTEM::GWindowStyle::WINDOWEDLOCKED:
				{
					return GReturn::FEATURE_UNSUPPORTED;
					break;
				}

				// working to the best of my ability as of right now
				case SYSTEM::GWindowStyle::FULLSCREENBORDERED:
				{
					int x = 0, y = 0;

					auto process = CallOnMainViewUiThreadAsync([&x, &y]()
						{
							//auto win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
							auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
							auto di = winrt::Windows::Graphics::Display::DisplayInformation::GetForCurrentView();
							const float diWidth = static_cast<float>(di.ScreenWidthInRawPixels());
							const float diHeight = static_cast<float>(di.ScreenHeightInRawPixels());
							auto size = winrt::Windows::Foundation::Size(diWidth, diHeight);
							// this is done to give room for the task bar in the window
							size.Height -= 100;
							size.Width -= 100;
							if (!view.TryResizeView(size))
								GReturn::FAILURE;

							auto sizeWin = view.VisibleBounds();
							x = static_cast<int>(sizeWin.Width);
							y = static_cast<int>(sizeWin.Height);

						}); process.get();

					SetInteralData(0, 0, x, y, SYSTEM::GWindowStyle::FULLSCREENBORDERED);
					break;
				}

				case SYSTEM::GWindowStyle::FULLSCREENBORDERLESS:
				{
					int x = 0, y = 0;
					auto process = CallOnMainViewUiThreadAsync([&x, &y]()
						{
							auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
							if (!view.TryEnterFullScreenMode())
							{
								return GReturn::FAILURE;
							}

							auto di = winrt::Windows::Graphics::Display::DisplayInformation::GetForCurrentView();
							const float diWidth = static_cast<float>(di.ScreenWidthInRawPixels());
							const float diHeight = static_cast<float>(di.ScreenHeightInRawPixels());
							auto size = winrt::Windows::Foundation::Size(diWidth, diHeight);

							x = static_cast<int>(size.Width);
							y = static_cast<int>(size.Height);

							return GReturn::SUCCESS;
						}); process.get();
					SetInteralData(0, 0, x, y, SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
					break;
				}
				
				// might be possible, but needed to move on to other libraries before graduation
				case SYSTEM::GWindowStyle::MINIMIZED:
				{
					GReturn::FEATURE_UNSUPPORTED;
				}
				break;
				}
				return  GReturn::SUCCESS;
			}

			// Appends this to the Display name set in the manifest file.
			GReturn SetWindowName(const char* _newName) override
			{
				if (_newName == nullptr)
					return GReturn::INVALID_ARGUMENT;

				auto process = CallOnMainViewUiThreadAsync([&_newName]()
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						view.Title(INTERNAL::utf8_decode(std::string(_newName)));
					}); process.get();
				return GReturn::SUCCESS;
			}

			// might be possible, but needed to move on to other libraries before graduation
			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			// might be possible, but needed to move on to other libraries before graduation
			GReturn MoveWindow(int _x, int _y) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			// Currently this changes the whole window size, not the client
			// That will need to be changed in the future.
			GReturn ResizeWindow(int _width, int _height) override
			{
				SetInteralData(m_WindowX, m_WindowY, _width, _height, m_WindowStyle);
				bool sizeWorked = false;
				GAMING_DEVICE_MODEL_INFORMATION info = {};
				GetGamingDeviceModelInformation(&info);
				if (info.vendorId == GAMING_DEVICE_VENDOR_ID_MICROSOFT)
				{
					switch (info.deviceId)
					{
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE:
						
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_S:
						
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X:
						
					case GAMING_DEVICE_DEVICE_ID_XBOX_ONE_X_DEVKIT:
						return GReturn::FEATURE_UNSUPPORTED;
					default:
						break;
					}
				}
				auto process = CallOnMainViewUiThreadAsync([&_width, &_height, &sizeWorked]()
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						//auto titleBarHeight = winrt::Windows::ApplicationModel::Core::CoreApplication::GetCurrentView().TitleBar().Height();
						// if resizing window and in fullscreen mode, exit fullscreen mode first
						if (view.IsFullScreenMode())
						{
							view.ExitFullScreenMode();
						}

						auto size = winrt::Windows::Foundation::Size(static_cast<float>(_width), static_cast<float>(_height));
						if (view.TryResizeView(size))
						{
							// resize worked
							sizeWorked = true;

						}
					}); process.get();
				
				// if failing on desktop and you don't think it should, check to make sure you are not going below the allowed size for the app window - that can be found in GApp_uwp
				return sizeWorked ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn Maximize() override
			{
				if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERED ||
					!m_isInFocus ||
					m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDLOCKED)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERED);
				else if (m_WindowStyle == SYSTEM::GWindowStyle::WINDOWEDBORDERLESS)
					return ChangeWindowStyle(SYSTEM::GWindowStyle::FULLSCREENBORDERLESS);
				return GReturn::REDUNDANT;
			}

			GReturn Minimize() override
			{
				//return ChangeWindowStyle(SYSTEM::GWindowStyle::MINIMIZED);
				return GReturn::FEATURE_UNSUPPORTED;
			}

			GReturn ChangeWindowStyle(SYSTEM::GWindowStyle _style) override
			{
				return ReconfigureWindow(m_WindowX, m_WindowY, m_WindowWidth, m_WindowHeight, _style);
			}

			GReturn GetWidth(unsigned int& _outWidth) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outWidth]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto size = view.VisibleBounds();
						_outWidth = static_cast<unsigned int>(size.Width);
					}); process.get();
				//_outWidth = m_WindowWidth;
				return  GReturn::SUCCESS;
			}

			GReturn GetHeight(unsigned int& _outHeight) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outHeight]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto size = view.VisibleBounds();
						_outHeight = static_cast<unsigned int>(size.Height);
					}); process.get();
				//_outHeight = m_WindowHeight;
				return  GReturn::SUCCESS;
			}

			GReturn GetClientWidth(unsigned int& _outClientWidth) const override
			{
				threadShare.LockAsyncRead();
				int retVal = m_WindowWidth;
				threadShare.UnlockAsyncRead();
				_outClientWidth = retVal;

				return GReturn::SUCCESS;
			}

			GReturn GetClientHeight(unsigned int& _outClientHeight) const override
			{
				threadShare.LockAsyncRead();
				int retVal = m_WindowHeight;
				threadShare.UnlockAsyncRead();
				_outClientHeight = retVal;

				return GReturn::SUCCESS;
			}

			GReturn GetX(unsigned int& _outX) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outX]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto size = view.VisibleBounds();
						_outX = static_cast<unsigned int>(size.X);
					}); process.get();

				return GReturn::SUCCESS;
			}

			GReturn GetY(unsigned int& _outY) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outY]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto size = view.VisibleBounds();
						_outY = static_cast<unsigned int>(size.Y);
					}); process.get();

				return GReturn::SUCCESS;
			}

			GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outX, &_outY]() mutable
					{
						// run on UI thread
						winrt::agile_ref<winrt::Windows::UI::Core::CoreWindow> win;
						win = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
						winrt::Windows::Foundation::Rect windowRect;
						windowRect = win.get().Bounds();
						_outX = static_cast<unsigned int>(windowRect.X);
						_outY = static_cast<unsigned int>(windowRect.Y);
					}); process.get();

				return GReturn::SUCCESS;
			}

			// returns the CoreWindow
			// whe return value can be transferred back into a CoreWindow if on main thread
			GReturn GetWindowHandle(SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outUniversalWindowHandle]() mutable
					{
						static winrt::Windows::UI::Core::CoreWindow coreWindow = nullptr;
						// run on UI thread
						coreWindow = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();
						_outUniversalWindowHandle.window = &coreWindow;
						_outUniversalWindowHandle.display = nullptr;
					}); process.get();

				return GReturn::SUCCESS;
			}

			GReturn IsFullscreen(bool& _outIsFullscreen) const override
			{
				auto process = CallOnMainViewUiThreadAsync([&_outIsFullscreen]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						_outIsFullscreen = view.IsFullScreenMode();
					}); process.get();

				return GReturn::SUCCESS;
			}

			GReturn IsFocus(bool& _outIsFocus) const override
			{
				_outIsFocus = m_isFocus ? true : false;
				return m_isInFocus ? GReturn::SUCCESS : GReturn::FAILURE;
			}

			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override { return GEventGeneratorImplementation::Register(_observer, _callback); }
			GReturn Observers(unsigned int& _outCount) const override { return GEventGeneratorImplementation::Observers(_outCount); }
			GReturn Push(const GEvent& _newEvent) override { return GEventGeneratorImplementation::Push(_newEvent); }
		};
	}
}