// Link to our internal thread pool
#include "../GConcurrent/gw_internal_threadpool.hpp"
#include "../../../Interface/Core/GEventReceiver.h"

#include <vector>
#include <chrono>

// Make the implentation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I { // Going to switch to using containment for GEventGenerator (safer)
		class GDaemonImplementation : public virtual GDaemonInterface
		{
            // GDaemonImplementation HAS A GEventGenerator.
            // This will be safer than private Inheritance when we need to pass the generator around.
            CORE::GEventGenerator generator;
            // defines what a daemon actually is
			typedef std::tuple<std::chrono::time_point<std::chrono::steady_clock>,
				unsigned long long, CORE::GEventReceiver> Daemon;
			// Global daemon management variables, initialized below the class
			// responsible for creating and joining/stopping the daemon visitor
			class GDaemonManager {
			public:
				// Used to control access to the internal global daemon resources, must also be global
				CORE::GThreadShared g_daemonAccess;
				// Allows external insertion into the daemon list (prevents blocking)
				std::vector<Daemon> g_insertionStack; // order not critical so we favor cache perf
				// This just exists to allow GEventReceivers to exist
				CORE::GEventGenerator g_daemonNotifier;
				// Used to enable more advanced and careful sleeping in the manager thread
				std::condition_variable g_managerNotifier;
				// launches daemon thread
				GDaemonManager() {
					g_daemonAccess.Create();
					g_daemonNotifier.Create();
					g_daemonVisitor = std::thread(&GDaemonManager::processWaitingDaemons, this);
				}
				// frees daemon thread
				~GDaemonManager() {
					// were done now, this will signal thread to exit
					g_daemonNotifier = nullptr;
					if (g_daemonVisitor.joinable())
					{
						// its time to go... WAKE UP!!!
						g_managerNotifier.notify_one();
						g_daemonVisitor.join();
					}
					g_daemonAccess = nullptr;
				}
			private:
				// Global list of all daemons, traversed by worker thread adding them to the thread pool as needed. 
				std::vector<Daemon> g_daemons; // stores all daemons
				// Global worker thread not part of the main thread pool but responsible for adding to it.
				// Having g_daemonVisitor and GDaemonManager allows for safe termination of the Daemons with the
				// added benefit of being able to debug the end of the system. Without this, daemons would
				// have to be detached and There wouldn't be a guaranteed way to allow the thread to exit 
				// normally at the end of main.
				std::thread g_daemonVisitor;
				// routine that manages the daemon set until the last has finished (defined below)
				void processWaitingDaemons()
				{
					std::mutex sleeper; // used only for more controlled naps with std::condition_variable 
					// local function to update thread sleep time as needed
					auto updateNextWake = [](const std::chrono::time_point<std::chrono::steady_clock> upnext,
						std::chrono::time_point<std::chrono::steady_clock>& nextwake, bool& allow)
					{
						if (allow) // can only be updated once by the youngest
						{
							// we shorten the sleep time by potential spin time to better ensure launch
							nextwake = upnext - std::chrono::microseconds(G_DAEMON_LAUNCH_THRESHOLD);
							allow = false; // no more changes allowed, stay young
						}
					};
					do // loop until the program ends
					{
						bool allow_adj = true; // has the wake time been adjusted yet? (only allow once per loop)
						// grab the original time at loop start
						std::chrono::time_point<std::chrono::steady_clock> origin = std::chrono::steady_clock::now();
						// by default the next cycle is at "G_THREAD_DEFAULT_SLEEP" from now.
						// however if there are unlaunched items in the list below we sleep till right before the next up. 
						// Var used to record the youngest item that is not going to be launched this cycle.
						std::chrono::time_point<std::chrono::steady_clock> nextwake =
							origin + std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP);
						// lock the daemon array, sort it, traverse it, unlock it.
						if (+g_daemonAccess.LockSyncWrite())
						{
							// transfer any waiting Daemons into the main list. (staging)
							while (g_insertionStack.empty() == false)
							{
								g_daemons.push_back(g_insertionStack.back());
								g_insertionStack.pop_back();
							}
							// once we have inserted any waiting Daemons into the list we unlock to not cause stalls 
							g_daemonAccess.UnlockSyncWrite();

							//num_daemons = g_daemons.size(); // how many do we have now?
							std::sort(g_daemons.begin(), g_daemons.end(), sortWaitingDaemons);
							for (auto i = g_daemons.begin(); i != g_daemons.end(); ) // iteration is controlled inside
							{
								// grab the exact time at loop start
								std::chrono::time_point<std::chrono::steady_clock> exact = std::chrono::steady_clock::now();
								// if the GEventGenerator(GDaemon) is dead remove it from the list. 
								if (std::get<2>(*i) == nullptr)
								{
									i = g_daemons.erase(i); // get rid of this one and move to the next
								}
								// launch any valid daemons who are past time.
								else if (std::get<0>(*i) <= exact)
								{
									// deltaLag describes how far behind the daemon is.
									// numPeriods is how many periods have been missed in that time(deltaLag).
									// So, if this daemon is in the past after 1 period increase, 
									// set it's timer for the period it 'should' be at.
									// (currentTime += numOfMissedPeriods * periodLength)
									// This method is to prevent a "rubber-banding" sort of behavior.
									unsigned long long deltaLag = 
										std::chrono::duration_cast<std::chrono::milliseconds>(exact - std::get<0>(*i)).count();
									unsigned long long numPeriods = deltaLag / std::get<1>(*i);
									if ((std::get<0>(*i) + std::chrono::milliseconds(std::get<1>(*i))) < exact)
										std::get<0>(*i) += std::chrono::milliseconds(numPeriods * std::get<1>(*i));
									// before we launch we set the time to the next time step. (add time)
									std::get<0>(*i) += std::chrono::milliseconds(std::get<1>(*i));
									// update the wake cycle as needed
									updateNextWake(std::get<0>(*i), nextwake, allow_adj);
									// weak handle to GEventReceiver which we will invoke
									auto caller = std::get<2>(*i);
									// add to thread-pool, invoke Daemon routine
									internal_gw::GatewareThreadPool().AddJob([caller]() {
										// should be safe and not cause oddness
										caller.Invoke();
									});
									// move to the next item and continue traversal
									++i; // see if anyone else needs to be launched
								}
								else // if we get here we know the current Daemon is in the future
								{
									// how much time till this must launch?
									unsigned long long microleft =
										std::chrono::duration_cast<std::chrono::microseconds>(exact - std::get<0>(*i)).count();
									// if a daemon is less than G_DAEMON_LAUNCH_THRESHOLD microseconds from starting we spin-lock.
									if (microleft <= G_DAEMON_LAUNCH_THRESHOLD)
									{
										while (std::chrono::steady_clock::now() < std::get<0>(*i))
											std::this_thread::yield(); // spin lock and restart loop to launch
									}
									else // not time for this one yet, we move on
									{
										// update the wake cycle as needed
										updateNextWake(std::get<0>(*i), nextwake, allow_adj);
										++i;
									}
								}
							}
						}
						// once we exit the loop we have this thread take a short nap.
						// we wait for the default thread sleep time: G_THREAD_DEFAULT_SLEEP (typically 16ms)
						// reduce by the launch threshold so we have a better possibility of launching on time
						// if possible we wait for the next launching time span.
						// If a new daemon is added or a destructor is invoked we will be notified while sleeping.
						// The spurious wakeup will wake the thread no matter what since it is always safe to do so.
						{
							std::unique_lock<std::mutex> locker(sleeper);
							g_managerNotifier.wait_until(locker, nextwake, [&]() { return true; });
							///std::this_thread::sleep_until(nextwake); // how we used to do it
						}
					// The loop ends when g_daemonNotifier is destructed	
					} while (g_daemonNotifier);
				}
			};
			//static GDaemonManager g_daemonManager;
			static GDaemonManager& GetDaemonManager()
			{
				static GDaemonManager g_daemonManager;
				return g_daemonManager;
			}
			static bool sortWaitingDaemons(const GDaemonImplementation::Daemon& _a,
				const GDaemonImplementation::Daemon& _b)
			{
				return std::get<0>(_a) < std::get<0>(_b); // compare time points
			}
			// daemons are sorted by when they must next run in ascending order (oldest first)
			
			// * End Globals, start local variables *

			// used to pause execution of this daemon (disabled during Initialization)
			std::atomic_bool paused = { true };
			// flag that controls pausing and resuming of a daemon
			std::atomic_flag running = ATOMIC_FLAG_INIT;
			// keeps track of the current thread running our daemon to avoid possible deadlock
			std::atomic<std::thread::id> runner; // default constructor is a non-thread
			// the amount of time (milliseconds) between execution events
			unsigned int period = 0;
			// tracks the number of complete executions of the daemon
			std::atomic_uint64_t count;
			// caches a copy of the routine to be applied at the interval
			CORE::GEventReceiver daemon;
		public:
			// required for HAS-A relationship
			GReturn Register(CORE::GEventCache _observer) override {
				return generator.Register(_observer);
			}
			GReturn Register(CORE::GEventResponder _observer) override {
				return generator.Register(_observer);
			}
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
				return generator.Register(_observer, _callback);
			}
			GReturn Deregister(CORE::GInterface _observer) override {
				return generator.Deregister(_observer);
			}
			GReturn Observers(unsigned int& _outCount) const override {
				return generator.Observers(_outCount);
			}
			GReturn Push(const GEvent& _newEvent) override {
				return generator.Push(_newEvent);
			}
			// actual implementation functions here
			// alternative create, allows user to start the daemon paused
			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation, unsigned int _delay)
			{
				if (_targetInterval == 0) // daemons can't run faster than 1000hz
					return GReturn::INVALID_ARGUMENT;
				// If a _delay was supplied we need to unpause this daemon immediately (otherwise start paused)
				if (_delay > 0)
					paused = false; // initialized to true by default (for simplified init reasons)
                // make an internal event generator to implement that functionality
                // this may not be as efficient as private inheritance but allows for
                // safe message sending if the _daemonOperation is used to delete/replace this proxy
                generator.Create();
				period = _targetInterval; // fixed time step between daemon invocations
				count = 0; // nothing has been executed yet
				// create daemon here
				daemon.Create(GetDaemonManager().g_daemonNotifier, [&, _daemonOperation]() {
					// This is called whether we are ready or not.
					// Only run code if we are not already running
					if (std::atomic_flag_test_and_set_explicit(&running, std::memory_order_acquire) == false)
					{
						if (paused == false) // only execute when not paused
						{
							GEvent send; // event package
							runner = std::this_thread::get_id(); // avoid deadlocks
                            // this operation could technically erase this class so save a safe handle
                            CORE::GEventGenerator safe = generator;
                            if (_daemonOperation) // these are optional so we need to check
								_daemonOperation(); // we assume this MAY delete "this"
							// increase count, notify listeners and unlock spin if still possible
							CORE::GEventGenerator::burst_w alive = *safe;
							if (alive)
							{	// notify anyone listening we have completed
								unsigned long long c = ++count; // increase & copy count to POD
								send.Write(Events::OPERATION_COMPLETED, EVENT_DATA{ c }); // send current count
								alive->Push(send); // a daemon run is not done till eveyone has received it
								std::atomic_flag_clear_explicit(&running, std::memory_order_release);
							}
						}
						else // allow execution again
							std::atomic_flag_clear_explicit(&running, std::memory_order_release);
					}
					// done
				});
				// add ourselves to the vector
				// insert our daemon into the global list for processing
				GetDaemonManager().g_daemonAccess.LockSyncWrite();
				// insert relevant data to begin processing
				GetDaemonManager().g_insertionStack.push_back(
					{	std::chrono::steady_clock::now() + std::chrono::milliseconds(_delay),
						static_cast<unsigned long long>(period), daemon });
				GetDaemonManager().g_daemonAccess.UnlockSyncWrite();
				// wakeup the manager thread if it is snoozing
				GetDaemonManager().g_managerNotifier.notify_one();
				// were done here, the daemon processor will takeover.
				return GReturn::SUCCESS;
			}
			// main create, starts the daemon immediately unpaused
			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation)
			{
				paused = false; // start running right away! (true by default)
				return Create(_targetInterval, _daemonOperation, 0);
			}
			// alternate create, provide swappable logic
			GReturn Create(CORE::GLogic _daemonLogic, unsigned int _targetInterval, unsigned int _delayOrPause)
			{
				if (_daemonLogic == nullptr) return GReturn::INVALID_ARGUMENT;
				return Create(_targetInterval, [_daemonLogic]() { _daemonLogic.Invoke(); }, _delayOrPause);
			}
			GReturn Pause(bool _wait, unsigned int _spinUntil) override
			{
				if (paused == true) // no need to do this twice
					return GReturn::REDUNDANT;
				// inform the external threads to no longer execute our daemon
				paused = true;
				// if _wait == true we use the spin lock technique from GConcurrent
				if (_wait)
				{
					// are we inside the daemon thread?
					if (runner == std::this_thread::get_id())
					{
						paused = false; // didn't work so don't change the behavior
						return GReturn::DEADLOCK; // This would cause a deadlock!
					}
					// waits until "working" is false, sleeping the thread for 1ms each time _spinUntil is reached.
					auto last = std::chrono::steady_clock::now();
					// This operation is lock free, so very optimized for short wait times
					while (std::atomic_flag_test_and_set_explicit(&running, std::memory_order_acquire))
					{	// measures how much time this thread is spin locked
						if (std::chrono::duration_cast<std::chrono::nanoseconds>(
							std::chrono::steady_clock::now() - last).count() >= _spinUntil)
						{
							// if things take longer than our maximum lock time we free the thread and try again later
							std::this_thread::sleep_for(std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP));
							last = std::chrono::steady_clock::now();
						}
					}
					// Once we have converged we release our lock so we can lock again later.
					std::atomic_flag_clear_explicit(&running, std::memory_order_release);
				}
				// notify anyone listening a pause has been requested.
				GEvent send;
				unsigned long long c = count;
				send.Write(Events::OPERATIONS_PAUSED, EVENT_DATA{ c });
				Push(send); // let everyone know who cares
				// so far so good
				return GReturn::SUCCESS;
			}
			GReturn Resume() override
			{
				if (paused == false) // no need to do this twice
					return GReturn::REDUNDANT;
				// since we don't wait in this function, checking for deadlocks is not required
				paused = false;
				// notify anyone listening a resume has been requested.
				GEvent send;
				unsigned long long c = count;
				send.Write(Events::OPERATIONS_RESUMING, EVENT_DATA{ c });
				Push(send); // let everyone know who cares
				return GReturn::SUCCESS;
			}
			GReturn Counter(unsigned long long& _outCounter) const override
			{
				_outCounter = count; // this can't really fail
				return GReturn::SUCCESS;
			}
			// destructor ensures no waiting jobs exist
			~GDaemonImplementation()
			{
				// wakeup the manager thread if it is snoozing
				GetDaemonManager().g_managerNotifier.notify_one();
				// ensure we do not run any more daemon code
				Pause(true, 0);
			}
		};
	} // end CORE
} // end GW
