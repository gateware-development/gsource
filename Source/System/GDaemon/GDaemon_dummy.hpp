// dummy implementation of GDaemon
namespace GW {
	namespace I {
		class GDaemonImplementation :	public virtual GDaemonInterface,
										public GEventGeneratorImplementation
		{
		public:
			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Create(unsigned int _targetInterval, 
				std::function<void()> _daemonOperation, unsigned int _delayOrPause) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Create(CORE::GLogic _daemonLogic, 
				unsigned int _targetInterval, unsigned int _delayOrPause) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Pause(bool _wait, unsigned int _spinUntil) override {
				return GReturn::FAILURE;
			}
			GReturn Resume() override {
				return GReturn::FAILURE;
			}
			GReturn Counter(unsigned long long& _outCounter) const override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW