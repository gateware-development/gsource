//// Link to our internal thread pool
//#include "../GConcurrent/gw_internal_threadpool.h"
//
//// Make the implentation belong to the proper gateware namespace
//// We cannot use "using" here as this is an HPP and supports header only deployments
//namespace GW {
//	namespace I {
//		class GDaemonImplementation : public virtual GDaemonInterface,
//			protected GEventGeneratorImplementation,
//			public std::enable_shared_from_this<GEventGeneratorInterface>
//		{
//
//			// Global daemon managment variables, intialized below the class 
//			///static std::atomic_uint g_daemonCount; // how many are active total
//			///static std::atomic_bool g_daemonRunning; // Is the manager done?
//			// Used to control access to the internal global daemon resources, must also be global
//			static CORE::GThreadShared g_daemonAccess;
//			// Global list of all daemons, traversed by worker thread adding them to the thread pool as needed. 
//			typedef std::tuple<std::chrono::time_point<std::chrono::steady_clock>,
//				CORE::GEventGenerator, GDaemonImplementation*> Daemon;
//			static std::vector<Daemon> g_daemons; // stores all daemons
//			// Global worker thread not part of the main thread pool but responsible for adding to it.
//			// Having g_daemonVisitor and GDaemonManager allows for safe termination of the Daemons with the
//			// added benefit of being able to debug the end of the system. Without this, daemons would
//			// have to be detached and There wouldn't be a guaranteed way to allow the thread to exit 
//			// normally at the end of main.
//			static std::thread g_daemonVisitor;
//			// responsible for creating and joining/stopping the daemon visitor
//			struct GDaemonManager {
//				GDaemonManager() {
//					g_daemonAccess.Create();
//					g_daemonVisitor = std::thread(processWaitingDaemons);
//				}
//				~GDaemonManager() {
//					// delete the Access object, grab it and let go and destroy it
//					g_daemonAccess.LockSyncWrite();
//					g_daemonAccess.UnlockSyncWrite();
//					g_daemonAccess = nullptr; // were done now, this will signal thread to exit
//					if (g_daemonVisitor.joinable()) {
//						g_daemonVisitor.join();
//						std::cout << "g_daemonVisitor joined\n" << std::endl;
//					}
//				}
//			};
//			static GDaemonManager g_daemonManager;
//			// Global routines that manage the daemon set until the last has finshed (defined below)
//			// daemons are sorted by when they must next run in accending order (oldest first)
//			static bool sortWaitingDaemons(const Daemon& _a, const Daemon& _b);
//			static void processWaitingDaemons();
//
//			// * End Globals, start local variables *
//
//			// flag that controls pauseing and resuming of a daemon
//			std::atomic_flag running = ATOMIC_FLAG_INIT;
//			// keeps track of the current thread running our daemon to avoid possible deadlock
//			std::atomic<std::thread::id> runner; // deafault constructor is a non-thread
//			// the amount of time (milliseconds) between execution events
//			unsigned int period = 0;
//			// tracks the number of complete executions of the daemon
//			std::atomic_uint64_t count;
//			// caches a copy of the routine to be applied at the interval
//			std::function<void()> daemon;
//		public:
//			// Theses functions are overriden so we don't inherit via dominance (eliminates warning)
//			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override {
//				return GEventGeneratorImplementation::Register(_observer, _callback);
//			}
//			GReturn Observers(unsigned int& _outCount) const override {
//				return GEventGeneratorImplementation::Observers(_outCount);
//			}
//			GReturn Push(const GEvent& _newEvent) override {
//				return GEventGeneratorImplementation::Push(_newEvent);
//			}
//			// actual implementation functions here
//			GReturn Create(unsigned int _targetInterval, std::function<void()> _daemonOperation)
//			{
//				if (_targetInterval == 0) // daemons can't run faster than 1000hz
//					return GReturn::INVALID_ARGUMENT;
//
//				period = _targetInterval; // fixed time step between daemon invocations
//				count = 0; // nothing has been excecuted yet
//				daemon = _daemonOperation; // copy/move our operation to local storage
//
//				// increase the count of active background daemons.
//				//if (++g_daemonCount == 1) // is this the first?
//				{
//					// Launch our global recurring daemon manager if is not yet running.
//					///g_daemonAccess.Create(); // used to protect the daemons
//					//std::cout << "Create! DaemonCount == 1\n" << std::endl;
//					//if (!g_daemonRunning)
//					{
//						//std::cout << "Create! g_daemon-NOT-running!\n" << std::endl;
//						//if (g_daemonVisitor.joinable())
//						//	g_daemonVisitor.join();
//
//						/*if (g_daemonAccess == nullptr)
//							g_daemonAccess.Create();*/
//
//						// join(if size == 0) with read access if possible before continuing
//
//						// after we ensure the thread has been joined grab write access
//
//						/*
//						The problem: g_daemonAccess causes a deadlock when waiting to join.
//						if you lock down to check the count you also cause the processing thread to wait
//						then if you decide to join in that statement it will wait forever even if count is 0
//						Solution: ??? we need to wait for thread to end on zero size but not allow adding to size?
//						This means we need a writelock to check the size but then also join... which causes a writelock :/
//						
//						IDEA: Just create it in static constructor once, let it sleep until needed.
//						this removes the need to join for a relaunch. (not amazing but should work)
//						*/
//
//
//
//
//						//std::size_t numDaemons = 0;
//						//// if there was only one when we added ourselves we start a manager thread
//						//if (numDaemons == 0)
//						//{
//						//	if (g_daemonVisitor.joinable()) // wait for current manager to exit.
//						//		g_daemonVisitor.join(); // having this in above statement caused a deadlock
//						//	g_daemonVisitor = std::thread(processWaitingDaemons); // launch
//						//}
//						// add ourselves no matter what
//						g_daemonAccess.LockSyncWrite();
//							
//							// insert relevant data to begin processing
//							CORE::GEventGenerator send(shared_from_this());
//							g_daemons.push_back(
//								{ std::chrono::steady_clock::now() +
//								std::chrono::milliseconds(period), send, this });
//							// how many are in the vector?
//							//numDaemons = g_daemons.size();
//						
//						g_daemonAccess.UnlockSyncWrite();
//						
//						//std::this_thread::sleep_for(std::chrono::microseconds(3));
//
//					}
//					// detatching this thread ensures we won't join accidentally (in this destructor) when
//					// the last copy of a GDaemon is actually on the above thread. (which is an error)
//					//daemonVisitor.detach(); // make the manager a daemon thread itself
//				}
//				// insert our daemon into the global list for processing
//				// g_daemonAccess.LockSyncWrite();
//				// insert relevant data to begin processing
//				//CORE::GEventGenerator send(shared_from_this());
//				//g_daemons.push_back(
//				//	{ std::chrono::steady_clock::now() +
//				//		std::chrono::milliseconds(period), send, this });
//				// g_daemonAccess.UnlockSyncWrite();
//				// were done here, the daemon processor will takeover.
//				return GReturn::SUCCESS;
//			}
//			GReturn Pause(bool _wait, unsigned int _spinUntil) override
//			{
//				GReturn result = GReturn::FAILURE;
//				// just move next operation to the far far future
//				g_daemonAccess.LockSyncWrite();// safe because Pause is never be called on the daemon manager
//					// find our entry in the daemon array and move it to the far future
//				auto i = g_daemons.begin(); // outside so we can check for issues
//				for (; i != g_daemons.end(); ++i)
//					if (this == std::get<2>(*i))
//					{
//						if (std::get<0>(*i) != std::chrono::steady_clock::time_point::max())
//						{
//							std::get<0>(*i) = std::chrono::steady_clock::time_point::max();
//							result = GReturn::SUCCESS;
//						}
//						else
//							result = GReturn::REDUNDANT;
//						break;
//					}
//				// additonal error checking for things that should never happen
//				if (g_daemons.empty() == false && i == g_daemons.end())
//					result = GReturn::UNEXPECTED_RESULT; // How??
//				g_daemonAccess.UnlockSyncWrite();
//				// if _wait == true we use the spin lock technique from GConcurrent
//				if (_wait)
//				{
//					// are we inside the daemon thread?
//					if (runner == std::this_thread::get_id())
//						return result; // This would cause a deadlock!
//					// waits until "working" is false, sleeping the thread for 1ms each time _spinUntil is reached.
//					auto last = std::chrono::steady_clock::now();
//					// This operation is lock free, so very optimized for short wait times
//					while (std::atomic_flag_test_and_set_explicit(&running, std::memory_order_acquire))
//					{	// measures how much time this thread is spin locked
//						if (std::chrono::duration_cast<std::chrono::nanoseconds>(
//							std::chrono::steady_clock::now() - last).count() >= _spinUntil)
//						{
//							// if things take longer than our maximum lock time we free the thread and try again later
//							std::this_thread::sleep_for(std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP));
//							last = std::chrono::steady_clock::now();
//						}
//					}
//					// Once we have converged we release our lock so we can lock again later.
//					std::atomic_flag_clear_explicit(&running, std::memory_order_release);
//				}
//				return result;
//			}
//			GReturn Resume() override
//			{
//				// since we don't wait in this function, checking for deadlocks is not required
//				GReturn result = GReturn::UNEXPECTED_RESULT; // should NOT happen
//				g_daemonAccess.LockSyncWrite(); // safe because Resume is never be called on the daemon manager
//				// find our entry, if it is paused place next time point to right now+interval
//				for (auto i = g_daemons.begin(); i != g_daemons.end(); ++i)
//					if (this == std::get<2>(*i))
//					{
//						if (std::get<0>(*i) == std::chrono::steady_clock::time_point::max())
//						{
//							std::get<0>(*i) = std::chrono::steady_clock::now() + std::chrono::milliseconds(period);
//							result = GReturn::SUCCESS;
//						}
//						else
//							result = GReturn::REDUNDANT;
//						break;
//					}
//				// were done here
//				g_daemonAccess.UnlockSyncWrite();
//				return result;
//			}
//			GReturn Counter(unsigned long long& _outCounter) const override
//			{
//				_outCounter = count; // this can't really fail
//				return GReturn::SUCCESS;
//			}
//			// destructor drops the global count. if none left we remove the daemon manager
//			~GDaemonImplementation()
//			{
//				// * NEW * enusre we will get out of the manager
//				Pause(true, 100); // hmmmm
//
//				// initally I had a specific global object for my daemon thread.
//				// This was problematic because when I joined that object here when g_daemonCount == 0
//				// sometimes due to the auto deleting nature of proxies this destructor would get
//				// invoked by that actual thread. (I share this class with the thread)
//				// I then figured I could join if they were different threads. (check thread IDs)
//				// While this would have worked then the question became the need to resolve access
//				// to the thread object itself so we did not overwrite it at the wrong time.
//				// Rather than adding that additional overhead we remove the global thread object itself and
//				// simply use thread::detach() to make the thread a "deamon" thread not tied to any object.
//				// How very appropriate...
//				
//				//std::cout << "Thread: " << std::this_thread::get_id() << 
//				//	" is decrementing daemon count from " << g_daemonCount << 
//				//	" to " << g_daemonCount - 1 << std::endl;
//				///--g_daemonCount;
//			}
//		};
//		// C++11 requires we init the static variables here to avoid linking issues.
//		// In C++17 we could just do this in the class itself.
//		///std::atomic_uint GDaemonImplementation::g_daemonCount = { 0 };
//		///std::atomic_bool GDaemonImplementation::g_daemonRunning = { false };
//		CORE::GThreadShared GDaemonImplementation::g_daemonAccess;
//		std::vector<GDaemonImplementation::Daemon> GDaemonImplementation::g_daemons;
//		std::thread GDaemonImplementation::g_daemonVisitor;
//		GDaemonImplementation::GDaemonManager GDaemonImplementation::g_daemonManager;
//		// define global deamon managment routines
//		bool GDaemonImplementation::sortWaitingDaemons(const GDaemonImplementation::Daemon& _a,
//			const GDaemonImplementation::Daemon& _b)
//		{
//			return std::get<0>(_a) < std::get<0>(_b); // compare time points
//		}
//		// much of the work of GDaemon is done here
//		void GDaemonImplementation::processWaitingDaemons()
//		{
//			///g_daemonRunning = true;
//			// local function to update thread sleep time as needed
//			auto updateNextWake = [](const std::chrono::time_point<std::chrono::steady_clock> upnext,
//				std::chrono::time_point<std::chrono::steady_clock>& nextwake, bool& allow)
//			{
//				if (allow) // can only be updated once by the youngest
//				{
//					// we shorten the sleep time by potential spin time to better ensure launch
//					nextwake = upnext - std::chrono::microseconds(G_DAEMON_LAUNCH_THRESHOLD);
//					allow = false; // no more changes allowed, stay young
//				}
//			};
//			// as long as there are daemons we loop
//			//std::size_t num_daemons = 0; // how many are alive? (exit condition)
//			do // loop until daemon vector is empty
//			{
//				bool allow_adj = true; // has the wake time been adjusted yet? (only allow once per loop)
//				// grab the original time at loop start
//				std::chrono::time_point<std::chrono::steady_clock> origin = std::chrono::steady_clock::now();
//				// by default the next cycle is at "G_THREAD_DEFAULT_SLEEP" from now.
//				// however if there are unlaunched items in the list below we sleep till right before the next up. 
//				// Var used to record the youngest item that is not going to be launched this cycle.
//				std::chrono::time_point<std::chrono::steady_clock> nextwake =
//					origin + std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP);
//
//				// lock the daemon array, sort it, traverse it, unlock it.
//				if (+g_daemonAccess.LockSyncWrite())
//				{
//					//num_daemons = g_daemons.size(); // how many do we have now?
//					std::sort(g_daemons.begin(), g_daemons.end(), sortWaitingDaemons);
//					for (auto i = g_daemons.begin(); i != g_daemons.end(); ) // iteration is contolled inside
//					{
//						// grab the exact time at loop start
//						std::chrono::time_point<std::chrono::steady_clock> exact = std::chrono::steady_clock::now();
//						// this is a bit advanced... we need to temporarily lock down the bundled "this" pointer.
//						// If we don't we risk the Daemon falling out of scope during the function call.
//						// though normally not an issue with Proxys the bundled pointer is the risk.
//						// I may eventually re-engineer this with a GEventReceiver to make it cleaner.
//						CORE::GEventGenerator::burst_r run = *std::get<1>(*i);
//						// this pointer should be valid until "run" falls off the stack.
//						GDaemonImplementation* d = std::get<2>(*i);
//						// if the GEventGenerator(GDaemon) is dead remove it from the list. 
//						if (std::get<1>(*i) == nullptr)
//						{
//							std::cout << "erasing daemon from vector!\n" << std::endl;
//							i = g_daemons.erase(i); // get rid of this one and move to the next
//						}
//						// launch any valid daemons who are past time.
//						// The "running" flag ensures we do not run a daemon if it is already running
//						else if (run && std::get<0>(*i) <= exact &&
//							!std::atomic_flag_test_and_set_explicit(&d->running, std::memory_order_acquire))
//						{
//							// before we launch we set the time to the next time step. (add time)
//							// deltaLag describes how far behind the daemon is.
//							// numPeriods is how many periods have been missed in that time(deltaLag).
//							// So, if this daemon is in the past after 1 period increase, 
//							// set it's timer for the period it 'should' be at. (currentTime += numOfMissedPeriods * periodLength)
//							// This method is to prevent a "rubber-banding" sort of behavior.
//							unsigned long long deltaLag = std::chrono::duration_cast<std::chrono::milliseconds>(exact - std::get<0>(*i)).count();
//							unsigned long long numPeriods = deltaLag / static_cast<unsigned long long>(d->period);
//							if ((std::get<0>(*i) + std::chrono::milliseconds(d->period)) < exact)
//								std::get<0>(*i) += std::chrono::milliseconds(numPeriods * d->period);
//							//while(std::get<0>(*i) < exact)
//
//							std::get<0>(*i) += std::chrono::milliseconds(d->period);
//							// update the wake cycle as needed
//							updateNextWake(std::get<0>(*i), nextwake, allow_adj);
//							// add to threadpool, invoke Daemon routine notify listeners
//							internal_gw::GatewareThreadPool().AddJob([run, d]() {
//								GEvent send; // what people are listening for
//								// Mark this thread as the runner (for safety)
//								d->runner = std::this_thread::get_id();
//								// Call the daemon routine if we have one
//								if (d->daemon)	d->daemon();
//								// With the operation completed we notify anyone who cares
//								send.Write(Events::OPERATION_COMPLETED, ++d->count);
//								d->Push(send); // Notify anyone who is listening
//								// once were done mark we are no longer running
//								d->runner = std::thread::id();
//								// now that we are done, signal we are no longer running.
//								std::atomic_flag_clear_explicit(&d->running, std::memory_order_release);
//								});
//							// move to the next item and continue traversal
//							++i; // see if anyone else needs to be launched
//						}
//						else if (std::get<0>(*i) > exact)// if we get here we know the current Daemon is in the future
//						{
//							// how much time till this must launch?
//							unsigned long long microleft =
//								std::chrono::duration_cast<std::chrono::microseconds>(exact - std::get<0>(*i)).count();
//							// if a daemon is less than G_DAEMON_LAUNCH_THRESHOLD microseconds from starting we spinlock.
//							if (microleft <= G_DAEMON_LAUNCH_THRESHOLD)
//							{
//								while (std::chrono::steady_clock::now() < std::get<0>(*i))
//									std::this_thread::yield(); // spin lock and restart loop to launch
//							}
//							else // not time for this one yet, we move on
//							{
//								// update the wake cycle as needed
//								updateNextWake(std::get<0>(*i), nextwake, allow_adj);
//								++i;
//							}
//						}
//						else // if we get here that means that we are NOT in the future and are still running...
//						{
//							// we missed our window, shift to the next launch window
//							std::get<0>(*i) += std::chrono::milliseconds(d->period);
//
//							// update the wake cycle as needed
//							updateNextWake(std::get<0>(*i), nextwake, allow_adj);
//							++i; // skip to the next item so we don't spinlock here.
//						}
//						// paused items don't need special logic as their time is simply very far future.
//					}
//					g_daemonAccess.UnlockSyncWrite();
//				}
//				// once we exit the loop we have this thread take a short nap.
//				// we wait for the default thread sleep time: G_THREAD_DEFAULT_SLEEP (typically 1ms)
//				// reduce by the launch threshold so we have a better possibility of launching on time
//				// if possible we wait for the next launching time span. be aware this can delay new daemons.
//				// nextwake = now() + interval_time
//				//if (num_daemons > 0)
//					std::this_thread::sleep_until(nextwake);
//				// when all daemons have destructed we leave
//				
//				// To the brave soul debugging this:
//				// Somehow the deamon vector is empty but g_daemonCount is still at 1. 
//				// This keeps the program from closing.
//				// Introducing a 'sleep' of some sort circumvents this issue.
//				// Using cout statements and breakpoints or even actually thread::sleep will avoid this 
//				// scenario, but why?
//			} while (g_daemonAccess);
//			// notify Create we need another manager thread
//			///g_daemonRunning = false;
//
//			std::cout << "Daemon shutting down..." << std::endl;
//		}
//	} // end CORE
//} // end GW