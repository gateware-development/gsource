namespace GW
{
	namespace I
	{
		class GFileImplementation : public GFileInterface
		{
		public:
			GReturn Create() {
				return GReturn::INTERFACE_UNSUPPORTED;
			}

			GReturn OpenBinaryRead(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn OpenBinaryWrite(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn AppendBinaryWrite(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn OpenTextRead(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn OpenTextWrite(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn AppendTextWrite(const char* const _file) override {
				return GReturn::FAILURE;
			}

			GReturn Write(const char* const _inData, unsigned int _numBytes) override {
				return GReturn::FAILURE;
			}

			GReturn Read(char* _outData, unsigned int _numBytes) override {
				return GReturn::FAILURE;
			}

			GReturn WriteLine(const char* const _inData) override {
				return GReturn::FAILURE;
			}

			GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) override {
				return GReturn::FAILURE;
			}

			GReturn CloseFile() override {
				return GReturn::FAILURE;
			}

			GReturn FlushFile() override {
				return GReturn::FAILURE;
			}

			GReturn SetCurrentWorkingDirectory(const char* const _dir) override {
				return GReturn::FAILURE;
			}

			GReturn GetCurrentWorkingDirectory(char* _outDir, unsigned int _dirSize) override {
				return GReturn::FAILURE;
			}

			GReturn GetDirectorySize(unsigned int& _outSize) override {
				return GReturn::FAILURE;
			}

			GReturn GetSubDirectorySize(unsigned int& _outSize) override {
				return GReturn::FAILURE;
			}

			GReturn GetFilesFromDirectory(	char* _outFiles[], unsigned int _numFiles, 
											unsigned int _fileNameSize) override {
				return GReturn::FAILURE;
			}

			GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize,
											char* _outsubDir[]) override {
				return GReturn::FAILURE;
			}

			GReturn GetFileSize(const char* const _file, unsigned int& _outSize) override {
				return GReturn::FAILURE;
			}

			GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) override {
				return GReturn::FAILURE;
			}
			
			GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) override {
				return GReturn::FAILURE;
			}
			
			GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) override {
				return GReturn::FAILURE;
			}
			
			GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) override {
				return GReturn::FAILURE;
			}
			
			GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) override{
				return GReturn::FAILURE;
			}
			
			GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) override {
				return GReturn::FAILURE;
			}
		};
	}
}