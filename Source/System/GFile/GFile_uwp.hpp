#include <fstream>  //file streams
#include <ios>
#include <string>  //strings
#include <atomic>  //atomic variables
#include <mutex>  //mutex locks
#include <stdio.h>

#include <io.h>  //Included for mode change.
#include <fcntl.h>  //Included for mode change.
#include "direntw.hpp"
#include "../../Shared/uwputils.h"
#include "../../../Interface/Core/GThreadShared.h"
#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.Storage.h>

#ifndef DIR
#define DIR _WDIR
#endif
#ifndef dirent
#define dirent _wdirent
#endif
#ifndef string
#define string wstring
#endif

#ifndef opendir
#define opendir _wopendir
#endif
#ifndef readdir
#define readdir _wreaddir
#endif
#ifndef closedir
#define closedir _wclosedir
#endif
#ifndef rewinddir
#define rewinddir _wrewinddir
#endif

#ifndef DIR_SEPARATOR
#define DIR_SEPARATOR L'\\'
#endif

namespace GW
{
	namespace I
	{
		class GFileImplementation : public virtual GW::I::GFileInterface,
			protected GThreadSharedImplementation
		{
		private:
			DIR* currDirStream;  //Maintains the current directory.
			std::wfstream file;  //Maintains the current file (if one is open).
			std::fstream binaryFile; //for binary read and write
			char initialDir[250];
			
			unsigned int fileSize = 0;

			std::string currDir;  //A cached directory path for faster fetching.
			std::atomic<unsigned int> mode; //Used to track what open mode the file is in
			std::mutex lock; //Read/Write lock.
		public:
			GFileImplementation()
			{
				currDirStream = nullptr;
			}

			~GFileImplementation()
			{
				SetCurrentWorkingDirectory(initialDir);
				//Close the current directory.
				closedir(currDirStream);
				//Close the file stream.
				CloseFile();
			}

			GReturn Create()
			{
				//Set the current working directory to the directory the program was ran from.
				GReturn rv = SetCurrentWorkingDirectory("./");
				if (G_FAIL(rv))
					return rv;

				GetCurrentWorkingDirectory(initialDir, 250);

                // Create a UTF8 Locale to imbue the fstream with.
                std::locale utf8Locale("en_US.UTF8");

                // Imbue the fstream.
                file.imbue(utf8Locale);
                binaryFile.imbue(utf8Locale);

                return GReturn::SUCCESS;
			}

			GReturn OpenBinaryRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                binaryFile.open(INTERNAL::G_TO_UTF8(currDir) + _file, std::ios::in | std::ios::binary);

                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                binaryFile.open(INTERNAL::G_TO_UTF8(currDir) + _file, std::ios::out | std::ios::binary);

                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

                //Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                binaryFile.open(INTERNAL::G_TO_UTF8(currDir) + _file,
                                std::ios::out | std::ios::app | std::ios::ate | std::ios::binary);

                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + INTERNAL::G_TO_UTF16(_file), std::ios::in);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + INTERNAL::G_TO_UTF16(_file), std::ios::out);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + INTERNAL::G_TO_UTF16(_file), std::ios::out | std::ios::app | std::ios::ate);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn Write(const char* const _inData, unsigned int _numBytes) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr || _numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && !binaryFile.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Lock the write operations.
				lock.lock();

				//On windows we need to cast the char* to a wchar_t*.
				if (binaryFile.is_open())
					binaryFile.write(_inData, _numBytes);
				else
					file.write((wchar_t*)_inData, _numBytes);

				// update the file size
				fileSize += _numBytes;

				lock.unlock();

				return GReturn::SUCCESS;
			}

			GReturn Read(char* _outData, unsigned int _numBytes) override
			{
				if (_numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && !binaryFile.is_open())
				{
					_outData = nullptr;
					return GReturn::FAILURE;
				}

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				//Lock the read operations.
				lock.lock();

				if (binaryFile.is_open())
				{
					binaryFile.read(_outData, _numBytes);
					lock.unlock();
					if (binaryFile.eof()) return GReturn::END_OF_FILE;
					return GReturn::SUCCESS;
				}
				else
				{
					// this is only a problem on win32
					if ((_numBytes % 2) == 1) { // check for odd number of bytes to read because utf-16
						char tmp[2];
						//On Windows we need to cast the _outData char* to a wchar_t*.
						file.read((wchar_t*)_outData, _numBytes - 1); // read 1 less bytes into the buffer directly
						file.read((wchar_t*)tmp, 1); // read the last byte into the tmp
						_outData[_numBytes - 1] = tmp[0]; // assign the last byte
					}
					else {
						file.read((wchar_t*)_outData, _numBytes);
					}
				}

				lock.unlock();
				if (file.eof())
					return GReturn::END_OF_FILE;

				return GReturn::SUCCESS;
			}

			GReturn WriteLine(const char* const _inData) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Transfer the data to a string. #defines make it so the
				//string is what we need it to be on any system we support.
				std::string writeOutString = INTERNAL::G_TO_UTF16(_inData);

				//Lock the write operations.
				lock.lock();

				//Write out the string.
				file << writeOutString;

				// update the file size
				fileSize += static_cast<unsigned int>(writeOutString.length());

				lock.unlock();

				return GReturn::SUCCESS;
			}

			GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) override
			{
				if (_outData == nullptr || _outDataSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				//The string to be read into.
				std::string outString;

				//Lock the read operations.
				lock.lock();

				//Convert the UTF8 delimeter to UTF16.
				const wchar_t delimiter = *INTERNAL::G_TO_UTF16(&_delimiter).c_str();

				//Read the information.
				std::getline(file, outString, delimiter);

				//Copy the data over to the out parameter.
				strncpy_s(_outData, _outDataSize, INTERNAL::G_TO_UTF8(outString).c_str(), _TRUNCATE);

				//Copy the data over to the out parameter.
				strcpy_s(_outData, _outDataSize, INTERNAL::G_TO_UTF8(outString).c_str());

				lock.unlock();

				return GReturn::SUCCESS;
			}

			GReturn CloseFile() override
			{
				//If a file is not open, we can not close it.
				if (!file.is_open() && !binaryFile.is_open())
					return GReturn::FAILURE;

				if (binaryFile.is_open())
				{
                    binaryFile.flush();
                    binaryFile.close();
                }
				else
				{
					//Flush the file.
					file.flush();
					//Close the file.
					file.close();
				}

				// update the file size
				fileSize = 0;

				return GReturn::SUCCESS;
			}

			GReturn FlushFile() override
			{
                // If a file is not open we can not flush it.
                if (!file.is_open() && !binaryFile.is_open()) return GReturn::FAILURE;

                if (binaryFile.is_open())
                    binaryFile.flush();
                else
                    file.flush();

                return GReturn::SUCCESS;
            }

			GReturn SetCurrentWorkingDirectory(const char* const _dir) override
			{
				//Check for valid arguments.
				if (_dir == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Get the absolute path.
				wchar_t buffer[MAX_PATH];
				if (_wfullpath(buffer, INTERNAL::G_TO_UTF16(_dir).c_str(), MAX_PATH) == nullptr)
					return GReturn::FAILURE;

				//Check to make sure the directory exists.
				struct _stat s;
				if (_wstat(buffer, &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Assign the passed in directory to our internal directory storage.
				currDir = buffer;
				currDir += DIR_SEPARATOR;

				//If there is an open directory, close it.
				if (currDirStream != nullptr)
					closedir(currDirStream);

				//Open new directory.
				currDirStream = opendir(currDir.c_str());

				//Check to ensure directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}

			GReturn GetCurrentWorkingDirectory(char* _dir, unsigned int _dirSize) override
			{
				//Check for valid arguments.
				if (_dir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Check that a directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Copy the current directory to the out parameter.
				strcpy_s(_dir, _dirSize, INTERNAL::G_TO_UTF8(currDir).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct dirent* file;
				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				//Reset the dir size.
				_outSize = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				file = readdir(currDirStream);
				if (file == nullptr)
					return GReturn::FAILURE;
				file = readdir(currDirStream);
				if (file == nullptr)
					return GReturn::FAILURE;

				//Get the number of files in directory.
				while ((file = readdir(currDirStream)))
				{
					if (file->d_type == DT_REG)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetSubDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct dirent* subDir;
				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				//Reset the sub-dir size.
				_outSize = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				subDir = readdir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;
				subDir = readdir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;

				while ((subDir = readdir(currDirStream)))
				{
					if (subDir->d_type == DT_DIR)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetFilesFromDirectory(char* _outFiles[], unsigned int _numFiles, unsigned int _fileNameSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				struct dirent* file;
				unsigned int fileIndex = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				file = readdir(currDirStream);
				if (file == nullptr)
					return GReturn::FAILURE;
				file = readdir(currDirStream);
				if (file == nullptr)
					return GReturn::FAILURE;

				while ((file = readdir(currDirStream)) && fileIndex < _numFiles)
				{
					if (file->d_type == DT_REG)
					{
						std::string fileName(file->d_name);
						strcpy_s(_outFiles[fileIndex], _fileNameSize, INTERNAL::G_TO_UTF8(fileName).c_str());
						++fileIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize, char* _outsubDir[])
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				struct dirent* subDir;
				unsigned int subDirIndex = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				subDir = readdir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;
				subDir = readdir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;

				while ((subDir = readdir(currDirStream)) && subDirIndex < _numsubDir)
				{
					if (subDir->d_type == DT_DIR)
					{
						std::string subDirName(subDir->d_name);
						strcpy_s(_outsubDir[subDirIndex], _subDirNameSize, INTERNAL::G_TO_UTF8(subDirName).c_str());
						++subDirIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);
				return GReturn::SUCCESS;
			}

			GReturn GetFileSize(const char* const _file, unsigned int& _outSize) override
			{
				//Make a full path to the file.
				std::string filePath = currDir;
				filePath += INTERNAL::G_TO_UTF16(_file);

				//Other than the UTF8 to UTF16 conversion for the windows calls,
				//this is handled the same for each platform.
				//We call stat() and it fills in the passed in function
				//with the stats of the passed in path.
				struct _stat64i32 s;
				if (_wstat64i32(filePath.c_str(), &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Copy the file size to the out parameter.
				_outSize = (unsigned int)s.st_size;

				return GReturn::SUCCESS;
			}

			GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) override
			{
				// if text file
				if (file.is_open()) {
					// if not seeking from the current position
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							std::streamoff offset = static_cast<unsigned long long>(_seekFrom) + static_cast<long long>(_amount);
							file.seekg(offset, std::ios_base::beg);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					else { // seeking from the current position
						// check for invalid arguments
						// lock
						lock.lock();
						unsigned int pos = static_cast<unsigned int>(file.tellg());
						if (0 <= pos + _amount && pos + _amount < fileSize) {
							// seek to the new position relative to the current position
							file.seekg(_amount, std::ios_base::cur);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}

				}
				// if binary file
				else if (binaryFile.is_open()) {
					// check where we seek from
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							std::streamoff offset = static_cast<unsigned long long>(_seekFrom) + static_cast<long long>(_amount);
							binaryFile.seekg(offset, std::ios_base::beg);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(binaryFile.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					else { // seek from the current position
						// check for invalid arguments
						// lock
						lock.lock();

						unsigned int pos = static_cast<unsigned int>(binaryFile.tellg());
						if (0 <= pos + _amount && pos + _amount < fileSize) {
							// seek to the new position relative to the current position
							binaryFile.seekg(_amount, std::ios_base::cur);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(binaryFile.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
				}
				else return GReturn::FILE_NOT_FOUND;
			}

			GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Copy the directory to the out parameter.
				strcpy_s(_outDir, _dirSize, winrt::to_string(winrt::Windows::ApplicationModel::Package::Current().InstalledLocation().Path()).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Copy the directory to the out parameter.
				strcpy_s(_outDir, _dirSize, winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalFolder().Path()).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Copy the directory to the out parameter.
				strcpy_s(_outDir, _dirSize, winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().RoamingFolder().Path()).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Copy the directory to the out parameter.
				strcpy_s(_outDir, _dirSize, winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().TemporaryFolder().Path()).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Copy the directory to the out parameter.
				strcpy_s(_outDir, _dirSize, winrt::to_string(winrt::Windows::Storage::ApplicationData::Current().LocalCacheFolder().Path()).c_str());
				return GReturn::SUCCESS;
			}

			GReturn LockAsyncRead() const override
			{
				return GThreadSharedImplementation::LockAsyncRead();
			}

			GReturn UnlockAsyncRead() const override
			{
				return GThreadSharedImplementation::UnlockAsyncRead();
			}

			GReturn LockSyncWrite() override
			{
				return GThreadSharedImplementation::LockSyncWrite();
			}

			GReturn UnlockSyncWrite() override
			{
				return GThreadSharedImplementation::UnlockSyncWrite();
			}
		};
	} // end namespace I
} // end namespace GW

#undef DIR_SEPARATOR
#undef DIR
#undef dirent
#undef string

#undef opendir
#undef readdir
#undef closedir
#undef rewinddir
