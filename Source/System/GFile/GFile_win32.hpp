#include <fstream>  //file streams
#include <ios>
#include <string>  //strings
#include <atomic>  //atomic variables
#include <mutex>  //mutex locks
#include <stdio.h>
#include "../../Shared/winutils.h" //for UTF conversions and other UTF related tools

#include <io.h>  //Included for mode change.
#include <fcntl.h>  //Included for mode change.
#include "../../../Interface/Core/GThreadShared.h"

#ifndef DIR_SEPARATOR
#define G_DIR_SEPARATOR L'\\'
#endif

namespace GW
{
	namespace I
	{
		class GFileImplementation : public virtual GW::I::GFileInterface,
			protected GThreadSharedImplementation
		{
		private:
			_WDIR* currDirStream;  //Maintains the current directory.
			std::wfstream file;  //Maintains the current file (if one is open).
			std::fstream binaryFile; //for binary read and write
			char initialDir[250] = "";
			std::wstring IOString; //a string to read and write into.

			unsigned int fileSize = 0;

			std::wstring currDir;  //A cached directory path for faster fetching.

			std::atomic<unsigned int> mode; //Used to track what open mode the file is in
			std::mutex lock; //Read/Write lock.

		public:
			GFileImplementation()
			{
				currDirStream = nullptr;
			}
			~GFileImplementation()
			{
				SetCurrentWorkingDirectory(initialDir);
				//Close the current directory.
				_wclosedir(currDirStream);
				//Close the file stream.
				CloseFile();
			}

			GReturn Create()
			{
				//Set the current working directory to the directory the program was ran from.
				GReturn rv = SetCurrentWorkingDirectory("./");
				if (G_FAIL(rv))
					return rv;

				GetCurrentWorkingDirectory(initialDir, 250);

                // Create a UTF8 Locale to imbue the fstream with.
                std::locale utf8Locale("en_US.UTF8");

                // Imbue the fstream.
                file.imbue(utf8Locale);
                binaryFile.imbue(utf8Locale);

                return GReturn::SUCCESS;
			}

			GReturn OpenBinaryRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                // Open the new file
                binaryFile.open(currDir + internal_gw::UTFStringConverter(_file), std::ios::in | std::ios::binary);

                // If file failed to open
                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                // Open the new file
                binaryFile.open(currDir + internal_gw::UTFStringConverter(_file), std::ios::out | std::ios::binary);

                // If file failed to open
                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

                //Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

                if (binaryFile.is_open()) return GReturn::FAILURE;

                // Open the new file
                binaryFile.open(currDir + internal_gw::UTFStringConverter(_file),
                          std::ios::out | std::ios::app | std::ios::ate | std::ios::binary);

                // If file failed to open
                if (!binaryFile.is_open()) return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + internal_gw::UTFStringConverter(_file), std::ios::in);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + internal_gw::UTFStringConverter(_file), std::ios::out);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + internal_gw::UTFStringConverter(_file), std::ios::out | std::ios::app | std::ios::ate);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn Write(const char* const _inData, unsigned int _numBytes) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr || _numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && !binaryFile.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Lock the write operations.
				lock.lock();

				//On windows we need to cast the char* to a wchar_t*.
				if (binaryFile.is_open())
				{
					binaryFile.write(_inData, _numBytes);
				}
				else
					file.write((wchar_t*)_inData, _numBytes);

				// update the file size
				fileSize += _numBytes;

				lock.unlock();

				return GReturn::SUCCESS;
			}

			GReturn Read(char* _outData, unsigned int _numBytes) override
			{
				if (_numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && !binaryFile.is_open())
				{
					_outData = nullptr;
					return GReturn::FAILURE;
				}

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				//Lock the read operations.
				lock.lock();

				if (binaryFile.is_open())
				{
                    binaryFile.read(_outData, _numBytes);
                    lock.unlock();
                    if (binaryFile.eof()) return GReturn::END_OF_FILE;
					return GReturn::SUCCESS;
                }
				else
				{
					// this is only a problem on win32
					if ((_numBytes % 2) == 1) { // check for odd number of bytes to read because utf-16
						char tmp[2];
						//On Windows we need to cast the _outData char* to a wchar_t*.
						file.read((wchar_t*)_outData, _numBytes - 1); // read 1 less bytes into the buffer directly
						file.read((wchar_t*)tmp, 1); // read the last byte into the tmp
						_outData[_numBytes - 1] = tmp[0]; // assign the last byte
					} else {
						file.read((wchar_t*)_outData, _numBytes);
					}
				}

				lock.unlock();
				if (file.eof())
					return GReturn::END_OF_FILE;

				return GReturn::SUCCESS;
			}

			GReturn WriteLine(const char* const _inData) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Transfer the data to a string. #defines make it so the
				//string is what we need it to be on any system we support.
				IOString = internal_gw::UTFStringConverter(_inData);

				//Lock the write operations.
				lock.lock();

				//Write out the string.
				file << IOString;

				// update the file size
				fileSize += static_cast<unsigned int>(IOString.length());
				IOString.clear();
				lock.unlock();
				
				return GReturn::SUCCESS;
			}

			GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) override
			{
				if (_outData == nullptr || _outDataSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				

				//Lock the read operations.
				lock.lock();

				//Convert the UTF8 delimiter to UTF16.
				IOString = internal_gw::UTFStringConverter(&_delimiter);
				const wchar_t delimiter = IOString[0];
				IOString.erase(IOString.begin()); //remove the delimiter

				//Read the information.
				std::getline(file, IOString, delimiter);

				if (file.eof())
				{
					file.clear(); //reset the flag

					if ((_outDataSize - 1) > fileSize)
					{
						strncpy_s(_outData, fileSize, internal_gw::UTFStringConverter(IOString).c_str(), _TRUNCATE);
						lock.unlock();
						return GReturn::END_OF_FILE;
					}
				}

					//Copy the data over to the out parameter.
					strncpy_s(_outData, _outDataSize, internal_gw::UTFStringConverter(IOString).c_str(), _TRUNCATE);

					//Copy the data over to the out parameter.
					strcpy_s(_outData, _outDataSize, internal_gw::UTFStringConverter(IOString).c_str());

					file.clear(); //reset the eof bit
					IOString.clear();

					lock.unlock();
			
				return GReturn::SUCCESS;
			}

			GReturn CloseFile() override
			{
				//If a file is not open, we can not close it.
				if (!file.is_open() && !binaryFile.is_open())
					return GReturn::FAILURE;

				if (binaryFile.is_open())
				{
                    binaryFile.flush();
                    binaryFile.close();
                }
				else
				{
					//Flush the file.
					file.flush();
					//Close the file.
					file.close();
				}

				// update the file size
				fileSize = 0;

				return GReturn::SUCCESS;
			}

			GReturn FlushFile() override
			{
				//If a file is not open we can not flush it.
				if (!file.is_open() && !binaryFile.is_open())
					return GReturn::FAILURE;

                if (binaryFile.is_open())
                    binaryFile.flush();
                else
                    file.flush();

                return GReturn::SUCCESS;
			}

			GReturn SetCurrentWorkingDirectory(const char* const _dir) override
			{
				//Check for valid arguments.
				if (_dir == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Get the absolute path.
				wchar_t buffer[MAX_PATH];
				if (_wfullpath(buffer, internal_gw::UTFStringConverter(_dir).c_str(), MAX_PATH) == nullptr)
					return GReturn::FAILURE;

				//Check to make sure the directory exists.
				struct _stat s;
				if (_wstat(buffer, &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Assign the passed in directory to our internal directory storage.
				currDir = buffer;
				currDir += G_DIR_SEPARATOR;

				//If there is an open directory, close it.
				if (currDirStream != nullptr)
					_wclosedir(currDirStream);

				//Open new directory.
				currDirStream = _wopendir(currDir.c_str());

				//Check to ensure directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}

			GReturn GetCurrentWorkingDirectory(char* _dir, unsigned int _dirSize) override
			{
				//Check for valid arguments.
				if (_dir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Check that a directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Copy the current directory to the out parameter.
				strcpy_s(_dir, _dirSize, internal_gw::UTFStringConverter(currDir).c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct _wdirent* currFile;
				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				//Reset the dir size.
				_outSize = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				currFile = _wreaddir(currDirStream);
				if (currFile == nullptr)
					return GReturn::FAILURE;
				currFile = _wreaddir(currDirStream);
				if (currFile == nullptr)
					return GReturn::FAILURE;

				//Get the number of files in directory.
				while ((currFile = _wreaddir(currDirStream)))
				{
					if (currFile->d_type == DT_REG)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetSubDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct _wdirent* subDir;
				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				//Reset the sub-dir size.
				_outSize = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				subDir = _wreaddir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;
				subDir = _wreaddir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;

				while ((subDir = _wreaddir(currDirStream)))
				{
					if (subDir->d_type == DT_DIR)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);
				
				return GReturn::SUCCESS;
			}

			GReturn GetFilesFromDirectory(char* _outFiles[], unsigned int _numFiles, unsigned int _fileNameSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				struct _wdirent* currFile;
				unsigned int fileIndex = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				currFile = _wreaddir(currDirStream);
				if (currFile == nullptr)
					return GReturn::FAILURE;
				currFile = _wreaddir(currDirStream);
				if (currFile == nullptr)
					return GReturn::FAILURE;

				while ((currFile = _wreaddir(currDirStream)) && fileIndex < _numFiles)
				{
					if (currFile->d_type == DT_REG)
					{
						std::wstring fileName(currFile->d_name);
						strcpy_s(_outFiles[fileIndex], _fileNameSize, internal_gw::UTFStringConverter(fileName).c_str());
						++fileIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize, char* _outsubDir[])
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);

				struct _wdirent* subDir;
				unsigned int subDirIndex = 0;

				// In Windows platform when rewinddir is called, the first 2 dir will be . and ..
				// We can simply skip them
				subDir = _wreaddir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;
				subDir = _wreaddir(currDirStream);
				if (subDir == nullptr)
					return GReturn::FAILURE;

				while ((subDir = _wreaddir(currDirStream)) && subDirIndex < _numsubDir)
				{
					if (subDir->d_type == DT_DIR)
					{
						std::wstring subDirName(subDir->d_name);
						strcpy_s(_outsubDir[subDirIndex], _subDirNameSize, internal_gw::UTFStringConverter(subDirName).c_str());
						++subDirIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				_wrewinddir(currDirStream);
				return GReturn::SUCCESS;
			}

			GReturn GetFileSize(const char* const _file, unsigned int& _outSize) override
			{
				//Make a full path to the file.
				std::wstring filePath = currDir;
				filePath += internal_gw::UTFStringConverter(_file);

				//Other than the UTF8 to UTF16 conversion for the windows calls,
				//this is handled the same for each platform.
				//We call stat() and it fills in the passed in function
				//with the stats of the passed in path.
				struct _stat64i32 s;
				if (_wstat64i32(filePath.c_str(), &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Copy the file size to the out parameter.
				_outSize = s.st_size;

				return GReturn::SUCCESS;
			}

			GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) override
			{
				// if text file
				if (file.is_open()) {
					// if not seeking from the current position
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							std::streamoff offset = static_cast<unsigned long long>(_seekFrom) + static_cast<long long>(_amount);
							file.seekg(offset, std::ios_base::beg);
							
							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						} 
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					else { // seeking from the current position
						// check for invalid arguments
						// lock
						lock.lock();
						unsigned int pos = static_cast<unsigned int>(file.tellg());
						if (0 <= pos + _amount && pos + _amount < fileSize) {
							// seek to the new position relative to the current position
							file.seekg(_amount, std::ios_base::cur);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						} 
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					
				} 
				// if binary file
				else if (binaryFile.is_open()) {
					// check where we seek from
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							std::streamoff offset = static_cast<unsigned long long>(_seekFrom) + static_cast<long long>(_amount);
							binaryFile.seekg(offset, std::ios_base::beg);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(binaryFile.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						} 
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					} 
					else { // seek from the current position
						// check for invalid arguments
						// lock
						lock.lock();

                        unsigned int pos = static_cast<unsigned int>(binaryFile.tellg());
                        if (0 <= pos + _amount && pos + _amount < fileSize) {
							// seek to the new position relative to the current position
							binaryFile.seekg(_amount, std::ios_base::cur);

							// set the output to the new current position
                            _outCurrPos = static_cast<unsigned int>(binaryFile.tellg());

                            // unlock
							lock.unlock();

							return GReturn::SUCCESS;
						} 
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
				} 
				else return GReturn::FILE_NOT_FOUND;
			}

			GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//char* retVal = "./";

				SetCurrentWorkingDirectory("./");


				/*//Copy the directory to the out parameter
				INTERNAL::strcpy_s(_dir, _dirSize, retVal);*/

				return GetCurrentWorkingDirectory(_outDir, _dirSize);
			}

			GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./save";

				//Copy the directory to the out parameter
				strcpy_s(_outDir, _dirSize, retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./preferences";

				//Copy the directory to the out parameter
				strcpy_s(_outDir, _dirSize, retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./temp";

				//Copy the directory to the out parameter
				strcpy_s(_outDir, _dirSize, retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) override
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./cache";

				//Copy the directory to the out parameter
				strcpy_s(_outDir, _dirSize, retVal);

				return GReturn::SUCCESS;
			}

			GReturn LockAsyncRead() const override
			{
				return GThreadSharedImplementation::LockAsyncRead();
			}

			GReturn UnlockAsyncRead() const override
			{
				return GThreadSharedImplementation::UnlockAsyncRead();
			}

			GReturn LockSyncWrite() override
			{
				return GThreadSharedImplementation::LockSyncWrite();
			}

			GReturn UnlockSyncWrite() override
			{
				return GThreadSharedImplementation::UnlockSyncWrite();
			}
		};
	} // end namespace I
} // end namespace GW

#undef DIR_SEPARATOR
