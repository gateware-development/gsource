namespace GW
{
	namespace I
	{
		class GMusic3DImplementation : public virtual GMusic3DInterface,
			protected GMusicImplementation
		{
		private:
			GW::AUDIO::GAudio3D gAudio3D;
			GW::CORE::GEventReceiver gReceiver;

			MATH::GVECTORF position = MATH::GIdentityVectorF;
			MATH::GMATRIXF listener = MATH::GIdentityMatrixF;
			MATH::GMATRIXF transform = MATH::GIdentityMatrixF;
			GW::AUDIO::GATTENUATION attenuation = GW::AUDIO::GATTENUATION::LINEAR;
			float minRadius = 0.0f;
			float maxRadius = 50.0f;
			float volume = 1.0f;

			static GReturn CalculateRatio(const float theta, const float spread, const float start, const float end, float& outRatio)
			{
				if (start >= end)
					return GReturn::FAILURE;

				if (spread > 180.0f)
					return GReturn::FAILURE;

				const float startBound = theta - spread;
				const float endBound = theta + spread;
				const float deltaSection = end - start;

				const float RrR = (endBound - start) / deltaSection;
				const float RlR = (startBound - start) / deltaSection;

				const float clampR = G_CLAMP(RrR, 0.0f, 1.0f);
				const float clampL = G_CLAMP(RlR, 0.0f, 1.0f);

				outRatio = clampR - clampL;

				return GReturn::SUCCESS;
			}

			GReturn GetSpatializedChannelVolumes(GW::MATH::GVECTORF _position, float _volume,
			                                     const float _minRadius, const float _maxRadius,
			                                     const GW::AUDIO::GATTENUATION _attenuation, const unsigned int _channelNum,
			                                     float* _outChannelVolumes)
			{
				if (static_cast<int>(_attenuation) > G_IMPLEMENTED_ATTENUATIONS || static_cast<int>(_attenuation) < 0)
					return GReturn::INVALID_ARGUMENT;

				GW::MATH::GVECTORF localPosition{};
				GReturn result = GW::MATH::GMatrix::VectorXMatrixF(transform, _position, localPosition);

				if (result != GReturn::SUCCESS)
					return result;

				// D = |local|                              -float distance [0, MAX_DISTANCE]
				float distance;
				GW::MATH::GVector::MagnitudeF(localPosition, distance);

				GW::MATH::GVECTORF pFlat = { {{ localPosition.x, 0, localPosition.z, localPosition.w }} };

				float dFlat;
				GW::MATH::GVector::MagnitudeF(pFlat, dFlat);

				localPosition = pFlat;
				GW::MATH::GVector::NormalizeF(localPosition, localPosition);

				// Here is where the different attenuation shapes will be implemented.
				switch (_attenuation)
				{
				case GW::AUDIO::GATTENUATION::LINEAR:
					// V = volume = 1 - D / Dmax                -float volume [0, 1]

					// maps x [0, 1] => 1 - x [0, 1]
					_volume *= (1 - G_CLAMP(G_CLAMP(distance - _minRadius, 0, 1) / _maxRadius, 0, 1));
					break;
					/*case LOGARITHMIC:
						// TODO: map volume x [0, 1] => Log(x) [0, 1]
					break;*/
					/*case LOGREVERSE:
						// TODO: map volume x [0, 1] => Log-1(x) [0, 1]
					break;*/
					/*case INVERSE:
						// TODO: map volume x [0, 1] => 1/x [0, 1]
					break;*/
					/*case NATURAL:
						// TODO: map volume x [0, 1] => 10^((x-dB)/20) [0, 1]
					break;*/
				}

				// spread = (1 - DFlat / DFlatMax) * 180    -float spread [0, 180]
				const float spread = (1 - G_CLAMP(dFlat / _maxRadius, 0, 1)) * 180;

				GW::MATH::GVECTORF cross{};
				GW::MATH::GVECTORF forward = {{{0.0f, 0.0f, 1.0f, 0.0f}}};
				result = GW::MATH::GVector::CrossVector3F(forward, localPosition, cross);

				if (result != GReturn::SUCCESS)
					return result;

				float theta;
				result = GW::MATH::GVector::DotF(forward, localPosition, theta);

				if (result != GReturn::SUCCESS)
					return result;

				theta = (cross.y > 0) ? acosf(theta) : -1 * acosf(theta);
				theta = G_RADIAN_TO_DEGREE_F(theta);

				float ratio = 0;
				//float channelVolumes[6] = { 0, 0, 0, 0, 0, 0 };
				for (unsigned int i = 0; i < _channelNum; i++)
				{
					switch (i + 1)
					{
					case 1: // FRONT_LEFT
						result = CalculateRatio(theta, spread, -114, -38, ratio);

						if (result != GReturn::SUCCESS)
							return result;

						if (ratio == 0.0f)
						{
							result = CalculateRatio(theta, spread, 246, 322, ratio);

							if (result != GReturn::SUCCESS)
								return result;
						}
						break;
					case 2: // FRONT_RIGHT
						result = CalculateRatio(theta, spread, 38, 114, ratio);

						if (result != GReturn::SUCCESS)
							return result;

						if (ratio == 0.0f)
						{
							result = CalculateRatio(theta, spread, -322, -246, ratio);

							if (result != GReturn::SUCCESS)
								return result;
						}
						break;
					case 3: // FRONT_CENTER
						result = CalculateRatio(theta, spread, -38, 38, ratio);

						if (result != GReturn::SUCCESS)
							return result;

						if (ratio == 0.0f)
						{
							result = CalculateRatio(theta, spread, 322, 398, ratio);

							if (result != GReturn::SUCCESS)
								return result;
						}
						break;
					case 5: // SURROUND_LEFT
						result = CalculateRatio(theta, spread, 180, 246, ratio);

						if (result != GReturn::SUCCESS)
							return result;

						if (ratio == 0.0f)
						{
							result = CalculateRatio(theta, spread, -180, -114, ratio);

							if (result != GReturn::SUCCESS)
								return result;
						}
						break;
					case 6: // SURROUND_RIGHT
						result = CalculateRatio(theta, spread, 114, 180, ratio);

						if (result != GReturn::SUCCESS)
							return result;

						if (ratio == 0.0f)
						{
							result = CalculateRatio(theta, spread, -246, -180, ratio);

							if (result != GReturn::SUCCESS)
								return result;
						}
						break;
					default:
						break;
					}

					_outChannelVolumes[i] = ratio * _volume;
					_outChannelVolumes[4] = _outChannelVolumes[i] > _outChannelVolumes[4] ? _outChannelVolumes[i] : _outChannelVolumes[4];
				}

				return result;
			}

			GReturn Spatialize()
			{
				if (!gAudio3D)
					return GReturn::PREMATURE_DEALLOCATION;

				float volumes[6] = { 0, 0, 0, 0, 0, 0 };
				const GReturn result = GetSpatializedChannelVolumes(position, volume, minRadius, maxRadius, attenuation, 6, volumes);

				if (result != GReturn::SUCCESS)
					return result;

				return SetChannelVolumes(volumes, 6);
			}
		public:
			GReturn Create(const char* _path, const float _minRadius, const float _maxRadius,
			               const GW::AUDIO::GATTENUATION _attenuation, GW::AUDIO::GAudio3D _audio3D,
			               const float _volume = 1.0f)
			{
				if (!_path || !_audio3D)
					return GReturn::INVALID_ARGUMENT;

				if (_volume < 0.0f || _volume > 1.0f)
					return GReturn::INVALID_ARGUMENT;

				attenuation = _attenuation;
				minRadius = _minRadius;
				maxRadius = _maxRadius;
				gAudio3D = _audio3D;
				volume = _volume;

				const GReturn result = GMusicImplementation::Create(_path, _audio3D, _volume);
				if (result != GReturn::SUCCESS)
					return result;

				const auto audio3DImplementation = std::dynamic_pointer_cast<GW::I::GAudio3DImplementation>(*_audio3D);
				return gReceiver.Create(audio3DImplementation->gEventGen, [&]()
				{
					GW::GEvent event;
					if (+gReceiver.Pop(event))
					{
						GW::AUDIO::GAudio3D::Events audio3devent;
						if (+event.Read(audio3devent))
						{
							switch (audio3devent)
							{
								case GW::AUDIO::GAudio3D::Events::UPDATE_LISTENER:
								{
									GW::AUDIO::GAudio3D::EVENT_DATA eventData;
									event.Read(eventData);

									GW::MATH::GMATRIXF eventMatrix{};
									GW::MATH::GMatrix::ConvertQuaternionF(eventData.quaternion, eventMatrix);
									eventMatrix.row4 = eventData.position;
									listener = eventMatrix;

									break;
								}
								case GW::AUDIO::GAudio3D::Events::UPDATE_TRANSFORM_AND_SPATIALIZE:
								{
									GW::AUDIO::GAudio3D::EVENT_DATA eventData;
									event.Read(eventData);

									GW::MATH::GMATRIXF eventMatrix{};
									GW::MATH::GMatrix::ConvertQuaternionF(eventData.quaternion, eventMatrix);
									eventMatrix.row4 = eventData.position;
									transform = eventMatrix;

									Spatialize();
									break;
								}
								default:
									break;
							}
						}
					}
				});
			}

			GReturn UpdatePosition(const GW::MATH::GVECTORF _position) override
			{
				if (!gAudio3D)
					return GReturn::PREMATURE_DEALLOCATION;

				position = _position;
				return Spatialize();
			}

			GReturn UpdateAttenuation(const float _minRadius, const float _maxRadius, const GW::AUDIO::GATTENUATION _attenuation) override
			{
				if (!gAudio3D)
					return GReturn::PREMATURE_DEALLOCATION;

				if (static_cast<int>(_attenuation) > G_IMPLEMENTED_ATTENUATIONS || static_cast<int>(_attenuation) < 0)
					return GReturn::INVALID_ARGUMENT;

				if (_minRadius > _maxRadius)
					return GReturn::INVALID_ARGUMENT;

				minRadius = _minRadius;
				maxRadius = _maxRadius;
				attenuation = _attenuation;

				return Spatialize();
			}

			GReturn SetVolume(const float _newVolume) override
			{
				if (_newVolume < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				volume = (_newVolume > 1.0f) ? 1.0f : _newVolume;
				return GMusicImplementation::SetVolume(volume);
			}

			GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override { return GMusicImplementation::SetChannelVolumes(_values, _numChannels); }
			GReturn Play(bool _loop = false) override { return GMusicImplementation::Play(_loop); }
			GReturn Pause() override { return GMusicImplementation::Pause(); }
			GReturn Resume() override { return GMusicImplementation::Resume(); }
			GReturn Stop() override { return GMusicImplementation::Stop(); }
			GReturn GetSourceChannels(unsigned int& _returnedChannelNum) const override { return GMusicImplementation::GetSourceChannels(_returnedChannelNum); }
			GReturn GetOutputChannels(unsigned int& _returnedChannelNum) const override { return GMusicImplementation::GetOutputChannels(_returnedChannelNum); }
			GReturn isPlaying(bool& _returnedBool) const override { return GMusicImplementation::isPlaying(_returnedBool); }
		};
	}
}
