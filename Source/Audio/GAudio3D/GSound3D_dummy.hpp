namespace GW
{
	namespace I
	{
		class GSound3DImplementation : public virtual GSound3DInterface
		{
		public:
			GReturn Create(const char* _path, float _minRadius, float _maxRadius, GW::AUDIO::GATTENUATION _attenuation, GW::AUDIO::GAudio3D _audio3D) { return GReturn::INTERFACE_UNSUPPORTED; }
			GReturn UpdatePosition(GW::MATH::GVECTORF _position) override { return GReturn::FAILURE; }
			GReturn UpdateAttenuation(float _minRadius, float _maxRadius, GW::AUDIO::GATTENUATION _attenuation) override { return GReturn::FAILURE; }
			GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override { return GReturn::FAILURE; }
			GReturn SetVolume(float _newVolume) override { return GReturn::FAILURE; }
			GReturn Play() override { return GReturn::FAILURE; }
			GReturn Pause() override { return GReturn::FAILURE; }
			GReturn Resume() override { return GReturn::FAILURE; }
			GReturn Stop() override { return GReturn::FAILURE; }
			GReturn GetSourceChannels(unsigned int& _returnedChannelNum) const override { return GReturn::FAILURE; }
			GReturn GetOutputChannels(unsigned int& _returnedChannelNum) const override { return GReturn::FAILURE; }
			GReturn isPlaying(bool& _returnedBool) const override { return GReturn::FAILURE; }
		};
	}
}