namespace GW
{
	namespace I
	{
		class GAudio3DImplementation :	public virtual GAudio3DInterface,
										public GAudioImplementation
		{
		public:
			GReturn Create() { return GReturn::INTERFACE_UNSUPPORTED; }
			GReturn Update3DListener(GW::MATH::GVECTORF _position, GW::MATH::GQUATERNIONF _orientation) override { return GReturn::FAILURE; }
		};
	}
}