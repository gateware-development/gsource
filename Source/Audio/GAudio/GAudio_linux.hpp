#include <atomic>
#include <cmath>
#include <cfloat>
#include <cstring>
#include <memory>
#include <pulse/thread-mainloop.h>
#include <pulse/context.h>

namespace internal_gw
{
    class AudioState
    {
        static void close_context(pa_context *context)
        {
            if (context == nullptr) return;
            pa_context_disconnect(context);
            pa_context_unref(context);
        }

        static void close_main_loop(pa_threaded_mainloop *main_loop)
        {
            if (main_loop == nullptr) return;
            pa_threaded_mainloop_free(main_loop);
        }

      public:
        std::unique_ptr<pa_threaded_mainloop, decltype(&AudioState::close_main_loop)>
            main_loop{nullptr, &AudioState::close_main_loop};
        std::unique_ptr<pa_context, decltype(&AudioState::close_context)> context{nullptr, &AudioState::close_context};
        std::atomic_int                                                   pa_ready{};

        AudioState() = default;
        ~AudioState()
        {
            context   = nullptr;
            main_loop = nullptr;
        }
    };

    static void OnStateChange(const pa_context *c, void *userdata)
    {
        std::atomic<int> *pa_ready = static_cast<std::atomic<int> *>(userdata);

        switch (pa_context_get_state(c)) {
        case PA_CONTEXT_FAILED:
        case PA_CONTEXT_TERMINATED: {
            *pa_ready = -1;
            break;
        }
        case PA_CONTEXT_READY: {
            *pa_ready = 1;
            break;
        }
        default:
            break;
        }
    }

    inline bool get_audio_state(std::shared_ptr<AudioState> &ptr)
    {
        // Contain a weak_ptr of the audio state
        static std::weak_ptr<AudioState> state;

        // We check for nullptr here to see if the state has already been created
        if (state.expired()) {
            // If the state is null, we create a new state and configure it
            std::shared_ptr<AudioState> new_state(new AudioState());
            pa_threaded_mainloop       *main_loop = pa_threaded_mainloop_new();
            if (main_loop == nullptr) return false;

            pa_context *context = pa_context_new(pa_threaded_mainloop_get_api(main_loop), "main_audio");
            if (context == nullptr) {
                pa_threaded_mainloop_free(main_loop);
                return false;
            }

            pa_context_set_state_callback(context,
                                          reinterpret_cast<pa_context_notify_cb_t>(OnStateChange),
                                          &new_state->pa_ready);

            if (pa_context_connect(context, nullptr, PA_CONTEXT_NOFLAGS, nullptr) < 0) {
                pa_context_unref(context);
                pa_threaded_mainloop_free(main_loop);
                return false;
            }

            pa_threaded_mainloop_start(main_loop);

            new_state->main_loop.reset(main_loop);
            new_state->context.reset(context);

            // After configuring, we set the weak_ptr to point to this new state then return the state by copy
            state = new_state;
            ptr   = new_state;
            return true;
        }

        // If the state isn't null, we just return the locked shared state we have
        ptr = state.lock();
        return true;
    }
} // namespace internal_gw

namespace GW
{
	namespace I
	{
		class GAudioImplementation : public virtual GAudioInterface,
			public GEventGeneratorImplementation
		{
		public:
			float masterVolume = 1.0f;
			float soundsVolume = 1.0f;
			float musicVolume = 1.0f;
			float soundsChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
			float musicChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };

			~GAudioImplementation() override
			{
				// Broadcast DESTROY here
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::DESTROY, std::nullptr_t());
				this->GEventGeneratorImplementation::Push(gEvent);
			}

			// Main class
			GReturn Create()
			{
				// check for available hardware
				pa_threaded_mainloop* myMainLoop = pa_threaded_mainloop_new();
				if (myMainLoop == nullptr)
					return GReturn::HARDWARE_UNAVAILABLE;

				pa_context* myContext = pa_context_new(pa_threaded_mainloop_get_api(myMainLoop), "AudioHardware");
				if (myContext == nullptr)
				{
					pa_threaded_mainloop_unlock(myMainLoop);
					pa_threaded_mainloop_free(myMainLoop);
					return GReturn::HARDWARE_UNAVAILABLE;
				}

				if (pa_context_connect(myContext, nullptr, PA_CONTEXT_NOFLAGS, nullptr) < 0)
				{
					pa_context_unref(myContext);
					pa_threaded_mainloop_unlock(myMainLoop);
					pa_threaded_mainloop_free(myMainLoop);
					return GReturn::HARDWARE_UNAVAILABLE;
				}

				pa_context_disconnect(myContext);
				pa_context_unref(myContext);
				pa_threaded_mainloop_free(myMainLoop);

				// Initialize GAudio's volumes
				masterVolume = 1.0f;
				soundsVolume = 1.0f;
				musicVolume = 1.0f;

				return GEventGeneratorImplementation::Create();
			}

			GReturn SetMasterVolume(const float _value) override
			{
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - masterVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				masterVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData{};
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = masterVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::MASTER_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
			}

			GReturn SetGlobalSoundVolume(const float _value) override
			{
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - soundsVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				soundsVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData{};
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = soundsVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::SOUNDS_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
			}

			GReturn SetGlobalMusicVolume(const float _value) override
			{
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - musicVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				musicVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData{};
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = musicVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::MUSIC_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
			}

			GReturn SetSoundsChannelVolumes(const float* _values, const unsigned int _numChannels) override
			{
				if (_values == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (_numChannels == 0 || _numChannels > 6)
					return GReturn::INVALID_ARGUMENT;

				for (unsigned int i = 0; i < 6; ++i)
				{
					soundsChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
				}

				GEvent gEvent;
				EVENT_DATA eventData{};
				memcpy(eventData.channelVolumes, soundsChannelVolumes, 6 * sizeof(float));
				eventData.numOfChannels = _numChannels;
				gEvent.Write(Events::SOUND_CHANNEL_VOLUMES_CHANGED, eventData);

				return this->Push(gEvent);
			}

			GReturn SetMusicChannelVolumes(const float* _values, const unsigned int _numChannels) override
			{
				if (_values == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (_numChannels == 0 || _numChannels > 6)
					return GReturn::INVALID_ARGUMENT;

				for (unsigned int i = 0; i < 6; ++i)
				{
					musicChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
				}

				GEvent gEvent;
				EVENT_DATA eventData{};
				memcpy(eventData.channelVolumes, musicChannelVolumes, 6 * sizeof(float));
				eventData.numOfChannels = _numChannels;
				gEvent.Write(Events::MUSIC_CHANNEL_VOLUMES_CHANGED, eventData);

				return this->Push(gEvent);
			}

			// Sound Events
			GReturn PlaySounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PLAY_SOUNDS, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn PauseSounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PAUSE_SOUNDS, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn ResumeSounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::RESUME_SOUNDS, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn StopSounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::STOP_SOUNDS, std::nullptr_t());

				return this->Push(gEvent);
			}

			// Music Events
			GReturn PlayMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PLAY_MUSIC, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn PauseMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PAUSE_MUSIC, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn ResumeMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::RESUME_MUSIC, std::nullptr_t());

				return this->Push(gEvent);
			}
			GReturn StopMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::STOP_MUSIC, std::nullptr_t());

				return this->Push(gEvent);
			}
		};
	}// end I
}// end GW
