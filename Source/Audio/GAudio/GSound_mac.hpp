#define G_NUM_OF_OUTPUTS 6

namespace GW
{
    namespace I
    {
        class GSoundImplementation;
    }
}

namespace internal_gw
{
    class GMacSound
    {
    public:
        GW::I::GSoundImplementation* gSound;
        GMacAudio* audio;

        AVAudioPlayerNode* player;
        AVAudioUnit* matrixMixerNode;

        AVAudioFile* file;
        AVAudioPCMBuffer* buffer;

        std::atomic<bool> isPlaying;
        std::atomic<bool> isPaused;

        GMacSound* initWithPath(NSString* _path)
        {
            NSError* testError;
            // setting up player
            player = [[AVAudioPlayerNode alloc]init];
            [audio->engine attachNode : player] ;

            // setting up buffer
            NSURL* filePath = [[NSURL alloc]initFileURLWithPath:_path];
            file = [[AVAudioFile alloc]initForReading:filePath commonFormat : AVAudioPCMFormatFloat32 interleaved : false error : &testError];
            [filePath release] ;
            buffer = [[AVAudioPCMBuffer alloc]initWithPCMFormat:[file processingFormat] frameCapacity : [file length] ];
            bool success = [file readIntoBuffer : buffer error : &testError];
            if (!success)
                NSCAssert(success, @"could not read file into buffer", [testError localizedDescription]);

                    AudioComponentDescription mixerDesc;
            mixerDesc.componentType = kAudioUnitType_Mixer;
            mixerDesc.componentSubType = kAudioUnitSubType_MatrixMixer;
            mixerDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
            mixerDesc.componentFlags = kAudioComponentFlag_SandboxSafe;
            mixerDesc.componentFlagsMask = 0;

            matrixMixerNode = nullptr;
            [AVAudioUnit instantiateWithComponentDescription : mixerDesc options : kAudioComponentInstantiation_LoadInProcess completionHandler :
            ^ (__kindof AVAudioUnit * _Nullable mixerUnit, NSError * _Nullable error)
            {
                matrixMixerNode = mixerUnit;
                [audio->engine attachNode : matrixMixerNode] ;
            }] ;

            while (!matrixMixerNode)
            {
                usleep(100); // waiting for the completionHandler to finish
            }

            // Give the mixer one input bus and one output bus
            UInt32 inBuses = 1;
            UInt32 outBuses = 1;
            AudioUnitSetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &inBuses, sizeof(UInt32));
            AudioUnitSetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Output, 0, &outBuses, sizeof(UInt32));

            // Set the mixer's input format to have the correct number of channels
            UInt32 size;
            AudioStreamBasicDescription mixerFormatIn;
            AudioUnitGetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, &size);
            mixerFormatIn.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            AudioUnitSetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, size);

            // Set the mixer's output format to have the correct number of channels
            AudioStreamBasicDescription mixerFormatOut;
            AudioUnitGetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, &size);
            mixerFormatOut.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            AudioUnitSetProperty(matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, size);

            AVAudioFormat* audioFormat;
            if (buffer.format.channelCount == 1) // for mono sounds
                audioFormat = [[AVAudioFormat alloc]
                initWithCommonFormat:AVAudioPCMFormatFloat32 sampleRate : buffer.format.sampleRate channels : 1 interleaved : false];
            // Attempt to use more than 2 channels instead of default format
            else
                audioFormat = [[AVAudioFormat alloc]
                initWithCommonFormat:AVAudioPCMFormatFloat32 sampleRate : buffer.format.sampleRate channels : G_NUM_OF_OUTPUTS interleaved : false];

            [audio->engine connect : player to : audio->engine.mainMixerNode format : audioFormat] ;
            [audioFormat release] ;

            isPlaying = false;
            isPaused = false;

            return this;
        }

        unsigned int GetChannels()
        {
            if (!player)
                return 0;
            return[file processingFormat].channelCount;
        }

        bool SetChannelVolumes(float* _volumes, unsigned int _numChannels)
        {
            if (!player)
                return false;

            if (_numChannels > 1)
            {
                float newVal = 0.0f;
                float sumChannels = 0.0f;
                for (int i = 0; i < _numChannels; i++)
                {
                    // total of all channels
                    sumChannels += _volumes[i];
                    // channels 0 and 4 are left channels which have pan of -1
                    if (i == 0 || i == 4)
                        newVal += _volumes[i] * -1.0f;
                    // skip 2 and 3, since center channels have pan of 0
                    else if (i == 1 || i == 5)
                        newVal += _volumes[i];
                }

                // obtain average and set the panning (sum has to be greater than 1 to apply clamping)
                player.pan = (sumChannels > 1.0f) ? (newVal / sumChannels) : newVal;
            }
            else
            {
                player.pan = -1 * _volumes[0];
            }

            return true;
        }

        bool SetVolume(float _newVolume)
        {
            if (!player)
                return false;

            [player setVolume : (_newVolume)] ;
            float check = [player volume];

            if (check == _newVolume)
                return true;

            return false;
        }

        bool Play()
        {
            if (!player)
                return false;

            [player play] ;
            [player scheduleBuffer : buffer atTime : nil options : AVAudioPlayerNodeBufferInterrupts completionHandler :
            ^ {
                isPlaying = false;
                isPaused = false;
            }] ;

            isPlaying = true;
            isPaused = false;

            return[player isPlaying];
        }

        bool Pause()
        {
            if (!player)
                return false;

            [player pause] ;
            isPlaying = false;
            isPaused = true;

            return ![player isPlaying];
        }

        bool Resume()
        {
            if (!player)
                return false;

            [player playAtTime : nil] ;
            isPlaying = true;
            isPaused = false;

            return[player isPlaying];
        }

        bool Stop()
        {
            if (!player)
                return false;

            [player stop] ;
            isPlaying = false;
            isPaused = false;

            return ![player isPlaying];
        }

        bool Unload()
        {
            bool macresult = false;
            if (player != nil)
            {
                [player stop] ;
                [player release] ;
                player = nil;
                macresult = true;
            }

            // Cleans up memory in the autorelease pool
            if (file != nil)
            {
                [file release] ;
                file = nil;
            }

            if (buffer != nil)
            {
                [buffer release] ;
                buffer = nil;
            }

            return macresult;
        }
    };
}

namespace GW
{
    namespace I
    {
        class GSoundImplementation : public virtual GSoundInterface,
            protected GThreadSharedImplementation
        {
            float masterVolume = 1.0f; // global master volume
            float globalSoundsVolume = 1.0f; // global sounds volume
            float volume = 1.0f; // volume of this sound

            float       channelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // channel volumes of this sound
            float masterChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // global master volumes
            internal_gw::GMacSound* mac_snd = nullptr;
            GW::AUDIO::GAudio gAudio;
            GW::CORE::GEventReceiver gReceiver;

            void Destroy()
            {
                LockSyncWrite();
                if (mac_snd)
                {
                    mac_snd->Unload();
                    delete mac_snd;
                    mac_snd = nullptr;
                }
                UnlockSyncWrite();
            }
        public:
            virtual ~GSoundImplementation()
            {
                // Objective C cleanup
                Destroy();
            }

            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, float _volume = 1.0f)
            {
                if (!_path || !_audio)
                    return GReturn::INVALID_ARGUMENT;

                if (_volume < 0.0f || _volume > 1.0f)
                    return GReturn::INVALID_ARGUMENT;

                gAudio = _audio;
                auto audioImplementation = std::dynamic_pointer_cast<GW::I::GAudioImplementation>(*_audio);

                mac_snd = new internal_gw::GMacSound();
                mac_snd->gSound = this;
                mac_snd->audio = audioImplementation->mac_audio;
                NSString* nsPath = [[[NSString alloc]initWithUTF8String:_path] autorelease];
                mac_snd->initWithPath(nsPath);

                globalSoundsVolume = audioImplementation->soundsVolume;
                masterVolume = audioImplementation->masterVolume;
                memcpy(masterChannelVolumes, audioImplementation->soundsChannelVolumes, 6 * sizeof(float));

                GReturn result = SetVolume(_volume);
                if (result != GReturn::SUCCESS)
                    return result;

                result = SetChannelVolumes(channelVolumes, 6);
                if (result != GReturn::SUCCESS)
                    return result;

                result = GThreadSharedImplementation::Create();
                if (result != GReturn::SUCCESS)
                    return result;

                return gReceiver.Create(_audio, [&]()
                {
                    GW::GEvent gEvent;
                    GW::AUDIO::GAudio::Events audioEvent;
                    GW::AUDIO::GAudio::EVENT_DATA audioEventData;
                    // Process the event message
                    gReceiver.Pop(gEvent);
                    gEvent.Read(audioEvent);

                    switch (audioEvent)
                    {
                    case GW::AUDIO::GAudio::Events::DESTROY:
                    {
                        //printf("DESTROY RECEIVED IN SOUND\n");
                        // If GAudio is destroyed, the sound is no longer operational
                        Destroy();
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::PLAY_SOUNDS:
                    {
                        Play();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("SOUND PLAY %d\n", playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::PAUSE_SOUNDS:
                    {
                        Pause();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("SOUND PAUSE %d\n", !playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::RESUME_SOUNDS:
                    {
                        Resume();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("SOUND RESUME %d\n", playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::STOP_SOUNDS:
                    {
                        Stop();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("SOUND STOP: %d\n", !playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::MASTER_VOLUME_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        masterVolume = audioEventData.channelVolumes[0];
                        // Update the current volume with a new master volume
                        SetVolume(volume);
                        //printf("MASTER_VOLUME: %f | VOLUME: %f\n", masterVolume, volume * globalSoundsVolume * masterVolume);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::SOUNDS_VOLUME_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        globalSoundsVolume = audioEventData.channelVolumes[0];
                        // Update the current volume with a new master volume
                        SetVolume(volume);
                        //printf("GLOBAL SOUND VOLUME: %f | VOLUME: %f\n", globalSoundsVolume, volume * globalSoundsVolume * masterVolume);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::SOUND_CHANNEL_VOLUMES_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        memcpy(masterChannelVolumes, audioEventData.channelVolumes, audioEventData.numOfChannels * sizeof(float));
                        SetChannelVolumes(channelVolumes, audioEventData.numOfChannels);
                        //printf("SOUND CHANNEL VOLUMES: { %f, %f, %f, %f, %f, %f }\n", channelVolumes[0] * masterChannelVolumes[0], channelVolumes[1] * masterChannelVolumes[1], channelVolumes[2] * masterChannelVolumes[2], channelVolumes[3] * masterChannelVolumes[3], channelVolumes[4] * masterChannelVolumes[4], channelVolumes[5] * masterChannelVolumes[5]);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                    }
                });
            }

            GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_numChannels == 0 || _numChannels > 6 || _values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                float resultChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
                for (unsigned int i = 0; i < _numChannels; i++)
                {
                    if (_values[i] < 0.0f)
                        return GReturn::INVALID_ARGUMENT;

                    // apply clamping
                    channelVolumes[i] = (_values[i] > 1.0f) ? 1.0f : _values[i];
                    // apply master channel volumes
                    resultChannelVolumes[i] = channelVolumes[i] * masterChannelVolumes[i];
                }

                if (!mac_snd->player)
                    return GReturn::FAILURE;

                LockSyncWrite();
                bool result = mac_snd->SetChannelVolumes(resultChannelVolumes, _numChannels);
                UnlockSyncWrite();

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn SetVolume(float _newVolume) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_newVolume < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                // Clip the passed volume to max
                volume = (_newVolume > 1.0f) ? 1.0f : _newVolume;

                if (!mac_snd->player)
                    return GReturn::FAILURE;

                // Apply master volume ratios to the sound volume (Doesn't need to be normalized, since result is always < 1.0f)
                LockSyncWrite();
                bool result = mac_snd->SetVolume(volume * globalSoundsVolume * masterVolume);
                UnlockSyncWrite();

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Play() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = mac_snd->Play();

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Pause() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = mac_snd->Pause();

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Resume() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = mac_snd->Resume();

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Stop() override
            {
                bool result = mac_snd->Stop();

                // Critical function, all the code must be executed even in PREMATURE_DEALLOCATION state
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = mac_snd->GetChannels();
                return GReturn::SUCCESS;
            }

            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = G_NUM_OF_OUTPUTS;
                return GReturn::SUCCESS;
            }

            GReturn isPlaying(bool& _returnedBool) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                _returnedBool = mac_snd->isPlaying;
                return GReturn::SUCCESS;
            }

            // ThreadShared
            GReturn LockAsyncRead() const override
            {
                return GThreadSharedImplementation::LockAsyncRead();
            }

            GReturn UnlockAsyncRead() const override
            {
                return GThreadSharedImplementation::UnlockAsyncRead();
            }

            GReturn LockSyncWrite() override
            {
                return GThreadSharedImplementation::LockSyncWrite();
            }

            GReturn UnlockSyncWrite() override
            {
                return GThreadSharedImplementation::UnlockSyncWrite();
            }
        };
    }// end I
}// end GW

#undef G_NUM_OF_OUTPUTS
