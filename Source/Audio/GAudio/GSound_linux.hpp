#include <pulse/mainloop.h>
#include <pulse/thread-mainloop.h>
#include <pulse/mainloop-api.h>
#include <pulse/channelmap.h>
#include <pulse/context.h>
#include <pulse/volume.h>
#include <pulse/stream.h>
#include <pulse/introspect.h>

#include <thread>
#include <atomic>

#define G_NUM_OF_OUTPUTS 2 // Currently set to forced Stereo

// Cork defines
#define G_AUDIO_PAUSED 1
#define G_AUDIO_RESUMED 0

#include "wavreader.hpp"
#include "../../../Interface/System/GConcurrent.h"

namespace GW
{
    namespace I
    {
        class GSoundImplementation : public virtual GSoundInterface,
            protected GThreadSharedImplementation
        {
            std::atomic_bool atomic_isPlaying{};
            std::atomic_bool atomic_isPaused{};
            std::atomic_bool atomic_isComplete{};
            std::atomic_bool atomic_stopFlag{};
            float masterVolume = 1.0f; // global master volume
            float globalSoundsVolume = 1.0f; // global sounds volume
            float volume = 1.0f; // volume of this sound
            uint32_t sinkIndex = UINT32_MAX;
            std::unique_ptr<pa_channel_map> myMap{nullptr};

            std::function<void(pa_stream*)> streamDestructor = [&](pa_stream* _stream)
            {
                if (_stream)
                {
                    if (audio_state->main_loop) pa_threaded_mainloop_lock(audio_state->main_loop.get());
                    pa_stream_disconnect(_stream);
                    pa_stream_unref(_stream);
                    if (audio_state->main_loop) pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                }
            };
            std::unique_ptr<pa_stream, decltype(streamDestructor)> myStream{nullptr, streamDestructor};

			std::shared_ptr<internal_gw::AudioState> audio_state;

			pa_sample_spec mySampleSpec = { PA_SAMPLE_INVALID, 0, 0 };
            pa_cvolume vol = { 0, { PA_VOLUME_NORM, PA_VOLUME_NORM } };

            float       channelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // channel volumes of this sound
            float masterChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // global master volumes
            GW::AUDIO::GAudio gAudio;
            GW::CORE::GEventReceiver gReceiver;
            GW::SYSTEM::GConcurrent gConcurrent;
            WavReader myWavReader;

            void Destroy()
            {
                myStream = nullptr;
                myMap = nullptr;
            }

            static void FinishedDrainOp(pa_stream*, int, void* userdata)
            {
                GSoundImplementation* impl = static_cast<GSoundImplementation*>(userdata);
                impl->atomic_isComplete = true;
            }

            static void FlushOpCallback(pa_stream*, int, void* userdata)
            {
                const GSoundImplementation* impl = static_cast<GSoundImplementation*>(userdata);
                pa_threaded_mainloop_signal(impl->audio_state->main_loop.get(), 0);
            }

        public:
            ~GSoundImplementation() override
            {
                GSoundImplementation::Stop();
                GSoundImplementation::LockSyncWrite();
                Destroy();
                GSoundImplementation::UnlockSyncWrite();
            }

            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, const float _volume = 1.0f)
            {
                if (!_path || !_audio)
                    return GReturn::INVALID_ARGUMENT;

                if (_volume < 0.0f || _volume > 1.0f)
                    return GReturn::INVALID_ARGUMENT;

                GReturn result = GThreadSharedImplementation::Create();
                if (result != GReturn::SUCCESS)
                    return result;

                gAudio = _audio;
                const auto audioImplementation = std::dynamic_pointer_cast<GW::I::GAudioImplementation>(*_audio);

                if (-myWavReader.ReadWAV(_path))
                    return GReturn::FILE_NOT_FOUND;

                if (!internal_gw::get_audio_state(audio_state)) return GReturn::FAILURE;

                while (audio_state->pa_ready == 0)
                    std::this_thread::yield();

                if (audio_state->pa_ready != 1)
                    return GReturn::FAILURE;

                const auto header = myWavReader.GetHeader();
                switch (header.format.bitsPerSample)
                {
                case 8:
                    mySampleSpec.format = PA_SAMPLE_U8;
                    break;
                case 16:
                    mySampleSpec.format = PA_SAMPLE_S16LE;
                    break;
                case 24:
                    mySampleSpec.format = PA_SAMPLE_S24LE;
                    break;
                case 32:
                    mySampleSpec.format = (header.format.formatTag > 1) ? PA_SAMPLE_FLOAT32LE : PA_SAMPLE_S32LE; // Float type for IEEE and Signed 32int for PCM
                    break;
                default:
                    mySampleSpec.format = PA_SAMPLE_INVALID;
                    return GReturn::FAILURE;
                }

                mySampleSpec.rate = header.format.sampleRate;
                mySampleSpec.channels = header.format.channels;

                if (pa_channels_valid(mySampleSpec.channels) == 0)
                    return GReturn::FAILURE;

                pa_threaded_mainloop_lock(audio_state->main_loop.get());
                {
                    myMap.reset(new pa_channel_map());
                    if (pa_channel_map_init_extend(myMap.get(), mySampleSpec.channels, PA_CHANNEL_MAP_WAVEEX) == nullptr)
                    {
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                        return GReturn::FAILURE;
                    }

                    myStream.reset(pa_stream_new(audio_state->context.get(), "GSound", &mySampleSpec, myMap.get()));
                    if (myStream == nullptr)
                    {
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                        return GReturn::FAILURE;
                    }

                    if (pa_stream_connect_playback(myStream.get(), nullptr, nullptr, static_cast<pa_stream_flags_t>(0), nullptr, nullptr) != 0)
                    {
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                        return GReturn::FAILURE;
                    }
                }
                pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                globalSoundsVolume = audioImplementation->soundsVolume;
                masterVolume = audioImplementation->masterVolume;
                memcpy(masterChannelVolumes, audioImplementation->soundsChannelVolumes, 6 * sizeof(float));

                pa_cvolume_init(&vol);

                result = SetVolume(_volume);
                if (result != GReturn::SUCCESS)
                    return result;

                result = gConcurrent.Create(true);
                if (result != GReturn::SUCCESS) // Events are suppressed
                    return result;

                return gReceiver.Create(_audio, [&]()
                    {
                        GW::GEvent gEvent;
                        GW::AUDIO::GAudio::Events audioEvent;
                        GW::AUDIO::GAudio::EVENT_DATA audioEventData;
                        // Process the event message
                        gReceiver.Pop(gEvent);
                        gEvent.Read(audioEvent);

                        switch (audioEvent)
                        {
                        case GW::AUDIO::GAudio::Events::DESTROY:
                            {
                                Stop();
                                LockSyncWrite();
                                Destroy();
                                UnlockSyncWrite();
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::PLAY_SOUNDS:
                            {
                                Play();
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::PAUSE_SOUNDS:
                            {
                                Pause();
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::RESUME_SOUNDS:
                            {
                                Resume();
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::STOP_SOUNDS:
                            {
                                Stop();
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::MASTER_VOLUME_CHANGED:
                            {
                                gEvent.Read(audioEventData);
                                masterVolume = audioEventData.channelVolumes[0];
                                // Update the current volume with a new master volume
                                SetVolume(volume);
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::SOUNDS_VOLUME_CHANGED:
                            {
                                gEvent.Read(audioEventData);
                                globalSoundsVolume = audioEventData.channelVolumes[0];
                                // Update the current volume with a new master volume
                                SetVolume(volume);
                                break;
                            }
                        case GW::AUDIO::GAudio::Events::SOUND_CHANNEL_VOLUMES_CHANGED:
                            {
                                gEvent.Read(audioEventData);
                                memcpy(masterChannelVolumes, audioEventData.channelVolumes, static_cast<int>(audioEventData.numOfChannels * sizeof(float)));
                                SetChannelVolumes(channelVolumes, audioEventData.numOfChannels);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                        }
                    });
            }

            GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_numChannels == 0 || _numChannels > 6 || _values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                const float adjustedVolume = volume * masterVolume * globalSoundsVolume;
                for (unsigned int i = 0; i < _numChannels; i++)
                {
                    if (_values[i] < 0.0f)
                        return GReturn::INVALID_ARGUMENT;

                    // 5.1 to stereo fallback
                    if (_numChannels > G_NUM_OF_OUTPUTS)
                    {
                        channelVolumes[i] = std::min(_values[i], 1.0f);

                        switch (i)
                        {
                        case 2: // Front Center
                        {
                            channelVolumes[0] += channelVolumes[i] * 0.5f;
                            channelVolumes[1] += channelVolumes[i] * 0.5f;
                            break;
                        }
                        case 3: // LFE
                        {
                            channelVolumes[0] += channelVolumes[i] * 0.3f;
                            channelVolumes[1] += channelVolumes[i] * 0.3f;
                            break;
                        }
                        case 4: // Rear Left
                        {
                            channelVolumes[0] += channelVolumes[i] * 0.7f;
                            break;
                        }
                        case 5: //Rear Right
                        {
                            channelVolumes[1] += channelVolumes[i] * 0.7f;
                            break;
                        }
                        default:
                            break;
                        }

                        // clamp stereo to max of 1.0f
                        if (i == _numChannels - 1)
                        {
                            channelVolumes[0] = std::min(channelVolumes[0], 1.0f);
                            channelVolumes[1] = std::min(channelVolumes[1], 1.0f);

                            const double adjustedVolumeLeft = std::min(adjustedVolume * channelVolumes[0] * masterChannelVolumes[0], 1.0f);
                            const double adjustedVolumeRight = std::min(adjustedVolume * channelVolumes[1] * masterChannelVolumes[1], 1.0f);
                            vol.values[0] = pa_sw_volume_from_linear(adjustedVolumeLeft);
                            vol.values[1] = pa_sw_volume_from_linear(adjustedVolumeRight);
                            break;
                        }
                    }
                    else
                    {
                        channelVolumes[i] = (_values[i] > 1.0f) ? 1.0f : _values[i];

                        // apply clamping and master volume multiplier
                        const double adjustedVolumeClamped = std::min(volume * channelVolumes[i] * masterChannelVolumes[i], 1.0f);
                        vol.values[i] = pa_sw_volume_from_linear(adjustedVolumeClamped);
                    }
                }

                LockSyncWrite();
                while (sinkIndex == UINT32_MAX)
                {
                    pa_threaded_mainloop_lock(audio_state->main_loop.get());
                    sinkIndex = pa_stream_get_index(myStream.get()); //Returns the sink resp. source output index this stream is identified in the server with
                    pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                }
                UnlockSyncWrite();

                vol.channels = G_NUM_OF_OUTPUTS;

                //Set the volume of a sink input stream.
                pa_threaded_mainloop_lock(audio_state->main_loop.get());
                {
                    pa_operation* sinkInputVolumeOp = pa_context_set_sink_input_volume(audio_state->context.get(), sinkIndex, &vol, nullptr, nullptr);

                    if (!sinkInputVolumeOp)
                    {
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                        return GReturn::FAILURE;
                    }

                    pa_operation_unref(sinkInputVolumeOp);
                }
                pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                return GReturn::SUCCESS;
            }

            GReturn SetVolume(float _newVolume) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_newVolume < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                // Clip the passed volume to max
                _newVolume = std::min(_newVolume, 1.0f);
                volume = _newVolume;

                // Apply master volume ratio to the sound volume (Doesn't need to be normalized, since masterVolume is always < 1.0f)
                _newVolume *= masterVolume * globalSoundsVolume;
                vol.channels = G_NUM_OF_OUTPUTS;
                for (int i = 0; i < vol.channels; ++i)
                {
                    const double newVolumeClamped = std::min(_newVolume * channelVolumes[i] * masterChannelVolumes[i], 1.0f);
                    vol.values[i] = pa_sw_volume_from_linear(newVolumeClamped);
                }

                LockSyncWrite();
                while (sinkIndex == UINT32_MAX)
                {
                    pa_threaded_mainloop_lock(audio_state->main_loop.get());
                    sinkIndex = pa_stream_get_index(myStream.get()); //Returns the sink resp. source output index this stream is identified in the server with
                    pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                }
                UnlockSyncWrite();

                pa_threaded_mainloop_lock(audio_state->main_loop.get());
                {
                    //Set the volume of a sink input stream.
                    pa_operation* sinkInputVolumeOp = pa_context_set_sink_input_volume(audio_state->context.get(), sinkIndex, &vol, nullptr, nullptr);

                    if (!sinkInputVolumeOp)
                    {
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                        return GReturn::FAILURE;
                    }

                    pa_operation_unref(sinkInputVolumeOp);
                }
                pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                return GReturn::SUCCESS;
            }

            GReturn Play() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPlaying || atomic_isComplete)
                {
                    const GReturn result = Stop();

                    if (result != GReturn::SUCCESS)
                        return result;
                }

                if (!atomic_isPlaying)
                {
                    atomic_stopFlag = false;
                    atomic_isPaused = false;
                    atomic_isPlaying = true;
                    gConcurrent.BranchSingular([&]()
                    {
                        unsigned int playBackPt = 0;
                        while (true)
                        {
                            if (atomic_stopFlag)
                            {
                                pa_threaded_mainloop_lock(audio_state->main_loop.get());

                                pa_operation* flushOp = pa_stream_flush(myStream.get(), FlushOpCallback, this);
                                if (flushOp)
                                {
                                    while (pa_operation_get_state(flushOp) == PA_OPERATION_RUNNING)
                                        pa_threaded_mainloop_wait(audio_state->main_loop.get());
                                    pa_operation_unref(flushOp);
                                }

                                pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                                break;
                            }

                            if (atomic_isPlaying)
                            {
                                pa_threaded_mainloop_lock(audio_state->main_loop.get());
                                const pa_stream_state_t state = pa_stream_get_state(myStream.get());
                                // synchronous call, requires locking
                                pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                                if (state == PA_STREAM_READY)
                                {
                                    pa_threaded_mainloop_lock(audio_state->main_loop.get());
                                    const size_t writeableSize = pa_stream_writable_size(myStream.get());
                                    pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                                    const size_t sizeRemain = myWavReader.GetBufferSize() - playBackPt;
                                    const size_t writeSize = writeableSize > sizeRemain ? sizeRemain : writeableSize;

                                    if (writeSize > 0)
                                    {
                                        pa_threaded_mainloop_lock(audio_state->main_loop.get());
                                        pa_stream_write(myStream.get(),
                                                        myWavReader.GetBuffer().data.get() + playBackPt,
                                                        static_cast<int>(writeSize),
                                                        nullptr,
                                                        0,
                                                        PA_SEEK_RELATIVE);
                                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                                        playBackPt += writeSize;
                                    }
                                    else if (writeableSize > 0 && !atomic_isComplete)
                                    {
                                        pa_threaded_mainloop_lock(audio_state->main_loop.get());
                                        {
                                            pa_operation* drainOp = pa_stream_drain(myStream.get(), FinishedDrainOp, this);

                                            if (drainOp)
                                            {
                                                pa_operation_unref(drainOp);
                                            }
                                        }
                                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                                        break;
                                    }
                                }
                            }
                        }
                    });
                }

                return GReturn::SUCCESS;
            }

            GReturn Pause() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (!atomic_isPaused)
                {
                    pa_threaded_mainloop_lock(audio_state->main_loop.get());
                    const int value = pa_stream_is_corked(myStream.get()); // 1 = paused, 0 = resumed
                    pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                    if (value == G_AUDIO_RESUMED)
                    {
                        pa_threaded_mainloop_lock(audio_state->main_loop.get());
                        {
                            pa_operation* corkOp = pa_stream_cork(myStream.get(), G_AUDIO_PAUSED, nullptr, nullptr);

                            if (!corkOp)
                            {
                                pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                                return GReturn::FAILURE;
                            }

                            pa_operation_unref(corkOp);
                        }
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                    }

                    atomic_isPaused = true;
                    atomic_isPlaying = false;
                }

                return GReturn::SUCCESS;
            }

            GReturn Resume() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPaused)
                {
                    pa_threaded_mainloop_lock(audio_state->main_loop.get());
                    const int value = pa_stream_is_corked(myStream.get()); // 1 = paused, 0 = resumed
                    pa_threaded_mainloop_unlock(audio_state->main_loop.get());

                    if (value == G_AUDIO_PAUSED)
                    {
                        pa_threaded_mainloop_lock(audio_state->main_loop.get());
                        {
                            pa_operation* corkOp = pa_stream_cork(myStream.get(), G_AUDIO_RESUMED, nullptr, nullptr);

                            if (!corkOp)
                            {
                                pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                                return GReturn::FAILURE;
                            }

                            pa_operation_unref(corkOp);
                        }
                        pa_threaded_mainloop_unlock(audio_state->main_loop.get());
                    }

                    atomic_isPlaying = true;
                    atomic_isPaused = false;
                }
                else
                    return GReturn::REDUNDANT;

                return GReturn::SUCCESS;
            }

            GReturn Stop() override
            {
                atomic_isPlaying = false;
                atomic_isPaused = false;
                atomic_isComplete = false;
                atomic_stopFlag = true;

                gConcurrent.Converge(0);

                // Converge has to run before return
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                return GReturn::SUCCESS;
            }

            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = mySampleSpec.channels;
                return GReturn::SUCCESS;
            }

            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = G_NUM_OF_OUTPUTS; // forced Stereo
                return GReturn::SUCCESS;
            }

            GReturn isPlaying(bool& _returnedBool) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                _returnedBool = atomic_isPlaying;
                return GReturn::SUCCESS;
            }

        protected:
            // ThreadShared
            GReturn LockAsyncRead() const override
            {
                return GThreadSharedImplementation::LockAsyncRead();
            }

            GReturn UnlockAsyncRead() const override
            {
                return GThreadSharedImplementation::UnlockAsyncRead();
            }

            GReturn LockSyncWrite() override
            {
                return GThreadSharedImplementation::LockSyncWrite();
            }

            GReturn UnlockSyncWrite() override
            {
                return GThreadSharedImplementation::UnlockSyncWrite();
            }
        };
    }// end I
}// end GW

#undef G_NUM_OF_OUTPUTS
#undef G_AUDIO_PAUSED
#undef G_AUDIO_RESUMED
