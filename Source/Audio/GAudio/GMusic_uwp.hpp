#define G_NUM_OF_OUTPUTS 6
#define G_STREAMING_BUFFER_SIZE 65536
#define G_MAX_BUFFER_COUNT 3

#include "wavreader.hpp"
#include "../../../Interface/System/GConcurrent.h"

namespace GW
{
    namespace I
    {
        class GMusicImplementation : public virtual GMusicInterface,
            protected GThreadSharedImplementation,
            protected IXAudio2VoiceCallback
        {
            void STDMETHODCALLTYPE OnBufferStart(void*) override { ResetEvent(hBufferEndEvent.get()); }
            void STDMETHODCALLTYPE OnBufferEnd(void*) override { SetEvent(hBufferEndEvent.get()); }
            // Required studs as IXAudio2VoiceCallback is an abstract class
            void STDMETHODCALLTYPE OnVoiceProcessingPassStart(UINT32) override {}
            void STDMETHODCALLTYPE OnVoiceProcessingPassEnd() override {}
            void STDMETHODCALLTYPE OnVoiceError(void*, HRESULT) override {}
            void STDMETHODCALLTYPE OnStreamEnd() override {}
            void STDMETHODCALLTYPE OnLoopEnd(void*) override {}

            std::atomic_bool atomic_isPlaying = false;
            std::atomic_bool atomic_isPaused = false;
            std::atomic_bool atomic_isComplete = false;
            std::atomic_bool atomic_isLooping = false;
            std::atomic_bool atomic_stopFlag = false;
            float masterVolume = 1.0f; // global master volume
            float globalMusicVolume = 1.0f; // global music volume
            float volume = 1.0f; // volume of this sound
            unsigned int numOfChannels = 0;

            std::function<void(IXAudio2SourceVoice*)> mySourceVoiceDestructor = [&](IXAudio2SourceVoice* sourceVoice)
            {
                if (sourceVoice)
                    sourceVoice->DestroyVoice();
            };
            std::unique_ptr<IXAudio2SourceVoice, decltype(mySourceVoiceDestructor)> mySourceVoice{nullptr, mySourceVoiceDestructor};

            std::function<void(IXAudio2SubmixVoice*)> mySubmixVoiceDestructor = [&](IXAudio2SubmixVoice* submixVoice)
            {
                if (submixVoice)
                    submixVoice->DestroyVoice();
            };
            std::unique_ptr<IXAudio2SubmixVoice, decltype(mySubmixVoiceDestructor)> mySubmixVoice{nullptr, mySubmixVoiceDestructor};
            std::unique_ptr<char> filePath{nullptr};
            std::unique_ptr<void, decltype(&CloseHandle)> hBufferEndEvent{nullptr, &CloseHandle};

            float       channelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // channel volumes of this sound
            float masterChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // global master volumes
            GW::AUDIO::GAudio gAudio;
            GW::CORE::GEventReceiver gReceiver;
            GW::SYSTEM::GConcurrent gConcurrent;
            char buffers[G_MAX_BUFFER_COUNT][G_STREAMING_BUFFER_SIZE] = {0};

            static std::unique_ptr<char> CreateFilePath(const char* _constCharArray)
            {
	            const size_t size = strlen(_constCharArray) + 1;
                if (size >= 1024)
                    return nullptr;

                std::unique_ptr<char> charArray(new char[size]);
                strcpy_s(charArray.get(), size, _constCharArray);

                // This is here to fix the path and remove mixing of '/' and '\\'
                for (size_t i = 0; i < size; ++i)
                {
                    if (charArray.get()[i] == '/')
                        charArray.get()[i] = '\\';
                }

                return charArray;
            }

        public:
            ~GMusicImplementation() override
            {
	            GMusicImplementation::Stop();

                // We do not need to check for GAudio proxy here, since those handles would get cleaned up 
                // in the event callback if GAudio gets deleted first
	            GMusicImplementation::LockSyncWrite();
                mySourceVoice = nullptr;
                mySubmixVoice = nullptr;
	            GMusicImplementation::UnlockSyncWrite();
            }

            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, const float _volume = 1.0f)
            {
                if (!_path || !_audio)
                    return GReturn::INVALID_ARGUMENT;

                if (_volume < 0.0f || _volume > 1.0f)
                    return GReturn::INVALID_ARGUMENT;

                filePath = CreateFilePath(_path);
                // if _path is > 1024 chars
                if (filePath == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                gAudio = _audio;
                const auto audioImplementation = std::dynamic_pointer_cast<GW::I::GAudioImplementation>(*_audio);
                hBufferEndEvent.reset(CreateEventEx(nullptr, nullptr, CREATE_EVENT_MANUAL_RESET, EVENT_MODIFY_STATE | SYNCHRONIZE));
                if (!hBufferEndEvent)
                    return GReturn::FAILURE;

                WavReader wavReader;
                if (-wavReader.ReadWAVOffset(filePath.get()))
                    return GReturn::FAILURE;

                auto& header = wavReader.GetHeader();
                // Size, in bytes, of extra format information appended to the end of the WAVEFORMATEX structure. (for non-PCM formats)
                header.format.extraChunkSize = (header.format.formatTag > 1) ? 22 : 0;
                numOfChannels = static_cast<unsigned int>(header.format.channels);

                IXAudio2SubmixVoice* submixVoice = nullptr;
                if (audioImplementation->XAudioData.myAudio->CreateSubmixVoice(&submixVoice, G_NUM_OF_OUTPUTS, header.format.sampleRate) != S_OK)
                    return GReturn::FAILURE;
                mySubmixVoice.reset(submixVoice);

                XAUDIO2_SEND_DESCRIPTOR mscSendDcsp = { 0, mySubmixVoice.get() };
                const XAUDIO2_VOICE_SENDS mscSendList = { 1, &mscSendDcsp };

                IXAudio2SourceVoice* sourceVoice = nullptr;
                if (audioImplementation->XAudioData.myAudio->CreateSourceVoice(&sourceVoice, reinterpret_cast<const WAVEFORMATEX*>(&header.format), 0, XAUDIO2_DEFAULT_FREQ_RATIO, this, &mscSendList) != S_OK)
                    return GReturn::FAILURE;
                mySourceVoice.reset(sourceVoice);

                globalMusicVolume = audioImplementation->musicVolume;
                masterVolume = audioImplementation->masterVolume;
                memcpy(masterChannelVolumes, audioImplementation->musicChannelVolumes, 6 * sizeof(float));

                GReturn result = SetVolume(_volume);
                if (result != GReturn::SUCCESS)
                    return result;

                result = SetChannelVolumes(channelVolumes, 6);
                if (result != GReturn::SUCCESS)
                    return result;

                result = GThreadSharedImplementation::Create();
                if (result != GReturn::SUCCESS)
                    return result;

                result = gConcurrent.Create(true);
                if (result != GReturn::SUCCESS) // Events are suppressed
                    return result;

                return gReceiver.Create(_audio, [&]()
                    {
                        GW::GEvent gEvent;
                        GW::AUDIO::GAudio::Events audioEvent;
                        GW::AUDIO::GAudio::EVENT_DATA audioEventData;
                        // Process the event message
                        gReceiver.Pop(gEvent);
                        gEvent.Read(audioEvent);

                        switch (audioEvent)
                        {
                        case GW::AUDIO::GAudio::Events::DESTROY:
                        {
                            //printf("DESTROY RECEIVED IN MUSIC\n");
                            Stop();
                            // Need to cleanup XAudio handles before GAudio is deleted
                            LockSyncWrite();
                            mySourceVoice = nullptr;
                            mySubmixVoice = nullptr;
                            UnlockSyncWrite();
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::PLAY_MUSIC:
                        {
                            Play();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("MUSIC PLAY %d\n", playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::PAUSE_MUSIC:
                        {
                            Pause();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("MUSIC PAUSE %d\n", !playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::RESUME_MUSIC:
                        {
                            Resume();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("MUSIC RESUME %d\n", playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::STOP_MUSIC:
                        {
                            Stop();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("MUSIC STOP: %d\n", !playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::MASTER_VOLUME_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            masterVolume = audioEventData.channelVolumes[0];
                            // Update the current volume with a new master volume
                            SetVolume(volume);
                            //printf("MASTER_VOLUME: %f | VOLUME: %f\n", masterVolume, volume * globalMusicVolume * masterVolume);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::MUSIC_VOLUME_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            globalMusicVolume = audioEventData.channelVolumes[0];
                            // Update the current volume with a new master volume
                            SetVolume(volume);
                            //printf("GLOBAL MUSIC VOLUME: %f | VOLUME: %f\n", globalMusicVolume, volume * globalMusicVolume * masterVolume);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::MUSIC_CHANNEL_VOLUMES_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            memcpy(masterChannelVolumes, audioEventData.channelVolumes, audioEventData.numOfChannels * sizeof(float));
                            SetChannelVolumes(channelVolumes, audioEventData.numOfChannels);
                            //printf("MUSIC CHANNEL VOLUMES: { %f, %f, %f, %f, %f, %f }\n", channelVolumes[0] * masterChannelVolumes[0], channelVolumes[1] * masterChannelVolumes[1], channelVolumes[2] * masterChannelVolumes[2], channelVolumes[3] * masterChannelVolumes[3], channelVolumes[4] * masterChannelVolumes[4], channelVolumes[5] * masterChannelVolumes[5]);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                        }
                    });
            }

            GReturn SetChannelVolumes(const float* _values, const unsigned int _numChannels) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_numChannels == 0 || _numChannels > 6 || _values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                for (unsigned int i = 0; i < _numChannels; i++)
                {
                    if (_values[i] < 0.0f)
                        return GReturn::INVALID_ARGUMENT;

                    // apply clamping and master volume multiplier
                    channelVolumes[i] = (_values[i] > 1.0f) ? 1.0f : _values[i];
                }

                unsigned int sourceChannels = 0;
                GetSourceChannels(sourceChannels);

                // can only support up to 6 outputs
                float matrix[12] = { 0 };
                unsigned int trueIndex = 0;
                for (unsigned int i = 0; i < 12;)
                {
                    if (trueIndex < _numChannels)
                    {
	                    const float matrixVolume = channelVolumes[trueIndex] * masterChannelVolumes[trueIndex];
                        matrix[i] = matrixVolume;
                        matrix[i + 1] = matrixVolume;
                        trueIndex++;
                        i += 2;
                    }
                    else
                    {
                        matrix[i] = 0;
                        i++;
                    }
                }

                LockSyncWrite();
                if (mySourceVoice == nullptr || FAILED(mySourceVoice->SetOutputMatrix(mySubmixVoice.get(), sourceChannels, G_NUM_OF_OUTPUTS, matrix)))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                UnlockSyncWrite();

                return GReturn::SUCCESS;
            }

            GReturn SetVolume(float _newVolume) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_newVolume < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                // Clip the passed volume to max
                _newVolume = (_newVolume > 1.0f) ? 1.0f : _newVolume;
                volume = _newVolume;

                // Apply master volume ratio to the sound volume (Doesn't need to be normalized, since masterVolume is always < 1.0f)
                LockSyncWrite();
                if (mySourceVoice == nullptr || FAILED(mySourceVoice->SetVolume(volume * globalMusicVolume * masterVolume)))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                UnlockSyncWrite();

                return GReturn::SUCCESS;
            }

            GReturn Play(const bool _loop = false) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                atomic_isLooping = _loop;

                if (atomic_isPlaying || atomic_isComplete)
                {
	                const GReturn result = Stop();
                    if (result != GReturn::SUCCESS)
                        return result;
                }

                if (atomic_isPlaying == false)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Start(0, 0)))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();

                    atomic_stopFlag = false;
                    atomic_isPaused = false;
                    atomic_isPlaying = true;

                    gConcurrent.BranchSingular([&]()
                    {
                        WavReader wavReader;
                        if (-wavReader.ReadWAVOffset(filePath.get()))
                            return GReturn::FAILURE;

	                    int currentDiskReadBuffer = 0;
	                    DWORD currentPosition = 0;

                        const size_t bufferSize = wavReader.GetBufferSize();
	                    while (currentPosition < bufferSize && atomic_stopFlag == false)
	                    {
		                    if (atomic_isPlaying)
		                    {
			                    const DWORD cbValid = (G_STREAMING_BUFFER_SIZE < (bufferSize - currentPosition))
				                                          ? G_STREAMING_BUFFER_SIZE // if less
				                                          : (bufferSize - currentPosition); // if greater or equal

                                if (-wavReader.ReadDataChunk(buffers[currentDiskReadBuffer], cbValid))
                                    continue;

			                    // update the file position to where it will be once the read finishes
			                    currentPosition += cbValid;

			                    XAUDIO2_VOICE_STATE state = {0};
			                    while (atomic_isPlaying)
			                    {
				                    LockAsyncRead();
				                    if (mySourceVoice == nullptr)
				                    {
					                    UnlockAsyncRead();
					                    return GReturn::FAILURE;
				                    }
				                    mySourceVoice->GetState(&state);
				                    UnlockAsyncRead();

				                    if (state.BuffersQueued >= G_MAX_BUFFER_COUNT - 1)
				                    {
					                    if (WaitForSingleObjectEx(hBufferEndEvent.get(), INFINITE, TRUE) == WAIT_FAILED)
						                    break; // if deadlocks on pause check here <REMINDER>
				                    }
				                    else break;
			                    }

			                    /*
			                    FOR FUTURE AUDIO DEVELOPER
			                    PCM FUNCTION POINTER GOES HERE
			                    for(int i = 0; i < cbValid; i++)
			                    {
			                        dataFunction(buffers[CurrentDiskReadBuffer][i]);
			                    }
			                    */

			                    XAUDIO2_BUFFER buf = {0};
			                    buf.AudioBytes = cbValid;
			                    buf.pAudioData = reinterpret_cast<const BYTE*>(buffers[currentDiskReadBuffer]);

			                    if (currentPosition >= bufferSize)
			                    {
				                    if (atomic_isLooping)
				                    {
					                    LockSyncWrite();
					                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->SubmitSourceBuffer(&buf)))
					                    {
						                    UnlockSyncWrite();
						                    return GReturn::FAILURE;
					                    }
					                    UnlockSyncWrite();

					                    ++currentDiskReadBuffer %= G_MAX_BUFFER_COUNT;
					                    currentPosition = 0;

                                        if (-wavReader.SeekToDataOffset())
                                            return GReturn::FAILURE;

					                    continue;
				                    }
				                    else
					                    buf.Flags = XAUDIO2_END_OF_STREAM;
			                    }

			                    LockSyncWrite();
			                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->SubmitSourceBuffer(&buf)))
			                    {
				                    UnlockSyncWrite();
				                    return GReturn::FAILURE;
			                    }
			                    UnlockSyncWrite();
			                    ++currentDiskReadBuffer %= G_MAX_BUFFER_COUNT;
		                    }
	                    }

                        wavReader.CloseFile();

	                    XAUDIO2_VOICE_STATE state;
	                    LockAsyncRead();
	                    if (mySourceVoice == nullptr)
	                    {
		                    UnlockAsyncRead();
		                    return GReturn::FAILURE;
	                    }
	                    mySourceVoice->GetState(&state);
	                    UnlockAsyncRead();

	                    // Waits for last buffers to finish playing
	                    while (state.BuffersQueued > 0)
	                    {
		                    if (atomic_stopFlag == true)
			                    break;

		                    LockAsyncRead();
		                    if (mySourceVoice == nullptr)
		                    {
			                    UnlockAsyncRead();
			                    return GReturn::FAILURE;
		                    }
		                    mySourceVoice->GetState(&state);
		                    UnlockAsyncRead();

		                    // [TODO:] needs testing
		                    if (WaitForSingleObjectEx(hBufferEndEvent.get(), INFINITE, TRUE) == WAIT_FAILED)
			                    break;
	                    }

	                    // Stops the voice from producing more sound
	                    LockSyncWrite();
	                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Stop()))
	                    {
		                    UnlockSyncWrite();
		                    return GReturn::FAILURE;
	                    }
	                    UnlockSyncWrite();

	                    // Updates information about playback state
	                    atomic_isPlaying = false;
	                    atomic_isPaused = false;
	                    atomic_isComplete = true;

	                    return GReturn::SUCCESS;
                    });
                }

                return GReturn::SUCCESS;
            }

            GReturn Pause() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPaused == false)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Stop()))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();
                }

                atomic_isPlaying = false;
                atomic_isPaused = true;
                SetEvent(hBufferEndEvent.get());

                return GReturn::SUCCESS;
            }

            GReturn Resume() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPaused == true)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Start()))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();

                    atomic_isPlaying = true;
                    atomic_isPaused = false;
                }
                else
                    return GReturn::REDUNDANT;

                return GReturn::SUCCESS;
            }

            GReturn Stop() override
            {
                atomic_isPlaying = false;
                atomic_isPaused = false;
                atomic_isComplete = false;
                atomic_stopFlag = true;

                LockSyncWrite();
                if (mySourceVoice != nullptr)
                    mySourceVoice->FlushSourceBuffers();
                UnlockSyncWrite();

                gConcurrent.Converge(0);

                // Critical function, all the code must be executed even in PREMATURE_DEALLOCATION state
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                return GReturn::SUCCESS;
            }

            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = numOfChannels;
                return GReturn::SUCCESS;
            }

            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = G_NUM_OF_OUTPUTS;
                return GReturn::SUCCESS;
            }

            GReturn isPlaying(bool& _returnedBool) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                _returnedBool = atomic_isPlaying;
                return GReturn::SUCCESS;
            }

        protected:
            // ThreadShared
            GReturn LockAsyncRead() const override
            {
                return GThreadSharedImplementation::LockAsyncRead();
            }

            GReturn UnlockAsyncRead() const override
            {
                return GThreadSharedImplementation::UnlockAsyncRead();
            }

            GReturn LockSyncWrite() override
            {
                return GThreadSharedImplementation::LockSyncWrite();
            }

            GReturn UnlockSyncWrite() override
            {
                return GThreadSharedImplementation::UnlockSyncWrite();
            }
        };
    }// end I
}// end GW

#undef G_NUM_OF_OUTPUTS
#undef G_STREAMING_BUFFER_SIZE
#undef G_MAX_BUFFER_COUNT