#define G_NUM_OF_OUTPUTS 6
#include "wavreader.hpp"

namespace GW
{
    namespace I
    {
        class GSoundImplementation : public virtual GSoundInterface,
            protected GThreadSharedImplementation,
            protected IXAudio2VoiceCallback
        {
            void STDMETHODCALLTYPE OnStreamEnd() override
            {
                // When stream ends, the sound gets flagged as complete
                atomic_isComplete = true;
                atomic_isPaused = false;
                atomic_isPlaying = false;
                SetEvent(hStreamEndEvent.get());
            }
            // Required studs as IXAudio2VoiceCallback is an abstract class
            void STDMETHODCALLTYPE OnBufferEnd(void*) override {}
            void STDMETHODCALLTYPE OnBufferStart(void*) override {}
            void STDMETHODCALLTYPE OnVoiceProcessingPassStart(UINT32) override {}
            void STDMETHODCALLTYPE OnVoiceProcessingPassEnd() override {}
            void STDMETHODCALLTYPE OnVoiceError(void*, HRESULT) override {}
            void STDMETHODCALLTYPE OnLoopEnd(void*) override {}

            std::atomic_bool atomic_isPlaying = false;
            std::atomic_bool atomic_isPaused = false;
            std::atomic_bool atomic_isComplete = false;
            float masterVolume = 1.0f; // global master volume
            float globalSoundsVolume = 1.0f; // global sounds volume
            float volume = 1.0f; // volume of this sound
            unsigned int numOfChannels = 0;

            std::function<void(IXAudio2SourceVoice*)> mySourceVoiceDestructor = [&](IXAudio2SourceVoice* sourceVoice)
            {
                if (sourceVoice)
                    sourceVoice->DestroyVoice();
            };
            std::unique_ptr<IXAudio2SourceVoice, decltype(mySourceVoiceDestructor)> mySourceVoice{nullptr, mySourceVoiceDestructor};

            std::function<void(IXAudio2SubmixVoice*)> mySubmixVoiceDestructor = [&](IXAudio2SubmixVoice* submixVoice)
            {
                if (submixVoice)
                    submixVoice->DestroyVoice();
            };
            std::unique_ptr<IXAudio2SubmixVoice, decltype(mySubmixVoiceDestructor)> mySubmixVoice{nullptr, mySubmixVoiceDestructor};
            std::unique_ptr<void, decltype(&CloseHandle)> hStreamEndEvent{nullptr, &CloseHandle};

            float       channelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // channel volumes of this sound
            float masterChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // global master volumes
            GW::AUDIO::GAudio gAudio;
            GW::CORE::GEventReceiver gReceiver;
            WavReader wavReader;
            XAUDIO2_BUFFER buffer = {}; // We need the XAudio buffer to live as long as the wav reader.

            static std::unique_ptr<char> getValidPath(const char* path)
            {
	            const size_t size = strlen(path) + 1;
                std::unique_ptr<char> corrected(new char[size]);

                strcpy_s(corrected.get(), size, path);

            	for (size_t i = 0; i < size; ++i)
                {
                    if (corrected.get()[i] == '/')
                        corrected.get()[i] = '\\';
                }

                return corrected;
            }

        public:
            ~GSoundImplementation() override
            {
	            GSoundImplementation::Stop();

                // We do not need to check for GAudio proxy here, since those handles would get cleaned up 
                // in the event callback if GAudio gets deleted first
	            GSoundImplementation::LockSyncWrite();
                mySourceVoice = nullptr;
                mySubmixVoice = nullptr;
	            GSoundImplementation::UnlockSyncWrite();
            }

            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, const float _volume = 1.0f)
            {
                if (!_path || !_audio)
                    return GReturn::INVALID_ARGUMENT;

                if (_volume < 0.0f || _volume > 1.0f)
                    return GReturn::INVALID_ARGUMENT;

                hStreamEndEvent.reset(CreateEventEx(nullptr, nullptr, CREATE_EVENT_MANUAL_RESET, EVENT_MODIFY_STATE | SYNCHRONIZE));
                if (!hStreamEndEvent)
                    return GReturn::FAILURE;

                const std::unique_ptr<char> path = getValidPath(_path);
                if (-wavReader.ReadWAV(path.get()))
                    return GReturn::FAILURE;

                //if (wfmx.Format.nChannels > maxChannels)
                //    maxChannels = wfmx.Format.nChannels;

                gAudio = _audio;
                auto& header = wavReader.GetHeader();
                const auto audioImplementation = std::dynamic_pointer_cast<GW::I::GAudioImplementation>(*_audio);

                IXAudio2SubmixVoice* submixVoice = nullptr;
                if (audioImplementation->XAudioData.myAudio->CreateSubmixVoice(&submixVoice, G_NUM_OF_OUTPUTS, header.format.sampleRate) != S_OK)
                    return GReturn::FAILURE;
                mySubmixVoice.reset(submixVoice);

                XAUDIO2_SEND_DESCRIPTOR sndSendDcsp = { 0, mySubmixVoice.get() };
                const XAUDIO2_VOICE_SENDS sndSendList = { 1, &sndSendDcsp };

                // Size, in bytes, of extra format information appended to the end of the WAVEFORMATEX structure. (for non-PCM formats)
                header.format.extraChunkSize = (header.format.formatTag > 1) ? 22 : 0;
                numOfChannels = static_cast<unsigned int>(header.format.channels);

                IXAudio2SourceVoice* sourceVoice = nullptr;
                if (audioImplementation->XAudioData.myAudio->CreateSourceVoice(&sourceVoice, reinterpret_cast<const WAVEFORMATEX*>(&header.format), 0, XAUDIO2_DEFAULT_FREQ_RATIO, this, &sndSendList) != S_OK)
                    return GReturn::FAILURE;
                mySourceVoice.reset(sourceVoice);

                buffer.AudioBytes = static_cast<UINT32>(wavReader.GetBufferSize());
                buffer.pAudioData = static_cast<const BYTE*>(wavReader.GetBuffer().data.get());
                buffer.Flags = XAUDIO2_END_OF_STREAM;
                if (FAILED(mySourceVoice->SubmitSourceBuffer(&buffer)))
                    return GReturn::FAILURE;

                globalSoundsVolume = audioImplementation->soundsVolume;
                masterVolume = audioImplementation->masterVolume;
                memcpy(masterChannelVolumes, audioImplementation->soundsChannelVolumes, 6 * sizeof(float));

                GReturn result = SetVolume(_volume);
                if (result != GReturn::SUCCESS)
                    return result;

                result = SetChannelVolumes(channelVolumes, 6);
                if (result != GReturn::SUCCESS)
                    return result;

                result = GThreadSharedImplementation::Create();
                if (result != GReturn::SUCCESS)
                    return result;

                return gReceiver.Create(_audio, [&]()
                    {
                        GW::GEvent gEvent;
                        GW::AUDIO::GAudio::Events audioEvent;
                        GW::AUDIO::GAudio::EVENT_DATA audioEventData;
                        // Process the event message
                        gReceiver.Pop(gEvent);
                        gEvent.Read(audioEvent);

                        switch (audioEvent)
                        {
                        case GW::AUDIO::GAudio::Events::DESTROY:
                        {
                            //printf("DESTROY RECEIVED IN SOUND\n");
                            Stop();
                            // Need to cleanup XAudio handles before GAudio is deleted
                            LockSyncWrite();
                            mySourceVoice = nullptr;
                            mySubmixVoice = nullptr;
                            UnlockSyncWrite();
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::PLAY_SOUNDS:
                        {
                            Play();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("SOUND PLAY %d\n", playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::PAUSE_SOUNDS:
                        {
                            Pause();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("SOUND PAUSE %d\n", !playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::RESUME_SOUNDS:
                        {
                            Resume();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("SOUND RESUME %d\n", playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::STOP_SOUNDS:
                        {
                            Stop();
                            //bool playing;
                            //isPlaying(playing);
                            //printf("SOUND STOP: %d\n", !playing);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::MASTER_VOLUME_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            masterVolume = audioEventData.channelVolumes[0];
                            // Update the current volume with a new master volume
                            SetVolume(volume);
                            //printf("MASTER_VOLUME: %f | VOLUME: %f\n", masterVolume, volume * globalSoundsVolume * masterVolume);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::SOUNDS_VOLUME_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            globalSoundsVolume = audioEventData.channelVolumes[0];
                            // Update the current volume with a new master volume
                            SetVolume(volume);
                            //printf("GLOBAL SOUND VOLUME: %f | VOLUME: %f\n", globalSoundsVolume, volume * globalSoundsVolume * masterVolume);
                            break;
                        }
                        case GW::AUDIO::GAudio::Events::SOUND_CHANNEL_VOLUMES_CHANGED:
                        {
                            gEvent.Read(audioEventData);
                            memcpy(masterChannelVolumes, audioEventData.channelVolumes, audioEventData.numOfChannels * sizeof(float));
                            SetChannelVolumes(channelVolumes, audioEventData.numOfChannels);
                            //printf("SOUND CHANNEL VOLUMES: { %f, %f, %f, %f, %f, %f }\n", channelVolumes[0] * masterChannelVolumes[0], channelVolumes[1] * masterChannelVolumes[1], channelVolumes[2] * masterChannelVolumes[2], channelVolumes[3] * masterChannelVolumes[3], channelVolumes[4] * masterChannelVolumes[4], channelVolumes[5] * masterChannelVolumes[5]);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                        }
                    });
            }

            GReturn SetChannelVolumes(const float* _values, const unsigned int _numChannels) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_numChannels == 0 || _numChannels > 6 || _values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                for (unsigned int i = 0; i < _numChannels; i++)
                {
                    if (_values[i] < 0.0f)
                        return GReturn::INVALID_ARGUMENT;

                    // apply clamping and master volume multiplier
                    channelVolumes[i] = (_values[i] > 1.0f) ? 1.0f : _values[i];
                }

                // can only support up to 6 outputs
                float matrix[12] = { 0 };
                unsigned int trueIndex = 0;
                for (unsigned int i = 0; i < 12;)
                {
                    if (trueIndex < _numChannels)
                    {
	                    const float matrixVolume = channelVolumes[trueIndex] * masterChannelVolumes[trueIndex];
                        matrix[i] = matrixVolume;
                        matrix[i + 1] = matrixVolume;
                        trueIndex++;
                        i += 2;
                    }
                    else
                    {
                        matrix[i] = 0;
                        i++;
                    }
                }

                LockSyncWrite();
                if (mySourceVoice == nullptr || FAILED(mySourceVoice->SetOutputMatrix(mySubmixVoice.get(), numOfChannels, G_NUM_OF_OUTPUTS, matrix)))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                UnlockSyncWrite();

                return GReturn::SUCCESS;
            }

            GReturn SetVolume(const float _newVolume) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_newVolume < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                // Clip the passed volume to max
                volume = (_newVolume > 1.0f) ? 1.0f : _newVolume;

                // Apply master volume ratio to the sound volume (Doesn't need to be normalized, since masterVolume is always < 1.0f)
                LockSyncWrite();
                if (mySourceVoice == nullptr || FAILED(mySourceVoice->SetVolume(volume * globalSoundsVolume * masterVolume)))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                UnlockSyncWrite();

                return GReturn::SUCCESS;
            }

            GReturn Play() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPlaying || atomic_isComplete)
                {
	                const GReturn result = Stop();
                    if (result != GReturn::SUCCESS)
                        return result;
                }

                if (atomic_isPlaying == false)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Start()))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();

                    atomic_isPlaying = true;
                    atomic_isPaused = false;
                }

                return GReturn::SUCCESS;
            }

            GReturn Pause() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPaused == false)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Stop()))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();

                    atomic_isPlaying = false;
                    atomic_isPaused = true;
                }

                return GReturn::SUCCESS;
            }

            GReturn Resume() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (atomic_isPaused == true)
                {
                    LockSyncWrite();
                    if (mySourceVoice == nullptr || FAILED(mySourceVoice->Start()))
                    {
                        UnlockSyncWrite();
                        return GReturn::FAILURE;
                    }
                    UnlockSyncWrite();

                    atomic_isPlaying = true;
                    atomic_isPaused = false;
                }
                else
                    return GReturn::REDUNDANT;

                return GReturn::SUCCESS;
            }

            GReturn Stop() override
            {
                atomic_isPlaying = false;
                atomic_isPaused = false;
                atomic_isComplete = false;

                LockSyncWrite();
                if (mySourceVoice == nullptr || FAILED(mySourceVoice->Stop()))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                mySourceVoice->FlushSourceBuffers();
                if (FAILED(mySourceVoice->SubmitSourceBuffer(&buffer)))
                {
                    UnlockSyncWrite();
                    return GReturn::FAILURE;
                }
                UnlockSyncWrite();

                // Critical function, all the code must be executed even in PREMATURE_DEALLOCATION state
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                return GReturn::SUCCESS;
            }

            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = numOfChannels;
                return GReturn::SUCCESS;
            }

            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = G_NUM_OF_OUTPUTS;
                return GReturn::SUCCESS;
            }

            GReturn isPlaying(bool& _returnedBool) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                _returnedBool = atomic_isPlaying;
                return GReturn::SUCCESS;
            }

        protected:
            // ThreadShared
            GReturn LockAsyncRead() const override
            {
                return GThreadSharedImplementation::LockAsyncRead();
            }

            GReturn UnlockAsyncRead() const override
            {
                return GThreadSharedImplementation::UnlockAsyncRead();
            }

            GReturn LockSyncWrite() override
            {
                return GThreadSharedImplementation::LockSyncWrite();
            }

            GReturn UnlockSyncWrite() override
            {
                return GThreadSharedImplementation::UnlockSyncWrite();
            }
        };
    }// end I
}// end GW

#undef G_NUM_OF_OUTPUTS