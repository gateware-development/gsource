namespace GW
{
    namespace I
    {
        class GSoundImplementation : public virtual GSoundInterface
        {
        public:
            // Main class
            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, float _value = 1.0f)
            {
                return GReturn::INTERFACE_UNSUPPORTED;
            }
            GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetVolume(float _newVolume) override
            {
                return GReturn::FAILURE;
            }
            GReturn Play() override
            {
                return GReturn::FAILURE;
            }
            GReturn Pause() override
            {
                return GReturn::FAILURE;
            }
            GReturn Resume() override
            {
                return GReturn::FAILURE;
            }
            GReturn Stop() override
            {
                return GReturn::FAILURE;
            }
            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                return GReturn::FAILURE;
            }
            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                return GReturn::FAILURE;
            }
            GReturn isPlaying(bool& _returnedBool) const override
            {
                return GReturn::FAILURE;
            }
            //// ThreadShared
            //GReturn LockAsyncRead() const override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn UnlockAsyncRead() const override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn LockSyncWrite() override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn UnlockSyncWrite() override
            //{
            //    return GReturn::FAILURE;
            //}
            //// Events
            //GReturn Append(const GEvent& _inEvent) override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Waiting(unsigned int& _outCount) const override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Pop(GEvent& _outEvent) override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Peek(GEvent& _outEvent) const override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Missed(unsigned int& _outCount) const override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Clear() override
            //{
            //    return GReturn::FAILURE;
            //}
            //GReturn Invoke() const override
            //{
            //    return GReturn::FAILURE;
            //}
        };
    }
}
