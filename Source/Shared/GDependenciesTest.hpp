#ifndef GDEPENDENCIESTEST_HPP
#define GDEPENDENCIESTEST_HPP
/*!
	File: GDependenciesTest.hpp
	Purpose: Checks if user enabled libraries have their dependencies.
	Dependencies: None
	Author: Lam Truong
	Contributors:
	Interface Status: Beta (GatewareX)
	Copyright: 7thGate Software LLC.
	License: MIT
*/

/*!
-If user wants error messages instead of warnings, define 'GATEWARE_DEPENDENCY_TEST_FORCE'

Should work as follows:
- check if dependency test is enabled

- core test is unique:
-- since every library is dependent on 'Core', this checks if 'Core' is UNDEFINED
-- if undefined, define a variable that will act as a 'token' for messages, if not check for its dependencies
--- i designed(hardcoded) it so there is no check for reoccurring 'tokens' unless it may have been defined from a test before

-rest of tests go as follows:

- check if a specific library is enabled, if not skip

- if it is enabled, check dependencies on major libraries(Audio, Core, Math, etc)
- then, check if each header file the enabled library contains, has their dependencies
-- dependencies are found in header files(their includes)

- if a needed library is disabled, define a variable 'token' using name of library
-- example: if 'audio' is enabled -> check if its dependency, 'system' is enabled -> if yes: (do nothing), if no: #define GW_DSystem

- check for what variable tokens were made and send a message saying what libraries need to be enabled
-- message type depends on if 'GATEWARE_DEPENDENCY_TEST_FORCE' is defined by user.
-- default message type is warnings, force is errors

- undefine any possible 'token'

NOTES:
- Core/GInterface does not have a test because its dependency is Core defines, which has no .hpp file

- For Graphics, the GRasterSurface API is the only one that is dependent on GW_Math. So there is an extra check in the 'graphics' test for that API

- One error message is sent at a time. User may hit another error on the next build if another library dependency is missing

*/

//--DEPENDENCY TESTS--
// If check to see if dependency test is defined/enabled from main
#ifndef GATEWARE_DISABLE_DEPENDENCY_TEST

	#pragma region CORE_DEPENDENCY_TESTS
	//--CORE LIBRARY DEPENDENCY TESTS--

	// check if 'Core' library is enabled
	#ifdef GATEWARE_ENABLE_CORE
		// check if header file is DISABLED
		// - GEventCache will be disabled if user manually defines 'GATEWARE_DISABLE_GEVENTCACHE' in main, or anywhere really
		#ifndef GATEWARE_DISABLE_GEVENTCACHE
			// check if GEventCache's dependency is disabled
			#ifdef GATEWARE_DISABLE_GTHREADSHARED
				#define GW_DGThreadShared
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GEVENTGENERATOR
			#ifdef GATEWARE_DISABLE_GEVENTCACHE
				#define GW_DGEventCache
			#endif
			#ifdef GATEWARE_DISABLE_GEVENTRESPONDER
				#define GW_DGEventResponder
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GEVENTQUEUE
			#ifdef GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GEVENTRECEIVER
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GEVENTRESPONDER
			#ifdef GATEWARE_DISABLE_GLOGIC
				#define GW_DGLogic
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GLOGIC
			#ifdef GATEWARE_DISABLE_GTHREADSHARED
				#define GW_DGThreadShared
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GTHREADSHARED
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
	#else
		// if undefined set token
		#define GW_DCore
	#endif
	//--END-CORE LIBRARY DEPENDENCY TESTS
	#pragma endregion

	#pragma region AUDIO_DEPENDENCY_TESTS
	//--AUDIO DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_AUDIO
		#ifndef GATEWARE_ENABLE_SYSTEM
			#define GW_DSystem
		#endif
		#ifndef GATEWARE_ENABLE_MATH
			#define GW_DMath
		#endif
		#ifndef GATEWARE_ENABLE_CORE
			#define GW_DCore
		#endif

		#ifndef GATEWARE_DISABLE_GAUDIO
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif

		#ifndef GATEWARE_DISABLE_GMUSIC
			#ifdef GATEWARE_DISABLE_GAUDIO
				#define GW_DAudio
			#endif
			#ifdef GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
			#ifdef GATEWARE_DISABLE_GCONCURRENT
				#define GW_DGConcurrent
			#endif
			#ifdef GATEWARE_DISABLE_GFILE
				#define GW_DGFile
			#endif
		#endif

		#ifndef GATEWARE_DISABLE_GSOUND
			#ifdef GATEWARE_DISABLE_GAUDIO
				#define GW_DAudio
			#endif
			#ifdef GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
			#ifdef GATEWARE_DISABLE_GFILE
				#define GW_DGFile
			#endif
		#endif

		#ifndef GATEWARE_DISABLE_GAUDIO3D
			#ifdef GATEWARE_DISABLE_GAUDIO
				#define GW_DAudio
			#endif
			#ifdef GATEWARE_DISABLE_GMATRIX
				#define GW_DGMatrix
			#endif
			#ifdef GATEWARE_DISABLE_GQUATERNION
				#define GW_DGQuaternion
			#endif
			#ifdef GATEWARE_DISABLE_GVECTOR
				#define GW_DGVector
			#endif
		#endif

		#ifndef GATEWARE_DISABLE_GMUSIC3D
			#ifdef GATEWARE_DISABLE_GAUDIO3D
				#define GW_DAudio3D
			#endif
			#ifdef GATEWARE_DISABLE_GMUSIC
				#define GW_DGMusic
			#endif
		#endif

		#ifndef GATEWARE_DISABLE_GSOUND3D
			#ifdef GATEWARE_DISABLE_GAUDIO3D
				#define GW_DAudio3D
			#endif
			#ifdef GATEWARE_DISABLE_GSOUND
				#define GW_DGSound
			#endif
		#endif
	#endif

	
	//--END-AUDIO DEPENDENCY TESTS--
	#pragma endregion

	#pragma region GRAPHICS_DEPENDENCY_TESTS
	//--GRAPHICS DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_GRAPHICS
		#ifndef GATEWARE_ENABLE_SYSTEM
			#ifndef GW_DSystem
				#define GW_DSystem
			#endif
		#endif
		// This is a special check, if the user is using GRasterSurface API
		#ifdef GRASTERSURFACE_H
			#ifndef GATEWARE_ENABLE_MATH
				#ifndef GW_DMath
					#define GW_DMath
				#endif
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GBLITTER
			#ifdef GATEWARE_DISABLE_GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GDIRECTX11SURFACE
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GDIRECTX12SURFACE
			#ifdef GATEWARE_DISABLE_GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GOPENGLSURFACE
			#ifdef GATEWARE_DISABLE_GATEWARE_DISABLE_GEVENTRECEIVER
				#define GW_DGEventReceiver
			#endif
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
		#if defined(GATEWARE_SKIP_GRASTERSURFACE) || !defined(GATEWARE_DISABLE_GRASTERSURFACE)
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GVULKANSURFACE
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
			#ifdef GATEWARE_DISABLE_GEVENTRESPONDER
				#define GW_DGEventResponder
			#endif
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
	#endif
	

	//--END-GRAPHICS DEPENDENCY TESTS--
	#pragma endregion

	#pragma region INPUT_DEPENDENCY_TEST
	//--INPUT DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_INPUT
		#ifndef GATEWARE_ENABLE_SYSTEM
			#ifndef GW_DSystem
				#define GW_DSystem
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GBUFFEREDINPUT
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GCONTROLLER
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GINPUT
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
			#ifdef GATEWARE_DISABLE_GWINDOW
				#define GW_DWindow
			#endif
		#endif
	#endif

	//--END-INPUT DEPENDENCY TESTS--
	#pragma endregion

	#pragma region MATH_DEPENDENCY_TESTS
	//--MATH DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_MATH
		#ifndef GATEWARE_DISABLE_GCOLLISION
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
			#ifdef GATEWARE_DISABLE_GVECTOR
				#define GW_DGVector
			#endif
			#ifdef GATEWARE_DISABLE_GMATRIX
				#define GW_DGMatrix
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GMATRIX
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GQUATERNION
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GVECTOR
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
	#endif
	//--END-MATH DEPENDENCY TESTS--
	#pragma endregion

	#pragma region MATH2D_DEPENDENCY_TESTS
	//--MATH2D DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_MATH2D
		#ifndef GATEWARE_DISABLE_GCOLLISION2D
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GMATRIX2D
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GVECTOR2D
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
	#endif
	//--END-MATH2D DEPENDENCY TESTS--
	#pragma endregion

	#pragma region	SYSTEM_DEPENDENCY_TESTS
	//--SYSTEM DEPENDENCY TESTS--

	#ifdef GATEWARE_ENABLE_SYSTEM
		#ifndef GATEWARE_DISABLE_GCONCURRENT
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GDAEMON
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GFILE
			#ifdef GATEWARE_DISABLE_GINTERFACE
				#define GW_DGInterface
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GLOG
			#ifdef GATEWARE_DISABLE_GFILE
				#define GW_DGFile
			#endif
		#endif
		#ifndef GATEWARE_DISABLE_GLOG
			#ifdef GATEWARE_DISABLE_GEVENTGENERATOR
				#define GW_DGEventGenerator
			#endif
		#endif
	#endif

	//--END-SYSTEM DEPENDENCY TESTS--
	#pragma endregion

	// If check to see what type of message to send: 0 = warning, 1(or higher) = error
	#ifndef GATEWARE_DISABLE_DEPENDENCY_TEST
		#pragma message("**Dependency Test Output**")

		// check to see if marker is defined
		#ifdef GW_DCore
		#pragma message("Dependency Message: You need to enable Gatewares Core Library.")
		#endif
		#ifdef GW_DSystem
		#pragma message("Dependency Message: You need to enable Gatewares System Library.")
		#endif
		#ifdef GW_DMath
		#pragma message("Dependency Message: You need to enable Gatewares Math Library.")
		#endif

		#pragma region CORE_MESSAGES
		#ifdef GW_DGEventCache
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventCache Library.")
		#endif

		#ifdef GW_DGEventGenerator
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventGenerator Library.")
		#endif

		// should be removed on later versions of GW
		#ifdef GW_DGEventQueue
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventQueue Library.")
		#endif

		#ifdef GW_DGEventReceiver
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventReceiver Library.")
		#endif

		#ifdef GW_DGEventResponder
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventResponder Library.")
		#endif

		#ifdef GW_DGInterface
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventInterface Library.")
		#endif

		#ifdef GW_DGLogic
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventLogic Library.")
		#endif

		#ifdef GW_DGThreadShared
		#pragma message("Dependency Message: You need to enable Gatewares Core/GEventThreadShared Library.")
		#endif
		#pragma endregion

		#pragma region AUDIO_MESSAGES
		#ifdef GW_DGAudio
		#pragma message("Dependency Message: You need to enable Gatewares Audio/GAudio Library.")
		#endif

		#ifdef GW_DGAudio3D
		#pragma message("Dependency Message: You need to enable Gatewares Audio/GAudio3D Library.")
		#endif

		#ifdef GW_DGMusic
		#pragma message("Dependency Message: You need to enable Gatewares Audio/GMusic Library.")
		#endif

		#ifdef GW_DGSound
		#pragma message("Dependency Message: You need to enable Gatewares Audio/GSound Library.")
		#endif
		#pragma endregion

		#pragma region MATH_MESSAGES
		#ifdef GW_DGMatrix
		#pragma message("Dependency Message: You need to enable Gatewares Math/GMatrix Library.")
		#endif

		#ifdef GW_DGQuaternion
		#pragma message("Dependency Message: You need to enable Gatewares Math/GQuaternion Library.")
		#endif

		#ifdef GW_DGVector
		#pragma message("Dependency Message: You need to enable Gatewares Math/GVector Library.")
		#endif
		#pragma endregion

		#pragma region SYSTEM_MESSAGES
		#ifdef GW_DGConcurrent
		#pragma message("Dependency Message: You need to enable Gatewares System/GConcurrent Library.")
		#endif

		#ifdef GW_DGFile
		#pragma message("Dependency Message: You need to enable Gatewares System/GFile Library.")
		#endif

		#ifdef GW_DWindow
		#pragma message("Dependency Message: You need to enable Gatewares System/GWindow Library.")
		#endif
		#pragma endregion

		#pragma message("**End Dependency Test Output**")
	#endif

	#if !defined(GATEWARE_DISABLE_DEPENDENCY_TEST) && defined(GATEWARE_FORCE_DEPENDENCY_TEST)
		#ifdef GW_DCore
			#error Dependency Message: You need to enable Gatewares CORE Library.
		#endif
		#ifdef GW_DSystem
			#error Dependency Message: You need to enable Gatewares SYSTEM Library.
		#endif
		#ifdef GW_DMath
			#error Dependency Message: You need to enable Gatewares MATH Library.
		#endif

		#pragma region CORE_MESSAGES
		#ifdef GW_DGEventCache
			#error Dependency Message: You need to enable Gatewares Core/GEventCache Library.
		#endif
		#ifdef GW_DGEventGenerator
			#error Dependency Message: You need to enable Gatewares Core/GEventGenerator Library.
		#endif
		#ifdef GW_DGEventQueue
			#error Dependency Message: You need to enable Gatewares Core/GEventQueue Library.
		#endif
		#ifdef GW_DGEventReceiver
			#error Dependency Message: You need to enable Gatewares Core/GEventReceiver Library.
		#endif
		#ifdef GW_DGEventResponder
			#error Dependency Message: You need to enable Gatewares Core/GEventResponder Library.
		#endif
		#ifdef GW_DGInterface
			#error Dependency Message: You need to enable Gatewares Core/GEventInterface Library.
		#endif
		#ifdef GW_DGLogic
			#error Dependency Message: You need to enable Gatewares Core/GEventLogic Library.
		#endif
		#ifdef GW_DGThreadShared
			#error Dependency Message: You need to enable Gatewares Core/GEventThreadShared Library.
		#endif
		#pragma endregion

		#pragma region AUDIO_MESSAGES
		#ifdef GW_DGAudio
			#error Dependency Message: You need to enable Gatewares Audio/GAudio Library.
		#endif
		#ifdef GW_DGAudio3D
			#error Dependency Message: You need to enable Gatewares Audio/GAudio3D Library.
		#endif
		#ifdef GW_DGMusic
			#error Dependency Message: You need to enable Gatewares Audio/GMusic Library.
		#endif
		#ifdef GW_DGSound
			#error Dependency Message: You need to enable Gatewares Audio/GSound Library.
		#endif
		#pragma endregion

		#pragma region MATH_MESSAGES
		#ifdef GW_DGMatrix
			#error Dependency Message: You need to enable Gatewares Math/GMatrix Library.
		#endif
		#ifdef GW_DGQuaternion
			#error Dependency Message: You need to enable Gatewares Math/GQuaternion Library.
		#endif
		#ifdef GW_DGVector
			#error Dependency Message: You need to enable Gatewares Math/GVector Library.
		#endif
		#pragma endregion

		#pragma region SYSTEM_MESSAGES
		#ifdef GW_DGConcurrent
			#error Dependency Message: You need to enable Gatewares System/GConcurrent Library.
		#endif
		#ifdef GW_DGFile
			#error Dependency Message: You need to enable Gatewares System/GFile Library.
		#endif
		#ifdef GW_DWindow
			#error Dependency Message: You need to enable Gatewares System/GWindow Library.
		#endif
		#pragma endregion

	#endif

		// clean up
	#undef GW_DCore
	#undef GW_DSystem
	#undef GW_DMath
	#undef GW_DGEventCache
	#undef GW_DGEventGenerator
	#undef GW_DGEventQueue
	#undef GW_DGEventReceiver
	#undef GW_DGEventResponder
	#undef GW_DGInterface
	#undef GW_DGLogic
	#undef GW_DGThreadShared
	#undef GW_DGAudio
	#undef GW_DGAudio3D
	#undef GW_DGMusic
	#undef GW_DGSound
	#undef GW_DGMatrix
	#undef GW_DGQuaternion
	#undef GW_DGVector
	#undef GW_DGConcurrent
	#undef GW_DGFile
	#undef GW_DWindow

#endif //endif GATEWARE_DISABLE_DEPENDENCY_TEST
#endif //endif GDEPENDENCIESTEST_HPP
