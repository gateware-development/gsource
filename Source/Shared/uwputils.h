#ifndef GUTILITY_H
#define GUTILITY_H

#include <locale>
#include <codecvt>

// unknwn.h is not available on Apple or Linux based machines.
#if (defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)) || defined(_WIN32)
#include <unknwn.h>
#endif

namespace INTERNAL
{
	//UINT_MAX is not defined on Mac or Linux.
	//Internal #define for this.
#define G_UINT_MAX 0xFFFFFFFF

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
#include <stringapiset.h>
	//Macro to preform string conversions.
#define G_TO_UTF16(value) utf8_decode(value)
#define G_TO_UTF8(value) utf8_encode(value)

#endif

	//All variables and functions below are macroed above. This is so the code wrote out
	//will be the exact same code no matter the system we are writing it for.

	//These will convert utf8 to utf16 and vice versa
	//This is needed to be done because codecvt has been depricated in c++17
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
	//Convert a wide UTF16 string to an UTF8 string
	static std::string utf8_encode(const std::wstring& wstr)
	{
		if (wstr.empty())
		{
			return std::string();
		}
		int size = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
		std::string strRet(size, 0);
		WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strRet[0], size, NULL, NULL);
		return strRet;
	}

	//Convert a UTF8 string to a wide UTF16 string
	static std::wstring utf8_decode(const std::string& str)
	{
		if (str.empty())
		{
			return std::wstring();
		}
		int size = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
		std::wstring wstrRet(size, 0);
		MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrRet[0], size);
		return wstrRet;
	}

#endif

}//end INTERNAL namespace

#endif // #endif GUTILITY_H
