#ifndef CONCURRENT_THREADPOOL_H
#define CONCURRENT_THREADPOOL_H

#include <atomic>
#include <thread>
#include <mutex>
#include <array>
#include <list>
#include <functional>
#include <condition_variable>

namespace nbsdx {
namespace concurrent {

// Global per-thread access to the current ActiveJob running
// This is used for internal detection of deadlocks
namespace internal {
    static unsigned long long& GetActiveJob() {
        // (Gateware specific) tracks the active job on this thread. (-1 if no job)
        static thread_local unsigned long long ActiveJob = -1;
        return ActiveJob; // used to detect potential deadlocks
    }
}

/**
 *  Simple ThreadPool that creates `MaxThreadCount` threads upon its creation or
 *  std::thread::hardware_concurrency() whichever is less and pulls from a queue to get new jobs. 
 *  This class has been modified slightly for Gateware's needs, do not instantiate more than
 *  one of these classes. The class now uses thread_local vars for deadlock detection/prevention.
 *
 *  This class requires a number of c++11 features be present in your compiler.
 */
template <const unsigned MaxThreadCount>
class ThreadPool {

    // (Gateware custom) added this array for queries about running threadIds
    std::array<std::thread::id, MaxThreadCount> ids;
    std::array<std::thread, MaxThreadCount> threads;
    // each new job is assigned an increasing ID
    typedef std::pair<std::function<void(void)>, unsigned long long> JOB;
    std::list<JOB> queue; // all waiting jobs

    unsigned long long      JobCount;
    const unsigned          ThreadCount;
    std::atomic_int         jobs_left;
    std::atomic_bool        bailout;
    std::atomic_bool        finished;
    std::condition_variable job_available_var;
    std::condition_variable wait_var;
    std::mutex              wait_mutex;
    std::mutex              queue_mutex;
    /**
     *  Take the next job in the queue and run it.
     *  Notify the main thread that a job has completed.
     */
    void Task() {
        while( !bailout ) {
            next_job().first();
            // job has completed clear the thread local JobID var here
            internal::GetActiveJob() = -1; // no job is currently running on this thread
            --jobs_left;
            wait_var.notify_one();
        }
    }

    /**
     *  Get the next job; pop the first item in the queue, 
     *  otherwise wait for a signal from the main thread.
     */
    JOB next_job() {
        JOB res;
        std::unique_lock<std::mutex> job_lock( queue_mutex );

        // Wait for a job if we don't have any.
        job_available_var.wait( job_lock, [this]() ->bool { return queue.size() || bailout; } );
        
        // Get job from the queue
        if( !bailout ) {
            res = queue.front();
            queue.pop_front();
            // we about to run the next job...
            internal::GetActiveJob() = res.second;
        }
        else { // If we're bailing out, 'inject' a job into the queue to keep jobs_left accurate.
            res.first = []{};
            ++jobs_left;
        }
        return res;
    }

public:
   
    ThreadPool()
        : JobCount( 0 )
        , jobs_left( 0 )
        , bailout( false )
        , finished( false )
        , ThreadCount( // select all hardware threads or MaxThreads whichever is less
            (MaxThreadCount < std::thread::hardware_concurrency())
            ? MaxThreadCount : std::thread::hardware_concurrency())
    {
        for (unsigned i = 0; i < ThreadCount; ++i) {
            threads[i] = std::thread([this] { this->Task(); });
            // (Gateware custom) cache ids of running threads for fast queries
            ids[i] = threads[i].get_id();
        }
    }

    /**
     *  JoinAll on deconstruction
     */
    ~ThreadPool() {
        JoinAll();
    }

    /**
     *  Get the number of threads in this pool
     */
    inline unsigned Size() const {
        return ThreadCount;
    }

    /**
     *  Determine if a thread is one of the threads in the pool
     *  used to prevent possible deadlocks
     */
    inline bool Search(std::thread::id threadID) const {
        bool found = false;
        for (std::size_t i = 0; i < ThreadCount; ++i)
            if (ids[i] == threadID) {
                found = true;
                break;
            }
        return found;
    }

    /**
    *  Determine if the invoking thread is currently inside(actively running)
    *  the job identified by the provided JobID provided by "AddJob()"
    *  can be used to more accuratley detect/prevent possible deadlocks
    */
    inline bool InsideJob(unsigned long long jobID) const {
        return internal::GetActiveJob() == jobID;
    }

    /**
     *  Get the number of jobs left in the queue.
     */
    inline unsigned JobsRemaining() {
        std::lock_guard<std::mutex> guard( queue_mutex );
        return queue.size();
    }

    /**
     *  Add a new job to the pool. If there are no jobs in the queue,
     *  a thread is woken up to take the job. If all threads are busy,
     *  the job is added to the end of the queue
     *  *NEW* Function now returns an ID that can be used identify a running job on a thread.
     */
    unsigned long long AddJob( std::function<void(void)> job ) {
        std::lock_guard<std::mutex> guard( queue_mutex );
        queue.emplace_back( JOB(job, ++JobCount) );
        ++jobs_left;
        job_available_var.notify_one();
        return JobCount;
    }

    /**
     *  Variant of AddJob that allows you to supply your own JobID (don't use -1)
     *  to each job you add. You can then check against this ID using "InsideJob".
     */
    void AddJob(std::function<void(void)> job, unsigned long long jobID_override) {
        std::lock_guard<std::mutex> guard(queue_mutex);
        queue.emplace_back(JOB(job, jobID_override));
        ++JobCount; ++jobs_left;
        job_available_var.notify_one();
    }

    /**
     *  Join with all threads. Block until all threads have completed.
     *  Params: WaitForAll: If true, will wait for the queue to empty 
     *          before joining with threads. If false, will complete
     *          current jobs, then inform the threads to exit.
     *  The queue will be empty after this call, and the threads will
     *  be done. After invoking `ThreadPool::JoinAll`, the pool can no
     *  longer be used. If you need the pool to exist past completion
     *  of jobs, look to use `ThreadPool::WaitAll`.
     */
    void JoinAll( bool WaitForAll = true ) {
        if( !finished ) {
            if( WaitForAll ) {
                WaitAll();
            }

            // note that we're done, and wake up any thread that's
            // waiting for a new job
            bailout = true;
            job_available_var.notify_all();

            for (unsigned i = 0; i < ThreadCount; ++i) {
                if (threads[i].joinable()) {
                    threads[i].join();
                }
            }
            finished = true;
        }
    }

    /**
     *  Wait for the pool to empty before continuing. 
     *  This does not call `std::thread::join`, it only waits until
     *  all jobs have finshed executing.
     */
    void WaitAll() {
        if( jobs_left > 0 ) {
            std::unique_lock<std::mutex> lk( wait_mutex );
            wait_var.wait( lk, [this]{ return this->jobs_left == 0; } );
            lk.unlock();
        }
    }
};

} // namespace concurrent
} // namespace nbsdx

#endif // #endif CONCURRENT_THREADPOOL_H
