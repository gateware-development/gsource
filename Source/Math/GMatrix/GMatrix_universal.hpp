#include <cmath>

namespace GW
{
	namespace I
	{
		class GMatrixImplementation : public virtual GMatrixInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}
			// Floats
			static GReturn AddMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m1 = _matrix1;
				GW::MATH::GMATRIXF _m2 = _matrix2;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m1.data[i] + _m2.data[i];
				}
				return GW::GReturn::SUCCESS;
			}
			static GReturn SubtractMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m1 = _matrix1;
				GW::MATH::GMATRIXF _m2 = _matrix2;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m1.data[i] - _m2.data[i];
				}
				return GW::GReturn::SUCCESS;
			}
			static GReturn MultiplyMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m1 = _matrix1;
				GW::MATH::GMATRIXF _m2 = _matrix2;

				_outMatrix.data[0] = _m1.data[0] * _m2.data[0] + _m1.data[1] * _m2.data[4] + _m1.data[2] * _m2.data[8] + _m1.data[3] * _m2.data[12];
				_outMatrix.data[1] = _m1.data[0] * _m2.data[1] + _m1.data[1] * _m2.data[5] + _m1.data[2] * _m2.data[9] + _m1.data[3] * _m2.data[13];
				_outMatrix.data[2] = _m1.data[0] * _m2.data[2] + _m1.data[1] * _m2.data[6] + _m1.data[2] * _m2.data[10] + _m1.data[3] * _m2.data[14];
				_outMatrix.data[3] = _m1.data[0] * _m2.data[3] + _m1.data[1] * _m2.data[7] + _m1.data[2] * _m2.data[11] + _m1.data[3] * _m2.data[15];

				_outMatrix.data[4] = _m1.data[4] * _m2.data[0] + _m1.data[5] * _m2.data[4] + _m1.data[6] * _m2.data[8] + _m1.data[7] * _m2.data[12];
				_outMatrix.data[5] = _m1.data[4] * _m2.data[1] + _m1.data[5] * _m2.data[5] + _m1.data[6] * _m2.data[9] + _m1.data[7] * _m2.data[13];
				_outMatrix.data[6] = _m1.data[4] * _m2.data[2] + _m1.data[5] * _m2.data[6] + _m1.data[6] * _m2.data[10] + _m1.data[7] * _m2.data[14];
				_outMatrix.data[7] = _m1.data[4] * _m2.data[3] + _m1.data[5] * _m2.data[7] + _m1.data[6] * _m2.data[11] + _m1.data[7] * _m2.data[15];

				_outMatrix.data[8] = _m1.data[8] * _m2.data[0] + _m1.data[9] * _m2.data[4] + _m1.data[10] * _m2.data[8] + _m1.data[11] * _m2.data[12];
				_outMatrix.data[9] = _m1.data[8] * _m2.data[1] + _m1.data[9] * _m2.data[5] + _m1.data[10] * _m2.data[9] + _m1.data[11] * _m2.data[13];
				_outMatrix.data[10] = _m1.data[8] * _m2.data[2] + _m1.data[9] * _m2.data[6] + _m1.data[10] * _m2.data[10] + _m1.data[11] * _m2.data[14];
				_outMatrix.data[11] = _m1.data[8] * _m2.data[3] + _m1.data[9] * _m2.data[7] + _m1.data[10] * _m2.data[11] + _m1.data[11] * _m2.data[15];

				_outMatrix.data[12] = _m1.data[12] * _m2.data[0] + _m1.data[13] * _m2.data[4] + _m1.data[14] * _m2.data[8] + _m1.data[15] * _m2.data[12];
				_outMatrix.data[13] = _m1.data[12] * _m2.data[1] + _m1.data[13] * _m2.data[5] + _m1.data[14] * _m2.data[9] + _m1.data[15] * _m2.data[13];
				_outMatrix.data[14] = _m1.data[12] * _m2.data[2] + _m1.data[13] * _m2.data[6] + _m1.data[14] * _m2.data[10] + _m1.data[15] * _m2.data[14];
				_outMatrix.data[15] = _m1.data[12] * _m2.data[3] + _m1.data[13] * _m2.data[7] + _m1.data[14] * _m2.data[11] + _m1.data[15] * _m2.data[15];

				return GW::GReturn::SUCCESS;
			}

			static GReturn VectorXMatrixF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GVECTORF& _outVector)
			{
				GW::MATH::GVECTORF _v = _vector;

				_outVector.x = _v.data[0] * _matrix.data[0] + _v.data[1] * _matrix.data[4] + _v.data[2] * _matrix.data[8] + _v.data[3] * _matrix.data[12];
				_outVector.y = _v.data[0] * _matrix.data[1] + _v.data[1] * _matrix.data[5] + _v.data[2] * _matrix.data[9] + _v.data[3] * _matrix.data[13];
				_outVector.z = _v.data[0] * _matrix.data[2] + _v.data[1] * _matrix.data[6] + _v.data[2] * _matrix.data[10] + _v.data[3] * _matrix.data[14];
				_outVector.w = _v.data[0] * _matrix.data[3] + _v.data[1] * _matrix.data[7] + _v.data[2] * _matrix.data[11] + _v.data[3] * _matrix.data[15];

				return GW::GReturn::SUCCESS;
			}
			static GReturn ConvertQuaternionF(GW::MATH::GQUATERNIONF _quaternion, GW::MATH::GMATRIXF& _outMatrix)
			{
				float xx2 = 2 * _quaternion.x * _quaternion.x;
				float yy2 = 2 * _quaternion.y * _quaternion.y;
				float zz2 = 2 * _quaternion.z * _quaternion.z;

				float xy2 = 2 * _quaternion.x * _quaternion.y;
				float xz2 = 2 * _quaternion.x * _quaternion.z;
				float yz2 = 2 * _quaternion.y * _quaternion.z;

				float wx2 = 2 * _quaternion.w * _quaternion.x;
				float wy2 = 2 * _quaternion.w * _quaternion.y;
				float wz2 = 2 * _quaternion.w * _quaternion.z;

				_outMatrix.data[0] = 1.0f - yy2 - zz2;
				_outMatrix.data[1] = xy2 - wz2;
				_outMatrix.data[2] = xz2 + wy2;
				_outMatrix.data[3] = 0.0f;
				_outMatrix.data[4] = xy2 + wz2;
				_outMatrix.data[5] = 1.0f - xx2 - zz2;
				_outMatrix.data[6] = yz2 - wx2;
				_outMatrix.data[7] = 0.0f;
				_outMatrix.data[8] = xz2 - wy2;
				_outMatrix.data[9] = yz2 + wx2;
				_outMatrix.data[10] = 1.0f - xx2 - yy2;
				_outMatrix.data[11] = 0.0f;
				_outMatrix.data[12] = 0.0f;
				_outMatrix.data[13] = 0.0f;
				_outMatrix.data[14] = 0.0f;
				_outMatrix.data[15] = 1.0f;

				return GW::GReturn::SUCCESS;
			}
			static GReturn MultiplyNumF(GW::MATH::GMATRIXF _matrix, float _scalar, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m = _matrix;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m.data[i] * _scalar;
				}
				return GW::GReturn::SUCCESS;
			}

			static GReturn DeterminantF(GW::MATH::GMATRIXF _matrix, float& _outValue)
			{
				float a0 = _matrix.data[0] * _matrix.data[5] - _matrix.data[1] * _matrix.data[4];
				float a1 = _matrix.data[0] * _matrix.data[6] - _matrix.data[2] * _matrix.data[4];
				float a2 = _matrix.data[0] * _matrix.data[7] - _matrix.data[3] * _matrix.data[4];
				float a3 = _matrix.data[1] * _matrix.data[6] - _matrix.data[2] * _matrix.data[5];
				float a4 = _matrix.data[1] * _matrix.data[7] - _matrix.data[3] * _matrix.data[5];
				float a5 = _matrix.data[2] * _matrix.data[7] - _matrix.data[3] * _matrix.data[6];
				float b0 = _matrix.data[8] * _matrix.data[13] - _matrix.data[9] * _matrix.data[12];
				float b1 = _matrix.data[8] * _matrix.data[14] - _matrix.data[10] * _matrix.data[12];
				float b2 = _matrix.data[8] * _matrix.data[15] - _matrix.data[11] * _matrix.data[12];
				float b3 = _matrix.data[9] * _matrix.data[14] - _matrix.data[10] * _matrix.data[13];
				float b4 = _matrix.data[9] * _matrix.data[15] - _matrix.data[11] * _matrix.data[13];
				float b5 = _matrix.data[10] * _matrix.data[15] - _matrix.data[11] * _matrix.data[14];

				_outValue = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

				return GW::GReturn::SUCCESS;
			}
			static GReturn TransposeF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m = _matrix;

				_outMatrix.data[0] = _m.data[0];
				_outMatrix.data[1] = _m.data[4];
				_outMatrix.data[2] = _m.data[8];
				_outMatrix.data[3] = _m.data[12];
				_outMatrix.data[4] = _m.data[1];
				_outMatrix.data[5] = _m.data[5];
				_outMatrix.data[6] = _m.data[9];
				_outMatrix.data[7] = _m.data[13];
				_outMatrix.data[8] = _m.data[2];
				_outMatrix.data[9] = _m.data[6];
				_outMatrix.data[10] = _m.data[10];
				_outMatrix.data[11] = _m.data[14];
				_outMatrix.data[12] = _m.data[3];
				_outMatrix.data[13] = _m.data[7];
				_outMatrix.data[14] = _m.data[11];
				_outMatrix.data[15] = _m.data[15];
				return GW::GReturn::SUCCESS;
			}
			static GReturn InverseF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m = _matrix;

				float det;
				float a0 = _m.data[0] * _m.data[5] - _m.data[1] * _m.data[4];
				float a1 = _m.data[0] * _m.data[6] - _m.data[2] * _m.data[4];
				float a2 = _m.data[0] * _m.data[7] - _m.data[3] * _m.data[4];
				float a3 = _m.data[1] * _m.data[6] - _m.data[2] * _m.data[5];
				float a4 = _m.data[1] * _m.data[7] - _m.data[3] * _m.data[5];
				float a5 = _m.data[2] * _m.data[7] - _m.data[3] * _m.data[6];
				float b0 = _m.data[8] * _m.data[13] - _m.data[9] * _m.data[12];
				float b1 = _m.data[8] * _m.data[14] - _m.data[10] * _m.data[12];
				float b2 = _m.data[8] * _m.data[15] - _m.data[11] * _m.data[12];
				float b3 = _m.data[9] * _m.data[14] - _m.data[10] * _m.data[13];
				float b4 = _m.data[9] * _m.data[15] - _m.data[11] * _m.data[13];
				float b5 = _m.data[10] * _m.data[15] - _m.data[11] * _m.data[14];

				det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
				if(G_WITHIN_STANDARD_DEVIATION_F(det, 0.0f))
				{
					return GW::GReturn::FAILURE;
				}
				_outMatrix.data[0] = _m.data[5] * b5 - _m.data[6] * b4 + _m.data[7] * b3;
				_outMatrix.data[1] = -_m.data[1] * b5 + _m.data[2] * b4 - _m.data[3] * b3;
				_outMatrix.data[2] = _m.data[13] * a5 - _m.data[14] * a4 + _m.data[15] * a3;
				_outMatrix.data[3] = -_m.data[9] * a5 + _m.data[10] * a4 - _m.data[11] * a3;

				_outMatrix.data[4] = -_m.data[4] * b5 + _m.data[6] * b2 - _m.data[7] * b1;
				_outMatrix.data[5] = _m.data[0] * b5 - _m.data[2] * b2 + _m.data[3] * b1;
				_outMatrix.data[6] = -_m.data[12] * a5 + _m.data[14] * a2 - _m.data[15] * a1;
				_outMatrix.data[7] = _m.data[8] * a5 - _m.data[10] * a2 + _m.data[11] * a1;

				_outMatrix.data[8] = _m.data[4] * b4 - _m.data[5] * b2 + _m.data[7] * b0;
				_outMatrix.data[9] = -_m.data[0] * b4 + _m.data[1] * b2 - _m.data[3] * b0;
				_outMatrix.data[10] = _m.data[12] * a4 - _m.data[13] * a2 + _m.data[15] * a0;
				_outMatrix.data[11] = -_m.data[8] * a4 + _m.data[9] * a2 - _m.data[11] * a0;

				_outMatrix.data[12] = -_m.data[4] * b3 + _m.data[5] * b1 - _m.data[6] * b0;
				_outMatrix.data[13] = _m.data[0] * b3 - _m.data[1] * b1 + _m.data[2] * b0;
				_outMatrix.data[14] = -_m.data[12] * a3 + _m.data[13] * a1 - _m.data[14] * a0;
				_outMatrix.data[15] = _m.data[8] * a3 - _m.data[9] * a1 + _m.data[10] * a0;

				MultiplyNumF(_outMatrix, 1.0f / det, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn IdentityF(GW::MATH::GMATRIXF& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixF;
				return GW::GReturn::SUCCESS;
			}

			static GReturn GetRotationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GQUATERNIONF& _outQuaternion)
			{
				GW::MATH::GMATRIXF _m = _matrix;

				float det;
				float sx = sqrt(_m.data[0] * _m.data[0] + _m.data[4] * _m.data[4] + _m.data[8] * _m.data[8]);
				float sy = sqrt(_m.data[1] * _m.data[1] + _m.data[5] * _m.data[5] + _m.data[9] * _m.data[9]);
				float sz = sqrt(_m.data[2] * _m.data[2] + _m.data[6] * _m.data[6] + _m.data[10] * _m.data[10]);
				DeterminantF(_m, det);

				if(G_WITHIN_STANDARD_DEVIATION_F(det, 0.0f))
					return GW::GReturn::FAILURE;

				if(det < 0)
				{
					sx = -sx;
				}

				GW::MATH::GMATRIXF Rotation = _m;
				Rotation.data[0] /= sx;
				Rotation.data[4] /= sx;
				Rotation.data[8] /= sx;
				Rotation.data[1] /= sy;
				Rotation.data[5] /= sy;
				Rotation.data[9] /= sy;
				Rotation.data[2] /= sz;
				Rotation.data[6] /= sz;
				Rotation.data[10] /= sz;


				float trace = Rotation.data[0] + Rotation.data[5] + Rotation.data[10] + 1;

				if(trace > G_EPSILON_F)
				{
					float s = 0.5f / sqrt(trace);
					_outQuaternion.x = (Rotation.row3.y - Rotation.row2.z) * s;
					_outQuaternion.y = (Rotation.row1.z - Rotation.row3.x) * s;
					_outQuaternion.z = (Rotation.row2.x - Rotation.row1.y) * s;
					_outQuaternion.w = 0.25f / s;
				}
				else
				{
					if(Rotation.row1.x > Rotation.row2.y&& Rotation.row1.x > Rotation.row3.z)
					{
						float s = 0.5f / sqrt(1.0f + Rotation.row1.x - Rotation.row2.y - Rotation.row3.z);
						_outQuaternion.x = 0.25f / s;
						_outQuaternion.y = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.z = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.w = (Rotation.row3.y - Rotation.row2.z) * s;
					}
					else if(Rotation.row2.y > Rotation.row3.z)
					{
						float s = 0.5f / sqrt(1.0f + Rotation.row2.y - Rotation.row1.x - Rotation.row3.z);
						_outQuaternion.x = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.y = 0.25f / s;
						_outQuaternion.z = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.w = (Rotation.row1.z - Rotation.row3.x) * s;
					}
					else
					{
						float s = 0.5f / sqrt(1.0f + Rotation.row3.z - Rotation.row1.x - Rotation.row2.y);
						_outQuaternion.x = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.y = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.z = 0.25f / s;
						_outQuaternion.w = (Rotation.row2.x - Rotation.row1.y) * s;
					}
				}

				return GW::GReturn::SUCCESS;
			}
			static GReturn GetTranslationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector)
			{
				_outVector = _matrix.row4;
				_outVector.w = 0;
				return GW::GReturn::SUCCESS;
			}
			static GReturn GetScaleF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector)
			{
				float det;

				_outVector.x = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[4] * _matrix.data[4] + _matrix.data[8] * _matrix.data[8]);
				_outVector.y = sqrt(_matrix.data[1] * _matrix.data[1] + _matrix.data[5] * _matrix.data[5] + _matrix.data[9] * _matrix.data[9]);
				_outVector.z = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[6] * _matrix.data[6] + _matrix.data[10] * _matrix.data[10]);
				_outVector.w = 0;

				DeterminantF(_matrix, det);
				if(det < 0)
				{
					_outVector.x = -_outVector.x;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateXGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix)
			{
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[5] = c;
				Rotation.data[6] = s;
				Rotation.data[9] = -s;
				Rotation.data[10] = c;

				// store translation
				GW::MATH::GVECTORF translation = _matrix.row4;

				MultiplyMatrixF(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateXLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) {
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[5] = c;
				Rotation.data[6] = s;
				Rotation.data[9] = -s;
				Rotation.data[10] = c;

				MultiplyMatrixF(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateYGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix)
			{
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[0] = c;
				Rotation.data[2] = -s;
				Rotation.data[8] = s;
				Rotation.data[10] = c;

				// store translation
				GW::MATH::GVECTORF translation = _matrix.row4;

				MultiplyMatrixF(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateYLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) {
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[0] = c;
				Rotation.data[2] = -s;
				Rotation.data[8] = s;
				Rotation.data[10] = c;

				MultiplyMatrixF(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateZGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix)
			{
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[0] = c;
				Rotation.data[1] = s;
				Rotation.data[4] = -s;
				Rotation.data[5] = c;

				// store translation
				GW::MATH::GVECTORF translation = _matrix.row4;

				MultiplyMatrixF(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateZLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) {
				float c = cos(_radian);
				float s = sin(_radian);
				GW::MATH::GMATRIXF Rotation = GW::MATH::GIdentityMatrixF;
				Rotation.data[0] = c;
				Rotation.data[1] = s;
				Rotation.data[4] = -s;
				Rotation.data[5] = c;

				MultiplyMatrixF(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotationYawPitchRollF(float _yaw, float _pitch, float _roll, GW::MATH::GMATRIXF& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixF;
				GW::MATH::GMATRIXF RotationX;
				GW::MATH::GMATRIXF RotationY;
				GW::MATH::GMATRIXF RotationZ;
				GW::MATH::GMATRIXF reuslt;
				RotateZGlobalF(GW::MATH::GIdentityMatrixF, _roll, RotationZ);
				RotateXGlobalF(GW::MATH::GIdentityMatrixF, _pitch, RotationX);
				RotateYGlobalF(GW::MATH::GIdentityMatrixF, _yaw, RotationY);

				MultiplyMatrixF(RotationX, RotationY, reuslt);
				MultiplyMatrixF(RotationZ, reuslt, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotationByVectorF(GW::MATH::GVECTORF _vector, float _radian, GW::MATH::GMATRIXF& _outMatrix)
			{
				float x = _vector.x;
				float y = _vector.y;
				float z = _vector.z;

				float magnitude = x * x + y * y + z * z;
				if(!G_WITHIN_STANDARD_DEVIATION_F(magnitude, 1))
				{
					magnitude = sqrt(magnitude);
					if(!G_WITHIN_STANDARD_DEVIATION_F(magnitude, 0))
					{
						magnitude = 1.0f / magnitude;
						x = x * magnitude;
						y = y * magnitude;
						z = z * magnitude;
					}
					else return GW::GReturn::FAILURE;
				}
				float c = cos(_radian);
				float s = sin(_radian);

				float t = 1.0f - c;
				float tx = t * x;
				float ty = t * y;
				float tz = t * z;
				float txy = tx * y;
				float txz = tx * z;
				float tyz = ty * z;
				float sx = s * x;
				float sy = s * y;
				float sz = s * z;

				_outMatrix.data[0] = c + tx * x;
				_outMatrix.data[1] = txy + sz;
				_outMatrix.data[2] = txz - sy;
				_outMatrix.data[3] = 0.0f;

				_outMatrix.data[4] = txy - sz;
				_outMatrix.data[5] = c + ty * y;
				_outMatrix.data[6] = tyz + sx;
				_outMatrix.data[7] = 0.0f;

				_outMatrix.data[8] = txz + sy;
				_outMatrix.data[9] = tyz - sx;
				_outMatrix.data[10] = c + tz * z;
				_outMatrix.data[11] = 0.0f;

				_outMatrix.data[12] = 0.0f;
				_outMatrix.data[13] = 0.0f;
				_outMatrix.data[14] = 0.0f;
				_outMatrix.data[15] = 1.0f;

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) {
				GW::MATH::GMATRIXF Translation = GW::MATH::GIdentityMatrixF;
				Translation.data[12] = _vector.x;
				Translation.data[13] = _vector.y;
				Translation.data[14] = _vector.z;

				MultiplyMatrixF(_matrix, Translation, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn TranslateLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF Translation = GW::MATH::GIdentityMatrixF;
				Translation.data[12] = _vector.x;
				Translation.data[13] = _vector.y;
				Translation.data[14] = _vector.z;

				MultiplyMatrixF(Translation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF Scale = GW::MATH::GIdentityMatrixF;
				Scale.data[0] = _vector.x;
				Scale.data[5] = _vector.y;
				Scale.data[10] = _vector.z;

				// store translation
				GW::MATH::GVECTORF translation = _matrix.row4;

				MultiplyMatrixF(_matrix, Scale, _outMatrix);

				// restore translation
				_outMatrix.row4 = translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ScaleLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) {
				GW::MATH::GMATRIXF Scale = GW::MATH::GIdentityMatrixF;
				Scale.data[0] = _vector.x;
				Scale.data[5] = _vector.y;
				Scale.data[10] = _vector.z;

				MultiplyMatrixF(Scale, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn LerpF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, float _ratio, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF _m1 = _matrix1;
				GW::MATH::GMATRIXF _m2 = _matrix2;

				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = G_LERP(_m1.data[i], _m2.data[i], _ratio);
				}
				return GW::GReturn::SUCCESS;
			}

			static GReturn ProjectionDirectXLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY / 2.0f);
				float z = _zf / (_zf - _zn);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = 1.0f;
				_outMatrix.row4.data[2] = -_zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionDirectXRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY * 0.5f);
				float z = _zf / (_zn - _zf);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = -1.0f;
				_outMatrix.row4.data[2] = _zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionOpenGLRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY / 2.0f);
				float z = _zf - _zn;

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = -((_zf + _zn) / z);
				_outMatrix.row3.data[3] = -1.0f;
				_outMatrix.row4.data[2] = -((2.0f * _zf * _zn) / z);

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionOpenGLLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY / 2.0f);
				float z = _zn - _zf; // note these are also swapped for LH in OGL

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = -((_zf + _zn) / z);
				_outMatrix.row3.data[3] = 1.0f;
				_outMatrix.row4.data[2] = ((2.0f * _zf * _zn) / z);

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionVulkanLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY * 0.5f);
				float z = _zf / (_zf - _zn);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = -yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = 1.0f;
				_outMatrix.row4.data[2] = -_zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionVulkanRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_F(_aspect, 0.0f)) return GW::GReturn::FAILURE;

				float yScale = 1.0f / tanf(_fovY * 0.5f);
				float z = _zf / (_zn - _zf);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = -yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = -1.0f;
				_outMatrix.row4.data[2] = _zn * z;

				return GW::GReturn::SUCCESS;
			}

			static GReturn LookAtLHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixF;
				GW::MATH::GVECTORF temp;
				GW::MATH::GVECTORF camForward;	//zaxis
				GW::MATH::GVECTORF camRight;		//xaxis
				GW::MATH::GVECTORF camUp;			//yaxis
				float magnitudeX;
				float magnitudeZ;

				//camForward = targetPosition - eyePosition 
				camForward.x = _at.x - _eye.x;
				camForward.y = _at.y - _eye.y;
				camForward.z = _at.z - _eye.z;

				magnitudeZ = sqrt((camForward.x * camForward.x) + (camForward.y * camForward.y) + (camForward.z * camForward.z));
				if(G_WITHIN_STANDARD_DEVIATION_F(magnitudeZ, 0.0f)) return GW::GReturn::FAILURE;

				//normalize camForward 
				camForward.x /= magnitudeZ;
				camForward.y /= magnitudeZ;
				camForward.z /= magnitudeZ;

				//camRight = cross(worldUp, camForward)
				camRight.x = (_up.y * camForward.z) - (_up.z * camForward.y);
				camRight.y = (_up.z * camForward.x) - (_up.x * camForward.z);
				camRight.z = (_up.x * camForward.y) - (_up.y * camForward.x);

				magnitudeX = sqrt((camRight.x * camRight.x) + (camRight.y * camRight.y) + (camRight.z * camRight.z));
				if(G_WITHIN_STANDARD_DEVIATION_F(magnitudeX, 0.0f)) return GW::GReturn::FAILURE;

				//normalize camRight
				camRight.x /= magnitudeX;
				camRight.y /= magnitudeX;
				camRight.z /= magnitudeX;

				//camUp = cross(camForward, camRight)
				camUp.x = (camForward.y * camRight.z) - (camForward.z * camRight.y);
				camUp.y = (camForward.z * camRight.x) - (camForward.x * camRight.z);
				camUp.z = (camForward.x * camRight.y) - (camForward.y * camRight.x);

				//invert our position by our orientation (to turn this world matrix into a view matrix) 
				temp.x = camRight.x * _eye.x + camRight.y * _eye.y + camRight.z * _eye.z;
				temp.y = camUp.x * _eye.x + camUp.y * _eye.y + camUp.z * _eye.z;
				temp.z = camForward.x * _eye.x + camForward.y * _eye.y + camForward.z * _eye.z;

				//compose results into matrix
				_outMatrix.data[0] = camRight.x;
				_outMatrix.data[4] = camRight.y;
				_outMatrix.data[8] = camRight.z;
				_outMatrix.data[12] = -temp.x;

				_outMatrix.data[1] = camUp.x;
				_outMatrix.data[5] = camUp.y;
				_outMatrix.data[9] = camUp.z;
				_outMatrix.data[13] = -temp.y;

				_outMatrix.data[2] = camForward.x;
				_outMatrix.data[6] = camForward.y;
				_outMatrix.data[10] = camForward.z;
				_outMatrix.data[14] = -temp.z;
				return GW::GReturn::SUCCESS;
			}
			static GReturn LookAtRHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixF;
				GW::MATH::GVECTORF temp;
				GW::MATH::GVECTORF camForward;	//zaxis
				GW::MATH::GVECTORF camRight;		//xaxis
				GW::MATH::GVECTORF camUp;			//yaxis
				float magnitudeX;
				float magnitudeZ;

				//camForward = eyePosition - targetPosition
				camForward.x = _eye.x - _at.x;
				camForward.y = _eye.y - _at.y;
				camForward.z = _eye.z - _at.z;

				magnitudeZ = sqrt((camForward.x * camForward.x) + (camForward.y * camForward.y) + (camForward.z * camForward.z));
				if (G_WITHIN_STANDARD_DEVIATION_F(magnitudeZ, 0.0f)) return GW::GReturn::FAILURE;

				//normalize camForward 
				camForward.x /= magnitudeZ;
				camForward.y /= magnitudeZ;
				camForward.z /= magnitudeZ;

				//camRight = cross(worldUp, camForward)
				camRight.x = (_up.y * camForward.z) - (_up.z * camForward.y);
				camRight.y = (_up.z * camForward.x) - (_up.x * camForward.z);
				camRight.z = (_up.x * camForward.y) - (_up.y * camForward.x);

				magnitudeX = sqrt((camRight.x * camRight.x) + (camRight.y * camRight.y) + (camRight.z * camRight.z));
				if (G_WITHIN_STANDARD_DEVIATION_F(magnitudeX, 0.0f)) return GW::GReturn::FAILURE;

				//normalize camRight
				camRight.x /= magnitudeX;
				camRight.y /= magnitudeX;
				camRight.z /= magnitudeX;

				//camUp = cross(camForward, camRight)
				camUp.x = (camForward.y * camRight.z) - (camForward.z * camRight.y);
				camUp.y = (camForward.z * camRight.x) - (camForward.x * camRight.z);
				camUp.z = (camForward.x * camRight.y) - (camForward.y * camRight.x);

				//invert our position by our orientation (to turn this world matrix into a view matrix) 
				temp.x = camRight.x * _eye.x + camRight.y * _eye.y + camRight.z * _eye.z;
				temp.y = camUp.x * _eye.x + camUp.y * _eye.y + camUp.z * _eye.z;
				temp.z = camForward.x * _eye.x + camForward.y * _eye.y + camForward.z * _eye.z;

				//compose results into matrix
				_outMatrix.data[0] = camRight.x;
				_outMatrix.data[4] = camRight.y;
				_outMatrix.data[8] = camRight.z;
				_outMatrix.data[12] = -temp.x;

				_outMatrix.data[1] = camUp.x;
				_outMatrix.data[5] = camUp.y;
				_outMatrix.data[9] = camUp.z;
				_outMatrix.data[13] = -temp.y;

				_outMatrix.data[2] = camForward.x;
				_outMatrix.data[6] = camForward.y;
				_outMatrix.data[10] = camForward.z;
				_outMatrix.data[14] = -temp.z;
				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelativeF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix)
			{
				GW::MATH::GMATRIXF inverseMatrix2; 
				GW::GReturn retVal = InverseF(_matrix2, inverseMatrix2); //we need to return a failure if the inverse fails

				MultiplyMatrixF(_matrix1, inverseMatrix2, _outMatrix);

				return retVal;
			}
			static GReturn MakeSeparateF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) 
			{
				MultiplyMatrixF(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Upgrade(GW::MATH::GMATRIXF _matrixF, GW::MATH::GMATRIXD& _outMatrixD) 
			{
				for (int i = 0; i < 16; i++)
				{
					_outMatrixD.data[i] = static_cast<double>(_matrixF.data[i]);
				}

				return GReturn::SUCCESS;
			}


			// Doubles
			static GReturn AddMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m1 = _matrix1;
				GW::MATH::GMATRIXD _m2 = _matrix2;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m1.data[i] + _m2.data[i];
				}
				return GW::GReturn::SUCCESS;
			}
			static GReturn SubtractMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m1 = _matrix1;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m1.data[i] - _matrix2.data[i];
				}
				return GW::GReturn::SUCCESS;
			}
			static GReturn MultiplyMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m1 = _matrix1;
				GW::MATH::GMATRIXD _m2 = _matrix2;
				_outMatrix.data[0] = _m1.data[0] * _m2.data[0] + _m1.data[1] * _m2.data[4] + _m1.data[2] * _m2.data[8] + _m1.data[3] * _m2.data[12];
				_outMatrix.data[1] = _m1.data[0] * _m2.data[1] + _m1.data[1] * _m2.data[5] + _m1.data[2] * _m2.data[9] + _m1.data[3] * _m2.data[13];
				_outMatrix.data[2] = _m1.data[0] * _m2.data[2] + _m1.data[1] * _m2.data[6] + _m1.data[2] * _m2.data[10] + _m1.data[3] * _m2.data[14];
				_outMatrix.data[3] = _m1.data[0] * _m2.data[3] + _m1.data[1] * _m2.data[7] + _m1.data[2] * _m2.data[11] + _m1.data[3] * _m2.data[15];

				_outMatrix.data[4] = _m1.data[4] * _m2.data[0] + _m1.data[5] * _m2.data[4] + _m1.data[6] * _m2.data[8] + _m1.data[7] * _m2.data[12];
				_outMatrix.data[5] = _m1.data[4] * _m2.data[1] + _m1.data[5] * _m2.data[5] + _m1.data[6] * _m2.data[9] + _m1.data[7] * _m2.data[13];
				_outMatrix.data[6] = _m1.data[4] * _m2.data[2] + _m1.data[5] * _m2.data[6] + _m1.data[6] * _m2.data[10] + _m1.data[7] * _m2.data[14];
				_outMatrix.data[7] = _m1.data[4] * _m2.data[3] + _m1.data[5] * _m2.data[7] + _m1.data[6] * _m2.data[11] + _m1.data[7] * _m2.data[15];

				_outMatrix.data[8] = _m1.data[8] * _m2.data[0] + _m1.data[9] * _m2.data[4] + _m1.data[10] * _m2.data[8] + _m1.data[11] * _m2.data[12];
				_outMatrix.data[9] = _m1.data[8] * _m2.data[1] + _m1.data[9] * _m2.data[5] + _m1.data[10] * _m2.data[9] + _m1.data[11] * _m2.data[13];
				_outMatrix.data[10] = _m1.data[8] * _m2.data[2] + _m1.data[9] * _m2.data[6] + _m1.data[10] * _m2.data[10] + _m1.data[11] * _m2.data[14];
				_outMatrix.data[11] = _m1.data[8] * _m2.data[3] + _m1.data[9] * _m2.data[7] + _m1.data[10] * _m2.data[11] + _m1.data[11] * _m2.data[15];

				_outMatrix.data[12] = _m1.data[12] * _m2.data[0] + _m1.data[13] * _m2.data[4] + _m1.data[14] * _m2.data[8] + _m1.data[15] * _m2.data[12];
				_outMatrix.data[13] = _m1.data[12] * _m2.data[1] + _m1.data[13] * _m2.data[5] + _m1.data[14] * _m2.data[9] + _m1.data[15] * _m2.data[13];
				_outMatrix.data[14] = _m1.data[12] * _m2.data[2] + _m1.data[13] * _m2.data[6] + _m1.data[14] * _m2.data[10] + _m1.data[15] * _m2.data[14];
				_outMatrix.data[15] = _m1.data[12] * _m2.data[3] + _m1.data[13] * _m2.data[7] + _m1.data[14] * _m2.data[11] + _m1.data[15] * _m2.data[15];

				return GW::GReturn::SUCCESS;
			}

			static GReturn VectorXMatrixD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GVECTORD& _outVector)
			{
				GW::MATH::GMATRIXD _m = _matrix;
				_outVector.x = _vector.data[0] * _m.data[0] + _vector.data[1] * _m.data[4] + _vector.data[2] * _m.data[8] + _vector.data[3] * _m.data[12];
				_outVector.y = _vector.data[0] * _m.data[1] + _vector.data[1] * _m.data[5] + _vector.data[2] * _m.data[9] + _vector.data[3] * _m.data[13];
				_outVector.z = _vector.data[0] * _m.data[2] + _vector.data[1] * _m.data[6] + _vector.data[2] * _m.data[10] + _vector.data[3] * _m.data[14];
				_outVector.w = _vector.data[0] * _m.data[3] + _vector.data[1] * _m.data[7] + _vector.data[2] * _m.data[11] + _vector.data[3] * _m.data[15];

				return GW::GReturn::SUCCESS;
			}
			static GReturn ConvertQuaternionD(GW::MATH::GQUATERNIOND _quaternion, GW::MATH::GMATRIXD& _outMatrix)
			{
				double xx2 = 2 * _quaternion.x * _quaternion.x;
				double yy2 = 2 * _quaternion.y * _quaternion.y;
				double zz2 = 2 * _quaternion.z * _quaternion.z;

				double xy2 = 2 * _quaternion.x * _quaternion.y;
				double xz2 = 2 * _quaternion.x * _quaternion.z;
				double yz2 = 2 * _quaternion.y * _quaternion.z;

				double wx2 = 2 * _quaternion.w * _quaternion.x;
				double wy2 = 2 * _quaternion.w * _quaternion.y;
				double wz2 = 2 * _quaternion.w * _quaternion.z;

				_outMatrix.data[0] = 1.0f - yy2 - zz2;
				_outMatrix.data[1] = xy2 - wz2;
				_outMatrix.data[2] = xz2 + wy2;
				_outMatrix.data[3] = 0.0f;
				_outMatrix.data[4] = xy2 + wz2;
				_outMatrix.data[5] = 1.0f - xx2 - zz2;
				_outMatrix.data[6] = yz2 - wx2;
				_outMatrix.data[7] = 0.0f;
				_outMatrix.data[8] = xz2 - wy2;
				_outMatrix.data[9] = yz2 + wx2;
				_outMatrix.data[10] = 1.0f - xx2 - yy2;
				_outMatrix.data[11] = 0.0f;
				_outMatrix.data[12] = 0.0f;
				_outMatrix.data[13] = 0.0f;
				_outMatrix.data[14] = 0.0f;
				_outMatrix.data[15] = 1.0f;

				return GW::GReturn::SUCCESS;
			}
			static GReturn MultiplyNumD(GW::MATH::GMATRIXD _matrix, double _scalar, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m = _matrix;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = _m.data[i] * _scalar;
				}
				return GW::GReturn::SUCCESS;
			}

			static GReturn DeterminantD(GW::MATH::GMATRIXD _matrix, double& _outValue)
			{
				double a0 = _matrix.data[0] * _matrix.data[5] - _matrix.data[1] * _matrix.data[4];
				double a1 = _matrix.data[0] * _matrix.data[6] - _matrix.data[2] * _matrix.data[4];
				double a2 = _matrix.data[0] * _matrix.data[7] - _matrix.data[3] * _matrix.data[4];
				double a3 = _matrix.data[1] * _matrix.data[6] - _matrix.data[2] * _matrix.data[5];
				double a4 = _matrix.data[1] * _matrix.data[7] - _matrix.data[3] * _matrix.data[5];
				double a5 = _matrix.data[2] * _matrix.data[7] - _matrix.data[3] * _matrix.data[6];
				double b0 = _matrix.data[8] * _matrix.data[13] - _matrix.data[9] * _matrix.data[12];
				double b1 = _matrix.data[8] * _matrix.data[14] - _matrix.data[10] * _matrix.data[12];
				double b2 = _matrix.data[8] * _matrix.data[15] - _matrix.data[11] * _matrix.data[12];
				double b3 = _matrix.data[9] * _matrix.data[14] - _matrix.data[10] * _matrix.data[13];
				double b4 = _matrix.data[9] * _matrix.data[15] - _matrix.data[11] * _matrix.data[13];
				double b5 = _matrix.data[10] * _matrix.data[15] - _matrix.data[11] * _matrix.data[14];

				_outValue = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

				return GW::GReturn::SUCCESS;
			}
			static GReturn TransposeD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m = _matrix;
				_outMatrix.data[0] = _m.data[0];
				_outMatrix.data[1] = _m.data[4];
				_outMatrix.data[2] = _m.data[8];
				_outMatrix.data[3] = _m.data[12];
				_outMatrix.data[4] = _m.data[1];
				_outMatrix.data[5] = _m.data[5];
				_outMatrix.data[6] = _m.data[9];
				_outMatrix.data[7] = _m.data[13];
				_outMatrix.data[8] = _m.data[2];
				_outMatrix.data[9] = _m.data[6];
				_outMatrix.data[10] = _m.data[10];
				_outMatrix.data[11] = _m.data[14];
				_outMatrix.data[12] = _m.data[3];
				_outMatrix.data[13] = _m.data[7];
				_outMatrix.data[14] = _m.data[11];
				_outMatrix.data[15] = _m.data[15];
				return GW::GReturn::SUCCESS;
			}
			static GReturn InverseD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m = _matrix;
				double det;
				double a0 = _m.data[0] * _m.data[5] - _m.data[1] * _m.data[4];
				double a1 = _m.data[0] * _m.data[6] - _m.data[2] * _m.data[4];
				double a2 = _m.data[0] * _m.data[7] - _m.data[3] * _m.data[4];
				double a3 = _m.data[1] * _m.data[6] - _m.data[2] * _m.data[5];
				double a4 = _m.data[1] * _m.data[7] - _m.data[3] * _m.data[5];
				double a5 = _m.data[2] * _m.data[7] - _m.data[3] * _m.data[6];
				double b0 = _m.data[8] * _m.data[13] - _m.data[9] * _m.data[12];
				double b1 = _m.data[8] * _m.data[14] - _m.data[10] * _m.data[12];
				double b2 = _m.data[8] * _m.data[15] - _m.data[11] * _m.data[12];
				double b3 = _m.data[9] * _m.data[14] - _m.data[10] * _m.data[13];
				double b4 = _m.data[9] * _m.data[15] - _m.data[11] * _m.data[13];
				double b5 = _m.data[10] * _m.data[15] - _m.data[11] * _m.data[14];

				det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
				if(G_WITHIN_STANDARD_DEVIATION_D(det, 0))
				{
					return GW::GReturn::FAILURE;
				}
				_outMatrix.data[0] = _m.data[5] * b5 - _m.data[6] * b4 + _m.data[7] * b3;
				_outMatrix.data[1] = -_m.data[1] * b5 + _m.data[2] * b4 - _m.data[3] * b3;
				_outMatrix.data[2] = _m.data[13] * a5 - _m.data[14] * a4 + _m.data[15] * a3;
				_outMatrix.data[3] = -_m.data[9] * a5 + _m.data[10] * a4 - _m.data[11] * a3;

				_outMatrix.data[4] = -_m.data[4] * b5 + _m.data[6] * b2 - _m.data[7] * b1;
				_outMatrix.data[5] = _m.data[0] * b5 - _m.data[2] * b2 + _m.data[3] * b1;
				_outMatrix.data[6] = -_m.data[12] * a5 + _m.data[14] * a2 - _m.data[15] * a1;
				_outMatrix.data[7] = _m.data[8] * a5 - _m.data[10] * a2 + _m.data[11] * a1;

				_outMatrix.data[8] = _m.data[4] * b4 - _m.data[5] * b2 + _m.data[7] * b0;
				_outMatrix.data[9] = -_m.data[0] * b4 + _m.data[1] * b2 - _m.data[3] * b0;
				_outMatrix.data[10] = _m.data[12] * a4 - _m.data[13] * a2 + _m.data[15] * a0;
				_outMatrix.data[11] = -_m.data[8] * a4 + _m.data[9] * a2 - _m.data[11] * a0;

				_outMatrix.data[12] = -_m.data[4] * b3 + _m.data[5] * b1 - _m.data[6] * b0;
				_outMatrix.data[13] = _m.data[0] * b3 - _m.data[1] * b1 + _m.data[2] * b0;
				_outMatrix.data[14] = -_m.data[12] * a3 + _m.data[13] * a1 - _m.data[14] * a0;
				_outMatrix.data[15] = _m.data[8] * a3 - _m.data[9] * a1 + _m.data[10] * a0;

				MultiplyNumD(_outMatrix, 1.0f / det, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn IdentityD(GW::MATH::GMATRIXD& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixD;
				return GW::GReturn::SUCCESS;
			}

			static GReturn GetRotationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GQUATERNIOND& _outQuaternion)
			{
				double det;
				double sx = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[4] * _matrix.data[4] + _matrix.data[8] * _matrix.data[8]);
				double sy = sqrt(_matrix.data[1] * _matrix.data[1] + _matrix.data[5] * _matrix.data[5] + _matrix.data[9] * _matrix.data[9]);
				double sz = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[6] * _matrix.data[6] + _matrix.data[10] * _matrix.data[10]);
				DeterminantD(_matrix, det);
				if(G_WITHIN_STANDARD_DEVIATION_D(det, 0))
				{
					return GW::GReturn::FAILURE;
				}
				if(det < 0)
				{
					sx = -sx;
				}
				GW::MATH::GMATRIXD Rotation = _matrix;
				Rotation.data[0] /= sx;
				Rotation.data[1] /= sy;
				Rotation.data[2] /= sz;
				Rotation.data[4] /= sx;
				Rotation.data[5] /= sy;
				Rotation.data[6] /= sz;
				Rotation.data[8] /= sx;
				Rotation.data[9] /= sy;
				Rotation.data[10] /= sz;

				double trace = Rotation.data[0] + Rotation.data[5] + Rotation.data[10] + 1;

				if(trace > G_EPSILON_D)
				{
					double s = 0.5 / sqrt(trace);
					_outQuaternion.x = (Rotation.row3.y - Rotation.row2.z) * s;
					_outQuaternion.y = (Rotation.row1.z - Rotation.row3.x) * s;
					_outQuaternion.z = (Rotation.row2.x - Rotation.row1.y) * s;
					_outQuaternion.w = 0.25 / s;
				}
				else
				{
					if(Rotation.row1.x > Rotation.row2.y&& Rotation.row1.x > Rotation.row3.z)
					{
						double s = 0.5f / sqrt(1.0 + Rotation.row1.x - Rotation.row2.y - Rotation.row3.z);
						_outQuaternion.x = 0.25 / s;
						_outQuaternion.y = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.z = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.w = (Rotation.row3.y - Rotation.row2.z) * s;
					}
					else if(Rotation.row2.y > Rotation.row3.z)
					{
						double s = 0.5 / sqrt(1.0 + Rotation.row2.y - Rotation.row1.x - Rotation.row3.z);
						_outQuaternion.x = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.y = 0.25 / s;
						_outQuaternion.z = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.w = (Rotation.row1.z - Rotation.row3.x) * s;
					}
					else
					{
						double s = 0.5 / sqrt(1.0 + Rotation.row3.z - Rotation.row1.x - Rotation.row2.y);
						_outQuaternion.x = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.y = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.z = 0.25 / s;
						_outQuaternion.w = (Rotation.row2.x - Rotation.row1.y) * s;
					}
				}

				return GW::GReturn::SUCCESS;
			}
			static GReturn GetTranslationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector)
			{
				_outVector = _matrix.row4;
				_outVector.w = 0;
				return GW::GReturn::SUCCESS;
			}
			static GReturn GetScaleD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector)
			{
				double det;

				_outVector.x = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[4] * _matrix.data[4] + _matrix.data[8] * _matrix.data[8]);
				_outVector.y = sqrt(_matrix.data[1] * _matrix.data[1] + _matrix.data[5] * _matrix.data[5] + _matrix.data[9] * _matrix.data[9]);
				_outVector.z = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[6] * _matrix.data[6] + _matrix.data[10] * _matrix.data[10]);
				_outVector.w = 0;

				DeterminantD(_matrix, det);
				if(det < 0)
				{
					_outVector.x = -_outVector.x;
				}

				return GW::GReturn::SUCCESS;
			}

			static GReturn RotateXGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[5] = c;
				Rotation.data[6] = s;
				Rotation.data[9] = -s;
				Rotation.data[10] = c;

				// store translation
				GW::MATH::GVECTORD Translation = _matrix.row4;

				MultiplyMatrixD(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = Translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateXLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[5] = c;
				Rotation.data[6] = s;
				Rotation.data[9] = -s;
				Rotation.data[10] = c;

				MultiplyMatrixD(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateYGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[0] = c;
				Rotation.data[2] = -s;
				Rotation.data[8] = s;
				Rotation.data[10] = c;

				// store translation
				GW::MATH::GVECTORD Translation = _matrix.row4;

				MultiplyMatrixD(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = Translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateYLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[0] = c;
				Rotation.data[2] = -s;
				Rotation.data[8] = s;
				Rotation.data[10] = c;

				MultiplyMatrixD(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateZGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[0] = c;
				Rotation.data[1] = s;
				Rotation.data[4] = -s;
				Rotation.data[5] = c;

				// store translation
				GW::MATH::GVECTORD Translation = _matrix.row4;

				MultiplyMatrixD(_matrix, Rotation, _outMatrix);

				// restore translation
				_outMatrix.row4 = Translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotateZLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double c = cos(_radian);
				double s = sin(_radian);
				GW::MATH::GMATRIXD Rotation = GW::MATH::GIdentityMatrixD;
				Rotation.data[0] = c;
				Rotation.data[1] = s;
				Rotation.data[4] = -s;
				Rotation.data[5] = c;

				MultiplyMatrixD(Rotation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotationYawPitchRollD(double _yaw, double _pitch, double _roll, GW::MATH::GMATRIXD& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixD;
				GW::MATH::GMATRIXD RotationX;
				GW::MATH::GMATRIXD RotationY;
				GW::MATH::GMATRIXD RotationZ;
				GW::MATH::GMATRIXD reuslt;
				RotateZGlobalD(GW::MATH::GIdentityMatrixD, _roll, RotationZ);
				RotateXGlobalD(GW::MATH::GIdentityMatrixD, _pitch, RotationX);
				RotateYGlobalD(GW::MATH::GIdentityMatrixD, _yaw, RotationY);

				MultiplyMatrixD(RotationX, RotationY, reuslt);
				MultiplyMatrixD(RotationZ, reuslt, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn RotationByVectorD(GW::MATH::GVECTORD _vector, double _radian, GW::MATH::GMATRIXD& _outMatrix)
			{
				double x = _vector.x;
				double y = _vector.y;
				double z = _vector.z;

				double magnitude = x * x + y * y + z * z;
				if(!G_WITHIN_STANDARD_DEVIATION_D(magnitude, 1))
				{
					magnitude = sqrt(magnitude);
					if(!G_WITHIN_STANDARD_DEVIATION_D(magnitude, 0))
					{
						magnitude = 1.0 / magnitude;
						x = x * magnitude;
						y = y * magnitude;
						z = z * magnitude;
					}
					else return GW::GReturn::FAILURE;
				}
				double c = cos(_radian);
				double s = sin(_radian);

				double t = 1.0f - c;
				double tx = t * x;
				double ty = t * y;
				double tz = t * z;
				double txy = tx * y;
				double txz = tx * z;
				double tyz = ty * z;
				double sx = s * x;
				double sy = s * y;
				double sz = s * z;

				_outMatrix.data[0] = c + tx * x;
				_outMatrix.data[1] = txy + sz;
				_outMatrix.data[2] = txz - sy;
				_outMatrix.data[3] = 0.0f;

				_outMatrix.data[4] = txy - sz;
				_outMatrix.data[5] = c + ty * y;
				_outMatrix.data[6] = tyz + sx;
				_outMatrix.data[7] = 0.0f;

				_outMatrix.data[8] = txz + sy;
				_outMatrix.data[9] = tyz - sx;
				_outMatrix.data[10] = c + tz * z;
				_outMatrix.data[11] = 0.0f;

				_outMatrix.data[12] = 0.0f;
				_outMatrix.data[13] = 0.0f;
				_outMatrix.data[14] = 0.0f;
				_outMatrix.data[15] = 1.0f;

				return GW::GReturn::SUCCESS;
			}

			static GReturn TranslateGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD Translation = GW::MATH::GIdentityMatrixD;

				Translation.data[12] = _vector.x;
				Translation.data[13] = _vector.y;
				Translation.data[14] = _vector.z;

				MultiplyMatrixD(_matrix, Translation, _outMatrix);

				return GW::GReturn::SUCCESS;
			}
			static GReturn TranslateLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD Translation = GW::MATH::GIdentityMatrixD;

				Translation.data[12] = _vector.x;
				Translation.data[13] = _vector.y;
				Translation.data[14] = _vector.z;

				MultiplyMatrixD(Translation, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn ScaleGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD Scale = GW::MATH::GIdentityMatrixD;
				Scale.data[0] = _vector.x;
				Scale.data[5] = _vector.y;
				Scale.data[10] = _vector.z;

				// store translation
				GW::MATH::GVECTORD Translation = _matrix.row4;

				MultiplyMatrixD(_matrix, Scale, _outMatrix);

				// restore translation
				_outMatrix.row4 = Translation;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ScaleLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD Scale = GW::MATH::GIdentityMatrixD;
				Scale.data[0] = _vector.x;
				Scale.data[5] = _vector.y;
				Scale.data[10] = _vector.z;

				MultiplyMatrixD(Scale, _matrix, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn LerpD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, double _ratio, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD _m1 = _matrix1;
				GW::MATH::GMATRIXD _m2 = _matrix2;
				for(int i = 0; i < 16; i++)
				{
					_outMatrix.data[i] = G_LERP(_m1.data[i], _m2.data[i], _ratio);
				}
				return GW::GReturn::SUCCESS;
			}

			static GReturn ProjectionDirectXLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY / 2.0);
				double z = _zf / (_zf - _zn);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = 1.0;
				_outMatrix.row4.data[2] = -_zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionDirectXRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY * 0.5);
				double z = _zf / (_zn - _zf);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = -1.0;
				_outMatrix.row4.data[2] = _zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionOpenGLRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY / 2.0);
				double z = _zf - _zn;

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = -((_zf + _zn) / z);
				_outMatrix.row3.data[3] = -1.0;
				_outMatrix.row4.data[2] = -((2.0 * _zf * _zn) / z);

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionOpenGLLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY / 2.0);
				double z = _zn - _zf; // note these are also swapped for LH in OGL

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = yScale;
				_outMatrix.row3.data[2] = -((_zf + _zn) / z);
				_outMatrix.row3.data[3] = 1.0;
				_outMatrix.row4.data[2] = ((2.0 * _zf * _zn) / z);

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionVulkanLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY * 0.5);
				double z = _zf / (_zf - _zn);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = -yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = 1.0;
				_outMatrix.row4.data[2] = -_zn * z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn ProjectionVulkanRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix)
			{
				if (G_WITHIN_STANDARD_DEVIATION_D(_aspect, 0.0)) return GW::GReturn::FAILURE;

				double yScale = 1.0 / tan(_fovY * 0.5);
				double z = _zf / (_zn - _zf);

				_outMatrix = {};
				_outMatrix.row1.data[0] = yScale / _aspect;
				_outMatrix.row2.data[1] = -yScale;
				_outMatrix.row3.data[2] = z;
				_outMatrix.row3.data[3] = -1.0;
				_outMatrix.row4.data[2] = _zn * z;

				return GW::GReturn::SUCCESS;
			}

			static GReturn LookAtLHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixD;
				GW::MATH::GVECTORD temp;
				GW::MATH::GVECTORD camForward;	//zaxis
				GW::MATH::GVECTORD camRight;		//xaxis
				GW::MATH::GVECTORD camUp;			//yaxis
				double magnitudeX;
				double magnitudeZ;

				//forward = targetPosition - eyePosition 
				camForward.x = _at.x - _eye.x;
				camForward.y = _at.y - _eye.y;
				camForward.z = _at.z - _eye.z;

				magnitudeZ = sqrt((camForward.x * camForward.x) + (camForward.y * camForward.y) + (camForward.z * camForward.z));
				if(G_WITHIN_STANDARD_DEVIATION_D(magnitudeZ, 0)) return GW::GReturn::FAILURE;

				//normalize forward
				camForward.x /= magnitudeZ;
				camForward.y /= magnitudeZ;
				camForward.z /= magnitudeZ;

				//right = Cross(worldUp, camForward)
				camRight.x = (_up.y * camForward.z) - (_up.z * camForward.y);
				camRight.y = (_up.z * camForward.x) - (_up.x * camForward.z);
				camRight.z = (_up.x * camForward.y) - (_up.y * camForward.x);

				magnitudeX = sqrt((camRight.x * camRight.x) + (camRight.y * camRight.y) + (camRight.z * camRight.z));
				if(G_WITHIN_STANDARD_DEVIATION_D(magnitudeX, 0)) return GW::GReturn::FAILURE;


				//normalize right
				camRight.x /= magnitudeX;
				camRight.y /= magnitudeX;
				camRight.z /= magnitudeX;


				//up = cross (camForward, camRight)
				camUp.x = (camForward.y * camRight.z) - (camForward.z * camRight.y);
				camUp.y = (camForward.z * camRight.x) - (camForward.x * camRight.z);
				camUp.z = (camForward.x * camRight.y) - (camForward.y * camRight.x);

				//invert our position by our orientation (to turn this world matrix into a view matrix) 
				temp.x = camRight.x * _eye.x + camRight.y * _eye.y + camRight.z * _eye.z;
				temp.y = camUp.x * _eye.x + camUp.y * _eye.y + camUp.z * _eye.z;
				temp.z = camForward.x * _eye.x + camForward.y * _eye.y + camForward.z * _eye.z;

				//compose results into matrix
				_outMatrix.data[0] = camRight.x;
				_outMatrix.data[4] = camRight.y;
				_outMatrix.data[8] = camRight.z;
				_outMatrix.data[12] = -temp.x;

				_outMatrix.data[1] = camUp.x;
				_outMatrix.data[5] = camUp.y;
				_outMatrix.data[9] = camUp.z;
				_outMatrix.data[13] = -temp.y;

				_outMatrix.data[2] = camForward.x;
				_outMatrix.data[6] = camForward.y;
				_outMatrix.data[10] = camForward.z;
				_outMatrix.data[14] = -temp.z;

				return GW::GReturn::SUCCESS;
			}
			static GReturn LookAtRHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix)
			{
				_outMatrix = GW::MATH::GIdentityMatrixD;
				GW::MATH::GVECTORD temp;
				GW::MATH::GVECTORD camForward;	//zaxis
				GW::MATH::GVECTORD camRight;		//xaxis
				GW::MATH::GVECTORD camUp;			//yaxis
				double magnitudeX;
				double magnitudeZ;

				//forward = targetPosition - eyePosition 
				camForward.x = _eye.x - _at.x;
				camForward.y = _eye.y - _at.y;
				camForward.z = _eye.z - _at.z;

				magnitudeZ = sqrt((camForward.x * camForward.x) + (camForward.y * camForward.y) + (camForward.z * camForward.z));
				if (G_WITHIN_STANDARD_DEVIATION_D(magnitudeZ, 0)) return GW::GReturn::FAILURE;

				//normalize forward
				camForward.x /= magnitudeZ;
				camForward.y /= magnitudeZ;
				camForward.z /= magnitudeZ;

				//right = Cross(worldUp, camForward)
				camRight.x = (_up.y * camForward.z) - (_up.z * camForward.y);
				camRight.y = (_up.z * camForward.x) - (_up.x * camForward.z);
				camRight.z = (_up.x * camForward.y) - (_up.y * camForward.x);

				magnitudeX = sqrt((camRight.x * camRight.x) + (camRight.y * camRight.y) + (camRight.z * camRight.z));
				if (G_WITHIN_STANDARD_DEVIATION_D(magnitudeX, 0)) return GW::GReturn::FAILURE;


				//normalize right
				camRight.x /= magnitudeX;
				camRight.y /= magnitudeX;
				camRight.z /= magnitudeX;


				//up = cross (camForward, camRight)
				camUp.x = (camForward.y * camRight.z) - (camForward.z * camRight.y);
				camUp.y = (camForward.z * camRight.x) - (camForward.x * camRight.z);
				camUp.z = (camForward.x * camRight.y) - (camForward.y * camRight.x);

				//invert our position by our orientation (to turn this world matrix into a view matrix) 
				temp.x = camRight.x * _eye.x + camRight.y * _eye.y + camRight.z * _eye.z;
				temp.y = camUp.x * _eye.x + camUp.y * _eye.y + camUp.z * _eye.z;
				temp.z = camForward.x * _eye.x + camForward.y * _eye.y + camForward.z * _eye.z;

				//compose results into matrix
				_outMatrix.data[0] = camRight.x;
				_outMatrix.data[4] = camRight.y;
				_outMatrix.data[8] = camRight.z;
				_outMatrix.data[12] = -temp.x;

				_outMatrix.data[1] = camUp.x;
				_outMatrix.data[5] = camUp.y;
				_outMatrix.data[9] = camUp.z;
				_outMatrix.data[13] = -temp.y;

				_outMatrix.data[2] = camForward.x;
				_outMatrix.data[6] = camForward.y;
				_outMatrix.data[10] = camForward.z;
				_outMatrix.data[14] = -temp.z;

				return GW::GReturn::SUCCESS;
			}

			static GReturn MakeRelativeD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix)
			{
				GW::MATH::GMATRIXD inverseMatrix2;
				GW::GReturn retval = InverseD(_matrix2, inverseMatrix2);

				MultiplyMatrixD(_matrix1, inverseMatrix2, _outMatrix);

				return retval;
			}
			static GReturn MakeSeparateD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix)
			{
				MultiplyMatrixD(_matrix1, _matrix2, _outMatrix);

				return GW::GReturn::SUCCESS;
			}

			static GReturn Downgrade(GW::MATH::GMATRIXD _matrixD, GW::MATH::GMATRIXF& _outMatrixF) 
			{
				for (int i = 0; i < 16; i++)
				{
					_outMatrixF.data[i] = static_cast<float>(_matrixD.data[i]);
				}

				return GReturn::SUCCESS;
			}
		};
	}
}
