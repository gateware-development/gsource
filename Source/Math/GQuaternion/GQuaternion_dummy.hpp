namespace GW
{
	namespace I
	{
		class GQuaternionImplementation : public virtual GQuaternionInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			// Floats
			static GReturn AddQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SubtractQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn MultiplyQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn ScaleF(MATH::GQUATERNIONF _quaternion, float _scalar, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SetByVectorAngleF(MATH::GVECTORF _vector, float _radian, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SetByMatrixF(MATH::GMATRIXF _matrix, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn DotF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float& _outValue)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn ConjugateF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn InverseF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn MagnitudeF(MATH::GQUATERNIONF _quaternion, float& _outMagnitude)
			{
				return GReturn::FAILURE;
			}
			static GReturn NormalizeF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn IdentityF(MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn LerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SlerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn Upgrade(MATH::GQUATERNIONF _quaternionF, MATH::GQUATERNIOND& _outQuaternionD) 
			{
				return GReturn::FAILURE; 
			}
			// Doubles
			static GReturn AddQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SubtractQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn MultiplyQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn ScaleD(MATH::GQUATERNIOND _quaternion, double _scalar, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SetByVectorAngleD(MATH::GVECTORD _vector, double _radian, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SetByMatrixD(MATH::GMATRIXD _matrix, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn DotD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn ConjugateD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn InverseD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn MagnitudeD(MATH::GQUATERNIOND _quaternion, double& _outMagnitude)
			{
				return GReturn::FAILURE;
			}
			static GReturn NormalizeD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn IdentityD(MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn LerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, float _ratio, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn SlerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion)
			{
				return GReturn::FAILURE;
			}
			static GReturn Downgrade(MATH::GQUATERNIOND _quaternionD, MATH::GQUATERNIONF& _outQuaternionF)
			{
				return GReturn::FAILURE; 
			}
		};
	}
}