#include <cmath>

namespace GW
{
	namespace I
	{
		class GVectorImplementation : public virtual GVectorInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}

			//float vector
			static GReturn AddVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;
				_outVector.z = _vector1.z + _vector2.z;
				_outVector.w = _vector1.w + _vector2.w;

				return GReturn::SUCCESS;
			}
			static GReturn SubtractVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;
				_outVector.z = _vector1.z - _vector2.z;
				_outVector.w = _vector1.w - _vector2.w;

				return GReturn::SUCCESS;
			}
			static GReturn ScaleF(MATH::GVECTORF _vector, float _scalar, MATH::GVECTORF& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;
				_outVector.z = _scalar * _vector.z;
				_outVector.w = _scalar * _vector.w;

				return GReturn::SUCCESS;
			}

			static GReturn DotF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y) + (_vector1.z * _vector2.z) + (_vector1.w * _vector2.w);

				return GReturn::SUCCESS;
			}
			static GReturn CrossVector2F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue)
			{
				_outValue = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			}
			static GReturn CrossVector3F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				_outVector.x = (_vector1.y * _vector2.z) - (_vector1.z * _vector2.y);
				_outVector.y = (_vector1.z * _vector2.x) - (_vector1.x * _vector2.z);
				_outVector.z = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);
				_outVector.w = _vector1.w * _vector2.w;

				return GReturn::SUCCESS;
			}

			static GReturn VectorXMatrixF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector)
			{
				MATH::GVECTORF _v = _vector;

				_outVector.x = (_v.x * _matrix.row1.x) + (_v.y * _matrix.row2.x) + (_v.z * _matrix.row3.x) + (_v.w * _matrix.row4.x);
				_outVector.y = (_v.x * _matrix.row1.y) + (_v.y * _matrix.row2.y) + (_v.z * _matrix.row3.y) + (_v.w * _matrix.row4.y);
				_outVector.z = (_v.x * _matrix.row1.z) + (_v.y * _matrix.row2.z) + (_v.z * _matrix.row3.z) + (_v.w * _matrix.row4.z);
				_outVector.w = (_v.x * _matrix.row1.w) + (_v.y * _matrix.row2.w) + (_v.z * _matrix.row3.w) + (_v.w * _matrix.row4.w);

				return GReturn::SUCCESS;
			}
			static GReturn TransformF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x) + (_vector.z * _matrix.row3.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y) + (_vector.z * _matrix.row3.y);
				_outVector.z = (_vector.x * _matrix.row1.z) + (_vector.y * _matrix.row2.z) + (_vector.z * _matrix.row3.z);
				_outVector.w = _vector.w;

				return GReturn::SUCCESS;
			}

			static GReturn MagnitudeF(MATH::GVECTORF _vector, float& _outMagnitude)
			{
				_outMagnitude = sqrtf((_vector.x * _vector.x) + (_vector.y * _vector.y) + (_vector.z * _vector.z) + (_vector.w * _vector.w));
				return GReturn::SUCCESS;
			}
			static GReturn NormalizeF(MATH::GVECTORF _vector, MATH::GVECTORF& _outVector)
			{
				GReturn returnCode = GReturn::FAILURE;

				float magnitude = 0.0f, reciprocalMagnitude = 0.0f;
				MagnitudeF(_vector, magnitude);

				if (G_WITHIN_STANDARD_DEVIATION_F(magnitude, 0) == false)
				{
					reciprocalMagnitude = 1.0f / magnitude;
					returnCode = GReturn::SUCCESS;
				}


				_outVector.x = _vector.x * reciprocalMagnitude;
				_outVector.y = _vector.y * reciprocalMagnitude;
				_outVector.z = _vector.z * reciprocalMagnitude;
				_outVector.w = _vector.w * reciprocalMagnitude;

				return returnCode;
			}

			static GReturn LerpF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float _ratio, MATH::GVECTORF& _outVector)
			{
				_outVector.x = G_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G_LERP(_vector1.y, _vector2.y, _ratio);
				_outVector.z = G_LERP(_vector1.z, _vector2.z, _ratio);
				_outVector.w = G_LERP(_vector1.w, _vector2.w, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn SplineF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF _vector3, MATH::GVECTORF _vector4, float _ratio, MATH::GVECTORF& _outVector)
			{
				float t0 = 0.0f;
				float t1 = powf(sqrtf(powf((_vector2.x - _vector1.x), 2) + powf((_vector2.y - _vector1.y), 2) + powf((_vector2.z - _vector1.z), 2)), 0.5f) + t0;
				float t2 = powf(sqrtf(powf((_vector3.x - _vector2.x), 2) + powf((_vector3.y - _vector2.y), 2) + powf((_vector3.z - _vector2.z), 2)), 0.5f) + t1;
				float t3 = powf(sqrtf(powf((_vector4.x - _vector3.x), 2) + powf((_vector4.y - _vector3.y), 2) + powf((_vector4.z - _vector3.z), 2)), 0.5f) + t2;

				MATH::GVECTORF A1;
				MATH::GVECTORF A2;
				MATH::GVECTORF A3;
				MATH::GVECTORF B1;
				MATH::GVECTORF B2;

				float t = t1 + (t2 - t1) * _ratio;

				for (int i = 0; i < 3; i++)
				{
					A1.data[i] = (t1 - t) / (t1 - t0) * _vector1.data[i] + (t - t0) / (t1 - t0) * _vector2.data[i];
					A2.data[i] = (t2 - t) / (t2 - t1) * _vector2.data[i] + (t - t1) / (t2 - t1) * _vector3.data[i];
					A3.data[i] = (t3 - t) / (t3 - t2) * _vector3.data[i] + (t - t2) / (t3 - t2) * _vector4.data[i];

					B1.data[i] = (t2 - t) / (t2 - t0) * A1.data[i] + (t - t0) / (t2 - t0) * A2.data[i];
					B2.data[i] = (t3 - t) / (t3 - t1) * A2.data[i] + (t - t1) / (t3 - t1) * A3.data[i];

					_outVector.data[i] = (t2 - t) / (t2 - t1) * B1.data[i] + (t - t1) / (t2 - t1) * B2.data[i];
				}
				_outVector.w = 0;

				return GReturn::SUCCESS;
			}

			static GReturn Upgrade(MATH::GVECTORF _vectorF, MATH::GVECTORD& _outVectorD)
			{
				_outVectorD.x = static_cast<double>(_vectorF.x);
				_outVectorD.y = static_cast<double>(_vectorF.y);
				_outVectorD.z = static_cast<double>(_vectorF.z);
				_outVectorD.w = static_cast<double>(_vectorF.w);

				return GReturn::SUCCESS;
			}


			//double vector
			static GReturn AddVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				_outVector.x = _vector1.x + _vector2.x;
				_outVector.y = _vector1.y + _vector2.y;
				_outVector.z = _vector1.z + _vector2.z;
				_outVector.w = _vector1.w + _vector2.w;

				return GReturn::SUCCESS;
			}
			static GReturn SubtractVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				_outVector.x = _vector1.x - _vector2.x;
				_outVector.y = _vector1.y - _vector2.y;
				_outVector.z = _vector1.z - _vector2.z;
				_outVector.w = _vector1.w - _vector2.w;

				return GReturn::SUCCESS;
			}
			static GReturn ScaleD(MATH::GVECTORD _vector, double _scalar, MATH::GVECTORD& _outVector)
			{
				_outVector.x = _scalar * _vector.x;
				_outVector.y = _scalar * _vector.y;
				_outVector.z = _scalar * _vector.z;
				_outVector.w = _scalar * _vector.w;

				return GReturn::SUCCESS;
			}

			static GReturn DotD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue)
			{
				_outValue = (_vector1.x * _vector2.x) + (_vector1.y * _vector2.y) + (_vector1.z * _vector2.z) + (_vector1.w * _vector2.w);

				return GReturn::SUCCESS;
			}
			static GReturn CrossVector2D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue)
			{
				_outValue = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);

				return GReturn::SUCCESS;
			}
			static GReturn CrossVector3D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				_outVector.x = (_vector1.y * _vector2.z) - (_vector1.z * _vector2.y);
				_outVector.y = (_vector1.z * _vector2.x) - (_vector1.x * _vector2.z);
				_outVector.z = (_vector1.x * _vector2.y) - (_vector1.y * _vector2.x);
				_outVector.w = _vector1.w * _vector2.w;

				return GReturn::SUCCESS;
			}

			static GReturn VectorXMatrixD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector)
			{
				MATH::GVECTORD _v = _vector;
				_outVector.x = (_v.x * _matrix.row1.x) + (_v.y * _matrix.row2.x) + (_v.z * _matrix.row3.x) + (_v.w * _matrix.row4.x);
				_outVector.y = (_v.x * _matrix.row1.y) + (_v.y * _matrix.row2.y) + (_v.z * _matrix.row3.y) + (_v.w * _matrix.row4.y);
				_outVector.z = (_v.x * _matrix.row1.z) + (_v.y * _matrix.row2.z) + (_v.z * _matrix.row3.z) + (_v.w * _matrix.row4.z);
				_outVector.w = (_v.x * _matrix.row1.w) + (_v.y * _matrix.row2.w) + (_v.z * _matrix.row3.w) + (_v.w * _matrix.row4.w);

				return GReturn::SUCCESS;
			}
			static GReturn TransformD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector)
			{
				_outVector.x = (_vector.x * _matrix.row1.x) + (_vector.y * _matrix.row2.x) + (_vector.z * _matrix.row3.x);
				_outVector.y = (_vector.x * _matrix.row1.y) + (_vector.y * _matrix.row2.y) + (_vector.z * _matrix.row3.y);
				_outVector.z = (_vector.x * _matrix.row1.z) + (_vector.y * _matrix.row2.z) + (_vector.z * _matrix.row3.z);
				_outVector.w = _vector.w;

				return GReturn::SUCCESS;
			}

			static GReturn MagnitudeD(MATH::GVECTORD _vector, double& _outMagnitude)
			{
				_outMagnitude = sqrt((_vector.x * _vector.x) + (_vector.y * _vector.y) + (_vector.z * _vector.z) + (_vector.w * _vector.w));
				return GReturn::SUCCESS;
			}
			static GReturn NormalizeD(MATH::GVECTORD _vector, MATH::GVECTORD& _outVector)
			{
				GReturn returnCode = GReturn::FAILURE;

				double magnitude = 0.0, reciprocalMagnitude = 0.0;
				MagnitudeD(_vector, magnitude);

				if (magnitude != 0.0)
				{
					reciprocalMagnitude = 1.0 / magnitude;
					returnCode = GReturn::SUCCESS;
				}


				_outVector.x = _vector.x * reciprocalMagnitude;
				_outVector.y = _vector.y * reciprocalMagnitude;
				_outVector.z = _vector.z * reciprocalMagnitude;
				_outVector.w = _vector.w * reciprocalMagnitude;

				return returnCode;
			}

			static GReturn LerpD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double _ratio, MATH::GVECTORD& _outVector)
			{
				_outVector.x = G_LERP(_vector1.x, _vector2.x, _ratio);
				_outVector.y = G_LERP(_vector1.y, _vector2.y, _ratio);
				_outVector.z = G_LERP(_vector1.z, _vector2.z, _ratio);
				_outVector.w = G_LERP(_vector1.w, _vector2.w, _ratio);

				return GReturn::SUCCESS;
			}

			static GReturn SplineD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD _vector3, MATH::GVECTORD _vector4, double _ratio, MATH::GVECTORD& _outVector)
			{
				double t0 = 0.0;
				double t1 = t1 = pow(sqrt(pow((_vector2.x - _vector1.x), 2.0) + pow((_vector2.y - _vector1.y), 2.0) + pow((_vector2.z - _vector1.z), 2.0)), 0.5) + t0;
				double t2 = t2 = pow(sqrt(pow((_vector3.x - _vector2.x), 2.0) + pow((_vector3.y - _vector2.y), 2.0) + pow((_vector3.z - _vector2.z), 2.0)), 0.5) + t1;
				double t3 = t3 = pow(sqrt(pow((_vector4.x - _vector3.x), 2.0) + pow((_vector4.y - _vector3.y), 2.0) + pow((_vector4.z - _vector3.z), 2.0)), 0.5) + t2;

				MATH::GVECTORD A1;
				MATH::GVECTORD A2;
				MATH::GVECTORD A3;
				MATH::GVECTORD B1;
				MATH::GVECTORD B2;

				double t = t1 + (t2 - t1) * _ratio;

				for (int i = 0; i < 3; i++)
				{
					A1.data[i] = (t1 - t) / (t1 - t0) * _vector1.data[i] + (t - t0) / (t1 - t0) * _vector2.data[i];
					A2.data[i] = (t2 - t) / (t2 - t1) * _vector2.data[i] + (t - t1) / (t2 - t1) * _vector3.data[i];
					A3.data[i] = (t3 - t) / (t3 - t2) * _vector3.data[i] + (t - t2) / (t3 - t2) * _vector4.data[i];

					B1.data[i] = (t2 - t) / (t2 - t0) * A1.data[i] + (t - t0) / (t2 - t0) * A2.data[i];
					B2.data[i] = (t3 - t) / (t3 - t1) * A2.data[i] + (t - t1) / (t3 - t1) * A3.data[i];

					_outVector.data[i] = (t2 - t) / (t2 - t1) * B1.data[i] + (t - t1) / (t2 - t1) * B2.data[i];
				}
				_outVector.w = 0;
				return GReturn::SUCCESS;
			}

			static GReturn Downgrade(MATH::GVECTORD _vectorD, MATH::GVECTORF& _outVectorF)
			{
				_outVectorF.x = static_cast<float>(_vectorD.x);
				_outVectorF.y = static_cast<float>(_vectorD.y);
				_outVectorF.z = static_cast<float>(_vectorD.z);
				_outVectorF.w = static_cast<float>(_vectorD.w);

				return GReturn::SUCCESS;
			}
		};
	};
}