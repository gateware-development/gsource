# Introduction

To contribute to the project and support the active and on-going development, all you have to do is fork the project.
However, this document will explain the proper guidelines for how to do this, and what rules should be followed.

## Forking

Simply go to the [GSource project](https://gitlab.com/gateware-development/gsource) and fork the project from there.

After you have forked the project, clone it into your desired location and go into the directory.

## Building

To build the project, go to `HOWTOBUILD.txt` in the DevOps folder and follow the instruction there for your platform.

## Creating a new change

To do this, do a quick check.

- Ensure that you are on the latest changes in the main branch (section above). You can do this by going to your forked
repository on GitLab and seeing if it prompts for syncing with the base repository.

After doing this, create a new branch using your preferred git GUI or git command-line directly.

`git checkout -b your-branch-name refs/heads/master --no-track`

### Creating your MR (Merge Request)

To create an MR from your new changes, simply go to GitLab and, if your changes are pushed, you should be a small
prompt on the home page of the repository enquiring if you want to make an MR. By default, when you make the MR, it will
target the main branch in the base repository. This is the branch we typically want to target.

*Remember to follow proper guidelines with your MR!*

- Give a good title
- Explain your changes in detail and give proper information as to *why* these changes should be submitted
- Keep it concise. You don't need to write anything crazy, a simple list of the changes and a short summary is enough.
Additionally, we don't need so much detail for the MR itself since the commits and file changes should explain your
changes thoroughly enough.

Something I feel should be noted is that with titles, aim for a title that acts as a commit. Particularly, the final commit.
This title for the issue should have an easy to read summary that explains what all your other commits do. Examples for this
are easy.

Fixing a window resize bug on Linux? "fix: Window resize not working on Linux".

Trouble with a controller not being detected? "fix: GInput not detecting controller".

Just doing some code refactoring? "refactor: Swap raw pointers to `std::unique_ptrs`".

Having a good title is important, and these keep not only the MR easy to read, but the git log for the project itself clean
as the title is stored in the MR commit done on merge. The main takeaway should be, ensure your MR is easy to work with and provides
the needed information to properly review the changes.

#### Example

Title:

fix: Window resize not working on Linux

Description:

Small issue with the window resize not working on Linux was fixed. It appeared to be a miscommunication with X11 not
updating the state of the window in time before events began being processed.

(This in GitLab will close the issue linked on merge)

Closes #123

# Updating main branch

To update your main branch all you have to do is go your forked repository's home page, and at the top of the repository, you
should see a banner mentioning that your branch is out-of-date. This will provide a handy `Update Fork` button that you
can press and update the forked main branch automatically. However, if you wish to update the main branch through CLI (command-line interface),
or you do not have access to GitLab in the moment, you can follow the instruction below on how to update through CLI
accordingly (optional, but recommended).

### Configuring

Configuration of the project is important, not doing these steps can cause problems down the line as the project continues
taking on new changes. So it's best to remember these.

First, ensure that you have an additional upstream configured in git that points to the base repo.

`git remote add upstream https://gitlab.com/gateware-development/gsource.git`

This will add a new remote called `upstream` that will target the base repository. This is useful if you want to sync
the main branch *without* using GitLab (such as using GitKracken or LazyGit).

Once you've done this, you are now configured.

### Syncing main branch

Syncing the main branch with the base repo is relatively straight forward.

`git rebase --interactive --autostash --keep-empty --no-autosquash --rebase-merges upstream/master`

Since our `master` branch is purely acting as a reflection of the base repo's master branch, we will just rebase instead
of merging, this will remove the need to have merge commits for simple updates of the master branch. You may also,
`git push` your master branch to update your own forked master branch, keeping everything updated.
