#! /bin/bash

# Contributors: Lari Norri, Caio Tavares, Colby Peck, Alexander Cusaac

DEFAULT_VALUE="CodeLite - Unix Makefiles"

VALUE="${1:-$DEFAULT_VALUE}"

echo "Linux Setup. . ."
cd ../

echo "Executing CMake debug build. . ."
cmake ./ -G "$VALUE" -DCMAKE_BUILD_TYPE=Debug -B ./build/debug

echo "Executing CMake release build. . ."
cmake ./ -G "$VALUE" -DCMAKE_BUILD_TYPE=Release -B ./build/release

#the following commands are needed since linux does not support PRE_BUILD steps in cmake. They are done in PRE_BUILD steps in SingleHeaderTest/CMakeLists.txt on mac/windows
echo "Generating single header and version info. . ."
cd DevOps || exit
cmake -P ./GenVerHpp.cmake
cmake -P ./GenTempSingleHeader.cmake

echo "***Linux Setup Finish***"
