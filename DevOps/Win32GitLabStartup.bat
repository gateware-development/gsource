:: This script allows GitLab runners installed on native Windows machines to access GPU resources.
:: By default GitLab runners run as a windows service, while conveinent this disallows GPU access.
:: After you register and install your GitLab runner on a windows machine do the following four steps:
:: STEP A - Disable automatic startup of GitLab runner service:
:: 1.) In the windows search bar type: "services" (no quotes) and press enter.
:: 2.) Search the list that pops-up for "gitlab-runner" and right-click and select properties.
:: 3.) Switch the "Startup Type:" from "Automatic" to "Manual" mode and press "Apply". 
:: STEP B - Install this direct runner launch script on machine startup.
:: 1.) In the windows search bar type: "run" (no quotes) and press enter.
:: 2.) In the Run prompt type: "shell:common startup" (again no quotes) and press enter.
:: 3.) A folder should pop-up, copy this script to that folder. (You will need Admin access)
:: 4.) Reboot the machine and you should see a minimized terminal running GitLab natively open at startup.
:: 5.) (optional) Use a third party program to hide this batch file in the system tray if desired.
:: STEP C - Modify the "config.toml" file to correctly use powershell.
:: 1.) Navigate to the default install location of the runner.(typically C:\GitLab-Runner)
:: 2.) Open the "config.toml" file using notepad or some other text editor.
:: 3.) Modify the line < shell = "pwsh" > to < shell = "powershell" > and save the file.
:: 4.) Reboot the machine or restart the runner in the terminal.
:: STEP D - Install required command line tools and setup the machine for development builds.
:: 1.) See the "HOWTOBUILD.txt" document in this folder for details on how to setup a windows dev machine.
:: 2.) In addition to the above, download and install command line access to GIT and Ruby.
:: 3.) After installing all the additional compilers, software and SDKs be sure to restart the machine.
:: NOTE: This script assumes you installed the GitLab runner to the deafult location.(typically C:\GitLab-Runner)
::       If you installed the runner in a different location just adjust the script below appropriately.
@echo on
cd C:\GitLab-Runner
start /min cmd /c gitlab-runner.exe run --config C:\GitLab-Runner\config.toml --service gitlab-runner --syslog
:: Replace above with below to run unminimized (good for debugging)
::gitlab-runner.exe run --config C:\GitLab-Runner\config.toml --service gitlab-runner --syslog
  