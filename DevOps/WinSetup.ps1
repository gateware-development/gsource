# Author: Alexander Cusaac
# Date: 12/01/2023
# Description: This script will install the necessary tools to build the project on Windows.

# Check if winget is installed
winget --version

if ($LASTEXITCODE -eq 0) {
    Write-Output "Winget already installed."
} else {
    Write-Output "Winget not installed..."
    Write-Output "Install winget from Microsoft Store by installing App Installer..."
    Pause
    exit
}

# Install nuget
winget install Microsoft.NuGet -e --accept-source-agreements --accept-package-agreements --force

# Refresh PATH
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")

Set-Location ../

# Create the build directory if it doesn't exist
if (Test-Path -PathType Container "build") {
    Write-Output "Build directory already exists."
} else {
    mkdir build
}

Set-Location build

# Create the Desktop directory if it doesn't exist
if (Test-Path -PathType Container "Desktop") {
    Write-Output "Desktop directory already exists."
} else {
    mkdir Desktop
}

Set-Location Desktop

# Run cmake to generate the build files for the Desktop project
cmake -A x64 "../../"

# Errorlevel doesn't work in powershell, so we have to use $LASTEXITCODE instead
if ($LASTEXITCODE -eq 0) {
    Write-Output "Build successful."
} else {
    Write-Output "Build failed."
}

Set-Location ..

# Create the UWP directory if it doesn't exist
if (Test-Path -PathType Container "UWP") {
    Write-Output "UWP directory already exists."
} else {
    mkdir UWP
}

Set-Location UWP

# Run cmake to generate the build files for the UWP project
cmake -A x64 "../../App/UWP"

# Errorlevel doesn't work in powershell, so we have to use $LASTEXITCODE instead
if ($LASTEXITCODE -eq 0) {
    Write-Output "Build successful."
} else {
    Write-Output "Build failed."
}

Pause
