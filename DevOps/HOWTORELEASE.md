###### Updated: 3/29/2022

These instruction are out of date now. We are moving to a continuous deployment model.
Currently the project version and Gateware.h will be automatically updated as you develop.
The only thing not auto generated yet is the doxygen documentation.
The "Gateware" folder on the master branch will now be our offical "Release".

###### Last Updated: 12/01/2020

# Gateware Release Instructions

## Before You Begin
The following should only be done after the developer branch of Gateware has been thoroughly tested on each platform successfully in both debug and release mode. You will also need Lari’s permission to proceed with the release.

## Steps to Release

#### A. Preparing the release candidate
1. Open GitKraken.
2. Checkout the developer branch and pull the latest commit if you have not already.
3. Right click on the development branch and select “Create tag here”
4. Name the tag “Candidate_<VERSION>”, replacing <VERSION> with the version number of the release (ex. Candidate_1.2a).
5. With the tag created, right click on it and select push tag.

###### Note: You will need maintainer access beyond this point.

#### B. Pushing the release to Gateware’s master branch
1. In GitKraken, checkout the master branch.
2. Drag the development branch onto the master branch.
3. Select fast forward master to origin/development. DON’T PUSH YET.
4. The master branch, development branch, and candidate tag should all be together in the branch graph.
5. In file explorer, go to “gateware/source/shared” folder.
6. Open “gateware.ver” and update the version.
7. In file explorer, go to “gateware” folder.
8. Delete the “build“ folder, if it exists.
9. In file explorer, go to “gateware/DevOps” folder.
10. Install CMake, if you haven't already.
11. Build the project by double-clicking WinSetup.bat. It will run CMake and update the version in the source.
12. Open up the project in Visual Studio to give it one final run to make sure everything is working correctly before the next step.
13. Push the master branch.
14. Right click on the master branch and select “Create tag here”
15. Name the tag “Release_<VERSION>”, replacing <VERSION> with the version number of the release (ex. Release_1.2a).
16. With the tag created, right click on it, and select push tag to origin.

#### C. Releasing Gateware on GitLab
1. Log in to GitLab.
2. Go to gitlab.com/gateware-development/gateware
3. On the left side of the page, click “Repository”, then click “Tags”
4. Edit the release notes.
5. On the left side of the page, click “Project Overview”, then click “Releases”
6. You should see the release.
7. Click edit release. The release notes are to follow the same format as the previous releases. Use them for reference.
8. Add the following text to the release notes, “API Single Header (recommended for end users)”
9. Attach to the release zip for the single-header. Use the previous release zip as a reference. To prepare the zip, do the following:
   1. Create a folder for the release
   2. Add the documentation
      1. Install Doxygen GUI, if it’s not already installed.
      2. Run Doxygen GUI.
      3. Click File > Open
      4. Open “gateware/DevOps/doxyfile”
      5. Change the version
      6. Save
      7. Click the “Run” tab
      8. Click “Run Doxygen”
      9. Preview the documentation after it is finished and make sure everything looks good.
      10. In the folder you created for the release, add a folder named “documentation”
      11. Add the latex and html folders that doxygen generated to the documentation folder.
   3. Add Gateware.h
      1. In file explorer, go to “gateware/DevOps/Windows”
      2. Generate a new Gateware.h using the GWHeaderCompiler.exe. Use --help to see args. Be sure to use "./" in front of all arguments so pathing works right. Sometimes it doesn't show the l  argument in --help. It is for the output folder. 
      3. Add the generated Gateware.h to the folder you created.
   4. Add the GatewareLogo.png. You can get it from the “gateware/DevOps” folder.
   5. Add the LICENSE.md. You can get it from the “gateware” folder.
   6. Add the README.md. You can get it from the “gateware” folder.
   7. Compress all those files in a way that does not create an extra folder.
   8. Name the zip file “Gateware_r<VERSION>.zip”, replacing <VERSION> with the version number of the release (ex. Gateware_r1.2a.zip).
10. Add a bullet-pointed summary of the change log to the release notes.
11. Click “Save Changes”

#### D. Updating the templates
1. In GitKraken, open the templates repo and pull the latest development commit.
2. Replace the Gateware folder contents with the contents of the Gateware zip you made in step 31.
3. Build/Run the templates to ensure they all still work on their various intended platforms. Fix any interface changes that break compatibility.
4. Commit the changes using the message format "Templates Updated to Release <VERSION>!", replacing <VERSION> with the version number of the release.
5. One everything is working again pull/push the changes into the master branch. (maintainer access required)

#### E. Spreading the news
1. Login to Discord and go to the Gateware server.
2. In the #change-log channel post a message with the change log. Follow a similar format to the previous posts.
3. In the #new-releases channel post a message announcing the release and providing a link. Follow a similar format to the previous posts.

## Finished!
